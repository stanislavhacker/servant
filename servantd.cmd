@IF EXIST "%~dp0\node.exe" (
  "%~dp0\node.exe"  "%~dp0\servant-cli\dist\servant-cli" %*
) ELSE (
  @SETLOCAL
  @SET PATHEXT=%PATHEXT:;.JS;=;%
  node  "%~dp0\servant-cli\dist\servant-cli" %*
)