import * as dgram from "dgram";

import { PORT_FROM, PORT_TO } from "../ports";
import {
	Command,
	CommandClose,
	commandClose,
	CommandData,
	commandData,
	commandHello,
	CommandPing,
	commandPing,
	CommandType,
} from "../commands";

const DISCONNECT_TIMEOUT = 20000; //20s

type DiscoveryServerState = {
	id: string;
	port: number;
	running: boolean;
	heartbeat: {
		frame: number;
		ping: NodeJS.Timer | null;
		live: NodeJS.Timer | null;
	};
	instances: {
		[key: string]: DiscoveryInstance;
	};
};

export type DiscoveryInstance = {
	id: string;
	port: number;
	//time from last live ping
	tfl: number;
};

export type DiscoveryServerSettings = {
	onError?: (err: Error) => void;
	onData?: (instance: DiscoveryInstance, data: unknown) => void;
	onStart?: (instance: DiscoveryInstance) => void;
	onClose?: (instance: DiscoveryInstance) => void;
	onConnect?: (instance: DiscoveryInstance) => void;
	onDisconnect?: (instance: DiscoveryInstance) => void;
};

export type DiscoveryServerApi = {
	id: string;
	port: () => number;
	send: (data: unknown, to?: DiscoveryInstance[]) => void;
	close: () => void;
};

export function createDiscoveryServer(settings: DiscoveryServerSettings = {}): DiscoveryServerApi {
	const state = createState();
	const client = dgram.createSocket("udp4");

	listeningStart(client, state, settings);
	listeningClose(client, state, settings);
	//start discovering
	discoverPort(client, state, settings);

	return {
		id: state.id,
		port: () => state.port,
		send: (data, to = []) =>
			sendCommand(
				client,
				state,
				commandData(
					state.id,
					state.port,
					data,
					to.map((int) => int.id)
				)
			),
		close: () => {
			sendCommand(client, state, commandClose(state.id, state.port), () => {
				client.close();
			});
		},
	};
}

function listeningStart(
	client: dgram.Socket,
	state: DiscoveryServerState,
	settings: DiscoveryServerSettings
) {
	const { onStart = () => void 0 } = settings;

	client.on("listening", () => {
		state.running = true;
		sendCommand(client, state, commandHello(state.id, state.port));
		sendingPing(client, state);
		disconnectInstances(state, settings);
		onStart({ id: state.id, port: state.port, tfl: createTfl() });
	});
	client.on("message", (message) => {
		receiveCommand(client, state, message, settings);
	});
}

function listeningClose(
	client: dgram.Socket,
	state: DiscoveryServerState,
	settings: DiscoveryServerSettings
) {
	const { onClose = () => void 0 } = settings;

	client.on("close", () => {
		state.running = false;
		state.heartbeat.ping && clearInterval(state.heartbeat.ping);
		state.heartbeat.live && clearInterval(state.heartbeat.live);
		onClose({ id: state.id, port: state.port, tfl: createTfl() });
	});
}

function discoverPort(
	client: dgram.Socket,
	state: DiscoveryServerState,
	settings: DiscoveryServerSettings
) {
	client.on("error", (err: Error & { code: string }) => {
		//wrong code
		if (err.code !== "EADDRINUSE") {
			throwError(err, settings);
			return;
		}
		//too many tries
		if (state.port >= PORT_TO) {
			throwError(
				new Error(`Can not found available port for Servant discovery server.`),
				settings
			);
			return;
		}
		//inc port and try it again
		state.port++;
		client.bind(state.port);
		return;
	});
	client.bind(state.port);
}

function createState(): DiscoveryServerState {
	return {
		id: Math.random().toString(36).slice(2),
		port: PORT_FROM,
		running: false,
		instances: {},
		heartbeat: {
			frame: calculateFrame(),
			ping: null,
			live: null,
		},
	};
}

function calculateFrame() {
	const random = Math.random();
	return Math.round(1000 + 1000 * random);
}

function throwError(err: Error, { onError }: DiscoveryServerSettings) {
	if (onError) {
		onError(err);
		return;
	}
	//re throw
	throw err;
}

//pings

function sendingPing(client: dgram.Socket, state: DiscoveryServerState) {
	const { heartbeat } = state;
	const { frame } = heartbeat;

	heartbeat.ping = setInterval(
		() => sendCommand(client, state, commandPing(state.id, state.port)),
		frame
	);
}

//instance

function getInstance(state: DiscoveryServerState, command: Command) {
	const { from } = command;
	if (state.instances[from]) {
		return state.instances[from];
	}
	return null;
}

function createInstance(state: DiscoveryServerState, command: Command) {
	const { port, from } = command;

	const instance = {
		id: from,
		port,
		tfl: createTfl(),
	};
	state.instances[from] = instance;
	return instance;
}

function createTfl() {
	return new Date().getTime();
}

function disconnectInstances(state: DiscoveryServerState, settings: DiscoveryServerSettings) {
	const { heartbeat } = state;
	const { frame } = heartbeat;

	heartbeat.live = setInterval(() => {
		const current = createTfl();
		Object.keys(state.instances).forEach((instance) => {
			const { tfl } = state.instances[instance];
			//time from last ping call is bigger than DISCONNECT_TIMEOUT
			if (current - tfl > DISCONNECT_TIMEOUT) {
				disconnectInstance(state, instance, settings);
			}
		});
	}, frame);
}

function disconnectInstance(
	state: DiscoveryServerState,
	instance: string,
	{ onDisconnect = () => void 0 }: DiscoveryServerSettings
) {
	const { tfl, id, port } = state.instances[instance];
	delete state.instances[instance];
	onDisconnect({ id, port, tfl });
}

//commands

function sendCommand(
	client: dgram.Socket,
	state: DiscoveryServerState,
	command: Command,
	onSend?: () => void
) {
	const data = JSON.stringify(command);
	const message = Buffer.from(data);

	if (!state.running) {
		return;
	}

	let sent = 0;
	let ack = 0;

	function ackSend() {
		ack++;
		if (sent === ack && onSend) {
			onSend();
		}
	}

	for (let i = PORT_FROM; i <= PORT_TO; i++) {
		//do not send command to self
		if (i !== state.port) {
			client.send(message, 0, message.length, i, "127.0.0.1", ackSend);
			sent++;
		}
	}
}

function receiveCommand(
	client: dgram.Socket,
	state: DiscoveryServerState,
	command: Buffer,
	settings: DiscoveryServerSettings
) {
	const data = getCommandFromBuffer(command);

	//always process hello command
	processHello(client, state, data as Command, settings);
	//command received and is valid
	switch (data && data.type) {
		case CommandType.Ping:
			processPing(client, state, data as CommandPing);
			break;
		case CommandType.Data:
			processData(client, state, data as CommandData, settings);
			break;
		case CommandType.Close:
			processClose(client, state, data as CommandClose, settings);
			break;
		default:
			break;
	}
}

function getCommandFromBuffer(command: Buffer): Command | null {
	try {
		const json = JSON.parse(command.toString()) as Command;
		return json && json.type ? json : null;
	} catch (_e) {
		return null;
	}
}

//HELLO

function processHello(
	client: dgram.Socket,
	state: DiscoveryServerState,
	command: Command,
	{ onConnect = () => void 0 }: DiscoveryServerSettings
) {
	if (getInstance(state, command)) {
		return;
	}

	const { id, port } = createInstance(state, command);
	onConnect({ id, port, tfl: createTfl() });
	sendCommand(client, state, commandHello(state.id, state.port));
}

//PING

function processPing(client: dgram.Socket, state: DiscoveryServerState, command: CommandPing) {
	const instance = getInstance(state, command);
	if (instance) {
		instance.tfl = createTfl();
	}
}

//DATA

function processData(
	client: dgram.Socket,
	state: DiscoveryServerState,
	command: CommandData,
	{ onData = () => void 0 }: DiscoveryServerSettings
) {
	const instance = getInstance(state, command);
	if (instance) {
		instance.tfl = createTfl();
		//send data
		onData(instance, command.data);
	}
}

//CLOSE

function processClose(
	client: dgram.Socket,
	state: DiscoveryServerState,
	command: CommandClose,
	settings: DiscoveryServerSettings
) {
	const instance = getInstance(state, command);
	if (instance) {
		disconnectInstance(state, instance.id, settings);
	}
}
