import { Command, CommandType } from "./base";

export type CommandData = Command & {
	data: unknown;
};

export function commandData(from: string, port: number, data: unknown, to?: string[]): CommandData {
	return {
		type: CommandType.Data,
		from,
		port,
		to,
		data,
	};
}
