import { Command, CommandType } from "./base";

export type CommandClose = Command;

export function commandClose(from: string, port: number): CommandClose {
	return {
		type: CommandType.Close,
		from,
		port,
	};
}
