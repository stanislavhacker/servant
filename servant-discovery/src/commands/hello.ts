import { Command, CommandType } from "./base";

export type CommandHello = Command;

export function commandHello(from: string, port: number): CommandHello {
	return {
		type: CommandType.Hello,
		from,
		port,
	};
}
