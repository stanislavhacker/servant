import { Command, CommandType } from "./base";

export type CommandPing = Command;

export function commandPing(from: string, port: number): CommandPing {
	return {
		type: CommandType.Ping,
		from,
		port,
	};
}
