export enum CommandType {
	Hello = "SERVANT-CMD-HELLO",
	Ping = "SERVANT-CMD-PING",
	Data = "SERVANT-CMD-DATA",
	Close = "SERVANT-CMD-CLOSE",
}

export type Command = {
	from: string;
	to?: Array<string>;
	port: number;
	type: CommandType;
};
