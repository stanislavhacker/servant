import { createDiscoveryServer, DiscoveryInstance } from "../src";

describe("servant-discovery", () => {
	describe("server", () => {
		function createDiscover() {
			const onError = jasmine.createSpy("onError");
			const onConnect = jasmine.createSpy("onConnect");
			const onClose = jasmine.createSpy("onClose");
			const onDisconnect = jasmine.createSpy("onDisconnect");
			const onStart = jasmine.createSpy("onStart");
			const onData = jasmine.createSpy("onData");

			return {
				onData,
				onClose,
				onStart,
				onConnect,
				onDisconnect,
				onError,
				start: () =>
					createDiscoveryServer({
						onError,
						onConnect,
						onClose,
						onDisconnect,
						onStart,
						onData,
					}),
			};
		}

		it("check start and close of discovery", (done) => {
			const { onStart, onClose, start } = createDiscover();
			const api = start();

			onStart.and.callFake((instance: DiscoveryInstance) => {
				expect(instance.id).toBe(api.id);
				expect(instance.port).toBeGreaterThanOrEqual(5000);
				api.close();
			});
			onClose.and.callFake((instance: DiscoveryInstance) => {
				expect(instance.id).toBe(api.id);
				expect(instance.port).toBeGreaterThanOrEqual(5000);
				done();
			});
		});

		it("check connect and disconnect of discovery", (done) => {
			const discover1 = createDiscover();
			const discover2 = createDiscover();

			const api1 = discover1.start();
			const api2 = discover2.start();

			discover2.onConnect.and.callFake((instance: DiscoveryInstance) => {
				expect(instance.id).toBe(api1.id);
				expect(instance.port).toBe(api1.port());
				api1.close();
			});
			discover2.onDisconnect.and.callFake((instance: DiscoveryInstance) => {
				expect(instance.id).toBe(api1.id);
				expect(instance.port).toBe(api1.port());
				api2.close();
			});
			discover2.onClose.and.callFake(() => {
				done();
			});
		});

		it("check send data by discovery", (done) => {
			const discover1 = createDiscover();
			const discover2 = createDiscover();
			const data = { text: "Test", data: true };

			const api1 = discover1.start();
			const api2 = discover2.start();

			discover2.onConnect.and.callFake(() => {
				api1.send(data);
			});
			discover2.onDisconnect.and.callFake((instance: DiscoveryInstance) => {
				expect(instance.id).toBe(api1.id);
				expect(instance.port).toBe(api1.port());
				api2.close();
			});
			discover2.onData.and.callFake((instance: DiscoveryInstance, dt) => {
				expect(instance.id).toBe(api1.id);
				expect(instance.port).toBe(api1.port());
				expect(dt).toEqual(data);
				api1.close();
			});
			discover2.onClose.and.callFake(() => {
				done();
			});
		});
	});
});
