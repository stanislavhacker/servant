# ![Servant][logo] Servant discovery

Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

Servant also come with feature called "**_Servants link_**". If there are more Servants run in watch mode, there are
communicating each others. Servant detects others packages and can rebuild modules that are connected through dependencies.
**Let's look on some example**. You have a company monorepo "main-app" with packages. Also, there is company repo "library-app".
Some package from "main-app" has dependency to "library-app". Both repos use Servant as build tool. If you run "main-app" and
"library-app" in watch mode, Servant run from "main-app" detect that there is a "library-app" Servant running and link this into
"main-app". So now "main-app" use live build of library "library-app" and also, if you make some changes Servant first ensure
rebuild of "library-app" and then "main-app". And that's all :) You never need to create some link and another hacks manually!

This library is used for establish connection between other Servant process and sending them commands. Also contains ping and hello
runtime to discover new process or check if all connected process are still alive. Can be used separately to implement in own build
tool or just for your special purposes.

### License

[Licensed under GPLv3][license]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
[1]: https://docs.npmjs.com/files/package.json
[2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
[3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
[4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
[5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
[license]: https://gitlab.com/stanislavhacker/servant/blob/master/LICENSE
