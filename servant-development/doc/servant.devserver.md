![Servant][logo] Servant development server
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

## What is it?

Servant development server is used for fast developing web based modules in servant. You do some settings
 in `servant.json` and you can run development server. Servant will create server for each entry
 module that you specified in this setting. After that you can debug you code very simple way.
 Just open your browser with specified url for desired entry module. :wink: 
 
You can do some custom setup in templates html. More info is provided here in **[servant.json][2]** section.

Development server automatically check you changes in project and make rebuild on demand. This rebuilds are done
 on page reload. So if you do not working with build modules, no compiling is done. This is because of 
 performance problem when you develop and build in same time. Next time you will relaod page, Servant detect changes
 and make rebuild before page reloads.
 
 ![Servant development server][preview]


 [logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
 [preview]: https://gitlab.com/stanislavhacker/servant/raw/master/assets/servant.devserver.gif
 
 [1]: https://docs.npmjs.com/files/package.json
 [2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
 [3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
 [4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
 [5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md