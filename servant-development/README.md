# ![Servant][logo] Servant development

Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

Servant development is server for serving data into browser. It's provided build and rebuild of application
that is marked for serve. Development server is configured in [servant.json][2] and is using templates files
for serving and initializing app. In the exmaple below, there is part with development server configuration.
This properties are described in [servant.json][2] part of documentation.

```json
   {
   ...
   "server": {
         "entries": {
            "package-name": {
              "title": "Debug Server for package-name",
              "template": "./package-name/template.html"
            }
         },
         "css": ["http://fonts.googleapis.com/css?family=Open+Sans", "./globals/styles.css"],
         "js": ["https://code.highcharts.com/highcharts.js"]
       }
   }
```

### License

[Licensed under GPLv3][license]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
[preview]: https://gitlab.com/stanislavhacker/servant/raw/master/assets/servant.example1.gif
[init]: https://gitlab.com/stanislavhacker/servant/raw/master/assets/servant.init.gif
[1]: https://docs.npmjs.com/files/package.json
[2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
[3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
[4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
[5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
[license]: https://gitlab.com/stanislavhacker/servant/blob/master/LICENSE
