import * as api from "@servant/servant";

export interface ServantDevelopmentApi {
	server: (
		port: string | number,
		entry: string,
		log: ServantDevLogger
	) => Promise<ServantServerInfo>;
}

export interface ServantServerInfo {
	domain: string;
	init: api.InitData | null;
	client: {
		[key: string]: {
			port: number;
		};
	};
	server: {
		[key: string]: {
			command: string;
		};
	};
	shared: {
		links: ServantServerShared[];
	};
}

export type ServantServerShared = {
	id: string;
	fromModule: api.Module.ModuleDefinition;
	from: string;
	toModule: api.Module.ModuleDefinition;
	to: string;
};

export type ServantDevLog =
	| ServantServerDevLog
	| ServantClientDevLog
	| ServantChangesDevLog
	| ServantSharedDevLog;

export type ServantDefaultDevLog = {
	date: Date;
	time: [number, number];
	error?: Error;
};

export type ServantServerDevLog = ServantDefaultDevLog & {
	type: "server";
	//server
	module: string;
	status: "waiting" | "running" | "restarting" | "stopped" | "compiling";
	stdoutCount?: number;
	stderrCount?: number;
	pid?: number;
};

export function isServantServerDevLog(log: ServantDevLog): log is ServantServerDevLog {
	return log && log.type === "server";
}

export type ServantClientDevLog = ServantDefaultDevLog & {
	type: "client";
	module: string;
	status: "listening" | "serving" | "stopped";
	url?: string;
	method?: string;
};

export function isServantClientDevLog(log: ServantDevLog): log is ServantClientDevLog {
	return log && log.type === "client";
}

export type ServantChangesDevLog = ServantDefaultDevLog & {
	type: "changes";
	modules: Array<string>;
	files: Array<[string, -1 | 0 | 1, number]>;
	results: Array<api.Module.BuildResult>;
	errors: Record<string, Array<Error>>;
};

export function isServantChangesDevLog(log: ServantDevLog): log is ServantChangesDevLog {
	return log && log.type === "changes";
}

export type ServantSharedDevLog = ServantDefaultDevLog & {
	status: "created" | "failed";
	type: "shared";
	id: string;
};

export function isServantSharedDevLog(log: ServantDevLog): log is ServantSharedDevLog {
	return log && log.type === "shared";
}

export type ServantServerState = {
	domain: string;
	init: api.InitData | null;
	info: api.Watcher.WatcherInfo | null;
	change: api.Watcher.WatcherChange | null;
	changes: Record<string, Date>;
	client: ServantServerInfo["client"];
	server: ServantServerInfo["server"];
	shared: ServantServerInfo["shared"];
};

export type ServantDevLogger = (log: ServantDevLog) => void;

export function createState(domain: string): ServantServerState {
	return {
		domain,
		init: null,
		change: null,
		info: null,
		changes: {},
		client: {},
		server: {},
		shared: {
			links: [],
		},
	};
}

export function createInfo(state: ServantServerState): ServantServerInfo {
	return {
		server: state.server,
		client: state.client,
		shared: state.shared,
		init: state.init,
		domain: state.domain,
	};
}
