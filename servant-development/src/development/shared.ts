import * as api from "@servant/servant";
import { EntriesOnly } from "@servant/servant";
import * as path from "path";
import * as fs from "fs";

import { ServantServerInfo, ServantServerState } from "../index";
import { ServantServerShared } from "../types";
import { ServantDevEmitter, ServantSharedDevEmitter } from "../events";
import { invariant } from "../invariant";

export function createShared(
	state: ServantServerState,
	emitter: ServantDevEmitter,
	{ client, server }: { client: EntriesOnly; server: EntriesOnly }
): Promise<ServantServerInfo["shared"]> {
	const data: ServantServerInfo["shared"] = { links: [] };

	const s = new Promise((resolve, reject) => {
		const { info } = state;
		const sharedEmitter = emitter.shared();

		invariant(
			state.init,
			"Invalid init data provided. Passed wrong parameters or is probably error in Servant."
		);
		invariant(
			info && info.modulesData && info.modulesData.graph,
			"Invalid watcher info data provided. Passed wrong parameters or is probably error in Servant."
		);

		const initData = state.init;
		const [clientLinks, clientErrors] = collectLinks(
			initData.entry,
			info.modulesData.graph,
			client
		);
		const [serverLinks, serverErrors] = collectLinks(
			initData.entry,
			info.modulesData.graph,
			server
		);
		const errors = [...clientErrors, ...serverErrors];
		const links = [...clientLinks, ...serverLinks];

		//report first error, Servant will fail
		if (errors.length > 0) {
			reject(errors[0]);
			return;
		}

		//save links
		data.links = links;

		createLinks(sharedEmitter, links).then(resolve);
	});
	return s.catch((error) => emitter.emit("reject", error)).then(() => data);
}

function collectLinks(
	entry: string,
	graph: api.Module.DependenciesGraph,
	entries: EntriesOnly
): [ServantServerShared[], Error[]] {
	const links: ServantServerShared[] = [];
	const errors: Error[] = [];

	Object.keys(entries.entries).forEach((key) => {
		const oneEntry = entries.entries[key];
		const shared = oneEntry.shared || {};

		Object.keys(shared).forEach((pth) => {
			const { to, toModule } = retrieveInput(entry, graph, key, pth);
			const { from, fromModule } = retrieveOutput(graph, shared[pth]);

			//error state
			if (!from || !module || !to || !toModule) {
				errors.push(
					new Error(
						`Can not create shared folder "${to}" for module "${shared[pth]}". Is this module exists?`
					)
				);
				return;
			}

			links.push({
				id: Math.random().toString(36).slice(2),
				to,
				toModule,
				from,
				fromModule,
			});
		});
	});

	return [links, errors];
}

function retrieveOutput(graph: api.Module.DependenciesGraph, module = "") {
	const definition = graph.modules[module];

	if (definition && definition.module?.internal && definition.module?.servantJson) {
		const servantJson = definition.module.servantJson;
		return {
			from: path.join(servantJson.cwd, servantJson.content.output.directory),
			fromModule: definition,
		};
	}
	return {
		from: undefined,
		fromModule: undefined,
	};
}

function retrieveInput(
	entry: string,
	graph: api.Module.DependenciesGraph,
	module: string,
	to: string
) {
	const definition = graph.modules[module];

	if (definition && definition.module?.internal) {
		return {
			to: path.join(entry, to),
			toModule: definition,
		};
	}
	return {
		to: undefined,
		toModule: undefined,
	};
}

function createLinks(emitter: ServantSharedDevEmitter, links: ServantServerShared[]) {
	const linksPromises = links.map((link) =>
		createLink(link)
			.then(() => emitter.emit("log", { status: "created", id: link.id }))
			.catch((error) => emitter.emit("log", { status: "failed", id: link.id, error }))
	);
	return Promise.all(linksPromises);
}

function createLink(link: ServantServerShared) {
	return remove(link.to)
		.then(() => dir(link.from))
		.then(() => symlink(link.from, link.to));
}

function remove(dir: string): Promise<void> {
	return new Promise((resolve) => {
		fs.rmdir(dir, { recursive: true }, () => resolve());
	});
}

function dir(dir: string): Promise<void> {
	return new Promise((resolve) => {
		fs.mkdir(dir, { recursive: true }, () => resolve());
	});
}

function symlink(from: string, to: string): Promise<void> {
	return new Promise((resolve, reject) => {
		fs.symlink(from, to, "dir", (err) => {
			err ? reject(err) : resolve();
		});
	});
}
