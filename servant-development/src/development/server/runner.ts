import * as api from "@servant/servant";
import { EntriesOnly } from "@servant/servant";
import { ChildProcess, fork } from "child_process";
import * as path from "path";
import * as os from "os";

import { ServantDevEmitter, ServantServerDevEmitter } from "../../events";
import { ServantServerState } from "../../index";
import { invariant } from "../../invariant";

type RunnerState = {
	module: string;
	restarting: boolean;
	stdout: string[];
	stderr: string[];
	process: ChildProcess | null;
	changed: Date;
	checker: number;
};

export interface DevelopmentRunnerInfo {
	command: string;
}

export function createDevelopmentRunner(
	state: ServantServerState,
	emitter: ServantDevEmitter,
	module: string,
	entry: api.ServantJson.ServerEntry
): Promise<DevelopmentRunnerInfo> {
	const runnerState = createRunnerState(module);

	return new Promise((resolve) => {
		const moduleInfo = state.info?.modulesData?.graph?.modules[module];
		const init = state.init;

		invariant(
			init,
			"Invalid init data provided. Passed wrong parameters or is probably error in Servant."
		);
		invariant(
			moduleInfo,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);
		invariant(
			moduleInfo.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const serverEmitter = emitter.server(module);
		const cwd = moduleInfo.module.servantJson.cwd;
		const command = api.ServantJson.getServerEntryCommand(
			init.entry,
			moduleInfo.module.servantJson,
			entry.command
		);

		createChecker(state, runnerState, serverEmitter, cwd, command);
		resolve(createProcess(state, runnerState, serverEmitter, cwd, command));
	});
}

function createProcess(
	state: ServantServerState,
	runnerState: RunnerState,
	emitter: ServantServerDevEmitter,
	cwd: string,
	command: NonNullable<EntriesOnly["entries"][string]["command"]>
) {
	const cmd = fork(command.file || "", command.args ?? [], {
		cwd,
		stdio: "pipe",
	});

	runnerState.process = cmd;
	runnerState.changed = new Date();

	//data from child
	cmd.stdout?.on("data", (data) => {
		runnerState.stdout.push(...parseProcessData(data));
		emitter.emit("log", {
			status: "running",
			stderrCount: runnerState.stderr.length,
			stdoutCount: runnerState.stdout.length,
			pid: cmd.pid,
		});
	});
	cmd.stderr?.on("data", (data) => {
		runnerState.stderr.push(...parseProcessData(data));
		emitter.emit("log", {
			status: "running",
			stderrCount: runnerState.stderr.length,
			stdoutCount: runnerState.stdout.length,
			pid: cmd.pid,
		});
	});

	cmd.on("spawn", () => {
		runnerState.restarting = false;
		emitter.emit("log", {
			status: "running",
			stderrCount: runnerState.stderr.length,
			stdoutCount: runnerState.stdout.length,
			pid: cmd.pid,
		});
	});
	cmd.on("error", (error) => {
		emitter.emit("log", {
			status: "stopped",
			stderrCount: runnerState.stderr.length,
			stdoutCount: runnerState.stdout.length,
			pid: cmd.pid,
			error,
		});
	});
	cmd.on("exit", () => {
		//restart service or mark as stopped
		if (runnerState.restarting) {
			runnerState.restarting = false;
			createProcess(state, runnerState, emitter, cwd, command);
		} else {
			emitter.emit("log", {
				status: "stopped",
				stderrCount: runnerState.stderr.length,
				stdoutCount: runnerState.stdout.length,
			});
			runnerState.process = null;
		}
		//reset
		runnerState.stdout = [];
		runnerState.stderr = [];
	});

	emitter.emit("log", {
		status: "waiting",
		stderrCount: runnerState.stderr.length,
		stdoutCount: runnerState.stdout.length,
	});

	return {
		command: `node "${path.relative(cwd, command.file || "")}" ${command.args?.join(" ")}`,
	};
}

function createChecker(
	state: ServantServerState,
	runnerState: RunnerState,
	emitter: ServantServerDevEmitter,
	cwd: string,
	command: NonNullable<EntriesOnly["entries"][string]["command"]>
) {
	runnerState.checker = setInterval(() => {
		const last = state.changes[runnerState.module];

		//running instance is up-to-date or not in changed module
		if (!last || last.getTime() < runnerState.changed.getTime()) {
			return;
		}

		if (runnerState.process) {
			//need restart
			runnerState.changed = new Date();
			runnerState.restarting = true;
			emitter.emit("log", {
				status: "restarting",
				stderrCount: runnerState.stderr.length,
				stdoutCount: runnerState.stdout.length,
			});
			runnerState.process.kill();
		} else {
			//need start again
			createProcess(state, runnerState, emitter, cwd, command);
		}
	}, 30) as unknown as number;
}

function createRunnerState(module: string): RunnerState {
	return {
		module,
		restarting: false,
		stdout: [],
		stderr: [],
		process: null,
		changed: new Date(),
		checker: -1,
	};
}

function parseProcessData(data: string): string[] {
	return data
		.toString()
		.split(new RegExp(`${os.EOL}|\n`, "gi"))
		.filter(Boolean);
}
