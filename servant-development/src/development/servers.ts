import { EntriesOnly } from "@servant/servant";

import { ServantServerInfo, ServantServerState } from "../index";
import { ServantDevEmitter } from "../events";
import { invariant } from "../invariant";

import { createDevelopmentRunner, DevelopmentRunnerInfo } from "./server/runner";

export function runServers(
	state: ServantServerState,
	emitter: ServantDevEmitter,
	server: EntriesOnly
): Promise<ServantServerInfo["server"]> {
	const data: ServantServerInfo["server"] = {};
	const entries = Object.keys(server.entries);

	invariant(
		state.info?.modulesData?.graph,
		"No module declaration found. Invalid data provided or is probably error in Servant."
	);

	const s = entries.reduce((s, module) => {
		s = s.then(() => {
			return createDevelopmentRunner(state, emitter, module, server.entries[module]);
		});
		return s.then((serverInfo: DevelopmentRunnerInfo) => {
			if (serverInfo) {
				data[module] = {
					command: serverInfo.command,
				};
			}
		});
	}, Promise.resolve<unknown>(null));
	return s.catch((error) => emitter.emit("reject", error)).then(() => data);
}
