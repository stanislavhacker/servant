import { Path, Extensions, HTML } from "@servant/servant-files";
import * as api from "@servant/servant";
import * as http from "http";
import * as url from "url";
import * as path from "path";
import * as fs from "fs";

import { createTemplate, TemplateResults, createError, ErrorData, FAVICON } from "../../public";
import { ServantClientDevEmitter, ServantDevEmitter } from "../../events";
import { ServantServerState } from "../../index";
import { loadPatterns } from "../../utils";
import { invariant } from "../../invariant";

import { compileWait } from "../changes/watcher";

const indexName = Extensions.replace("index", HTML.MAIN);

export function listener(
	state: ServantServerState,
	emitter: ServantDevEmitter,
	module: string,
	entry: api.ServantJson.ServerEntry
): http.RequestListener {
	return (req: http.IncomingMessage, res: http.ServerResponse) => {
		const method = req.method || "GET";
		const url = req.url || "";
		const clientEmitter = emitter.client(module, method, url);

		invariant(
			state.init,
			"Invalid init data provided. Passed wrong parameters or is probably error in Servant."
		);
		invariant(
			state.info?.modulesData,
			"Invalid modules graph provided, can not iterate Servant modules."
		);

		const initData = state.init;
		const modulesData = state.info.modulesData;

		clientEmitter.emit("log", { status: "serving" });

		let p = Promise.resolve(null) as Promise<unknown>;
		p = p.then(() => compileWait(state));
		p = p.then((info) => info || getSpecialInfo(initData.entry, url));
		p = p.then((info) => info || getFilenameInfo(initData.entry, url));
		p = p.then((info) => info || getResourceInfo(initData.entry, modulesData, url));
		p = p.then(
			serveFileByFileInfo(clientEmitter, res, url, initData, modulesData, module, entry)
		);
		p = p.then(sendFileByFileInfo(res));
		p.then(() => clientEmitter.emit("log", { status: "listening" })).catch((error) =>
			clientEmitter.emit("log", { status: "listening", error })
		);
	};
}

//states

function error(
	module: string,
	serverEntry: api.ServantJson.ServerEntry,
	status: 500 | 404,
	res: http.ServerResponse,
	data: Omit<ErrorData, "status">
) {
	res.statusCode = status;
	res.end(createError(module, serverEntry, { ...data, status }));
}

//Generate file

type File = {
	contentType: string;
	content: Buffer;
};

function getGeneratedFile(
	initData: api.InitData,
	modulesData: api.ModulesData,
	module: string,
	serverEntry: api.ServantJson.ServerEntry
): Promise<File> {
	return new Promise((resolve, reject) => {
		invariant(
			initData.servantJson,
			"Invalid init data provided. Passed wrong parameters or is probably error in Servant."
		);

		const servantJson = initData.servantJson;
		createTemplate(servantJson, modulesData, module, serverEntry)
			.then((template: TemplateResults) => {
				if (!template) {
					reject();
					return;
				}

				resolve({
					content: Buffer.from(template.template),
					contentType: Extensions.getMimeType(HTML.MAIN),
				});
			})
			.catch(reject);
	});
}

function getFavicon(): Promise<File> {
	return Promise.resolve({
		contentType: "image/vnd.microsoft.icon",
		content: FAVICON,
	});
}

function getFile(url: string, info: FileInfo | null | undefined): Promise<File> {
	if (!info) {
		return Promise.reject(new Error(`Can not load file on url '${url}'.`));
	}
	return new Promise((resolve, reject) => {
		fs.readFile(info.path, (err, data) => {
			if (err) {
				reject(err);
				return;
			}
			resolve({
				content: data,
				contentType: Extensions.getMimeType(info.extension),
			});
		});
	});
}

//Determine file info

type FileInfo = {
	path: string;
	basename: string;
	extension: string;
	type: "RESOURCE" | "INDEX" | "FAVICON";
};

function createFileInfo(pathname: string, providedType: FileInfo["type"]): FileInfo {
	const parsed = path.parse(pathname);
	const ext = Extensions.create(parsed.ext || HTML.MAIN);
	const basename = path.basename(pathname);
	const type = indexName === basename ? "INDEX" : providedType;

	return {
		extension: ext,
		basename: basename,
		path: pathname,
		type,
	};
}

function serveFileByFileInfo(
	emitter: ServantClientDevEmitter,
	res: http.ServerResponse,
	url: string,
	initData: api.InitData,
	modulesData: api.ModulesData,
	module: string,
	serverEntry: api.ServantJson.ServerEntry
) {
	return (info: FileInfo | null) => {
		switch (info && info.type) {
			case "INDEX":
				return getGeneratedFile(initData, modulesData, module, serverEntry).catch((err) => {
					error(module, serverEntry, 500, res, {
						error: err,
						entry: initData.entry,
						path: info?.path,
						type: "MISSING_TEMPLATE",
					});
					throw err;
				});
			case "FAVICON":
				return getFavicon();
			case "RESOURCE":
			default:
				return getFile(url, info).catch((err) => {
					error(module, serverEntry, 404, res, {
						error: err,
						entry: initData.entry,
						path: info?.path,
						type: "NOT_FOUND",
					});
					throw err;
				});
		}
	};
}

function sendFileByFileInfo(res: http.ServerResponse) {
	return (file: File) => {
		if (file) {
			res.setHeader("Content-type", file.contentType);
			res.end(file.content);
		}
		return Promise.resolve();
	};
}

function getFilenameInfo(entry: string, urlPath: string): Promise<FileInfo | null> {
	return new Promise((resolve) => {
		const parsedUrl = url.parse(urlPath);
		let pathname = path.join(entry, `.${parsedUrl.pathname}`);

		fs.stat(pathname, (err, stats) => {
			if (err) {
				resolve(null);
				return;
			}

			if (stats && stats.isDirectory()) {
				pathname = path.join(pathname, indexName);
			}

			resolve(createFileInfo(pathname, "RESOURCE"));
		});
	});
}

function getSpecialInfo(entry: string, urlPath: string): Promise<FileInfo | null> {
	return new Promise((resolve) => {
		const parsedUrl = url.parse(urlPath);
		const pathname = path.join(entry, `.${parsedUrl.pathname}`);

		if (pathname.includes("favicon.ico")) {
			resolve(createFileInfo(pathname, "FAVICON"));
			return;
		}

		resolve(null);
	});
}

function getResourceInfo(
	entry: string,
	modulesData: api.ModulesData,
	urlPath: string
): Promise<FileInfo | null> {
	return new Promise((resolve) => {
		const parsedUrl = url.parse(urlPath);
		const filename = parsedUrl.pathname || "";

		const resources = loadResourcesPatterns(entry, modulesData);
		loadPatterns(entry, resources).then((files: Array<string>) => {
			const found = files.filter((file) => {
				return file.indexOf(filename) >= 0;
			});

			if (found.length === 0) {
				resolve(null);
				return;
			}

			resolve(createFileInfo(found[0], "RESOURCE"));
		});
	});
}

function loadResourcesPatterns(entry: string, modulesData: api.ModulesData): Array<string> {
	const resources: Array<string> = [];
	const graph = modulesData.graph;

	if (graph) {
		graph.all
			.map((name) => {
				return graph.modules[name];
			})
			.filter((module) => {
				return module.module && module.module.servantJson.content.resources.length > 0;
			})
			.forEach((module) => {
				if (module.module) {
					const servantJson = module.module.servantJson;
					const res = servantJson.content.resources;
					res.forEach((resource) => {
						resources.push(
							Path.normalize(
								path.join(path.relative(entry, servantJson.cwd), resource)
							)
						);
					});
				}
			});
	}

	return resources;
}
