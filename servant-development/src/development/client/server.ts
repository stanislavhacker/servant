import * as http from "http";

import { invariant } from "../../invariant";
import { ServantServerState } from "../../index";
import { ServantDevEmitter, ServantClientDevEmitter } from "../../events/index";

const tries = 100;
const defaultPort = 9000;

export interface DevelopmentServerInfo {
	port: number;
	server: http.Server;
}

export function createDevelopmentServer(
	state: ServantServerState,
	emitter: ServantDevEmitter,
	module: string,
	port: string | number,
	listener: http.RequestListener
): Promise<DevelopmentServerInfo> {
	const currentPort = parseInt(port.toString()) || defaultPort;

	return new Promise((resolve) => {
		const ports: Array<number> = [];
		const clientEmitter = emitter.client(module);

		for (let i = currentPort; i <= currentPort + tries; i++) {
			ports.push(i);
		}

		findFirst(ports, (port) => createServer(state, clientEmitter, port, module, listener))
			.then((info) => resolve(info as DevelopmentServerInfo))
			.catch(() => {
				clientEmitter.emit(
					"reject",
					new Error(
						`Can not create dev server on port ${currentPort}. Not founded free port between ${port} - ${
							currentPort + tries
						}.`
					)
				);
			});
	});
}

function createServer(
	state: ServantServerState,
	emitter: ServantClientDevEmitter,
	port: number,
	module: string,
	listener: http.RequestListener
): Promise<DevelopmentServerInfo> {
	return new Promise((resolve, reject) => {
		const moduleInfo = state.info?.modulesData?.graph?.modules[module];

		invariant(
			moduleInfo,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const info = {} as DevelopmentServerInfo;
		const server = http.createServer((reg, res) => listener(reg, res)).listen(port);

		info.port = port;
		info.server = server;

		server.on("listening", () => {
			emitter.emit("log", { status: "listening" });
			resolve(info);
		});
		server.on("error", (error) => {
			emitter.emit("log", { status: "stopped", error });
			reject(error);
		});
	});
}

function findFirst<T, R>(values: Array<T>, fn: (port: T) => Promise<R>) {
	return new Promise((resolve, reject) => {
		if (values.length === 0) {
			reject();
			return;
		}

		fn(values[0])
			.then((val) => {
				resolve(val);
			})
			.catch(() => {
				values.shift();
				findFirst(values, fn).then(resolve).catch(reject);
			});
	});
}
