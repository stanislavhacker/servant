import { EntriesOnly } from "@servant/servant";

import { ServantServerInfo, ServantServerState } from "../index";
import { ServantDevEmitter } from "../events";

import { createDevelopmentServer, DevelopmentServerInfo } from "./client/server";
import { listener } from "./client/listener";

export function runClients(
	state: ServantServerState,
	emitter: ServantDevEmitter,
	client: EntriesOnly,
	settings: {
		port: string | number;
	}
): Promise<ServantServerInfo["client"]> {
	const data: ServantServerInfo["client"] = {};
	const entries = Object.keys(client.entries);
	const { port } = settings;

	const s = entries.reduce((s, module) => {
		s = s.then(() => {
			const listenerHandler = listener(state, emitter, module, client.entries[module]);
			return createDevelopmentServer(state, emitter, module, port, listenerHandler);
		});
		return s.then((serverInfo: DevelopmentServerInfo) => {
			if (serverInfo) {
				data[module] = {
					port: serverInfo.port,
				};
			}
		});
	}, Promise.resolve<unknown>(null));
	return s.catch((error) => emitter.emit("reject", error)).then(() => data);
}
