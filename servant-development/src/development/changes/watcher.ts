import * as api from "@servant/servant";

import { ServantServerState } from "../../index";
import { ServantDevEmitter } from "../../events";

type DevelopmentWatcherState = {
	progress: boolean;
};

export function watcher(
	state: ServantServerState,
	emitter: ServantDevEmitter,
	initData: api.InitData
): Promise<void> {
	const watcherState = createWatcherState();

	return new Promise((resolve) => {
		const changesEmitter = emitter.changes();
		api.watch(
			initData,
			(info: api.Watcher.WatcherInfo, change: api.Watcher.WatcherChange) => {
				const errors = mapErrors(change.results);
				const error = (errors[Object.keys(errors)[0]] || [])[0] || undefined;
				//update state
				state.change = change;
				state.info = info;
				//log
				changesEmitter.emit("log", {
					modules: [...change.modules],
					files: [...change.files],
					results: [...change.results],
					error,
					errors,
				});
				//update changed
				updateChanged(state, watcherState, change);
				//resolve on first time
				resolveOnce(resolve);
			},
			{ transpile: false }
		).catch((error) => changesEmitter.emit("reject", error));
	});
}

export function compileWait(state: ServantServerState): Promise<void> {
	return new Promise((resolve) => {
		//no progress, resolve immediately
		if (!state.change?.progress) {
			resolve();
			return;
		}
		//wait for done
		const waiter = setInterval(() => {
			//still in progress
			if (state.change?.progress) {
				return;
			}
			//done
			clearInterval(waiter);
			resolve();
		}, 30);
	});
}

interface ResolveCallback {
	(): void;
	resolved?: boolean;
}
function resolveOnce(resolve: ResolveCallback) {
	if (resolve.resolved) {
		return;
	}
	resolve.resolved = true;
	resolve();
}

function updateChanged(
	state: ServantServerState,
	watcherState: DevelopmentWatcherState,
	change: api.Watcher.WatcherChange
) {
	if (watcherState.progress && !change.progress) {
		//update build time
		state.changes = change.modules.reduce(
			(prev, module) => ({ ...prev, [module]: new Date() }),
			state.changes
		);
		watcherState.progress = false;
	}
	if (!watcherState.progress && change.progress) {
		watcherState.progress = true;
	}
}

function createWatcherState(): DevelopmentWatcherState {
	return {
		progress: false,
	};
}

function mapErrors(results: Array<api.Module.BuildResult>): Record<string, Array<Error>> {
	return results.reduce((prev, { css, sass, less, typescript, module }) => {
		prev[module] = [
			...(css?.errors ?? []),
			...(sass?.errors.map((item) => item.error) ?? []),
			...(less?.errors.map((item) => item.error) ?? []),
			...(typescript?.errors ?? []),
		].filter(Boolean) as Array<Error>;
		return prev;
	}, {} as Record<string, Array<Error>>);
}
