import * as path from "path";
import * as fs from "fs";
import * as xml from "xml2js";
import * as api from "@servant/servant";

import { FilesData, getFilesData } from "../../utils";

import { createIndexHtml } from "./index.html";

export interface TemplateResults {
	files: FilesData;
	template: string;
}

export function createTemplate(
	servantJson: api.ServantJson.ServantJsonInfo,
	modulesData: api.ModulesData,
	module: string,
	serverEntry: api.ServantJson.ServerEntry
): Promise<TemplateResults> {
	return new Promise((resolve, reject) => {
		const all: Array<Promise<unknown>> = [];

		all.push(getFilesData(servantJson, modulesData, module));
		all.push(loadTemplate(servantJson, serverEntry));

		Promise.all(all)
			.then((data: [FilesData, TemplateData]) => {
				const [filesData, template] = data;

				resolve({
					files: filesData,
					template: createIndexHtml(module, serverEntry, template, filesData),
				});
			})
			.catch(reject);
	});
}

export interface TemplateData {
	meta: Array<string>;
	styles: Array<string>;
	scripts: Array<string>;
	body: Array<string>;
}

function loadTemplate(
	servantJson: api.ServantJson.ServantJsonInfo,
	serverEntry: api.ServantJson.ServerEntry
): Promise<TemplateData> {
	return new Promise((resolve, reject) => {
		const templateData = {} as TemplateData;
		templateData.styles = [];
		templateData.scripts = [];
		templateData.meta = [];
		templateData.body = [];

		if (!serverEntry.template) {
			resolve(templateData);
			return;
		}

		const filename = path.join(servantJson.cwd, serverEntry.template);

		fs.readFile(filename, (err, data) => {
			if (err) {
				reject(err);
				return;
			}

			xml.parseString(
				data.toString(),
				{
					preserveChildrenOrder: true,
					explicitChildren: true,
				},
				(err, result) => {
					if (err) {
						reject(err);
						return;
					}

					try {
						const children = result.template || {};

						if (children.meta) {
							const metas = children.meta || [];
							metas.forEach((meta) => {
								if (typeof meta === "string") {
									templateData.meta.push(meta.trim());
								} else {
									templateData.meta.push(build(meta, "meta"));
								}
							});
						}
						if (children.body) {
							const bodys = children.body || [];
							bodys.forEach((body) => {
								if (typeof body === "string") {
									templateData.body.push(body.trim());
								} else {
									(body.$$ || []).forEach((item) => {
										templateData.body.push(build(item));
									});
								}
							});
						}
						if (children.script) {
							const scripts = children.script || [];
							scripts.forEach((script) => {
								templateData.scripts.push(script._ || "");
							});
						}
						if (children.style) {
							const styles = children.style || [];
							styles.forEach((style) => {
								templateData.styles.push(style._ || "");
							});
						}

						resolve(templateData);
					} catch (e) {
						reject(e);
					}
				}
			);
		});
	});
}

function build(item: Record<string, string>, name?: string): string {
	const builder = new xml.Builder({ headless: true });
	const cname = (item["#name"] || name) as string;
	delete item["#name"];
	return builder.buildObject({ [cname]: item });
}
