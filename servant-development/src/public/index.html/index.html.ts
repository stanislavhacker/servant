import * as api from "@servant/servant";

import { FilesData } from "../../utils/files";
import { TemplateData } from "./index";

export function createIndexHtml(
	module: string,
	serverEntry: api.ServantJson.ServerEntry,
	template: TemplateData,
	filesData: FilesData
): string {
	return `
		<!DOCTYPE html>
		<html lang="en">
			<head>
				<meta charset="UTF-8">
				${template.meta.join("")}
				<title>${serverEntry.title || module} - Servant Debug Server</title>
				${filesData.css
					.map((css) => {
						return link(css);
					})
					.join("")}
				${template.styles
					.map((content) => {
						return `<style>${content}</style>`;
					})
					.join("")}
			</head>
			<body>
			    ${template.body.join("")}
				${Object.keys(filesData.libraries)
					.map((lib) => {
						return script(filesData.libraries[lib]);
					})
					.join("")}
				${filesData.js
					.map((js) => {
						return script(js);
					})
					.join("")}
				${template.scripts
					.map((content) => {
						return `<script type="text/javascript">${content}</script>`;
					})
					.join("")}
			</body>
		</html>
`;
}

function link(url: string): string {
	return `<link rel="stylesheet" type="text/css" href="${url}" />`;
}

function script(url: string) {
	return `<script src="${url}" type="text/javascript"></script>`;
}
