export type NamedError = "MISSING_TEMPLATE" | "NOT_FOUND";

export const ERRORS: Record<NamedError, { title: string; description: string }> = {
	MISSING_TEMPLATE: {
		title: "Missing template file",
		description: `There are no template file in the project. You need to create template html file that
                      will have name defined in <code>servant.json</code> "server" property. More info 
                      on <a target="_blank" href="https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md#server-object">servant.json documentation</a>.`,
	},
	NOT_FOUND: {
		title: "File not found",
		description: `The file you are looking for does not exist. Check if the file exists and if it is
                      in the correct location. If you are looking for resources, check if you have
                      specified this resource in the <code>servant.json</code>. More info
                      on <a target="_blank" href="https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md">servant.json documentation</a>.`,
	},
};
