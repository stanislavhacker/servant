import * as api from "@servant/servant";
import * as pth from "path";

import { ErrorData, NamedError } from "./index";
import { ERRORS } from "./errors";

//language=css
const Styles = `
    body {
        margin: 0;
        padding: 0;
        color: white;
        background: #282C34;
        font-family: "Roboto", sans-serif;
    }

    body .container {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        display: flex;
        flex-direction: row;
        align-content: center;
        justify-content: center;
        align-items: center;
    }

    body .container .content {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 50%;
        min-width: 400px;
    }

    body .container .content .status {
        font-size: 60px;
        font-weight: bold;
        margin: 10px 0;
        color: #ff0000;
    }

    body .container .content .file {
        font-size: 16px;
        width: 100%;
        margin: 25px 0 0 0;
    }

    body .container .content code.error {
        font-size: 16px;
        font-family: monospace;
        margin: 25px 0 0 0;
        padding: 25px;
        color: black;
        background: darkgrey;
        text-align: center;
    }

    body .container .content .info {
        width: 100%;
        margin: 25px 0 0 0;
    }

    body .container .content .info .title {
        font-size: 16px;
        font-weight: bold;
    }

    body .container .content .info .description {
        font-size: 15px;
        margin: 10px 0 0 0;
    }
    
    a {
        color: #61DBFA;
    }
`;

export function createErrorHtml(
	module: string,
	serverEntry: api.ServantJson.ServerEntry,
	data: ErrorData
): string {
	const { error, status, entry, path, type } = data;

	const fileTemplate = createFileTemplate(entry, path);
	const typeTemplate = createNamedErrorTemplate(type);

	return `
		<!DOCTYPE html>
		<html lang="en">
			<head>
				<meta charset="UTF-8">
				<title>${serverEntry.title || module} - Error occured</title>
				<style>${Styles}</style>
			</head>
			<body>
				<div class="container">
					<div class="content">
						<div class="status">${status}</div>
						${typeTemplate}
						${fileTemplate}
						<code class="error">${error.toString()}</code>
					</div>
				</div>
			</body>
		</html>
`;
}

function createFileTemplate(entry?: string, path?: string) {
	return entry && path
		? `
		<div class="file">
			<b>Related file:</b> <code>${pth.relative(entry, path)}</code>
		</div>`
		: "";
}

function createNamedErrorTemplate(type?: NamedError) {
	const err = type && ERRORS[type];

	return err
		? `
		<div class="info">
			<div class="title">${err.title}</div>
			<div class="description">${err.description}</div>
		</div>`
		: "";
}
