import * as api from "@servant/servant";

import { createErrorHtml } from "./error.html";
import { NamedError } from "./errors";

export { NamedError };

export type ErrorData = {
	error: Error;
	status: number;
	type?: NamedError;
	entry?: string;
	path?: string;
};

export function createError(
	module: string,
	serverEntry: api.ServantJson.ServerEntry,
	data: ErrorData
) {
	return createErrorHtml(module, serverEntry, data);
}
