const ServantDataErrors = [
	"No module declaration found. Invalid data provided or is probably error in Servant.",
	"Invalid init data provided. Passed wrong parameters or is probably error in Servant.",
	"Invalid watcher info data provided. Passed wrong parameters or is probably error in Servant.",
	"Invalid modules graph provided, can not iterate Servant modules.",
] as const;

export function invariant(
	// eslint-disable-next-line @typescript-eslint/ban-types
	condition: boolean | string | null | undefined | number | Object,
	message: (typeof ServantDataErrors)[number]
): asserts condition {
	if (!condition) {
		throw new Error(message);
	}
}
