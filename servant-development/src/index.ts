import * as api from "@servant/servant";

import { watcher, runClients, runServers, createShared } from "./development";
import { invariant } from "./invariant";
import { createEmitter } from "./events";
import {
	createInfo,
	createState,
	isServantServerDevLog,
	isServantClientDevLog,
	isServantChangesDevLog,
	isServantSharedDevLog,
	ServantChangesDevLog,
	ServantClientDevLog,
	ServantDevelopmentApi,
	ServantDevLog,
	ServantDevLogger,
	ServantServerDevLog,
	ServantServerInfo,
	ServantServerState,
	ServantSharedDevLog,
	ServantServerShared,
} from "./types";

export {
	ServantDevelopmentApi,
	ServantServerInfo,
	ServantDevLog,
	ServantServerDevLog,
	ServantClientDevLog,
	ServantChangesDevLog,
	ServantSharedDevLog,
	ServantServerShared,
	ServantServerState,
	ServantDevLogger,
	isServantServerDevLog,
	isServantClientDevLog,
	isServantChangesDevLog,
	isServantSharedDevLog,
};

export function ServantDevelopment(): ServantDevelopmentApi {
	return {
		server: (port, entry, log) => server(port, entry, log),
	};
}

function server(
	port: string | number,
	entry: string,
	log: ServantDevLogger
): Promise<ServantServerInfo> {
	return new Promise((resolve, reject) => {
		const emitter = createEmitter(log, reject);
		const state = createState("localhost");

		let p: Promise<unknown> = api.init(entry).catch(reject);
		p = p.then((initData: api.InitData) => {
			state.init = initData;
			return watcher(state, emitter, initData);
		});
		p = p.then(() => {
			const { init, info } = state;

			invariant(
				init,
				"Invalid init data provided. Passed wrong parameters or is probably error in Servant."
			);
			invariant(
				init.servantJson,
				"Invalid init data provided. Passed wrong parameters or is probably error in Servant."
			);

			invariant(
				info,
				"Invalid watcher info data provided. Passed wrong parameters or is probably error in Servant."
			);
			invariant(
				info.modulesData,
				"Invalid watcher info data provided. Passed wrong parameters or is probably error in Servant."
			);

			const { client, server } = api.resolveEntries(init, info.modulesData);

			//shared
			const sharedPromise = createShared(state, emitter, { client, server }).then(
				(shared) => {
					state.shared = shared;
				}
			);

			//clients
			const clientsPromise = runClients(state, emitter, client, { port }).then((client) => {
				state.client = client;
			});

			//servers
			const serversPromise = runServers(state, emitter, server).then((server) => {
				state.server = server;
			});

			return sharedPromise
				.then(() => Promise.all([clientsPromise, serversPromise]))
				.catch(reject);
		});
		p.then(() => resolve(createInfo(state)));
	});
}
