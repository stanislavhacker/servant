import {
	ServantClientDevLog,
	ServantServerDevLog,
	ServantChangesDevLog,
	ServantSharedDevLog,
} from "../types";

export function createClientLog(
	module: string,
	status: ServantClientDevLog["status"],
	start: [number, number],
	props: Pick<ServantClientDevLog, "method" | "url" | "error">
): ServantClientDevLog {
	return {
		type: "client",
		date: new Date(),
		time: process.hrtime(start),
		status,
		module,
		...props,
	};
}

export function createServerLog(
	module: string,
	status: ServantServerDevLog["status"],
	start: [number, number],
	props: Pick<ServantServerDevLog, "stderrCount" | "stdoutCount" | "pid" | "error">
): ServantServerDevLog {
	return {
		type: "server",
		date: new Date(),
		time: process.hrtime(start),
		status,
		module,
		...props,
	};
}

export function createChangesLog(
	start: [number, number],
	props: Pick<ServantChangesDevLog, "modules" | "files" | "results" | "error" | "errors">
): ServantChangesDevLog {
	return {
		type: "changes",
		date: new Date(),
		time: process.hrtime(start),
		...props,
	};
}

export function createSharedLog(
	start: [number, number],
	status: ServantSharedDevLog["status"],
	props: Pick<ServantSharedDevLog, "id" | "error">
): ServantSharedDevLog {
	return {
		type: "shared",
		date: new Date(),
		time: process.hrtime(start),
		status,
		...props,
	};
}
