import * as events from "events";

import {
	ServantClientDevLog,
	ServantChangesDevLog,
	ServantServerDevLog,
	ServantDevLogger,
	ServantSharedDevLog,
	ServantDevLog,
} from "../types";
import { createChangesLog, createClientLog, createServerLog, createSharedLog } from "./log";

interface ServantDevEmitterEvents {
	reject: (err: Error) => void;
	"log-raw": (log: ServantDevLog) => void;
}
export interface ServantDevEmitter extends events.EventEmitter {
	on<U extends keyof ServantDevEmitterEvents>(
		event: U,
		listener: ServantDevEmitterEvents[U]
	): this;
	emit<U extends keyof ServantDevEmitterEvents>(
		event: U,
		...args: Parameters<ServantDevEmitterEvents[U]>
	): boolean;
	//helpers
	client(module: string, method?: string, url?: string): ServantClientDevEmitter;
	server(module: string): ServantServerDevEmitter;
	shared(): ServantSharedDevEmitter;
	changes(): ServantChangesDevEmitter;
}

interface ServantClientDevEmitterEvents extends ServantDevEmitterEvents {
	log: (log: Pick<ServantClientDevLog, "status" | "error">) => void;
}
export interface ServantClientDevEmitter extends events.EventEmitter {
	on<U extends keyof ServantClientDevEmitterEvents>(
		event: U,
		listener: ServantClientDevEmitterEvents[U]
	): this;
	emit<U extends keyof ServantClientDevEmitterEvents>(
		event: U,
		...args: Parameters<ServantClientDevEmitterEvents[U]>
	): boolean;
}

interface ServantServerDevEmitterEvents extends ServantDevEmitterEvents {
	log: (
		log: Pick<ServantServerDevLog, "status" | "stderrCount" | "stdoutCount" | "pid" | "error">
	) => void;
}
export interface ServantServerDevEmitter extends events.EventEmitter {
	on<U extends keyof ServantServerDevEmitterEvents>(
		event: U,
		listener: ServantServerDevEmitterEvents[U]
	): this;
	emit<U extends keyof ServantServerDevEmitterEvents>(
		event: U,
		...args: Parameters<ServantServerDevEmitterEvents[U]>
	): boolean;
}

interface ServantChangesDevEmitterEvents extends ServantDevEmitterEvents {
	log: (
		log: Pick<ServantChangesDevLog, "results" | "files" | "modules" | "error" | "errors">
	) => void;
}
export interface ServantChangesDevEmitter extends events.EventEmitter {
	on<U extends keyof ServantChangesDevEmitterEvents>(
		event: U,
		listener: ServantChangesDevEmitterEvents[U]
	): this;
	emit<U extends keyof ServantChangesDevEmitterEvents>(
		event: U,
		...args: Parameters<ServantChangesDevEmitterEvents[U]>
	): boolean;
}

interface ServantSharedDevEmitterEvents extends ServantDevEmitterEvents {
	log: (log: Pick<ServantSharedDevLog, "status" | "id" | "error">) => void;
}
export interface ServantSharedDevEmitter extends events.EventEmitter {
	on<U extends keyof ServantSharedDevEmitterEvents>(
		event: U,
		listener: ServantSharedDevEmitterEvents[U]
	): this;
	emit<U extends keyof ServantSharedDevEmitterEvents>(
		event: U,
		...args: Parameters<ServantSharedDevEmitterEvents[U]>
	): boolean;
}

export function createEmitter(
	log: ServantDevLogger,
	reject: (reason: Error) => void
): ServantDevEmitter {
	const emitter = createEmitterBase<ServantDevEmitter>(reject);

	emitter.client = (module: string, method?: string, url?: string) => {
		const start = process.hrtime();
		const client = createEmitterBase<ServantClientDevEmitter>(reject);

		client.on("log", ({ status, error }) => {
			emitter.emit("log-raw", createClientLog(module, status, start, { method, url, error }));
		});
		client.on("log-raw", (...args) => emitter.emit("log-raw", ...args));

		return client;
	};

	emitter.server = (module: string) => {
		const start = process.hrtime();
		const client = createEmitterBase<ServantServerDevEmitter>(reject);

		client.on("log", ({ status, error, pid, stdoutCount, stderrCount }) => {
			emitter.emit(
				"log-raw",
				createServerLog(module, status, start, { error, pid, stdoutCount, stderrCount })
			);
		});
		client.on("log-raw", (...args) => emitter.emit("log-raw", ...args));

		return client;
	};

	emitter.shared = () => {
		const start = process.hrtime();
		const shared = createEmitterBase<ServantSharedDevEmitter>(reject);

		shared.on("log", ({ status, id, error }) => {
			emitter.emit("log-raw", createSharedLog(start, status, { id, error }));
		});
		shared.on("log-raw", (...args) => emitter.emit("log-raw", ...args));

		return shared;
	};

	emitter.changes = () => {
		const start = process.hrtime();
		const client = createEmitterBase<ServantChangesDevEmitter>(reject);

		client.on("log", ({ files, modules, results, error, errors }) => {
			emitter.emit(
				"log-raw",
				createChangesLog(start, { error, files, modules, results, errors })
			);
		});
		client.on("log-raw", (...args) => emitter.emit("log-raw", ...args));

		return client;
	};

	emitter.on("log-raw", (data) => log(data));

	return emitter;
}

function createEmitterBase<T extends events.EventEmitter>(reject: (reason: Error) => void): T {
	const emitter = new events.EventEmitter();

	emitter.on("reject", (reason) => reject(reason));

	return emitter as T;
}
