import * as api from "@servant/servant";
import { Path, CSS, JS } from "@servant/servant-files";
import * as path from "path";
import * as url from "url";

import { invariant } from "../invariant";

import { loadPatterns } from "./patterns";

export type FilesData = {
	errors: {
		[key: string]: Error;
	};
	libraries: {
		[key: string]: string;
	};
	js: Array<string>;
	css: Array<string>;
};

export function getFilesData(
	servantJson: api.ServantJson.ServantJsonInfo,
	modulesData: api.ModulesData,
	module: string
): Promise<FilesData> {
	return getFiles(servantJson, modulesData.graph?.modules[module] ?? null, createFiles());
}

function getFiles(
	servantJson: api.ServantJson.ServantJsonInfo,
	module: api.Module.ModuleDefinition | null,
	files: FilesData
): Promise<FilesData> {
	return new Promise((resolve) => {
		invariant(
			module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const moduleServantJson = module.module.servantJson;
		let p: Promise<unknown> = Promise.resolve();

		p = p.then(() => loadLibraries(servantJson, module, files));
		p = p.then(() => loadExternals(servantJson, servantJson, files));
		p = p.then(() => loadExternals(servantJson, moduleServantJson, files));
		p = p.then(() =>
			loadPatterns(
				moduleServantJson.cwd,
				[moduleServantJson.content.output?.directory ?? ""],
				"**/*"
			)
		);
		p.then((fls: Array<string>) => {
			const distFiles = createFiles();

			distFiles.js = JS.only(fls);
			distFiles.css = CSS.only(fls);

			distFiles.js = distFiles.js.map(normalize(servantJson.cwd));
			distFiles.css = distFiles.css.map(normalize(servantJson.cwd));

			//save files
			files.css = [...new Set([...distFiles.css, ...files.css])];
			files.js = [...new Set([...distFiles.js, ...files.js])];

			const all: Array<Promise<unknown>> = [];

			module.internals.forEach((int) => {
				all.push(getFiles(servantJson, int, files));
			});

			Promise.all(all).then(() => resolve(files));
		});
	});
}

function loadLibraries(
	servantJson: api.ServantJson.ServantJsonInfo,
	module: api.Module.ModuleDefinition,
	files: FilesData
): Promise<void> {
	return new Promise((resolve) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const info = module.module;
		info.dependencies.forEach((dep) => {
			const name = dep.content.name;
			const lib = api.loadLibrary(info.servantJson, name);

			if (lib && !lib.err && path.extname(lib.path) && !files.libraries[name]) {
				files.libraries[name] = normalize(servantJson.cwd)(lib.path);
			}
			if ((!lib || lib.err) && !files.errors[name]) {
				files.errors[name] = new Error(`Can not find source for module ${name}.`);
			}
		});
		resolve();
	});
}

function loadExternals(
	servantJson: api.ServantJson.ServantJsonInfo,
	moduleServantJson: api.ServantJson.ServantJsonInfo,
	files: FilesData
): Promise<void> {
	return new Promise((resolve) => {
		const css = moduleServantJson.content.server.css;
		const js = moduleServantJson.content.server.js;

		css.forEach((item) => {
			const urlData = url.parse(item);

			if (urlData.protocol) {
				files.css.push(item);
			} else {
				files.css.push(normalize(servantJson.cwd)(path.join(moduleServantJson.cwd, item)));
			}
		});
		js.forEach((item) => {
			const urlData = url.parse(item);

			if (urlData.protocol) {
				files.js.push(item);
			} else {
				files.js.push(normalize(servantJson.cwd)(path.join(moduleServantJson.cwd, item)));
			}
		});

		files.css = [...new Set(files.css)];
		files.js = [...new Set(files.js)];

		resolve();
	});
}

function normalize(entry: string): (item: string) => string {
	return (item: string) => {
		return "/" + Path.normalize(path.relative(entry, item));
	};
}

function createFiles(): FilesData {
	return {
		errors: {},
		js: [],
		css: [],
		libraries: {},
	};
}
