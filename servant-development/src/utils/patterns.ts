import { Path } from "@servant/servant-files";

import * as path from "path";
import * as glob from "glob";

export function loadPatterns(
	entry: string,
	relatives: Array<string>,
	what?: string
): Promise<Array<string>> {
	return new Promise((fulfill) => {
		const all: Array<Promise<Array<string>>> = [];
		const files: Array<string> = [];

		relatives.forEach((module) => {
			const pattern = Path.normalize(path.join(entry, module, what || ""));

			all.push(
				new Promise((fulfill) => {
					//find all
					glob(pattern, (err, files) => fulfill(files));
				})
			);
		});

		Promise.all(all).then((data) => {
			data.forEach((fls) => {
				files.push(...fls);
			});

			fulfill([...new Set(files)]);
		});
	});
}
