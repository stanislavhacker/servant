export interface TestsResults {
	summary: {
		time: [number, number];
		testsCount: number;
		completedCount: number;
		failedCount: number;
		excludedCount: number;
		pendingCount: number;
		passed: boolean;
		excluded: boolean;
		pending: boolean;
		done: boolean;
		random: {
			state: boolean;
			seed: number | string;
		};
	};
	results: {
		[key: string]: TestsSpec | TestsSuit;
	};
	failed: Array<TestsSpec | TestsSuit>;
	excluded: Array<TestsSpec | TestsSuit>;
	pending: Array<TestsSpec | TestsSuit>;
}

export enum TestsItemType {
	Suit,
	Spec,
}
export enum TestsSpecStatus {
	Failed = "failed",
	Passed = "passed",
	Excluded = "excluded",
	Pending = "pending",
}
export interface TestsSuit {
	name: string;
	fullName: string;
	type: TestsItemType;
	suits: {
		[key: string]: TestsSuit | TestsSpec;
	};
	failed: Array<SpecExpectation>;
	duration: null | number;
	done: boolean;
	status: TestsSpecStatus;
}
export interface TestsSpec {
	name: string;
	fullName: string;
	type: TestsItemType;
	status: TestsSpecStatus;
	pending: string | null;
	passed: Array<SpecExpectation>;
	failed: Array<SpecExpectation>;
	done: boolean;
}
export interface SpecExpectation {
	actual?: unknown;
	expected?: unknown;
	matcherName: string;
	message: string;
	passed: boolean;
	stack: string;
}

export function init(cwd: string, pushUrl: string): jasmine.CustomReporter {
	const results = createTestsResults();
	const state = createState(results);

	return createReporter(cwd, results, (results: TestsResults) => {
		state.results = results;
		state.changed = true;
		push(pushUrl, state);
	});
}

export function createTestsResults(): TestsResults {
	return {
		summary: {
			testsCount: 0,
			completedCount: 0,
			failedCount: 0,
			excludedCount: 0,
			pendingCount: 0,
			time: [0, 0],
			passed: true,
			excluded: false,
			pending: false,
			done: false,
			random: {
				state: false,
				seed: "none",
			},
		},
		results: {},
		failed: [],
		excluded: [],
		pending: [],
	};
}

function push(pushUrl: string, state: ReporterState) {
	//running request or not changed
	if (state.http || !state.changed) {
		return;
	}

	const http = (state.http = new XMLHttpRequest());
	state.changed = false;

	http.onreadystatechange = () => {
		if (http.readyState === 4) {
			state.http = null;
			push(pushUrl, state);
		}
	};
	http.open("POST", `${pushUrl}`, true);
	http.send(JSON.stringify(state.results));
}

//STATE

type ReporterState = {
	http: XMLHttpRequest | null;
	changed: boolean;
	results: TestsResults;
};

function createState(results: TestsResults): ReporterState {
	return {
		http: null,
		changed: true,
		results: results,
	};
}

//REPORTERS

function createReporter(
	cwd: string,
	results: TestsResults,
	progress: (results: TestsResults) => void
): jasmine.CustomReporter {
	const dirs: Array<TestsResults["results"]> = [results.results];
	const suits: { [key: string]: TestsSuit } = {};
	const specs: { [key: string]: TestsSpec } = {};

	return {
		jasmineStarted: (suiteInfo) => {
			results.summary.testsCount = suiteInfo.totalSpecsDefined;
			progress(results);
		},

		suiteStarted: (result) => {
			const suit = createTestsSuit(result.fullName, result.description);

			suits[result.id] = suit;
			dirs[0][result.id] = suit;
			dirs.unshift(suit.suits);
			progress(results);
		},

		specStarted: (result) => {
			const spec = createTestsSpec(result.fullName, result.description);

			dirs[0][result.id] = spec;
			specs[result.id] = spec;
			progress(results);
		},

		specDone: (result) => {
			const spec = specs[result.id] as TestsSpec;

			spec.failed = normalizeSpecs(result.failedExpectations);
			spec.passed = normalizeSpecs(result.passedExpectations);
			spec.pending = result.pendingReason;
			spec.status = result.status as TestsSpecStatus;
			spec.done = true;
			//normalize
			//save global
			results.summary.completedCount++;
			if (spec.status === TestsSpecStatus.Failed) {
				results.summary.failedCount++;
				results.failed.push(spec);
			}
			if (spec.status === TestsSpecStatus.Pending) {
				results.summary.pendingCount++;
				results.pending.push(spec);
			}
			if (spec.status === TestsSpecStatus.Excluded) {
				results.summary.excludedCount++;
				results.excluded.push(spec);
			}
			progress(results);
		},

		suiteDone: (result) => {
			const suit = suits[result.id] as TestsSuit;

			dirs.shift();
			suit.failed = normalizeSpecs(result.failedExpectations);
			suit.duration = result.duration;
			suit.status = result.status as TestsSpecStatus;
			suit.done = true;
			//save global
			if (suit.status === TestsSpecStatus.Failed) {
				results.failed.push(suit);
			}
			if (suit.status === TestsSpecStatus.Pending) {
				results.pending.push(suit);
			}
			if (suit.status === TestsSpecStatus.Excluded) {
				results.excluded.push(suit);
			}
			progress(results);
		},

		jasmineDone: (result) => {
			results.summary.done = true;
			results.summary.random = {
				seed: result.order.seed,
				state: result.order.random,
			};
			results.summary.excluded = results.excluded.length > 0;
			results.summary.pending = results.pending.length > 0;
			results.summary.passed = results.failed.length === 0;
			progress(results);
		},
	};
}

function createTestsSuit(fullName: string, name: string): TestsSuit {
	return {
		name: name,
		fullName: fullName,
		type: TestsItemType.Suit,
		suits: {},
		done: false,
		failed: [],
		duration: null,
		status: TestsSpecStatus.Failed,
	};
}

function createTestsSpec(fullName: string, name: string): TestsSpec {
	return {
		name: name,
		fullName: fullName,
		type: TestsItemType.Spec,
		failed: [],
		passed: [],
		pending: null,
		status: TestsSpecStatus.Failed,
		done: false,
	};
}

function normalizeSpecs(specs: Array<SpecExpectation>): Array<SpecExpectation> {
	return specs.map((spec) => {
		return normalizeStackPath(spec);
	});
}

function normalizeStackPath(expect: SpecExpectation): SpecExpectation {
	if (expect.expected) {
		expect.expected = removeCircular(expect.expected);
	}
	if (expect.actual) {
		expect.actual = removeCircular(expect.actual);
	}
	return expect;
}

function removeCircular(obj: object): object {
	const stackSet = new Set();

	function repair(obj: object) {
		//no object
		if (obj && typeof obj !== "object") {
			return obj;
		}
		//detected
		if (stackSet.has(obj)) {
			return "<circular-reference>";
		}

		stackSet.add(obj);

		const newObj = {};
		for (const k in obj) {
			if (Object.prototype.hasOwnProperty.call(obj, k)) {
				newObj[k] = repair(obj[k]);
			}
		}

		stackSet.delete(obj);
		return newObj;
	}

	return repair(obj);
}
