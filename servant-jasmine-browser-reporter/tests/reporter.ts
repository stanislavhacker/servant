import { init, TestsResults, TestsItemType, TestsSpecStatus } from "../src/index";

describe("servant-jasmine-browser-reporter", () => {
	const url = "/push/rand/";
	let reporter;

	beforeEach(function () {
		jasmine.Ajax.install();
	});

	beforeEach(() => {
		reporter = init("C:/tests/test", url);
	});

	afterEach(function () {
		jasmine.Ajax.uninstall();
	});

	it("init create reporter", () => {
		expect(reporter.jasmineStarted).toBeDefined();
		expect(reporter.suiteStarted).toBeDefined();
		expect(reporter.specStarted).toBeDefined();
		expect(reporter.specDone).toBeDefined();
		expect(reporter.suiteDone).toBeDefined();
		expect(reporter.jasmineDone).toBeDefined();
	});

	it("call jasmine started", () => {
		reporter.jasmineStarted({
			totalSpecsDefined: 10,
		});

		const request = jasmine.Ajax.requests.mostRecent();
		expect(request.url).toBe(url);
		expect(JSON.parse(request.params)).toEqual({
			excluded: [],
			failed: [],
			pending: [],
			results: {},
			summary: {
				completedCount: 0,
				done: false,
				excluded: false,
				excludedCount: 0,
				failedCount: 0,
				passed: true,
				pending: false,
				pendingCount: 0,
				testsCount: 10,
				time: [0, 0],
				random: {
					state: false,
					seed: "none",
				},
			},
		});
	});

	it("call suit started", () => {
		reporter.suiteStarted({
			fullName: "desc suit 1",
			description: "suit 1",
			id: "suit1",
		});

		const request = jasmine.Ajax.requests.mostRecent();
		expect(request.url).toBe(url);
		expect(JSON.parse(request.params) as TestsResults).toEqual({
			excluded: [],
			failed: [],
			pending: [],
			results: {
				suit1: {
					name: "suit 1",
					fullName: "desc suit 1",
					type: TestsItemType.Suit,
					suits: {},
					done: false,
					failed: [],
					duration: null,
					status: TestsSpecStatus.Failed,
				},
			},
			summary: {
				completedCount: 0,
				done: false,
				excluded: false,
				excludedCount: 0,
				failedCount: 0,
				passed: true,
				pending: false,
				pendingCount: 0,
				testsCount: 0,
				time: [0, 0],
				random: {
					state: false,
					seed: "none",
				},
			},
		});
	});

	it("call spec started", () => {
		reporter.specStarted({
			fullName: "desc suit 1 spec 1",
			description: "spec 1",
			id: "spec1",
		});

		const request = jasmine.Ajax.requests.mostRecent();
		expect(request.url).toBe(url);
		expect(JSON.parse(request.params) as TestsResults).toEqual({
			excluded: [],
			failed: [],
			pending: [],
			results: {
				spec1: {
					name: "spec 1",
					fullName: "desc suit 1 spec 1",
					type: TestsItemType.Spec,
					failed: [],
					passed: [],
					pending: null,
					status: TestsSpecStatus.Failed,
					done: false,
				},
			},
			summary: {
				completedCount: 0,
				done: false,
				excluded: false,
				excludedCount: 0,
				failedCount: 0,
				passed: true,
				pending: false,
				pendingCount: 0,
				testsCount: 0,
				time: [0, 0],
				random: {
					state: false,
					seed: "none",
				},
			},
		});
	});

	describe("call spec done", () => {
		it("ok", () => {
			reporter.specStarted({
				fullName: "desc suit 1 spec 1",
				description: "spec 1",
				id: "spec1",
			});

			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});

			reporter.specDone({
				id: "spec1",
				failedExpectations: [],
				passedExpectations: [
					{
						actual: "true",
						expected: "true",
						matcherName: "matcher name",
						message: "This is message",
						passed: true,
						stack: "",
					},
				],
				pendingReason: null,
				status: TestsSpecStatus.Passed,
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);
			expect(JSON.parse(request.params) as TestsResults).toEqual({
				excluded: [],
				failed: [],
				pending: [],
				results: {
					spec1: {
						name: "spec 1",
						fullName: "desc suit 1 spec 1",
						type: TestsItemType.Spec,
						failed: [],
						passed: [
							{
								actual: "true",
								expected: "true",
								matcherName: "matcher name",
								message: "This is message",
								passed: true,
								stack: "",
							},
						],
						pending: null,
						status: TestsSpecStatus.Passed,
						done: true,
					},
				},
				summary: {
					completedCount: 1,
					done: false,
					excluded: false,
					excludedCount: 0,
					failedCount: 0,
					passed: true,
					pending: false,
					pendingCount: 0,
					testsCount: 0,
					time: [0, 0],
					random: {
						state: false,
						seed: "none",
					},
				},
			});
		});

		it("fail", () => {
			reporter.specStarted({
				fullName: "desc suit 1 spec 1",
				description: "spec 1",
				id: "spec1",
			});

			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});

			reporter.specDone({
				id: "spec1",
				failedExpectations: [
					{
						actual: "false",
						expected: "true",
						matcherName: "matcher name",
						message: "This is message",
						passed: false,
						stack: "",
					},
				],
				passedExpectations: [],
				pendingReason: null,
				status: TestsSpecStatus.Failed,
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);

			const spec = {
				name: "spec 1",
				fullName: "desc suit 1 spec 1",
				type: TestsItemType.Spec,
				failed: [
					{
						actual: "false",
						expected: "true",
						matcherName: "matcher name",
						message: "This is message",
						passed: false,
						stack: "",
					},
				],
				passed: [],
				pending: null,
				status: TestsSpecStatus.Failed,
				done: true,
			};

			expect(JSON.parse(request.params) as TestsResults).toEqual({
				excluded: [],
				failed: [spec],
				pending: [],
				results: {
					spec1: spec,
				},
				summary: {
					completedCount: 1,
					done: false,
					excluded: false,
					excludedCount: 0,
					failedCount: 1,
					passed: true,
					pending: false,
					pendingCount: 0,
					testsCount: 0,
					time: [0, 0],
					random: {
						state: false,
						seed: "none",
					},
				},
			});
		});

		it("pending", () => {
			reporter.specStarted({
				fullName: "desc suit 1 spec 1",
				description: "spec 1",
				id: "spec1",
			});

			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});

			reporter.specDone({
				id: "spec1",
				failedExpectations: [],
				passedExpectations: [],
				pendingReason: "This is pending",
				status: TestsSpecStatus.Pending,
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);

			const spec = {
				name: "spec 1",
				fullName: "desc suit 1 spec 1",
				type: TestsItemType.Spec,
				failed: [],
				passed: [],
				pending: "This is pending",
				status: TestsSpecStatus.Pending,
				done: true,
			};

			expect(JSON.parse(request.params) as TestsResults).toEqual({
				excluded: [],
				failed: [],
				pending: [spec],
				results: {
					spec1: spec,
				},
				summary: {
					completedCount: 1,
					done: false,
					excluded: false,
					excludedCount: 0,
					failedCount: 0,
					passed: true,
					pending: false,
					pendingCount: 1,
					testsCount: 0,
					time: [0, 0],
					random: {
						state: false,
						seed: "none",
					},
				},
			});
		});

		it("excluded", () => {
			reporter.specStarted({
				fullName: "desc suit 1 spec 1",
				description: "spec 1",
				id: "spec1",
			});

			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});

			reporter.specDone({
				id: "spec1",
				failedExpectations: [],
				passedExpectations: [],
				pendingReason: null,
				status: TestsSpecStatus.Excluded,
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);

			const spec = {
				name: "spec 1",
				fullName: "desc suit 1 spec 1",
				type: TestsItemType.Spec,
				failed: [],
				passed: [],
				pending: null,
				status: TestsSpecStatus.Excluded,
				done: true,
			};

			expect(JSON.parse(request.params) as TestsResults).toEqual({
				excluded: [spec],
				failed: [],
				pending: [],
				results: {
					spec1: spec,
				},
				summary: {
					completedCount: 1,
					done: false,
					excluded: false,
					excludedCount: 1,
					failedCount: 0,
					passed: true,
					pending: false,
					pendingCount: 0,
					testsCount: 0,
					time: [0, 0],
					random: {
						state: false,
						seed: "none",
					},
				},
			});
		});
	});

	describe("call suit done", () => {
		it("ok", () => {
			reporter.suiteStarted({
				fullName: "desc suit 1",
				description: "suit 1",
				id: "suit1",
			});

			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});

			reporter.suiteDone({
				id: "suit1",
				failedExpectations: [],
				duration: 100,
				status: TestsSpecStatus.Passed,
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);
			expect(JSON.parse(request.params) as TestsResults).toEqual({
				excluded: [],
				failed: [],
				pending: [],
				results: {
					suit1: {
						name: "suit 1",
						fullName: "desc suit 1",
						type: TestsItemType.Suit,
						suits: {},
						done: true,
						failed: [],
						duration: 100,
						status: TestsSpecStatus.Passed,
					},
				},
				summary: {
					completedCount: 0,
					done: false,
					excluded: false,
					excludedCount: 0,
					failedCount: 0,
					passed: true,
					pending: false,
					pendingCount: 0,
					testsCount: 0,
					time: [0, 0],
					random: {
						state: false,
						seed: "none",
					},
				},
			});
		});

		it("fail", () => {
			reporter.suiteStarted({
				fullName: "desc suit 1",
				description: "suit 1",
				id: "suit1",
			});

			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});

			reporter.suiteDone({
				id: "suit1",
				failedExpectations: [
					{
						actual: "false",
						expected: "true",
						matcherName: "matcher name",
						message: "This is message",
						passed: false,
						stack: "",
					},
				],
				duration: 100,
				status: TestsSpecStatus.Failed,
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);

			const suit = {
				name: "suit 1",
				fullName: "desc suit 1",
				type: TestsItemType.Suit,
				suits: {},
				done: true,
				failed: [
					{
						actual: "false",
						expected: "true",
						matcherName: "matcher name",
						message: "This is message",
						passed: false,
						stack: "",
					},
				],
				duration: 100,
				status: TestsSpecStatus.Failed,
			};

			expect(JSON.parse(request.params) as TestsResults).toEqual({
				excluded: [],
				failed: [suit],
				pending: [],
				results: {
					suit1: suit,
				},
				summary: {
					completedCount: 0,
					done: false,
					excluded: false,
					excludedCount: 0,
					failedCount: 0,
					passed: true,
					pending: false,
					pendingCount: 0,
					testsCount: 0,
					time: [0, 0],
					random: {
						state: false,
						seed: "none",
					},
				},
			});
		});
	});

	describe("call jasmine done", () => {
		function prepare(spec: unknown) {
			reporter.suiteStarted({
				fullName: "desc suit 1",
				description: "suit 1",
				id: "suit1",
			});
			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});
			reporter.specStarted({
				fullName: "desc suit 1 spec 1",
				description: "spec 1",
				id: "spec1",
			});
			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});
			reporter.specDone(spec);
			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});
			reporter.suiteDone({
				id: "suit1",
				failedExpectations: [],
				duration: 100,
				status: TestsSpecStatus.Passed,
			});
			jasmine.Ajax.requests.mostRecent().respondWith({
				status: 200,
			});
		}

		it("ok", () => {
			prepare({
				id: "spec1",
				failedExpectations: [],
				passedExpectations: [
					{
						actual: "true",
						expected: "true",
						matcherName: "matcher name",
						message: "This is message",
						passed: true,
						stack: "",
					},
				],
				pendingReason: null,
				status: TestsSpecStatus.Passed,
			});

			reporter.jasmineDone({
				order: { seed: 666, random: true },
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);
			expect((JSON.parse(request.params) as TestsResults).summary).toEqual({
				completedCount: 1,
				done: true,
				excluded: false,
				excludedCount: 0,
				failedCount: 0,
				passed: true,
				pending: false,
				pendingCount: 0,
				testsCount: 0,
				time: [0, 0],
				random: {
					state: true,
					seed: 666,
				},
			});
		});

		it("failed", () => {
			prepare({
				id: "spec1",
				failedExpectations: [
					{
						actual: "false",
						expected: "true",
						matcherName: "matcher name",
						message: "This is message",
						passed: false,
						stack: "",
					},
				],
				passedExpectations: [],
				pendingReason: null,
				status: TestsSpecStatus.Failed,
			});

			reporter.jasmineDone({
				order: { seed: 666, random: true },
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);
			expect((JSON.parse(request.params) as TestsResults).summary).toEqual({
				completedCount: 1,
				done: true,
				excluded: false,
				excludedCount: 0,
				failedCount: 1,
				passed: false,
				pending: false,
				pendingCount: 0,
				testsCount: 0,
				time: [0, 0],
				random: {
					state: true,
					seed: 666,
				},
			});
		});

		it("excluded", () => {
			prepare({
				id: "spec1",
				failedExpectations: [],
				passedExpectations: [],
				pendingReason: null,
				status: TestsSpecStatus.Excluded,
			});

			reporter.jasmineDone({
				order: { seed: 666, random: true },
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);
			expect((JSON.parse(request.params) as TestsResults).summary).toEqual({
				completedCount: 1,
				done: true,
				excluded: true,
				excludedCount: 1,
				failedCount: 0,
				passed: true,
				pending: false,
				pendingCount: 0,
				testsCount: 0,
				time: [0, 0],
				random: {
					state: true,
					seed: 666,
				},
			});
		});

		it("pending", () => {
			prepare({
				id: "spec1",
				failedExpectations: [],
				passedExpectations: [],
				pendingReason: "Must be done",
				status: TestsSpecStatus.Pending,
			});

			reporter.jasmineDone({
				order: { seed: 666, random: true },
			});

			const request = jasmine.Ajax.requests.mostRecent();
			expect(request.url).toBe(url);
			expect((JSON.parse(request.params) as TestsResults).summary).toEqual({
				completedCount: 1,
				done: true,
				excluded: false,
				excludedCount: 0,
				failedCount: 0,
				passed: true,
				pending: true,
				pendingCount: 1,
				testsCount: 0,
				time: [0, 0],
				random: {
					state: true,
					seed: 666,
				},
			});
		});
	});
});
