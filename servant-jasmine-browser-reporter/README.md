# ![Servant][logo] Servant jasmine browser reporter

Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

Servant jasmine reporter that is used for reporting results from runner in browser into
servant test server. This module can be used separately but it's not supported and interface
can be changed in future!

### License

[Licensed under GPLv3][license]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
[1]: https://docs.npmjs.com/files/package.json
[2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
[3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
[4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
[5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
[license]: https://gitlab.com/stanislavhacker/servant/blob/master/LICENSE
