import * as pw from "playwright";
import { webkit } from "./browsers/webkit";
import { firefox } from "./browsers/firefox";
import { chromium } from "./browsers/chromium";

export { pw, webkit, firefox, chromium };

export type BrowserInfo = {
	name: string;
	rand: string;
};

export function createBrowserInfo(name: string, rand: string): BrowserInfo {
	return {
		name: name,
		rand: rand,
	};
}

export function createUrl(port: number, rand: string): string {
	return `http://localhost:${port}/${rand}/`;
}

export function createName(browser: string, device?: string | null): string {
	if (device) {
		return `${browser} on ${device}`;
	}
	return `${browser} on Desktop`;
}
