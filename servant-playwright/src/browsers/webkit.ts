import * as pw from "playwright";
import { createBrowserInfo, BrowserInfo, createUrl, createName } from "../index";

export function webkit(
	port: number,
	launched: (info: BrowserInfo) => void,
	start: (browser: pw.Browser, info: BrowserInfo) => void,
	exit: (info: BrowserInfo) => void,
	device: string | null,
	gui: boolean
): Promise<void> {
	return new Promise((resolve, reject) => {
		const rand = Math.random().toString(36).slice(2);
		const browserInfo = createBrowserInfo(createName("Webkit", device), rand);
		const dev = device ? pw.devices[device] : null;

		//no device
		if (device && !dev) {
			resolve();
			return;
		}

		//launched
		launched(browserInfo);

		//launch
		pw.webkit
			.launch({
				headless: !gui,
				args: getArgs(gui),
			})
			.then((browser) => {
				//context create
				let p: Promise<unknown> = browser.newContext(
					dev
						? {
								viewport: dev.viewport,
								userAgent: dev.userAgent,
						  }
						: {}
				);
				//page create
				p = p.then((context: pw.BrowserContext) => context.newPage()).catch(reject);
				//navigate to page
				p = p.then((page: pw.Page) => page.goto(createUrl(port, rand))).catch(reject);
				//start
				p.then(() => {
					start(browser, browserInfo);
				}).catch(reject);
			})
			.catch(reject);
	});
}

function getArgs(gui: boolean) {
	const flags = ["--no-default-browser-check", "--process-per-tab", "--new-window"];

	if (gui) {
		//in case of gui, add automatic open dev tool, debugging
		flags.push("--auto-open-devtools-for-tabs");
		flags.push("--start-maximized");
	} else {
		flags.push("--headless");
		flags.push("--no-sandbox");
	}

	return flags;
}
