# ![Servant][logo] Servant playwright

Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

Servant playwright is module that use [playwright][playwright] module from [Microsoft][ms] to running tests against more
browsers. Noy you can run test on these browsers: **Chromium**, **Webkit** and **Firefox**. This list can change as playwright is
developing so actually supported browser you can find on [playwright][playwright] page. [Playwright][playwright] also support
emulating devices. Actually list of devices can be found [on device list][devices].

Module is only wrapper and provide mapping [playwright][playwright] API on Servant api.

> **Caveats**
>
> This module is optional for Servant, and it's not installed when installing Servant
> because of **size** of browsers core. To use more complex testing run `npm install @servant/servant-playwright --save-dev`
> in root project (when servant is installed).

### Examples of usage

```typescript
import { webkit } from "@servant/servant-playwright";

webkit(port, onLaunch, onStart, onEnd, device, gui);
```

```typescript
import { firefox } from "@servant/servant-playwright";

firefox(port, onLaunch, onStart, onEnd, device, gui);
```

```typescript
import { chromium } from "@servant/servant-playwright";

chromium(port, onLaunch, onStart, onEnd, device, gui);
```

### License

[Licensed under MIT][license]

[Playwright][playwright] is licensed under [Apache-2.0][license2]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
[playwright]: https://www.npmjs.com/package/playwright
[ms]: https://github.com/microsoft
[1]: https://docs.npmjs.com/files/package.json
[2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
[3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
[4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
[5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
[license]: https://gitlab.com/stanislavhacker/servant-playwright/blob/master/LICENSE
[license2]: https://github.com/microsoft/playwright/blob/master/LICENSE
[devices]: https://github.com/Microsoft/playwright/blob/master/src/deviceDescriptors.ts
