![Servant][logo] Servant Docker

This is docker image for testing nodejs and web based project with Servant. In this docker there are all necessary dependencies to run servant with browser and node js testing. Image contains **node 16.15.0** with **the latest Chrome** browser. Of course, you can use own image that contains node and chrome. If you want to run test with **playwright** (using [`--browser`][tests-cli] flag or same settings in [`servant.json`][browsers]) you can skip installing Chrome, because servant do it for you.

#### Docker url

```
https://hub.docker.com/r/hackerstanislav/servant
```

### Docker build

Simple steps how to build docket image and push the latest into remote docker hub repository.

```
 docker build -t hackerstanislav/servant .
 docker push hackerstanislav/servant:latest
```

 [logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
 [tests-cli]: ../../servant-cli/doc/servant.clia.tests.md
 [browsers]: ../../servant/doc/servant.json.md