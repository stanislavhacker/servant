# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.14] - 2023/09/01
- *FIX*: Target `web_page` automatically bundled all modules, also use alias for determine exactly one module in case of symlinked or shared modules

## [1.0.13] - 2023/08/23
- *FIX*: Repair external libraries in browsers tests

## [1.0.12] - 2023/08/19
- *FIX*: Repair exports for typed css, use default export

## [1.0.11] - 2023/08/18
- *FEATURE*: Module `trancorder` and `trancorder-cli` used for localisation
- *FEATURE*: Use `trancorder-cli` in servant in validate command now
- *FIX*: Try to find first non-used port
- *FIX*: Assets in webpack

## [1.0.10] - 2023/05/25
- *FIX*: Repair of detecting main module for generators

## [1.0.9] - 2023/05/23
 - Error handling in preview
 - Implementation and support for generators
 - Replace generator for `init` and `unify` command
 - *FIX*: Command `shared` with **--link** mode try to remove death links created before
 - *FIX*: Convert devlink errors into normal errors
 - *FIX*: Right merging and using server property in servant.json
 - *FIX*: Added check if discovered module is not same as me
 - *FIX*: Maximum callstack in module graph and tree determine
 - *FIX*: Remove circular reference in tests results, to be able to send through IPC channel
 - *FEATURE*: Added transpile option into watcher
 - *FEATURE*: Support `shared` folders in server property in `servant.json`, these folders are cleaned by `clean` command
 - *FEATURE*: Make possibility to create web page bundle by using `web_page` target with webpage in `servant.json`


**Totally rework of development server**

Developments server can now run web modules and also node modules. For every node module
that is defined in entries, Servant spawn process and try to keep this process running
with the newest source codes.

## [1.0.8] - 2023/01/21
 - *FIX*: Repair bug with super props

## [1.0.7] - 2023/01/21
 - Shared modules support with possibility to create links to globally shared modules

## [1.0.6] - 2023/01/12
 - Resolving local included external libraries in dependency graph, this will allow to use shared dependencies in multiple packages. More information in documentation.
 - *FIX*: Clean do not remove .dot files in linked node modules

## [1.0.5] - 2022/12/09
 - Replace internal symlink for "devlink-core" package
 - *FIX*: Jasmine complains about invalid character in file paths
 - *FIX*: Empty root describe in nodejs tests never ends tests
 - *FIX*: Invalid error catching in validate command
 - *FIX*: Prettier glob pattern ignore node_modules folder
 - *FIX*: Unknown file for prettier is reported as error
 - *FIX*: Getting external module in discovery skip already used
 - *FIX*: Better error if there are no permissions to create symlink

## [1.0.4] - 2022/11/24
 - *FIX*: Module definition can not be serialized now, sends only module info into workers

## [1.0.3] - 2022/11/24
 - Better typings for `command(...)` and `create(...)` method on API
 - Command `analyze` that can show some stats about project as described in [#15](https://gitlab.com/stanislavhacker/servant/-/issues/15) 
 - Support for `analyze` report, that report project dependencies as described in [#1](https://gitlab.com/stanislavhacker/servant/-/issues/1)

## [1.0.2] - 2022/10/13
 - Build command now resolve files, that need to be build correctly
 - **FEATURE**: Sass support as described in [#19](https://gitlab.com/stanislavhacker/servant/-/issues/19)

## [1.0.1] - 2022/09/27
 - Update of documentation
 - Init Servant now run also "install" command on end
 - Unify command now allows repair versions in console, without provided in command line.
 - Unify command now can use `--latest` flag

## [1.0.0] - 2022/09/18
 **FIRST MAJOR RELEASE**
 - Stabilized nodejs API for Servant
 - Shipped all necessary features and tools
 - Validated and check by prettier and eslint

## [0.3.2] - 2022-09-14
 - Added flag `--increment <type>` for override default behaviour set in `servant.json`

## [0.3.1] - 2022-09-12
 - Implementation of discovery service and Servant external modules link
 - Implementation of "validation command"
 - Implementation of native **eslint** support in validation command
 - Implementation of native **prettier** support in validation command
 - Init module can create eslint and prettier config files in module
 - Documentation in cli
 - Update init command to be more helpfully
 - Support running command from submodule project and use `--only` flag automatically

## [0.2.4] - 2022-07-29
 - Repair build status in case there is only warning in build module
 - Update documentation

## [0.2.3] - 2022-07-17
 - Update webpack to version **5**
 - Update all webpack loaders to be compatible with webpack 5

## [0.2.2] - 2022-07-08
 - Update resolving of modules in servant

## [0.2.1] - 2022-06-01
 - Increment versions of modules, except webpack => will be done in separate version
 - Update docker file, increment node version
 - Support for `<meta ...>` and `<body>` tags in dev server template

## [0.1.42] - 2021-09-24
### Added
 - Flag `--dependencies` that can be used with flag `--only` and is used for run commands on defined modules in
  only and all dependants modules.
 - Upgrade version of dependencies due to security audits

## [0.1.41] - 2021-06-10
### Added
 - Flag `--noaudit` for disable auditing during `install` command

## [0.1.40]
### Changed - 2021-06-10
 - Update dependencies due to security audit

## [0.1.39]
### Changed - 2021-04-17
 - Update view for Watcher, watcher shows changed files
 - *FIX*: Watcher infinitely build changed modules
 - **PERF**: Due to checking crc of file content, watcher can be slower than in previous versions

## [0.1.38]
### Changed - 2021-03-26
 - Update dependencies for `envfull` module

## [0.1.37]
### Changed - 2020-11-02
 - Update dependencies due to security audit
 
## [0.1.36] - 2020-03-06
### Added
 - Flag `--commit` that is used in `publish` command. Servant will make commit with defined commit message for every module version incremented.
### Changed
 - Update of help for flags `--devices` and `--browsers` 
 - *FIX*: Added 'file-loader' into internal webpack for loading large images

## [0.1.35] - 2020-02-01
### Changed
 - Rework of playwright module into Promises like calls
 - *FIX*: Loading of optional playwright module

## [0.1.34] - 2020-02-01
### Added
 - Added module '@servant/servant-playwright' for testing in more browsers
 - Jasmine module can use playwright to run test against more browsers
 - Tests view shows browsers where test are running + count of devices
 
### Changed
 - Move "memfs" module for tests into servant module
 - *FIX*: Right watch changes only in css or less files and rebuild module

## [0.1.33] - 2020-01-03
### Changed
 - Update dependencies version, remove outdated modules, security audit fix.

## [0.1.32] - 2020-01-03
### Changed
 - *FIX*: Servant not load dev dependencies on main package.json. This bug causing  that tests are
  not working in case that root module is marked as web based. 

## [0.1.31] - 2019-10-28
### Changed
 - *FIX*: Freeze version of "memfs" dependency

## [0.1.30] - 2019-10-28
### Added
 - Automatically open chrome fullscreen if there is a `--gui` flag
### Changed
 - *FIX*: Bundled dependencies are not loaded into tests


## [0.1.29] - 2019-09-23
### Change
 - Update version of "envfull" module

## [0.1.28] - 2019-09-03
### Added
 - Support of `bundledDependencies` from `package.json`
 - Because of bundled modules, we need resolving js and jsx even if there is only Typescript module

## [0.1.27] - 2019-08-31
### Change
 - Servant use "envfull" module for parsing of cmd and .env data
 - API for unify was changed. Module for unify is now defined in cmd as "--m.<module-name> <version>"

## [0.1.26] - 2019-08-29
### Changed
 - *FIX*: Do not create scripts named as "publish" and "package".

## [0.1.25] - 2019-08-28
### Added
 - Support for `.gitignore` file create
 - Init can initialize new git repo if not exists
 - If there is git available, add files created by init into git repo
 - Support for `package.json` properties:
    - `optionalDependencies` property
    - `bundledDependencies` property
    - `peerDependencies` property
 - Support for scoped registries
 - Support for dev dependencies strip

## [0.1.24] - 2019-07-11

## [0.1.23] - 2019-07-10
### Changed
 - Documentation update + tests update 

## [0.1.22] - 2019-07-09
### Changed
 - *FIX*: Transpile tests before run

## [0.1.21] - 2019-07-09
### Changed
 - Added no sandbox for chrome launcher

## [0.1.20] - 2019-07-07
### Changed
 - *FIX*: Init not fill output filename correctly

## [0.1.19] - 2019-07-07
### Changed
 - *UPGRADE*: Chokidar file watcher version upgrade

## [0.1.18] - 2019-07-06
### Changed
 - *FIX*: Init not create submodules right way

## [0.1.17] - 2019-07-04
### Changed
 - *FIX*: External libraries mapping not work correctly

## [0.1.16] - 2019-06-26
### Changed
 - If we setup gui for tests, apply timeout to wait on browser gui start (dev tools 
  open and debugger can work)
 - *FIX*: Do not polyfill Buffer for nodejs app

## [0.1.15] - 2019-06-20
### Changed
 - Init create `tsconfig.json` only if you choose typescript
 - Documentation update

## [0.1.14] - 2019-06-19
### Added
 - Loading resources into dev server defined in servant.json `resources` property
 - Flag `--watch` for watching changes in project and rebuild it
 - Less use less node api compiler, do not fork another process

### Changed
 - `mappings` in servant.json can be also false (no library will be included)
 - *FIX*: Resolve externals do not loading externals libraries in right order
 - *FIX*: Loading source load generated files, that are cause always rebuilds
 - *FIX*: Resolving and building css based on imports or on entry name

## [0.1.13] - 2019-05-26
### Added
 - Flag `--prune` for clearing node_modules dir and package-lock.json file
 - Reporting missing modules when you are not run install or update command
 - There is `@servant/servant-build-webpack` module for building typescript in separate process

### Changed
 - Better clean support, clean deletes also directories and . start files
 - Because of memory leak of ts-loader every build is done in separate process
 - Change build of declarations files and bundle
 - Parallel build and publish of modules, that can be run in parallel mode
 - Better clean files, do not removed modules that are locally installed
 - Update build of d.ts files from css
 - *FIX*: Can not run Servant if there are modules without installed packages
 - *FIX*: Git status renamed files repair

## [0.1.12] - 2019-05-19
### Changed
 - Speed up building of module
 - *FIX*: Create project do scripts into package.json

## [0.1.11] - 2019-05-18
### Changed
 - *FIX*: Resolving loaders in webpack
 - *FIX*: Resolving modules for tsc, css
 - *FIX*: Css build if there was no less build

## [0.1.10] - 2019-05-17
### Changed
 - *FIX*: Require resolve is moved into function, cause resolving web modules inside nodejs

## [0.1.9] - 2019-05-17
### Added
 - support for package.json private field, module mark with private are reported into servant 
  cli while publish and skipped
 - added temp directory defined in `temp` property that was deleted by clean command
 - support for running `tests` command, default there are Jasmine
 - init has language type to determine right creating of servant.json
 - fix report for parsing issue name from test name, output into console
 - report generation, now support only "issues" report

### Changed
 - servant.json `entry` property can have value `FALSE` => module not intended for build output
 - `@servant/servant-data` module with files definitions
 - `resources` can contain path into another module, like npm://react//path/to/file.js
 - `server.libs` property in servant.json moved to `mappings`, universal used
 -  `init` can create folder structure
 - *FIX*: Module resolving
 - *FIX*: Less runner path repair

## [0.1.8] - 2019-04-15
### Added
 - init create `tsconfig.js` file as default - can be deleted by user
 - error codes implementation + enum of these codes
 
### Changed
 - repair default for `output.filename` property
 - error codes implementation + enum of these codes
 - *FIX*: Resolving of internal modules and @types/.. modules

## [0.1.7] - 2019-04-09
### Added
 - property `server` for development server definition 
 - development server command line view
 - module for development server
 - JSON schema for `servant.json`
 
### Changed
 - Property `publish.registry` was moved to `registry` only. It's used by another commands.

## [0.1.6] - 2019-04-07
### Added
 - Command line help create
 
### Changed
 - Command `publish` has manipulate with main and types property. More at [publish command][publish].
 - Change of view for command, init, help

## [0.1.4] - 2019-04-01
### Added
 - Changelog file
 - Added flag `--init` for creating new project
 - *FIX*: Merge of `servant.json` nor work correctly for output and publish

## [0.1.1] - 2019-03-29
### Added
- First version of Servant
- Support for command `clean`, `install`, `update`, `publish`, `unify`
- Prepared for help
- NodeJS api
- Command line api

[publish]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.publish.md

[Unreleased]: https://gitlab.com/stanislavhacker/servant/compare/1.0.14...master
[1.0.14]: https://gitlab.com/stanislavhacker/servant/compare/1.0.13...1.0.14
[1.0.13]: https://gitlab.com/stanislavhacker/servant/compare/1.0.12...1.0.13
[1.0.12]: https://gitlab.com/stanislavhacker/servant/compare/1.0.11...1.0.12
[1.0.11]: https://gitlab.com/stanislavhacker/servant/compare/1.0.10...1.0.11
[1.0.10]: https://gitlab.com/stanislavhacker/servant/compare/1.0.9...1.0.10
[1.0.9]: https://gitlab.com/stanislavhacker/servant/compare/1.0.8...1.0.9
[1.0.8]: https://gitlab.com/stanislavhacker/servant/compare/1.0.7...1.0.8
[1.0.7]: https://gitlab.com/stanislavhacker/servant/compare/1.0.6...1.0.7
[1.0.6]: https://gitlab.com/stanislavhacker/servant/compare/1.0.5...1.0.6
[1.0.5]: https://gitlab.com/stanislavhacker/servant/compare/1.0.4...1.0.5
[1.0.4]: https://gitlab.com/stanislavhacker/servant/compare/1.0.3...1.0.4
[1.0.3]: https://gitlab.com/stanislavhacker/servant/compare/1.0.2...1.0.3
[1.0.2]: https://gitlab.com/stanislavhacker/servant/compare/1.0.1...1.0.2
[1.0.1]: https://gitlab.com/stanislavhacker/servant/compare/1.0.0...1.0.1
[1.0.0]: https://gitlab.com/stanislavhacker/servant/compare/0.3.2...1.0.0
[0.3.2]: https://gitlab.com/stanislavhacker/servant/compare/0.3.1...0.3.2
[0.3.1]: https://gitlab.com/stanislavhacker/servant/compare/0.2.4...0.3.1
[0.2.4]: https://gitlab.com/stanislavhacker/servant/compare/0.2.3...0.2.4
[0.2.3]: https://gitlab.com/stanislavhacker/servant/compare/0.2.2...0.2.3
[0.2.2]: https://gitlab.com/stanislavhacker/servant/compare/0.2.1...0.2.2
[0.2.1]: https://gitlab.com/stanislavhacker/servant/compare/0.1.42...0.2.1
[0.1.42]: https://gitlab.com/stanislavhacker/servant/compare/0.1.41...0.1.42
[0.1.41]: https://gitlab.com/stanislavhacker/servant/compare/0.1.40...0.1.41
[0.1.40]: https://gitlab.com/stanislavhacker/servant/compare/0.1.39...0.1.40
[0.1.39]: https://gitlab.com/stanislavhacker/servant/compare/0.1.38...0.1.39
[0.1.38]: https://gitlab.com/stanislavhacker/servant/compare/0.1.37...0.1.38
[0.1.37]: https://gitlab.com/stanislavhacker/servant/compare/0.1.36...0.1.37
[0.1.36]: https://gitlab.com/stanislavhacker/servant/compare/0.1.35...0.1.36
[0.1.35]: https://gitlab.com/stanislavhacker/servant/compare/0.1.34...0.1.35
[0.1.34]: https://gitlab.com/stanislavhacker/servant/compare/0.1.33...0.1.34
[0.1.33]: https://gitlab.com/stanislavhacker/servant/compare/0.1.32...0.1.33
[0.1.32]: https://gitlab.com/stanislavhacker/servant/compare/0.1.31...0.1.32
[0.1.31]: https://gitlab.com/stanislavhacker/servant/compare/0.1.30...0.1.31
[0.1.30]: https://gitlab.com/stanislavhacker/servant/compare/0.1.29...0.1.30
[0.1.29]: https://gitlab.com/stanislavhacker/servant/compare/0.1.28...0.1.29
[0.1.28]: https://gitlab.com/stanislavhacker/servant/compare/0.1.27...0.1.28
[0.1.27]: https://gitlab.com/stanislavhacker/servant/compare/0.1.26...0.1.27
[0.1.26]: https://gitlab.com/stanislavhacker/servant/compare/0.1.25...0.1.26
[0.1.25]: https://gitlab.com/stanislavhacker/servant/compare/0.1.24...0.1.25
[0.1.24]: https://gitlab.com/stanislavhacker/servant/compare/0.1.23...0.1.24
[0.1.23]: https://gitlab.com/stanislavhacker/servant/compare/0.1.22...0.1.23
[0.1.22]: https://gitlab.com/stanislavhacker/servant/compare/0.1.21...0.1.22
[0.1.21]: https://gitlab.com/stanislavhacker/servant/compare/0.1.20...0.1.21
[0.1.20]: https://gitlab.com/stanislavhacker/servant/compare/0.1.19...0.1.20
[0.1.19]: https://gitlab.com/stanislavhacker/servant/compare/0.1.18...0.1.19
[0.1.18]: https://gitlab.com/stanislavhacker/servant/compare/0.1.17...0.1.18
[0.1.17]: https://gitlab.com/stanislavhacker/servant/compare/0.1.16...0.1.17
[0.1.16]: https://gitlab.com/stanislavhacker/servant/compare/0.1.15...0.1.16
[0.1.15]: https://gitlab.com/stanislavhacker/servant/compare/0.1.14...0.1.15
[0.1.14]: https://gitlab.com/stanislavhacker/servant/compare/0.1.13...0.1.14
[0.1.13]: https://gitlab.com/stanislavhacker/servant/compare/0.1.12...0.1.13
[0.1.12]: https://gitlab.com/stanislavhacker/servant/compare/0.1.11...0.1.12
[0.1.11]: https://gitlab.com/stanislavhacker/servant/compare/0.1.10...0.1.11
[0.1.10]: https://gitlab.com/stanislavhacker/servant/compare/0.1.9...0.1.10
[0.1.9]: https://gitlab.com/stanislavhacker/servant/compare/0.1.8...0.1.9
[0.1.8]: https://gitlab.com/stanislavhacker/servant/compare/0.1.7...0.1.8
[0.1.7]: https://gitlab.com/stanislavhacker/servant/compare/0.1.6...0.1.7
[0.1.6]: https://gitlab.com/stanislavhacker/servant/compare/0.1.4...0.1.6
[0.1.4]: https://gitlab.com/stanislavhacker/servant/compare/0.1.1...0.1.4
[0.1.1]: https://gitlab.com/stanislavhacker/servant/commit/1b116e32957ba012ee0fd799bf98c83a09c28d2b
