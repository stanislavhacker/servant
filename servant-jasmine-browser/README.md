# ![Servant][logo] Servant jasmine browser

Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

Servant jasmine browser is module for Servant build tool that is intended for running tests in browser
environment. Module can be included as module `@servant/servant-jasmine-browser` or can be forked
from main nodejs process. If you want to run it as process, you need send message with data to
say module that you need some work.

```typescript
import { ServantJson, PackageJson } from "@servant/servant-data";

export type TestsJson = {
	cwd: string;
	entry: string | null;
	extensions: Array<string>;
	externals: Array<string>;
	packageJson: PackageJson.PackageJsonInfo | null;
	servantJson: ServantJson.ServantJsonInfo | null;
};
```

Message must contain `cwd` (working directory path) and `entry` that is used as a base directory
for loading test from.

Property `extensions` is array of extensions that are resolved by tests.

Property `externals` is a list of external libraries that are necessary for running tests but there
are not a part of your module.

Property `packageJson` is PackageJsonInfo for [package.json][1].

Property `servantJson` is ServantJsonInfo for [servant.json][2].

### Playwright

This module optionally can use [playwright][playwright] module from [Microsoft][ms]. This module is used to multi browser
testing of your web browsers modules. By settings that is done in `testing` property in [servant.json][2] you can
setup test to run in **Chrome**, **Firefox** and **Chromium** browsers with emulating more than **70** different devices!

### License

[Licensed under GPLv3][license]

[Playwright][playwright] is licensed under [Apache-2.0][license2]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
[playwright]: https://www.npmjs.com/package/playwright
[ms]: https://github.com/microsoft
[1]: https://docs.npmjs.com/files/package.json
[2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
[3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
[4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
[5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
[license]: https://gitlab.com/stanislavhacker/servant/blob/master/LICENSE
[license2]: https://github.com/microsoft/playwright/blob/master/LICENSE
