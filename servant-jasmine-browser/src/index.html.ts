export function createTemplate(
	cwd: string,
	rand: string,
	module: string,
	api: string,
	push: string,
	source: string,
	tests: Array<string>
) {
	return `<!DOCTYPE html>
			<html lang="en">
				<head>
					<meta charset="UTF-8">
					<title>Jasmine - module ${module}</title>
					
					<link rel="stylesheet" type="text/css" href="/${api}/jasmine.css">

					<script type="text/javascript" src="/${api}/jasmine.js"></script>
					<script type="text/javascript" src="/${api}/jasmine-html.js"></script>
					<script type="text/javascript" src="/${api}/boot0.js"></script>
					<script type="text/javascript" src="/${api}/boot1.js"></script>
					<script type="text/javascript" src="/${api}/mock-ajax.js"></script>
					<script type="text/javascript" src="/${api}/servant-jasmine-browser-reporter.js"></script>
					<script>
						var reporter = window["@servant/servant-jasmine-browser-reporter"];
						var env = jasmine.getEnv();

						env.addReporter(reporter.init('${cwd}', "/${rand}/${push}"));
					</script>
				</head>
				<body>
					${tests.map((t) => `<script type="text/javascript" src="/${source}/${t}"></script>`).join("")}
				</body>
			</html>`;
}
