const PlaywrightName = "@servant/servant-playwright";
let playwright;

try {
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-ignore
	const requireFn = typeof __webpack_require__ === "function" ? __non_webpack_require__ : require;
	playwright = requireFn(PlaywrightName);
} catch (e) {
	playwright = null;
}

export { PlaywrightName, playwright };

export type PlaywrightBrowser = {
	close: () => void;
};
