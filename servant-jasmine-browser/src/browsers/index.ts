import { chrome } from "./chrome";

export { chrome };

export type BrowserInfo = {
	name: string;
	rand: string;
};

export function createBrowserInfo(name: string, rand: string): BrowserInfo {
	return {
		name: name,
		rand: rand,
	};
}

export function createUrl(port: number, rand: string): string {
	return `http://localhost:${port}/${rand}/`;
}
