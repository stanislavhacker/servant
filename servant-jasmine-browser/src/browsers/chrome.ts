import * as chromeLauncher from "chrome-launcher";
import * as cp from "child_process";

import { createUrl, createBrowserInfo, BrowserInfo } from "./index";

export function chrome(
	port: number,
	launched: (info: BrowserInfo) => void,
	start: (process: cp.ChildProcess, info: BrowserInfo) => void,
	exit: (info: BrowserInfo) => void,
	gui: boolean
): Promise<void> {
	const rand = Math.random().toString(36).slice(2);

	return new Promise((resolve, reject) => {
		const browserInfo = createBrowserInfo("Chrome", rand);

		chromeLauncher
			.launch({
				startingUrl: createUrl(port, rand),
				chromeFlags: getFlags(gui),
			})
			.then((chrome) => {
				//exist event
				chrome.process.on("exit", () => exit(browserInfo));
				//start
				start(chrome.process, browserInfo);
				resolve();
			})
			.catch(reject);
		//launched
		launched(browserInfo);
	});
}

function getFlags(gui: boolean): Array<string> {
	const flags = ["--no-default-browser-check", "--process-per-tab", "--new-window"];

	if (gui) {
		//in case of gui, add automatic open dev tool, debugging
		flags.push("--auto-open-devtools-for-tabs");
		flags.push("--start-maximized");
	} else {
		flags.push("--headless");
		flags.push("--no-sandbox");
	}

	return flags;
}
