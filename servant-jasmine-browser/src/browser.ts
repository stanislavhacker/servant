import { WebpackConfig, ServantJson, Modules } from "@servant/servant-data";
import { Path, Extensions } from "@servant/servant-files";
import {
	TestsResults,
	createTestsResults,
	TestsSuit,
	TestsSpec,
	TestsItemType,
	TestsSpecStatus,
	SpecExpectation,
} from "@servant/servant-jasmine-browser-reporter";
import { playwright, PlaywrightBrowser, PlaywrightName } from "./playwright";
import * as webpack from "webpack";

import * as http from "http";
import * as path from "path";
import * as fs from "fs";
import * as glob from "glob";
import * as cp from "child_process";

import { createTemplate } from "./index.html";
import { chrome, BrowserInfo } from "./browsers";
import { invariant } from "./invariant";

const tries = 100;
const defaultPort = 8263;
const api = "--";
const source = "sources";
const push = "push";

export type TestsProgresses = {
	playwright: boolean;
	browsers: Array<string>;
	devices: {
		used: Array<string>;
		invalid: Array<string>;
	};
	progresses: {
		[key: string]: TestsProgress;
	};
	complete: boolean;
	error: string | null;
};

export type TestsProgress = TestsResults & {
	type: "ok" | "warn" | "fail";
	browser: BrowserInfo;
};

export { TestsResults, TestsSuit, TestsSpec, TestsItemType, TestsSpecStatus, SpecExpectation };

export function tests(
	cwd: string | null,
	entry: string | null,
	externals: Array<string>,
	module: Modules.ModuleInfo | null,
	browsers: Array<string>,
	devices: Array<string>,
	gui: boolean,
	progress: (progress: TestsProgresses) => void
): Promise<TestsProgresses> {
	return new Promise((resolve) => {
		const time = process.hrtime();
		const progresses = createProgresses();

		//no entry or cwd
		if (entry === null || cwd === null || module === null) {
			progresses.error = `You must specify these parameters: 'entry', 'cwd', 'module'.`;
			resolve(progresses);
			return;
		}

		invariant(
			module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const packageJson = module.packageJson;
		const servantJson = module.servantJson;

		let p: Promise<unknown> = Promise.resolve();
		let results: CompileResults | null = null;

		const testing = ServantJson.getTesting(servantJson);
		//NOTE: Update definitions, cmd data has always priority
		testing.browsers = browsers.length
			? (browsers as Array<ServantJson.Browsers>)
			: testing.browsers;
		testing.devices = devices.length ? devices : testing.devices;

		const reject = createReject(progresses, resolve);
		const prg = createHandler(progresses, time, progress, resolve);

		//check for entry point
		if (!entryExists(servantJson)) {
			const testsFiles = Path.join(servantJson.cwd, servantJson.content.tests);
			//get tests files from servant.json
			getFiles(testsFiles)
				.then((files) => {
					//no tests files found on project = no tests => mark as resolved, otherwise error
					if (files.length > 0) {
						reject(
							new Error(
								`There are no entry point for tests '${servantJson.content.test}' in module ${packageJson.content.name}. Servant need this for properly running tests in browser.`
							)
						);
					} else {
						resolve(progresses);
					}
				})
				.catch(reject);
			return;
		}

		//multi browser testing is defined and no playwright installed
		if (playwrightNeeded(testing) && !playwright) {
			reject(
				new Error(
					`There is defined 'testing' property with more browsers or devices. This is not base functionality of Servant and you need special module ${PlaywrightName}. Run 'npm install ${PlaywrightName} --save-dev' to install module.`
				)
			);
			return;
		}
		//use playwright
		updateProgressByTesting(
			progresses,
			testing,
			Boolean(playwrightNeeded(testing) && playwright)
		);

		//compile data
		p = p.then(() => compile(cwd, module).catch(reject));
		p = p.then((compileResults: CompileResults) => {
			results = compileResults;
			return Promise.all([
				getFiles(externals).catch(reject),
				getFiles([Path.patterns.all(compileResults.output)]).catch(reject),
			]).catch(reject);
		});
		p.then((data: [Array<string>, Array<string>]) =>
			serve(entry, results, [...data[0], ...data[1]], testing, gui, prg).catch(reject)
		);
	});
}

function updateProgressByTesting(
	progresses: TestsProgresses,
	testing: ServantJson.TestingSettings,
	allowed: boolean
) {
	//use playwright
	progresses.playwright = allowed;
	progresses.browsers = [
		...(testing.browsers.indexOf("Webkit") >= 0 ? ["Webkit"] : []),
		...(testing.browsers.indexOf("Firefox") >= 0 ? ["Firefox"] : []),
		...(testing.browsers.indexOf("Chromium") >= 0 ? ["Chromium"] : []),
	];
	//validate devices
	if (allowed) {
		testing.devices.forEach((device) => {
			if (playwright.pw.devices[device]) {
				progresses.devices.used.push(device);
			} else {
				progresses.devices.invalid.push(device);
			}
		});
	}
}

function createHandler(
	progresses: TestsProgresses,
	time: [number, number],
	progress: (progress: TestsProgresses) => void,
	resolve: (progress: TestsProgresses) => void
) {
	return (rand: string, data: BrowserData, done: boolean) => {
		progresses.progresses[rand] = returnResult(data, time);
		//progress
		progress(progresses);
		//Done
		done && resolve(progresses);
	};
}

function createReject(progresses: TestsProgresses, resolve: (progress: TestsProgresses) => void) {
	return (error: Error) => {
		progresses.error = error.message;
		resolve(progresses);
	};
}

type CompileResults = {
	errors: Array<Error>;
	success: boolean;
	output: string;
};

function compile(cwd: string, module: Modules.ModuleInfo): Promise<CompileResults> {
	return new Promise((resolve, reject) => {
		//normal module
		const webpackConfig = WebpackConfig.tests(cwd, module, {
			production: false,
			transpile: false,
			excluded: [],
		});

		//call webpack
		webpack(webpackConfig as webpack.Configuration, (err, stats) => {
			if (err) {
				reject(err);
				return;
			}

			if (!stats) {
				reject(
					new Error(
						`Can get stats from webpack for module ${module}! Something go wrong with Servant :(`
					)
				);
				return;
			}

			const errors = stats.compilation.errors.slice(0);
			const output = stats.compilation.outputOptions.path || "";

			resolve({
				output,
				errors,
				success: errors.length === 0,
			});
		});
	});
}

function getFiles(outputs: Array<string>): Promise<Array<string>> {
	return new Promise((resolve, reject) => {
		const arr: Array<Promise<Array<string>>> = [];

		outputs.map((output) => {
			arr.push(getFile(output));
		});

		return Promise.all(arr)
			.then((results) => {
				resolve(
					results.reduce((flat, toFlatten) => {
						return [...flat, ...toFlatten];
					}, [])
				);
			})
			.catch(reject);
	});
}

function getFile(output: string): Promise<Array<string>> {
	return new Promise((resolve, reject) => {
		glob(Path.normalize(output), (err: Error, files: Array<string>) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(files);
		});
	});
}

function entryExists(servantJson: ServantJson.ServantJsonInfo): boolean {
	const check: Array<string> = Extensions.resolve(
		servantJson.cwd,
		servantJson.content.test,
		servantJson.content.resolve
	);

	return check.some((filepath) => fs.existsSync(filepath));
}

function playwrightNeeded(testing: ServantJson.TestingSettings): boolean {
	return ServantJson.isComplexTesting(testing);
}

//SERVE

function serve(
	entry: string,
	results: CompileResults | null,
	files: Array<string>,
	testing: ServantJson.TestingSettings,
	gui: boolean,
	handler: (rand: string, data: BrowserData, done: boolean) => void
): Promise<void> {
	return new Promise((resolve, reject) => {
		const jasmine = resolveModulePath("jasmine-core");
		const jasmineDirectory = path.join(path.dirname(jasmine), "./jasmine-core");

		const jasmineAjax = resolveModulePath("jasmine-ajax");
		const jasmineAjaxDirectory = path.dirname(jasmineAjax);

		const reporter = resolveModulePath("@servant/servant-jasmine-browser-reporter");
		const reporterDir = path.join(path.dirname(reporter), "../dist");

		const module = path.basename(entry);
		const browsersData: Browsers = {};

		const callback = (rand: string, results: TestsResults, done: boolean) => {
			//save
			resultsSave(browsersData, rand, results);
			//send
			handler(rand, browsersData[rand], done);
		};

		testsServer(
			//listener
			listener(
				entry,
				jasmineDirectory,
				jasmineAjaxDirectory,
				reporterDir,
				browsersData,
				gui,
				callback
			)
		)
			.then((data: [http.Server, number]) => {
				//create browsers by using playwright
				if (ServantJson.isComplexTesting(testing)) {
					//create browsers for running tests
					return playwrightBrowsers(
						data,
						browsersData,
						entry,
						module,
						files,
						testing,
						gui,
						callback
					).catch(reject);
				}
				//create browsers for running tests
				return simpleBrowsers(
					data,
					browsersData,
					entry,
					module,
					files,
					gui,
					callback
				).catch(reject);
			})
			.then(resolve)
			.catch(reject);
	});
}

function listener(
	entry: string,
	jasmineDir: string,
	ajaxDir: string,
	reporterDir: string,
	browsers: Browsers,
	gui: boolean,
	handler: (rand: string, results: TestsResults, done: boolean) => void
): http.RequestListener {
	return (request: http.IncomingMessage, response: http.ServerResponse) => {
		let handled = false;

		//index.html template
		handled = handled || sendTemplate(request, response, browsers, gui);
		handled = handled || sendInner(request, response, jasmineDir, ajaxDir, reporterDir);
		handled = handled || sendSource(request, response);
		handled = handled || receivePush(request, response, handler);
		handled || send404(response);
	};
}

//RESULTS

function resultsSave(browsersData: Browsers, rand: string, results: TestsResults) {
	const browserInfo = browsersData[rand];
	const process = browserInfo.process;
	const browser = browserInfo.browser;

	//save results
	browserInfo.results = results;
	//test are done, kill it
	if (results.summary.done && process && !process.killed) {
		process.kill();
	}
	//test are done, kill it
	if (results.summary.done && browser) {
		browser.close();
	}
}

//BROWSER

type BrowserData = {
	browser: PlaywrightBrowser | null;
	process: cp.ChildProcess | null;
	template: string;
	results: TestsResults;
	info: BrowserInfo;
};

type Browsers = {
	[key: string]: BrowserData;
};

function simpleBrowsers(
	data: [http.Server, number],
	browsers: Browsers,
	entry: string,
	module: string,
	files: Array<string>,
	gui: boolean,
	handler: (rand: string, results: TestsResults, done: boolean) => void
): Promise<void> {
	return new Promise((resolve, reject) => {
		const [, port] = data;
		const promises: Array<Promise<void>> = [];

		const onLaunch = (info: BrowserInfo) => {
			//save
			browsers[info.rand] = createBrowserData(entry, module, files, info);
		};
		const onStart = (process: cp.ChildProcess, info: BrowserInfo) => {
			//save process
			browsers[info.rand].process = process;
		};
		const onEnd = (info: BrowserInfo) => {
			const browser = browsers[info.rand];
			const results = browser.results;
			const done = Object.keys(browsers).length === 1;
			//handler call
			handler(info.rand, results, done);
			//remove
			delete browsers[info.rand];
			//done
			done && resolve();
		};

		promises.push(chrome(port, onLaunch, onStart, onEnd, gui));

		Promise.all(promises).catch(reject);
	});
}

function playwrightBrowsers(
	data: [http.Server, number],
	browsers: Browsers,
	entry: string,
	module: string,
	files: Array<string>,
	testing: ServantJson.TestingSettings,
	gui: boolean,
	handler: (rand: string, results: TestsResults, done: boolean) => void
): Promise<void> {
	return new Promise((resolve, reject) => {
		const [, port] = data;
		const promises: Array<Promise<void>> = [];

		const onLaunch = (info: BrowserInfo) => {
			//save
			const browser = (browsers[info.rand] = createBrowserData(entry, module, files, info));
			const results = browser.results;
			//handler call
			handler(info.rand, results, false);
		};
		const onStart = (browser: PlaywrightBrowser, info: BrowserInfo) => {
			//save process
			browsers[info.rand].browser = browser;
		};
		const onEnd = (info: BrowserInfo) => {
			const browser = browsers[info.rand];
			const results = browser.results;
			const done = Object.keys(browsers).length === 1;
			//handler call
			handler(info.rand, results, done);
			//remove
			delete browsers[info.rand];
			//done
			done && resolve();
		};

		//add webkit
		if (testing.browsers.indexOf("Webkit") >= 0) {
			promises.push(playwright.webkit(port, onLaunch, onStart, onEnd, null, gui));
			testing.devices.forEach((dev) => {
				promises.push(playwright.webkit(port, onLaunch, onStart, onEnd, dev, gui));
			});
		}
		//add firefox
		if (testing.browsers.indexOf("Firefox") >= 0) {
			promises.push(playwright.firefox(port, onLaunch, onStart, onEnd, null, gui));
			testing.devices.forEach((dev) => {
				promises.push(playwright.firefox(port, onLaunch, onStart, onEnd, dev, gui));
			});
		}
		//add chromium
		if (testing.browsers.indexOf("Chromium") >= 0) {
			promises.push(playwright.chromium(port, onLaunch, onStart, onEnd, null, gui));
			testing.devices.forEach((dev) => {
				promises.push(playwright.chromium(port, onLaunch, onStart, onEnd, dev, gui));
			});
		}

		Promise.all(promises).catch(reject);
	});
}

function createBrowserData(
	entry: string,
	module: string,
	files: Array<string>,
	info: BrowserInfo
): BrowserData {
	return {
		browser: null,
		process: null,
		template: createTemplate(entry, info.rand, module, api, push, source, files),
		results: createTestsResults(),
		info: info,
	};
}

//SERVER

function receivePush(
	request: http.IncomingMessage,
	response: http.ServerResponse,
	handler: (rand: string, results: TestsResults, done: boolean) => void
) {
	const urlData = (request.url || "").split("/").slice(1);
	const rand = urlData.shift();
	const isPush = urlData.shift() === push;

	//no push
	if (!isPush || !rand) {
		return false;
	}

	let data = "";
	request.on("data", (chunk) => {
		data += chunk;
	});
	request.on("end", () => {
		handler(rand, JSON.parse(data) as TestsResults, false);
		response.writeHead(200);
		response.end();
	});
	return true;
}

function sendSource(request: http.IncomingMessage, response: http.ServerResponse) {
	const urlData = (request.url || "").split("/").slice(1);
	const isSource = urlData.shift() === source;

	//no api
	if (!isSource) {
		return false;
	}

	const file = urlData.join("/");
	//check exists
	if (fs.existsSync(file)) {
		fs.readFile(file, (err, data) => {
			if (!err) {
				response.writeHead(200);
				response.write(data.toString());
				response.end();
			} else {
				send500(response);
			}
		});
		return true;
	}
	return false;
}

function sendInner(
	request: http.IncomingMessage,
	response: http.ServerResponse,
	jasmineDir: string,
	reporterDir: string,
	ajaxDir: string
) {
	const urlData = (request.url || "").split("/").slice(1);
	const isApi = urlData.shift() === api;
	let file: string;

	//no api
	if (!isApi) {
		return false;
	}

	//load fn
	const load = (pth: string) => {
		fs.readFile(pth, (err, data) => {
			if (!err) {
				response.writeHead(200);
				response.write(data.toString());
				response.end();
			} else {
				send500(response);
			}
		});
	};

	//jasmine
	file = path.join(jasmineDir, urlData.join("/"));
	if (fs.existsSync(file)) {
		load(file);
		return true;
	}

	//reporter
	file = path.join(reporterDir, urlData.join("/"));
	if (fs.existsSync(file)) {
		load(file);
		return true;
	}

	//ajax
	file = path.join(ajaxDir, urlData.join("/"));
	if (fs.existsSync(file)) {
		load(file);
		return true;
	}

	return false;
}

function sendTemplate(
	request: http.IncomingMessage,
	response: http.ServerResponse,
	browsers: Browsers,
	gui: boolean
): boolean {
	const urlData = (request.url || "").split("/").slice(1);
	const rand = urlData.shift();
	const isRoot = urlData.shift() === "";

	//NOTE: If we setup gui, apply timeout to wait on browser gui start
	const wait = gui ? 1000 : 0;

	//index.html template
	if (urlData.length === 0 && isRoot && rand) {
		setTimeout(() => {
			response.writeHead(200);
			response.write(browsers[rand].template);
			response.end();
		}, wait);
		return true;
	}
	return false;
}

function send404(response: http.ServerResponse): boolean {
	response.writeHead(404);
	response.write("NOT FOUND");
	response.end();
	return true;
}

function send500(response: http.ServerResponse): boolean {
	response.writeHead(500);
	response.write("ERROR");
	response.end();
	return true;
}

function testsServer(listener: http.RequestListener): Promise<[http.Server, number]> {
	return new Promise((resolve, reject) => {
		runServer(defaultPort)
			.then((data) => {
				data[0].on("request", listener);
				resolve(data);
			})
			.catch(reject);
	});
}

function runServer(port: number): Promise<[http.Server, number]> {
	const currentPort = port;

	return new Promise((resolve, reject) => {
		const ports: Array<number> = [];

		for (let i = currentPort; i <= currentPort + tries; i++) {
			ports.push(i);
		}

		findFirst(ports, (port) => {
			return createServer(port);
		})
			.then((data) => {
				resolve(data as [http.Server, number]);
			})
			.catch(() => {
				reject(
					new Error(
						`Can not create test server on port ${currentPort}. Not founded free port between ${port} - ${
							currentPort + tries
						}.`
					)
				);
			});
	});
}

function createServer(port: number): Promise<[http.Server, number]> {
	return new Promise((resolve, reject) => {
		const server = http.createServer().listen(port);

		server.on("listening", () => resolve([server, port]));
		server.on("error", (err) => reject(err));
	});
}

function findFirst<T, R>(values: Array<T>, fn: (port: T) => Promise<R>) {
	return new Promise((resolve, reject) => {
		if (values.length === 0) {
			reject();
			return;
		}

		fn(values[0])
			.then((val) => {
				resolve(val);
			})
			.catch(() => {
				values.shift();
				findFirst(values, fn).then(resolve).catch(reject);
			});
	});
}

//UTILS

function resolveModulePath(module: string): string {
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-ignore
	const requireFn = typeof __webpack_require__ === "function" ? __non_webpack_require__ : require;
	return requireFn.resolve(module);
}

//RESULTS

function returnResult(data: BrowserData, time: [number, number]): TestsProgress {
	let type: "ok" | "warn" | "fail" = "ok";
	const result = data.results;

	//pending or excluded spec, mark as warning
	if (result.summary.pending || result.summary.excluded) {
		type = "warn";
	}
	//not passed => fail
	if (!result.summary.passed) {
		type = "fail";
	}

	//save time
	result.summary.time = process.hrtime(time);
	return {
		type: type,
		browser: data.info,
		...result,
	};
}

function createProgresses(): TestsProgresses {
	return {
		browsers: [],
		devices: {
			invalid: [],
			used: [],
		},
		playwright: false,
		complete: false,
		error: null,
		progresses: {},
	};
}
