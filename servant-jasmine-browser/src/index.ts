import "source-map-support/register";
import { TestsJson } from "@servant/servant-data";
import {
	tests,
	TestsProgress,
	TestsResults,
	TestsProgresses,
	TestsSuit,
	TestsSpec,
	SpecExpectation,
	TestsSpecStatus,
	TestsItemType,
} from "./browser";

export {
	tests,
	TestsResults,
	TestsProgress,
	TestsProgresses,
	TestsSuit,
	TestsSpec,
	TestsItemType,
	TestsSpecStatus,
	SpecExpectation,
};

process.on("message", (mes: Partial<TestsJson.TestsJson>) => {
	const message = TestsJson.remap(mes);

	tests(
		message.cwd,
		message.entry,
		message.externals,
		message.module,
		message.browsers,
		message.devices,
		message.gui,
		onProgress
	).then(onProgress);
});

function onProgress(progresses: TestsProgresses) {
	process.send && process.send(progresses);
}
