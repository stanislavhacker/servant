import { mockFs } from "./mock-fs";
import { GeneratorManifest } from "../src";

export function createEmpty(): () => void {
	return mockFs({
		// /projects/project
		"/projects": {
			project: {},
		},
	});
}

export function createWithManifest(
	manifest1: Partial<GeneratorManifest> | Array<unknown>
): () => void {
	return mockFs({
		// /projects/project
		"/projects": {
			project: {
				generator1: {
					"manifest.json": JSON.stringify(
						Array.isArray(manifest1) ? manifest1 : { ...manifest1, compiled: true }
					),
					"index.js": "", //need to be spied
				},
			},
		},
	});
}

export function createWithManifests(
	manifest1: Partial<GeneratorManifest>,
	manifest2: Partial<GeneratorManifest>
): () => void {
	return mockFs({
		// /projects/project
		"/projects": {
			project: {
				generator1: {
					"manifest.json": JSON.stringify({ ...manifest1, compiled: true }),
					"index.js": "", //need to be spied
				},
				generator2: {
					"manifest.json": JSON.stringify({ ...manifest2, compiled: true }),
					"index.js": "", //need to be spied
				},
			},
		},
	});
}
