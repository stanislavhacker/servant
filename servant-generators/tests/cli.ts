import { getArguments, GeneratorCliArguments } from "../src/cli";

describe("generators cli", () => {
	function normalizePaths(data: GeneratorCliArguments): GeneratorCliArguments {
		return {
			...data,
			output: data.output.replace(/\\/g, "/"),
			dirs: data.dirs.map((path) => path.replace(/\\/g, "/")),
		};
	}

	describe("parsing cli", () => {
		function createProcess(argv: string, env: NodeJS.ProcessEnv = {}): NodeJS.Process {
			const proc = jasmine.createSpyObj<NodeJS.Process>("process", ["cwd"]);

			proc.cwd.and.returnValue("/project/path/to/dir");
			proc.env = env;
			proc.argv = ["node", "index.js"].concat(argv.length ? argv.split(" ") : []);

			return proc;
		}

		it("empty command line", () => {
			const proc = createProcess("");
			const data = getArguments(proc);

			expect(normalizePaths(data)).toEqual({
				output: "/project/path/to/dir/.output",
				dirs: ["/project/path/to/dir", "/project/path/to/dir/node_modules"],
				generator: undefined,
				debug: false,
				help: false,
			});
		});

		it("with defined list of dirs", () => {
			const proc = createProcess("path/dir1 path/dir2");
			const data = getArguments(proc);

			expect(normalizePaths(data)).toEqual({
				output: "/project/path/to/dir/.output",
				dirs: [
					"/project/path/to/dir",
					"path/dir1",
					"path/dir2",
					"/project/path/to/dir/node_modules",
				],
				generator: undefined,
				debug: false,
				help: false,
			});
		});

		it("with defined list of dirs comma separated", () => {
			const proc = createProcess("path/dir1,path/dir2");
			const data = getArguments(proc);

			expect(normalizePaths(data)).toEqual({
				output: "/project/path/to/dir/.output",
				dirs: [
					"/project/path/to/dir",
					"path/dir1",
					"path/dir2",
					"/project/path/to/dir/node_modules",
				],
				generator: undefined,
				debug: false,
				help: false,
			});
		});

		it("with defined list of dirs by flag", () => {
			const proc = createProcess("--dir=path/dir1 --dir=path/dir2");
			const data = getArguments(proc);

			expect(normalizePaths(data)).toEqual({
				output: "/project/path/to/dir/.output",
				dirs: [
					"path/dir1",
					"path/dir2",
					"/project/path/to/dir",
					"/project/path/to/dir/node_modules",
				],
				generator: undefined,
				debug: false,
				help: false,
			});
		});

		it("with defined list of dirs by flag comma separated", () => {
			const proc = createProcess("--dir=path/dir1,path/dir2");
			const data = getArguments(proc);

			expect(normalizePaths(data)).toEqual({
				output: "/project/path/to/dir/.output",
				dirs: [
					"path/dir1",
					"path/dir2",
					"/project/path/to/dir",
					"/project/path/to/dir/node_modules",
				],
				generator: undefined,
				debug: false,
				help: false,
			});
		});

		it("with defined generator", () => {
			const proc = createProcess("--generator test");
			const data = getArguments(proc);

			expect(normalizePaths(data)).toEqual({
				output: "/project/path/to/dir/.output",
				dirs: ["/project/path/to/dir", "/project/path/to/dir/node_modules"],
				generator: "test",
				debug: false,
				help: false,
			});
		});

		it("with defined debug", () => {
			const proc = createProcess("--debug");
			const data = getArguments(proc);

			expect(normalizePaths(data)).toEqual({
				output: "/project/path/to/dir/.output",
				dirs: ["/project/path/to/dir", "/project/path/to/dir/node_modules"],
				generator: undefined,
				debug: true,
				help: false,
			});
		});

		it("with defined help", () => {
			const proc = createProcess("--help");
			const data = getArguments(proc);

			expect(normalizePaths(data)).toEqual({
				output: "/project/path/to/dir/.output",
				dirs: ["/project/path/to/dir", "/project/path/to/dir/node_modules"],
				generator: undefined,
				debug: false,
				help: true,
			});
		});
	});
});
