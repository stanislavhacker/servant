/* eslint-disable @typescript-eslint/no-explicit-any */
import {
	AnswerType,
	GeneratorAnswer,
	GeneratorEmptyResults,
	GeneratorManifest,
	GeneratorPreparedQuestions,
	GeneratorQuestion,
} from "../../src";
import {
	getAnswer,
	createAnswer,
	getCustomData,
	getQuestion,
	extractPreparedQuestions,
	hasAnswer,
	evaluateMessages,
	getProp,
} from "../../src/form";
import { addAnswer } from "../../src/form/answers";
import { meetCondition } from "../../src/form/condition";
import { updateCustomData } from "../../src/form/customData";
import { addQuestion } from "../../src/form/questions";
import { convertFromDefined } from "../../src/form/messages";

describe("form", () => {
	describe("answers", () => {
		let answers;

		beforeEach(() => {
			answers = [
				createAnswer("type", "Variable"),
				createAnswer("name", "Foo"),
				createAnswer("value", "Bar"),
			];
		});

		it("should get answer", () => {
			expect(getAnswer(answers, "type")).toEqual({ id: "type", value: "Variable" });
		});

		it("should not get answer", () => {
			expect(getAnswer(answers, "test")).toEqual(null);
		});

		it("create answer", () => {
			expect(createAnswer("id", "value")).toEqual({ id: "id", value: "value" });
		});

		it("add answer", () => {
			expect(answers.length).toEqual(3);
			const answer = addAnswer(answers, createAnswer("id", "value"));
			expect(answer).toEqual({ id: "id", value: "value" });
			expect(answers.length).toEqual(4);
		});
	});

	describe("questions", () => {
		let questions: GeneratorQuestion[] = [];

		beforeEach(() => {
			questions = [
				{ id: "type", question: "What type?", type: "string" },
				{ id: "version", question: "What version?", type: "string" },
				{ id: "name", question: "What name?", type: "string" },
				{ id: "entry", question: "What entry?", type: "string" },
				{ id: "license", question: "What license?", type: "string" },
			];
		});

		it("get question by id", () => {
			expect(getQuestion(questions, "type")).toEqual({
				id: "type",
				question: "What type?",
				type: "string",
			});
		});

		it("get question by non existing id", () => {
			expect(getQuestion(questions, "test")).toEqual(null);
		});

		it("has answer, determiner as no", () => {
			const answers: GeneratorAnswer[] = [];
			const type = hasAnswer(questions, answers, "type");
			expect(type).toEqual(AnswerType.No);
		});

		it("has answer, determiner as yes", () => {
			const answers: GeneratorAnswer[] = [createAnswer("type", "Variable")];
			const type = hasAnswer(questions, answers, "type");
			expect(type).toEqual(AnswerType.Yes);
		});

		it("has answer, determiner as skip", () => {
			questions.push({
				id: "boilerplate",
				question: "What boilerplate?",
				type: "string",
				condition: {
					and: { type: "variable" },
				},
			});
			const answers: GeneratorAnswer[] = [];
			const type = hasAnswer(questions, answers, "boilerplate");
			expect(type).toEqual(AnswerType.Skip);
		});

		it("has answer, determiner condition as no", () => {
			questions.push({
				id: "boilerplate",
				question: "What boilerplate?",
				type: "string",
				condition: {
					and: { type: "variable" },
				},
			});
			const answers: GeneratorAnswer[] = [createAnswer("type", "variable")];
			const type = hasAnswer(questions, answers, "boilerplate");
			expect(type).toEqual(AnswerType.No);
		});
	});

	describe("questions", () => {
		let questions: GeneratorPreparedQuestions = [];

		beforeEach(() => {
			questions = [{ id: "type", question: "What type?", type: "string", prepared: true }];
		});

		it("add question", () => {
			expect(questions.length).toBe(1);
			const question = addQuestion(questions, {
				id: "test",
				question: "What test?",
				type: "string",
			});
			expect(questions.length).toBe(2);
			expect(question).toEqual({
				id: "test",
				question: "What test?",
				type: "string",
				prepared: true,
				generator: undefined,
			});
		});

		it("add question with generator", () => {
			expect(questions.length).toBe(1);
			const question = addQuestion(
				questions,
				{ id: "test", question: "What test?", type: "string" },
				"gen"
			);
			expect(questions.length).toBe(2);
			expect(question).toEqual({
				id: "test",
				question: "What test?",
				type: "string",
				prepared: true,
				generator: "gen",
			});
		});

		it("extract prepared for next generator", () => {
			questions = [{ id: "type", question: "What type?", type: "string", prepared: true }];

			const { remainingQuestions, generatorQuestions } = extractPreparedQuestions(
				questions,
				"generator"
			);
			expect(remainingQuestions).toEqual([]);
			expect(generatorQuestions).toEqual([
				{ id: "type", question: "What type?", type: "string" },
			]);
		});

		it("extract prepared for another generator", () => {
			questions = [
				{
					id: "type",
					question: "What type?",
					type: "string",
					prepared: true,
					generator: "TEST",
				},
				{ id: "name", question: "What name?", type: "string", prepared: true },
			];

			const { remainingQuestions, generatorQuestions } = extractPreparedQuestions(
				questions,
				"generator"
			);
			expect(remainingQuestions).toEqual([
				{
					id: "type",
					question: "What type?",
					type: "string",
					prepared: true,
					generator: "TEST",
				},
			]);
			expect(generatorQuestions).toEqual([
				{ id: "name", question: "What name?", type: "string" },
			]);
		});

		it("extract prepared for me generator", () => {
			questions = [
				{
					id: "type",
					question: "What type?",
					type: "string",
					prepared: true,
					generator: "generator",
				},
				{ id: "name", question: "What name?", type: "string", prepared: true },
			];

			const { remainingQuestions, generatorQuestions } = extractPreparedQuestions(
				questions,
				"generator"
			);
			expect(remainingQuestions).toEqual([]);
			expect(generatorQuestions).toEqual([
				{ id: "type", question: "What type?", type: "string" },
				{ id: "name", question: "What name?", type: "string" },
			]);
		});
	});

	describe("conditions", () => {
		let questions: GeneratorQuestion[] = [];

		beforeEach(() => {
			questions = [
				{ id: "type", question: "What type?", type: "string" },
				{ id: "version", question: "What version?", type: "string" },
				{ id: "name", question: "What name?", type: "string" },
				{ id: "entry", question: "What entry?", type: "string" },
				{ id: "license", question: "What license?", type: "string" },
				{ id: "target", question: "What target?", type: "string" },
			];
		});

		it("empty, match always", () => {
			const answers: GeneratorAnswer[] = [];
			expect(meetCondition(questions, answers)).toEqual(true);
		});

		it("empty object, match always", () => {
			const answers: GeneratorAnswer[] = [];
			expect(meetCondition(questions, answers, {})).toEqual(true);
		});

		it("defined AND condition, but no answers, do not match", () => {
			const answers: GeneratorAnswer[] = [];
			expect(
				meetCondition(questions, answers, {
					and: { type: "variable" },
				})
			).toEqual(false);
		});

		it("defined OR condition, but no answers, do not match", () => {
			const answers: GeneratorAnswer[] = [];
			expect(
				meetCondition(questions, answers, {
					and: { type: "variable" },
				})
			).toEqual(false);
		});

		it("defined AND and filled both, match", () => {
			const answers: GeneratorAnswer[] = [
				{ id: "type", value: "variable" },
				{ id: "entry", value: "true" },
			];
			expect(
				meetCondition(questions, answers, {
					and: {
						type: "variable",
						entry: "true",
					},
				})
			).toEqual(true);
		});

		it("defined AND and filled one, do not match", () => {
			const answers: GeneratorAnswer[] = [{ id: "type", value: "variable" }];
			expect(
				meetCondition(questions, answers, {
					and: {
						type: "variable",
						entry: "true",
					},
				})
			).toEqual(false);
		});

		it("defined OR and filled both, match", () => {
			const answers: GeneratorAnswer[] = [
				{ id: "type", value: "variable" },
				{ id: "entry", value: "true" },
			];
			expect(
				meetCondition(questions, answers, {
					or: {
						type: "variable",
						entry: "true",
					},
				})
			).toEqual(true);
		});

		it("defined OR and filled one, match", () => {
			const answers: GeneratorAnswer[] = [{ id: "type", value: "variable" }];
			expect(
				meetCondition(questions, answers, {
					or: {
						type: "variable",
						entry: "true",
					},
				})
			).toEqual(true);
		});

		it("defined OR and AND and filled all, match", () => {
			const answers: GeneratorAnswer[] = [
				{ id: "type", value: "variable" },
				{ id: "entry", value: "true" },
				{ id: "license", value: "MIT" },
			];
			expect(
				meetCondition(questions, answers, {
					and: {
						type: "variable",
						entry: "true",
					},
					or: {
						license: "MIT",
					},
				})
			).toEqual(true);
		});

		it("defined OR and AND and filled all, both need to match (global AND apply), match", () => {
			const answers: GeneratorAnswer[] = [
				{ id: "type", value: "variable" },
				{ id: "entry", value: "true" },
				{ id: "license", value: "MIT" },
			];
			expect(
				meetCondition(questions, answers, {
					and: {
						type: "variable",
						entry: "true",
					},
					or: {
						license: "MIT",
					},
				})
			).toEqual(true);
		});

		it("defined OR and AND and filled one, both need to match (global AND apply), do not match", () => {
			const answers: GeneratorAnswer[] = [
				{ id: "type", value: "variable" },
				{ id: "entry", value: "true" },
				{ id: "license", value: "test" },
			];
			expect(
				meetCondition(questions, answers, {
					and: {
						type: "variable",
						entry: "true",
					},
					or: {
						license: "MIT",
					},
				})
			).toEqual(false);
		});

		it("defined complex condition, OR and 2 ANDS, first ok", () => {
			const answers: GeneratorAnswer[] = [
				{ id: "target", value: "web" },
				{ id: "entry", value: "true" },
			];
			expect(
				meetCondition(questions, answers, {
					or: {
						web_target: {
							and: {
								target: "web",
								entry: "true",
							},
						},
						web_page_target: {
							and: {
								target: "web_page",
								entry: "true",
							},
						},
					},
				})
			).toEqual(true);
		});

		it("defined complex condition, OR and 2 ANDS, second ok", () => {
			const answers: GeneratorAnswer[] = [
				{ id: "target", value: "web_page" },
				{ id: "entry", value: "true" },
			];
			expect(
				meetCondition(questions, answers, {
					or: {
						web_target: {
							and: {
								target: "web",
								entry: "true",
							},
						},
						web_page_target: {
							and: {
								target: "web_page",
								entry: "true",
							},
						},
					},
				})
			).toEqual(true);
		});

		it("defined complex condition, OR and 2 ANDS, not true", () => {
			const answers: GeneratorAnswer[] = [
				{ id: "target", value: "node" },
				{ id: "entry", value: "true" },
			];
			expect(
				meetCondition(questions, answers, {
					or: {
						web_target: {
							and: {
								target: "web",
								entry: "true",
							},
						},
						web_page_target: {
							and: {
								target: "web_page",
								entry: "true",
							},
						},
					},
				})
			).toEqual(false);
		});
	});

	describe("custom data", () => {
		it("should get custom data", () => {
			const customData = { a: true };
			const x = getCustomData({ ...GeneratorEmptyResults, customData });
			expect(x).toEqual(customData);
		});

		it("should update custom data", () => {
			const customData = { a: true };
			const customDataNew = { b: true };
			const x = updateCustomData(
				{ ...GeneratorEmptyResults, customData },
				customDataNew as any
			);
			expect(x).toEqual({ ...customData, ...customDataNew });
		});
	});

	describe("messages", () => {
		const manifest: GeneratorManifest = {
			engine: "servant-generator",
			name: "Test",
			use: [],
			entry: "test.js",
			path: "",
			description: "",
		};
		let answers;
		let questions: GeneratorQuestion[] = [];

		beforeEach(() => {
			answers = [
				createAnswer("type", "Variable"),
				createAnswer("name", "Foo"),
				createAnswer("license", "MIT"),
			];
			questions = [
				{ id: "type", question: "What type?", type: "string" },
				{ id: "version", question: "What version?", type: "string" },
				{ id: "name", question: "What name?", type: "string" },
				{ id: "entry", question: "What entry?", type: "string" },
				{ id: "license", question: "What license?", type: "string" },
			];
		});

		it("convert from defined", () => {
			const converted = convertFromDefined(
				[{ id: "msg1", type: "warning", text: "Message" }],
				manifest
			);

			expect(converted).toEqual([
				{ id: "msg1", type: "warning", text: "Message", created: manifest, updated: [] },
			]);
		});

		it("no condition", () => {
			const messages = evaluateMessages(
				questions,
				answers,
				convertFromDefined([{ id: "msg1", type: "warning", text: "Message" }], manifest)
			);

			expect(messages).toEqual([
				{ id: "msg1", type: "warning", text: "Message", created: manifest, updated: [] },
			]);
		});

		it("matched condition", () => {
			const condition = {
				and: {
					type: "Variable",
					name: "Foo",
				},
			};
			const messages = evaluateMessages(
				questions,
				answers,
				convertFromDefined(
					[{ id: "msg1", type: "warning", text: "Message", condition }],
					manifest
				)
			);

			expect(messages).toEqual([
				{
					id: "msg1",
					type: "warning",
					text: "Message",
					condition,
					created: manifest,
					updated: [],
				},
			]);
		});

		it("not matched condition", () => {
			const condition = {
				and: {
					type: "Variable",
					name: "xxxxxx",
				},
			};
			const messages = evaluateMessages(
				questions,
				answers,
				convertFromDefined(
					[{ id: "msg1", type: "warning", text: "Message", condition }],
					manifest
				)
			);

			expect(messages).toEqual([]);
		});
	});

	describe("props", () => {
		it("get prop from object", () => {
			expect(getProp({ a: 1 }, "a")).toEqual(1);
		});
	});
});
