/* eslint-disable @typescript-eslint/no-explicit-any */
import * as path from "path";
import { runner, multiRunner } from "../../src/runner";
import {
	Generator,
	GeneratorConfig,
	GeneratorEmptyResults,
	GeneratorInput,
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorResults,
} from "../../src";

describe("runner", () => {
	const manifest: GeneratorManifest = {
		engine: "servant-generator",
		name: "Manifest",
		description: "Description",
		entry: "index.js",
		use: [],
		path: "",
	};
	const props = { a: true, valid: true };
	let generator;

	describe("check simple runner", () => {
		beforeEach(() => {
			generator = jasmine.createSpy("generator").and.callFake((data, fn, next) => {
				next("success");
			});
		});

		it("simple run generator", async () => {
			const results = await runner(
				GeneratorEmptyResults,
				{ manifest, answers: [], questions: [], messages: [] },
				generator,
				props
			);

			expect(generator).toHaveBeenCalled();
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [],
				answers: [],
				messages: [],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("simple run generator with output", async () => {
			const results = await runner(
				{ ...GeneratorEmptyResults, output: "/path/to/out" },
				{ manifest, answers: [], questions: [], messages: [] },
				generator,
				props
			);

			expect(generator).toHaveBeenCalled();
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [],
				answers: [],
				messages: [],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: undefined,
				output: "/path/to/out",
			});
		});

		it("simple run generator with previous data", async () => {
			const previous: GeneratorResults<unknown> = {
				...GeneratorEmptyResults,
				preparedQuestions: [
					{ prepared: true, id: "id1", question: "Question 1?", type: "string" },
				],
				questions: [{ id: "id2", question: "Question 2?", type: "string" }],
				answers: [{ id: "id2", value: "Value1" }],
				messages: [
					{
						id: "msg1",
						type: "warning",
						text: "Message1",
						created: manifest,
						updated: [],
					},
				],
				errors: [new Error("Error1")],
				directories: [{ relativePath: "./test", created: manifest }],
				files: [
					{
						relativePath: "./test/data.json",
						content: "Test",
						created: manifest,
						updated: [],
					},
				],
				status: "error",
			};
			const results = await runner(
				previous,
				{ manifest, answers: [], questions: [], messages: [] },
				generator,
				props
			);

			expect(generator).toHaveBeenCalled();
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [
					{ id: "id2", question: "Question 2?", type: "string" },
					{ id: "id1", question: "Question 1?", type: "string" },
				],
				answers: [{ id: "id2", value: "Value1" }],
				messages: [
					{
						id: "msg1",
						type: "warning",
						text: "Message1",
						created: manifest,
						updated: [],
					},
				],
				errors: [new Error("Error1")],
				directories: [{ relativePath: "./test", created: manifest }],
				files: [
					{
						relativePath: "./test/data.json",
						content: "Test",
						created: manifest,
						updated: [],
					},
				],
				status: "error",
				customData: undefined,
				output: undefined,
			});
		});

		it("simple run generator with previous data and input", async () => {
			const previous: GeneratorResults<unknown> = {
				...GeneratorEmptyResults,
				preparedQuestions: [
					{ prepared: true, id: "id1", question: "Question 1?", type: "string" },
				],
				questions: [{ id: "id2", question: "Question 2?", type: "string" }],
				answers: [{ id: "id2", value: "Value1" }],
				messages: [
					{
						id: "msg1",
						type: "warning",
						text: "Message1",
						created: manifest,
						updated: [],
					},
				],
				errors: [new Error("Error1")],
				directories: [{ relativePath: "./test", created: manifest }],
				files: [
					{
						relativePath: "./test/data.json",
						content: "Test",
						created: manifest,
						updated: [],
					},
				],
				status: "error",
			};
			const results = await runner(
				previous,
				{
					manifest,
					answers: [{ id: "id3", value: "Value3" }],
					questions: [{ id: "id3", question: "Question 3?", type: "string" }],
					messages: [
						{
							id: "msg2",
							type: "error",
							text: "Message2",
							created: manifest,
							updated: [],
						},
					],
				},
				generator,
				props
			);

			expect(generator).toHaveBeenCalled();
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [
					{ id: "id2", question: "Question 2?", type: "string" },
					{ id: "id3", question: "Question 3?", type: "string" },
					{ id: "id1", question: "Question 1?", type: "string" },
				],
				answers: [
					{ id: "id2", value: "Value1" },
					{ id: "id3", value: "Value3" },
				],
				messages: [
					{
						id: "msg1",
						type: "warning",
						text: "Message1",
						created: manifest,
						updated: [],
					},
					{ id: "msg2", type: "error", text: "Message2", created: manifest, updated: [] },
				],
				errors: [new Error("Error1")],
				directories: [{ relativePath: "./test", created: manifest }],
				files: [
					{
						relativePath: "./test/data.json",
						content: "Test",
						created: manifest,
						updated: [],
					},
				],
				status: "error",
				customData: undefined,
				output: undefined,
			});
		});

		it("simple run generator with error", async () => {
			//throw
			generator = jasmine.createSpy("generator").and.callFake(() => {
				throw new Error("This is inside error");
			});

			const results = await runner(
				GeneratorEmptyResults,
				{ manifest, answers: [], questions: [], messages: [] },
				generator,
				props
			);

			expect(generator).toHaveBeenCalled();
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [],
				answers: [],
				messages: [],
				errors: [new Error("This is inside error")],
				files: [],
				directories: [],
				status: "error",
				customData: undefined,
				output: undefined,
			});
		});
	});

	describe("check runner functions", () => {
		const input: GeneratorInput = {
			manifest,
			answers: [
				{ id: "name", value: "Standa" },
				{ id: "surname", value: "Hacker" },
			],
			questions: [
				{ id: "name", question: "Whats your name?", type: "string" },
				{ id: "surname", question: "Whats your surname?", type: "string" },
				{ id: "age", question: "Whats your age?", type: "string" },
				{ id: "job", question: "Whats your job?", type: "string" },
			],
			messages: [
				{
					id: "missing_job",
					type: "error",
					text: "You need to fill job. We want to able create category for you.",
					created: manifest,
					updated: [],
					condition: { and: { job: "" } },
				},
				{
					id: "filled",
					type: "success",
					text: "Thanks for filling this generator data.",
					created: manifest,
					updated: [],
				},
			],
		};

		async function run(gen: Generator<unknown, unknown>, expected: GeneratorResults<unknown>) {
			const generator = jasmine.createSpy("generator").and.callFake(gen);
			const results = await runner(GeneratorEmptyResults, input, generator, props);

			expect(generator).toHaveBeenCalled();
			expect(results.results).toEqual(expected);
		}

		it("check data handlers", async () => {
			const gen: Generator<typeof props, unknown> = (data, fn, next) => {
				//props
				expect(data.getProp("valid")).toBe(true);

				expect(data.getAnswer("name")).toEqual({ id: "name", value: "Standa" });
				expect(data.getAnswer("test")).toEqual(null);

				expect(data.getQuestion("name")).toEqual({
					id: "name",
					question: "Whats your name?",
					type: "string",
				});
				expect(data.getQuestion("test")).toEqual(null);

				data.addAnswer({ id: "age", value: "25" });
				data.addQuestion({
					id: "job_type",
					question: "Whats your job type?",
					type: "string",
				});

				expect(data.getAnswer("age")).toEqual({ id: "age", value: "25" });
				//note: questions is added into prepared questions array
				expect(data.getQuestion("job_type")).toEqual(null);

				next("success");
			};

			await run(gen, {
				preparedQuestions: [
					{
						id: "job_type",
						question: "Whats your job type?",
						type: "string",
						generator: undefined,
						prepared: true,
					},
				],
				questions: [...input.questions],
				answers: [...input.answers, { id: "age", value: "25" }],
				messages: [...input.messages],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("check fn handlers for messages", async () => {
			const gen: Generator<typeof props, unknown> = (data, fn, next) => {
				expect(fn.getMessage("missing_job")).toEqual(input.messages[0]);
				expect(fn.getMessage("test")).toEqual(null);

				fn.createMessage("warning", {
					text: "This is warning message",
					title: "Title",
					type: "warning",
				});
				expect(fn.getMessage("warning")).toEqual({
					text: "This is warning message",
					title: "Title",
					type: "warning",
					id: "warning",
					created: manifest,
					updated: [],
				});

				fn.deleteMessage("warning");
				expect(fn.getMessage("warning")).toEqual(null);

				fn.createMessage("warning1", {
					text: "This is warning1 message",
					title: "Title",
					type: "warning",
				});

				next("success");
			};

			await run(gen, {
				preparedQuestions: [],
				questions: [...input.questions],
				answers: [...input.answers],
				messages: [
					...input.messages,
					{
						id: "warning1",
						text: "This is warning1 message",
						title: "Title",
						type: "warning",
						created: manifest,
						updated: [],
					},
				],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("check fn handlers for custom data", async () => {
			const gen: Generator<typeof props, unknown> = (data, fn, next) => {
				expect(fn.getCustomData()).toEqual(undefined);

				fn.updateCustomData({ test: "test" });
				expect(fn.getCustomData()).toEqual({ test: "test" });

				next("success");
			};

			await run(gen, {
				preparedQuestions: [],
				questions: [...input.questions],
				answers: [...input.answers],
				messages: [...input.messages],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: { test: "test" },
				output: undefined,
			});
		});

		it("check fn handlers for files", async () => {
			const gen: Generator<typeof props, unknown> = (data, fn, next) => {
				let file = fn.getFile("./data/data.json");
				expect(file).toEqual(null);

				//create file
				file = fn.createFile("./data/data.json", { content: "test" });
				expect(file).toEqual({
					created: manifest,
					updated: [],
					relativePath: path.normalize("./data/data.json"),
					content: { content: "test" },
				});

				//get file
				file = fn.getFile("./data/data.json");
				expect(file).toEqual({
					created: manifest,
					updated: [],
					relativePath: path.normalize("./data/data.json"),
					content: { content: "test" },
				});

				//exists
				file = fn.createFile("./data/data.json", { content: "test1" });
				expect(file).toEqual({
					created: manifest,
					updated: [],
					relativePath: path.normalize("./data/data.json"),
					content: { content: "test" },
					err: "exists",
				});

				//update
				file = fn.updateFile("./data/data.json", { content: "updated" });
				expect(file).toEqual({
					created: manifest,
					updated: [manifest],
					relativePath: path.normalize("./data/data.json"),
					content: { content: "updated" },
				});

				//not exists for update
				file = fn.updateFile("./data/data1.json", { content: "updated" });
				expect(file).toEqual({
					created: manifest,
					updated: [],
					relativePath: path.normalize("./data/data1.json"),
					content: { content: "updated" },
					err: "not-found",
				});

				//not exists for delete
				file = fn.removeFile("./data/data1.json");
				expect(file).toEqual({
					created: manifest,
					updated: [],
					relativePath: path.normalize("./data/data1.json"),
					content: "",
					err: "not-found",
				});

				//delete
				file = fn.removeFile("./data/data.json");
				expect(file).toEqual({
					created: manifest,
					updated: [manifest],
					relativePath: path.normalize("./data/data.json"),
					content: { content: "updated" },
				});

				//create file
				file = fn.createFile("./data/data1.json", { content: "test" });
				expect(file).toEqual({
					created: manifest,
					updated: [],
					relativePath: path.normalize("./data/data1.json"),
					content: { content: "test" },
				});

				expect(fn.getFiles()).toEqual([
					{
						updated: [],
						relativePath: path.normalize("./data/data1.json"),
						created: manifest,
						content: { content: "test" },
					},
				]);

				next("success");
			};

			await run(gen, {
				preparedQuestions: [],
				questions: [...input.questions],
				answers: [...input.answers],
				messages: [...input.messages],
				errors: [],
				files: [
					{
						updated: [],
						relativePath: path.normalize("./data/data1.json"),
						created: manifest,
						content: { content: "test" },
					},
				],
				directories: [],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("check fn handlers for directories", async () => {
			const gen: Generator<typeof props, unknown> = (data, fn, next) => {
				let directory = fn.getDir("./data");
				expect(directory).toEqual(null);

				//create directory
				directory = fn.createDir("./data");
				expect(directory).toEqual({
					created: manifest,
					relativePath: path.normalize("./data"),
				});

				//exists
				directory = fn.createDir("./data");
				expect(directory).toEqual({
					created: manifest,
					relativePath: path.normalize("./data"),
					err: "exists",
				});

				directory = fn.getDir("./data");
				expect(directory).toEqual({
					created: manifest,
					relativePath: path.normalize("./data"),
				});

				//not exists for delete
				directory = fn.removeDir("./data1");
				expect(directory).toEqual({
					created: manifest,
					relativePath: path.normalize("./data1"),
					err: "not-found",
				});

				//delete
				directory = fn.removeDir("./data");
				expect(directory).toEqual({
					created: manifest,
					relativePath: path.normalize("./data"),
				});

				//create directory
				directory = fn.createDir("./data");
				expect(directory).toEqual({
					created: manifest,
					relativePath: path.normalize("./data"),
				});

				expect(fn.getDirs()).toEqual([
					{ relativePath: path.normalize("./data"), created: manifest },
				]);

				next("success");
			};

			await run(gen, {
				preparedQuestions: [],
				questions: [...input.questions],
				answers: [...input.answers],
				messages: [...input.messages],
				errors: [],
				files: [],
				directories: [{ relativePath: path.normalize("./data"), created: manifest }],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("check fn handlers for output", async () => {
			const gen: Generator<typeof props, unknown> = (data, fn, next) => {
				expect(fn.getOutput()).toEqual(null);

				fn.setOutput("/path/to/output");
				expect(fn.getOutput()).toEqual("/path/to/output");

				next("success");
			};

			await run(gen, {
				preparedQuestions: [],
				questions: [...input.questions],
				answers: [...input.answers],
				messages: [...input.messages],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: undefined,
				output: "/path/to/output",
			});
		});
	});

	describe("check multi runner", () => {
		const manifest1: GeneratorManifest = {
			engine: "servant-generator",
			name: "Manifest 1",
			description: "Description 1",
			entry: "index.js",
			use: ["local:Manifest 2"],
			path: "",
		};
		const manifest2: GeneratorManifest = {
			engine: "servant-generator",
			name: "Manifest 2",
			description: "Description 2",
			entry: "index.js",
			use: [],
			path: "",
		};
		let loaded1, loaded2;
		let generator1, generator2;
		const props = { a: true, valid: true };

		beforeEach(() => {
			generator1 = jasmine.createSpy("generator").and.callFake((data, fn, next) => {
				next("success");
			});

			generator2 = jasmine.createSpy("generator").and.callFake((data, fn, next) => {
				next("success");
			});

			loaded2 = {
				manifest: manifest2,
				config: {},
				generator: generator2,
				use: [],
			} as GeneratorLoaded<any, any>;

			loaded1 = {
				manifest: manifest1,
				config: {},
				generator: generator1,
				use: [loaded2],
			} as GeneratorLoaded<any, any>;
		});

		it("simple run generator", async () => {
			const results = await multiRunner(GeneratorEmptyResults, loaded1, props);

			expect(generator1).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalledBefore(generator1);
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [],
				answers: [],
				messages: [],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("simple run generator with config", async () => {
			const config: Required<GeneratorConfig> = {
				questions: [
					{
						id: "name",
						question: "Whats your name?",
						type: "string",
						category: "person",
					},
					{
						id: "surname",
						question: "Whats your surname?",
						type: "string",
						category: "person",
					},
					{ id: "age", question: "Whats your age?", type: "string" },
					{ id: "city", question: "Whats your city?", type: "string" },
					{ id: "type", question: "Whats your account type?", type: "string" },
				],
				answers: [
					{ id: "name", value: "John" },
					{ id: "surname", value: "Doe" },
					{ id: "age", value: "30" },
					{ id: "city", value: "London" },
				],
				messages: [
					{
						id: "wrong_type",
						type: "error",
						title: "Unknown account type",
						text: "Please fill FREE or PREMIUM",
						condition: { and: { type: "" } },
					},
				],
				categories: {
					person: "red",
				},
			};
			const results = await multiRunner(GeneratorEmptyResults, { ...loaded1, config }, props);

			expect(generator1).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalledBefore(generator1);
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [...config.questions],
				answers: [...config.answers],
				messages: [{ ...config.messages[0], created: manifest1, updated: [] }],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("simple run generator with fill another questions", async () => {
			const impl: Generator<any, any> = (data, fn, next) => {
				data.addQuestion({
					id: "job",
					question: "Whats your job position?",
					type: "string",
					category: "person",
				});
				data.addQuestion({
					id: "pay",
					question: "Whats your job pay?",
					type: "string",
					category: "person",
				});
				next("success");
			};

			generator2 = jasmine.createSpy("generator").and.callFake(impl);
			loaded2.generator = generator2;

			const config: Required<GeneratorConfig> = {
				questions: [
					{
						id: "name",
						question: "Whats your name?",
						type: "string",
						category: "person",
					},
					{
						id: "surname",
						question: "Whats your surname?",
						type: "string",
						category: "person",
					},
					{ id: "age", question: "Whats your age?", type: "string" },
					{ id: "city", question: "Whats your city?", type: "string" },
					{ id: "type", question: "Whats your account type?", type: "string" },
				],
				answers: [
					{ id: "name", value: "John" },
					{ id: "surname", value: "Doe" },
					{ id: "age", value: "30" },
					{ id: "city", value: "London" },
				],
				messages: [
					{
						id: "wrong_type",
						type: "error",
						title: "Unknown account type",
						text: "Please fill FREE or PREMIUM",
						condition: { and: { type: "" } },
					},
				],
				categories: {
					person: "red",
				},
			};
			const results = await multiRunner(GeneratorEmptyResults, { ...loaded1, config }, props);

			expect(generator1).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalledBefore(generator1);
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [
					...config.questions,
					{
						id: "job",
						question: "Whats your job position?",
						type: "string",
						category: "person",
					},
					{
						id: "pay",
						question: "Whats your job pay?",
						type: "string",
						category: "person",
					},
				],
				answers: [...config.answers],
				messages: [{ ...config.messages[0], created: manifest1, updated: [] }],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("simple run generator with fill files", async () => {
			const impl2: Generator<any, any> = (data, fn, next) => {
				fn.createDir("./impl2");
				fn.createFile("./impl2/file2.txt", "file2");
				next("success");
			};

			generator2 = jasmine.createSpy("generator").and.callFake(impl2);
			loaded2.generator = generator2;

			const impl1: Generator<any, any> = (data, fn, next) => {
				fn.createDir("./impl1");
				fn.createFile("./impl1/file1.txt", "file1");
				next("success");
			};

			generator1 = jasmine.createSpy("generator").and.callFake(impl1);
			loaded1.generator = generator1;

			const results = await multiRunner(GeneratorEmptyResults, { ...loaded1 }, props);

			expect(generator1).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalledBefore(generator1);
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [],
				answers: [],
				messages: [],
				errors: [],
				files: [
					{
						updated: [],
						relativePath: path.normalize("./impl2/file2.txt"),
						created: manifest2,
						content: "file2",
					},
					{
						updated: [],
						relativePath: path.normalize("./impl1/file1.txt"),
						created: manifest1,
						content: "file1",
					},
				],
				directories: [
					{ relativePath: path.normalize("./impl2"), created: manifest2 },
					{ relativePath: path.normalize("./impl1"), created: manifest1 },
				],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("simple run generator with update file", async () => {
			const impl2: Generator<any, any> = (data, fn, next) => {
				fn.createFile("./impl/file.txt", "0");
				next("success");
			};

			generator2 = jasmine.createSpy("generator").and.callFake(impl2);
			loaded2.generator = generator2;

			const impl1: Generator<any, any> = (data, fn, next) => {
				fn.updateFile("./impl/file.txt", "1");
				next("success");
			};

			generator1 = jasmine.createSpy("generator").and.callFake(impl1);
			loaded1.generator = generator1;

			const results = await multiRunner(GeneratorEmptyResults, { ...loaded1 }, props);

			expect(generator1).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalledBefore(generator1);
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [],
				answers: [],
				messages: [],
				errors: [],
				files: [
					{
						updated: [manifest1],
						relativePath: path.normalize("./impl/file.txt"),
						created: manifest2,
						content: "1",
					},
				],
				directories: [],
				status: "success",
				customData: undefined,
				output: undefined,
			});
		});

		it("simple run generator with update output", async () => {
			const impl2: Generator<any, any> = (data, fn, next) => {
				fn.setOutput("/path/output2");
				next("success");
			};

			generator2 = jasmine.createSpy("generator").and.callFake(impl2);
			loaded2.generator = generator2;

			const impl1: Generator<any, any> = (data, fn, next) => {
				fn.setOutput("/path/output1");
				next("success");
			};

			generator1 = jasmine.createSpy("generator").and.callFake(impl1);
			loaded1.generator = generator1;

			const results = await multiRunner(
				{ ...GeneratorEmptyResults, output: "/path/default" },
				{ ...loaded1 },
				props
			);

			expect(generator1).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalled();
			expect(generator2).toHaveBeenCalledBefore(generator1);
			expect(results.results).toEqual({
				preparedQuestions: [],
				questions: [],
				answers: [],
				messages: [],
				errors: [],
				files: [],
				directories: [],
				status: "success",
				customData: undefined,
				output: "/path/output1",
			});
		});
	});
});
