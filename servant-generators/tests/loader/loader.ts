/* eslint-disable @typescript-eslint/no-explicit-any */
import { createEmpty, createWithManifest, createWithManifests } from "../projects";
import { loader } from "../../src/loader";
import * as generator from "../../src/loader/require";
import { GeneratorRequired } from "../../src";

describe("loader", () => {
	it("no directories", async () => {
		const clean = await createEmpty();
		const { generators, errors } = await loader([]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toBe(
			"Error: No directories provided for loading generators for. You must entry at least one directory where generators are stored."
		);

		await clean();
	});

	it("no manifests", async () => {
		const clean = await createEmpty();
		const { generators, errors } = await loader(["/projects/project"]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toBe(
			"Error: No generator manifests found in provided directories. You must entry at least one directory where generators are stored."
		);

		await clean();
	});

	it("manifest with invalid object", async () => {
		const clean = await createWithManifest([] as any);
		const { generators, errors } = await loader(["/projects/project"]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toMatch(
			/No generator manifests found in provided directories\. You must entry at least one directory where generators are stored\./g
		);

		await clean();
	});

	it("manifest without any definition", async () => {
		const clean = await createWithManifest({});
		const { generators, errors } = await loader(["/projects/project"]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toMatch(
			/No generator manifests found in provided directories\. You must entry at least one directory where generators are stored\./g
		);

		await clean();
	});

	it("manifest with wrong engine", async () => {
		const clean = await createWithManifest({ engine: "servant" } as any);
		const { generators, errors } = await loader(["/projects/project"]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toMatch(
			/No generator manifests found in provided directories\. You must entry at least one directory where generators are stored\./g
		);

		await clean();
	});

	it("manifest with engine only", async () => {
		const clean = await createWithManifest({ engine: "servant-generator" });
		const { generators, errors } = await loader(["/projects/project"]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toMatch(
			/Error: Manifest file in .*manifest\.json is missing name\./g
		);

		await clean();
	});

	it("manifest with name only", async () => {
		const clean = await createWithManifest({ engine: "servant-generator", name: "Manifest" });
		const { generators, errors } = await loader(["/projects/project"]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toMatch(
			/Error: Manifest file in .*manifest\.json is missing description\./g
		);

		await clean();
	});

	it("manifest with name and description but no entry", async () => {
		const clean = await createWithManifest({
			engine: "servant-generator",
			name: "Manifest",
			description: "Description",
		});
		const { generators, errors } = await loader(["/projects/project"]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toMatch(/Error: Cannot find module/g);

		await clean();
	});

	it("manifest with name and description and empty entry object", async () => {
		spyOn(generator, "requireGenerator").and.returnValue({} as GeneratorRequired);

		const clean = await createWithManifest({ name: "Manifest", description: "Description" });
		const { generators, errors } = await loader(["/projects/project"]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toMatch(
			/No generator manifests found in provided directories\. You must entry at least one directory where generators are stored\./g
		);

		await clean();
	});

	it("manifest with name and description and object with config", async () => {
		spyOn(generator, "requireGenerator").and.returnValue({
			config: {},
		} as GeneratorRequired);

		const clean = await createWithManifest({
			engine: "servant-generator",
			name: "Manifest",
			description: "Description",
		});
		const { generators, errors } = await loader(["/projects/project"]);

		expect(generators.length).toBe(0);
		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toMatch(
			/Error: Generator "Manifest" in ".*manifest\.json" does not have a "generator" function\./g
		);

		await clean();
	});

	it("manifest with name and description and object with config and generator", async () => {
		spyOn(generator, "requireGenerator").and.returnValue({
			config: {},
			generator: (data, fn, next) => {
				next("success");
			},
		} as GeneratorRequired);

		const clean = await createWithManifest({
			engine: "servant-generator",
			name: "Manifest",
			description: "Description",
		});
		const { generators, errors } = await loader(["/projects/project"]);

		expect(errors.length).toBe(0);
		expect(generators.length).toBe(1);

		expect(generators[0].use.length).toBe(0);
		expect(generators[0].manifest).toEqual({
			engine: "servant-generator",
			name: "Manifest",
			description: "Description",
			path: jasmine.anything(),
			compiled: true,
		});
		expect(generators[0].config).toEqual({});
		expect(generators[0].generator).toBeDefined();

		await clean();
	});

	it("more generator manifests", async () => {
		spyOn(generator, "requireGenerator").and.returnValue({
			config: {},
			generator: (data, fn, next) => {
				next("success");
			},
		} as GeneratorRequired);

		const clean = await createWithManifests(
			{ engine: "servant-generator", name: "Manifest1", description: "Description1" },
			{ engine: "servant-generator", name: "Manifest2", description: "Description2" }
		);
		const { generators, errors } = await loader(["/projects/project"]);

		expect(errors.length).toBe(0);
		expect(generators.length).toBe(2);

		expect(generators[0].use.length).toBe(0);
		expect(generators[0].manifest).toEqual({
			engine: "servant-generator",
			name: "Manifest1",
			description: "Description1",
			path: jasmine.anything(),
			compiled: true,
		});
		expect(generators[0].config).toEqual({});
		expect(generators[0].generator).toBeDefined();

		expect(generators[1].use.length).toBe(0);
		expect(generators[1].manifest).toEqual({
			engine: "servant-generator",
			name: "Manifest2",
			description: "Description2",
			path: jasmine.anything(),
			compiled: true,
		});
		expect(generators[1].config).toEqual({});
		expect(generators[1].generator).toBeDefined();

		await clean();
	});

	it("more generator manifests connected together", async () => {
		spyOn(generator, "requireGenerator").and.returnValue({
			config: {},
			generator: (data, fn, next) => {
				next("success");
			},
		} as GeneratorRequired);

		const clean = await createWithManifests(
			{ engine: "servant-generator", name: "Manifest1", description: "Description1" },
			{
				engine: "servant-generator",
				name: "Manifest2",
				description: "Description2",
				use: ["local:Manifest1"],
			}
		);
		const { generators, errors } = await loader(["/projects/project"]);

		expect(errors.length).toBe(0);
		expect(generators.length).toBe(2);

		expect(generators[0].use.length).toBe(0);
		expect(generators[0].manifest).toEqual({
			engine: "servant-generator",
			name: "Manifest1",
			description: "Description1",
			path: jasmine.anything(),
			compiled: true,
		});
		expect(generators[0].config).toEqual({});
		expect(generators[0].generator).toBeDefined();

		expect(generators[1].use.length).toBe(1);
		expect(generators[1].use[0].manifest).toEqual({
			engine: "servant-generator",
			name: "Manifest1",
			description: "Description1",
			path: jasmine.anything(),
			compiled: true,
		});
		expect(generators[1].manifest).toEqual({
			engine: "servant-generator",
			name: "Manifest2",
			description: "Description2",
			use: ["local:Manifest1"],
			path: jasmine.anything(),
			compiled: true,
		});
		expect(generators[1].config).toEqual({});
		expect(generators[1].generator).toBeDefined();

		await clean();
	});

	it("more generator manifests connected together invalid", async () => {
		spyOn(generator, "requireGenerator").and.returnValue({
			config: {},
			generator: (data, fn, next) => {
				next("success");
			},
		} as GeneratorRequired);

		const clean = await createWithManifests(
			{ engine: "servant-generator", name: "Manifest1", description: "Description1" },
			{
				engine: "servant-generator",
				name: "Manifest2",
				description: "Description2",
				use: ["local:Manifest3"],
			}
		);
		const { generators, errors } = await loader(["/projects/project"]);

		expect(errors.length).toBe(1);
		expect(errors[0].toString()).toMatch(
			/Error: Generator Manifest2 uses "local:Manifest3" but it is not loaded correctly\./g
		);
		expect(generators.length).toBe(2);

		await clean();
	});
});
