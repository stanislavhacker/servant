export function clearOutput(text = ""): Array<string> {
	return text
		.replace(
			// eslint-disable-next-line no-control-regex
			/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g,
			""
		)
		.split("\n")
		.map((line) => line.trimEnd());
}
