/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import * as React from "react";
import { render } from "ink-testing-library";

import { clearOutput } from "./utils";

import { createEmpty } from "../projects";
import { Errors } from "../../src/view/Errors";

describe("Errors component", () => {
	let clean;

	beforeEach(() => {
		clean = createEmpty();
	});

	it("no errors at all", () => {
		const { lastFrame } = render(<Errors errors={[]} debug={false} />);
		expect(clearOutput(lastFrame())).toEqual([""]);
	});

	it("2 errors", () => {
		const { lastFrame } = render(
			<Errors errors={[new Error("error 1"), new Error("error 2")]} debug={false} />
		);
		expect(clearOutput(lastFrame())).toEqual([
			"Some errors occurred when running generator:",
			"  Error error 1",
			"",
			"  Error error 2",
			"",
		]);
	});

	afterEach(() => {
		clean();
	});
});
