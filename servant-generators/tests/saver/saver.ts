/* eslint-disable @typescript-eslint/no-explicit-any */
import * as path from "path";
import * as fs from "fs";

import { saver } from "../../src/saver";
import { createEmpty, createWithManifest } from "../projects";
import {
	GeneratorDir,
	GeneratorFile,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorResults,
} from "../../src";

async function getFiles(dir) {
	const items = await new Promise<string[]>((resolve, reject) =>
		fs.readdir(dir, (err, files) => (err ? reject(err) : resolve(files)))
	);
	const files = await Promise.all(
		items.map((item) => {
			const res = path.resolve(dir, item);
			const stat = fs.statSync(res);
			if (stat.isDirectory()) {
				return getFiles(res).then((files) => [res, ...files]);
			}
			return Promise.resolve(res);
		})
	);
	return Array.prototype.concat(...files);
}

describe("saver", () => {
	const where = "/projects/project";
	const invalidWhere = "/projects/project/generator1/manifest.json";
	const manifest: GeneratorManifest = {
		engine: "servant-generator",
		name: "Manifest",
		description: "Description",
		entry: "index.js",
		use: [],
		path: "",
	};

	function createResults(
		files: GeneratorFile[],
		directories: GeneratorDir[],
		messages: GeneratorMessage[],
		errors: Error[] = [],
		status: GeneratorResults<any>["status"] = "success"
	): GeneratorResults<any> {
		return {
			files,
			directories,
			errors,
			status,
			messages,
			//not used in saver at all
			questions: [],
			answers: [],
			customData: undefined,
			preparedQuestions: [],
		};
	}

	it("save empty at all", async () => {
		const clean = await createEmpty();
		const res = createResults([], [], []);

		const { results, fn } = await saver(where, res);

		expect(results).toEqual({
			questions: res.questions,
			answers: res.answers,
			status: "success",
			messages: [],
			errors: [],
			paths: [],
			files: [],
			directories: [],
		});
		expect(fn.getFile).toBeDefined();
		expect(fn.getDir).toBeDefined();

		const files = await getFiles(where);
		expect(files).toEqual([]);

		await clean();
	});

	it("save files and directory without error", async () => {
		const clean = await createEmpty();
		const res = createResults(
			[
				{
					created: manifest,
					updated: [],
					relativePath: "./tests/index.txt",
					content: "test",
				},
			],
			[{ created: manifest, relativePath: "./content" }],
			[]
		);

		const { results, fn } = await saver(where, res);

		expect(results).toEqual({
			questions: res.questions,
			answers: res.answers,
			status: "success",
			messages: [],
			errors: [],
			paths: [
				path.normalize("/projects/project/content"),
				path.normalize("/projects/project/tests/index.txt"),
			],
			files: [
				{
					...res.files[0],
					absolutePath: path.normalize("/projects/project/tests/index.txt"),
				},
			],
			directories: [
				{
					...res.directories[0],
					absolutePath: path.normalize("/projects/project/content"),
				},
			],
		});
		expect(fn.getFile).toBeDefined();
		expect(fn.getDir).toBeDefined();

		const files = await getFiles(where);
		expect(files).toEqual([
			path.resolve("/projects/project/content"),
			path.resolve("/projects/project/tests"),
			path.resolve("/projects/project/tests/index.txt"),
		]);

		await clean();
	});

	it("save files and directory with 'Some error'", async () => {
		const clean = await createEmpty();
		const res = createResults(
			[
				{
					created: manifest,
					updated: [],
					relativePath: "./tests/index.txt",
					content: "test",
				},
			],
			[{ created: manifest, relativePath: "./content" }],
			[],
			[new Error("Some error.")]
		);

		const { results, fn } = await saver(where, res);

		expect(results).toEqual({
			questions: res.questions,
			answers: res.answers,
			status: "error",
			messages: [],
			errors: [new Error("Some error.")],
			paths: [],
			files: [],
			directories: [],
		});
		expect(fn.getFile).toBeDefined();
		expect(fn.getDir).toBeDefined();

		const files = await getFiles(where);
		expect(files).toEqual([]);

		await clean();
	});

	it("save files and directory with error status", async () => {
		const clean = await createEmpty();
		const res = createResults(
			[
				{
					created: manifest,
					updated: [],
					relativePath: "./tests/index.txt",
					content: "test",
				},
			],
			[{ created: manifest, relativePath: "./content" }],
			[],
			[],
			"error"
		);

		const { results, fn } = await saver(where, res);

		expect(results).toEqual({
			questions: res.questions,
			answers: res.answers,
			status: "error",
			messages: [],
			errors: [
				new Error(
					'Generator failed to save files into folder, because results ends with "error" status.'
				),
			],
			paths: [],
			files: [],
			directories: [],
		});
		expect(fn.getFile).toBeDefined();
		expect(fn.getDir).toBeDefined();

		const files = await getFiles(where);
		expect(files).toEqual([]);

		await clean();
	});

	it("save files and directory with non existing into", async () => {
		const clean = await createWithManifest({});
		const res = createResults(
			[
				{
					created: manifest,
					updated: [],
					relativePath: "./tests/index.txt",
					content: "test",
				},
			],
			[{ created: manifest, relativePath: "./content" }],
			[]
		);

		const { results, fn } = await saver(invalidWhere, res);

		expect(results).toEqual({
			questions: res.questions,
			answers: res.answers,
			status: "error",
			messages: [],
			errors: [
				new Error('Path "/projects/project/generator1/manifest.json" is not a directory.'),
			],
			paths: [],
			files: [],
			directories: [],
		});
		expect(fn.getFile).toBeDefined();
		expect(fn.getDir).toBeDefined();

		await clean();
	});

	it("save empty but convert messages", async () => {
		const clean = await createEmpty();
		const res = createResults(
			[],
			[],
			[
				{
					id: "msg2",
					title: "Title 2",
					text: "Text 2",
					type: "error",
					created: manifest,
					updated: [],
					condition: { and: { type: "FREE" } },
				},
				{
					id: "msg1",
					title: "Title 1",
					text: "Text 1",
					type: "error",
					created: manifest,
					updated: [],
				},
			]
		);

		const { results, fn } = await saver(where, res);

		expect(results).toEqual({
			questions: res.questions,
			answers: res.answers,
			status: "success",
			messages: [res.messages[1]],
			errors: [],
			paths: [],
			files: [],
			directories: [],
		});
		expect(fn.getFile).toBeDefined();
		expect(fn.getDir).toBeDefined();

		const files = await getFiles(where);
		expect(files).toEqual([]);

		await clean();
	});

	it("save files and directory, check getFile handler", async () => {
		const clean = await createEmpty();
		const res = createResults(
			[
				{
					created: manifest,
					updated: [],
					relativePath: "./tests/index.txt",
					content: "test",
				},
			],
			[{ created: manifest, relativePath: "./content" }],
			[]
		);

		const { fn } = await saver(where, res);

		let file = fn.getFile("./tests/index.txt");
		expect(file).toEqual({
			created: manifest,
			updated: [],
			relativePath: "./tests/index.txt",
			content: "test",
			absolutePath: path.normalize("/projects/project/tests/index.txt"),
		});

		file = fn.getFile("./tests/index1.txt");
		expect(file).toBeNull();

		await clean();
	});

	it("save files and directory, check getDir handler", async () => {
		const clean = await createEmpty();
		const res = createResults(
			[
				{
					created: manifest,
					updated: [],
					relativePath: "./tests/index.txt",
					content: "test",
				},
			],
			[{ created: manifest, relativePath: "./content" }],
			[]
		);

		const { fn } = await saver(where, res);

		let dir = fn.getDir("./content");
		expect(dir).toEqual({
			created: manifest,
			relativePath: "./content",
			absolutePath: path.normalize("/projects/project/content"),
		});

		dir = fn.getDir("./content1");
		expect(dir).toBeNull();

		await clean();
	});
});
