import * as React from "react";
import { useState } from "react";
import { Box } from "ink";

import {
	GeneratorLoaded,
	GeneratorResults,
	GeneratorSaveResults,
	GeneratorPreparedQuestions,
} from "../types";
import { GeneratorEmptyResults } from "../constants";
import { Help } from "../help";

import { useGeneratorsList } from "./hooks/useGeneratorsList";
import { usePreselectedGenerator } from "./hooks/usePreselectedGenerator";
import { Generators } from "./Generators";
import { Generator } from "./Generator";
import { Generated } from "./Generated";
import { useSelectGenerator } from "./hooks/useSelectGenerator";

export type MainProps<T> = {
	dirs: string[];
	output?: string;
	generator?: string;
	debug?: boolean;
	help?: boolean;
	props?: T;
	onDone?: (results: GeneratorSaveResults) => void;
	onErrors?: (errors: Error[]) => void;
	preparedQuestions?: GeneratorPreparedQuestions;
};
export function Main<T, D>(props: MainProps<T>) {
	if (props.help) {
		return <Help />;
	}
	return <ListGenerators<T, D> {...props} />;
}

function ListGenerators<T, D>({
	dirs,
	generator,
	output,
	debug,
	props,
	onDone,
	onErrors,
	preparedQuestions = [],
}: MainProps<T>) {
	const [results, setResults] = useState<GeneratorResults<D> | null>(null);

	const { selectedGenerator, onSelect } = useSelectGenerator(generator);
	const { status, list, directories } = useGeneratorsList(dirs);
	const loadedGenerator = usePreselectedGenerator(list, selectedGenerator);

	if (loadedGenerator) {
		return (
			<>
				<LoadedGenerator<T, D>
					columns={[]}
					results={
						{
							...GeneratorEmptyResults,
							preparedQuestions,
							output,
						} as GeneratorResults<D>
					}
					loaded={loadedGenerator}
					props={props}
					onDone={(res) => setResults(res)}
				/>
				{results && (
					<Box marginTop={1}>
						<Generated
							debug={debug}
							loaded={loadedGenerator}
							results={results}
							onDone={onDone}
						/>
					</Box>
				)}
			</>
		);
	}

	return (
		<Generators
			directories={directories}
			debug={debug}
			list={list}
			status={status}
			onSelect={onSelect}
			onErrors={onErrors}
		/>
	);
}

type LoadedGeneratorProps<T, D> = {
	columns?: number[];
	results: GeneratorResults<D>;
	loaded: GeneratorLoaded<T, D>;
	onDone: (results: GeneratorResults<D>) => void;
	props?: T;
};
function LoadedGenerator<T, D>({
	columns,
	results,
	loaded,
	onDone,
	props,
}: LoadedGeneratorProps<T, D>) {
	const { generator, use, config, manifest } = loaded;

	return (
		<Generator
			previous={results}
			columns={columns}
			config={config}
			manifest={manifest}
			generator={generator}
			use={use}
			props={props}
			onDone={onDone}
		/>
	);
}
