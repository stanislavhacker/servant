import { useMemo } from "react";

import { GeneratorAnswer, GeneratorQuestions, QuestionItem } from "../../types";
import { getAnswer, hasAnswer } from "../../form";

export function useQuestionItems(
	questions: GeneratorQuestions,
	answers: GeneratorAnswer[]
): QuestionItem[] {
	return useMemo(() => {
		return questions.map(
			(question) =>
				({
					question,
					answer: getAnswer(answers, question.id),
					type: hasAnswer(questions, answers, question.id),
				} as QuestionItem)
		);
	}, [questions, answers]);
}
