import { useEffect, useState } from "react";

import { GeneratorsList } from "../../types";
import { loader } from "../../loader";

export function useGeneratorLoader(
	directories: string[],
	onDone: (results: GeneratorsList) => void
) {
	const [status, setStatus] = useState<"pending" | "running" | "done">("pending");

	useEffect(() => {
		//already running or done
		if (status !== "pending") {
			return;
		}

		setStatus("running");
		loader(directories).then((results) => {
			setStatus("done");
			onDone(results);
		});
	}, [...directories, onDone]);

	return {
		status,
	};
}
