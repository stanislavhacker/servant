import { useCallback, useState } from "react";

import { GeneratorLoaded } from "../../types";

export function useSelectGenerator<T, D>(generator?: string) {
	const [selectedGenerator, setGn] = useState(generator);

	const onSelect = useCallback((gen: GeneratorLoaded<T, D> | "exit") => {
		if (gen === "exit") {
			process.exit(0);
		} else {
			setGn(gen.manifest.name);
		}
	}, []);

	return {
		selectedGenerator,
		onSelect,
	};
}
