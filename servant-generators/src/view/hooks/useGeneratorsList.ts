import { useState } from "react";

import { GeneratorsList } from "../../types";

import { useGeneratorLoader } from "./useGeneratorLoader";

export function useGeneratorsList(directories: string[]) {
	const [list, setList] = useState<GeneratorsList | null>(null);
	const { status } = useGeneratorLoader(directories, setList);

	return {
		directories,
		status,
		list,
	};
}
