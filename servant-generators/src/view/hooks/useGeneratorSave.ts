import { useEffect, useState } from "react";

import { GeneratorResults, GeneratorSaveResults, GeneratorSavedHandlers } from "../../types";
import { saver } from "../../saver";

export function useGeneratorSave<D>(
	results: GeneratorResults<D>,
	onDone: (results: GeneratorSaveResults, fn: GeneratorSavedHandlers) => void
) {
	const [status, setStatus] = useState<"pending" | "running" | "done">("pending");

	useEffect(() => {
		//already running or done
		if (status !== "pending") {
			return;
		}

		setStatus("running");
		saver(results.output, results).then(({ results, fn }) => {
			setStatus("done");
			onDone(results, fn);
		});
	}, [results.output, results, onDone]);

	return {
		status,
	};
}
