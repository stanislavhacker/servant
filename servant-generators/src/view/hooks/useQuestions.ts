import { useMemo, useState } from "react";
import { GeneratorConfig, GeneratorResults } from "../../types";
import { extractPreparedQuestions } from "../../form";

export function useQuestions<D>(
	config: GeneratorConfig,
	name: string,
	previous: GeneratorResults<D>
) {
	const questions = useCollectedQuestions(config, name, previous);
	return useState(questions);
}

function useCollectedQuestions<D>(
	config: GeneratorConfig,
	name: string,
	previous: GeneratorResults<D>
) {
	return useMemo(() => {
		const { remainingQuestions, generatorQuestions } = extractPreparedQuestions(
			previous.preparedQuestions,
			name
		);

		previous.preparedQuestions = remainingQuestions;
		return [...(config.questions || []), ...generatorQuestions];
	}, [config, name, previous]);
}
