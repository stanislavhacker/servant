import { useEffect, useState } from "react";

import {
	Generator,
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorInput,
	GeneratorResults,
} from "../../types";
import { runner } from "../../runner";

export function useGeneratorRun<T, D>(
	prev: GeneratorResults<D>,
	input: GeneratorInput,
	generator: Generator<T, D>,
	filled: boolean,
	onDone: (
		results: GeneratorResults<D>,
		data: GeneratorDataHandlers,
		fn: GeneratorFnHandlers
	) => void,
	props?: T
) {
	const [status, setStatus] = useState<"pending" | "running" | "done">("pending");

	useEffect(() => {
		//not filled
		if (!filled) {
			return;
		}
		//already running or done
		if (status !== "pending") {
			return;
		}

		setStatus("running");
		runner(prev, input, generator, props).then(({ results, data, fn }) => {
			setStatus("done");
			onDone(results, data, fn);
		});
	}, [generator, filled, props, prev, input, onDone]);

	return {
		status,
	};
}
