import { useMemo } from "react";

import { GeneratorAnswer, GeneratorQuestions, AnswerType } from "../../types";

import { useQuestionItems } from "./useQuestionItems";

export function useQuestionsState(
	questions: GeneratorQuestions,
	answersCurrent: GeneratorAnswer[],
	answersPrevious: GeneratorAnswer[]
) {
	const allAnswers = useMemo(
		() => [...answersCurrent, ...answersPrevious],
		[answersCurrent, answersPrevious]
	);
	const items = useQuestionItems(questions, allAnswers);
	const missing = useMemo(() => items.filter(({ type }) => type === AnswerType.No), [items]);
	const filled = useMemo(() => items.filter(({ type }) => type === AnswerType.Yes), [items]);

	return {
		items,
		missing,
		filled,
	};
}
