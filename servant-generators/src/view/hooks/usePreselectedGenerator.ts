import { GeneratorsList } from "../../types/index";
import { useMemo } from "react";

export function usePreselectedGenerator(list: GeneratorsList | null, generator?: string) {
	return useMemo(() => {
		if (list && list.errors.length === 0 && generator) {
			return list.generators.find((g) => g.manifest.name === generator) || null;
		}
		return null;
	}, [list, generator]);
}
