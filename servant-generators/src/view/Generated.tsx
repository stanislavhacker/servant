import * as React from "react";
import { useCallback, useState } from "react";
import { Box, Text } from "ink";
import * as path from "path";

import {
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorResults,
	GeneratorSaveResults,
} from "../types";
import { useGeneratorSave } from "./hooks/useGeneratorSave";
import { Errors } from "./Errors";

export type GeneratedProps<T, D> = {
	loaded: GeneratorLoaded<T, D>;
	results: GeneratorResults<D>;
	onDone?: (results: GeneratorSaveResults) => void;
	debug?: boolean;
};
export function Generated<T, D>({ loaded, results, debug, onDone }: GeneratedProps<T, D>) {
	const [saveResults, setSaveResults] = useState<GeneratorSaveResults | null>(null);

	const onSave = useCallback(
		(results: GeneratorSaveResults) => {
			onDone && onDone(results);
			setSaveResults(results);
		},
		[onDone]
	);

	const { status } = useGeneratorSave(results, onSave);

	return (
		<Box flexDirection="column" minWidth={Number.MAX_SAFE_INTEGER}>
			{status !== "done" && (
				<>
					<GeneratedProgressHeader into={results.output} manifest={loaded.manifest} />
					<Box flexDirection="row" marginLeft={2}>
						<Text italic color="whiteBright">
							{" "}
							Generator saving results on disk ...
						</Text>
					</Box>
				</>
			)}
			{status === "done" && saveResults && saveResults.status === "success" && (
				<>
					<GeneratedFinishedHeader
						into={results.output}
						manifest={loaded.manifest}
						paths={saveResults.paths}
					/>
					{saveResults.paths.map((file, index) => (
						<Box marginLeft={1} flexDirection="row" key={index}>
							<Text>
								<Text bold>{index + 1}.</Text>
								<Text> </Text>
								<Text color="whiteBright">{path.basename(file)}</Text>:{" "}
								<Text underline>{file}</Text>
							</Text>
						</Box>
					))}
					{saveResults.messages.length > 0 && (
						<Box flexDirection="column" marginTop={1}>
							<Box flexDirection="row">
								<Text color="cyanBright">Messages collected by generator:</Text>
							</Box>
							{saveResults.messages
								.slice()
								.sort(sortMessages)
								.map((message, index) => (
									<GeneratedMessage key={index} message={message} />
								))}
						</Box>
					)}
				</>
			)}
			{status === "done" && saveResults && saveResults.status === "error" && (
				<Errors errors={saveResults.errors} debug={debug} />
			)}
		</Box>
	);
}

type GeneratedHeaderProps = {
	manifest: GeneratorManifest;
	into?: string;
	paths?: string[];
};
const GeneratedProgressHeader: React.FC<GeneratedHeaderProps> = ({ manifest, into }) => {
	return (
		<Box flexDirection="row">
			<Text color="whiteBright">Generator "</Text>
			<Text bold color="blueBright">
				{manifest.name}
			</Text>
			{Boolean(into) && (
				<>
					<Text color="whiteBright">" will create data in "</Text>
					<Text color="greenBright">{into}</Text>
					<Text color="whiteBright">" folder</Text>
				</>
			)}
			{!into && (
				<>
					<Text color="whiteBright">" has no create any output, </Text>
					<Text color="redBright">because is not defined.</Text>
				</>
			)}
		</Box>
	);
};

const GeneratedFinishedHeader: React.FC<GeneratedHeaderProps> = ({
	manifest,
	into,
	paths = [],
}) => {
	if (paths.length === 0) {
		return (
			<Box flexDirection="row">
				<Text color="whiteBright">Generator "</Text>
				<Text bold color="blueBright">
					{manifest.name}
				</Text>
				<Text color="whiteBright">" do not </Text>
				<Text color="yellowBright">changed any file</Text>
				<Text color="whiteBright"> in provided folder</Text>
			</Box>
		);
	}
	return (
		<Box flexDirection="row">
			<Text color="whiteBright">Generator "</Text>
			<Text bold color="blueBright">
				{manifest.name}
			</Text>
			<Text color="whiteBright">" created or update data in "</Text>
			<Text color="greenBright">{into}</Text>
			<Text color="whiteBright">" folder</Text>
		</Box>
	);
};

type GeneratedMessageProps = {
	message: GeneratorMessage;
};
const GeneratedMessage: React.FC<GeneratedMessageProps> = ({ message }) => {
	return (
		<Box marginLeft={1} flexDirection="row">
			{message.type === "error" && (
				<Text>
					<Text backgroundColor="redBright" color="whiteBright">
						[ERROR]
					</Text>{" "}
				</Text>
			)}
			{message.type === "warning" && (
				<Text>
					<Text backgroundColor="yellowBright" color="whiteBright">
						[WARNING]
					</Text>{" "}
				</Text>
			)}
			{message.type === "info" && (
				<Text>
					<Text backgroundColor="blueBright" color="whiteBright">
						[INFO]
					</Text>{" "}
				</Text>
			)}
			{message.type === "success" && (
				<Text>
					<Text backgroundColor="greenBright" color="whiteBright">
						[SUCCESS]
					</Text>{" "}
				</Text>
			)}
			{message.title && <Text color={getColorByType(message.type)}>{message.title}: </Text>}
			<Text color={getColorByType(message.type)} italic>
				{message.text}
			</Text>
		</Box>
	);
};

function getColorByType(type: GeneratorMessage["type"]) {
	switch (type) {
		case "error":
			return "redBright";
		case "warning":
			return "yellowBright";
		case "info":
			return "blueBright";
		case "success":
			return "greenBright";
		default:
			return "whiteBright";
	}
}

function sortMessages(a: GeneratorMessage, b: GeneratorMessage) {
	return getPriority(b.type) - getPriority(a.type);
}

function getPriority(type: GeneratorMessage["type"]) {
	switch (type) {
		case "error":
			return 3;
		case "warning":
			return 2;
		case "info":
		case "success":
			return 1;
		default:
			return 0;
	}
}
