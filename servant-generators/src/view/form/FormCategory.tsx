import * as React from "react";
import { Box, Text } from "ink";

import { GeneratorConfig } from "../../types";

export const Category: React.FC<{
	config: GeneratorConfig;
	category: string | undefined;
	columns?: number[];
}> = ({ config, category, columns = [] }) => {
	const { categories = {} } = config;

	if (!category) {
		return null;
	}

	return (
		<Box flexDirection="row" minWidth={columns[0]}>
			<Text bold color={categories[category] || "white"}>
				[{category.toUpperCase()}]{" "}
			</Text>
		</Box>
	);
};
