import * as React from "react";
import { useMemo, useState } from "react";
import { Box, Text } from "ink";
import TextInput from "ink-text-input";
import SelectInput from "ink-select-input";

import {
	GeneratorAnswer,
	GeneratorConfig,
	GeneratorDataHandlers,
	GeneratorQuestion,
	GeneratorResults,
} from "../../types/index";
import { getAnswer, getProp, getQuestion } from "../../form";
import { addQuestion } from "../../form/questions";
import { addAnswer } from "../../form/answers";

import { Category } from "./FormCategory";

export type QuestionProps<T, D> = {
	previous: GeneratorResults<D>;
	config: GeneratorConfig;
	questions: GeneratorQuestion[];
	answers: GeneratorAnswer[];
	question: GeneratorQuestion;
	answer: GeneratorAnswer;
	onDone: (answer: GeneratorAnswer) => void;
	columns?: number[];
	props?: T;
};

export function Question<T, D>({
	columns = [],
	questions,
	answers,
	question,
	answer,
	config,
	onDone,
	props,
	previous,
}: QuestionProps<T, D>) {
	const [value, setValue] = useState(answer.value || "");
	const handlers = useDataHandlers(previous, questions, answers, props);
	const placeholder = usePlaceholder(question, handlers);

	const handleChange = (value: unknown) => {
		setValue(value as string);
	};

	const handleSubmit = (value: unknown) => {
		setValue("");
		answer.value = (value || placeholder) as string;
		onDone(answer);
	};

	return (
		<Box flexDirection="column">
			<Box flexDirection="column">
				<Box flexDirection="row">
					<Category config={config} category={question.category} columns={columns} />
					<Text italic bold color="blueBright">
						{question.question}:
					</Text>
				</Box>
				{question.tip && (
					<Text italic>
						{" "}
						<Text color="yellowBright">INFO:</Text> {question.tip}
					</Text>
				)}
			</Box>
			{question.type === "select" && (
				<SelectInput
					limit={question.limit ?? 10}
					items={question.values.map(({ label, value }, i) => ({
						label,
						value,
						key: label + i,
					}))}
					onSelect={(item) => handleSubmit(item.value(handlers))}
				/>
			)}
			{question.type === "string" && (
				<TextInput
					value={value}
					placeholder={placeholder}
					onChange={handleChange}
					onSubmit={handleSubmit}
				/>
			)}
		</Box>
	);
}

function useDataHandlers<T, D>(
	previous: GeneratorResults<D>,
	questions: GeneratorQuestion[],
	answers: GeneratorAnswer[],
	props?: T
): GeneratorDataHandlers {
	return useMemo(
		() => ({
			getAnswer: (id) => getAnswer([...previous.answers, ...answers], id),
			getQuestion: (id) => getQuestion(questions, id),
			getProp: (id) => getProp(props, id),
			addQuestion: (question, generator) =>
				addQuestion(previous.preparedQuestions, question, generator),
			addAnswer: (answer) => addAnswer(previous.answers, answer),
		}),
		[answers, questions, previous]
	);
}

function usePlaceholder(question: GeneratorQuestion, handlers: GeneratorDataHandlers) {
	return useMemo(() => {
		const placeholder = question.defaultValue
			? question.defaultValue.value(handlers) || ""
			: "";
		return Array.isArray(placeholder) ? placeholder.join(", ") : String(placeholder);
	}, [question, handlers]);
}
