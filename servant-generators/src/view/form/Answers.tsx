import * as React from "react";
import { Box, Text } from "ink";

import { AnswerType, GeneratorConfig, QuestionItem } from "../../types";

import { Category } from "./FormCategory";

export const Answers: React.FC<{
	config: GeneratorConfig;
	filled: QuestionItem[];
	previous: QuestionItem[];
	columns?: number[];
}> = ({ columns, config, filled, previous }) => {
	return (
		<Box flexDirection="column">
			{[...previous, ...filled].map(({ question, answer, type }, index) => {
				if (type === AnswerType.Skip) {
					return null;
				}
				return (
					<Box flexDirection="row" key={index}>
						<Category config={config} category={question.category} columns={columns} />
						<Text italic bold color="whiteBright">
							{question.question}
						</Text>
						<Text> </Text>
						<Text color="greenBright">{answer?.value}</Text>
					</Box>
				);
			})}
		</Box>
	);
};
