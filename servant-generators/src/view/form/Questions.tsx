import * as React from "react";

import {
	GeneratorAnswer,
	GeneratorConfig,
	GeneratorQuestion,
	GeneratorResults,
	QuestionItem,
} from "../../types";
import { createAnswer } from "../../form";

import { Question } from "./Question";

export type QuestionsProps<T, D> = {
	previous: GeneratorResults<D>;
	config: GeneratorConfig;
	questions: GeneratorQuestion[];
	answers: GeneratorAnswer[];
	missing: QuestionItem[];
	onDone: (answer: GeneratorAnswer) => void;
	columns?: number[];
	props?: T;
};

export function Questions<T, D>({
	config,
	questions,
	answers,
	missing,
	onDone,
	columns,
	props,
	previous,
}: QuestionsProps<T, D>) {
	//alL answers has values
	if (missing.length === 0) {
		return null;
	}

	//first not answered
	const { question } = missing[0];

	return (
		<Question
			config={config}
			columns={columns}
			question={question}
			answer={createAnswer(question.id, "")}
			questions={questions}
			answers={answers}
			props={props}
			previous={previous}
			onDone={onDone}
		/>
	);
}
