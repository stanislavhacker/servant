import * as React from "react";
import { useState } from "react";
import { Box, Text } from "ink";

import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorResults,
} from "../types";
import { GeneratorEmptyResults } from "../constants";

import { useQuestionsState } from "./hooks/useQuestionsState";
import { useGeneratorRun } from "./hooks/useGeneratorRun";
import { useQuestionItems } from "./hooks/useQuestionItems";
import { useQuestions } from "./hooks/useQuestions";
import { Answers } from "./form/Answers";
import { Questions } from "./form/Questions";
import { convertFromDefined } from "../form/messages";

export type GeneratorProps<T, D> = GeneratorLoaded<T, D> & {
	columns?: number[];
	props?: T;
	previous?: GeneratorResults<D>;
	onDone: (
		results: GeneratorResults<D>,
		data: GeneratorDataHandlers,
		fn: GeneratorFnHandlers
	) => void;
};
export function Generator<T, D>({
	columns,
	use,
	generator,
	manifest,
	config,
	props,
	onDone,
	previous = GeneratorEmptyResults as GeneratorResults<D>,
}: GeneratorProps<T, D>) {
	const [used, setUsed] = useState(use);
	const [results, setResults] = useState({
		...previous,
		answers: [...(config.answers || []), ...previous.answers],
	});

	if (used.length) {
		return (
			<Generator
				key={used[0].manifest.name}
				previous={results}
				columns={columns}
				props={props}
				{...used[0]}
				onDone={(results) => {
					setResults(results);
					setUsed(used.slice(1));
				}}
			/>
		);
	}

	return (
		<GeneratorCore<T, D>
			manifest={manifest}
			config={config}
			generator={generator}
			previous={results}
			onDone={onDone}
			props={props}
			columns={columns}
		/>
	);
}

type GeneratorCoreProps<T, D> = Pick<GeneratorLoaded<T, D>, "generator" | "manifest" | "config"> & {
	columns?: number[];
	previous: GeneratorResults<D>;
	onDone: (
		results: GeneratorResults<D>,
		data: GeneratorDataHandlers,
		fn: GeneratorFnHandlers
	) => void;
	props?: T;
};

function GeneratorCore<T, D>({
	columns,
	previous,
	manifest,
	config,
	generator,
	props,
	onDone,
}: GeneratorCoreProps<T, D>) {
	const [questions] = useQuestions(config, manifest.name, previous);
	const [answers, setAnswers] = useState(config.answers || []);
	const [messages] = useState(config.messages || []);

	const previousItems = useQuestionItems(previous.questions, previous.answers);
	const { missing, filled } = useQuestionsState(questions, answers, previous.answers);
	const { status } = useGeneratorRun(
		previous,
		{ manifest, answers, questions, messages: convertFromDefined(messages, manifest) },
		generator,
		missing.length === 0,
		onDone,
		props
	);

	return (
		<Box flexDirection="column">
			<GeneratorHeader manifest={manifest} />
			<Answers config={config} columns={columns} filled={filled} previous={previousItems} />
			<Questions
				config={config}
				columns={columns}
				missing={missing}
				answers={answers}
				questions={questions}
				props={props}
				previous={previous}
				onDone={(answer) => setAnswers([...answers, answer])}
			/>
			<GeneratorStatus manifest={manifest} status={status} />
		</Box>
	);
}

type GeneratorHeaderProps = {
	manifest: GeneratorManifest;
};
const GeneratorHeader: React.FC<GeneratorHeaderProps> = ({ manifest }) => {
	return (
		<Box flexDirection="row">
			<Text color="whiteBright">Generator "</Text>
			<Text bold color="blueBright">
				{manifest.name}
			</Text>
			<Text color="whiteBright">" </Text>
			<Text italic color="grey">
				{manifest.description}
			</Text>
		</Box>
	);
};

type GeneratorStatusProps = {
	manifest: GeneratorManifest;
	status: "pending" | "running" | "done";
};
const GeneratorStatus: React.FC<GeneratorStatusProps> = ({ status, manifest }) => {
	if (status === "running") {
		return (
			<Box flexDirection="row" marginLeft={2}>
				<Text italic color="whiteBright">
					{" "}
					Generator "{manifest.name}" running...
				</Text>
			</Box>
		);
	}

	if (status === "done") {
		return (
			<Box flexDirection="row">
				<Text color="greenBright">
					{" "}
					✓ All questions answered and generator "{manifest.name}" finished.
				</Text>
			</Box>
		);
	}

	return null;
};
