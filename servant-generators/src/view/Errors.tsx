import * as React from "react";
import { Box, Text } from "ink";

export type ErrorsProps = {
	errors: Error[];
	debug?: boolean;
};
export const Errors: React.FC<ErrorsProps> = ({ errors, debug }) => {
	//no errors
	if (errors.length === 0) {
		return null;
	}

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<Text color="redBright">
					Some errors occurred when running{" "}
					<Text color="greenBright" bold>
						generator
					</Text>
					:
				</Text>
			</Box>
			{errors.map((error, index) => (
				<Box flexDirection="column" key={index} marginBottom={1} marginLeft={2}>
					<Box flexDirection="row">
						<Text color="redBright" bold>
							{error.name}
						</Text>
						<Text> </Text>
						<Text color="grey" italic>
							{error.message}
						</Text>
					</Box>
					{debug && <Text color="redBright">{error.stack}</Text>}
				</Box>
			))}
		</Box>
	);
};
