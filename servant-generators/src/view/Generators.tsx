import * as React from "react";
import { Box, Text } from "ink";
import SelectInput from "ink-select-input";
import { useEffect } from "react";

import { GeneratorLoaded, GeneratorsList } from "../types";

import { useGeneratorsList } from "./hooks/useGeneratorsList";
import { Errors } from "./Errors";

export type GeneratorsProps<T, D> = {
	directories: ReturnType<typeof useGeneratorsList>["directories"];
	list: ReturnType<typeof useGeneratorsList>["list"];
	status: ReturnType<typeof useGeneratorsList>["status"];
	onSelect: (gen: GeneratorLoaded<T, D> | "exit") => void;
	onErrors?: (errors: Error[]) => void;
	debug?: boolean;
};
export function Generators<T, D>({
	directories,
	list,
	status,
	onSelect,
	onErrors,
	debug,
}: GeneratorsProps<T, D>) {
	return (
		<>
			<GeneratorsHeader directories={directories} />
			<GeneratorsStatus status={status} />
			{list && (
				<GeneratorsRoster
					list={list}
					debug={debug}
					onErrors={onErrors}
					onSelect={onSelect}
				/>
			)}
		</>
	);
}

type GeneratorsHeaderProps = {
	directories: string[];
};
const GeneratorsHeader: React.FC<GeneratorsHeaderProps> = ({ directories }) => {
	return (
		<Box flexDirection="column" marginBottom={directories.length > 0 ? 1 : 0}>
			<Text color="whiteBright">Generators loader</Text>
			{directories.map((directory, index) => (
				<Text key={index}>
					<Text> </Text>
					<Text color="whiteBright" bold>
						{index + 1}.
					</Text>
					<Text> </Text>
					<Text color="greenBright" italic>
						{directory}
					</Text>
				</Text>
			))}
		</Box>
	);
};

type GeneratorsStatusProps = {
	status: "pending" | "running" | "done";
};
const GeneratorsStatus: React.FC<GeneratorsStatusProps> = ({ status }) => {
	if (status === "running" || status === "pending") {
		return (
			<Box flexDirection="column" marginLeft={2}>
				<Text italic color="whiteBright">
					{" "}
					Generators loader running for directories ...
				</Text>
			</Box>
		);
	}

	return null;
};

type SelectItem<T, D> = {
	label: string;
	value: GeneratorLoaded<T, D> | "exit";
	key: string;
};

type GeneratorsRosterProps<T, D> = {
	list: GeneratorsList;
	onSelect: (gen: GeneratorLoaded<T, D> | "exit") => void;
	onErrors?: (errors: Error[]) => void;
	debug?: boolean;
};
function GeneratorsRoster<T, D>({ list, debug, onSelect, onErrors }: GeneratorsRosterProps<T, D>) {
	const { generators, errors } = list;

	useEffect(() => {
		if (errors.length > 0) {
			onErrors && onErrors(errors);
		}
	}, [errors.length]);

	if (errors.length > 0) {
		return <Errors errors={errors} debug={debug} />;
	}

	const items: SelectItem<T, D>[] = [
		...generators.map((generator) => {
			return {
				label: generator.manifest.name,
				value: generator,
				key: generator.manifest.name,
			};
		}),
		{ label: "(exit)", value: "exit", key: "(exit)" },
	];

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<Text color="whiteBright">
					Select{" "}
					<Text color="greenBright" bold>
						generator
					</Text>{" "}
					to run:
				</Text>
			</Box>
			<SelectInput items={items} onSelect={(item) => onSelect(item.value)} />
		</Box>
	);
}
