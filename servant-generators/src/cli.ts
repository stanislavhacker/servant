import Process = NodeJS.Process;
import * as path from "path";

import { envfull, EnvfullVars } from "envfull";

export type GeneratorCliArguments = {
	output: string;
	dirs: string[];
	generator?: string;
	debug?: boolean;
	help?: boolean;
};

export function getArguments(process: Process): GeneratorCliArguments {
	const { $, _ } = get(process);

	return {
		output: $.output as string,
		dirs: parseDirs(process, $.dir, _),
		generator: $.generator,
		debug: $.debug,
		help: $.help,
	};
}

type GeneratorRawArguments = {
	output: string;
	dir: string[] | string;
	generator?: string;
	debug?: boolean;
	help?: boolean;
};
function get(process: Process): EnvfullVars<GeneratorRawArguments> {
	return envfull<GeneratorRawArguments>(process, {
		env: [/GENERATOR\.{*.}/],
		arrays: ["dir"],
		defaults: {
			output: path.join(process.cwd(), ".output"),
			dir: process.cwd(),
			debug: false,
			help: false,
			generator: undefined,
		},
		aliases: {},
	})();
}

function parseDirs(process: Process, dir: string | string[] = [], rest: Array<unknown>): string[] {
	const dirs = Array.isArray(dir) ? dir : [dir];
	const restDirs = rest.slice(0) as string[];

	const forcedDirs = [process.cwd(), path.join(process.cwd(), "node_modules")];

	const directories = [...dirs, ...restDirs, ...forcedDirs]
		.map((dir) => dir.split(","))
		.reduce((acc, val) => acc.concat(val), [])
		.filter(Boolean);

	return [...new Set(directories)];
}
