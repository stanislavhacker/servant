import * as path from "path";

import { GeneratorDir, GeneratorFile, GeneratorManifest, GeneratorResults } from "../types";

export function createFile(
	results: GeneratorResults<unknown>,
	created: GeneratorManifest,
	relativePth: string,
	content: string | Buffer | Record<string, unknown>
): GeneratorFile {
	const relativePath = path.normalize(relativePth);
	const existing = getFile(results, relativePath);

	if (existing) {
		return {
			...existing,
			err: "exists",
		};
	}

	const file = {
		updated: [],
		relativePath,
		created,
		content,
	};
	results.files.push(file);
	return { ...file };
}

export function getFile(
	results: GeneratorResults<unknown>,
	relativePth: string
): GeneratorFile | null {
	const file = findFile(relativePth, results);

	if (file) {
		return { ...file };
	}
	return null;
}

export function getFiles(results: GeneratorResults<unknown>): GeneratorFile[] {
	return [...results.files.map((f) => ({ ...f }))];
}

export function updateFile(
	results: GeneratorResults<unknown>,
	created: GeneratorManifest,
	relativePth: string,
	content: string | Buffer | Record<string, unknown>
): GeneratorFile {
	const relativePath = path.normalize(relativePth);
	const existing = findFile(relativePth, results);

	if (existing) {
		const index = results.files.indexOf(existing);
		const file = {
			...existing,
			content,
			updated: [...existing.updated, created],
		};
		results.files[index] = file;
		return { ...file };
	}

	return {
		updated: [],
		relativePath,
		created,
		content,
		err: "not-found",
	};
}

export function removeFile(
	results: GeneratorResults<unknown>,
	created: GeneratorManifest,
	relativePth: string
): GeneratorFile {
	const relativePath = path.normalize(relativePth);
	const existing = findFile(relativePth, results);

	if (existing) {
		const index = results.files.indexOf(existing);
		const file = results.files[index];
		results.files.splice(index, 1);
		return { ...file };
	}

	return {
		updated: [],
		relativePath,
		created,
		content: "",
		err: "not-found",
	};
}

export function createDir(
	results: GeneratorResults<unknown>,
	created: GeneratorManifest,
	relativePth: string
): GeneratorDir {
	const relativePath = path.normalize(relativePth);
	const existing = getDir(results, relativePath);

	if (existing) {
		return {
			...existing,
			err: "exists",
		};
	}

	const dir = {
		relativePath,
		created,
	};
	results.directories.push(dir);
	return { ...dir };
}

export function getDir(
	results: GeneratorResults<unknown>,
	relativePth: string
): GeneratorDir | null {
	const dir = findDir(relativePth, results);

	if (dir) {
		return { ...dir };
	}
	return null;
}

export function getDirs(results: GeneratorResults<unknown>): GeneratorDir[] {
	return [...results.directories.map((d) => ({ ...d }))];
}

export function removeDir(
	results: GeneratorResults<unknown>,
	created: GeneratorManifest,
	relativePth: string
): GeneratorDir {
	const relativePath = path.normalize(relativePth);
	const existing = findDir(relativePath, results);

	if (existing) {
		const index = results.directories.indexOf(existing);
		const dir = results.directories[index];
		results.directories.splice(index, 1);
		return { ...dir };
	}

	return {
		relativePath,
		created,
		err: "not-found",
	};
}

function findFile(relativePth: string, results: GeneratorResults<unknown>) {
	const relativePath = path.normalize(relativePth);
	return results.files.find((file) => path.normalize(file.relativePath) === relativePath) || null;
}

function findDir(relativePth: string, results: GeneratorResults<unknown>) {
	const relativePath = path.normalize(relativePth);
	return (
		results.directories.find((file) => path.normalize(file.relativePath) === relativePath) ||
		null
	);
}
