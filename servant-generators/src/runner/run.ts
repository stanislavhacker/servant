import {
	Generator,
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorInput,
	GeneratorLoaded,
	GeneratorResults,
	GeneratorRunnerResults,
} from "../types";
import { extractPreparedQuestions, getAnswer, getCustomData, getProp, getQuestion } from "../form";

import {
	createDir,
	createFile,
	getDir,
	getDirs,
	getFile,
	getFiles,
	removeDir,
	removeFile,
	updateFile,
} from "./files";
import { createMessage, deleteMessage, getMessage } from "./messages";
import { convertFromDefined } from "../form/messages";
import { updateCustomData } from "../form/customData";
import { addQuestion } from "../form/questions";
import { addAnswer } from "../form/answers";
import { getOutput, setOutput } from "./output";

export function multiRunner<T, D>(
	previous: GeneratorResults<D>,
	{ generator, config, manifest, use }: GeneratorLoaded<T, D>,
	props?: T
): Promise<GeneratorRunnerResults<D>> {
	return new Promise<GeneratorRunnerResults<D>>((resolve) => {
		const input = {
			messages: convertFromDefined(config.messages || [], manifest),
			questions: config.questions || [],
			answers: config.answers || [],
			manifest,
		};

		let p = Promise.resolve({ results: previous, data: getDataHandlers(previous, props) });
		use.forEach((loaded) => {
			p = p.then(({ results }) => multiRunner(results, loaded, props));
		});
		p = p.then(({ results }) => runner(results, input, generator, props));
		p.then(({ results }) => {
			resolve({
				results,
				data: getDataHandlers(results, props),
				fn: getFnHandlers(results, input),
			});
		});
	});
}

export function runner<T, D>(
	previous: GeneratorResults<D>,
	input: GeneratorInput,
	generator: Generator<T, D>,
	props?: T
): Promise<GeneratorRunnerResults<D>> {
	return new Promise<GeneratorRunnerResults<D>>((resolve) => {
		const { remainingQuestions, generatorQuestions } = extractPreparedQuestions(
			previous.preparedQuestions,
			input.manifest.name
		);
		const next: GeneratorResults<D> = {
			preparedQuestions: [...remainingQuestions],
			questions: [...previous.questions, ...input.questions, ...generatorQuestions],
			answers: [...previous.answers, ...input.answers],
			messages: [...previous.messages, ...input.messages],
			errors: [...previous.errors],
			files: [...previous.files],
			directories: [...previous.directories],
			status: previous.status,
			customData: previous.customData,
			output: previous.output,
		};

		try {
			generator(
				getDataHandlers(next, props),
				getFnHandlers(next, input),
				onDone(next, input, resolve, props),
				props
			);
		} catch (e) {
			onError(next, input, e, resolve, props);
		}
	});
}

function getDataHandlers<T, D>(next: GeneratorResults<D>, props?: T): GeneratorDataHandlers {
	return {
		getAnswer: (id) => getAnswer(next.answers, id),
		getQuestion: (id) => getQuestion(next.questions, id),
		getProp: (id) => getProp(props, id),
		addQuestion: (question, generator) =>
			addQuestion(next.preparedQuestions, question, generator),
		addAnswer: (answer) => addAnswer(next.answers, answer),
	};
}

function getFnHandlers<D>(next: GeneratorResults<D>, input: GeneratorInput): GeneratorFnHandlers {
	return {
		createFile: (relativePath, content) =>
			createFile(next, input.manifest, relativePath, content),
		getFile: (relativePath) => getFile(next, relativePath),
		getFiles: () => getFiles(next),
		updateFile: (relativePath, content) =>
			updateFile(next, input.manifest, relativePath, content),
		removeFile: (relativePath) => removeFile(next, input.manifest, relativePath),
		createDir: (relativePath) => createDir(next, input.manifest, relativePath),
		getDir: (relativePath) => getDir(next, relativePath),
		getDirs: () => getDirs(next),
		removeDir: (relativePath) => removeDir(next, input.manifest, relativePath),
		createMessage: (id, message) => createMessage(next, input.manifest, id, message),
		deleteMessage: (id) => deleteMessage(next, input.manifest, id),
		getMessage: (id) => getMessage(next, id),
		getCustomData: <D>() => getCustomData<D>(next as unknown as GeneratorResults<D>) as D,
		updateCustomData: <D>(data: D) =>
			updateCustomData<D>(next as unknown as GeneratorResults<D>, data),
		setOutput: (output) => setOutput(next, output),
		getOutput: () => getOutput(next),
	};
}

function onDone<T, D>(
	next: GeneratorResults<D>,
	input: GeneratorInput,
	resolve: (value: GeneratorRunnerResults<D>) => void,
	props?: T
) {
	return (status: GeneratorResults<D>["status"]) => {
		resolve({
			results: {
				...next,
				status: getStatus(next.status, status),
			},
			data: getDataHandlers(next, props),
			fn: getFnHandlers(next, input),
		});
	};
}

function onError<T, D>(
	next: GeneratorResults<D>,
	input: GeneratorInput,
	e: Error,
	resolve: (value: GeneratorRunnerResults<D>) => void,
	props?: T
) {
	resolve({
		results: {
			...next,
			errors: [...next.errors, e],
			status: "error",
		},
		data: getDataHandlers(next, props),
		fn: getFnHandlers(next, input),
	});
}

function getStatus(
	status: GeneratorResults<unknown>["status"],
	status2: GeneratorResults<unknown>["status"]
) {
	if (status === "error" || status2 === "error") {
		return "error";
	}
	return "success";
}
