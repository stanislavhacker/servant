import {
	GeneratorDefinedMessage,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorResults,
} from "../types";

export function createMessage(
	next: GeneratorResults<unknown>,
	created: GeneratorManifest,
	id: string,
	message: Omit<GeneratorDefinedMessage, "id">
): GeneratorMessage {
	const existing = getMessage(next, id);

	if (existing) {
		return {
			...existing,
			err: "exists",
		};
	}

	const msg: GeneratorMessage = {
		...message,
		id,
		created,
		updated: [],
	};
	next.messages.push(msg);
	return { ...msg };
}

export function deleteMessage(
	next: GeneratorResults<unknown>,
	created: GeneratorManifest,
	id: string
): GeneratorMessage {
	const existing = getMessage(next, id);

	if (!existing) {
		return {
			id,
			created,
			updated: [],
			text: "",
			type: "info",
			err: "not-found",
		};
	}

	next.messages = next.messages.filter((msg) => msg.id !== id);
	return { ...existing };
}

export function getMessage(next: GeneratorResults<unknown>, id: string): GeneratorMessage | null {
	const msg = next.messages.find((m) => m.id === id);

	if (msg) {
		return { ...msg };
	}
	return null;
}
