import { GeneratorResults } from "../types";

export function setOutput(next: GeneratorResults<unknown>, output: string): void {
	next.output = output;
}

export function getOutput(next: GeneratorResults<unknown>): string | null {
	return next.output ?? null;
}
