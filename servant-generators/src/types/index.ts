export type GeneratorManifestFile = {
	engine: "servant-generator";
	name: string;
	description: string;
	entry?: string;
	use?: string[];
	compiled?: boolean;
};
export type GeneratorManifest = GeneratorManifestFile & {
	path: string;
};

export type GeneratorsList = {
	generators: GeneratorLoaded<unknown, unknown>[];
	errors: Error[];
};
export type GeneratorLoaded<T, D> = {
	use: GeneratorLoaded<T, D>[];
	manifest: GeneratorManifest;
	config: GeneratorConfig;
	generator: Generator<T, D>;
};

export type GeneratorConfig = {
	questions?: GeneratorQuestions;
	answers?: GeneratorAnswer[];
	categories?: Record<string, string>;
	messages?: GeneratorDefinedMessage[];
};

export type GeneratorPreparedQuestions = GeneratorPreparedQuestion[];
export type GeneratorPreparedQuestion = GeneratorQuestion & {
	generator?: string;
	prepared: true;
};

export type GeneratorQuestions = GeneratorQuestion[];
export type GeneratorQuestion = GeneratorStringQuestion | GeneratorSelectQuestion;
export type GeneratorStringQuestion = GeneratorDefaultQuestion & {
	type: "string";
};
export type GeneratorSelectQuestion = GeneratorDefaultQuestion & {
	type: "select";
	values: Array<GeneratorQuestionValue>;
	limit?: number;
};
export type GeneratorDefaultQuestion = {
	id: string;
	category?: string;
	key?: unknown;
	question: string;
	defaultValue?: GeneratorQuestionValue;
	tip?: string;
	condition?: GeneratorCondition;
};
export type GeneratorCondition = {
	or?:
		| {
				[key: string]: string;
		  }
		| {
				[key: string]: GeneratorCondition;
		  };
	and?:
		| {
				[key: string]: string;
		  }
		| {
				[key: string]: GeneratorCondition;
		  };
};
export type GeneratorQuestionValue = {
	label: string;
	value: (data: GeneratorDataHandlers) => string;
};
export type GeneratorDefinedMessage = {
	id: string;
	title?: string;
	text: string;
	type: "info" | "warning" | "error" | "success";
	condition?: GeneratorCondition;
};

export type GeneratorAnswer = {
	id: string;
	value: string;
};

export type GeneratorResults<D> = {
	preparedQuestions: GeneratorPreparedQuestions;
	questions: GeneratorQuestions;
	answers: GeneratorAnswer[];
	messages: GeneratorMessage[];
	errors: Array<Error>;
	files: GeneratorFile[];
	directories: GeneratorDir[];
	status: "success" | "error";
	customData?: D;
	output?: string;
};

export type GeneratorRunnerResults<D> = {
	results: GeneratorResults<D>;
	data: GeneratorDataHandlers;
	fn: GeneratorFnHandlers;
};

export type GeneratorSaveResults = {
	questions: GeneratorQuestions;
	answers: GeneratorAnswer[];
	messages: GeneratorMessage[];
	errors: Array<Error>;
	files: GeneratorSaveFile[];
	directories: GeneratorSaveDir[];
	paths: string[];
	status: "success" | "error";
};

export type GeneratorSaverResults = {
	results: GeneratorSaveResults;
	fn: GeneratorSavedHandlers;
};

export type GeneratorMessageError = "exists" | "not-found";
export type GeneratorMessage = GeneratorDefinedMessage & {
	created: GeneratorManifest;
	updated: GeneratorManifest[];
	err?: GeneratorMessageError;
};

export type GeneratorFileError = "exists" | "not-found";
export type GeneratorFile = {
	relativePath: string;
	content: string | Buffer | Record<string, unknown>;
	created: GeneratorManifest;
	updated: GeneratorManifest[];
	err?: GeneratorFileError;
};
export type GeneratorSaveFile = GeneratorFile & {
	absolutePath: string;
};

export type GeneratorDirError = "exists" | "not-found";
export type GeneratorDir = {
	relativePath: string;
	created: GeneratorManifest;
	err?: GeneratorDirError;
};
export type GeneratorSaveDir = GeneratorDir & {
	absolutePath: string;
};

export type GeneratorInput = {
	questions: GeneratorQuestions;
	answers: GeneratorAnswer[];
	messages: GeneratorMessage[];
	manifest: GeneratorManifest;
};
export interface Generator<T, D> {
	(
		data: GeneratorDataHandlers,
		fn: GeneratorFnHandlers,
		next: GeneratorNextHandler<D>,
		props?: T
	): void;
}
export interface GeneratorNextHandler<D> {
	(status: GeneratorResults<D>["status"]): void;
}
export interface GeneratorDataHandlers {
	getAnswer: FnGetAnswer;
	getQuestion: FnGetQuestion;
	getProp: FnGetProp;
	addQuestion: FnAddQuestion;
	addAnswer: FnAddAnswer;
}
export interface GeneratorFnHandlers {
	createFile: FnCreateFile;
	updateFile: FnUpdateFile;
	getFile: FnGetFile;
	getFiles: FnGetFiles;
	removeFile: FnRemoveFile;
	createDir: FnCreateDir;
	getDir: FnGetDir;
	getDirs: FnGetDirs;
	removeDir: FnRemoveDir;
	createMessage: FnCreateMessage;
	getMessage: FnGetMessage;
	deleteMessage: FnDeleteMessage;
	getCustomData: FnGetCustomData;
	updateCustomData: FnUpdateCustomData;
	getOutput: FnGetOutput;
	setOutput: FnSetOutput;
}
export interface GeneratorSavedHandlers {
	getFile: FnGetSavedFile;
	getDir: FnGetSavedDir;
}

export interface GeneratorRequired {
	config: GeneratorConfig;
	generator: Generator<unknown, unknown>;
}

export type FnGetAnswer = (id: string) => GeneratorAnswer | null;
export type FnGetQuestion = (id: string) => GeneratorQuestion | null;
export type FnAddQuestion = (
	question: GeneratorQuestion,
	generator?: string
) => GeneratorPreparedQuestion;
export type FnAddAnswer = (answer: GeneratorAnswer) => GeneratorAnswer;
export type FnGetProp = <T>(id: string) => T | null;
export type FnCreateFile = (
	relativePath: string,
	content: string | Buffer | Record<string, unknown>
) => GeneratorFile;
export type FnUpdateFile = (
	relativePath: string,
	content: string | Buffer | Record<string, unknown>
) => GeneratorFile;
export type FnRemoveFile = (relativePath: string) => GeneratorFile;
export type FnGetFile = (relativePath: string) => GeneratorFile | null;
export type FnGetFiles = () => GeneratorFile[];
export type FnCreateDir = (relativePath: string) => GeneratorDir;
export type FnGetDir = (relativePath: string) => GeneratorDir | null;
export type FnGetDirs = () => GeneratorDir[];
export type FnRemoveDir = (relativePath: string) => GeneratorDir;
export type FnCreateMessage = (
	id: string,
	msg: Omit<GeneratorDefinedMessage, "id">
) => GeneratorMessage | null;
export type FnGetMessage = (id: string) => GeneratorMessage | null;
export type FnDeleteMessage = (id: string) => GeneratorMessage | null;
export type FnGetCustomData = <D>() => D;
export type FnUpdateCustomData = <D>(data: D) => D;
export type FnGetOutput = () => string | null;
export type FnSetOutput = (output: string) => void;

export type FnGetSavedFile = (relativePath: string) => GeneratorSaveFile | null;
export type FnGetSavedDir = (relativePath: string) => GeneratorSaveDir | null;

export type QuestionItem = {
	question: GeneratorQuestion;
	answer: GeneratorAnswer | null;
	type: AnswerType;
};

export enum AnswerType {
	Yes = "yes",
	No = "no",
	Skip = "skip",
}
