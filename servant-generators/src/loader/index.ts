import { loadGenerator } from "./generator";
import { loadGenerators } from "./generators";

import { GeneratorLoaded, GeneratorsList } from "../types";

import { connectGenerators } from "./connect";

export function loader(directories: Array<string>): Promise<GeneratorsList> {
	return new Promise((resolve) => {
		loadGenerators(directories).then(({ generators, errors }) => {
			Promise.all(generators.map((generator) => loadGenerator(generator))).then((data) => {
				const validGenerators = data.filter(({ errors }) => errors.length === 0);
				const invalidGenerators = data.filter(({ errors }) => errors.length > 0);
				const { generators, connectedErrors } = connectGenerators(
					validGenerators.map(asGeneratorLoaded)
				);

				resolve({
					errors: [
						...errors,
						...invalidGenerators.reduce((acc, { errors }) => [...acc, ...errors], []),
						...connectedErrors,
					],
					generators,
				});
			});
		});
	});
}

function asGeneratorLoaded<T, D>({
	generator,
	manifest,
	config,
}: Awaited<ReturnType<typeof loadGenerator>>): GeneratorLoaded<T, D> {
	return { generator, manifest, config, use: [] };
}
