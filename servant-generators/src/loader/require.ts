import { GeneratorRequired } from "../types/index";

export function requireGenerator(module: string, paths?: Array<string>): GeneratorRequired {
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-ignore
	const requireFn = typeof __webpack_require__ === "function" ? __non_webpack_require__ : require;
	return requireFn(module, { paths });
}
