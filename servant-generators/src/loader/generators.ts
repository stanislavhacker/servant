import * as glob from "glob";
import * as fs from "fs";
import * as path from "path";

import { GeneratorManifest } from "../types";
import { ManifestFile } from "../constants";

import { loadManifest } from "./manifest";

type LoadGeneratorsResult = {
	errors: Array<Error>;
	generators: Array<GeneratorManifest>;
};

export function loadGenerators(directories: Array<string>): Promise<LoadGeneratorsResult> {
	return new Promise((resolve) => {
		const errors: Array<Error> = [];

		//no directories
		if (directories.length === 0) {
			resolve({
				errors: [
					new Error(
						"No directories provided for loading generators for. You must entry at least one directory where generators are stored."
					),
				],
				generators: [],
			});
			return;
		}

		Promise.all(directories.map(readGeneratorsDir))
			.then((files) => {
				const manifests = files.reduce((acc, val) => [...acc, ...val], []);

				//no manifests
				if (manifests.length === 0) {
					resolve({
						errors: [
							new Error(
								"No generator manifests found in provided directories. You must entry at least one directory where generators are stored."
							),
						],
						generators: [],
					});
					return;
				}

				Promise.all(
					manifests.map((generator) =>
						loadManifestFile(generator)
							.then((data) => loadManifest(generator, data))
							.then((manifest) => manifest)
							.catch((err) => {
								errors.push(err);
							})
					)
				).then((data) => {
					const generators = data.filter(Boolean) as Array<GeneratorManifest>;

					//no generators and no errors
					if (errors.length === 0 && generators.length === 0) {
						resolve({
							errors: [
								new Error(
									"No generator manifests found in provided directories. You must entry at least one directory where generators are stored."
								),
							],
							generators: [],
						});
						return;
					}

					resolve({
						errors,
						generators,
					});
				});
			})
			.catch((err) => {
				resolve({
					errors: [err],
					generators: [],
				});
			});
	});
}

function loadManifestFile(manifestFile: string): Promise<unknown> {
	return new Promise((resolve, reject) => {
		fs.readFile(manifestFile, (err, data) => {
			if (data) {
				try {
					resolve(JSON.parse(data.toString()));
				} catch (e) {
					reject(e);
				}
				return;
			}
			reject(err);
		});
	});
}

function readGeneratorsDir(dir: string): Promise<string[]> {
	return new Promise((resolve, reject) => {
		const pattern = normalize(path.join(dir, "**", ManifestFile));
		glob(pattern, (err, files) => {
			if (err) {
				reject(err);
			} else {
				resolve(files);
			}
		});
	});
}

function normalize(path: string) {
	return path.replace(/\\/g, "/");
}
