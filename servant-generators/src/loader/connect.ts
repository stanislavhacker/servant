import { GeneratorLoaded } from "../types";

export const LocalPrefixes = ["local:"];

export function connectGenerators<T, D>(
	loadedGenerators: GeneratorLoaded<T, D>[]
): { generators: GeneratorLoaded<T, D>[]; connectedErrors: Error[] } {
	const connectedErrors: Error[] = [];
	const generators = loadedGenerators.map((generator) => {
		const { manifest } = generator;
		const use = (manifest.use || [])
			.map((name) => {
				const loaded = loadLocalGenerator(loadedGenerators, getLocalName(name));

				if (!loaded) {
					connectedErrors.push(
						new Error(
							`Generator ${manifest.name} uses "${name}" but it is not loaded correctly.`
						)
					);
				}
				return loaded;
			})
			.filter(Boolean) as GeneratorLoaded<T, D>[];

		return {
			...generator,
			use,
		};
	});

	return {
		generators,
		connectedErrors,
	};
}

function getLocalName(name: string): string | null {
	if (LocalPrefixes.some((prefix) => name.startsWith(prefix))) {
		return LocalPrefixes.reduce((acc, prefix) => acc.replace(prefix, ""), name);
	}
	return null;
}

function loadLocalGenerator<T, D>(
	arr: GeneratorLoaded<T, D>[],
	localName: string | null
): GeneratorLoaded<T, D> | null {
	if (!localName) {
		return null;
	}
	return arr.find(({ manifest }) => manifest.name === localName) || null;
}
