import { webpackBuild } from "@servant/servant-build-webpack";
import {
	Modules,
	ServantJson,
	PackageJson,
	EslintrcJson,
	PrettierrcJson,
	TrancorderJson,
} from "@servant/servant-data";
import * as path from "path";
import * as fs from "fs";

import { Generator, GeneratorConfig, GeneratorManifest } from "../types";
import { GeneratorEntry } from "../constants";
import { requireGenerator } from "./require";

type LoadGeneratorResult<T, D> = {
	errors: Array<Error>;
	manifest: GeneratorManifest;
	config: GeneratorConfig;
	generator: Generator<T, D>;
};

export function loadGenerator<T, D>(
	manifest: GeneratorManifest
): Promise<LoadGeneratorResult<T, D>> {
	return new Promise((resolve) => {
		const dir = path.dirname(manifest.path);
		const entry = path.join(dir, manifest.entry ?? GeneratorEntry);

		//check exists
		if (!fs.existsSync(entry)) {
			resolve(
				createFailedLoadGeneratorResult(
					manifest,
					new Error(
						`Can not find module "${entry}" for generator "${manifest.name}". Please check if this file exists.`
					)
				)
			);
			return;
		}

		//try to build it :)
		buildGenerator(manifest, dir, entry)
			.then((results) => {
				//handle errors from build
				if (results.errors.length > 0) {
					resolve(createFailedLoadGeneratorResult(manifest, results.errors[0]));
					return;
				}

				try {
					const generator = requireGenerator(results.output, [manifest.path]);

					if (!generator) {
						resolve(
							createFailedLoadGeneratorResult(
								manifest,
								new Error(
									`Generator "${manifest.name}" in "${manifest.path}" is not a module.`
								)
							)
						);
						return;
					}
					if (!generator.config) {
						resolve(
							createFailedLoadGeneratorResult(
								manifest,
								new Error(
									`Generator "${manifest.name}" in "${manifest.path}" does not have a "config" property.`
								)
							)
						);
						return;
					}
					if (!generator.generator) {
						resolve(
							createFailedLoadGeneratorResult(
								manifest,
								new Error(
									`Generator "${manifest.name}" in "${manifest.path}" does not have a "generator" function.`
								)
							)
						);
						return;
					}

					resolve({
						errors: [],
						manifest,
						...generator,
					} as LoadGeneratorResult<T, D>);
				} catch (err) {
					resolve(createFailedLoadGeneratorResult(manifest, err));
				}
			})
			.catch((error) => {
				resolve(createFailedLoadGeneratorResult(manifest, error));
			});
	});
}

function createFailedLoadGeneratorResult<T, D>(
	manifest: GeneratorManifest,
	err: Error
): LoadGeneratorResult<T, D> {
	return {
		manifest,
		config: {},
		errors: [err],
		generator: emptyGenerator(),
	};
}

function emptyGenerator<T, D>(): Generator<T, D> {
	return (_1, _2, next) => {
		next("error");
	};
}

function buildGenerator(
	manifest: GeneratorManifest,
	dir: string,
	entry: string
): ReturnType<typeof webpackBuild> {
	//NOTE: Generators that are distributed as node modules can have been compiled already.
	if (manifest.compiled) {
		return Promise.resolve({
			errors: [],
			output: entry,
		});
	}
	return webpackBuild(dir, entry, createModule(dir, entry), true, false);
}

function createModule(dir: string, entry: string): Modules.ModuleInfo {
	const basename = path.basename(entry, path.extname(entry));

	const servantJsonContent = ServantJson.create("generator");
	servantJsonContent.entry = entry;
	servantJsonContent.target = ServantJson.ModuleTarget.node;
	servantJsonContent.output = {
		filename: `./${basename}.min.js`,
		directory: "./",
	} as ServantJson.ServantOutput;

	const packageJson = {
		path: path.join(dir, PackageJson.PACKAGE_JSON),
		main: true,
		cwd: dir,
		content: PackageJson.map(PackageJson.create("generator", "1.0.0")),
	};
	const servantJson = {
		path: path.join(dir, ServantJson.SERVANT_JSON),
		cwd: dir,
		content: ServantJson.fill(servantJsonContent, packageJson),
	};

	return Modules.info(
		packageJson,
		servantJson,
		{
			path: path.join(dir, EslintrcJson.ESLINTRC_JSON),
			cwd: dir,
			content: null,
		},
		{
			path: path.join(dir, PrettierrcJson.PRETTIERRC_JSON),
			cwd: dir,
			content: null,
		},
		{
			path: path.join(dir, TrancorderJson.TRANCORDER_JSON),
			cwd: dir,
			content: null,
		},
		[],
		Modules.directories(),
		true
	);
}
