import { GeneratorManifest } from "../types";

export function loadManifest(path: string, data: unknown): Promise<GeneratorManifest | null> {
	return new Promise((resolve, reject) => {
		if (typeof data === "object") {
			const manifest = data as GeneratorManifest;

			if (isManifestValid(manifest)) {
				resolve({
					...manifest,
					path,
				});
				return;
			}

			//no manifest intended for servant-generator
			if (manifest.engine !== "servant-generator") {
				resolve(null);
				return;
			}

			//validate missing items
			if (!manifest.name) {
				reject(new Error(`Manifest file in ${path} is missing name.`));
				return;
			}
			if (!manifest.description) {
				reject(new Error(`Manifest file in ${path} is missing description.`));
				return;
			}

			reject(new Error(`Manifest file in ${path} is invalid.`));
			return;
		}

		//no manifest intended for servant-generator
		resolve(null);
	});
}

function isManifestValid(data: GeneratorManifest) {
	return Boolean(data.engine === "servant-generator" && data.name && data.description);
}
