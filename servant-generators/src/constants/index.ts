import { GeneratorResults } from "../types";

export const ManifestFile = "manifest.json";
export const GeneratorEntry = "./index.js";
export const GeneratorEmptyResults: GeneratorResults<unknown> = {
	preparedQuestions: [],
	questions: [],
	answers: [],
	messages: [],
	errors: [],
	directories: [],
	files: [],
	status: "success",
};
