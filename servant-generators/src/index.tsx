import * as React from "react";
import { render } from "ink";

import { GeneratorEmptyResults } from "./constants";
import { resolveMain } from "./resolve";
import { getArguments } from "./cli";
import { loader } from "./loader";
import { runner, multiRunner } from "./runner";
import { saver } from "./saver";
import {
	GeneratorProps,
	Generator,
	GeneratorsProps,
	Generators,
	GeneratedProps,
	Generated,
	Main,
	//hooks
	useGeneratorsList,
} from "./view";

/**
 * @public
 */
export * from "./types";

/**
 * @public
 */
export const api = {
	loader,
	saver,
	runner,
	multiRunner,
};

/**
 * @public
 */
export type { GeneratorProps, GeneratorsProps, GeneratedProps };

/**
 * @public
 */
export const view = {
	//view
	Generator,
	Generators,
	Generated,
	Main,
};

/**
 * @public
 */
export {
	//const
	GeneratorEmptyResults,
	//hooks
	useGeneratorsList,
};

if (resolveMain() === module) {
	const data = getArguments(process);
	render(
		<Main
			dirs={data.dirs}
			output={data.output}
			generator={data.generator}
			debug={data.debug}
			help={data.help}
		/>
	);
}
