import * as Module from "module";

export function resolveMain(): Module | undefined {
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-ignore
	const requireFn = typeof __webpack_require__ === "function" ? __non_webpack_require__ : require;
	return requireFn.main;
}
