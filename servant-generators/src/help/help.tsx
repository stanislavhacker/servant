import * as React from "react";
import { Box, Text } from "ink";

export const Help: React.StatelessComponent = () => {
	return (
		<Box flexDirection="column" marginTop={1}>
			<Box flexDirection="row">
				<Text color="green" bold>
					Generators help
				</Text>
			</Box>
			<Box flexDirection="row" marginTop={1}>
				<Text italic color="gray">
					sg
				</Text>
				<Text> </Text>
				<Text color="blueBright">
					{"<dir1> <dir2> ... <dirN>"}
					<Text> </Text>
				</Text>
				<Text color="greenBright">
					--output {"<output_directory>"}
					<Text> </Text>
				</Text>
				<Text color="cyanBright">
					--generator {"<generator>"}
					<Text> </Text>
				</Text>
				<Text color="yellowBright">
					--debug
					<Text> </Text>
				</Text>
				<Text color="yellowBright">
					--help
					<Text> </Text>
				</Text>
			</Box>
			<Box flexDirection="column" marginLeft={1}>
				<Box flexDirection="row" marginTop={1}>
					<Text>
						<Text bold>1.</Text>{" "}
						<Text color="blueBright">{"<dir1> <dir2> ... <dirN>"}</Text>: Define all
						directories where generators are stored.
					</Text>
				</Box>
				<Box flexDirection="row">
					<Text>
						<Text bold>2.</Text>{" "}
						<Text color="greenBright">--output {"<output_directory>"}</Text>: Define
						directory, where generated content will bre saved.
					</Text>
				</Box>
				<Box flexDirection="row">
					<Text>
						<Text bold>3.</Text>{" "}
						<Text color="cyanBright">--generator {"<generator>"}</Text>: Define name of
						generator if is known to run it without showing generators list.
					</Text>
				</Box>
				<Box flexDirection="row">
					<Text>
						<Text bold>4.</Text> <Text color="yellowBright">--debug</Text>: Turn on
						debug mode with more info and detailed errors report.
					</Text>
				</Box>
				<Box flexDirection="row">
					<Text>
						<Text bold>5.</Text> <Text color="yellowBright">--help</Text>: Show this
						help screen.
					</Text>
				</Box>
			</Box>
		</Box>
	);
};
