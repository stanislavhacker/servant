import {
	AnswerType,
	GeneratorAnswer,
	GeneratorPreparedQuestion,
	GeneratorPreparedQuestions,
	GeneratorQuestion,
	GeneratorQuestions,
} from "../types";

import { getAnswer } from "./answers";
import { meetCondition } from "./condition";

export function getQuestion(questions: GeneratorQuestion[], id: string): GeneratorQuestion | null {
	return questions.find((question) => question.id === id) ?? null;
}

export function hasAnswer(
	questions: GeneratorQuestion[],
	answers: GeneratorAnswer[],
	id: string
): AnswerType {
	const question = getQuestion(questions, id);

	if (!question) {
		return AnswerType.Skip;
	}

	const meet = meetCondition(questions, answers, question.condition);
	const answer = getAnswer(answers, question.id);

	if (!meet) {
		return AnswerType.Skip;
	}
	return answer ? AnswerType.Yes : AnswerType.No;
}

export function addQuestion(
	questions: GeneratorPreparedQuestions,
	question: GeneratorQuestion,
	generator?: string
): GeneratorPreparedQuestion {
	const prepared: GeneratorPreparedQuestion = {
		...question,
		generator,
		prepared: true,
	};

	questions.push(prepared);
	return prepared;
}

export type ExtractedPreparedQuestions = {
	remainingQuestions: GeneratorPreparedQuestions;
	generatorQuestions: GeneratorQuestions;
};

export function extractPreparedQuestions(
	preparedQuestions: GeneratorPreparedQuestions,
	name: string
): ExtractedPreparedQuestions {
	const myQuestions = preparedQuestions.filter(
		(q) => q.prepared && (q.generator === name || !q.generator)
	);
	const remainingQuestions = preparedQuestions.filter((q) => myQuestions.indexOf(q) === -1);

	const generatorQuestions = myQuestions.map((q) => {
		const question = { ...q } as Partial<GeneratorPreparedQuestion>;

		delete question.prepared;
		delete question.generator;

		return question as GeneratorQuestion;
	});

	return {
		remainingQuestions,
		generatorQuestions,
	};
}
