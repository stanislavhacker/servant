import { GeneratorAnswer, GeneratorCondition, GeneratorQuestion } from "../types";

import { getAnswer } from "./answers";

export function meetCondition(
	questions: GeneratorQuestion[],
	answers: GeneratorAnswer[],
	condition?: GeneratorCondition
): boolean {
	if (!condition) {
		return true;
	}

	const { or, and } = condition;

	const meetAnd = and ? meetAndCond(questions, answers, and) : true;
	const meetOr = or ? meetOrCond(questions, answers, or) : true;

	return meetAnd && meetOr;
}

function meetAndCond(
	questions: GeneratorQuestion[],
	answers: GeneratorAnswer[],
	data: NonNullable<GeneratorCondition["and"]>
) {
	return Object.keys(data).every((id) => meetCond(questions, answers, data, id));
}

function meetOrCond(
	questions: GeneratorQuestion[],
	answers: GeneratorAnswer[],
	data: NonNullable<GeneratorCondition["or"]>
) {
	return Object.keys(data).some((id) => meetCond(questions, answers, data, id));
}

function meetCond(
	questions: GeneratorQuestion[],
	answers: GeneratorAnswer[],
	data: NonNullable<GeneratorCondition["or"] | GeneratorCondition["and"]>,
	id: string
) {
	const item = data[id];
	//simple answer
	if (typeof item === "string") {
		const answer = getAnswer(answers, id);
		return answer && answer.value === data[id];
	}
	//complex condition
	return meetCondition(questions, answers, item);
}
