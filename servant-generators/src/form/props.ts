export function getProp<T, V>(prop: T, id: string): V {
	return prop[id] as V;
}
