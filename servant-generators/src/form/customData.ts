import { GeneratorResults } from "../types";

export function getCustomData<D>(next: GeneratorResults<D>): D | undefined {
	return next.customData;
}

export function updateCustomData<D>(next: GeneratorResults<D>, customData: D): D {
	next.customData = {
		...(next.customData || {}),
		...customData,
	};
	return next.customData;
}
