import {
	GeneratorAnswer,
	GeneratorDefinedMessage,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorQuestion,
} from "../types";

import { meetCondition } from "./condition";

export function evaluateMessages(
	questions: GeneratorQuestion[],
	answers: GeneratorAnswer[],
	messages: GeneratorMessage[]
) {
	return messages.filter((message) => meetCondition(questions, answers, message.condition));
}

export function convertFromDefined(
	messages: GeneratorDefinedMessage[],
	created: GeneratorManifest
): GeneratorMessage[] {
	return messages.map((message) => ({
		...message,
		created,
		updated: [],
	}));
}
