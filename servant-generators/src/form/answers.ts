import { GeneratorAnswer } from "../types";

export function getAnswer(answers: GeneratorAnswer[], id: string): GeneratorAnswer | null {
	return answers.find((answer) => answer.id === id) ?? null;
}

export function createAnswer(id: string, value: string): GeneratorAnswer {
	return {
		id,
		value,
	};
}

export function addAnswer(answers: GeneratorAnswer[], answer: GeneratorAnswer): GeneratorAnswer {
	answers.push(answer);
	return answer;
}
