import * as path from "path";

import { GeneratorSaveFile, GeneratorSaveDir, GeneratorSaveResults } from "../types";

export function getFile(
	results: GeneratorSaveResults,
	relativePath: string
): GeneratorSaveFile | null {
	const file = findFile(relativePath, results);

	if (file) {
		return { ...file };
	}
	return null;
}

export function getDir(
	results: GeneratorSaveResults,
	relativePath: string
): GeneratorSaveDir | null {
	const dir = findDir(relativePath, results);

	if (dir) {
		return { ...dir };
	}
	return null;
}

function findFile(relativePth: string, results: GeneratorSaveResults) {
	const relativePath = path.normalize(relativePth);
	return results.files.find((file) => path.normalize(file.relativePath) === relativePath) || null;
}

function findDir(relativePth: string, results: GeneratorSaveResults) {
	const relativePath = path.normalize(relativePth);
	return (
		results.directories.find((file) => path.normalize(file.relativePath) === relativePath) ||
		null
	);
}
