import * as path from "path";
import * as fs from "fs";

import {
	GeneratorAnswer,
	GeneratorMessage,
	GeneratorQuestion,
	GeneratorResults,
	GeneratorSaveDir,
	GeneratorSaveFile,
	GeneratorSaveResults,
	GeneratorSaverResults,
	GeneratorSavedHandlers,
} from "../types";
import { evaluateMessages } from "../form";

import { getFile, getDir } from "./files";

export function saver<D>(
	into: string | undefined,
	results: GeneratorResults<D>
): Promise<GeneratorSaverResults> {
	return new Promise<GeneratorSaverResults>((resolve) => {
		const errors: Array<Error> = [];

		Promise.resolve()
			.then(() => handlerErrorStatus(results, errors))
			.then(() => handlerIntoExists(into, errors))
			.then(() => {
				const { files, directories, messages, questions, answers } = results;

				//make save if there is no error
				if (errors.length === 0 && into) {
					const dirs = directories.map(
						(dir) =>
							({
								...dir,
								absolutePath: path.join(into, dir.relativePath),
							} as GeneratorSaveDir)
					);
					const fls = files.map(
						(file) =>
							({
								...file,
								absolutePath: path.join(into, file.relativePath),
							} as GeneratorSaveFile)
					);

					const dirsPromises = dirs.map((dir) => mkdir(dir.absolutePath));
					const filesPromises = fls.map((file) => write(file.absolutePath, file.content));

					Promise.all([...dirsPromises, ...filesPromises])
						.then(() => {
							const paths = [...dirs, ...fls].map((item) => item.absolutePath);
							const results = getSaveResults(
								questions,
								answers,
								errors,
								paths,
								fls,
								dirs,
								messages
							);

							resolve({
								results,
								fn: getFileHandlers(results),
							});
						})
						.catch((err) => {
							const results = getSaveResults(
								questions,
								answers,
								[...errors, err],
								[],
								fls,
								dirs,
								messages,
								"error"
							);
							resolve({
								results,
								fn: getFileHandlers(results),
							});
						});

					//resolve with errors
				} else {
					const results = getSaveResults(
						questions,
						answers,
						errors,
						[],
						[],
						[],
						messages,
						"error"
					);

					resolve({
						results,
						fn: getFileHandlers(results),
					});
				}
			});
	});
}

function handlerErrorStatus(
	results: GeneratorResults<unknown>,
	errors: Array<Error>
): Promise<void> {
	return new Promise((resolve) => {
		//error status in results
		if (results.status === "error") {
			errors.push(
				new Error(
					`Generator failed to save files into folder, because results ends with "error" status.`
				)
			);
		}
		//errors in results
		if (results.errors.length > 0) {
			errors.push(...results.errors);
		}
		resolve();
	});
}

function handlerIntoExists(into: string | undefined, errors: Array<Error>): Promise<void> {
	return new Promise((resolve) => {
		//output not defined
		if (!into) {
			errors.push(new Error(`Output directory is not defined.`));
			resolve();
			return;
		}
		//check directory
		return exists(into).then(({ exists, directory }) => {
			//not directory
			if (exists && !directory) {
				errors.push(new Error(`Path "${into}" is not a directory.`));
				resolve();
				return;
			}
			//try create
			mkdir(into)
				.then(() => resolve())
				.catch((err: Error) => {
					errors.push(
						new Error(`Can not create directory "${into}" due to: ${err.name}`)
					);
					resolve();
				});
		});
	});
}

function exists(path: string): Promise<{ exists: boolean; directory: boolean }> {
	return new Promise((resolve) => {
		fs.stat(path, (err, stats) => {
			if (stats) {
				resolve({ exists: true, directory: stats.isDirectory() });
				return;
			}
			resolve({ exists: false, directory: false });
		});
	});
}

function mkdir(pth: string): Promise<void> {
	return new Promise((resolve, reject) => {
		fs.mkdir(pth, { recursive: true }, (err) => {
			if (!err) {
				resolve();
				return;
			}
			reject(err);
		});
	});
}

function write(pth: string, data: string | Buffer | Record<string, unknown>): Promise<void> {
	return new Promise((resolve, reject) => {
		const dir = path.dirname(pth);
		mkdir(dir)
			.then(() => {
				fs.writeFile(pth, getSaveData(data), (err) => {
					if (!err) {
						resolve();
						return;
					}
					reject(err);
				});
			})
			.catch(reject);
	});
}

function getSaveData(data: string | Buffer | Record<string, unknown>): string | Buffer {
	if (Buffer.isBuffer(data)) {
		return data;
	}
	if (typeof data === "string") {
		return data;
	}
	return JSON.stringify(data, null, 2);
}

function getSaveResults(
	questions: GeneratorQuestion[],
	answers: GeneratorAnswer[],
	errors: Array<Error>,
	paths: string[],
	files: GeneratorSaveFile[],
	directories: GeneratorSaveDir[],
	messages: GeneratorMessage[],
	status: GeneratorSaveResults["status"] = "success"
): GeneratorSaveResults {
	return {
		questions,
		answers,
		errors,
		paths,
		status,
		files,
		directories,
		messages: evaluateMessages(questions, answers, messages),
	};
}

function getFileHandlers(results: GeneratorSaveResults): GeneratorSavedHandlers {
	return {
		getFile: (path) => getFile(results, path),
		getDir: (path) => getDir(results, path),
	};
}
