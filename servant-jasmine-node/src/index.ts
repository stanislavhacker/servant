import "source-map-support/register";
import { TestsJson } from "@servant/servant-data";
import { tests, TestsProgress } from "./node";
import { TestsResults, TestsSuit, TestsSpec, TestsSpecStatus, TestsItemType } from "./jasmine";

export { tests, TestsProgress, TestsResults, TestsSuit, TestsSpec, TestsItemType, TestsSpecStatus };

process.on("message", (mes: Partial<TestsJson.TestsJson>) => {
	const message = TestsJson.remap(mes);
	tests(message.cwd, message.entry, message.files, onProgress).then(onProgress);
});

function onProgress(progress: TestsProgress) {
	process.send && process.send(progress);
}
