import { testNode, TestsResults } from "./jasmine";

export type TestsProgress = TestsResults & {
	type: "ok" | "warn" | "fail";
};

export function tests(
	cwd: string | null,
	entry: string | null,
	files: Array<string>,
	progress: (progress: TestsProgress) => void
): Promise<TestsProgress> {
	return new Promise((resolve) => {
		testNode(cwd, entry, files, (result: TestsResults) => {
			progress(returnResult(result));
		}).then((result: TestsResults) => {
			resolve(returnResult(result));
		});
	});
}

function returnResult(result: TestsResults): TestsProgress {
	let type: "ok" | "warn" | "fail" = "ok";

	//pending or excluded spec, mark as warning
	if (result.summary.pending || result.summary.excluded) {
		type = "warn";
	}
	//not passed => fail
	if (!result.summary.passed) {
		type = "fail";
	}
	return {
		type: type,
		...result,
	};
}
