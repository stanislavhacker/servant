// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as Jasmine from "jasmine";

export interface TestsResults {
	summary: {
		time: [number, number];
		testsCount: number;
		completedCount: number;
		failedCount: number;
		excludedCount: number;
		pendingCount: number;
		passed: boolean;
		excluded: boolean;
		pending: boolean;
		done: boolean;
		random: {
			state: boolean;
			seed: number | string;
		};
	};
	error: string | null;
	results: {
		[key: string]: TestsSuit | TestsSpec;
	};
	failed: Array<TestsSpec | TestsSuit>;
	excluded: Array<TestsSpec | TestsSuit>;
	pending: Array<TestsSpec | TestsSuit>;
}

export enum TestsItemType {
	Suit,
	Spec,
}
export enum TestsSpecStatus {
	Failed = "failed",
	Passed = "passed",
	Excluded = "excluded",
	Pending = "pending",
}
export interface TestsSuit {
	name: string;
	fullName: string;
	type: TestsItemType;
	suits: {
		[key: string]: TestsSuit | TestsSpec;
	};
	failed: Array<SpecExpectation>;
	duration: null | number;
	done: boolean;
	status: TestsSpecStatus;
}
export interface TestsSpec {
	name: string;
	fullName: string;
	type: TestsItemType;
	status: TestsSpecStatus;
	pending: string | null;
	passed: Array<SpecExpectation>;
	failed: Array<SpecExpectation>;
	done: boolean;
}
export interface SpecExpectation {
	actual?: unknown;
	expected?: unknown;
	matcherName: string;
	message: string;
	passed: boolean;
	stack: string;
}

export function testNode(
	cwd: string | null,
	entry: string | null,
	tests: Array<string>,
	progress: (results: TestsResults) => void
): Promise<TestsResults> {
	return new Promise((resolve) => {
		const results = createTestsResults();
		const time = process.hrtime();

		//no entry or cwd
		if (entry === null || cwd === null) {
			results.summary.done = true;
			results.summary.passed = false;
			results.summary.time = process.hrtime(time);
			results.error = `You must specify these parameters: 'entry', 'cwd'.`;
			resolve(results);
			return;
		}

		//jasmine runner
		const runner = createRunner(entry, tests);
		runner.addReporter(
			createReporter(cwd, entry, results, (res: TestsResults) => {
				progress(res);
			})
		);
		//run all test!
		runner
			.execute()
			.then((status) => {
				results.summary.random.state = status.order.random;
				results.summary.random.seed = status.order.seed;
				results.summary.done = true;
			})
			.catch((err: Error) => {
				results.error = err.message;
				results.summary.done = true;
			})
			.finally(() => {
				results.summary.time = process.hrtime(time);
				progress(results);
				resolve(results);
			});
	});
}

function createRunner(entry: string, tests: Array<string>): Jasmine {
	const runner = new Jasmine({
		projectBaseDir: entry,
	});

	runner.loadConfig({
		spec_dir: "./",
		spec_files: tests,
	});
	runner.configureDefaultReporter({
		print: function () {
			//disable print
		},
		showColors: false,
	});
	runner.exitOnCompletion = false;

	return runner;
}

function createReporter(
	cwd: string,
	entry: string,
	results: TestsResults,
	progress: (results: TestsResults) => void
): jasmine.CustomReporter {
	const dirs: Array<TestsResults["results"]> = [results.results];
	const suits: { [key: string]: TestsSuit } = {};
	const specs: { [key: string]: TestsSpec } = {};

	return {
		jasmineStarted: (suiteInfo) => {
			results.summary.testsCount = suiteInfo.totalSpecsDefined;
			progress(results);
		},

		suiteStarted: (result) => {
			const suit = createTestsSuit(result.fullName, result.description);

			suits[result.id] = suit;
			dirs[0][result.id] = suit;
			dirs.unshift(suit.suits);
			progress(results);
		},

		specStarted: (result) => {
			const spec = createTestsSpec(result.fullName, result.description);

			dirs[0][result.id] = spec;
			specs[result.id] = spec;
			progress(results);
		},

		specDone: (result) => {
			const spec = specs[result.id] as TestsSpec;

			spec.failed = normalizeSpecs(cwd, result.failedExpectations);
			spec.passed = normalizeSpecs(cwd, result.passedExpectations);
			spec.pending = result.pendingReason;
			spec.status = result.status as TestsSpecStatus;
			spec.done = true;
			//normalize
			//save global
			results.summary.completedCount++;
			if (spec.status === TestsSpecStatus.Failed) {
				results.summary.failedCount++;
				results.failed.push(spec);
			}
			if (spec.status === TestsSpecStatus.Pending) {
				results.summary.pendingCount++;
				results.pending.push(spec);
			}
			if (spec.status === TestsSpecStatus.Excluded) {
				results.summary.excludedCount++;
				results.excluded.push(spec);
			}
			progress(results);
		},

		suiteDone: (result) => {
			const suit = suits[result.id] as TestsSuit;

			dirs.shift();
			suit.failed = normalizeSpecs(cwd, result.failedExpectations);
			suit.duration = result.duration;
			suit.status = result.status as TestsSpecStatus;
			suit.done = true;
			//save global
			if (suit.status === TestsSpecStatus.Failed) {
				results.failed.push(suit);
			}
			if (suit.status === TestsSpecStatus.Pending) {
				results.pending.push(suit);
			}
			if (suit.status === TestsSpecStatus.Excluded) {
				results.excluded.push(suit);
			}
			progress(results);
		},

		jasmineDone: () => {
			results.summary.excluded = results.excluded.length > 0;
			results.summary.pending = results.pending.length > 0;
			results.summary.passed = results.failed.length === 0;
			progress(results);
		},
	};
}

function createTestsSuit(fullName: string, name: string): TestsSuit {
	return {
		name: name,
		fullName: fullName,
		type: TestsItemType.Suit,
		suits: {},
		done: false,
		failed: [],
		duration: null,
		status: TestsSpecStatus.Failed,
	};
}

function createTestsSpec(fullName: string, name: string): TestsSpec {
	return {
		name: name,
		fullName: fullName,
		type: TestsItemType.Spec,
		failed: [],
		passed: [],
		pending: null,
		status: TestsSpecStatus.Failed,
		done: false,
	};
}

function createTestsResults(): TestsResults {
	return {
		summary: {
			testsCount: 0,
			completedCount: 0,
			failedCount: 0,
			excludedCount: 0,
			pendingCount: 0,
			time: [0, 0],
			passed: true,
			excluded: false,
			pending: false,
			done: false,
			random: {
				state: false,
				seed: "none",
			},
		},
		results: {},
		failed: [],
		excluded: [],
		pending: [],
		error: null,
	};
}

function normalizeSpecs(cwd: string, specs: Array<SpecExpectation>): Array<SpecExpectation> {
	return specs.map((spec) => {
		return normalizeStackPath(cwd, spec);
	});
}

function normalizeStackPath(cwd: string, expect: SpecExpectation): SpecExpectation {
	if (expect.stack) {
		expect.stack = expect.stack.replace(new RegExp(escapeRegExp(cwd + "\\"), "g"), "");
		expect.stack = expect.stack.replace(new RegExp(escapeRegExp("\\"), "g"), "/");
	}
	if (expect.expected) {
		expect.expected = removeCircular(expect.expected);
	}
	if (expect.actual) {
		expect.actual = removeCircular(expect.actual);
	}
	return expect;
}

function escapeRegExp(str: string): string {
	return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}

function removeCircular(obj: object): object {
	const stackSet = new Set();

	function repair(obj: object) {
		//no object
		if (obj && typeof obj !== "object") {
			return obj;
		}
		//detected
		if (stackSet.has(obj)) {
			return "<circular-reference>";
		}

		stackSet.add(obj);

		const newObj = {};
		for (const k in obj) {
			if (Object.prototype.hasOwnProperty.call(obj, k)) {
				newObj[k] = repair(obj[k]);
			}
		}

		stackSet.delete(obj);
		return newObj;
	}

	return repair(obj);
}
