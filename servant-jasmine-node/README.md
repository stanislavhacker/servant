# ![Servant][logo] Servant jasmine node

Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

Servant jasmine node is module for Servant build tool that is intended for running tests in node
environment. Module can be included as module `@servant/servant-jasmine-node` or can be forked
from main nodejs process. If you want to run it as process, you need send message with data to
say module that you need some work.

```typescript
import { ServantJson, PackageJson } from "@servant/servant-data";

export type TestsJson = {
	cwd: string;
	entry: string | null;
	files: Array<string>;
	extensions: Array<string>;
	externals: Array<string>;
	packageJson: PackageJson.PackageJsonInfo | null;
	servantJson: ServantJson.ServantJsonInfo | null;
};
```

Message must contain `cwd` (working directory path) and `entry` that is used as a base directory
for loading test from.

Property `files` is array of glob patterns or array of files with tests. Modules send message on
every tests progress. These data are `TestsProgress` and you can read them from message
event in parent process.

Property `extensions` is array of extensions that are resolved by tests.

Property `externals` is a list of external libraries that are necessary for running tests but there
are not a part of your module.

Property `packageJson` is PackageJsonInfo for package.json.

Property `servantJson` is ServantJsonInfo for servant.json.

### License

[Licensed under GPLv3][license]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
[1]: https://docs.npmjs.com/files/package.json
[2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
[3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
[4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
[5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
[license]: https://gitlab.com/stanislavhacker/servant/blob/master/LICENSE
