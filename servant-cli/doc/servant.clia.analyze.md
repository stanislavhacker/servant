Servant command: `analyze`
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

##### Examples:

> `servant analyze`
>
> `servant analyze --entry "/path/to/project"`
>
> `servant analyze --debug`
>
> `servant analyze --changed`
>
> `servant install --only "module-name"`

#### Preview

![Analyze command preview][preview]

#### Description

Analyze command is used to get some statistics information about project. These data can be than used for some other 
 statistics researches and planning. This information are basically important for developers and testers. Can be showed
 in process console and exported as report for another processing.

#### Basic features of analyze

1. **Types of dependencies for every module** - In analyze report, there is list of internal, production, developer, peer and
    optional dependencies and size of every dependency. This information is module based, but in report you can see list
    of all dependencies on one place.
2. **Sizes info** - Report shows size of whole **module bundle** and sizes of **source** and **tests** files. As a bonus you 
    can see final downloaded size, that will be probably download during `npm install` command. This is handy, because you 
    will know final size of your module after publish. **This downloaded calculated size exclude bundled dependencies**!
3. **Lines count of sources and tests** - report also show lines of code for all types of codes in module. For now its shows
    lines for **typescript**, **javascript**, **less**, **sass** and **css**. **These lines are clean so its remove empty lines
    and lines, that are related to comments.**
4. **Project validation results** - There are also validation results on  the end of report. It's possible to find not installed
    modules and also modules with multiple versions across all modules in whole project. That's basically mean, that there is
    something invalid in project.

> **Note**
>
> Flag `--production` have not any special meaning for this command.

#### Universal settings

- [`--entry <path>`][entry]  - see [--entry][entry] for more info.
- [`--debug`][debug]  - see [--debug][debug] for more info.
- [`--production`][production]  - see [--production][production] for more info.
- [`--changed`][changed]  - see [--changed][changed] for more info.
- [`--only <module-name>`][only]  - see [--only][only] for more info.
- [`--init`][init]  - see [--init][init] for more info.
- [`--dependencies`][init]  - see [--dependencies][dependencies] for more info.

[1]: https://docs.npmjs.com/files/package.json
[2]: ../../servant/doc/servant.json.md
[3]: ../../servant/doc/servant.nodejs.md
[4]: servant.clia.md
[entry]: servant.clia.flags.md#-entry-path
[debug]: servant.clia.flags.md#-debug
[production]: servant.clia.flags.md#-production
[changed]: servant.clia.flags.md#-changed
[only]: servant.clia.flags.md#-only-module-name
[init]: servant.clia.flags.md#-init
[dependencies]: servant.clia.flags.md#-dependencies

[preview]: ../../assets/commands/command.analyze.gif