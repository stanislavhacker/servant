Servant command: `tests`
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

##### Examples:

 > `servant tests --gui` 
 > 
 > `servant tests --entry "/path/to/project"`  
 > 
 > `servant tests --browsers "Webkit, Firefox"`
 > 
 > `servant tests --browsers "Webkit, Firefox" --devices "BlackBerry Z30, Galaxy Note 3 landscape"`
 > 
 > `servant tests --only "module name"`
 > 
 > `servant tests --only "module name" --dependencies`
 
#### Preview

 ![Tests command preview][preview]
 
#### Description

Command `tests` is used to run you tests for whole project. It depends on your settings in *[servant.json][2].
 If you have defined target as `node` or `node-cli` it will runs tests in nodejs environment. If you have
 define `web` Servant will run tests in browsers.

 > **Note**
 > 
 > Flags `--production` have not any special meaning for this command. :wink: 

 
> **Caveats about `devDependencies`**
>
> If you specify `devDependencies` at your main `package.json` (root of project) and this root is also mark as module 
> (property `modules` in `servant.json` contains `./`) these `devDependencies` will not be loaded by Servant. This is because
> we expected that `devDependencies` in main `package.json` includes lots of dev tools (and also "@servant/servant-cli").
 
#### Using [playwright][playwright] module

Servant can use [playwright][playwright] module from [Microsoft][ms], that is used for running tests against more 
 browsers. **Default behaviour is that Servant run tests only in installed Chrome.** But with property `testing` defined in
 [servant.json][2] or by command line switchers (`--browsers`, `--devices`) Servant can run tests in more browsers 
 and on [defined devices][devices].
 
 > **Caveats**
 >
 > This is special feature that use [playwright][playwright] module from [Microsoft][ms] to run tests against more
 > browsers and you need install optional module `@servant/servant-playwright` to get this working. So after install
 > Servant run `npm install @servant/servant-playwright --save-dev` to install this optional module. **Be careful 
 > because this module is big (about 300 MB to download)!**

 ![Playwright preview][playwright-preview]

#### Universal settings

 -  [`--entry <path>`][entry]  - see [--entry][entry] for more info.
 -  [`--debug`][debug]  - see [--debug][debug] for more info.
 -  [`--production`][production]  - see [--production][production] for more info.
 -  [`--changed`][changed]  - see [--changed][changed] for more info.
 -  [`--only <module-name>`][only]  - see [--only][only] for more info.
 -  [`--init`][init]  - see [--init][init] for more info.
 -  [`--dependencies`][init]  - see [--dependencies][dependencies] for more info.
 
#### Specific settings

Tests command can used gui flag, browsers and devices.

#### `--gui`

Define that browsers will be running in gui mode not in headless. It's recommended for debugging.

*Example:*
 > `servant tests --gui`

#### `--browsers`

Define browsers in witch Servant runs tests. This switch override browsers defined in [servant.json][2].

*Example:*
 > `servant tests --browsers "Webkit, Firefox"`

#### `--devices`

Define devices in witch Servant runs tests. This switch override devices defined in [servant.json][2].

*Example:*
 > `servant tests --devices "BlackBerry Z30, Galaxy Note 3 landscape"`

 [playwright]: https://www.npmjs.com/package/playwright
 [ms]: https://github.com/microsoft
 [devices]: https://github.com/Microsoft/playwright/blob/master/src/deviceDescriptors.ts

 [1]: https://docs.npmjs.com/files/package.json
 [2]: ../../servant/doc/servant.json.md
 [3]: ../../servant/doc/servant.nodejs.md
 [4]: servant.clia.md
 [entry]: servant.clia.flags.md#-entry-path
 [debug]: servant.clia.flags.md#-debug
 [production]: servant.clia.flags.md#-production
 [changed]: servant.clia.flags.md#-changed
 [only]: servant.clia.flags.md#-only-module-name
 [init]: servant.clia.flags.md#-init
 [dependencies]: servant.clia.flags.md#-dependencies
 
 [preview]: ../../assets/commands/command.tests.gif
 [playwright-preview]: ../../assets/features/servant.playwright.gif
 