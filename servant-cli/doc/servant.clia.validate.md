Servant command: `validate`
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

##### Examples:

> `servant validate`
>
> `servant validate --entry "/path/to/project"`
>
> `servant validate --debug`
>
> `servant validate --changed`
>
> `servant validate --fix`

#### Preview

![Validate command preview][preview]

#### Description

Command `validate` is used to validate your project if is valid for rules.
 Rules can be defined by all supported validators. There is a list of currently supported
 validators, that can changed in future releases.

 1. **Eslint** - validating ts, js files
 2. **Prettier** - validating ts, js + all supported files by prettier and configuration
 3. **Trancorder** - validating ts, js and localisation files

### Validator **"eslint"**

#### How to enable eslint validation?

Eslint validator is standard validator that is used now days. Servant automatically contains
 eslint core and can run validation for sources and tests files. For using eslint there need
 to be defined one of config.

#### **Configuration as `.eslintrc.json`**

Most common ways is to define `.eslintrc.json` in root of package and define some rules here. It's recommended to create
 one config file in root of whole servant project and then use config file in every package. You can reference to root 
 config file from packages.

**Root config file**

```json
{
  "root": true,
  "rules": {
    "@typescript-eslint/no-explicit-any": 2,
    "@typescript-eslint/no-use-before-define": 2,
    ...
  }
}
```

**Package config file**

```json
{
  "root": false,
  "extends": [
     "../.eslintrc.json" //this is relative path for root config file
  ]
}
```

#### **Configuration as property `eslintConfig` in `package.json`**

You can also use `package.json` property `eslintConfig` to configure eslint rules. It is similar to normal eslint config
 file.

```json
{
 "name": "@servant/project",
 "version": "1.2.4",
 ...
 "eslintConfig": {
   "root": false,
   "extends": [
    "../.eslintrc.json" //this is relative path for root config file
   ]
 }
}
```

> **Caveats**
>
> You can not extend `package.json` config with another `package.json` config. Because of that you need to have `.eslintrc.json`
> as root config. **Over all, it's not recommended to use eslint config in `package.json` and always use `.eslintrc.json`.**


Without one of these settings, servant will disable eslint validation for this module. You can 
 choose which module will be validated and which not by only remove or add root config.

> **Caveats**
> 
> Default configuration is defined for eslint validator. After you use one of configuration
> type, Servant automatically adds recommended defaults. You can always change behaviour in
> your config, but you can use it out of the box with recommendation setting.

#### Default config file

```json
{
   "parser": "@typescript-eslint/parser",
   "plugins": [
    "@typescript-eslint",
   ],
   "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended"
   ]
}
```

### Validator **"prettier"**

#### How to enable prettier validation?

Prettier is validator and formatter that is used now days for checking if file is valid in content formatting. Servant
automatically contains prettier core and can run validation for sources, tests files and files defined by configuration
in **[servant.json][2]**. For using prettier there need to be defined one of config.

#### **Configuration as `.prettierrc.json`**

Most common ways is to define `.prettierrc.json` in root of package and define some rules here. This config is loaded
by Servant and automatically applied on all validated files.

```json
{
 "trailingComma": "es5",
 "tabWidth": 4,
 "semi": false,
 "singleQuote": true
}
```

#### **Configuration as property `prettier` in `package.json`**

You can also use `package.json` property `prettier` to configure prettier rules. It is similar to normal prettier config
file.

```json
{
 "name": "@servant/project",
 "version": "1.2.4",
 ...
 "prettier": {
  "trailingComma": "es5",
  "tabWidth": 4,
  "semi": false,
  "singleQuote": true
 }
}
```

Without one of these settings, servant will disable prettier validation for this module. You can
choose which module will be validated and which not by only remove or add root config.

> **Caveats**
>
> Default configuration is defined for prettier validator. **This config is empty!** Default setting for current
> version of prettier is used. You can always change behaviour in your config, but you can use it out of the box with 
> recommendation setting.

#### Default config file

```json
{
   "parser": "@typescript-eslint/parser",
   "plugins": [
    "@typescript-eslint"
   ],
   "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended"
   ]
}
```

### Validator **"trancorder"**

#### How to enable trancorder validation?

Trancorder is validator and generator that is used for validating translations usage and generating and updating 
localisation files. There is separated documentation for [`trancorder-cli`][trancorder-cli] and [`trancorder`][trancorder] 
modules where is described how is working. Servant automatically contains trancorder core and can run validation for 
sources and localisations defined in **[servant.json][2]** and `.trancorder.json`. For using trancorder there need to 
be defined config file.

#### **Configuration `.trancorder.json`**

Most common ways is to define `.trancorder.json` in root of package and define related properties here. This config is loaded
by Servant and automatically applied on all validated files. Also it allows run trancorder directly. More info about this config
file can be found [here][trancorder-cli].

```json
{
 "translations": "src/locales"
}
```

Without this config file, servant will disable trancorder validation for this module. You can
choose which module will be validated and which not by only remove or add this config file to root.

> **Caveats**
>
> Default configuration is defined for trancorder validator. **This config is filled only with path to translations directory!** 
> Default setting for current version of trancorder is used. You can always change behaviour in your config, but you can use it out of the box with
> recommendation setting.

### All validators

#### Universal settings

- [`--entry <path>`][entry]  - see [--entry][entry] for more info.
- [`--debug`][debug]  - see [--debug][debug] for more info.
- [`--production`][production]  - see [--production][production] for more info.
- [`--changed`][changed]  - see [--changed][changed] for more info.
- [`--only <module-name>`][only]  - see [--only][only] for more info.
- [`--init`][init]  - see [--init][init] for more info.
- [`--dependencies`][init]  - see [--dependencies][dependencies] for more info.

#### Specific settings

Validate command can use fix flag.

#### `--fix`

This is flag that automatically try to fix validation errors than can be fixed. This is valid options for all available
 validation tools, such asi **eslint**, **prettier**, **trancorder** and others. After successfully runs, servant will report all touched files
 into console. All errors and warnings that are fixed automatically are not listed in final result.

*Example:*
> `servant validate --fix`

[1]: https://docs.npmjs.com/files/package.json
[2]: ../../servant/doc/servant.json.md
[3]: ../../servant/doc/servant.nodejs.md
[4]: servant.clia.md
[entry]: servant.clia.flags.md#-entry-path
[debug]: servant.clia.flags.md#-debug
[production]: servant.clia.flags.md#-production
[changed]: servant.clia.flags.md#-changed
[only]: servant.clia.flags.md#-only-module-name
[init]: servant.clia.flags.md#-init
[dependencies]: servant.clia.flags.md#-dependencies

[trancorder-cli]: https://gitlab.com/stanislavhacker/servant/-/blob/master/trancorder-cli/README.md
[trancorder]: https://gitlab.com/stanislavhacker/servant/-/blob/master/trancorder/README.md

[preview]: ../../assets/commands/command.validate.gif
 