Servant command: `clean`
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

##### Examples:

 > `servant clean` 
 > 
 > `servant clean --entry "/path/to/project"` 
 > 
 > `servant clean --debug` 
 > 
 > `servant clean --production --changed` 
 > 
 > `servant clean --only "module-name"` 
 
#### Preview

 ![Clean command preview][preview]
 
#### Description

Command `clean` is used to remove all files that are not part of project. Typically, these files are generated files
 from typescript or from less or sass compilers or output directories and files.
 
**What files will be removed?**

 - Files defined in `files` property of [`package.json`][1].
 - All files with extensions defined in [`servant.json`][2] `clean` property 
  that are inside `src` or `tests` directories
 - All files that are generated for your source files (js, jsx, css) but not if these files are alone!
  
 > **Caveats**
 >
 > Files that are marked for clean but **there are under vcs (eg. git) will not be deleted!** This is because
 > you want to add same .md files (for example) into you package, but these files are under vcs and are maintained
 > inside your project. Because of this Servant not delete this files!

#### Universal settings

 - [`--entry <path>`][entry]  - see [--entry][entry] for more info.
 - [`--debug`][debug]  - see [--debug][debug] for more info.
 - [`--production`][production]  - see [--production][production] for more info.
 - [`--changed`][changed]  - see [--changed][changed] for more info.
 - [`--only <module-name>`][only]  - see [--only][only] for more info.
 - [`--init`][init]  - see [--init][init] for more info.
 - [`--dependencies`][init]  - see [--dependencies][dependencies] for more info.
 
#### Specific settings

Clean command can used prune flag.

#### `--prune`

Clean also all `node_modules` folders and `package-lock.json` in module folder. After this you must call install command.
 Prune command not deleted modules that are locally referenced (`file:../project-module`). It's because it's not possible
 to have these modules "out of date".

 > **Caveats**
 >
 > If you use main module as one of Servant module (defined in main [`servant.json`][2] modules property as `"./"`), prune flag
 > cause that also main folder node_modules will be removed! Servant stop working because there will be no packages for Servant :) 
 > You must run `npm install` manually and after that Servant will be accessible.

*Example:*
 > `servant clean --prune`

 [1]: https://docs.npmjs.com/files/package.json
 [2]: ../../servant/doc/servant.json.md
 [3]: ../../servant/doc/servant.nodejs.md
 [4]: servant.clia.md
 [entry]: servant.clia.flags.md#-entry-path
 [debug]: servant.clia.flags.md#-debug
 [production]: servant.clia.flags.md#-production
 [changed]: servant.clia.flags.md#-changed
 [only]: servant.clia.flags.md#-only-module-name
 [init]: servant.clia.flags.md#-init
 [dependencies]: servant.clia.flags.md#-dependencies
 
 [preview]: ../../assets/commands/command.clean.gif
 