Servant universal settings
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

#### `--entry <path>`

This is main entry point servant. Without setting this, servant will use *current working rectory* (**cwd**).
 In other case Servant will use provided **absolute** path as context for working.
 
*Example:*
 > `servant --entry "/path/to/project"`

#### `--debug`

This is option for debuging servant. For user that not need info about errors in Servant is not intereting option.
 **Do not use it if you are not a Servant contributor**.
 
*Example:*
 > `servant --debug`

#### `--production`

This flag affects only specific commands. It's used to switching from development to production env. In `build` sources
 will be minimized and source maps will be removed. In `publish` production flag will publish modules into registry.
 **Use this flag only if you are ready to produce your modules!**
 
*Example:*
 > `servant --production`
 
#### `--watch`

This flag is used for watching changes in project and automatically rebuilding it. It can be combined with
 all commands. Servant first run all commands and then after all commands are done start watching for changes.
 It can be also run watcher without commands and start watching immediately. It's recommended to use watch
 flag in combination with build command. Servant build project and after that starts watching.
 
 > **Caveats**
 > 
 > 1. Watcher try to determine what files will be watched. For example if you have `index.ts`, watcher will not
 > watch files `index.js`, `index.d.ts` and `index.js.map`. But if you have `index.js` only (without ts), watcher will be
 > watched this `index.js` file. 
 > 2. Watcher builds only files that are defined by glob in properties `src`, `tests`, `watch`, `resources`. Others files 
 > are ignored!

### Watcher and discovery service

Watcher has special discovery feature. Servant in this mode can communicate with other instances of Servant and
 create symbolic link to modules that are in external repository or project. This is good to make sure that you 
 will have also changes from module that is marked as external dependency. Servant will automatically try to 
 rebuild these modules and used all changes that are done during development.

**For example:** This project `@servant/servant-cli` use as external module `envfull` module for parsing command
 line arguments. `envfull` is also Servant project, so it can run `--watch` mode. Then if `--watch` mode is run in
 `@servant/servant-cli` project, these two instances are connected together and linked running `envfull` into
 `@servant/servant-cli`. You can see this in screen below.

![Discovery preview][discovery-preview]
 
*Example:*
 > `servant build --watch`
 > 
 > `servant --watch`
 
#### Watch preview

 ![Watcher preview][watcher-preview]

#### `--only <module-name>`

By default Servant works with all modules structure. If you want to run commands only on specified modules you can
 use only option. Commands will be runned only for selected modules and submodules. 
 
 > **Caveats**
 >
 > Prop `module-name` is real module name defined in [`package.json`][1]. **Is not a module directory!**
 
*Example:*
 > `servant --only "module.one" --only "module.two"`

#### `--changed`

If you use **git version system** you can use changed setting for building only changed modules and module dependants.
 This is good for large project with more modules when you need to save time and build only changed modules. But
 this is not only connected to git. It's connected to filesystem too. So if module [meet conditions](#conditions-for-rebuild-module) 
 for rebuild, it will be also build even if there is no git changed files.
 
##### **Conditions for rebuild module**
 
 - module is changed by git status (added, removed, change one or more files)
 - there is no output files or folder (module was never build before or cleaned by `clean` command)
 - src file change timestamp is newest than output file (you pull changes from vcs)

#### `--dependencies`

Applicable only with `--only` flag. Run command with modules and add all modules dependencies. This flag 
 is helpful when developing on single module and need run test for all dependant modules. It's also good to use this with
 combination of CI pipelines to run test only for modules that are affected.

*Example:*
> `servant tests --only "module-name" --dependencies`
 
#### `--init`
 
 This flag clear all other flags and commands and start init of your project or submodule. It's used for 
  creating new project definition or submodule definition. At the end Servant init create **[package.json][1]**
  and **[servant.json][2]**. If you choose typescript language, Servant will also create **tsconfig.json**.
  
 *Example:*
  > `servant --init`
 
#### `--generate (<generator-name>)`
 
 This flag clear all other flags and commands and start servant generator runtime. It's used for 
  creating new components and content based on downloaded and installed generators or custom local generators.
  
 *Example:*
  > `servant --generate`
  > 
  > `servant --generate login-component`
  
#### `--server`
 
 This flag clear all commands and start development server of your project. It's used for
 fast development of your multi modules project. You must have specified `server` property in your
 **[servant.json][2]**. Then you can use this flag to run development server!

 *Example:*
  > `servant --server`

#### `--port <port>`
 
 This flag is used with combination of `--server` flag. It's used for define base starting port for running
  development servers. Default port is 9000. Servant will try to find first not used port automatically. 

 *Example:*
  > `servant --server --port 8081`  
  
#### `--report <report-name | reports-list>`

This flag used for settings which reports can Servant generate. Servant read this and create reports based
 on settings.
 
**Current supported reports**

 - **issues**: Generate list of all issues that are tested by your automatic tests for **tests** command.
 - **analyze**: Generate list of all dependencies and versions that are used in project for **analyze** command.
 
 *Example:*
  > `servant --report "issues"`
  > 
  > `servant --report "analyze"`
  > 
  > `servant --report "issues" --report "analyze"`
  > 
  > `servant --report "issues,analyze"`
  

 [1]: https://docs.npmjs.com/files/package.json
 [2]: ../../servant/doc/servant.json.md
 [3]: ../../servant/doc/servant.nodejs.md
 [4]: servant.clia.md
 [5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
 
 [watcher-preview]: ../../assets/servant.watch.gif
 [discovery-preview]: ../../assets/features/discovery.png