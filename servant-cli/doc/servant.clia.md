Servant command line api
======
Quick references: **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

Servant builder is simple build tool for developers, that wants to build and manage modularized large projects in 
 monorepo or separated into more repositories. With this tool you will be able to control used libraries, build and rebuild
 all packages at all or only selected once, run tests in more browsers and device, validate all your code against 
 eslint or prettier and finally publish your packages into repository. Servant basically try to learn the best way of
 managing and developing of multiple modules projects and its maintenance with grace. It's try to be simple but powerful 
 tool and good servant :wink:

### Key features

- **Monorepo manage and configuration** Manage monorepo with one configuration file that can contains definition
    for your monorepo, directory structure, validator settings, publish settings and more! There will be only small
    config files in every module to customize names and overriding some specific settings.


- **Installing and update at once** Make install and update npm commands at once for all defined modules or for
    some specific modules only.


- **Controlling and unifying dependencies versions** Manage all dependencies, unify across all packages to be same in 
    version. Servant will automatically warn you if there are some dependencies that are not same across all packages 
    and provide same commands to unify this versions across all packages.


- **Testing on more browsers and devices** Servant allows you to run tests against more than 70 devices in 3 different 
    browsers. This all because Servant using playwright library to run tests in. But even if you are not want to use
    playwright, Servant try to run all tests in Chrome browsers. And that is possible only one simple configuration
    record in configuration file.


- **Validating quality of your code** Check quality of your code by using eslint and prettier: servant can run validation
    of all supported validators in one step and report all errors and warnings on one place with nice command line results.
    These validators can be manage by using or not using config files related to supported validator and can be turned on
    only for some modules or for all.


- **Smart publishing into repository** Servant can also provide publish of all your packages into repository. And because
    we need some kind of versioning manage, you are able to choose if Servant will increase packages patch, minor, major or
    any prerelease version and also commit some message about version increment into git. Servant can also fix packages 
    versions or removing devDependencies during publishing.


- **Development server with "_Servants link_"** For better developer experience, Servant has own dev server, that can provide
    automatically rebuilding on change. This server automatically includes all necessary libraries that are defined in
    dependencies and by default is include only libraries, that are compatible with browser. So you are able to check if
    your modules will be work as package in browser.

    Servant also come with feature called "**_Servants link_**". If there are more Servants run in watch mode, there are 
    communicating each others. Servant detects others packages and can rebuild modules that are connected through dependencies.
    **Let's look on some example**. You have a company monorepo "main-app" with packages. Also, there is company repo "library-app".
    Some package from "main-app" has dependency to "library-app". Both repos use Servant as build tool. If you run "main-app" and
    "library-app" in watch mode, Servant run from "main-app" detect that there is a "library-app" Servant running and link this into
    "main-app". So now "main-app" use live build of library "library-app" and also, if you make some changes Servant first ensure 
    rebuild of "library-app" and then "main-app". And that's all :) You never need to create some link and another hacks manually!

-   **Code generators** Servant provide external or custom generators that can provide fast and standard way how to create
    predefined components, code samples and other content. Just run generators, select desired on and answers all questions
    that generators need. After that content will be generated into wanted modules and selected folder.

> **Caveats**
> 
> Servant dev serves has no support for hot module replacement for now. This will be implemented in future releases so now
> you need make pages reload after rebuild of modules.

![Servant preview][preview]

### Installation and start!

#### 1. Globally installed

You need the latest version of nodejs and then run:

> `npm install @servant/servant-cli -g`

Then you can run in current directory:

> `servant clean`

> **Caveats**
>
> Installing globally is not recommended.

Servant will be complaining about missing [`package.json`][1] and [`servant.json`][2]. We will create it.
Servant has special flag to init main project or submodule. We will create main project now with using
`--init` flag and answer all questions that servant give us.

> `servant --init`

After init project with calling `"node_modules/.bin/servant" --init`, Servant create [`package.json`][1]
file with script called "servant". Now you can use `npm run servant` to call servant from your
current directory.

> **Caveats**
>
> If you want to pass arguments into Servant run through `npm run` you must separate arguments
> with `--`
>
> **For example:**
> `npm run servant -- clean build --production`

#### 2. Locally installed

You need the latest version of nodejs and then run:

> `npm install @servant/servant-cli`

Then you can run in current directory:

> `"node_modules/.bin/servant" clean`

Servant will be complaining about missing [`package.json`][1] and [`servant.json`][2]. We will create it.
Servant has special flag to init main project or submodule. We will create main project now with using
`--init` flag and answer all questions that servant give us.

> `node_modules/.bin/servant --init`

After init project with calling `"node_modules/.bin/servant" --init`, Servant create [`package.json`][1]
file with script called "servant". Now you can use `npm run servant` to call servant from your
current directory.

> **Caveats**
>
> If you want to pass arguments into Servant run through `npm run` you must separate arguments
> with `--`
>
> **For example:**
> `npm run servant -- clean build --production`

#### 3. Installed and init with `npx`

You need the latest version of nodejs and then run

> `npx @servant/servant-cli --init`

This command run init runtime of servant, that will create skeleton of first project. There is wizard that
can help to create settings that you need and also prepare first files and make install of all dependencies.

![Servant init example][init]

### Commands

Servant supports lots of commands that are available with settings.

List of all commands:

 - [clean][clean] - Used to remove all files that are not part of project.
 - [install][install] - Same like npm install command. But this is done for all submodule in your project.
 - [update][update] - Same like npm update command. But this is done for all submodule in your project.
 - [build][build] - Used to do all stuff! Build module into single file or files.
 - [unify][unify] - Used to unify versions through all modules in your project.
 - [publish][publish] - Used to publish or create pack from modules in your project.
 - [tests][tests] - Used to run you tests for whole project.
 - [validate][validate] - Used to validate your project.
 - [analyze][analyze] - Used to analyze your project.
 - [shared][shared] - Used to list or link all shared modules in your project.
 
There is also list of [universal settings][flags] that can be used for every command.

List of all universal flags:

 - [--entry \<entry\>][flags] - This is main entry point servant.
 - [--production][flags] - It's used to switching from development to production env.
 - [--watch][flags] - This flag is used for watching changes in project and automatically rebuilding it.
 - [--only \<module-name\>][flags] - If you want to run commands only on specified modules.
 - [--changed][flags] - If you use **git version system** you can use changed setting for building only changed modules and module dependants.
 - [--init][flags] - This flag clear all other flags and commands and start init of your project or submodule.
 - [--server][flags] - This flag clear all commands and start development server of your project.
 - [--port \<port\>][flags] - This flag is used with combination of `--server` flag.
 - [--report \<report-name\>][flags] - This flag used for settings which reports can Servant generate.

#### `--help`

Show help for Servant or for current command line.
 
*Example:*
 > `servant --help`
 >
 > `servant clean build publish --production --tag "v1.1.1" --help`
 
![Servant help][help]

#### Exit codes

Servant has lots of different exit code for determine what is wrong. There are error codes for
 warnings and errors that come from Servant. If there is possible more than one error code, the only 
 most important is returned as exit code!
 
#### Errors

  **1** - Servant load error - occurs when there are not **[`servant.json`][2]** or `package json` in 
   entry directory
  
  **2** - Modules load error - occurs when Servant is not able to load modules structure 
   and check dependencies 
  
  **3** - Servant command error - occurs when Servant running commands for modules
  
  **5** - Servant command for one module failed - occurs when there is failed result from command.
   Basically this mean that Servant is ok but something in command fail. (Cannot build because of 
   syntax error, cannot run tests because of specs missing and some similar cases)
  
  **7** - Servant report generator failed - occurs if some reports can not be generated. For example
   issues report can access output folder due to restriction of write rights.
  
  **8** - Servant watcher occured some critical error - occurs if watcher for files changes can not start
   for some reasons.
  
  **101** - Servant init error - some error occurred when running `--init` flag on Servant.
  
  **901** - Server init error - this is same as error code **1**, but for `--server` flag.
   
  **902** - Servant start error - occurs when there are some error when running Servant server. For
   example Servant is not able to find free ports.
 
#### Warnings

  **-4** - Servant command warning - occurs when there is same warning comming from running Servant
   command. Basically this mean that Servant is ok but something in command return warning.
   
  **-6** - Servant commands are empty - occurs when you are not provide any command to run. 

 [2]: ../../servant/doc/servant.json.md
 [3]: ../../servant/doc/servant.nodejs.md
 [5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
 
 [preview]: ../../assets/servant.example1.gif
 [help]: ../../assets/servant.help.gif
 
 [clean]: servant.clia.clean.md
 [install]: servant.clia.install.md
 [update]: servant.clia.update.md
 [build]: servant.clia.build.md
 [unify]: servant.clia.unify.md
 [publish]: servant.clia.publish.md
 [flags]: servant.clia.flags.md
 [tests]: servant.clia.tests.md
 [validate]: servant.clia.validate.md
 [analyze]: servant.clia.analyze.md
 [shared]: servant.clia.shared.md
