Servant command: `install`
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

##### Examples:

 > `servant install` 
 > 
 > `servant install --entry "/path/to/project"` 
 > 
 > `servant install --debug` 
 > 
 > `servant install --production --changed` 
 > 
 > `servant install --only "module-name"` 
 > 
 > `servant install --noaudit`
 
#### Preview

 ![Install command preview][preview]
 
#### Description

Command `install` is same like npm install command. But this is done for all submodule in your project. You don't
 need it to run it separately but you can use this command to **install it all**! Servant will try too install 
 from registry that is defined in **[servant.json][2]** `registry` property or from default registry (npmjs.org) 
 if omitted. 
 
 > **Note**
 > 
 > Flag `--production` have not any special meaning for this command. Command always install production and development
 > dependencies because we need it all for development :wink: 
 
#### Universal settings

 - [`--entry <path>`][entry]  - see [--entry][entry] for more info.
 - [`--debug`][debug]  - see [--debug][debug] for more info.
 - [`--production`][production]  - see [--production][production] for more info.
 - [`--changed`][changed]  - see [--changed][changed] for more info.
 - [`--only <module-name>`][only]  - see [--only][only] for more info.
 - [`--init`][init]  - see [--init][init] for more info.
 - [`--dependencies`][init]  - see [--dependencies][dependencies] for more info.

#### Specific settings

Install command can used noaudit flag.

#### `--noaudit`

Disable npm audit checks for `install` command. Normal run do audit check but for example on
 build servers when running build runtime you want to disable audit checking for skipping unwanted
warning reports.

*Example:*
> `servant install --noaudit`

 [1]: https://docs.npmjs.com/files/package.json
 [2]: ../../servant/doc/servant.json.md
 [3]: ../../servant/doc/servant.nodejs.md
 [4]: servant.clia.md
 [entry]: servant.clia.flags.md#-entry-path
 [debug]: servant.clia.flags.md#-debug
 [production]: servant.clia.flags.md#-production
 [changed]: servant.clia.flags.md#-changed
 [only]: servant.clia.flags.md#-only-module-name
 [init]: servant.clia.flags.md#-init
 [dependencies]: servant.clia.flags.md#-dependencies
 
 [preview]: ../../assets/commands/command.install.gif