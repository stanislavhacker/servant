Servant command: `publish`
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

##### Examples:

 > `servant publish` 
 > 
 > `servant publish --production` 
 > 
 > `servant publish --production --tag "v1.1.1"` 
 >
 > `servant publish --production --freeze`
 > 
 > `servant publish --production --freeze --commit`
 > 
 > `servant publish --production --increment minor` 
 
#### Preview

 ![Publish command preview][preview]
 
#### Description

Command `publish` is used to publish or create pack from modules in your project. If you use `--production` flag
 Servant will try too publish your module into registry that is defined in **[servant.json][2]** `registry` 
 property or into default registry (npmjs.org) if omitted. 
 
 > If you are omitted `--production` flag, Servant create
 `*.tgz` files in every module that is intended for publish. 
 
 > If you use `--production` flag and `private` field in **[package.json][1]**, this module will
  be skipped when publishing and reported into Servant cli!
 
Publish also make some changes into **[package.json][1]** of every module.
 
 1. **Increment version.** - Automatically increment version of module in **[package.json][1]**. Default behaviour
  is to change "patch" number of version. This can be changed in **[servant.json][2]** `publish.increment` property
  or by using `--increment <type>` command line flag.

 1. **Replace local version.** - Servant replace locally required modules (with `file:`) by module version. It's 
  mean that all locally modules will be generated as global available module with **fixed** version.
 
 1. **Change `main` and `types`.** - Servant change main property and if you has defined types property
  it's change it too. Typically you have main and types property with paths into development folder 
  (src folder). Servant will change this paths and repalce it by dist files! 
 
 1. **Fixed versions.** - If you provide `--freeze` setting, Servant freeze versions in **[package.json][1]**. So there 
  will be fixed versions in dependencies!
	
 > **Caveats**
 >
 > If Servant not found corresponding version for local module, the any version mark (`*`) will be used 
 > and Servant console warn about this. This is for most of cases strange and it needs to be fixed in your
 > project settings.

#### Universal settings

 - [`--entry <path>`][entry]  - see [--entry][entry] for more info.
 - [`--debug`][debug]  - see [--debug][debug] for more info.
 - [`--production`][production]  - see [--production][production] for more info.
 - [`--changed`][changed]  - see [--changed][changed] for more info.
 - [`--only <module-name>`][only]  - see [--only][only] for more info.
 - [`--init`][init]  - see [--init][init] for more info.
 - [`--dependencies`][init]  - see [--dependencies][dependencies] for more info.
 
#### Specific settings

Publish command used there special settings.

#### `--tag <tag>`

Define tag for published packages. It's same as `tag` option in `npm publish` command. Default is also same
 and is used "latest".

*Example:*
 > `servant publish --tag "latest"`
 
#### `--freeze`
 
This option cause fixed version in your **[package.json][1]**. It's used version from actual state of your project
 node modules. So if you have version for example react module in **[package.json][1]** defined as "^16.8.2" and
 current installed version is "16.8.6", Servant change this version in published **[package.json][1]** and
 provide here freeze version "16.8.6". So your published module will have fixed versions everywhere.  
 
*Example:*
 > `servant publish --freeze`

#### `--commit`

This is option that is used only if your project is in version control system. After every `package.json` in module is updated, Servant make commit of these changed files and add 
 commit message that is defined in **[servant.json][2]**'s property `publish.commitMessage`
 or used it's default. 
 
 > **Caveats**
 >
 > Servant make only `git commit` command, never `push`. It's because push command is make
 > changes into server repository and this is danger operation. If you want to push these 
 > changes into server, you must do it by another script. Try make push in 
 > package.json script. For example you can use this script: 
 >`servant clean build publish --production --commit && git push`

*Example:*
 > `servant publish --production --commit`

#### `--increment <type>`

This option can be used to override option `publich.increment` in **[servant.json][2]**. Its necessary if publishing
 some another version than it specified in **[servant.json][2]**. For example. Default definition is "patch" so every
 publish patch version is increased. But in job for minor or major release we want to change it. This option can
 override it for current run only.

*Example:*
> `servant publish --production --increment minor`

 [1]: https://docs.npmjs.com/files/package.json
 [2]: ../../servant/doc/servant.json.md
 [3]: ../../servant/doc/servant.nodejs.md
 [4]: servant.clia.md
 [entry]: servant.clia.flags.md#-entry-path
 [debug]: servant.clia.flags.md#-debug
 [production]: servant.clia.flags.md#-production
 [changed]: servant.clia.flags.md#-changed
 [only]: servant.clia.flags.md#-only-module-name
 [init]: servant.clia.flags.md#-init
 [dependencies]: servant.clia.flags.md#-dependencies
 
 [preview]: ../../assets/commands/command.publish.gif
 