Servant command: `shared`
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

##### Examples:

> `servant shared`
>
> `servant shared --entry "/path/to/project"`
>
> `servant shared --debug`
>
> `servant shared --link`
>
> `servant shared --unlink`

#### Preview

![Shared command preview][preview]

#### Description

Command `shared` is used to list all shared dependencies in project modules or link/unlink them to 
global node_modules. For example some libraries needs to be included in app only once (React for example) 
so you can share them from global node_modules. This is basically necessary only in development.

#### Universal settings

- [`--entry <path>`][entry]  - see [--entry][entry] for more info.
- [`--debug`][debug]  - see [--debug][debug] for more info.
- [`--production`][production]  - see [--production][production] for more info.
- [`--changed`][changed]  - see [--changed][changed] for more info.
- [`--only <module-name>`][only]  - see [--only][only] for more info.
- [`--init`][init]  - see [--init][init] for more info.
- [`--dependencies`][init]  - see [--dependencies][dependencies] for more info.

#### Specific settings

Shared command can use link and unlink flag.

#### `--link`

Flag that automatically links all shared dependencies to defined **node_modules**. Its used `devlink` module
that make symbolic links and keep original module, so it can be reverted by `unlink` flag. This command
and flag is automatically used when you run `servant install` command.

*Example:*
> `servant shared --link`
>
> `servant install`

#### `--unlink`

Flag that automatically unlinks all shared dependencies that was linked by `link` flag. This flag is
barely used, because uts not necessary to call it at all, but it can helps to solve some problems.

*Example:*
> `servant shared --unlink`

[1]: https://docs.npmjs.com/files/package.json
[2]: ../../servant/doc/servant.json.md
[3]: ../../servant/doc/servant.nodejs.md
[4]: servant.clia.md
[entry]: servant.clia.flags.md#-entry-path
[debug]: servant.clia.flags.md#-debug
[production]: servant.clia.flags.md#-production
[changed]: servant.clia.flags.md#-changed
[only]: servant.clia.flags.md#-only-module-name
[init]: servant.clia.flags.md#-init
[dependencies]: servant.clia.flags.md#-dependencies

[preview]: ../../assets/commands/command.shared.gif
 