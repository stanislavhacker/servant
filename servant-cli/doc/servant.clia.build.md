Servant command: `build`
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

##### Examples:

 > `servant build` 
 > 
 > `servant build --entry "/path/to/project"` 
 > 
 > `servant build --debug` 
 > 
 > `servant build --production --changed` 
 > 
 > `servant build --only "module-name"` 
 
#### Preview

 ![Build command preview][preview]
 
#### Description

Command `build` is used to do all stuff! Build module into single file or files. Command collect files from `src`, `tests` and `resources`
 in [`servant.json`][2]. Build all **\*.ts** files from these source and make output file defined in `output.filename` property in 
 [`servant.json`][2]. Also if you use **\*.less**, **\*.scss** or **\*.css**, it will be bundled. There generated files and also files defined
 in `resources` property will be copied into directory defined in `output.directory` property. And thats all, your project is bundled and
 ready for publish!
 
**Resolving `.css` files**

Servant can resolve `.css` **two ways**. 

 1. First way is to **import `.css` file** in your `.ts` file. It can be done like `import * as css from "./my.css";`. Because Servant
 have css to d.ts converter it can generate d.ts from css and your IDE can hint class names for you. Servant will 
 be used css file as normal module and can build bundled css file into output folder.
 1. Second way is add file beside your entry point file, with same name, but with extension `.css`, `.scss` or `.less`. Servant will load this file
  and copy result file into output folder. If this file is `.less` or `.scss`, Servant build it before copying.
  
  _For example_: If you have `"./src/index"` as  entry, create `"./src/index.less"` file, and it's done!
  
  _For example_: If you have `"./src/index"` as  entry, create `"./src/index.scss"` file, and it's done!

**Resolving `bundled` dependencies**

If you specify `bundledDependencies`, Servant will behave another way than npm. Servant will try bundle
 these modules into final `.js` file and remove these dependencies from published `package.json`. This is 
 good if you want to bundle dependencies, but you don't want to specify these dependencies in 
 `package.json`.
 
> **Caveats about `devDependencies`**
>
> If you specify `devDependencies` at your main `package.json` (root of project) and this root is also mark as module 
> (property `modules` in `servant.json` contains `./`) these `devDependencies` will not be loaded by Servant. This is because
> we expected that `devDependencies` in main `package.json` includes lots of dev tools (and also "@servant/servant-cli").

#### Universal settings

 - [`--entry <path>`][entry]  - see [--entry][entry] for more info.
 - [`--debug`][debug]  - see [--debug][debug] for more info.
 - [`--production`][production]  - see [--production][production] for more info.
 - [`--changed`][changed]  - see [--changed][changed] for more info.
 - [`--only <module-name>`][only]  - see [--only][only] for more info.
 - [`--init`][init]  - see [--init][init] for more info.
-  [`--dependencies`][init]  - see [--dependencies][dependencies] for more info.
 
#### Specific settings

 > Build has not any specific setting.

 [1]: https://docs.npmjs.com/files/package.json
 [2]: ../../servant/doc/servant.json.md
 [3]: ../../servant/doc/servant.nodejs.md
 [4]: servant.clia.md
 [entry]: servant.clia.flags.md#-entry-path
 [debug]: servant.clia.flags.md#-debug
 [production]: servant.clia.flags.md#-production
 [changed]: servant.clia.flags.md#-changed
 [only]: servant.clia.flags.md#-only-module-name
 [init]: servant.clia.flags.md#-init
 [dependencies]: servant.clia.flags.md#-dependencies
 
 [preview]: ../../assets/commands/command.build.gif
 