Servant command: `unify`
======
Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

##### Examples:

 > `servant unify --m.react "16.2.7"` 
 > 
 > `servant unify --m.react "16.2.7" --entry "/path/to/project"` 
 > 
 > `servant unify --m.react "16.2.7" --debug` 
 
#### Preview

 ![Unify command preview][preview]
 
#### Description

Command `unify` is used to unify versions through all modules in your project. Servant validating your project structure and can inform
 you about different versions of same module in modules [`package.jsons`][1]. There can be basically 2 ways how to unify versions.

#### 1. Use flag `--m.<library> <version>`

This flag is described below and can be used for specified unify versions for libraries. You can define all versions that will be used
 across all projects.

#### 2. Use manual select or latest

For libraries that are not specified with `--m.<library> <version>` flag, Servant can choose you from two options. You can select that
 you want to upgrade these libraries on latest version (mean latest version used in project, not the latest version in registry) or
 provide version manually. In this case, you need to pick right version for all libraries in list and after select, Servant will run
 "unify" command again to set selected versions.

 > **Note**
 > 
 > Flags `--production`, `--changed`, `--only` have not any special meaning for this command. Command is used to unify versions through
 > all modules. So for this case is strange call this command with `--changed`, `--production` or `--only` flags :wink: 
 
 > **Caveats**
 >
 > Servant not run any "install" command after unify. This is because you can check final results 
 > and after that its simple possible to run "servant install" command. Of you want to do it 
 > always, just add some script into `package.json` as is show below.
 > 

```json
{
  ...
  "scripts": {
    "unify-ann-update": "servant unify --latest && servant install"
  },
  ...
}
```

#### Universal settings

 - [`--entry <path>`][entry]  - see [--entry][entry] for more info.
 - [`--debug`][debug]  - see [--debug][debug] for more info.
 - [`--production`][production]  - see [--production][production] for more info.
 - [`--changed`][changed]  - see [--changed][changed] for more info.
 - [`--only <module-name>`][only]  - see [--only][only] for more info.
 - [`--init`][init]  - see [--init][init] for more info.
 - [`--dependencies`][init]  - see [--dependencies][dependencies] for more info.
 
#### Specific settings

Unify command used variable list of modules for unify.

#### `--m.<library> <version>`

Define unifying version for library. You can set up more libraries in one command.

*Example:*
 > `servant unify --m.react "16.7.12 --m.react-dom "16.3.8"`


#### `--latest`

Servant automatically set versions of all not unified packages to the latest version that 
 is used in project. All `--m.<module-name>` flags was ignored in this case.

*Example:*
> `servant unify --latest`


[1]: https://docs.npmjs.com/files/package.json
 [2]: ../../servant/doc/servant.json.md
 [3]: ../../servant/doc/servant.nodejs.md
 [4]: servant.clia.md
 [entry]: servant.clia.flags.md#-entry-path
 [debug]: servant.clia.flags.md#-debug
 [production]: servant.clia.flags.md#-production
 [changed]: servant.clia.flags.md#-changed
 [only]: servant.clia.flags.md#-only-module-name
 [init]: servant.clia.flags.md#-init
 [dependencies]: servant.clia.flags.md#-dependencies
 
 [preview]: ../../assets/commands/command.unify.gif
 