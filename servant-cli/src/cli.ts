import { Commands } from "@servant/servant";
import { envfull, EnvfullVars } from "envfull";
import { ServantProps } from "./app";
import Process = NodeJS.Process;

export type GetData = {
	props: Pick<ServantProps, Exclude<keyof ServantProps, "commands" | "generator">>;
	commands: Array<Commands.Commands>;
};

export function get(process: Process): GetData {
	const data: EnvfullVars<ServantProps> = envfull<ServantProps>(process, {
		env: [/SERVANT\.{*.}/],
		arrays: ["only", "report"],
		defaults: {
			m: {},
			entry: process.cwd(),
			debug: false,
			production: false,
			dependencies: false,
			changed: false,
			tag: undefined,
			help: false,
			browsers: "",
			devices: "",
			freeze: false,
			init: false,
			generate: false,
			prune: false,
			noaudit: false,
			server: false,
			watch: false,
			gui: false,
			port: 9000,
			report: [],
			only: [],
			commit: false,
			increment: undefined,
			fix: false,
			latest: false,
			link: false,
			unlink: false,
		},
		aliases: {
			clean: ["c"],
			install: ["i"],
			update: ["u"],
			build: ["b"],
			unify: ["f"],
			publish: ["p"],
			tests: ["t"],
			validate: ["v"],
			analyze: ["a"],
			shared: ["s"],
		},
	})();

	const props = data.$ as GetData["props"];
	const commands = data._ as GetData["commands"];

	return {
		commands,
		props,
	};
}
