/**
 * Look at for documentation
 * ./servant-cli/doc/servant.clia.md
 */

export enum ExitReason {
	//ERRORS

	//servant
	ServantLoadError = 1,
	ServantModulesLoadError = 2,
	ServantCommandError = 3,
	ServantCommandFailed = 5,
	ServantReportsError = 7,
	ServantWatcherError = 8,
	//--init
	ServantInitError = 101,
	//--generate
	ServantGenerateError = 201,
	//--server
	ServantServerInitError = 901,
	ServantServerStartError = 902,

	//WARNINGS

	//servant
	ServantCommandWarning = -4,
	ServantCommandsEmpty = -6,
}

export function errorCode(code: ExitReason) {
	const currentCode = process.exitCode;

	//save if not defined yet
	if (currentCode === undefined) {
		process.exitCode = code;
		return;
	}

	let rewrite = false;

	//NOTE: Current code is warning and new is error, rewrite it!
	rewrite = rewrite || (currentCode < 0 && code > 0);
	//NOTE: Is more important warning or error
	rewrite = rewrite || (currentCode < 0 && code > currentCode);
	rewrite = rewrite || (currentCode > 0 && code < currentCode);

	if (rewrite) {
		process.exitCode = code;
	}
}

export function exit() {
	process.exit();
}
