import * as React from "react";
import { Box, Text } from "ink";
import * as api from "@servant/servant";

import { ServantState } from "../index";

const commandMargin = 1;
const descriptionLeft = 2;

export const Commands: React.StatelessComponent<ServantState> = (props) => {
	const { commands } = props;

	return (
		<Box flexDirection="column" marginTop={1}>
			<Box flexDirection="column">
				<Box flexDirection="row">
					<Text bold color="green">
						Commands
					</Text>
				</Box>
				{commands.indexOf("clean") >= 0 && (
					<CommandHelp command="clean" url="http://bit.ly/2HVCjHB">
						Command clean is used to remove all files that are not part of project.
					</CommandHelp>
				)}
				{commands.indexOf("install") >= 0 && (
					<CommandHelp command="install" url="http://bit.ly/2WJzvkb">
						Command install is same like npm but is done for all submodule in your
						project.
					</CommandHelp>
				)}
				{commands.indexOf("update") >= 0 && (
					<CommandHelp command="update" url="http://bit.ly/2Uvp3PR">
						Command update is same like npm but is done for all submodule in your
						project.
					</CommandHelp>
				)}
				{commands.indexOf("build") >= 0 && (
					<CommandHelp command="build" url="http://bit.ly/2UcOx5f">
						Command build is used to do all stuff! Build module into single file or
						files.
					</CommandHelp>
				)}
				{commands.indexOf("unify") >= 0 && (
					<CommandHelp command="unify" url="http://bit.ly/2ONDpWl">
						Command unify is used to unify versions through all modules in your project.
					</CommandHelp>
				)}
				{commands.indexOf("publish") >= 0 && (
					<CommandHelp command="publish" url="http://bit.ly/2VkZxd9">
						Command publish is used to publish or create pack from modules in your
						project.
					</CommandHelp>
				)}
				{commands.indexOf("tests") >= 0 && (
					<CommandHelp command="tests" url="http://bit.ly/2VfVVgF">
						Command tests is used to run you tests for whole project.
					</CommandHelp>
				)}
				{commands.indexOf("validate") >= 0 && (
					<CommandHelp command="validate" url="https://bit.ly/3P1F48u">
						Command validate is used to validate all files. Its run all checkers that
						are defined.
					</CommandHelp>
				)}
				{commands.indexOf("analyze") >= 0 && (
					<CommandHelp command="analyze" url="https://bit.ly/3E65uCi">
						Command analyze is used to get some stats info about project and modules.
						You can see sizes, lines of codes and other usefully info.
					</CommandHelp>
				)}
				{commands.indexOf("shared") >= 0 && (
					<CommandHelp command="shared" url="http://bit.ly/3iTIYGq">
						Command shared is used to get info about shared modules in your project. You
						can also link or unlink shared modules.
					</CommandHelp>
				)}
			</Box>
		</Box>
	);
};

interface CommandHelpProps {
	command: api.Commands.Commands;
	url: string;
}

const CommandHelp: React.StatelessComponent<CommandHelpProps> = (props) => {
	const { command, url, children } = props;

	return (
		<Box flexDirection="column" marginLeft={commandMargin} marginTop={commandMargin}>
			<Box>
				<Text bold color="blueBright">
					{command}
				</Text>
			</Box>
			<Box flexDirection="column" marginLeft={descriptionLeft}>
				<Text color="white">{children}</Text>
			</Box>
			<Box flexDirection="row">
				<Text color="gray">More info at: </Text>
				<Text underline color="blueBright">
					{url}
				</Text>
			</Box>
		</Box>
	);
};
