import * as React from "react";
import { ServantState } from "../index";
import { Box, Text } from "ink";
import { CommandStart } from "./command";

const universalFlags = "http://bit.ly/2FQLxBj";
const publishFlags = "http://bit.ly/2VkZxd9";
const unifyFlags = "http://bit.ly/2ONDpWl";
const testsFlags = "http://bit.ly/2VfVVgF";
const cleanFlags = "http://bit.ly/2HVCjHB";
const installFlags = "http://bit.ly/2WJzvkb";
const validateFlags = "https://bit.ly/3P1F48u";
const sharedFlags = "http://bit.ly/3iTIYGq";

export const Flags: React.StatelessComponent<ServantState> = (props) => {
	const { commands } = props;

	return (
		<Box flexDirection="column">
			<Box flexDirection="row" marginTop={1}>
				<Text bold color="green">
					Settings
				</Text>
			</Box>
			{commands.indexOf("install") >= 0 && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text color="cyanBright">--noaudit</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>Disable npm audit checks for install command.</Text>
						</Box>
						<FlagsLink url={installFlags} />
					</Box>
				</Box>
			)}
			{commands.indexOf("publish") >= 0 && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text color="greenBright">--freeze</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>Freeze all version in your package.json when publishing.</Text>
						</Box>
						<FlagsLink url={publishFlags} />
					</Box>
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text>
								<Text color="cyanBright">--tag</Text>{" "}
								<Text underline>{"<tag>"}</Text>
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Publish all modules with tag {"<tag>"}. Used only with --production
								flag.
							</Text>
						</Box>
						<FlagsLink url={publishFlags} />
					</Box>
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text color="green">--commit</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Make commit for every module version incremented. Attach message
								defined in servant.json in property 'publish.commitMessage'.
							</Text>
						</Box>
						<FlagsLink url={publishFlags} />
					</Box>
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text>
								<Text color="redBright">--increment</Text> {"<type>"}
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Override increment value from servant.json and make version
								increment by provided {"<type>"}
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								<Text bold>Allowed types:</Text> major, premajor, minor, preminor,
								patch, prepatch, prerelease, none
							</Text>
						</Box>
						<FlagsLink url={publishFlags} />
					</Box>
				</Box>
			)}
			{commands.indexOf("unify") >= 0 && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text>
								<Text color="white">--m.{"<module-name>"}</Text> {"<version>"}
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Set {"<version>"} for {"<module-name>"} to unify cross project.
							</Text>
						</Box>
						<FlagsLink url={unifyFlags} />
					</Box>
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text color="redBright">--latest</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								When omit --m.{"<module-name>"} flags, Servant automatically set
								versions of all not unified packages to latest version that is used
								in project.
							</Text>
						</Box>
						<FlagsLink url={unifyFlags} />
					</Box>
				</Box>
			)}
			{commands.indexOf("tests") >= 0 && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text color="cyanBright">--gui</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Mark that test can be running without headless mode in browser.
							</Text>
						</Box>
						<FlagsLink url={testsFlags} />
					</Box>
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text>
								<Text color="greenBright">--browsers</Text> {"<browsers>"}
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								List of browsers in which test will be run. Separated by comma (,)
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text bold>
								For this flag you need to install module
								"@servant/servant-playwright" by using "npm install
								@servant/servant-playwright --save-dev".
							</Text>
						</Box>
						<FlagsLink url={testsFlags} />
					</Box>
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text>
								<Text color="greenBright">--devices</Text> {"<devices>"}
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								List of emulated devices in which test will be run. Separated by
								comma (,)
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text bold>
								For this flag you need to install module
								"@servant/servant-playwright" by using "npm install
								@servant/servant-playwright --save-dev".
							</Text>
						</Box>
						<FlagsLink url={testsFlags} />
					</Box>
				</Box>
			)}
			{commands.indexOf("clean") >= 0 && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text color="cyanBright">--prune</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Clean also all node_modules and package-lock.json in module folder.
								After this you must call install command.
							</Text>
						</Box>
						<FlagsLink url={cleanFlags} />
					</Box>
				</Box>
			)}
			{commands.indexOf("validate") >= 0 && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text color="cyanBright">--fix</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>Try to auto fix errors that are against validation rules.</Text>
						</Box>
						<FlagsLink url={validateFlags} />
					</Box>
				</Box>
			)}
			{commands.indexOf("shared") >= 0 && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text color="cyanBright">--link</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Flag that automatically links all shared dependencies to defined
								node_modules.
							</Text>
						</Box>
						<FlagsLink url={sharedFlags} />
					</Box>
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text color="cyanBright">--unlink</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Flag that automatically unlinks all shared dependencies that was
								linked by "link" flag.
							</Text>
						</Box>
						<FlagsLink url={sharedFlags} />
					</Box>
				</Box>
			)}
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text color="cyanBright">--help</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>Show help for defined {"<command>"}.</Text>
				</Box>
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text>
						<Text color="cyanBright">--entry</Text> {"<path>"}
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						Entry point for project where {"<path>"} is absolute path to project.
					</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text>
						<Text color="cyanBright">--only</Text> {"<module-name>"}
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						Run commands for specified module where {"<module-name>"} is module name.
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text bold>Can be used more times.</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text color="cyanBright">--dependencies</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						Applicable only with <Text color="cyanBright">--only</Text> flag. Run
						command with modules and add all modules dependencies. This flag is helpful
						when developing on single module and need run test for all dependant
						modules.
					</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text color="redBright">--production</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>Run commands in production mode.</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text color="greenBright">--watch</Text>
					<Text>, </Text>
					<CommandStart command={false} />
					<Text color="greenBright">--watch</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>Start watching for changes and rebuild project.</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text color="greenBright">--changed</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>Run commands only on changed or not build modules.</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text color="yellowBright">--debug</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>Run in debug mode witn stack traces if error occured.</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text bold>Used mainly by developers of Servant!</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={false} />
					<Text color="greenBright">--init</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						Ignore all commands and settings and run initialize of Servant module.
					</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={false} />
					<Text color="greenBright">--generate</Text> (
					<Text underline>{"<generator-name>"}</Text>)
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						Ignore all commands and settings and run code and content generator. Select
						from list or provide {"<generator-name>"} to run specific generator.
					</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={false} />
					<Text color="greenBright">--server</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>Ignore all commands and run servant development server.</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={false} />
					<Text>
						<Text color="cyanBright">--port</Text> <Text underline>{"<port>"}</Text>
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						User port {"<port>"} as default port for development server. Used only with
						--server flag.
					</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={false} />
					<Text>
						<Text color="cyanBright">--report</Text>{" "}
						<Text underline>{"<report-name>"}</Text>
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						If you want to Servant create report, use {"<report-name>"} to define which
						reports will be generated.
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						<Text bold>Possible reports: </Text> issues, analyze
					</Text>
				</Box>
				<FlagsLink url={universalFlags} />
			</Box>
		</Box>
	);
};

interface FlagsLinkProps {
	url: string;
}

export const FlagsLink: React.StatelessComponent<FlagsLinkProps> = (props) => {
	const { url } = props;

	return (
		<Box flexDirection="row">
			<Text color="gray">More info at: </Text>
			<Text color="blueBright" underline>
				{url}
			</Text>
		</Box>
	);
};
