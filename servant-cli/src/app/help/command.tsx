import * as React from "react";
import { Box, Text } from "ink";
import { ServantFlags, ServantState, ServantModules } from "../index";
import * as api from "@servant/servant";
import { Commands } from "./commands";
import { Flags } from "./flags";

export interface CommandPreviewProps {
	flags: ServantFlags;
	modules: ServantModules;
	commands: Array<api.Commands.Commands>;
	reports: Array<string>;
	port: string | number;
}

export const CommandPreview: React.StatelessComponent<CommandPreviewProps> = (props) => {
	const { commands, flags, modules, port, reports } = props;
	const paddingLeft = 10;

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<Text italic>
					<Text color="gray">servant</Text>
				</Text>
				<Text> </Text>
				{commands.map((command, index) => (
					<Text color="blueBright" key={index}>
						{command}
						<Text> </Text>
					</Text>
				))}
			</Box>
			{flags.entry && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--entry</Text> "
						<Text underline>{flags.entry}</Text>"<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.only.map((only, index) => (
				<Box flexDirection="row" paddingLeft={paddingLeft} key={index}>
					<Text>
						<Text color="cyanBright">--only</Text> "<Text underline>{only}</Text>"
						<Text> </Text>
					</Text>
				</Box>
			))}
			{flags.tag && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--tag</Text> "<Text underline>{flags.tag}</Text>"
						<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.commit && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="green">--commit</Text>{" "}
					</Text>
				</Box>
			)}
			{flags.gui && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--gui</Text>{" "}
					</Text>
				</Box>
			)}
			{flags.fix && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--fix</Text>{" "}
					</Text>
				</Box>
			)}
			{flags.dependencies && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--dependencies</Text>{" "}
					</Text>
				</Box>
			)}
			{flags.browsers.length > 0 && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="greenBright">--browsers</Text> "
						<Text underline>{flags.browsers.join(", ")}</Text>"
					</Text>
				</Box>
			)}
			{flags.devices.length > 0 && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="greenBright">--devices</Text> "
						<Text underline>{flags.devices.join(", ")}</Text>"
					</Text>
				</Box>
			)}
			{flags.production && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="redBright">
						--production<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.latest && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="redBright">
						--latest<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.increment && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="redBright">--increment</Text> "
						<Text underline>{flags.increment}</Text>"
					</Text>
				</Box>
			)}
			{flags.watch && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="greenBright">
						--watch<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.changed && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="greenBright">
						--changed<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.freeze && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="greenBright">
						--freeze<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.debug && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="yellowBright">
						--debug<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.prune && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="yellowBright">
						--prune<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.noaudit && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="yellowBright">
						--noaudit<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.init && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="greenBright">
						--init<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.server && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="greenBright">
						--server<Text> </Text>
					</Text>
				</Box>
			)}
			{port && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--port</Text> "<Text underline>{port}</Text>"
						<Text> </Text>
					</Text>
				</Box>
			)}
			{reports.map((report, index) => (
				<Box flexDirection="row" paddingLeft={paddingLeft} key={index}>
					<Text>
						<Text color="cyanBright">--report</Text> "<Text underline>{report}</Text>"
						<Text> </Text>
					</Text>
				</Box>
			))}
			{flags.link && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="greenBright">
						--link<Text> </Text>
					</Text>
				</Box>
			)}
			{flags.unlink && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text color="greenBright">
						--unlink<Text> </Text>
					</Text>
				</Box>
			)}
			{Object.keys(modules).length > 0 && (
				<Box flexDirection="column" paddingLeft={paddingLeft} marginTop={1}>
					{Object.keys(modules).map((moduleName, index) => (
						<Box flexDirection="row" key={index}>
							<Text>
								<Text color="white">--m.{moduleName}</Text> "
								<Text underline>{modules[moduleName].toString()}</Text>"
								<Text> </Text>
							</Text>
						</Box>
					))}
				</Box>
			)}
		</Box>
	);
};

export const CommandCompose: React.StatelessComponent<ServantState> = (props) => {
	const { commands } = props;

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<CommandStart command={true} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<Text>
						Where <Text color="blueBright">{"<command>"}</Text> is one or more from:
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text underline>clean</Text>
					<Text> </Text>
					<Text underline>install</Text>
					<Text> </Text>
					<Text underline>update</Text>
					<Text> </Text>
					<Text underline>build</Text>
					<Text> </Text>
					<Text underline>unify</Text>
					<Text> </Text>
					<Text underline>publish</Text>
					<Text> </Text>
					<Text underline>tests</Text>
					<Text> </Text>
					<Text underline>validate</Text>
					<Text> </Text>
					<Text underline>analyze</Text>
					<Text> </Text>
					<Text underline>shared</Text>
				</Box>
				{commands.length > 0 && <Commands {...props} />}
			</Box>

			<Flags {...props} />
		</Box>
	);
};

export interface CommandStartProps {
	command: boolean;
}

export const CommandStart: React.StatelessComponent<CommandStartProps> = (props) => {
	const { command } = props;

	return (
		<>
			<Text italic color="gray">
				servant
			</Text>
			<Text> </Text>
			{command && (
				<Text color="blueBright">
					{"<command>"}
					<Text> </Text>
				</Text>
			)}
		</>
	);
};
