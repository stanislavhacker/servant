import * as React from "react";
import { Box, Text } from "ink";

import { ServantState } from "../index";
import { CommandPreview, CommandCompose } from "./command";

export const Help: React.StatelessComponent<ServantState> = (props) => {
	const { m, flags, commands, server, reports } = props;

	return (
		<Box flexDirection="column" marginTop={1}>
			<Box flexDirection="row">
				<Text color="green" bold>
					Servant help
				</Text>
			</Box>
			<Box flexDirection="row" marginTop={1}>
				<Text color="white" underline>
					Provided command for help:
				</Text>
			</Box>
			<Box padding={1}>
				<CommandPreview
					commands={commands}
					flags={flags}
					modules={m}
					port={server.port}
					reports={reports.list}
				/>
			</Box>
			<Box flexDirection="row" marginTop={1}>
				<Text color="white" underline>
					Full command composition:
				</Text>
			</Box>
			<Box padding={1}>
				<CommandCompose {...props} />
			</Box>
		</Box>
	);
};
