import * as api from "@servant/servant";
import * as React from "react";
import { Text, Box } from "ink";

import { Result } from "../result";

const moduleWidth = 70;

export interface PublishProps {
	result: api.Commands.CommandProgress["publish"] | undefined;
	module: string;
	index: number;
}

export const Publish: React.StatelessComponent<PublishProps> = (props) => {
	const { result, module, index } = props;
	const publishResult = result && result.data;

	return (
		<Box flexDirection="row">
			{!result && (
				<Box flexDirection="row">
					<Box width={moduleWidth}>
						<Text>
							<Text bold>
								<Text color="blueBright">{index}.</Text>
							</Text>{" "}
							<Text color="gray">{module}</Text>{" "}
						</Text>
					</Box>
				</Box>
			)}
			{result && (
				<Box flexDirection="row">
					<Box width={moduleWidth}>
						<Result module={module} result={result} index={index} />
					</Box>
					<Box>
						{publishResult && publishResult.private && (
							<Text bold>
								<Text color="yellowBright">private</Text>
								<Text color="gray">, not published</Text>{" "}
							</Text>
						)}
						{publishResult && publishResult.package && (
							<Text>
								<Text underline color="white">
									{publishResult.package}
								</Text>{" "}
							</Text>
						)}
						{publishResult && publishResult.published && (
							<Text>
								<Text italic color="white">
									{publishResult.published}
								</Text>{" "}
							</Text>
						)}
						{publishResult && publishResult.versions[1] && (
							<Text>
								<Text color="greenBright">v{publishResult.versions[1]}</Text>{" "}
							</Text>
						)}
						{publishResult && publishResult.commit && (
							<Text>
								<Text color="green">✔ Committed</Text>{" "}
							</Text>
						)}
					</Box>
				</Box>
			)}
		</Box>
	);
};
