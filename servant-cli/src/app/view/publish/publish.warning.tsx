import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { Warning } from "../warning";

export interface PublishWarningProps {
	publishResult: api.Module.PublishResult;
	production: boolean;
	debug: boolean;
}

export const PublishWarning: React.StatelessComponent<PublishWarningProps> = (props) => {
	const { publishResult } = props;
	const { anyVersions, module, versions } = publishResult;

	return (
		<Box flexDirection="row" marginLeft={2} marginTop={1} marginBottom={1}>
			<Box flexDirection="column">
				<Text>
					Publish for module <Text color="blueBright">{module}</Text> return some
					warnings.
				</Text>
				<Text underline>Check package.json or servant.json.</Text>
				<Box flexDirection="column">
					{versions[1] === null && (
						<Warning
							message={`Module has invalid version. Can not make semver increment from '${versions[0]}'.`}
						/>
					)}
					{anyVersions.map((mod, index) => (
						<Warning
							message={`Dependency module '${mod}' has unknown (*) version.`}
							key={index}
						/>
					))}
				</Box>
			</Box>
		</Box>
	);
};

export interface PublishNonProductionWarningProps {
	production: boolean;
	debug: boolean;
}

export const PublishNonProductionWarning: React.StatelessComponent<
	PublishNonProductionWarningProps
> = (props) => {
	if (props.production) {
		return null;
	}

	return (
		<Box flexDirection="column" marginBottom={1}>
			<Warning message={`Calling publish command without --production flag.`} />
			<Box flexDirection="column" marginLeft={2}>
				<Text color="yellowBright">
					<Text underline bold>
						Modules will not be published into registry!
					</Text>
				</Text>
				<Text color="white">
					Servant will create packages for these modules and save it into module root.
				</Text>
			</Box>
		</Box>
	);
};
