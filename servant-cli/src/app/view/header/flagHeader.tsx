import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";
import Spinner from "ink-spinner";

import { FIRST_COLUMN, SECOND_COLUMN } from "../../constants";

type FlagHeaderProps = {
	date: Date;
	loaded: [number, number] | null;
	packageJson?: api.PackageJson.PackageJsonInfo | null;
	name: string;
	description: string[];
	loader: boolean;
};

export const FlagHeader: React.StatelessComponent<FlagHeaderProps> = ({
	packageJson,
	date,
	loaded,
	name,
	description,
	loader,
}) => {
	const firstDesc = description[0];
	const restDesc = description.slice(1);

	return (
		<Box flexDirection="column" marginBottom={1}>
			<Box flexDirection="row">
				<Box width={FIRST_COLUMN}>
					<Text bold color="green">
						{name}
					</Text>
				</Box>
				{packageJson && (
					<>
						<Box width={SECOND_COLUMN}>
							<Text bold color="yellow">
								v{packageJson.content.version}
							</Text>
						</Box>
						<Box>
							<Text color="white" italic>
								Started at {date.toLocaleTimeString()}
							</Text>
							{loaded !== null && (
								<Text color="gray">
									, load time{" "}
									<Text bold>{`${loaded[0]}s ${loaded[1] / 1000000}ms`}</Text>
								</Text>
							)}
						</Box>
					</>
				)}
			</Box>
			{firstDesc && (
				<Box flexDirection="row">
					<Box width={FIRST_COLUMN}>
						<Text color="green" bold>
							Description
						</Text>
					</Box>
					<Box>
						<Text color="white">{firstDesc}</Text>
					</Box>
				</Box>
			)}
			{restDesc.map((desc, index) => (
				<Box key={index} flexDirection="row">
					<Box width={FIRST_COLUMN} />
					<Box>
						<Text color="white">{desc}</Text>
					</Box>
				</Box>
			))}
			{loader && (
				<Box marginTop={1}>
					<Text color="green">
						<Spinner type="arc" /> Loading {name} runtime ...
					</Text>
				</Box>
			)}
		</Box>
	);
};
