import * as React from "react";
import * as api from "@servant/servant";
import * as path from "path";
import { Box, Text } from "ink";
import Spinner from "ink-spinner";

import { FIRST_COLUMN, SECOND_COLUMN } from "../../constants";
import { ServantFlags, ServantState } from "../../index";
import { Error } from "../error";

export const Header: React.StatelessComponent<ServantState> = (props) => {
	const { servant, modules, date, loaded, flags, commands, reports } = props;
	const { initData } = servant;
	const { modulesData } = modules;

	const isSubmodule = initData && initData.module;

	return (
		<Box flexDirection="column" marginTop={1}>
			<Box flexDirection="column">
				<Box flexDirection="row">
					<Box width={FIRST_COLUMN}>
						<Text bold color="grey">
							Flags
						</Text>
					</Box>
					{flagsArray(commands, flags, reports.list).map((flag) => {
						return (
							<Text key={flag} italic>
								<Text color="yellow">{flag}</Text>{" "}
							</Text>
						);
					})}
					{flagsArray(commands, flags, reports.list).length === 0 && (
						<Text italic>
							<Text color="grey">No flags provided</Text>{" "}
						</Text>
					)}
				</Box>
			</Box>
			<Box flexDirection="column">
				<Box flexDirection="row">
					<Box width={FIRST_COLUMN}>
						<Text bold color="green">
							Servant
						</Text>
					</Box>
					{initData && initData.packageJson && (
						<>
							<Box width={SECOND_COLUMN}>
								<Text bold color="yellow">
									v{initData.packageJson.content.version}
								</Text>
							</Box>
							<Box>
								<Text italic color="white">
									Started at {date.toLocaleTimeString()}
								</Text>
								{loaded !== null && (
									<Text color="gray">
										, load time{" "}
										<Text bold>{`${loaded[0]}s ${loaded[1] / 1000000}ms`}</Text>
									</Text>
								)}
							</Box>
						</>
					)}
				</Box>
			</Box>
			{initData && initData.servantJson && (
				<Box flexDirection="column">
					<Box flexDirection="row">
						<Box width={FIRST_COLUMN}>
							<Text bold color="green">
								Project
							</Text>
						</Box>
						<Box width={SECOND_COLUMN}>
							<Text underline color="cyan">
								{initData.servantJson.content.package ||
									path.basename(initData.servantJson.cwd) ||
									"(empty)"}
							</Text>
						</Box>
						<Text italic color="white">
							Entry on "{initData.entry}"
						</Text>
					</Box>
				</Box>
			)}
			{initData && initData.module && (
				<Box flexDirection="column" marginTop={1}>
					<Box flexDirection="row">
						<Box width={FIRST_COLUMN}>
							<Text bold underline color="blueBright">
								Submodule
							</Text>
						</Box>
						<Box>
							<Text underline color="magenta">
								{initData.module}
							</Text>
						</Box>
					</Box>
					<Box marginLeft={1} marginRight={1} width={80}>
						<Text color="blueBright" italic>
							Servant detect that you run command in submodule. Automatically run
							Servant command in parent project for given submodule.
						</Text>
					</Box>
				</Box>
			)}
			{modulesData && modulesData.graph && !isSubmodule && (
				<Box flexDirection="column">
					<Box flexDirection="row">
						<Box width={FIRST_COLUMN}>
							<Text bold color="green">
								Modules
							</Text>
						</Box>
						<Box width={SECOND_COLUMN}>
							<Text underline color="magenta">
								{api.Module.iterateSorted(modulesData.graph.sorted).length} modules
							</Text>
						</Box>
					</Box>
				</Box>
			)}
			{showLoader(props) && (
				<Box>
					<Text color="green">
						<Spinner type="arc" /> Loading Servant ...
					</Text>
				</Box>
			)}
			{servant.error && <Error error={servant.error} debug={flags.debug} />}
		</Box>
	);
};

function showLoader(props: ServantState) {
	const { servant, modules } = props;
	const { initData } = servant;

	return (
		(!initData || !initData.packageJson || !initData.servantJson) &&
		!servant.error &&
		!modules.error
	);
}

function flagsArray(
	commands: Array<string>,
	flags: ServantFlags,
	reports: Array<string>
): Array<string> {
	const array: Array<string> = [];

	if (flags.debug) {
		array.push("DEBUG");
	}
	if (flags.gui) {
		array.push("GUI");
	}
	if (flags.dependencies) {
		array.push("DEPENDENCIES");
	}
	if (flags.changed) {
		array.push("CHANGED");
	}
	if (flags.server) {
		array.push("SERVER");
	}
	if (flags.entry) {
		//No flag for entry
	}
	if (flags.init) {
		array.push("INIT");
	}
	if (flags.freeze) {
		array.push("FREEZE");
	}
	if (flags.production) {
		array.push("PRODUCTION");
	}
	if (flags.watch) {
		array.push("WATCH");
	}
	if (flags.tag) {
		array.push("TAG");
	}
	if (flags.prune) {
		array.push("PRUNE");
	}
	if (flags.fix) {
		array.push("FIX");
	}
	if (flags.latest) {
		array.push("LATEST");
	}
	if (flags.increment) {
		array.push("INCREMENT");
	}
	if (flags.noaudit) {
		array.push("NOAUDIT");
	}
	if (flags.only && flags.only.length > 0) {
		array.push("ONLY");
	}
	if (flags.link) {
		array.push("LINK");
	}
	if (flags.unlink) {
		array.push("UNLINK");
	}
	if (reports.length > 0) {
		array.push("REPORT");
	}

	commands.forEach((command) => {
		array.push(command.toUpperCase());
	});

	return array;
}
