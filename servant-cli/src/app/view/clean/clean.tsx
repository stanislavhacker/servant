import * as api from "@servant/servant";
import * as React from "react";
import { Text } from "ink";

import { Result } from "../result";

export interface CleanProps {
	result: api.Commands.CommandProgress["clean"] | undefined;
	module: string;
	index: number;
}

export const Clean: React.StatelessComponent<CleanProps> = (props) => {
	const { result, module, index } = props;

	return (
		<Text>
			{!result && (
				<Text>
					<Text bold>
						<Text color="blueBright">{index}.</Text>
					</Text>{" "}
					<Text color="gray">{module}</Text>{" "}
				</Text>
			)}
			{result && <Result module={module} result={result} index={index} />}
		</Text>
	);
};
