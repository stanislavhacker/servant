import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { ValidateResults } from "./validate.results";
import { groupValidationResults } from "../utils/validate";

export interface ValidateOkProps {
	validateResult: api.Module.ValidateResult;
	debug: boolean;
}

export const ValidateOk: React.StatelessComponent<ValidateOkProps> = (props) => {
	const { validateResult, debug } = props;
	const results = groupValidationResults(validateResult);

	//NOTE: In OK status we need only to show fixed files
	if (results.fixedFiles.length === 0) {
		return null;
	}

	return (
		<Box flexDirection="column" marginLeft={1} marginTop={1} marginBottom={1}>
			<Box>
				<Text bold underline>
					{validateResult.module}
				</Text>
			</Box>
			<ValidateResults {...results} debug={debug} />
		</Box>
	);
};
