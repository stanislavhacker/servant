import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { getValidationResultsStatus, mergeValidationResults } from "../utils/validate";
import { Result } from "../result";

const moduleWidth = 45;
const countWidth = 6;
const fixableWidth = 20;
const indexWidth = 5;

export interface ValidateProps {
	result: api.Commands.CommandProgress["validate"] | undefined;
	module: string;
	index: number;
}

export const Validate: React.StatelessComponent<ValidateProps> = (props) => {
	const { result, module, index } = props;
	const validateResult = result && result.data;

	const allResults = mergeValidationResults([
		validateResult && validateResult.eslint,
		validateResult && validateResult.prettier,
		validateResult && validateResult.trancorder,
	]);
	const { infos, errors, warnings, suppressed, fixable } = getValidationResultsStatus(allResults);

	return (
		<Box flexDirection="row">
			{!result && (
				<Box flexDirection="row">
					<Box width={moduleWidth + indexWidth}>
						<Text>
							<Text bold color="blueBright">
								{index}.
							</Text>{" "}
							<Text color="gray">{module}</Text>{" "}
						</Text>
					</Box>
				</Box>
			)}
			{result && (
				<Box flexDirection="row">
					<Box width={moduleWidth + indexWidth}>
						<Result module={module} result={result} index={index} />
					</Box>
					<ValidateSummaryView
						errors={errors.length}
						warnings={warnings.length}
						infos={infos.length}
						suppressed={suppressed.length}
						fixable={fixable.length}
						available={allResults.available}
					/>
					<Box>
						<ValidateResultsView
							errors={errors.length}
							warnings={warnings.length}
							infos={infos.length}
							type={allResults.type}
							available={allResults.available}
						/>
					</Box>
				</Box>
			)}
		</Box>
	);
};

interface ValidateSummaryProp {
	infos: number;
	errors: number;
	warnings: number;
	suppressed: number;
	fixable: number;
	available: boolean;
}

export const ValidateSummaryView: React.StatelessComponent<ValidateSummaryProp> = (props) => {
	const { infos, errors, warnings, suppressed, fixable, available } = props;

	if (!available) {
		return (
			<Box width={countWidth * 4 + fixableWidth}>
				<Text italic color="gray">
					validations unavailable
				</Text>
			</Box>
		);
	}

	return (
		<>
			<Box width={countWidth}>
				{errors > 0 && (
					<Text bold color="redBright">
						{errors}✘
					</Text>
				)}
			</Box>
			<Box width={countWidth}>
				{warnings > 0 && (
					<Text bold color="yellowBright">
						{warnings}!
					</Text>
				)}
			</Box>
			<Box width={countWidth}>
				{suppressed > 0 && (
					<Text bold color="gray">
						{" "}
						{suppressed}✖
					</Text>
				)}
			</Box>
			<Box width={countWidth}>
				{infos > 0 && (
					<Text bold color="blueBright">
						{" "}
						{infos}i
					</Text>
				)}
			</Box>
			<Box width={fixableWidth}>
				{fixable > 0 && (
					<Text underline italic color="magentaBright">
						{fixable} fixable
					</Text>
				)}
			</Box>
		</>
	);
};

interface ValidateResultsProp {
	infos: number;
	errors: number;
	warnings: number;
	type: api.Module.ValidationDoneType;
	available: boolean;
}

const ValidateResultsView: React.StatelessComponent<ValidateResultsProp> = (props) => {
	const { errors, warnings, infos, type, available } = props;
	const fail =
		errors > 0 ||
		type === api.Module.ValidationDoneType.FAIL ||
		type === api.Module.ValidationDoneType.ERROR;

	return (
		<>
			{available && !fail && warnings === 0 && infos === 0 && (
				<Text bold backgroundColor="green">
					[PASS]
				</Text>
			)}
			{available && !fail && warnings === 0 && infos > 0 && (
				<Text bold backgroundColor="blueBright">
					[PASS]
				</Text>
			)}
			{available && !fail && warnings > 0 && (
				<Text bold backgroundColor="yellowBright">
					[PASS]
				</Text>
			)}
			{available && fail && (
				<Text bold backgroundColor="red">
					[FAIL]
				</Text>
			)}
			{!available && (
				<Text bold backgroundColor="gray">
					[OFF]
				</Text>
			)}
		</>
	);
};
