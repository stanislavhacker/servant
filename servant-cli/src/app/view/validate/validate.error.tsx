import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { Error as ErrorComponent } from "../error";
import { collectMissingPackages, groupValidationResults } from "../utils/validate";
import { ValidateResults } from "./validate.results";

export interface ValidateErrorProps {
	validateResult: api.Module.ValidateResult;
	debug: boolean;
}

export const ValidateError: React.StatelessComponent<ValidateErrorProps> = (props) => {
	const { validateResult, debug } = props;
	const results = groupValidationResults(validateResult);
	const missingPackages = collectMissingPackages(validateResult);

	return (
		<Box flexDirection="column" marginLeft={1} marginTop={1} marginBottom={1}>
			<Box>
				<Text bold underline>
					{validateResult.module}
				</Text>
			</Box>
			<MissingPackagesError missingPackages={missingPackages} debug={debug} />
			<ValidateResults {...results} debug={debug} />
		</Box>
	);
};

const MissingPackagesError: React.StatelessComponent<{
	missingPackages: Array<string>;
	debug: boolean;
}> = (props) => {
	const { missingPackages, debug } = props;

	return (
		<>
			{missingPackages.length > 0 && (
				<>
					<ErrorComponent
						error={new Error(`Servant detect missing packages!`)}
						debug={debug}
					/>
					<Box flexDirection="column" marginLeft={2}>
						<Text color="redBright">
							<Text underline bold>
								Some packages need for validations are not installed:
							</Text>
						</Text>
						{missingPackages.map((pck, index) => (
							<Text key={index}>
								{" "}
								<Text bold>{index + 1}.</Text> {pck}
							</Text>
						))}
					</Box>
					<Box flexDirection="column" marginLeft={2} marginTop={1}>
						<Text color="blueBright">
							<Text>TIP:</Text> Add these packages into{" "}
							<Text underline>package.json</Text> and run command "servant install".
						</Text>
					</Box>
				</>
			)}
		</>
	);
};
