import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { GroupedResult, GroupedResults } from "../utils/validate";

export interface ValidateResultsProps extends GroupedResults {
	debug: boolean;
}

export const ValidateResults: React.StatelessComponent<ValidateResultsProps> = (props) => {
	return (
		<ValidateGrouped
			{...props}
			itemRenderer={({ id, type, message, fixable, engine, severity }, results, index) => (
				<Box flexDirection="column" marginTop={1} key={index}>
					<ValidateHeader
						engine={engine}
						id={id}
						type={type}
						count={results.length}
						fixable={fixable}
						severity={severity}
					/>
					<ValidateMessage message={message} />
					<ValidateFilesList items={results} />
				</Box>
			)}
		/>
	);
};

//GROUPING, GLOBAL REPORTS

const ValidateGrouped: React.StatelessComponent<
	GroupedResults & {
		itemRenderer: (main: GroupedResult, results: GroupedResult[], index: number) => JSX.Element;
	}
> = (props) => {
	const { groups, fixedFiles, itemRenderer } = props;

	return (
		<>
			<ReportFixedFiles fixedFiles={fixedFiles} />
			<ReportValidationResults groups={groups} itemRenderer={itemRenderer} />
		</>
	);
};

const ReportFixedFiles: React.StatelessComponent<{
	fixedFiles: Array<string>;
}> = ({ fixedFiles }) => {
	if (fixedFiles.length === 0) {
		return null;
	}

	return (
		<Box flexDirection="column" marginTop={1}>
			<Text color="magentaBright" bold>
				Servant runs in <Text underline>autofix</Text> mode and these files were updated
				during validation!
			</Text>
			<Box flexDirection="row" marginTop={1}>
				{fixedFiles.map((file, index) => (
					<ValidateFile file={file} index={index} key={index} />
				))}
			</Box>
		</Box>
	);
};

const ReportValidationResults: React.StatelessComponent<{
	groups: GroupedResults["groups"];
	itemRenderer: (main: GroupedResult, results: GroupedResult[], index: number) => JSX.Element;
}> = ({ groups, itemRenderer }) => {
	return (
		<>
			{Object.keys(groups).length > 0 &&
				Object.keys(groups).map((engine: api.Module.ValidationEngine) => {
					const rules = groups[engine];
					return Object.keys(rules).map((rule, index) => {
						const messages = rules[rule];
						return (
							<Box flexDirection="column" key={`${index}-${engine}-${rule}`}>
								{Object.keys(messages).map((msg, index) => {
									const items = messages[msg];
									const first = items[0];

									//SKIP none message
									if (
										first.severity ===
										api.Module.ValidationUniversalSeverity.NONE
									) {
										return null;
									}
									//render item
									return itemRenderer(first, items, index);
								})}
							</Box>
						);
					});
				})}
		</>
	);
};

//HEADER

const ValidateHeader: React.StatelessComponent<{
	engine: api.Module.ValidationEngine;
	id: string;
	type: string;
	count: number;
	fixable: boolean;
	severity: api.Module.ValidationUniversalSeverity;
}> = ({ engine, id, type, count, fixable, severity }) => {
	return (
		<Box flexDirection="row">
			<ValidateEngineHeader engine={engine} />
			<ValidateRuleHeader severity={severity} id={id} type={type} />
			<ValidateCountHeader severity={severity} count={count} fixable={fixable} />
		</Box>
	);
};

const ValidateEngineHeader: React.StatelessComponent<{
	engine: api.Module.ValidationEngine;
}> = ({ engine }) => {
	switch (engine) {
		case api.Module.ValidationEngine.Eslint:
			return (
				<Text color="cyan" bold>
					[{engine.toUpperCase()}]:{" "}
				</Text>
			);
		case api.Module.ValidationEngine.Prettier:
			return (
				<Text color="yellow" bold>
					[{engine.toUpperCase()}]:{" "}
				</Text>
			);
		case api.Module.ValidationEngine.Trancorder:
			return (
				<Text color="magenta" bold>
					[{engine.toUpperCase()}]:{" "}
				</Text>
			);
		default:
			return null;
	}
};

const ValidateRuleHeader: React.StatelessComponent<{
	id: string;
	type: string;
	severity: api.Module.ValidationUniversalSeverity;
}> = ({ id, type, severity }) => {
	const items: JSX.Element[] = [];

	switch (severity) {
		case api.Module.ValidationUniversalSeverity.ERROR:
			items.push(
				<Text color="red" key={severity}>
					{id}
				</Text>
			);
			break;
		case api.Module.ValidationUniversalSeverity.WARNING:
			items.push(
				<Text color="yellow" key={severity}>
					{id}
				</Text>
			);
			break;
		case api.Module.ValidationUniversalSeverity.INFO:
			items.push(
				<Text color="blue" key={severity}>
					{id}
				</Text>
			);
			break;
		default:
			break;
	}

	if (type) {
		items.push(
			<Text key="type">
				{" "}
				<Text italic color="gray">
					({type})
				</Text>
			</Text>
		);
	}

	items.push(<Text key="space"> </Text>);

	return <>{items}</>;
};

const ValidateCountHeader: React.StatelessComponent<{
	count: number;
	fixable: boolean;
	severity: api.Module.ValidationUniversalSeverity;
}> = ({ severity, count, fixable }) => {
	const items: JSX.Element[] = [];

	switch (severity) {
		case api.Module.ValidationUniversalSeverity.ERROR:
			items.push(
				<Text color="red" key={severity}>
					{count}x
				</Text>
			);
			break;
		case api.Module.ValidationUniversalSeverity.WARNING:
			items.push(
				<Text color="yellow" key={severity}>
					{count}x
				</Text>
			);
			break;
		case api.Module.ValidationUniversalSeverity.INFO:
			items.push(
				<Text color="blue" key={severity}>
					{count}x
				</Text>
			);
			break;
		default:
			items.push(
				<Text color="whiteBright" key={severity}>
					{count}x
				</Text>
			);
			break;
	}

	if (fixable) {
		items.push(
			<Text key="fixable">
				{" "}
				<Text color="magentaBright" italic underline>
					fixable
				</Text>
			</Text>
		);
	}

	return <>{items}</>;
};

//MESSAGE

const ValidateMessage: React.StatelessComponent<{ message: string }> = ({ message }) => {
	if (message) {
		return (
			<Box>
				<Text italic color="whiteBright">
					{message}
				</Text>
			</Box>
		);
	}
	return null;
};

//FILES

const ValidateFile: React.StatelessComponent<{
	index: number;
	file: string;
	pointers?: Array<string>;
}> = ({ index, file, pointers = [] }) => {
	const preview = pointers.slice(0, 10);

	return (
		<Text key={index}>
			<Text color="gray">{index + 1}. </Text>
			<Text color="blueBright" key={index}>
				{file}
			</Text>
			{pointers.length > 0 && (
				<>
					<Text> </Text>
					{preview.map((pointer, index) => (
						<Text key={index}>
							<Text color="blue">{pointer}</Text>,{" "}
						</Text>
					))}
					{preview.length < pointers.length && (
						<Text color="white">+{pointers.length - preview.length} places</Text>
					)}
				</>
			)}
			<Text> </Text>
		</Text>
	);
};

const ValidateFilesList: React.StatelessComponent<{
	items: GroupedResult[];
}> = ({ items }) => {
	return (
		<Box flexDirection="column" marginTop={1}>
			{items
				.filter(({ file }) => Boolean(file))
				.map(({ file, pointers }, index) => (
					<ValidateFile key={index} file={file || ""} pointers={pointers} index={index} />
				))}
		</Box>
	);
};
