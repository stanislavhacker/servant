import * as api from "@servant/servant";
import * as React from "react";
import { Text } from "ink";
import Spinner from "ink-spinner";

export interface ResultProps<C extends api.Commands.Commands> {
	result: api.Commands.CommandProgress[C];
	module: string;
	index: number;
}

export function Result<C extends api.Commands.Commands>(props: ResultProps<C>) {
	const { result, module, index } = props;

	return (
		<Text>
			{result.type === api.Module.DoneType.OK && (
				<Text>
					<Text bold color="blueBright">
						{index}.
					</Text>{" "}
					<Text strikethrough color="green">
						{module}
					</Text>{" "}
				</Text>
			)}
			{result.type === api.Module.DoneType.WARNING && (
				<Text>
					<Text bold color="blueBright">
						{index}.
					</Text>{" "}
					<Text strikethrough color="yellowBright">
						{module}
					</Text>{" "}
				</Text>
			)}
			{result.type === api.Module.DoneType.FAIL && (
				<Text>
					<Text bold color="blueBright">
						{index}.
					</Text>{" "}
					<Text strikethrough color="redBright">
						{module}
					</Text>{" "}
				</Text>
			)}
		</Text>
	);
}

export interface DotResultProps<C extends api.Commands.Commands> {
	result?: api.Commands.CommandProgress[C];
}

export function DotResult<C extends api.Commands.Commands>(props: DotResultProps<C>) {
	const { result } = props;

	return (
		<Text>
			{!result && (
				<Text>
					<Text bold color="gray">
						<Spinner type="bounce" />
					</Text>
				</Text>
			)}
			{result && result.type === api.Module.DoneType.OK && (
				<Text>
					<Text bold color="green">
						.
					</Text>
				</Text>
			)}
			{result && result.type === api.Module.DoneType.WARNING && (
				<Text>
					<Text bold color="yellowBright">
						.
					</Text>
				</Text>
			)}
			{result && result.type === api.Module.DoneType.FAIL && (
				<Text>
					<Text bold color="redBright">
						.
					</Text>
				</Text>
			)}
		</Text>
	);
}
