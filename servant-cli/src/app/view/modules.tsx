import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";
import Spinner from "ink-spinner";

import { CommandInfo, ServantState, areCommands } from "../index";
import { FIRST_COLUMN } from "../constants";

import { Clean } from "./clean";
import { Error } from "./error";
import { Install } from "./install";
import { Warning } from "./warning";
import { Update } from "./update";
import { Unify } from "./unify";
import { Analyze } from "./analyze";
import { Shared, SharedError, SharedOk } from "./shared";
import { Build, BuildError, BuildWarning } from "./build";
import { Tests, TestsError, TestsIssuesReport } from "./tests";
import { Publish, PublishWarning, PublishNonProductionWarning } from "./publish";
import { Validate, ValidateError, ValidateWarning, ValidateOk } from "./validate";

import { Validation } from "./modules/validations";

export const Modules: React.StatelessComponent<ServantState> = (props) => {
	const { modules, flags, servant, commands } = props;
	const { initData } = servant;

	//no init data
	if (!initData) {
		return null;
	}

	const graph = modules.modulesData && modules.modulesData.graph;
	const validation = modules.modulesData && modules.modulesData.validation;

	return (
		<Box flexDirection="column" marginTop={1}>
			{!graph && !modules.error && (
				<Box>
					<Text color="green">
						<Spinner type="arc" /> Loading Modules ...
					</Text>
				</Box>
			)}
			{validation && (
				<Box flexDirection="column">
					<Box flexDirection="row">
						<Box width={FIRST_COLUMN}>
							<Text bold color="green">
								Validation
							</Text>
						</Box>
						<Box>
							<Text bold color="magenta">
								Validation of internal and external modules
							</Text>
						</Box>
					</Box>
					<Validation validation={validation} />
				</Box>
			)}
			{graph && (
				<Box flexDirection="column">
					<Box flexDirection="row">
						<Box width={FIRST_COLUMN}>
							<Text bold color="green">
								Resolved
							</Text>
						</Box>
						<Box>
							<Text bold color="magenta">
								{api.Module.iterateSorted(graph.sorted).length} modules to build in
								current order
							</Text>
						</Box>
					</Box>
					<Box flexDirection="row" marginBottom={1} minWidth={Number.MAX_SAFE_INTEGER}>
						{api.Module.iterateSorted(graph.sorted).length === 0 && (
							<Box marginTop={1} flexDirection="column">
								<Text bold>
									<Text color="yellowBright">No modules found to build. </Text>
									{flags.changed && (
										<Text color="greenBright">Everything is up to date!</Text>
									)}
								</Text>
								{!flags.changed && (
									<Box marginLeft={2} flexDirection="column">
										<Text>
											<Text>There are no modules found to process. </Text>
											<Text>
												Maybe you have specify invalid{" "}
												<Text color="greenBright">'modules'</Text> pattern
												in <Text color="greenBright">servant.json</Text>.
											</Text>
										</Text>
										{servant.initData && servant.initData.servantJson && (
											<Text>
												Are you sure that pattern{" "}
												<Text color="greenBright">
													'
													{servant.initData.servantJson.content.modules.join(
														", "
													)}
													'
												</Text>{" "}
												is valid?
											</Text>
										)}
									</Box>
								)}
							</Box>
						)}
						{api.Module.iterateSorted(graph.sorted).map((module, index) => (
							<Box key={index}>
								<Text bold>
									<Text color="blueBright">{index + 1}.</Text>{" "}
								</Text>
								<Text>
									<Text color="white">{module}</Text>@
									<Text color="greenBright">
										v{getModuleVersion(graph, module)}
									</Text>{" "}
								</Text>
							</Box>
						))}
					</Box>
				</Box>
			)}
			{graph && graph.changes && api.Module.iterateSorted(graph.sorted).length > 0 && (
				<Box flexDirection="column">
					<Box flexDirection="row">
						<Box width={FIRST_COLUMN}>
							<Text color="green">
								<Text bold>Changed</Text>
							</Text>
						</Box>
						<Box>
							<Text color="magenta">
								<Text bold>
									{graph.changes.modules.filter((m) => m.changed).length} modules
									has changed
								</Text>
							</Text>
						</Box>
					</Box>
					<Box flexDirection="column" marginLeft={2} marginTop={1} marginBottom={1}>
						<Box>
							<Text>
								There is{" "}
								<Text color="magenta">
									<Text bold>
										{
											graph.changes.modules.filter(
												(m) =>
													m.changedBy.indexOf(
														api.Changes.ChangeBy.FILESYSTEM
													) >= 0
											).length
										}
									</Text>
								</Text>{" "}
								modules changed by <Text color="blueBright">filesystem</Text>{" "}
								modified time.
							</Text>
						</Box>
						<Box>
							<Text>
								There is{" "}
								<Text color="magenta">
									<Text bold>
										{
											graph.changes.modules.filter(
												(m) =>
													m.changedBy.indexOf(api.Changes.ChangeBy.GIT) >=
													0
											).length
										}
									</Text>
								</Text>{" "}
								modules changed by <Text color="greenBright">git</Text> status.
							</Text>
						</Box>
					</Box>
				</Box>
			)}
			{graph && (
				<Box flexDirection="column">
					{Object.keys(modules.commands).map((command) => {
						return (
							<Command
								key={command}
								debug={flags.debug}
								production={flags.production}
								command={command as api.Commands.Commands}
								info={modules.commands[command]}
								graph={graph}
							/>
						);
					})}
				</Box>
			)}
			{graph && areCommands(commands, flags) && (
				<Box flexDirection="column" marginLeft={1}>
					<Box>
						<Text color="yellowBright">
							<Text bold>You are not provided any command parameter!</Text>
						</Text>
					</Box>
					<Box>
						<Text color="yellowBright">
							<Text bold>
								As your servant i'm not able to do anything without commanding me :)
							</Text>
						</Text>
					</Box>
					<Box>
						<Text color="yellowBright">
							<Text bold underline>
								Sorry.
							</Text>
						</Text>
					</Box>
				</Box>
			)}
			{modules.error && <Error error={modules.error} debug={flags.debug} />}
		</Box>
	);
};

export interface CommandProps<C extends api.Commands.Commands> {
	debug: boolean;
	production: boolean;
	command: C;
	info: CommandInfo<C>;
	graph: api.Module.DependenciesGraph;
}

export function Command<C extends api.Commands.Commands>(props: CommandProps<C>) {
	const { graph, command, info, debug, production } = props;
	const modules = Object.keys(info.modules || {});

	const failed = modules.filter(
		(module) => info.modules[module].type === api.Module.DoneType.FAIL
	);
	const warning = modules.filter(
		(module) => info.modules[module].type === api.Module.DoneType.WARNING
	);
	const ok = modules.filter((module) => info.modules[module].type === api.Module.DoneType.OK);

	//not running yet
	if (!info.running) {
		return null;
	}

	return (
		<Box flexDirection="column" marginBottom={1}>
			<Box flexDirection="row">
				<Box width={FIRST_COLUMN}>
					<Text bold color="green">
						Command
					</Text>
				</Box>
				<Box>
					<Text bold color="cyan">
						{command}
					</Text>
				</Box>
			</Box>
			{command === "clean" && (
				<Box flexDirection="row" minWidth={Number.MAX_SAFE_INTEGER}>
					{api.Module.iterateSorted(graph.sorted).map((module, index) => (
						<Clean
							result={info.modules[module] as api.Commands.CommandProgress["clean"]}
							module={module}
							index={index + 1}
							key={module}
						/>
					))}
				</Box>
			)}
			{command === "install" && (
				<Box flexDirection="row" minWidth={Number.MAX_SAFE_INTEGER}>
					{api.Module.iterateSorted(graph.sorted).map((module, index) => (
						<Install
							result={info.modules[module] as api.Commands.CommandProgress["install"]}
							module={module}
							index={index + 1}
							key={module}
						/>
					))}
				</Box>
			)}
			{command === "update" && (
				<Box flexDirection="row" minWidth={Number.MAX_SAFE_INTEGER}>
					{api.Module.iterateSorted(graph.sorted).map((module, index) => (
						<Update
							result={info.modules[module] as api.Commands.CommandProgress["update"]}
							module={module}
							index={index + 1}
							key={module}
						/>
					))}
				</Box>
			)}
			{command === "build" && (
				<Box flexDirection="column" minWidth={Number.MAX_SAFE_INTEGER}>
					{api.Module.iterateSorted(graph.sorted).map((module, index) => (
						<Build
							result={info.modules[module] as api.Commands.CommandProgress["build"]}
							module={module}
							index={index + 1}
							key={module}
						/>
					))}
				</Box>
			)}
			{command === "tests" && (
				<Box flexDirection="column" minWidth={Number.MAX_SAFE_INTEGER}>
					{api.Module.iterateSorted(graph.sorted).map((module, index) => (
						<Tests
							result={info.modules[module] as api.Commands.CommandProgress["tests"]}
							module={module}
							index={index + 1}
							key={module}
						/>
					))}
					{api.Module.iterateSorted(graph.sorted).length === modules.length && (
						<TestsIssuesReport modules={info.modules} />
					)}
				</Box>
			)}
			{command === "unify" && (
				<Box flexDirection="column" minWidth={Number.MAX_SAFE_INTEGER}>
					<Box flexDirection="row">
						{api.Module.iterateSorted(graph.sorted).map((module, index) => (
							<Unify
								result={
									info.modules[module] as api.Commands.CommandProgress["unify"]
								}
								module={module}
								index={index + 1}
								key={module}
							/>
						))}
					</Box>
				</Box>
			)}
			{command === "publish" && (
				<Box flexDirection="column" minWidth={Number.MAX_SAFE_INTEGER}>
					<Box flexDirection="column">
						{api.Module.iterateSorted(graph.sorted).map((module, index) => (
							<Publish
								result={
									info.modules[module] as api.Commands.CommandProgress["publish"]
								}
								module={module}
								index={index + 1}
								key={module}
							/>
						))}
					</Box>
					<PublishNonProductionWarning production={production} debug={debug} />
				</Box>
			)}
			{command === "validate" && (
				<Box flexDirection="column" minWidth={Number.MAX_SAFE_INTEGER}>
					<Box flexDirection="column">
						{api.Module.iterateSorted(graph.sorted).map((module, index) => (
							<Validate
								result={
									info.modules[module] as api.Commands.CommandProgress["validate"]
								}
								module={module}
								index={index + 1}
								key={module}
							/>
						))}
					</Box>
				</Box>
			)}
			{command === "analyze" && (
				<Box flexDirection="row" minWidth={Number.MAX_SAFE_INTEGER}>
					{api.Module.iterateSorted(graph.sorted).map((module, index) => (
						<Analyze
							result={info.modules[module] as api.Commands.CommandProgress["analyze"]}
							module={module}
							index={index + 1}
							key={module}
						/>
					))}
				</Box>
			)}
			{command === "shared" && (
				<Box flexDirection="column" minWidth={Number.MAX_SAFE_INTEGER}>
					{api.Module.iterateSorted(graph.sorted).map((module, index) => (
						<Shared
							result={info.modules[module] as api.Commands.CommandProgress["shared"]}
							module={module}
							index={index + 1}
							key={module}
						/>
					))}
				</Box>
			)}
			{api.Module.iterateSorted(graph.sorted).length === modules.length && info.done && (
				<Box flexDirection="column" minWidth={Number.MAX_SAFE_INTEGER}>
					{warning.length > 0 && (
						<Box flexDirection="column">
							{warning.map((module, index) => {
								const res = info.modules[module];

								//special implementation of warnings
								if (command === "build") {
									return (
										<Box key={index}>
											<BuildWarning
												buildResult={
													res.data as NonNullable<
														api.Commands.CommandProgress["build"]["data"]
													>
												}
												debug={debug}
											/>
										</Box>
									);
								}
								if (command === "publish") {
									return (
										<Box key={index}>
											<PublishWarning
												publishResult={
													res.data as NonNullable<
														api.Commands.CommandProgress["publish"]["data"]
													>
												}
												debug={debug}
												production={production}
											/>
										</Box>
									);
								}
								if (command === "validate") {
									return (
										<Box key={index}>
											<ValidateWarning
												validateResult={
													res.data as NonNullable<
														api.Commands.CommandProgress["validate"]["data"]
													>
												}
												debug={debug}
											/>
										</Box>
									);
								}
								if (command === "tests") {
									//NOTE: Tests has no warnings implemented yet
									return null;
								}
								//default
								return (
									<Box key={index}>
										<Warning message={res.message.join(" ")} />
									</Box>
								);
							})}
						</Box>
					)}
					{failed.length > 0 && (
						<Box flexDirection="column">
							{failed.map((module, index) => {
								const res = info.modules[module];

								//special implementation of errors
								if (command === "build") {
									return (
										<Box key={index}>
											<BuildError
												buildResult={
													res.data as NonNullable<
														api.Commands.CommandProgress["build"]["data"]
													>
												}
												debug={debug}
											/>
										</Box>
									);
								}
								if (command === "tests") {
									return (
										<Box key={index}>
											<TestsError
												testsResult={
													res.data as NonNullable<
														api.Commands.CommandProgress["tests"]["data"]
													>
												}
												debug={debug}
												result={res}
											/>
										</Box>
									);
								}
								if (command === "validate") {
									return (
										<Box key={index}>
											<ValidateError
												validateResult={
													res.data as NonNullable<
														api.Commands.CommandProgress["validate"]["data"]
													>
												}
												debug={debug}
											/>
										</Box>
									);
								}
								if (command === "shared") {
									return (
										<Box key={index}>
											<SharedError
												sharedResult={
													res.data as NonNullable<
														api.Commands.CommandProgress["shared"]["data"]
													>
												}
												debug={debug}
											/>
										</Box>
									);
								}
								//default
								if (res.error) {
									return (
										<Box key={index}>
											<Error error={res.error} debug={debug} />
										</Box>
									);
								}
							})}
						</Box>
					)}
					{ok.length > 0 && (
						<Box flexDirection="column">
							{ok.map((module, index) => {
								const res = info.modules[module];

								//special implementation of ok
								if (command === "validate") {
									return (
										<Box key={index}>
											<ValidateOk
												validateResult={
													res.data as NonNullable<
														api.Commands.CommandProgress["validate"]["data"]
													>
												}
												debug={debug}
											/>
										</Box>
									);
								}
								if (command === "shared") {
									return (
										<Box key={index}>
											<SharedOk
												sharedResult={
													res.data as NonNullable<
														api.Commands.CommandProgress["shared"]["data"]
													>
												}
												debug={debug}
											/>
										</Box>
									);
								}
								//default
								return null;
							})}
						</Box>
					)}
					<Box flexDirection="row">
						<Text color="white">
							<Text color="green" bold>
								Command
							</Text>{" "}
							'
							<Text color="cyan" bold>
								{command}
							</Text>
							' is complete.{" "}
						</Text>
						{(warning.length > 0 || failed.length > 0) && (
							<Results failed={failed.length} warning={warning.length} />
						)}
						{info.time && (
							<Text color="blueBright" italic>
								{" "}
								+{info.time}
							</Text>
						)}
					</Box>
				</Box>
			)}
		</Box>
	);
}

function getModuleVersion(graph: api.Module.DependenciesGraph, module: string) {
	return graph.modules[module].versions.filter(api.PackageJson.versionIsExternal)[0];
}

interface ResultsProps {
	warning: number;
	failed: number;
}

const Results: React.StatelessComponent<ResultsProps> = (props) => {
	const { warning, failed } = props;

	return (
		<Text>
			<Text color="white">Results </Text>
			{warning > 0 && <Text color="yellowBright">{warning} modules warn</Text>}
			{warning > 0 && failed > 0 && <Text color="white"> and </Text>}
			{failed > 0 && <Text color="redBright">{failed} modules failed</Text>}
			<Text color="white">.</Text>
		</Text>
	);
};
