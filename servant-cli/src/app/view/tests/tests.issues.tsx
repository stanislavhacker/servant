import * as api from "@servant/servant";
import * as React from "react";
import { Text, Box } from "ink";

import { FIRST_COLUMN, SECOND_COLUMN } from "../../constants";
import { TestsSummaryView } from "./tests";

export interface TestsBugReportProps<C extends api.Commands.Commands> {
	modules: { [key: string]: api.Commands.CommandProgress[C] };
}

export function TestsIssuesReport<C extends api.Commands.Commands>(props: TestsBugReportProps<C>) {
	const bugs = mergeBugs(props.modules);
	const ids = Object.keys(bugs);

	if (ids.length === 0) {
		return null;
	}

	return (
		<Box flexDirection="column" marginTop={1}>
			<Box flexDirection="row">
				<Box width={FIRST_COLUMN}>
					<Text bold color="green">
						Fix Report
					</Text>
				</Box>
				<Box width={SECOND_COLUMN}>
					<Text bold color="magenta">
						{ids.length} fixed issues found
					</Text>
				</Box>
			</Box>
			<Box flexDirection="column" marginBottom={1} marginLeft={2}>
				{ids.map((id, index) => (
					<Box flexDirection="row" key={index}>
						<Box width={FIRST_COLUMN}>
							<Text bold>
								<Text color="blueBright">{id}</Text>{" "}
							</Text>
						</Box>
						<TestsSummaryView
							ok={bugs[id].passed}
							pending={bugs[id].pending}
							failed={bugs[id].failed}
							excluded={bugs[id].excluded}
						/>
						<Box>
							<Text color="gray">{bugs[id].url}</Text>
						</Box>
					</Box>
				))}
			</Box>
		</Box>
	);
}

export function mergeBugs<C extends api.Commands.Commands>(modules: {
	[key: string]: api.Commands.CommandProgress[C];
}): api.Module.Issues {
	let bugs: api.Module.Issues = {};

	Object.keys(modules).forEach((module) => {
		const results = modules[module].data as api.Module.TestingResults;
		bugs = api.Module.mergeIssues(bugs, results.issues);
	});

	return bugs;
}
