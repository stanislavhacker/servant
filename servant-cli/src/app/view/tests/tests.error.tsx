import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { Error } from "../error";

export interface TestsErrorProps<C extends api.Commands.Commands> {
	result: api.Commands.CommandProgress[C];
	testsResult: api.Module.TestingResults;
	debug: boolean;
}

export function TestsError<C extends api.Commands.Commands>(props: TestsErrorProps<C>) {
	const { testsResult, result, debug } = props;

	return (
		<Box flexDirection="column" marginLeft={1} marginTop={1} marginBottom={1}>
			<Box>
				<Text bold underline>
					{result.module}
				</Text>
			</Box>
			{testsResult.error && <Error error={testsResult.error} debug={debug} />}
			{testsResult.failed.map((failed, index) => (
				<Box flexDirection="column" marginLeft={1} marginTop={1} key={index}>
					<ItemError item={failed} debug={debug} />
				</Box>
			))}
		</Box>
	);
}

interface ItemErrorProps {
	item: api.Module.Item;
	debug: boolean;
}

const ItemError: React.StatelessComponent<ItemErrorProps> = (props) => {
	const { item } = props;

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<Text bold>
					<Text color="red">
						{"✘"} {item.fullName}
					</Text>{" "}
				</Text>
			</Box>
			{item.failed.map((failed, index) => (
				<Box flexDirection="column" marginLeft={2} key={index}>
					<Text>{failed.message}</Text>
					<Box marginLeft={2}>
						<Text color="gray">{failed.stack}</Text>
					</Box>
				</Box>
			))}
		</Box>
	);
};
