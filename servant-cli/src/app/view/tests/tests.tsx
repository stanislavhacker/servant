import * as api from "@servant/servant";
import * as React from "react";
import { Text, Box } from "ink";

import { Result } from "../result";

const moduleWidth = 45;
const countWidth = 6;
const indexWidth = 5;

export interface TestsProps {
	result: api.Commands.CommandProgress["tests"] | undefined;
	module: string;
	index: number;
}

export const Tests: React.StatelessComponent<TestsProps> = (props) => {
	const { result, module, index } = props;
	const testsResult = result && result.data;

	return (
		<Box flexDirection="row">
			{!result && (
				<Box flexDirection="row">
					<Box width={moduleWidth + indexWidth}>
						<Text>
							<Text bold color="blueBright">
								{index}.
							</Text>{" "}
							<Text color="gray">{module}</Text>{" "}
						</Text>
					</Box>
				</Box>
			)}
			{result && testsResult && (
				<Box flexDirection="row">
					<Box width={moduleWidth + indexWidth}>
						<Result module={module} result={result} index={index} />
					</Box>
					<TestsSummaryView
						ok={testsResult.summary.ok}
						failed={testsResult.summary.failed}
						excluded={testsResult.summary.excluded}
						pending={testsResult.summary.pending}
					/>
					<Box>
						<TestsResultsView
							error={testsResult.error}
							count={testsResult.summary.count}
							skipped={testsResult.summary.pending + testsResult.summary.excluded}
							completed={testsResult.summary.completed}
							failed={testsResult.summary.failed}
						/>
					</Box>
					{testsResult.playwright && (
						<Box>
							<TestsPlaywrightView
								browsers={testsResult.browsers}
								invalidDevices={testsResult.devices.invalid}
								usedDevices={testsResult.devices.used}
							/>
						</Box>
					)}
				</Box>
			)}
		</Box>
	);
};

interface TestsResultsProp {
	error: Error | null;
	completed: number;
	count: number;
	failed: number;
	skipped: number;
}

const TestsResultsView: React.StatelessComponent<TestsResultsProp> = (props) => {
	const { count, completed, failed, skipped, error } = props;
	const fail = failed > 0 || error;

	return (
		<>
			{count !== completed && <Text bold>[RUNS]</Text>}
			{count === completed && !fail && skipped === 0 && (
				<Text bold backgroundColor="green">
					[PASS]
				</Text>
			)}
			{count === completed && !fail && skipped > 0 && (
				<Text bold backgroundColor="yellowBright">
					[PASS]
				</Text>
			)}
			{count === completed && fail && (
				<Text bold backgroundColor="red">
					[FAIL]
				</Text>
			)}
		</>
	);
};

interface TestsSummaryProp {
	ok: number;
	failed: number;
	excluded: number;
	pending: number;
}

export const TestsSummaryView: React.StatelessComponent<TestsSummaryProp> = (props) => {
	const { failed, pending, excluded, ok } = props;

	return (
		<>
			<Box width={countWidth}>
				<Text bold color="greenBright">
					{ok}✔
				</Text>
			</Box>
			<Box width={countWidth}>
				{failed > 0 && (
					<Text bold color="redBright">
						{" "}
						{failed}✘
					</Text>
				)}
			</Box>
			<Box width={countWidth}>
				{excluded > 0 && (
					<Text bold color="gray">
						{" "}
						{excluded}✖
					</Text>
				)}
			</Box>
			<Box width={countWidth}>
				{pending > 0 && (
					<Text bold color="gray">
						{" "}
						{pending}⌛
					</Text>
				)}
			</Box>
		</>
	);
};

interface TestsPlaywrightProp {
	browsers: Array<string>;
	usedDevices: Array<string>;
	invalidDevices: Array<string>;
}

export const TestsPlaywrightView: React.StatelessComponent<TestsPlaywrightProp> = (props) => {
	const { browsers, invalidDevices, usedDevices } = props;
	const devicesUsed = usedDevices.length || invalidDevices.length;

	return (
		<>
			<Text color="gray"> in </Text>
			<Text bold>{browsers.join(", ")}</Text>
			{devicesUsed > 0 && (
				<>
					<Text bold> × </Text>
					{usedDevices.length <= 3 &&
						usedDevices.map((device) => {
							return (
								<Text key={device}>
									<Text underline color="greenBright">
										{device}
									</Text>{" "}
								</Text>
							);
						})}
					{usedDevices.length > 3 && (
						<Text>
							<Text underline color="greenBright">
								{usedDevices.length} devices
							</Text>{" "}
						</Text>
					)}
					{invalidDevices.length <= 3 &&
						invalidDevices.map((device) => {
							return (
								<Text key={device}>
									<Text strikethrough color="redBright">
										{device}
									</Text>{" "}
								</Text>
							);
						})}
					{invalidDevices.length > 3 && (
						<Text>
							<Text strikethrough color="redBright">
								{invalidDevices.length} unknown
							</Text>{" "}
						</Text>
					)}
				</>
			)}
		</>
	);
};
