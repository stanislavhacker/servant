import { Shared } from "./shared";
import { SharedError } from "./shared.error";
import { SharedOk } from "./shared.ok";

export { Shared, SharedOk, SharedError };
