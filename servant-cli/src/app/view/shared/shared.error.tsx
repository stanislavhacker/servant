import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { Error } from "../error";

export interface SharedErrorProps {
	sharedResult: api.Module.SharedResult;
	debug: boolean;
}

export const SharedError: React.StatelessComponent<SharedErrorProps> = (props) => {
	const { sharedResult, debug } = props;

	if (!sharedResult) {
		return null;
	}

	return (
		<Box flexDirection="column" marginTop={1} marginLeft={1}>
			<Box>
				<Text bold underline>
					{sharedResult.module}
				</Text>
			</Box>
			{sharedResult.missing.length > 0 && (
				<Box flexDirection="row" marginLeft={1} marginBottom={1}>
					<Text color="yellowBright" bold>
						Missing modules in package:{" "}
					</Text>
					{sharedResult.missing.map((module, index) => (
						<Text key={index}>
							<Text key={module} color="whiteBright" bold>
								{module}
							</Text>
							<Text color="gray">, </Text>
						</Text>
					))}
				</Box>
			)}
			{sharedResult.errors.map((error, index) => (
				<Error
					error={error}
					debug={debug}
					key={index}
					marginBottom={1}
					marginTop={index === 0 ? 1 : 0}
				/>
			))}
		</Box>
	);
};
