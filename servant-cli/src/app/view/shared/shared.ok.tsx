import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { INDEX_COLUMN, MODULE_COLUMN } from "../../constants";

export interface SharedOkProps {
	sharedResult: api.Module.SharedResult;
	debug: boolean;
}

export const SharedOk: React.StatelessComponent<SharedOkProps> = (props) => {
	const { sharedResult } = props;
	const shared = sharedResult ? Object.keys(sharedResult.shared) : [];

	if (!sharedResult || shared.length === 0) {
		return null;
	}

	return (
		<Box flexDirection="column" marginLeft={1} marginTop={1} marginBottom={1}>
			<Box>
				<Text bold underline>
					{sharedResult.module}
				</Text>
			</Box>
			<Box flexDirection="column" marginLeft={1}>
				<Text color="whiteBright">Shared modules in package:</Text>
				{shared.map((module, index) => (
					<Box key={module} flexDirection="row" marginLeft={1}>
						<Box minWidth={INDEX_COLUMN}>
							<Text color="whiteBright">{index + 1}.</Text>
						</Box>
						<Box minWidth={MODULE_COLUMN}>
							<Text color="greenBright">{module}</Text>
						</Box>
						<Box>
							<SharedLinkInfo
								name={module}
								on={sharedResult.shared[module]}
								exists={sharedResult.exists}
							/>
						</Box>
					</Box>
				))}
				{sharedResult.mode === "link" && sharedResult.links && (
					<Text>
						Mode{" "}
						<Text color="magentaBright" bold>
							link
						</Text>{" "}
						created new <Text bold>{sharedResult.links.length} links</Text> on modules.
					</Text>
				)}
				{sharedResult.mode === "unlink" && sharedResult.unlinks && (
					<Text>
						Mode{" "}
						<Text color="magentaBright" bold>
							unlink
						</Text>{" "}
						removed <Text bold>{sharedResult.unlinks.length} links</Text> on modules.
					</Text>
				)}
			</Box>
		</Box>
	);
};

export interface SharedLinkInfoProps {
	name: string;
	on: string;
	exists: api.Module.SharedResult["exists"];
}

const SharedLinkInfo: React.StatelessComponent<SharedLinkInfoProps> = (props) => {
	const { name, on, exists } = props;

	const module = exists[name];

	//no link
	if (!module) {
		return (
			<Text color="gray" italic>
				no link created, run "<Text bold>servant install</Text>"
			</Text>
		);
	}

	//link
	return (
		<Text>
			<Text color="greenBright" bold>
				↘
			</Text>
			<Text> </Text>
			<Text color="greenBright" bold>
				shared
			</Text>
			<Text color="whiteBright" italic>
				{" "}
				from "<Text color="blueBright">{on}</Text>"
			</Text>
		</Text>
	);
};
