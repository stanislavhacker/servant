import * as api from "@servant/servant";
import * as React from "react";
import { Text, Box } from "ink";

import { Result } from "../result";

const moduleWidth = 70;

export interface SharedProps {
	result: api.Commands.CommandProgress["shared"] | undefined;
	module: string;
	index: number;
}

export const Shared: React.StatelessComponent<SharedProps> = (props) => {
	const { result, module, index } = props;
	const sharedResult = result && result.data;
	const shared = sharedResult ? Object.keys(sharedResult.shared) : [];

	return (
		<Box flexDirection="row">
			{!result && (
				<Box flexDirection="row">
					<Box width={moduleWidth}>
						<Text>
							<Text bold>
								<Text color="blueBright">{index}.</Text>
							</Text>{" "}
							<Text color="gray">{module}</Text>{" "}
						</Text>
					</Box>
				</Box>
			)}
			{result && (
				<Box flexDirection="row">
					<Box width={moduleWidth}>
						<Result module={module} result={result} index={index} />
					</Box>
					<Box>
						{sharedResult && shared.length === 0 && (
							<Text bold color="gray">
								no shared modules found
							</Text>
						)}
						{sharedResult && shared.length > 0 && (
							<Text bold>
								<Text color="greenBright">shared:</Text> "{shared.join(", ")}"
							</Text>
						)}
						{sharedResult && sharedResult.missing.length > 0 && (
							<Text>
								<Text>, </Text>
								<Text bold color="yellowBright">
									missing {sharedResult.missing.length} packages
								</Text>
							</Text>
						)}
						{sharedResult && sharedResult.errors.length > 0 && (
							<Text>
								<Text>, </Text>
								<Text bold color="redBright">
									{sharedResult.errors.length} errors
								</Text>
							</Text>
						)}
					</Box>
				</Box>
			)}
		</Box>
	);
};
