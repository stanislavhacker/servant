import * as api from "@servant/servant";
import * as React from "react";
import { Text } from "ink";

import { Result } from "../result";

export interface UpdateProps {
	result: api.Commands.CommandProgress["update"] | undefined;
	module: string;
	index: number;
}

export const Update: React.StatelessComponent<UpdateProps> = (props) => {
	const { result, module, index } = props;

	return (
		<Text>
			{!result && (
				<Text>
					<Text bold color="blueBright">
						{index}.
					</Text>{" "}
					<Text color="gray">{module}</Text>{" "}
				</Text>
			)}
			{result && <Result module={module} result={result} index={index} />}
		</Text>
	);
};
