import * as React from "react";
import { Box, Text } from "ink";

export interface ErrorProps {
	error: Error;
	debug: boolean;
	marginTop?: number;
	marginBottom?: number;
}

export const Error: React.StatelessComponent<ErrorProps> = (props) => {
	const { error, debug, marginTop = 1, marginBottom = 1 } = props;
	const errorMargin = 10;

	return (
		<Box
			flexDirection="column"
			marginLeft={2}
			marginTop={marginTop}
			marginBottom={marginBottom}
		>
			<Box>
				<Text>
					<Text color="redBright">{"[ERROR]"}:</Text>{" "}
					<Text color="white">{error.message}</Text>
				</Text>
			</Box>
			{debug && (
				<Box flexDirection="column" marginLeft={errorMargin}>
					<Box marginTop={1}>
						<Text bold underline color="yellow">
							Stack trace:
						</Text>
					</Box>
					{error.stack}
				</Box>
			)}
		</Box>
	);
};
