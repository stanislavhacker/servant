import * as api from "@servant/servant";
import * as React from "react";
import { Text } from "ink";

export interface UnifyProps {
	result: api.Commands.CommandProgress["unify"] | undefined;
	module: string;
	index: number;
}

export const Unify: React.StatelessComponent<UnifyProps> = (props) => {
	const { result, module, index } = props;
	const unifyResult = result && result.data;

	return <UnifyResult index={index} unify={unifyResult} result={result} module={module} />;
};

interface UnifyResultProps {
	index: number;
	module: string;
	result: api.Module.CommandResult<api.Module.UnifyResult> | undefined | null;
	unify: api.Module.UnifyResult | null | undefined;
}

const UnifyResult: React.StatelessComponent<UnifyResultProps> = (props) => {
	const { index, unify, result, module } = props;

	return (
		<Text>
			<Text bold color="blueBright">
				{index}.
			</Text>
			<Text> </Text>
			{!result && <Text color="gray">{module}</Text>}
			{unify && unify.type === api.Module.DoneType.OK && (
				<Text strikethrough color="green">
					{module}
				</Text>
			)}
			{unify && unify.type === api.Module.DoneType.FAIL && (
				<Text strikethrough color="redBright">
					{module}
				</Text>
			)}
			{unify && unify.type === api.Module.DoneType.WARNING && (
				<Text strikethrough color="yellowBright">
					{module}
				</Text>
			)}
			<Text> </Text>
			{unify && <UnifyNumber unified={unify.unified} />}
			{!unify && <UnifyNumber unified={[]} />}
			<Text> </Text>
		</Text>
	);
};

interface UnifyNumberProps {
	unified: api.Module.UnifyPackageResult[];
}

const UnifyNumber: React.StatelessComponent<UnifyNumberProps> = ({ unified }) => {
	const { ok, warning, fail } = unifyCounts(unified);
	const pad = 2;
	const items: React.ReactElement[] = [];

	if (unified.length === 0) {
		items.push(<Text color="gray">{padLeft(0, pad)}x</Text>);
	}

	if (ok > 0) {
		items.push(<Text color="greenBright">{padLeft(ok, pad)}x</Text>);
	}
	if (warning > 0) {
		items.push(<Text color="yellowBright">{padLeft(warning, pad)}x</Text>);
	}
	if (fail > 0) {
		items.push(<Text color="redBright">{padLeft(fail, pad)}x</Text>);
	}

	return (
		<Text>
			{items.reduce((prev, item, index, arr) => {
				return arr.length - 1 !== index ? [item, <Text>, </Text>] : item;
			}, [] as React.ReactElement[])}
		</Text>
	);
};

function unifyCounts(unified: api.Module.UnifyPackageResult[]) {
	return unified.reduce(
		(prev, item) => {
			switch (item.type) {
				case api.Module.DoneType.FAIL:
					prev.fail++;
					return prev;
				case api.Module.DoneType.WARNING:
					prev.warning++;
					return prev;
				default:
					prev.ok++;
					return prev;
			}
		},
		{ ok: 0, fail: 0, warning: 0 }
	);
}

function padLeft(data: string | number, size: number): string {
	return (new Array(size + 1).join(" ") + String(data)).slice(-size);
}
