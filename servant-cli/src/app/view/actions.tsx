import * as React from "react";

import { Unify } from "../unify";
import { Init, InitInstall } from "../init";
import { Generate } from "../generate";
import { ServantState } from "../index";

export type ActionsProps = ServantState & {
	setState: (state: Partial<ServantState>, callback?: () => void) => void;
	onDone?: () => void;
};

export const UnifyAction: React.StatelessComponent<ActionsProps> = (props) => {
	const { flags, loaded, unify, m, date, setState, onDone } = props;
	const { waiter } = unify;

	//no unify
	if (!waiter) {
		return null;
	}

	return (
		<Unify
			debug={flags.debug}
			latest={flags.latest}
			modules={m}
			date={date}
			loaded={loaded}
			{...unify}
			setState={(updatedUnify) => {
				waiter.resolve();
				setState({
					unify: {
						...unify,
						...updatedUnify,
					},
				});
			}}
			onDone={(saveResult) => {
				setState({
					unify: {
						...unify,
						saveResult,
					},
				});
				onDone && onDone();
			}}
		/>
	);
};

export const InitAction: React.StatelessComponent<ActionsProps> = (props) => {
	const { flags, loaded, init, date, setState, onDone } = props;

	return (
		<>
			<Init
				debug={flags.debug}
				date={date}
				loaded={loaded}
				{...init}
				setState={(updatedInit) => {
					setState({
						init: {
							...init,
							...updatedInit,
						},
					});
				}}
				onDone={(saveResult) => {
					setState({
						init: {
							...init,
							saveResult,
						},
						commands: ["install"],
					});
					onDone && onDone();
				}}
			/>
			<InitInstall {...props} />
		</>
	);
};

export const GenerateAction: React.StatelessComponent<ActionsProps> = (props) => {
	const { flags, modules, generate, servant, loaded, date, setState, onDone } = props;
	const { modulesData } = modules;

	return (
		<Generate
			date={date}
			loaded={loaded}
			debug={flags.debug}
			modulesData={modulesData}
			initData={servant.initData}
			{...generate}
			setState={setState}
			onDone={() => {
				onDone && onDone();
			}}
		/>
	);
};
