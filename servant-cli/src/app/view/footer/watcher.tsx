import { Module, Watcher as ServantWatcher } from "@servant/servant";
import * as path from "path";
import * as React from "react";
import { Box, Text } from "ink";

import { FIRST_COLUMN } from "../../constants";
import { ServantState } from "../../index";
import { Error as ErrorComponent } from "../error";

const longColumn = 80;

export const Watcher: React.StatelessComponent<ServantState> = (props) => {
	return (
		<Box flexDirection="column" marginTop={1}>
			<Box flexDirection="column">
				<WatcherHeader {...props} />
				<WatcherDiscovery {...props} />
				<WatcherResults {...props} />
				<WatcherError {...props} />
			</Box>
		</Box>
	);
};

const WatcherHeader: React.StatelessComponent<ServantState> = (props) => {
	const { watcher, servant, modules } = props;
	const { info } = watcher;

	const ready = Boolean(info && info.ready);
	const failed = Boolean(servant.error || modules.error);

	return (
		<Box flexDirection="row">
			<Box width={FIRST_COLUMN}>
				{!ready && !failed && (
					<Text bold color="gray">
						Watcher
					</Text>
				)}
				{failed && (
					<Text bold color="red">
						Watcher
					</Text>
				)}
				{ready && (
					<Text bold color="green">
						Watcher
					</Text>
				)}
			</Box>
			<Box>
				{!ready && !failed && <Text color="gray">Waiting for start ...</Text>}
				{failed && <Text color="redBright">Not started due to error</Text>}
				{ready && info && (
					<Box>
						<Text italic color="white">
							Started at {info.date.toLocaleTimeString()}
						</Text>
						{info.time !== null && (
							<Text color="gray">
								, load time <Text bold>{info.time}</Text>
							</Text>
						)}
					</Box>
				)}
			</Box>
		</Box>
	);
};

const WatcherResults: React.StatelessComponent<ServantState> = (props) => {
	const { watcher } = props;
	const { change, error } = watcher;

	//no changes or fatal error
	if (!change || error) {
		return null;
	}

	return (
		<Box marginLeft={2} flexDirection="column">
			{change.progress && (
				<Text bold backgroundColor="green" color="whiteBright">
					{" "}
					Rebuilding ...{" "}
				</Text>
			)}
			{!change.progress && change.files.length > 0 && (
				<Box flexDirection="column" marginTop={1}>
					<Box flexDirection="row">
						<Text>
							Rebuild is <Text bold>done</Text> in{" "}
							<Text italic color="blueBright">
								{change.time}
							</Text>
							.{" "}
						</Text>
						<Text>
							Changed{" "}
							<Text bold color="green">
								{change.files.length}
							</Text>{" "}
							files in{" "}
							<Text bold color="magenta">
								{change.modules.length}
							</Text>{" "}
							modules.{" "}
						</Text>
					</Box>
					<Box flexDirection="row">
						<Text>
							<Text bold>Changed</Text> <Text color="cyanBright">modules</Text>:{" "}
						</Text>
						<Text>
							{change.modules.map((module, i) => (
								<Text key={i}>
									{i > 0 ? <Text>, </Text> : null}
									<Text color="gray">{i + 1}.</Text>{" "}
									<Text color="whiteBright">{module}</Text>
								</Text>
							))}
						</Text>
					</Box>
					<Box flexDirection="row">
						<Text>
							<Text bold>Changed</Text> <Text color="greenBright">files</Text>:{" "}
						</Text>
						<Text>
							{change.files.map(([stringPath, status], i) => (
								<Text key={i}>
									{i > 0 ? <Text>, </Text> : null}
									<Text color={getColor(status)}>
										{path.basename(stringPath)}
									</Text>
								</Text>
							))}
						</Text>
					</Box>
				</Box>
			)}
			{!change.progress && (
				<Text bold color="whiteBright" backgroundColor="gray">
					{" "}
					Waiting ...{" "}
				</Text>
			)}
		</Box>
	);
};

const WatcherError: React.StatelessComponent<ServantState> = (props) => {
	const { watcher, flags } = props;
	const { info, error } = watcher;

	const recoverableError = info && info.error;
	const fatalError = error;

	//no error
	if (!recoverableError || !fatalError) {
		return null;
	}

	return (
		<Box flexDirection="column">
			{recoverableError && (
				<Box flexDirection="column" marginTop={1} marginLeft={2}>
					<Text>
						There is error occurred during watching files. This error can be recoverable
						and Servant can continue to watching files and building.
					</Text>
					<Text color="yellowBright" bold>
						If not, restart Servant!
					</Text>
				</Box>
			)}
			<ErrorComponent error={recoverableError || fatalError} debug={flags.debug} />
		</Box>
	);
};

const WatcherDiscovery: React.StatelessComponent<ServantState> = (props) => {
	const { watcher, flags } = props;
	const { info } = watcher;

	//no discovery
	if (!info || !info.discovery) {
		return null;
	}

	const { instance, running } = info.discovery;

	return (
		<Box marginLeft={2} flexDirection="column">
			<Box flexDirection="row">
				<Text bold color="blueBright" backgroundColor="gray">
					{" "}
					Discovering ...{" "}
				</Text>
				{instance && (
					<Text color="gray">
						{" "}
						Port{" "}
						<Text bold color="white">
							{instance.port}
						</Text>
					</Text>
				)}
				{running.length > 0 && (
					<Text color="gray">
						{" "}
						with connected{" "}
						<Text bold color="magenta">
							{running.length} clients
						</Text>
					</Text>
				)}
			</Box>
			{running.length > 0 && (
				<Box flexDirection="column" marginLeft={1}>
					{running.map(({ job, err, port, modules, entry, results }, i) => {
						const prevModules = modules.slice(0, 3);

						return (
							<Box flexDirection="column" key={i}>
								<Box flexDirection="row">
									<Box flexDirection="row" width={longColumn}>
										<Box>
											<Text bold color="blueBright">
												{i + 1}.{" "}
											</Text>
										</Box>
										<Box>
											<Text>
												<Text color="white">
													{smartTrim(entry, "...", longColumn - 20)}
												</Text>{" "}
												<Text bold color="gray">
													on port {port}
												</Text>
											</Text>
										</Box>
									</Box>
									<Box>
										<JobStatus job={job} />
									</Box>
								</Box>
								<Box flexDirection="row" marginLeft={3}>
									<Text color="green" bold>
										Modules:{" "}
									</Text>
									{prevModules.map((module, i) => (
										<Text key={i}>
											<Text color="white">{module}</Text>
											{i !== prevModules.length - 1 && <Text>, </Text>}
										</Text>
									))}
									{prevModules.length < modules.length && (
										<Text color="gray">
											{" "}
											+ {modules.length - prevModules.length} modules
										</Text>
									)}
								</Box>
								{results.length > 0 && (
									<Box flexDirection="column" marginLeft={3}>
										<Text color="blue" bold>
											Symbolic links
										</Text>
										<Box flexDirection="column" marginLeft={2}>
											{results.map(({ result, error }, i) => (
												<Box key={i} flexDirection="row">
													{result && (
														<Text>
															<Text bold color="blueBright">
																{i + 1}.{" "}
															</Text>
															<Text color="white" bold>
																{result.on}
															</Text>
															<Text color="gray"> → </Text>
															<Text color="white" bold>
																{result.to}
															</Text>
														</Text>
													)}
													{error && (
														<LinkError
															error={error}
															debug={flags.debug}
														/>
													)}
												</Box>
											))}
										</Box>
									</Box>
								)}
								{err && <ErrorComponent error={err} debug={flags.debug} />}
							</Box>
						);
					})}
				</Box>
			)}
		</Box>
	);
};

const JobStatus: React.StatelessComponent<{ job: ServantWatcher.BuildJob | null }> = ({ job }) => {
	return (
		<>
			{job && (
				<>
					{!job.done && (
						<Text bold backgroundColor="green" color="whiteBright">
							{" "}
							Rebuilding ...{" "}
						</Text>
					)}
					{job.done && job.type === Module.DoneType.OK && (
						<Text bold backgroundColor="green" color="whiteBright">
							{" "}
							BUILD{" "}
						</Text>
					)}
					{job.done && job.type === Module.DoneType.WARNING && (
						<Text bold backgroundColor="yellow" color="whiteBright">
							{" "}
							BUILD{" "}
						</Text>
					)}
					{job.done && job.type === Module.DoneType.FAIL && (
						<Text bold backgroundColor="red" color="whiteBright">
							{" "}
							FAIL{" "}
						</Text>
					)}
				</>
			)}
			{!job && (
				<Text italic color="gray">
					{" "}
					waiting{" "}
				</Text>
			)}
		</>
	);
};

const LinkError: React.StatelessComponent<{
	error: ServantWatcher.WatcherDiscovery["running"][number]["results"][number]["error"];
	debug: boolean;
}> = ({ error, debug }) => {
	if (!error) {
		return null;
	}
	if (error.original && error.original["code"] === "EPERM") {
		return (
			<Box flexDirection="column" marginLeft={2} marginTop={1} marginBottom={1}>
				<Box>
					<Text>
						<Text color="redBright">{"[ERROR]"}:</Text>{" "}
						<Text color="white">
							It's look that servant has no permissions to create symbolic links. Try
							to run servant in elevated rights.
						</Text>
					</Text>
				</Box>
			</Box>
		);
	}
	return (
		<Box flexDirection="column">
			<ErrorComponent error={error.original || error} debug={debug} />
		</Box>
	);
};

function getColor(status: -1 | 0 | 1): string {
	if (status === -1) {
		return "red";
	}
	if (status === 1) {
		return "green";
	}
	return "blue";
}

function smartTrim(string: string, ellipses: string, max: number) {
	const maxLength = Math.max(2, max - ellipses.length);

	//string is ok
	if (!string || string.length <= maxLength) {
		return string;
	}

	const midpoint = Math.ceil(string.length / 2);
	const toRemove = string.length - maxLength;
	const left = Math.ceil(toRemove / 2);
	const right = toRemove - left;

	return string.substring(0, midpoint - left) + ellipses + string.substring(midpoint + right);
}
