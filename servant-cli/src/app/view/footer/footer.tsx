import * as React from "react";
import { Box } from "ink";

import { ServantState } from "../../index";

import { Watcher } from "./watcher";
import { Reports } from "./reports";

export const Footer: React.StatelessComponent<ServantState> = (props) => {
	const { reports, flags } = props;

	return (
		<Box flexDirection="column">
			{reports.list.length > 0 && <Reports {...props} />}
			{flags.watch && <Watcher {...props} />}
		</Box>
	);
};
