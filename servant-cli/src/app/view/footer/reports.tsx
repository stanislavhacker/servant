import * as React from "react";
import { Box, Text } from "ink";

import { FIRST_COLUMN } from "../../constants";
import { ServantState } from "../../index";
import { Error } from "../error";

export const Reports: React.StatelessComponent<ServantState> = (props) => {
	const { reports, flags } = props;

	return (
		<Box flexDirection="column">
			{reports.files.length > 0 && (
				<Box flexDirection="column">
					<Box flexDirection="row">
						<Box width={FIRST_COLUMN}>
							<Text bold color="green">
								Reports
							</Text>
						</Box>
						<Box>
							<Text underline color="cyan">
								{reports.files.length} created
							</Text>
						</Box>
					</Box>
					<Box marginTop={1} marginLeft={1} flexDirection="column">
						{reports.files.map((path, index) => (
							<Box flexDirection="row" key={index}>
								<Text>
									<Text color="gray">+</Text> <Text underline>{path}</Text>
								</Text>
							</Box>
						))}
					</Box>
				</Box>
			)}
			{reports.error && <Error error={reports.error} debug={flags.debug} />}
		</Box>
	);
};
