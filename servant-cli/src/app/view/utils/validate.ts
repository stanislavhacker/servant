import * as api from "@servant/servant";

//merge

export function mergeValidationResults(
	results: Array<api.Module.ValidationResult | undefined | null>
): api.Module.ValidationResult {
	const items = results.filter(Boolean) as Array<api.Module.ValidationResult>;
	const merged: api.Module.ValidationResult = {
		results: {
			files: {},
			errors: [],
		},
		fix: false,
		type: api.Module.ValidationDoneType.OK,
		available: false,
	};

	items.forEach((result) => {
		//results
		Object.values(result.results.files).forEach((item) => {
			const all = merged.results.files[item.file];
			if (all) {
				all.fixed = all.fixed || item.fixed;
				all.messages = [...all.messages, ...item.messages];
			} else {
				merged.results.files[item.file] = {
					fixed: item.fixed,
					file: item.file,
					messages: [...item.messages],
				};
			}
		});
		//errors
		merged.results.errors.push(...result.results.errors);
		//props
		merged.fix = merged.fix || result.fix;
		merged.available = merged.available || result.available;
		merged.type = mergeDoneType(merged.type, result.type);
	});

	return merged;
}

const priority = [
	api.Module.ValidationDoneType.FAIL,
	api.Module.ValidationDoneType.ERROR,
	api.Module.ValidationDoneType.WARNING,
	api.Module.ValidationDoneType.OK,
];

function mergeDoneType(type1: api.Module.ValidationDoneType, type2: api.Module.ValidationDoneType) {
	const p1 = priority.indexOf(type1);
	const p2 = priority.indexOf(type2);

	if (p1 < p2) {
		return type1;
	}
	return type2;
}

//collecting

export function collectMissingPackages(results?: api.Module.ValidateResult) {
	return [
		...(results?.eslint?.missingPlugins ?? []),
		...(results?.prettier?.missingPlugins ?? []),
	];
}

//get status

export function getValidationResultsStatus(results: api.Module.ValidationResult) {
	const data = Object.values(results.results.files).reduce(
		(prev, result) => {
			const { infos, errors, warnings, suppressed, fixable } = getValidationResultStatus(
				result.messages
			);
			prev.errors.push(...errors);
			prev.warnings.push(...warnings);
			prev.infos.push(...infos);
			prev.suppressed.push(...suppressed);
			prev.fixable.push(...fixable);
			return prev;
		},
		{
			errors: [] as api.Module.ValidationUniversalMessage[],
			warnings: [] as api.Module.ValidationUniversalMessage[],
			infos: [] as api.Module.ValidationUniversalMessage[],
			suppressed: [] as api.Module.ValidationUniversalMessage[],
			fixable: [] as api.Module.ValidationUniversalMessage[],
		}
	);

	const { infos, errors, warnings, suppressed, fixable } = getValidationResultStatus(
		results.results.errors
	);
	data.errors.push(...errors);
	data.warnings.push(...warnings);
	data.infos.push(...infos);
	data.suppressed.push(...suppressed);
	data.fixable.push(...fixable);

	return data;
}

function getValidationResultStatus(messages: api.Module.ValidationUniversalMessage[]) {
	return messages.reduce(
		(prev, msg) => {
			if (msg.fixable) {
				prev.fixable.push(msg);
			}

			if (msg.severity === api.Module.ValidationUniversalSeverity.ERROR) {
				prev.errors.push(msg);
				return prev;
			}
			if (msg.severity === api.Module.ValidationUniversalSeverity.WARNING) {
				prev.warnings.push(msg);
				return prev;
			}
			if (msg.severity === api.Module.ValidationUniversalSeverity.INFO) {
				prev.infos.push(msg);
				return prev;
			}
			prev.suppressed.push(msg);
			return prev;
		},
		{
			errors: [] as api.Module.ValidationUniversalMessage[],
			warnings: [] as api.Module.ValidationUniversalMessage[],
			infos: [] as api.Module.ValidationUniversalMessage[],
			suppressed: [] as api.Module.ValidationUniversalMessage[],
			fixable: [] as api.Module.ValidationUniversalMessage[],
		}
	);
}

//grouping

export type GroupedResults = {
	groups: {
		[key: string]: {
			[key: string]: {
				[key: string]: GroupedResult[];
			};
		};
	};
	fixedFiles: Array<string>;
};

export type GroupedResult = {
	//engine
	engine: api.Module.ValidationEngine;
	//message
	id: string;
	messageId: string;
	message: string;
	//file info
	severity: api.Module.ValidationUniversalSeverity;
	type: string;
	fixable: boolean;
	file: string | null;
	pointers: Array<string>;
};

export function groupValidationResults(validateResult: api.Module.ValidateResult): GroupedResults {
	const groups = {} as GroupedResults["groups"];
	const fixedFiles = {} as Record<string, true>;

	const results = [
		...(validateResult.eslint.available
			? Object.values(validateResult.eslint.results.files ?? {})
			: []),
		...(validateResult.prettier.available
			? Object.values(validateResult.prettier.results.files ?? {})
			: []),
		...(validateResult.trancorder.available
			? Object.values(validateResult.trancorder.results.files ?? {})
			: []),
	];

	const errors = [
		...(validateResult.eslint.available ? validateResult.eslint.results.errors ?? [] : []),
		...(validateResult.prettier.available ? validateResult.prettier.results.errors ?? [] : []),
		...(validateResult.trancorder.available
			? validateResult.trancorder.results.errors ?? []
			: []),
	];

	results.forEach((result) => {
		result.messages.forEach((msg) => {
			groupValidationResult(result, msg, groups);
		});

		if (result.fixed) {
			fixedFiles[api.Files.Path.normalize(result.file)] = true;
		}
	});

	errors.forEach((err) => {
		groupValidationResult(null, err, groups);
	});

	return {
		groups,
		fixedFiles: Object.keys(fixedFiles),
	};
}

function groupValidationResult(
	result: api.Module.ValidationUniversalResult | null,
	msg: api.Module.ValidationUniversalMessage,
	groups: GroupedResults["groups"]
) {
	const group = groups[msg.engine] || (groups[msg.engine] = {});
	const messages = group[msg.ruleId] || (group[msg.ruleId] = {});
	const items = messages[msg.message] || (messages[msg.message] = []);

	const record = items.find(
		(item) => result && item.file === api.Files.Path.normalize(result.file)
	);
	const pointer = getFilePointer(msg);
	if (record) {
		record.pointers.push(...(pointer ? [pointer] : []));
	} else {
		items.push({
			engine: msg.engine,
			id: msg.ruleId,
			message: msg.message,
			messageId: msg.messageId,
			severity: msg.severity,
			type: msg.nodeType,
			fixable: msg.fixable,
			file: result ? api.Files.Path.normalize(result.file) : null,
			pointers: pointer ? [pointer] : [],
		});
	}
}

function getFilePointer(msg: api.Module.ValidationUniversalMessage) {
	if (msg.line !== undefined && msg.column !== undefined) {
		return `${msg.line}:${msg.column}`;
	}
	return null;
}
