import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { Error } from "../error";

export interface BuildErrorProps {
	buildResult: api.Module.BuildResult;
	debug: boolean;
}

export const BuildError: React.StatelessComponent<BuildErrorProps> = (props) => {
	const { buildResult, debug } = props;

	return (
		<Box flexDirection="column" marginLeft={2} marginTop={1} marginBottom={1}>
			<Box flexDirection="column">
				{buildResult.less &&
					buildResult.less.errors.map((lessResult, index) => (
						<Box flexDirection="column" key={index}>
							<Text>
								File path in <Text color="blueBright">{buildResult.module}</Text>:{" "}
								<Text color="yellowBright">{lessResult.source}</Text>
							</Text>
							{lessResult.error && <Error error={lessResult.error} debug={debug} />}
						</Box>
					))}
				{buildResult.css && buildResult.css.errors.length > 0 && (
					<Box flexDirection="column">
						<Text>
							File path in <Text color="blueBright">{buildResult.module}</Text>:{" "}
							<Text color="yellowBright">{buildResult.css.where}</Text>
						</Text>
						{buildResult.css.errors.map((error, index) => (
							<Error error={error} debug={debug} key={index} />
						))}
					</Box>
				)}
				{buildResult.typescript && (
					<Box flexDirection="column">
						<Text>
							Module <Text color="blueBright">{buildResult.module}</Text> failed in
							build of typescript.
						</Text>
						<Box flexDirection="column">
							{buildResult.typescript.errors.map((error, index) => (
								<Error error={error} debug={debug} key={index} />
							))}
						</Box>
					</Box>
				)}
				{/* RESOURCES: Can only return warning or OK, no error */}
			</Box>
		</Box>
	);
};
