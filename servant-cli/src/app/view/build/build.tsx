import * as api from "@servant/servant";
import * as React from "react";
import { Text, Box } from "ink";

import { Result } from "../result";

const moduleWidth = 70;

export interface BuildProps {
	result: api.Commands.CommandProgress["build"] | undefined;
	module: string;
	index: number;
}

export const Build: React.StatelessComponent<BuildProps> = (props) => {
	const { result, module, index } = props;
	const buildResult = result && result.data;

	return (
		<Box flexDirection="row">
			{!result && (
				<Box flexDirection="row">
					<Box width={moduleWidth}>
						<Text>
							<Text bold color="blueBright">
								{index}.
							</Text>{" "}
							<Text color="gray">{module}</Text>{" "}
						</Text>
					</Box>
				</Box>
			)}
			{result && (
				<Box flexDirection="row">
					<Box width={moduleWidth}>
						<Result module={module} result={result} index={index} />
					</Box>
					<Box>
						{buildResult && buildResult.less && (
							<>
								<Extension
									exts={api.Files.LESS.EXT}
									files={buildResult.less.files.length}
									errors={buildResult.less.errors.length}
									warnings={0}
								/>
								<Text color="gray">, </Text>
							</>
						)}
						{buildResult && buildResult.sass && (
							<>
								<Extension
									exts={api.Files.SASS.EXT}
									files={buildResult.sass.files.length}
									errors={buildResult.sass.errors.length}
									warnings={0}
								/>
								<Text color="gray">, </Text>
							</>
						)}
						{buildResult && buildResult.css && (
							<>
								<Extension
									exts={api.Files.CSS.EXT}
									files={buildResult.css.files.length}
									errors={0}
									warnings={0}
								/>
								<Text color="gray">, </Text>
							</>
						)}
						{buildResult && buildResult.typescript && (
							<>
								<Extension
									exts={api.Files.TS.EXT}
									files={buildResult.typescript.files.length}
									errors={buildResult.typescript.errors.length}
									warnings={0}
								/>
								<Text color="gray">, </Text>
							</>
						)}
						{buildResult && buildResult.resources && (
							<>
								<Extension
									exts={api.Files.UNKNOWN.EXT}
									files={buildResult.resources.files.length}
									errors={0}
									warnings={buildResult.resources.warnings.length}
								/>
							</>
						)}
					</Box>
				</Box>
			)}
		</Box>
	);
};

export interface ExtensionProps {
	exts: Array<string>;
	files: number;
	warnings: number;
	errors: number;
}

export const Extension: React.StatelessComponent<ExtensionProps> = (props) => {
	const { exts, files, warnings, errors } = props;

	return (
		<Text>
			{files === 0 && (
				<Text color="gray">
					<Text>{files}x</Text>
					<Text> </Text>
					{exts.map((ext, index) => (
						<Text key={index}>*.{ext} </Text>
					))}
				</Text>
			)}
			{files > 0 && (
				<Text>
					<Text color="blueBright">{files}x</Text>
					<Text> </Text>
					{(errors > 0 || warnings > 0) && (
						<Text color="grey">
							<Text>(</Text>
							{warnings > 0 && <Text color="yellowBright">{warnings}x</Text>}
							{warnings > 0 && errors > 0 && <Text> </Text>}
							{errors > 0 && <Text color="redBright">{errors}x</Text>}
							<Text>) </Text>
						</Text>
					)}
					{exts.map((ext, index) => (
						<Text key={index}>
							<Text color="cyanBright">*.{ext}</Text>{" "}
						</Text>
					))}
				</Text>
			)}
		</Text>
	);
};
