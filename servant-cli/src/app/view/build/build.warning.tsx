import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { Warning } from "../warning";

export interface BuildWarningProps {
	buildResult: api.Module.BuildResult;
	debug: boolean;
}

export const BuildWarning: React.StatelessComponent<BuildWarningProps> = (props) => {
	const { buildResult } = props;

	return (
		<Box flexDirection="row" marginLeft={2} marginTop={1} marginBottom={1}>
			{/* LESS: Can only return error or OK, no warning */}
			{/* CSS: Can only return error or OK, no warning */}
			{/* TYPESCRIPT: Can only return error or OK, no warning */}
			{buildResult.resources && (
				<Box flexDirection="column">
					<Text>
						Resources for module <Text color="blueBright">{buildResult.module}</Text>{" "}
						return some warnings.
					</Text>
					<Box flexDirection="row">
						{buildResult.resources.warnings.map((warning, index) => (
							<Warning
								message={`Can not process resource from '${warning.from}'.`}
								key={index}
							/>
						))}
					</Box>
				</Box>
			)}
		</Box>
	);
};
