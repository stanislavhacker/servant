import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { CommandInfo } from "../../index";

export interface AnalyzeProps {
	commandInfo: CommandInfo<"analyze"> | undefined;
}

export const Analyzer: React.StatelessComponent<AnalyzeProps> = (props) => {
	const { commandInfo } = props;
	const data = commandInfo?.data;

	//no data or command
	if (!data) {
		return null;
	}

	return (
		<>
			<Box>
				<Text bold color="greenBright">
					Analyze modules report
				</Text>
			</Box>
			<Box flexDirection="column">
				{data.sorted.map((module, index) => (
					<InternalModule key={module} index={index + 1} {...data.modules[module]} />
				))}
			</Box>
			<Box marginTop={1}>
				<Text bold color="greenBright">
					Analyze project report
				</Text>
			</Box>
			<Box marginBottom={2}>
				<AnalyzerProject data={data} />
			</Box>
		</>
	);
};

type InternalModuleProps = api.Module.AnalyzeResult & { index: number };
const InternalModule: React.StatelessComponent<InternalModuleProps> = (props) => {
	const { dependencies } = props;
	const grouped = splitDependencies(dependencies);

	return (
		<Box flexDirection="column" marginBottom={1}>
			<Box flexDirection="row">
				<ModuleIndex {...props} />
				<ModuleName {...props} />
			</Box>
			<ModuleDependencies
				dependencies={grouped.internal}
				type={api.Module.DependencyType.Internal}
			/>
			<ModuleDependencies
				dependencies={grouped.production}
				type={api.Module.DependencyType.Production}
			/>
			<ModuleDependencies
				dependencies={grouped.develop}
				type={api.Module.DependencyType.Develop}
			/>
			<ModuleDependencies dependencies={grouped.peer} type={api.Module.DependencyType.Peer} />
			<ModuleDependencies
				dependencies={grouped.optional}
				type={api.Module.DependencyType.Optional}
			/>
			<InternalModuleAnalyze {...props} />
		</Box>
	);
};

type InternalModuleAnalyzeProps = Omit<InternalModuleProps, "index">;
const InternalModuleAnalyze: React.StatelessComponent<InternalModuleAnalyzeProps> = (props) => {
	const { sizes, lines } = props;
	const sourcesLines = linesCount(lines.sources);
	const testsLines = linesCount(lines.tests);

	return (
		<Box flexDirection="column" marginLeft={4} marginTop={1}>
			<Box>
				<Text color="white" bold>
					File sizes:{" "}
				</Text>
				<Splitter
					items={[
						sizes.bundle > 0 && (
							<Text key={0}>
								<Text color="cyanBright">bundled</Text> size is{" "}
								<Text underline color="whiteBright">
									{humanFileSize(sizes.bundle)}
								</Text>{" "}
							</Text>
						),
						sizes.sources > 0 && (
							<Text key={1}>
								<Text color="blueBright">source</Text> files size is{" "}
								<Text underline color="whiteBright">
									{humanFileSize(sizes.sources)}
								</Text>{" "}
							</Text>
						),
						sizes.tests > 0 && (
							<Text key={2}>
								<Text color="greenBright">test</Text> files size is{" "}
								<Text underline color="whiteBright">
									{humanFileSize(sizes.tests)}
								</Text>{" "}
							</Text>
						),
					]}
				/>
			</Box>
			<Box>
				<Text color="white" bold>
					Final size of module with downloaded all dependencies will be roughly{" "}
					<Text underline color="whiteBright">
						{humanFileSize(sizes.installation)}
					</Text>
				</Text>
			</Box>

			{(sourcesLines > 0 || testsLines > 0) && (
				<Box flexDirection="column" marginTop={1}>
					{sourcesLines > 0 && (
						<Box>
							<Text color="white" bold>
								Lines count for <Text color="blueBright">sources</Text>:{" "}
							</Text>
							<ModuleLines lines={lines.sources} />
						</Box>
					)}
					{testsLines > 0 && (
						<Box>
							<Text color="white" bold>
								Lines count for <Text color="greenBright">tests</Text>:{" "}
							</Text>
							<ModuleLines lines={lines.tests} />
						</Box>
					)}
				</Box>
			)}
		</Box>
	);
};

type ModuleIndexProps = InternalModuleProps;
const ModuleIndex: React.StatelessComponent<ModuleIndexProps> = (props) => {
	const { index } = props;
	return (
		<Text color="blueBright" bold>
			{index}.{" "}
		</Text>
	);
};

type ModuleNameProps = Omit<InternalModuleProps, "index">;
const ModuleName: React.StatelessComponent<ModuleNameProps> = (props) => {
	const { name, type, missing } = props;

	if (type.includes(api.Module.DependencyType.Internal)) {
		return (
			<Text color="green" bold>
				{name}
			</Text>
		);
	}
	if (missing) {
		return (
			<Text color="redBright" italic bold>
				✘ {name}
			</Text>
		);
	}
	return <Text color="whiteBright">{name}</Text>;
};

type ModuleDependenciesProps = Pick<InternalModuleProps, "dependencies"> & {
	type: api.Module.DependencyType;
};
const ModuleDependencies: React.StatelessComponent<ModuleDependenciesProps> = (props) => {
	const { dependencies, type } = props;

	if (dependencies.length === 0) {
		return null;
	}

	return (
		<Box flexDirection="row" marginLeft={4}>
			<Box width={5}>
				<Text bold italic color="gray">
					{dependencies.length}x
				</Text>
			</Box>
			<Box width={15}>
				<ModuleType type={type} />
			</Box>
			<Text>
				<Splitter
					items={dependencies.map((dep, index) => (
						<Text key={index}>
							<ModuleName {...dep} />
							<ExternalSize sizes={dep.sizes} type={type} missing={dep.missing} />
						</Text>
					))}
				/>
			</Text>
		</Box>
	);
};

type ModuleTypeProps = { type: api.Module.DependencyType };
const ModuleType: React.StatelessComponent<ModuleTypeProps> = ({ type }) => {
	if (type === api.Module.DependencyType.Internal) {
		return (
			<Text color="magentaBright" underline>
				internal
			</Text>
		);
	}
	if (type === api.Module.DependencyType.Production) {
		return (
			<Text color="greenBright" bold>
				production
			</Text>
		);
	}
	if (type === api.Module.DependencyType.Develop) {
		return (
			<Text color="blueBright" bold>
				develop
			</Text>
		);
	}
	if (type === api.Module.DependencyType.Peer) {
		return (
			<Text color="blueBright" bold>
				peer
			</Text>
		);
	}
	if (type === api.Module.DependencyType.Optional) {
		return (
			<Text color="whiteBright" italic>
				optional
			</Text>
		);
	}
	return null;
};

type ModuleSizeProps = Pick<InternalModuleProps, "missing" | "sizes"> & {
	type: api.Module.DependencyType;
};
const ExternalSize: React.StatelessComponent<ModuleSizeProps> = ({ missing, sizes, type }) => {
	if (type === api.Module.DependencyType.Internal) {
		return null;
	}
	if (sizes.bundle === 0 || missing) {
		return null;
	}
	return (
		<Text color="gray">
			{" "}
			(
			<Text color="white" italic>
				{humanFileSize(sizes.bundle, 2)}
			</Text>
			)
		</Text>
	);
};

const Splitter: React.StatelessComponent<{ items: (JSX.Element | false | string)[] }> = ({
	items,
}) => {
	return (
		<>
			{items.reduce(
				(prev, dep, index, arr) => [
					...prev,
					...(index === arr.length - 1 || dep === false
						? [dep]
						: [
								dep,
								<Text color="gray" key={index + ","}>
									,{" "}
								</Text>,
						  ]),
				],
				[]
			)}
		</>
	);
};

type ModuleLinesProps = { lines: api.Module.LinesResult };
const ModuleLines: React.StatelessComponent<ModuleLinesProps> = ({ lines }) => {
	return (
		<Splitter
			items={Object.keys(lines).map((type, index) => {
				const count = (lines[type] ?? 0) as number;
				if (count === 0) {
					return false;
				}
				return (
					<Text key={index}>
						<Text color="whiteBright">{type}</Text>{" "}
						<Text bold underline>
							{count}
						</Text>{" "}
						lines
					</Text>
				);
			})}
		/>
	);
};

//project

const AnalyzerProject: React.StatelessComponent<{ data: api.Commands.AnalyzeSummaryResult }> = ({
	data,
}) => {
	const { missing, versions } = data.analyze;

	return (
		<Box flexDirection="column" marginLeft={2}>
			{missing.length > 0 && (
				<Box>
					<Text>
						<Text color="whiteBright" bold>
							Missing modules:{" "}
						</Text>
						<Splitter
							items={missing.map((name, index) => (
								<Text key={index} color="redBright" italic bold>
									✘ {name}
								</Text>
							))}
						/>
					</Text>
				</Box>
			)}
			{Object.keys(versions).length > 0 && (
				<Box flexDirection="column">
					<Text color="whiteBright" bold>
						Multiple versions:
					</Text>
					<Box flexDirection="column" marginLeft={2}>
						{Object.keys(versions).map((module, index) => (
							<Text key={index}>
								<Text color="white" bold>
									{module}:
								</Text>{" "}
								<Text color="redBright">
									<Splitter items={versions[module]} />
								</Text>
							</Text>
						))}
					</Box>
				</Box>
			)}
		</Box>
	);
};

//utils

function splitDependencies(dependencies: Array<api.Module.AnalyzeResult>) {
	const data = {
		internal: [] as api.Module.AnalyzeResult[],
		production: [] as api.Module.AnalyzeResult[],
		develop: [] as api.Module.AnalyzeResult[],
		peer: [] as api.Module.AnalyzeResult[],
		optional: [] as api.Module.AnalyzeResult[],
	};

	return dependencies.reduce((prev, dep) => {
		if (dep.type.includes(api.Module.DependencyType.Internal)) {
			prev.internal.push(dep);
			return prev;
		}
		if (dep.type.includes(api.Module.DependencyType.Production)) {
			prev.production.push(dep);
			return prev;
		}
		if (dep.type.includes(api.Module.DependencyType.Develop)) {
			prev.develop.push(dep);
			return prev;
		}
		if (dep.type.includes(api.Module.DependencyType.Peer)) {
			prev.peer.push(dep);
			return prev;
		}
		if (dep.type.includes(api.Module.DependencyType.Optional)) {
			prev.optional.push(dep);
			return prev;
		}
		return prev;
	}, data);
}

function humanFileSize(bytes: number, dp = 1) {
	const thresh = 1024;

	if (Math.abs(bytes) < thresh) {
		return bytes + " B";
	}

	const units = ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
	const r = 10 ** dp;

	let u = -1;
	do {
		bytes /= thresh;
		++u;
	} while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);

	return bytes.toFixed(dp) + " " + units[u];
}

function linesCount(lines: api.Module.LinesResult) {
	return Object.values(lines).reduce((lines: number, current: number) => lines + current, 0);
}
