import * as React from "react";

import { ServantState } from "../index";

import { Analyzer } from "./analyze";

export type StatusesProps = ServantState;

export const AnalyzerStatus: React.StatelessComponent<StatusesProps> = (props) => {
	const { modules } = props;

	return <Analyzer commandInfo={modules.commands["analyze"]} />;
};
