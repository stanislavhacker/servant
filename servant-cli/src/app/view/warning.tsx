import * as React from "react";
import { Box, Text } from "ink";

export interface WarningProps {
	message: string;
}

export const Warning: React.StatelessComponent<WarningProps> = (props) => {
	const { message } = props;

	return (
		<Box flexDirection="row" marginLeft={2} marginTop={1} marginBottom={1}>
			<Text bold>
				<Text color="yellowBright">[WARNING]</Text>:{" "}
			</Text>
			<Text>{message}</Text>
		</Box>
	);
};
