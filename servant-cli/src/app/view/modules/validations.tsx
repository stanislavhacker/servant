import * as api from "@servant/servant";
import * as React from "react";
import { Box, Text } from "ink";

import { VALIDATE_FIRST_COLUMN } from "../../constants";
import { Warning } from "../warning";

export interface ValidationProps {
	validation: api.Module.DependenciesValidations;
}

export const Validation: React.StatelessComponent<ValidationProps> = (props) => {
	const { validation } = props;

	const internal = Object.keys(validation.internal);
	const external = Object.keys(validation.external);
	const data = api.Module.validateVersions(validation);
	const missing = Object.keys(data.missing);

	return (
		<Box flexDirection="column" marginBottom={1}>
			<Box flexDirection="column">
				{validation.internal && internal.length > 0 && (
					<ValidationHeader
						header="Internal"
						modulesCount={internal.length}
						invalidCount={data.internalInvalidVersions.length}
					/>
				)}
				{data.internalInvalidVersions.length > 0 && <ValidationBanner />}
				{data.internalInvalidVersions.map((name, index) => (
					<ValidationInvalidList
						key={index}
						module={name}
						versions={validation.internal[name]}
					/>
				))}
			</Box>
			<Box flexDirection="column" marginBottom={1}>
				{validation.external && external.length > 0 && (
					<ValidationHeader
						header="External"
						modulesCount={external.length}
						invalidCount={data.externalInvalidVersions.length}
					/>
				)}
				{data.externalInvalidVersions.length > 0 && <ValidationBanner />}
				{data.externalInvalidVersions.map((name, index) => (
					<ValidationInvalidList
						key={index}
						module={name}
						versions={validation.external[name]}
					/>
				))}
			</Box>
			{missing.length > 0 && (
				<Box flexDirection="column" marginBottom={1}>
					<ValidationMissingBanner />
					{missing.map((name, index) => (
						<ValidationMissingList
							key={index}
							module={name}
							missing={data.missing[name]}
						/>
					))}
				</Box>
			)}
		</Box>
	);
};

export interface ValidationHeaderProps {
	header: string;
	modulesCount: number;
	invalidCount: number;
}

export const ValidationHeader: React.StatelessComponent<ValidationHeaderProps> = (props) => {
	const { header, modulesCount, invalidCount } = props;

	return (
		<Box flexDirection="row">
			<Box width={VALIDATE_FIRST_COLUMN}>
				<Text bold color="blue">
					{header}
				</Text>
			</Box>
			<Box>
				<Text color="gray">
					<Text color="green">{modulesCount}</Text> modules
					{invalidCount > 0 && (
						<Text>
							, <Text color="redBright">{invalidCount} invalid</Text>
						</Text>
					)}
				</Text>
			</Box>
		</Box>
	);
};

export interface ValidationInvalidListProps {
	module: string;
	versions: Array<string>;
}

export const ValidationInvalidList: React.StatelessComponent<ValidationInvalidListProps> = (
	props
) => {
	const { module, versions } = props;

	return (
		<Box flexDirection="row" marginLeft={2}>
			<Box width={VALIDATE_FIRST_COLUMN}>
				<Text bold color="white">
					{module}
				</Text>
			</Box>
			<Box>
				<Text color="redBright">{versions.join(", ")}</Text>
			</Box>
		</Box>
	);
};

export const ValidationBanner: React.StatelessComponent = () => {
	return (
		<Warning
			message={
				'This external modules has more versions across project definition. Try to repair it manually or you can use "unify" command.'
			}
		/>
	);
};

export interface ValidationMissingListProps {
	module: string;
	missing: Array<string>;
}

export const ValidationMissingList: React.StatelessComponent<ValidationMissingListProps> = (
	props
) => {
	const { module, missing } = props;

	return (
		<Box flexDirection="row" marginLeft={2}>
			<Box width={VALIDATE_FIRST_COLUMN}>
				<Text bold color="white">
					{module}
				</Text>
			</Box>
			<Box>
				<Text color="redBright">{missing.join(", ")}</Text>
			</Box>
		</Box>
	);
};

export const ValidationMissingBanner: React.StatelessComponent = () => {
	return (
		<Warning
			message={
				"Can not found these libraries in module. Are you sure that you run install or update command?"
			}
		/>
	);
};
