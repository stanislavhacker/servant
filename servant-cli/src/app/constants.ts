export const INDEX_COLUMN = 3;
export const MODULE_COLUMN = 40;

export const FIRST_COLUMN = 20;
export const SECOND_COLUMN = 15;

export const UNIFY_FIRST_COLUMN = 50;
export const UNIFY_SECOND_COLUMN = 30;
export const VALIDATE_FIRST_COLUMN = 50;
