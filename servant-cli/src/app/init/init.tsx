import * as React from "react";
import { useMemo } from "react";
import { Box } from "ink";
import { GeneratorResults, GeneratorSaveResults, view } from "@servant/servant-generators";
import * as api from "@servant/servant";

import { FIRST_COLUMN, SECOND_COLUMN } from "../constants";
import { Error as ErrorComponent } from "../view";
import { FlagHeader } from "../view/header";

export type InitState = {
	preparedInit: api.Module.PrepareInitResults | null;
	result: GeneratorResults<api.Module.InitCustomData> | null;
	saveResult: GeneratorSaveResults | null;
	error: Error | null;
};

export type InitProps = InitState & {
	debug: boolean;
	loaded: [number, number] | null;
	date: Date;
	//handlers
	setState: (state: Partial<InitState>) => void;
	onDone: (results: GeneratorSaveResults) => void;
};

export const Init: React.StatelessComponent<InitProps> = (props) => {
	const { debug, loaded, date, result, error, preparedInit, setState, onDone } = props;
	const generator = useMemo(() => {
		return {
			...api.generators.initGenerator,
		};
	}, [api.generators.initGenerator]);
	const loader = !preparedInit;

	return (
		<Box flexDirection="column" marginTop={1} marginBottom={1}>
			<FlagHeader
				packageJson={preparedInit && preparedInit.packageJson}
				loaded={loaded}
				loader={loader}
				date={date}
				name="Servant init"
				description={[
					"This is module init helper for fast",
					"creating project or submodules in your project.",
				]}
			/>
			{preparedInit && !error && (
				<view.Generator<api.Module.PrepareInitResults, api.Module.InitCustomData>
					{...generator}
					columns={[FIRST_COLUMN, SECOND_COLUMN]}
					props={preparedInit}
					onDone={(result) => {
						setState({ result: { ...result, output: result.customData?.into } });
					}}
				/>
			)}
			{preparedInit && result && result.customData && (
				<Box marginTop={1}>
					<view.Generated<api.Module.PrepareInitResults, api.Module.InitCustomData>
						loaded={generator}
						results={result}
						debug={debug}
						onDone={onDone}
					/>
				</Box>
			)}
			{error && <ErrorComponent debug={debug} error={error} />}
		</Box>
	);
};
