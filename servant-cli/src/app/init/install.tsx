import * as React from "react";
import { Box, Text } from "ink";

import { CommandInfo, ServantState } from "../index";
import { DotResult, Error } from "../view";

export const InitInstall: React.StatelessComponent<ServantState> = (props) => {
	const { modules, servant, flags } = props;
	const { initData } = servant;

	//no init data
	if (!initData) {
		return null;
	}

	const graph = modules.modulesData && modules.modulesData.graph;
	const install = modules.commands["install"] as CommandInfo<"install">;
	const done = Boolean(
		graph && install && graph.all.length === Object.keys(install.modules).length
	);

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<Text color="cyanBright">Messages from Servant:</Text>
			</Box>
			{(!graph || !install) && !modules.error && (
				<Box flexDirection="row">
					<Text> </Text>
					<Text backgroundColor="gray" color="whiteBright">
						[INSTALLING]
					</Text>
					<Text italic color="whiteBright">
						{" "}
						Installing new modules and dependencies
					</Text>
				</Box>
			)}
			{graph && install && !modules.error && (
				<Box flexDirection="row">
					<Text> </Text>
					{!done && (
						<>
							<Text backgroundColor="gray" color="whiteBright">
								[INSTALLING]
							</Text>
							<Text color="whiteBright">
								{" "}
								Installing new modules and dependencies
							</Text>
						</>
					)}
					{done && (
						<>
							<Text backgroundColor="greenBright" color="whiteBright">
								[SUCCESS]
							</Text>
							<Text color="green"> Installing new modules and dependencies</Text>
						</>
					)}
					<Text color="whiteBright">
						{" "}
						{graph.all.map((module) => (
							<DotResult key={module} result={install.modules[module]} />
						))}
					</Text>
				</Box>
			)}
			{graph && install && !modules.error && done && (
				<Box flexDirection="column" marginTop={1}>
					<Box flexDirection="row">
						<Text bold color="whiteBright">
							What can do next?
						</Text>
					</Box>
					<Box flexDirection="row">
						<Text color="whiteBright">
							{" "}
							- <Text color="blueBright">"npm run servant -- build"</Text> {"=>"}{" "}
							build your project and create dist
						</Text>
					</Box>
					<Box flexDirection="row">
						<Text color="whiteBright">
							{" "}
							- <Text color="blueBright">"npm run servant -- test"</Text> {"=>"} test
							your app and run tests against code
						</Text>
					</Box>
					<Box flexDirection="row">
						<Text color="whiteBright">
							{" "}
							- <Text color="blueBright">"npm run servant -- --server"</Text> {"=>"}{" "}
							run preview for web target modules
						</Text>
					</Box>
				</Box>
			)}
			{modules.error && <Error error={modules.error} debug={flags.debug} />}
		</Box>
	);
};
