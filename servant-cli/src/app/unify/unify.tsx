import * as React from "react";
import { useMemo } from "react";
import { Box } from "ink";
import { GeneratorResults, GeneratorSaveResults, view } from "@servant/servant-generators";
import * as api from "@servant/servant";

import { UNIFY_FIRST_COLUMN, UNIFY_SECOND_COLUMN } from "../constants";
import { Error as ErrorComponent } from "../view";
import { ServantModules } from "../index";
import { Deffered } from "../utils";

export type UnifyState = {
	waiter: Deffered<void> | null;
	preparedUnify: api.Module.PrepareUnifyResults | null;
	result: GeneratorResults<api.Module.UnifyCustomData> | null;
	saveResult: GeneratorSaveResults | null;
	error: Error | null;
};

export type UnifyProps = UnifyState & {
	debug: boolean;
	loaded: [number, number] | null;
	date: Date;
	latest: boolean;
	modules: ServantModules;
	//handlers
	setState: (state: Partial<UnifyState>) => void;
	onDone: (state: GeneratorSaveResults) => void;
};

export const Unify: React.StatelessComponent<UnifyProps> = (props) => {
	const { error, result, preparedUnify, latest, modules, debug, setState, onDone } = props;
	const generator = useMemo(() => {
		return {
			...api.generators.unifyGenerator,
			config: {
				...api.generators.unifyGenerator.config,
				answers: api.Commands.answersUnify({ modules, latest }),
			},
		};
	}, [api.generators.unifyGenerator, latest, modules]);

	//no prepared results
	if (!preparedUnify) {
		return null;
	}

	return (
		<Box flexDirection="column" marginTop={1} marginBottom={1}>
			{!error && (
				<view.Generator<api.Module.PrepareUnifyResults, api.Module.UnifyCustomData>
					{...generator}
					columns={[UNIFY_FIRST_COLUMN, UNIFY_SECOND_COLUMN]}
					props={preparedUnify}
					onDone={(result) => {
						setState({ result: { ...result, output: result.customData?.entry } });
					}}
				/>
			)}
			{result && result.customData && (
				<Box marginTop={1}>
					<view.Generated<api.Module.PrepareUnifyResults, api.Module.UnifyCustomData>
						loaded={generator}
						results={result}
						debug={debug}
						onDone={onDone}
					/>
				</Box>
			)}
			{error && <ErrorComponent debug={debug} error={error} />}
		</Box>
	);
};
