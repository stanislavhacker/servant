import * as React from "react";
import { useMemo } from "react";
import {
	GeneratorPreparedQuestions,
	GeneratorSaveResults,
	view,
} from "@servant/servant-generators";
import * as api from "@servant/servant";
import { Box, Text } from "ink";
import Select from "ink-select-input";
import * as path from "path";

import { errorCode, ExitReason } from "../../exit";
import { ServantState } from "../index";
import { FlagHeader } from "../view/header";

export type ServantGeneratorProps = {
	root: string;
	module: api.Module.ModuleDefinition;
	sources: api.Module.ModuleDirectories["sources"];
	tests: api.Module.ModuleDirectories["tests"];
	others: api.Module.ModuleDirectories["others"];
};

export type GenerateState = {
	module: api.Module.ModuleDefinition | null;
	generator?: string;
};

export type GenerateProps = GenerateState & {
	initData: api.InitData | null;
	modulesData: api.ModulesData | null;
	debug: boolean;
	loaded: [number, number] | null;
	date: Date;
	setState: (state: Partial<ServantState>) => void;
	onDone: (state: GeneratorSaveResults) => void;
};

export const Generate: React.StatelessComponent<GenerateProps> = (props) => {
	const { debug, modulesData, loaded, date, initData, module, generator, setState, onDone } =
		props;

	const items = useItems(modulesData);
	const generators = useGenerators(initData?.entry, modulesData, module);
	const generatorPros = useProps(module);
	const loader = !(modulesData && initData && initData.packageJson && modulesData.graph);

	return (
		<Box flexDirection="column" marginTop={1} marginBottom={1}>
			<FlagHeader
				packageJson={initData && initData.packageJson}
				loaded={loaded}
				loader={loader}
				date={date}
				name="Servant generate"
				description={[
					"This is module generator helper for fast",
					"creating components and code pieces in your project.",
				]}
			/>
			{modulesData && initData && initData.packageJson && modulesData.graph && (
				<Box flexDirection="column">
					<Text>
						Select <Text color="blueBright">module</Text> from all{" "}
						<Text color="greenBright">
							available modules (<Text bold>{modulesData.graph.all.length}</Text>)
						</Text>{" "}
						into which you want to generate content.
					</Text>
					{!module && (
						<Select
							items={items}
							onSelect={(item) => {
								setState({
									generate: {
										module: modulesData?.graph?.modules[item.value] ?? null,
									},
								});
							}}
						/>
					)}
					{module && generatorPros && (
						<>
							<Text>
								Generating content for "
								<Text color="blueBright">{module.name}</Text>" module.
							</Text>
							<view.Main<ServantGeneratorProps, unknown>
								preparedQuestions={createOutputQuestions(
									module.module?.directories
								)}
								dirs={generators}
								generator={generator}
								debug={debug}
								props={generatorPros}
								onDone={(results) => {
									handleErrors(results.errors);
									onDone(results);
								}}
								onErrors={(errors) => handleErrors(errors)}
							/>
						</>
					)}
				</Box>
			)}
		</Box>
	);
};

function useItems(modulesData: api.ModulesData | null) {
	return useMemo(() => {
		return modulesData && modulesData.graph
			? modulesData.graph.all.map((module, index) => ({
					label: `${(index + 1).toString().padStart(3, " ")}. ${module}`,
					value: module,
			  }))
			: [];
	}, [modulesData && modulesData.graph]);
}

function useGenerators(
	entry: string | undefined,
	modulesData: api.ModulesData | null,
	module: api.Module.ModuleDefinition | null
): string[] {
	return useMemo(() => {
		if (entry && modulesData && module && module.module) {
			const packageJson = module.module.packageJson;
			const servantJson = module.module.servantJson;

			return [
				path.join(entry, "node_modules"),
				path.join(packageJson.cwd, "node_modules"),
				...(servantJson.content.generators
					? [path.join(servantJson.cwd, servantJson.content.generators)]
					: []),
			];
		}
		return [];
	}, [modulesData, module, entry]);
}

function useProps(module: api.Module.ModuleDefinition | null): ServantGeneratorProps | null {
	return useMemo(() => {
		if (!module || !module.module) {
			return null;
		}

		return {
			module,
			root: module.module.servantJson.cwd,
			...(module.module?.directories ?? {
				sources: [],
				tests: [],
				others: [],
			}),
		};
	}, [module]);
}

function handleErrors(errors: Error[]) {
	if (errors.length > 0) {
		errorCode(ExitReason.ServantGenerateError);
	}
}

function createOutputQuestions(
	directories?: api.Module.ModuleDirectories
): GeneratorPreparedQuestions {
	if (!directories) {
		return [];
	}
	return [
		{
			type: "select",
			question: "Where do you want to generate content?",
			tip: "Select directory from provided source, tests or others where generator create new content.",
			id: "servant.build.output",
			values: [
				...directories.sources.map((source) => ({
					label: `${source} (source)`,
					value: () => source,
				})),
				...directories.tests.map((test) => ({
					label: `${test} (tests)`,
					value: () => test,
				})),
				...directories.others.map((other) => ({
					label: `${other}`,
					value: () => other,
				})),
			],
			prepared: true,
		},
	];
}
