import * as dev from "@servant/servant-development";
import { ServantState } from "../index";

export function processServers(
	servers: ServantState["server"]["servers"],
	log: dev.ServantDevLog
): ServantState["server"]["servers"] {
	if (!dev.isServantServerDevLog(log)) {
		return servers;
	}

	const { module, pid, status, stdoutCount, stderrCount, error } = log;

	const original = servers[module] || {};
	servers[module] = {
		...original,
		status,
		pid,
		error,
		stderrCount,
		stdoutCount,
	};
	return servers;
}

export function processClients(
	clients: ServantState["server"]["clients"],
	log: dev.ServantDevLog
): ServantState["server"]["clients"] {
	if (!dev.isServantClientDevLog(log)) {
		return clients;
	}

	const { module, status, error } = log;

	const original = clients[module] || {};
	clients[module] = {
		...original,
		status,
		error,
	};
	return clients;
}

export function processChanges(
	changes: ServantState["server"]["changes"],
	log: dev.ServantDevLog
): ServantState["server"]["changes"] {
	if (!dev.isServantChangesDevLog(log)) {
		return changes;
	}

	const { files, modules, errors, time, date } = log;

	if (modules.length === 0) {
		return changes;
	}

	changes.push({
		errors,
		changed: {
			modules: modules.slice(0),
			files: files.map(([path]) => path),
		},
		date,
		time,
	});
	return changes;
}

export function processShared(
	shared: ServantState["server"]["shared"],
	log: dev.ServantDevLog
): ServantState["server"]["shared"] {
	if (!dev.isServantSharedDevLog(log)) {
		return shared;
	}

	const { id, error, status } = log;

	const original = shared[id] || {};
	shared[id] = {
		...original,
		status,
		error,
	};
	return shared;
}
