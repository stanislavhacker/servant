import { Path } from "@servant/servant-files";
import {
	ServantServerInfo,
	ServantServerDevLog,
	ServantClientDevLog,
	ServantSharedDevLog,
	ServantServerShared,
} from "@servant/servant-development";
import * as React from "react";
import * as path from "path";
import { Box, Text } from "ink";

import { FIRST_COLUMN, SECOND_COLUMN } from "../constants";
import { Error } from "../view";

type GroupModulesInfoProps<T> = {
	text: string;
	over?: Record<string, T>;
	items?: Array<T>;
	item: (key: string, item: T) => JSX.Element;
};

export function GroupModulesInfo<T>(props: GroupModulesInfoProps<T>) {
	const { text, over, items, item } = props;

	return (
		<Box flexDirection="column" marginTop={1}>
			<Text underline color="white">
				{text}
			</Text>
			{over &&
				Object.keys(over).map((key, index) => (
					<Box flexDirection="row" key={index} marginLeft={1}>
						{item(key, over[key])}
					</Box>
				))}
			{items &&
				items.map((itm, index) => (
					<Box flexDirection="row" key={index} marginLeft={1}>
						{item(index.toString(), itm)}
					</Box>
				))}
		</Box>
	);
}

type ModuleItemProps = {
	module: string;
};

export function ModuleItem(props: React.PropsWithChildren<ModuleItemProps>) {
	const { module, children } = props;

	return (
		<>
			<Box width={FIRST_COLUMN + SECOND_COLUMN}>
				<Text>
					Entry{" "}
					<Text underline color="cyan">
						{module}
					</Text>
				</Text>
			</Box>
			<Box flexDirection="row">{children}</Box>
		</>
	);
}

type LinkItemProps = {
	link: ServantServerShared;
	status: ServantSharedDevLog["status"];
	error?: ServantSharedDevLog["error"] | null;
	debug: boolean;
};

export function LinkItem(props: LinkItemProps) {
	const { link, status, error, debug } = props;
	const { fromModule, toModule, from, to } = link;

	const fromRelative =
		"/" + Path.normalize(path.relative(fromModule.module?.servantJson.cwd ?? "", from));
	const toRelative =
		"/" + Path.normalize(path.relative(toModule.module?.servantJson.cwd ?? "", to));

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<ModuleStatus status={status} />
				<Text>
					<Text underline color="cyan">
						{fromModule.name}
					</Text>
					<Text color="blueBright">{fromRelative}</Text>
					<Text bold color="brightWhite">
						{" "}
						{">"}{" "}
					</Text>
					<Text underline color="cyan">
						{toModule.name}
					</Text>
					<Text color="blueBright">{toRelative}</Text>
				</Text>
			</Box>
			{error && <Error error={error} debug={debug} marginTop={0} />}
		</Box>
	);
}

type ServerModuleItemProps = ServantServerInfo["server"][string] & {
	status: ServantServerDevLog["status"];
	pid?: number;
	stdoutCount?: number;
	stderrCount?: number;
};

export function ServerModuleItem(props: ServerModuleItemProps) {
	const { command, status, pid, stdoutCount, stderrCount } = props;

	return (
		<>
			<ModuleStatus status={status} />
			<Text italic color="blueBright">
				{command}
			</Text>
			{!!pid && (
				<Text italic color="gray">
					with PID {pid}
				</Text>
			)}
			{!!stdoutCount && (
				<Text italic color="blueBright">
					{" "}
					{stdoutCount}x stdout
				</Text>
			)}
			{!!stderrCount && (
				<Text italic color="yellowBright">
					{" "}
					{stderrCount}x stderr
				</Text>
			)}
		</>
	);
}

type ClientModuleItemProps = ServantServerInfo["client"][string] & {
	domain: string;
	status: ServantClientDevLog["status"];
};

export function ClientModuleItem(props: ClientModuleItemProps) {
	const { domain, port, status } = props;

	return (
		<>
			<ModuleStatus status={status} />
			<Text italic color="white">
				http://{domain}:{port}/
			</Text>
		</>
	);
}

type ModuleStatusProps = {
	status:
		| ServantServerDevLog["status"]
		| ServantClientDevLog["status"]
		| ServantSharedDevLog["status"];
};
export function ModuleStatus(props: ModuleStatusProps) {
	const { status } = props;
	const el = createStatus(status);

	return (
		<Box flexDirection="row" width={15}>
			{el}
		</Box>
	);
}

function createStatus(status: ModuleStatusProps["status"]) {
	switch (status) {
		case "running":
			return (
				<Text bold backgroundColor="greenBright" color="whiteBright">
					[RUNNING]
				</Text>
			);
		case "restarting":
			return (
				<Text bold backgroundColor="yellowBright" color="whiteBright">
					[RESTARTING]
				</Text>
			);
		case "stopped":
			return (
				<Text bold backgroundColor="red" color="whiteBright">
					[STOPPED]
				</Text>
			);
		case "waiting":
			return (
				<Text bold backgroundColor="blueBright" color="whiteBright">
					[WAITING]
				</Text>
			);
		case "serving":
			return (
				<Text bold backgroundColor="yellowBright" color="whiteBright">
					[SERVING]
				</Text>
			);
		case "listening":
			return (
				<Text bold backgroundColor="greenBright" color="whiteBright">
					[LISTENING]
				</Text>
			);
		case "created":
			return (
				<Text bold backgroundColor="greenBright" color="whiteBright">
					[CREATED]
				</Text>
			);
		case "failed":
			return (
				<Text bold backgroundColor="red" color="whiteBright">
					[FAILED]
				</Text>
			);
		default:
			return null;
	}
}
