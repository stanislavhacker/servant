import * as React from "react";
import { Box } from "ink";

import { Servant, ServantState } from "../index";
import { Error } from "../view";

import { DevModules } from "./modules";
import { DevHeader } from "./header";
import { DevChanges } from "./changes";

export type ServerProps = ServantState & { Servant: Servant };

export const Server: React.StatelessComponent<ServerProps> = (props) => {
	const { flags, server, servant } = props;

	return (
		<Box flexDirection="column" marginTop={1}>
			<DevHeader {...props} />
			<DevModules {...server} debug={flags.debug} />
			<DevChanges changes={server.changes} />
			{server.error && <Error error={server.error} debug={flags.debug} />}
			{servant.error && <Error error={servant.error} debug={flags.debug} />}
		</Box>
	);
};
