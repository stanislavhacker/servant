import * as React from "react";

import { ServantState } from "../index";
import {
	ClientModuleItem,
	GroupModulesInfo,
	LinkItem,
	ModuleItem,
	ServerModuleItem,
} from "./module";

export type DevModulesProps = ServantState["server"] & {
	debug: boolean;
};

export const DevModules: React.StatelessComponent<DevModulesProps> = (props) => {
	const { info, clients, servers, shared, debug } = props;

	if (!info) {
		return null;
	}

	return (
		<>
			{Object.keys(info.client).length > 0 && (
				<GroupModulesInfo
					text="Spawned servers for web entries"
					over={info.client}
					item={(module, value) => {
						const { status } = clients[module] || {};
						return (
							<ModuleItem module={module}>
								<ClientModuleItem
									domain={info.domain}
									port={value.port}
									status={status}
								/>
							</ModuleItem>
						);
					}}
				/>
			)}
			{Object.keys(info.server).length > 0 && (
				<GroupModulesInfo
					text="Spawned runners for node entries"
					over={info.server}
					item={(module, value) => {
						const { status, pid, stdoutCount, stderrCount } = servers[module] || {};
						return (
							<ModuleItem module={module}>
								<ServerModuleItem
									command={value.command}
									status={status}
									pid={pid}
									stderrCount={stderrCount}
									stdoutCount={stdoutCount}
								/>
							</ModuleItem>
						);
					}}
				/>
			)}
			{info.shared.links.length > 0 && (
				<GroupModulesInfo
					text="Created shared folders"
					items={info.shared.links}
					item={(_, value) => {
						const { status, error } = shared[value.id] || {};
						return (
							<LinkItem link={value} status={status} error={error} debug={debug} />
						);
					}}
				/>
			)}
		</>
	);
};
