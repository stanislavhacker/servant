import * as React from "react";
import { Box, Text } from "ink";

import { FIRST_COLUMN, SECOND_COLUMN } from "../constants";
import { ServantState } from "../index";

export type DevHeaderProps = ServantState;

export const DevHeader: React.StatelessComponent<DevHeaderProps> = (props) => {
	const { server, servant, date, loaded } = props;
	const { initData } = servant;

	const info = server.info;
	const clients = info ? Object.keys(info.client).length : 0;
	const servers = info ? Object.keys(info.server).length : 0;
	const shared = info ? info.shared.links.length : 0;

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<Box width={FIRST_COLUMN}>
					<Text bold color="green">
						Servant dev
					</Text>
				</Box>
				{initData && initData.packageJson && (
					<>
						<Box width={SECOND_COLUMN}>
							<Text bold color="yellow">
								v{initData.packageJson.content.version}
							</Text>
						</Box>
						<Box>
							<Text italic color="white">
								Started at {date.toLocaleTimeString()}
							</Text>
							{loaded !== null && (
								<Text color="gray">
									, load time{" "}
									<Text bold>{`${loaded[0]}s ${loaded[1] / 1000000}ms`}</Text>
								</Text>
							)}
						</Box>
					</>
				)}
			</Box>
			{info && (
				<Box flexDirection="column">
					<Box flexDirection="row">
						<Box width={FIRST_COLUMN}>
							<Text bold color="green">
								Entries
							</Text>
						</Box>
						<Box width={SECOND_COLUMN}>
							<Text underline color="magenta">
								{clients + servers} modules
							</Text>
						</Box>
					</Box>
					<Box flexDirection="row">
						<Box width={FIRST_COLUMN}>
							<Text bold color="blue">
								Shared
							</Text>
						</Box>
						<Box width={SECOND_COLUMN}>
							<Text underline color="blueBright">
								{shared} folders
							</Text>
						</Box>
					</Box>
				</Box>
			)}
		</Box>
	);
};
