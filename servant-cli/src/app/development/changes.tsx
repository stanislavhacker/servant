import * as React from "react";
import { Box, Text } from "ink";

import { ServantState } from "../index";

export type DevChangesProps = {
	changes: ServantState["server"]["changes"];
};

export const DevChanges: React.StatelessComponent<DevChangesProps> = (props) => {
	const { changes } = props;

	if (changes.length === 0) {
		return null;
	}

	return (
		<Box flexDirection="column" marginTop={1}>
			{changes
				.slice(Math.max(changes.length - 10, 0))
				.map(({ changed, errors, time, date }, key) => {
					const errorsList = errors
						? Object.keys(errors).reduce(
								(arr, module) => [...arr, ...errors[module]],
								[]
						  )
						: [];
					return (
						<Box flexDirection="row" key={key}>
							<Text italic>
								<Text color="gray">
									[{date.toLocaleTimeString()}]:<Text> </Text>
								</Text>
								<Text>
									<Text color="white">Totally </Text>
									<Text color="greenBright">{changed.modules.length}</Text>
									<Text color="white"> modules and </Text>
									<Text color="greenBright">{changed.files.length}</Text>
									<Text color="white"> files </Text>
									<Text color="blueBright">changed</Text>
								</Text>
								<Text color="gray"> +{`${time[0]}s ${time[1] / 1000000}ms`}</Text>
								{errors && errorsList.length > 0 && (
									<Text color="red">
										{" "}
										<Text bold>
											+{errorsList.length} errors in{" "}
											<Text underline>{Object.keys(errors).join(", ")}</Text>
										</Text>
									</Text>
								)}
								{errorsList.length === 0 && (
									<Text color="red">
										{" "}
										<Text bold underline color="greenBright">
											Successfully compiled!
										</Text>
									</Text>
								)}
							</Text>
						</Box>
					);
				})}
		</Box>
	);
};
