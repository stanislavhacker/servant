import * as path from "path";
import * as React from "react";
import { Box } from "ink";
import * as api from "@servant/servant";
import * as dev from "@servant/servant-development";

import { errorCode, ExitReason } from "../exit";

import {
	Header,
	Footer,
	Modules,
	UnifyAction,
	InitAction,
	GenerateAction,
	AnalyzerStatus,
	mergeBugs,
} from "./view";
import { UnifyState } from "./unify";
import { InitState } from "./init";
import { GenerateState } from "./generate";
import {
	processChanges,
	processClients,
	processServers,
	processShared,
	Server,
} from "./development";
import { Help } from "./help";
import { deferred } from "./utils";

const name = "servant-cli";

export type ServantProps = {
	init: boolean;
	generate: boolean | string;
	prune: boolean;
	noaudit: boolean;
	help: boolean;
	entry: string;
	debug: boolean;
	tag: string | undefined;
	gui: boolean;
	browsers: string;
	devices: string;
	production: boolean;
	dependencies: boolean;
	changed: boolean;
	freeze: boolean;
	only: Array<string>;
	commands: Array<api.Commands.Commands>;
	server: boolean;
	port: number;
	report: Array<string>;
	watch: boolean;
	commit: boolean;
	increment: string | undefined;
	fix: boolean;
	latest: boolean;
	link: boolean;
	unlink: boolean;
	m?: ServantModules;
};
export type ServantState = {
	servant: {
		initData: api.InitData | null;
		error: Error | null;
	};
	modules: {
		modulesData: api.ModulesData | null;
		error: Error | null;
		commands: {
			["clean"]?: CommandInfo<"clean">;
			["install"]?: CommandInfo<"install">;
			["update"]?: CommandInfo<"update">;
			["build"]?: CommandInfo<"build">;
			["unify"]?: CommandInfo<"unify">;
			["publish"]?: CommandInfo<"publish">;
			["tests"]?: CommandInfo<"tests">;
			["validate"]?: CommandInfo<"validate">;
			["analyze"]?: CommandInfo<"analyze">;
		};
	};
	init: InitState;
	unify: UnifyState;
	generate: GenerateState;
	server: {
		port: number;
		error: Error | null;
		info: dev.ServantServerInfo | null;
		packageJson: api.PackageJson.PackageJsonInfo | null;
		changes: Array<{
			changed: {
				modules: string[];
				files: string[];
			};
			time: [number, number];
			date: Date;
			errors?: Record<string, Array<Error>>;
		}>;
		clients: Record<
			string,
			{
				status: dev.ServantClientDevLog["status"];
				error?: Error | null;
			}
		>;
		servers: Record<
			string,
			{
				status: dev.ServantServerDevLog["status"];
				pid?: number;
				stdoutCount?: number;
				stderrCount?: number;
				error?: Error | null;
			}
		>;
		shared: Record<
			string,
			{
				status: dev.ServantSharedDevLog["status"];
				error?: Error | null;
			}
		>;
	};
	flags: ServantFlags;
	m: ServantModules;
	date: Date;
	started: [number, number];
	loaded: [number, number] | null;
	commands: Array<api.Commands.Commands>;
	reports: {
		error: Error | null;
		list: Array<api.Reports.Reporter>;
		files: Array<string>;
	};
	watcher: {
		error: Error | null;
		info: api.Watcher.WatcherInfo | null;
		change: api.Watcher.WatcherChange | null;
	};
};

export type ServantModules = {
	[key: string]: string;
};
export type ServantFlags = {
	init: boolean;
	generate: boolean | string;
	help: boolean;
	tag: string | undefined;
	only: Array<string>;
	browsers: Array<string>;
	devices: Array<string>;
	debug: boolean;
	entry: string;
	production: boolean;
	prune: boolean;
	dependencies: boolean;
	noaudit: boolean;
	changed: boolean;
	freeze: boolean;
	server: boolean;
	gui: boolean;
	watch: boolean;
	commit: boolean;
	increment: string | undefined;
	fix: boolean;
	latest: boolean;
	link: boolean;
	unlink: boolean;
};

export interface CommandInfo<C extends api.Commands.Commands> {
	command: C;
	modules: {
		[key: string]: api.Commands.CommandProgress[C];
	};
	time: string;
	running: boolean;
	done: boolean;
	data: api.Commands.CommandResults[C]["data"] | null;
}

export class Servant extends React.Component<ServantProps, ServantState> {
	private servant: api.ServantApi;
	private dev: dev.ServantDevelopmentApi;

	constructor(p: ServantProps) {
		super(p);

		const props = transformProps(p);
		this.servant = api.Servant();
		this.dev = dev.ServantDevelopment();

		this.state = {
			servant: {
				error: null,
				initData: null,
			},
			modules: {
				modulesData: null,
				error: null,
				commands: {},
			},
			init: {
				preparedInit: null,
				error: null,
				result: null,
				saveResult: null,
			},
			unify: {
				waiter: null,
				preparedUnify: null,
				error: null,
				result: null,
				saveResult: null,
			},
			generate: {
				module: null,
				generator: undefined,
			},
			server: {
				packageJson: null,
				port: props.port,
				error: null,
				info: null,
				servers: {},
				clients: {},
				shared: {},
				changes: [],
			},
			m: props.m || {},
			flags: {
				init: props.init,
				generate: props.generate,
				help: props.help,
				changed: props.changed,
				production: props.production,
				tag: props.tag,
				prune: props.prune,
				debug: props.debug,
				only: props.only,
				freeze: props.freeze,
				entry: props.entry || __dirname,
				server: props.server,
				dependencies: props.dependencies,
				gui: props.gui,
				watch: props.watch,
				commit: props.commit,
				increment: props.increment,
				noaudit: props.noaudit,
				fix: props.fix,
				latest: props.latest,
				link: props.link,
				unlink: props.unlink,
				browsers: asArray(props.browsers).filter((itm) => Boolean(itm)),
				devices: asArray(props.devices).filter((itm) => Boolean(itm)),
			},
			date: new Date(),
			started: process.hrtime(),
			loaded: null,
			watcher: {
				error: null,
				info: null,
				change: null,
			},
			commands: props.commands,
			reports: {
				error: null,
				list: api.Reports.parseReport(props.report),
				files: [],
			},
		};
	}

	render() {
		const { state } = this;
		const { flags } = state;

		//view for help
		if (flags.help) {
			return (
				<Box flexDirection="column">
					<Help {...state} />
				</Box>
			);
		}

		//createServer flag => special view
		if (flags.server) {
			return (
				<Box flexDirection="column">
					<Server {...state} Servant={this} />
				</Box>
			);
		}

		//init flag => special view + modules for install
		if (flags.init) {
			return (
				<Box flexDirection="column">
					<InitAction
						{...state}
						setState={(state) => this.setState(state as ServantState)}
						onDone={() => {
							this.runServant();
						}}
					/>
				</Box>
			);
		}

		if (flags.generate) {
			return (
				<Box flexDirection="column">
					<GenerateAction
						{...state}
						setState={(state) => this.setState(state as ServantState)}
					/>
				</Box>
			);
		}

		return (
			<Box flexDirection="column">
				<Header {...state} />
				<Modules {...state} />
				<UnifyAction
					{...state}
					setState={(state) => this.setState(state as ServantState)}
				/>
				<AnalyzerStatus {...state} />
				<Footer {...state} />
			</Box>
		);
	}

	componentDidMount() {
		const { state } = this;

		//NOTE: We don't make anything if we use help flag
		if (state.flags.help) {
			return;
		}

		//NOTE: We run dev server and skip all others
		if (state.flags.server) {
			this.runServer();
			return;
		}

		//NOTE: Run only init runtime and skip all others
		if (state.flags.init) {
			this.runInit();
			return;
		}
		//NOTE: Run only generate runtime and skip all others
		if (state.flags.generate) {
			this.runGenerate();
			return;
		}

		//runServer servant
		this.runServant();
	}

	//INIT

	private runInit() {
		const { started, init } = this.state;
		const { entry } = this.props;

		const err = (error) => {
			this.setState({
				init: {
					...this.state.init,
					error: error,
				},
			});
			errorCode(ExitReason.ServantInitError);
		};

		loadServantPackageJson()
			.then((packageJson) => api.Module.prepareInit(packageJson, entry))
			.then((preparedInit) => {
				this.setState({
					loaded: process.hrtime(started),
					init: {
						...init,
						preparedInit,
					},
				});
			})
			.catch(err);
	}

	//UNIFY

	private runUnify() {
		const { modules, servant, unify } = this.state;
		const { modulesData } = modules;
		const { initData } = servant;
		const commandInfo = modules.commands["unify"];

		if (!initData || !modulesData || !modulesData.graph || !commandInfo) {
			return Promise.resolve();
		}

		const waiter = deferred<void>();
		this.setState({
			unify: {
				...unify,
				waiter,
				preparedUnify: { graph: modulesData.graph },
			},
		});
		return waiter.then(() => {
			const { result } = this.state.unify;
			if (result) {
				const commandResult = api.Commands.convertUnify(result);
				commandInfo.time = commandResult.time;
				commandInfo.data = commandResult.data;
				commandInfo.modules = commandResult.modules;
			}
			commandInfo.done = true;
			this.forceUpdate();
		});
	}

	//GENERATE

	private runGenerate() {
		const { flags } = this.state;

		this.servant
			.init(flags.entry)
			.then((initData) => {
				this.setState({
					servant: {
						...this.state.servant,
						initData: initData,
					},
				});
				return this.loadModules();
			})
			.catch((error) => {
				this.setState({
					servant: {
						...this.state.servant,
						error: error,
					},
				});
				errorCode(ExitReason.ServantLoadError);
			});
	}

	//SERVANT

	private runServant() {
		const { flags } = this.state;

		this.servant
			.init(flags.entry)
			.then((initData) => {
				this.setState({
					servant: {
						...this.state.servant,
						initData: initData,
					},
				});
				this.loadModules().then(() => this.createCommands());
			})
			.catch((error) => {
				this.setState({
					servant: {
						...this.state.servant,
						error: error,
					},
				});
				errorCode(ExitReason.ServantLoadError);
			});
	}

	private loadModules(): Promise<void> {
		const { servant, flags, started, init } = this.state;
		const { initData } = servant;

		if (!initData) {
			return Promise.resolve();
		}

		return this.servant
			.modules(initData, {
				only: enrichOnly(flags.only, initData.module),
				changed: changedArray(flags.changed),
				dependencies: flags.dependencies,
				init: Boolean(init && init.result && init.result.errors.length === 0),
			})
			.then((modulesData) => {
				this.setState({
					loaded: process.hrtime(started),
					modules: {
						...this.state.modules,
						modulesData: modulesData,
					},
				});
			})
			.catch((error) => {
				this.setState({
					modules: {
						...this.state.modules,
						error: error,
					},
				});
				errorCode(ExitReason.ServantModulesLoadError);
			});
	}

	private fillCommands() {
		const { commands, modules, flags } = this.state;

		const err = (error) => {
			this.setState({
				modules: {
					...this.state.modules,
					error: error,
				},
			});
			errorCode(ExitReason.ServantCommandError);
		};

		//do not process commands if there are no modules
		if (
			modules &&
			modules.modulesData &&
			modules.modulesData.graph &&
			api.Module.iterateSorted(modules.modulesData.graph.sorted).length === 0
		) {
			return;
		}

		let p: Promise<unknown> = Promise.resolve();
		commands.forEach((cmd) => {
			p = p.then(() => this.runCommand(cmd)).catch(err);
		});

		p.then(() => this.createReports());
		p.then(() => this.runWatcher());

		if (areCommands(commands, flags)) {
			errorCode(ExitReason.ServantCommandsEmpty);
		}
	}

	private runCommand(cmd: api.Commands.Commands) {
		const { modules, servant, flags, m } = this.state;
		const { modulesData } = modules;
		const { initData } = servant;
		const commandInfo = modules.commands[cmd];

		if (!initData || !modulesData || !modulesData.graph || !commandInfo) {
			return Promise.resolve();
		}

		commandInfo.running = true;
		this.forceUpdate();

		//NOTE: Special case for unify command
		if (commandInfo.command === "unify") {
			return this.runUnify();
		}

		return api
			.command(initData, modulesData, commandInfo.command, {
				progress: (progress) => this.onProgress(progress),
				modules: m,
				browsers: flags.browsers,
				devices: flags.devices,
				prune: flags.prune,
				production: flags.production,
				transpile: false,
				tag: flags.tag,
				freeze: flags.freeze,
				gui: flags.gui,
				commit: flags.commit,
				increment: flags.increment,
				noaudit: flags.noaudit,
				fix: flags.fix,
				latest: flags.latest,
				link: flags.link,
				unlink: flags.unlink,
			})
			.then((result) => {
				commandInfo.time = result.time || "";
				commandInfo.data = result.data;
				commandInfo.done = true;
				this.forceUpdate();
			});
	}

	private onProgress<C extends api.Commands.Commands>(progress: api.Commands.CommandProgress[C]) {
		const commands = { ...this.state.modules.commands };
		const command = commands[progress.command];

		if (!command) {
			return;
		}

		command.modules[progress.module] = progress;

		//save exit code
		if (progress.type === api.Module.DoneType.WARNING) {
			errorCode(ExitReason.ServantCommandWarning);
		}
		if (progress.type === api.Module.DoneType.FAIL) {
			errorCode(ExitReason.ServantCommandFailed);
		}

		this.setState({
			modules: {
				...this.state.modules,
				commands: commands,
			},
		});
	}

	private createCommands() {
		const { commands } = this.state;

		commands.forEach((cmd) => {
			this.createCommand(cmd);
		});

		this.fillCommands();
	}

	private createCommand<C extends api.Commands.Commands>(cmd: C) {
		const { state } = this;

		const commands = {
			...state.modules.commands,
			[cmd]: createCommandInfo(cmd) as CommandInfo<C>,
		};

		this.setState({
			modules: {
				...state.modules,
				commands: commands,
			},
		});
	}

	//REPORTS

	private createReports() {
		const { reports, servant, modules } = this.state;
		const runnableReports = this.servant.reports;

		const promises: Array<Promise<string[]>> = [];
		const tests = modules && modules.commands["tests"];
		const analyze = modules && modules.commands["analyze"];

		const err = (error) => {
			this.setState({
				reports: {
					...this.state.reports,
					error: error,
				},
			});
			errorCode(ExitReason.ServantReportsError);
		};

		if (servant.initData) {
			//REPORT: Issues
			if (reports.list.indexOf("issues") >= 0 && tests) {
				promises.push(
					runnableReports.issues.run(servant.initData, mergeBugs(tests.modules))
				);
			}
			//REPORT: Analyze
			if (reports.list.indexOf("analyze") >= 0 && analyze && analyze.data) {
				promises.push(runnableReports.analyze.run(servant.initData, analyze.data));
			}

			Promise.all(promises)
				.catch(err)
				.then((paths: Array<string[]>) => {
					this.setState({
						reports: {
							...this.state.reports,
							files: paths.reduce((prev, p) => [...prev, ...p], []),
						},
					});
				});
		}
	}

	//SERVER

	private runServer() {
		const { flags } = this.state;

		this.servant
			.init(flags.entry)
			.then((initData) => {
				this.setState({
					servant: {
						...this.state.servant,
						initData: initData,
					},
				});
				this.createServer();
			})
			.catch((error) => {
				this.setState({
					servant: {
						...this.state.servant,
						error: error,
					},
				});
				errorCode(ExitReason.ServantServerInitError);
			});
	}

	private createServer() {
		const { server, flags, started } = this.state;

		this.dev
			.server(server.port, flags.entry, (log) => this.onLog(log))
			.then((info) => {
				this.setState({
					loaded: process.hrtime(started),
					server: {
						...this.state.server,
						info: info,
					},
				});
			})
			.catch((error) => {
				this.setState({
					server: {
						...this.state.server,
						error: error,
					},
				});
				errorCode(ExitReason.ServantServerStartError);
			});
	}

	private onLog(log: dev.ServantDevLog) {
		const { server } = this.state;

		this.setState({
			server: {
				...this.state.server,
				servers: processServers(server.servers, log),
				clients: processClients(server.clients, log),
				changes: processChanges(server.changes, log),
				shared: processShared(server.shared, log),
			},
		});
	}

	//WATCHER

	private runWatcher() {
		const { servant, flags } = this.state;
		const { initData } = servant;

		//no init data or no watcher flag
		if (!initData || !flags.watch) {
			return;
		}

		const exitCode = process.exitCode || 0;
		//do not start on error
		if (exitCode > 0) {
			return;
		}

		const err = (error) => {
			this.setState({
				watcher: {
					...this.state.watcher,
					error: error,
				},
			});
			errorCode(ExitReason.ServantWatcherError);
		};

		this.servant
			.watch(initData, (info, change) => {
				this.setState({
					watcher: {
						...this.state.watcher,
						info: info,
						change: change,
					},
				});
			})
			.catch(err);
	}
}

export function areCommands(commands: Array<api.Commands.Commands>, flags: ServantFlags): boolean {
	return commands.length === 0 && !flags.watch && !flags.generate;
}

function changedArray(changed: boolean): Array<api.Changes.ChangeBy> {
	if (changed) {
		return [api.Changes.ChangeBy.FILESYSTEM, api.Changes.ChangeBy.GIT];
	}
	return [];
}

function createCommandInfo<C extends api.Commands.Commands>(cmd: C): CommandInfo<C> {
	return {
		running: false,
		done: false,
		command: cmd,
		time: "",
		modules: {},
		data: null,
	};
}

function asArray(value: string | null): Array<string> {
	const val = value?.toString() ?? "";

	if (!val) {
		return [];
	}
	return val.split(",").map((item) => item.trim());
}

function loadServantPackageJson(): Promise<api.PackageJson.PackageJsonInfo> {
	const directory = loadServantDirectory();
	const packageJson = path.join(directory, api.PackageJson.PACKAGE_JSON);
	return api.PackageJson.load(packageJson, true);
}

function loadServantDirectory() {
	let pth = __dirname;

	while (pth && path.basename(pth) !== name) {
		pth = path.dirname(pth);
	}

	return pth;
}

function enrichOnly(only: Array<string>, module?: string) {
	if (module && only.indexOf(module) === -1) {
		return [...only, module];
	}
	return [...only];
}

function transformProps(props: ServantProps): ServantProps {
	const { commands } = props;
	//NOTE: Transform install command, add shared and turn
	//on link mode to creates link when install is run
	if (commands.includes("install") && !commands.includes("shared")) {
		return {
			...props,
			commands: [...commands, "shared"],
			link: true,
		};
	}
	return props;
}
