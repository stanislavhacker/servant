export type Deffered<T> = Promise<T> & {
	resolve: (value: T) => void;
	reject: (reason: Error) => void;
};

export function deferred<T>(): Deffered<T> {
	let resolve, reject;

	const promise = new Promise((res, rej) => {
		resolve = res;
		reject = rej;
	}) as Deffered<T>;

	promise.resolve = resolve;
	promise.reject = reject;

	return promise;
}
