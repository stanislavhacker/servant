import * as React from "react";
import { render } from "ink";

import { Servant } from "./app";
import { get } from "./cli";

const { props, commands } = get(process);

render(<Servant commands={commands} {...props} />);
