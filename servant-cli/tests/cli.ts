import { get, GetData } from "../src/cli";

describe("servant-cli", () => {
	describe("parsing cli and supported commands", () => {
		function createProcess(argv: string, env: NodeJS.ProcessEnv = {}): NodeJS.Process {
			const proc = jasmine.createSpyObj<NodeJS.Process>("process", ["cwd"]);

			proc.cwd.and.returnValue("/project/path/to/dir");
			proc.env = env;
			proc.argv = ["node", "index.js"].concat(argv.length ? argv.split(" ") : []);

			return proc;
		}

		function createProps(data: Partial<GetData["props"]> = {}): GetData["props"] {
			const defaults = {
				entry: "/project/path/to/dir",
				debug: false,
				production: false,
				dependencies: false,
				changed: false,
				tag: undefined,
				help: false,
				freeze: false,
				init: false,
				prune: false,
				server: false,
				watch: false,
				devices: "",
				browsers: "",
				gui: false,
				port: 9000,
				report: [],
				only: [],
				commit: false,
				noaudit: false,
				fix: false,
				latest: false,
				increment: undefined,
				generate: false,
				link: false,
				unlink: false,
			};

			return {
				...defaults,
				...data,
			};
		}

		it("empty command line", () => {
			const proc = createProcess("");
			const data = get(proc);

			expect(data.props).toEqual(createProps());
			expect(data.commands).toEqual([]);
		});

		it("command line with commands", () => {
			const proc = createProcess(
				"clean install update build unify publish tests shared validate"
			);
			const data = get(proc);

			expect(data.props).toEqual(createProps());
			expect(data.commands).toEqual([
				"clean",
				"install",
				"update",
				"build",
				"unify",
				"publish",
				"tests",
				"shared",
				"validate",
			]);
		});

		it("command line with commands alias", () => {
			const proc = createProcess("c i u b f p t s v");
			const data = get(proc);

			expect(data.props).toEqual(createProps());
			expect(data.commands).toEqual([
				"clean",
				"install",
				"update",
				"build",
				"unify",
				"publish",
				"tests",
				"shared",
				"validate",
			]);
		});

		it("command line with command and switches 1", () => {
			const proc = createProcess(
				"clean --entry C:\\test\\module --debug --production --changed --help --freeze --init --prune"
			);
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					entry: "C:\\test\\module",
					debug: true,
					production: true,
					changed: true,
					help: true,
					freeze: true,
					init: true,
					prune: true,
				})
			);
			expect(data.commands).toEqual(["clean"]);
		});

		it("command line with command and switches 2", () => {
			const proc = createProcess("publish --tag myTag --server --watch --gui --port 8888");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					tag: "myTag",
					server: true,
					watch: true,
					gui: true,
					port: 8888,
				})
			);
			expect(data.commands).toEqual(["publish"]);
		});

		it("command line with command and switches 3", () => {
			const proc = createProcess("build --report issues --only test");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					report: ["issues"],
					only: ["test"],
				})
			);
			expect(data.commands).toEqual(["build"]);
		});

		it("command line with command and switches form unify", () => {
			const proc = createProcess("unify --m.react 19.3.5 --m.envfull 0.1.8");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					m: {
						react: "19.3.5",
						envfull: "0.1.8",
					},
				})
			);
			expect(data.commands).toEqual(["unify"]);
		});

		it("command line with command and browsers and devices", () => {
			const proc = createProcess("tests --gui --browsers Webkit --devices Nokia");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					gui: true,
					devices: "Nokia",
					browsers: "Webkit",
				})
			);
			expect(data.commands).toEqual(["tests"]);
		});

		it("command line with command for tests only and dependencies", () => {
			const proc = createProcess("tests --only @servant/test --dependencies");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					only: ["@servant/test"],
					dependencies: true,
				})
			);
			expect(data.commands).toEqual(["tests"]);
		});

		it("command line with command commit", () => {
			const proc = createProcess("publish --commit");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					commit: true,
				})
			);
			expect(data.commands).toEqual(["publish"]);
		});

		it("command line with noaudit flag", () => {
			const proc = createProcess("install --noaudit");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					noaudit: true,
				})
			);
			expect(data.commands).toEqual(["install"]);
		});

		it("command line with fix flag", () => {
			const proc = createProcess("validate --fix");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					fix: true,
				})
			);
			expect(data.commands).toEqual(["validate"]);
		});

		it("command line with increment flag", () => {
			const proc = createProcess("publish --increment major");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					increment: "major",
				})
			);
			expect(data.commands).toEqual(["publish"]);
		});

		it("command line with latest flag", () => {
			const proc = createProcess("unify --latest");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					latest: true,
				})
			);
			expect(data.commands).toEqual(["unify"]);
		});

		it("command line with generate and generator", () => {
			const proc = createProcess("--generate generator");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					generate: "generator",
				})
			);
			expect(data.commands).toEqual([]);
		});

		it("command line with generate", () => {
			const proc = createProcess("--generate");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					generate: true,
				})
			);
			expect(data.commands).toEqual([]);
		});

		it("command line with generate, generator and commands", () => {
			const proc = createProcess("unify clean --generate generator");
			const data = get(proc);

			expect(data.props).toEqual(
				createProps({
					generate: "generator",
				})
			);
			expect(data.commands).toEqual(["unify", "clean"]); //commands will be ignored
		});
	});
});
