import * as path from "path";

import { newError } from "../../src/app/errors";
import { TrancorderErrorCode } from "../../src/app";

import { loadTranslations, LoadTranslationsResult } from "../../src/app/translations/load";
import { validateTranslations } from "../../src/app/translations/validate";
import { processTranslations } from "../../src/app/translations";

const projects = path.resolve(__dirname, "../projects");

describe("translations", () => {
	describe("loadTranslations", () => {
		it("invalid json files", async () => {
			const cwd = path.resolve(projects, "locales");
			const results = await loadTranslations(cwd, "invalid", "json");

			expect(results).toEqual({
				cwd,
				format: "json",
				translations: {
					"invalid/cs-CZ.json": {},
				},
				errors: {
					"invalid/en-US.json": [
						newError(TrancorderErrorCode.CanNotReadFile, [
							"invalid/en-US.json",
							"json",
						]),
					],
				},
				err: undefined,
			});
		});

		it("invalid type of files", async () => {
			const cwd = path.resolve(projects, "locales");
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			const results = await loadTranslations(cwd, "invalid", "txt" as any);

			expect(results).toEqual({
				cwd,
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				format: "txt" as any,
				translations: {},
				errors: {
					"invalid/cs-CZ.txt": [
						newError(TrancorderErrorCode.UnknownFileFormat, ["txt", "json"]),
					],
				},
				err: undefined,
			});
		});

		it("valid files", async () => {
			const cwd = path.resolve(projects, "locales");
			const results = await loadTranslations(cwd, "empty", "json");

			expect(results).toEqual({
				cwd,
				format: "json",
				translations: {
					"empty/cs-CZ.json": {},
					"empty/en-US.json": {},
				},
				errors: {},
				err: undefined,
			});
		});
	});

	describe("validateTranslations", () => {
		describe("already invalid", () => {
			let results: LoadTranslationsResult;

			beforeEach(async () => {
				const cwd = path.resolve(projects, "locales");
				results = await loadTranslations(cwd, "invalid", "json");
			});

			it("validate with invalid and invalid default", async () => {
				const res = await validateTranslations(results, "de-DE");
				expect(res).toEqual({
					cwd: results.cwd,
					format: results.format,
					err: newError(TrancorderErrorCode.NoDefaultLanguageFound, ["cs-CZ", "de-DE"]),
					errors: {
						...results.errors,
						"invalid/cs-CZ.json": [
							newError(TrancorderErrorCode.NoDefaultLanguageFound, [
								"cs-CZ",
								"de-DE",
							]),
						],
					},
					translations: res.translations,
					languages: {
						"cs-CZ": "invalid/cs-CZ.json",
					},
				});
			});
		});

		describe("valid", () => {
			let results: LoadTranslationsResult;

			beforeEach(async () => {
				const cwd = path.resolve(projects, "locales");
				results = await loadTranslations(cwd, "empty", "json");
			});

			it("default language is not found", async () => {
				const res = await validateTranslations(results, "de-DE");
				expect(res).toEqual({
					cwd: results.cwd,
					format: results.format,
					err: newError(TrancorderErrorCode.NoDefaultLanguageFound, [
						"cs-CZ, en-US",
						"de-DE",
					]),
					errors: {
						"empty/cs-CZ.json": [
							newError(TrancorderErrorCode.NoDefaultLanguageFound, [
								"cs-CZ, en-US",
								"de-DE",
							]),
						],
						"empty/en-US.json": [
							newError(TrancorderErrorCode.NoDefaultLanguageFound, [
								"cs-CZ, en-US",
								"de-DE",
							]),
						],
					},
					translations: res.translations,
					languages: {
						"cs-CZ": "empty/cs-CZ.json",
						"en-US": "empty/en-US.json",
					},
				});
			});

			it("default language is found, but not match schema", async () => {
				const res = await validateTranslations(results, "cs-CZ");
				expect(res).toEqual({
					cwd: results.cwd,
					format: results.format,
					err: undefined,
					errors: {
						"empty/cs-CZ.json": [
							newError(TrancorderErrorCode.SchemaError, [
								"instance",
								"minProperties",
								"does not meet minimum property length of 1",
							]),
						],
						"empty/en-US.json": [
							newError(TrancorderErrorCode.DefaultLanguageSchemaError, [
								"cs-CZ",
								"cs-CZ, en-US",
							]),
						],
					},
					translations: res.translations,
					languages: {
						"cs-CZ": "empty/cs-CZ.json",
						"en-US": "empty/en-US.json",
					},
				});
			});

			it("default language is found, default match schema, but not others", async () => {
				results.translations["empty/cs-CZ.json"] = { key: "Title" };

				const res = await validateTranslations(results, "cs-CZ");
				expect(res).toEqual({
					cwd: results.cwd,
					format: results.format,
					err: undefined,
					errors: {
						"empty/en-US.json": [
							newError(TrancorderErrorCode.SchemaError, [
								"instance",
								"minProperties",
								"does not meet minimum property length of 1",
							]),
						],
					},
					translations: res.translations,
					languages: {
						"cs-CZ": "empty/cs-CZ.json",
						"en-US": "empty/en-US.json",
					},
				});
			});

			it("default language is found, all match schema, extra key", async () => {
				results.translations["empty/cs-CZ.json"] = {
					title: "Titulek",
				};
				results.translations["empty/en-US.json"] = {
					title: "Title",
					footer: "Footer",
				};

				const res = await validateTranslations(results, "cs-CZ");
				expect(res).toEqual({
					cwd: results.cwd,
					format: results.format,
					err: undefined,
					errors: {
						"empty/en-US.json": [
							newError(TrancorderErrorCode.ExtraKeyInLanguage, [
								"en-US",
								"cs-CZ",
								"footer",
							]),
						],
					},
					translations: res.translations,
					languages: {
						"cs-CZ": "empty/cs-CZ.json",
						"en-US": "empty/en-US.json",
					},
				});
			});

			it("default language is found, all match schema, missing key", async () => {
				results.translations["empty/cs-CZ.json"] = {
					title: "Titulek",
					footer: "Zápatí",
				};
				results.translations["empty/en-US.json"] = {
					title: "Title",
				};

				const res = await validateTranslations(results, "cs-CZ");
				expect(res).toEqual({
					cwd: results.cwd,
					format: results.format,
					err: undefined,
					errors: {
						"empty/en-US.json": [
							newError(TrancorderErrorCode.MissingKeyFromDefaultLanguage, [
								"en-US",
								"cs-CZ",
								"footer",
							]),
						],
					},
					translations: res.translations,
					languages: {
						"cs-CZ": "empty/cs-CZ.json",
						"en-US": "empty/en-US.json",
					},
				});
			});
		});
	});

	it("processTranslations", async () => {
		const cwd = path.resolve(projects, "locales");
		const results = await processTranslations({
			cwd,
			format: "json",
			translations: "empty",
			sources: [],
			defaultLanguage: "cs-CZ",
			debug: false,
			tsCompilerOptions: {},
			tsConfigFilePath: "",
		});

		expect(results).toEqual({
			cwd: results.cwd,
			format: results.format,
			err: undefined,
			errors: {
				"empty/cs-CZ.json": [
					newError(TrancorderErrorCode.SchemaError, [
						"instance",
						"minProperties",
						"does not meet minimum property length of 1",
					]),
				],
				"empty/en-US.json": [
					newError(TrancorderErrorCode.DefaultLanguageSchemaError, [
						"cs-CZ",
						"cs-CZ, en-US",
					]),
				],
			},
			translations: {},
			languages: {
				"cs-CZ": "empty/cs-CZ.json",
				"en-US": "empty/en-US.json",
			},
		});
	});
});
