import { Project } from "ts-morph";

import {
	TrancorderEnumComplexUsageResult,
	TrancorderEnumSimpleUsageResult,
	trancorderSetupSearch,
	TrancorderSimpleUsageResult,
	TrancorderUsageResult,
} from "../../src/app/ast";
import { tsCompilerOptions } from "../utils";

import {
	importDestructingLocal,
	importDestructingWithAliasLocal,
	importNamespaceLocal,
	requireDestructingLocal,
	requireNamespaceLocal,
	importDestructingRemote,
	importDestructingWithAliasRemote,
	importNamespaceRemote,
	requireNamespaceRemote,
	requireDestructingRemote,
	importDestructingLocalAndRemote,
	invalidTs,
	usageWithTextOnly,
	usageWithTextAndValues,
	usageWithTextAndValuesAndOptions,
	usageWithEnumAsEnumAndText,
	usageWithEnumAsObjectAndText,
	usageWithEnumAsEnumAndComplex,
	usageWithEnumAsObjectAndComplex,
	usageWithEnumAsObjectAndComplexNumbers,
} from "./examples";

describe("ast", () => {
	describe("trancorderSetupSearch", () => {
		let program: Project;

		function checkSimpleUsage(
			usage: TrancorderUsageResult,
			key: string,
			values: object,
			options = false
		) {
			expect(usage.type).toBe("simple");

			const us = usage as TrancorderSimpleUsageResult;
			expect(us.key).toBe(key);
			expect(us.values).toEqual(values);
			expect(us.hasOptions).toBe(options);
		}

		function checkEnumSimpleUsage(
			usage: TrancorderUsageResult,
			key: string,
			enumObj: object,
			values: object,
			options = false
		) {
			expect(usage.type).toBe("enum-simple");

			const us = usage as TrancorderEnumSimpleUsageResult;
			expect(us.key).toBe(key);
			expect(us.enumObj).toEqual(enumObj);
			expect(us.values).toEqual(values);
			expect(us.hasOptions).toBe(options);
		}

		function checkEnumComplexUsage(
			usage: TrancorderUsageResult,
			keysObj: Record<string | number, string>,
			enumObj: object,
			values: object,
			options = false
		) {
			expect(usage.type).toBe("enum-complex");

			const us = usage as TrancorderEnumComplexUsageResult;
			expect(us.keysObj).toEqual(keysObj);
			expect(us.enumObj).toEqual(enumObj);
			expect(us.values).toEqual(values);
			expect(us.hasOptions).toBe(options);
		}

		beforeEach(() => {
			program = new Project({
				skipAddingFilesFromTsConfig: true,
				compilerOptions: tsCompilerOptions,
			});
		});

		it("destructuring import with localTrancorder", () => {
			const file = program.createSourceFile("/unknown.ts", importDestructingLocal);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("destructuring import with localTrancorder as tranc", () => {
			const file = program.createSourceFile("/unknown.ts", importDestructingWithAliasLocal);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("import as * for local", () => {
			const file = program.createSourceFile("/unknown.ts", importNamespaceLocal);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("destructuring require with localTrancorder", () => {
			const file = program.createSourceFile("/unknown.ts", requireDestructingLocal);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("require as namespace for local", () => {
			const file = program.createSourceFile("/unknown.ts", requireNamespaceLocal);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("destructuring import with remoteTrancorder", () => {
			const file = program.createSourceFile("/unknown.ts", importDestructingRemote);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("remote");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("destructuring import with remoteTrancorder as tranc", () => {
			const file = program.createSourceFile("/unknown.ts", importDestructingWithAliasRemote);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("remote");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("import as * for remote", () => {
			const file = program.createSourceFile("/unknown.ts", importNamespaceRemote);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("remote");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("destructuring require with remoteTrancorder", () => {
			const file = program.createSourceFile("/unknown.ts", requireDestructingRemote);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("remote");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("require as namespace for remote", () => {
			const file = program.createSourceFile("/unknown.ts", requireNamespaceRemote);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(1);

			expect(results[0].type).toBe("remote");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(0);
		});

		it("local and remote at once", () => {
			const file = program.createSourceFile("/unknown.ts", importDestructingLocalAndRemote);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(2);

			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/unknown.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("lt");
			expect(results[0].usages.length).toBe(0);

			expect(results[1].type).toBe("remote");
			expect(results[1].file.getFilePath()).toBe("/unknown.ts");
			expect(results[1].exports.length).toBe(1);
			expect(results[1].exports[0].getText()).toBe("rt");
			expect(results[1].usages.length).toBe(0);
		});

		it("invalid ts file", () => {
			const file = program.createSourceFile("/unknown.ts", invalidTs);
			const results = trancorderSetupSearch("", program, [file]);
			expect(results.length).toBe(0);
		});

		it("destructing import with text usage", () => {
			const file1 = program.createSourceFile("/setup.ts", importDestructingLocal);
			const file2 = program.createSourceFile("/usage.ts", usageWithTextOnly);

			const results = trancorderSetupSearch("", program, [file1, file2]);
			expect(results.length).toBe(1);
			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/setup.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(1);

			checkSimpleUsage(results[0].usages[0], "This is simple message", {});
		});

		it("import namespace with text usage", () => {
			const file1 = program.createSourceFile("/setup.ts", importNamespaceLocal);
			const file2 = program.createSourceFile("/usage.ts", usageWithTextOnly);

			const results = trancorderSetupSearch("", program, [file1, file2]);
			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/setup.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(1);

			checkSimpleUsage(results[0].usages[0], "This is simple message", {});
		});

		it("import namespace with text usage with values", () => {
			const file1 = program.createSourceFile("/setup.ts", importNamespaceLocal);
			const file2 = program.createSourceFile("/usage.ts", usageWithTextAndValues);

			const results = trancorderSetupSearch("", program, [file1, file2]);
			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/setup.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(1);

			checkSimpleUsage(results[0].usages[0], "Hello {name}! How are you?", {
				lastname: "<dynamic>",
				name: "<dynamic>",
			});
		});

		it("import namespace with text usage with values and options", () => {
			const file1 = program.createSourceFile("/setup.ts", importNamespaceLocal);
			const file2 = program.createSourceFile("/usage.ts", usageWithTextAndValuesAndOptions);

			const results = trancorderSetupSearch("", program, [file1, file2]);
			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/setup.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(1);

			checkSimpleUsage(
				results[0].usages[0],
				"Hello {name}! How are you?",
				{
					name: "Stanley",
					info: {
						age: 22,
						text: true,
					},
					data: [1, { test: "test" }],
				},
				true
			);
		});

		it("import namespace with enum usage with text", () => {
			const file1 = program.createSourceFile("/setup.ts", importNamespaceLocal);
			const file2 = program.createSourceFile("/usage.ts", usageWithEnumAsEnumAndText);

			const results = trancorderSetupSearch("", program, [file1, file2]);
			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/setup.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(1);

			checkEnumSimpleUsage(
				results[0].usages[0],
				"Hello {this}! How are you?",
				{
					Variable: "variable",
					Text: "text",
				},
				{}
			);
		});

		it("import namespace with object usage with text", () => {
			const file1 = program.createSourceFile("/setup.ts", importNamespaceLocal);
			const file2 = program.createSourceFile("/usage.ts", usageWithEnumAsObjectAndText);

			const results = trancorderSetupSearch("", program, [file1, file2]);
			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/setup.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(1);
		});

		it("import namespace with enum usage with complex texts", () => {
			const file1 = program.createSourceFile("/setup.ts", importNamespaceLocal);
			const file2 = program.createSourceFile("/usage.ts", usageWithEnumAsEnumAndComplex);

			const results = trancorderSetupSearch("", program, [file1, file2]);
			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/setup.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(1);

			checkEnumComplexUsage(
				results[0].usages[0],
				{
					variable: "This is variable.",
					text: "This is text.",
				},
				{
					Variable: "variable",
					Text: "text",
				},
				{}
			);
		});

		it("import namespace with object usage with complex texts", () => {
			const file1 = program.createSourceFile("/setup.ts", importNamespaceLocal);
			const file2 = program.createSourceFile("/usage.ts", usageWithEnumAsObjectAndComplex);

			const results = trancorderSetupSearch("", program, [file1, file2]);
			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/setup.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(1);

			checkEnumComplexUsage(
				results[0].usages[0],
				{
					variable: "This is variable.",
					text: "This is text.",
				},
				{
					Variable: "variable",
					Text: "text",
				},
				{}
			);
		});

		it("import namespace with object usage with complex texts with numbers enum", () => {
			const file1 = program.createSourceFile("/setup.ts", importNamespaceLocal);
			const file2 = program.createSourceFile(
				"/usage.ts",
				usageWithEnumAsObjectAndComplexNumbers
			);

			const results = trancorderSetupSearch("", program, [file1, file2]);
			expect(results[0].type).toBe("local");
			expect(results[0].file.getFilePath()).toBe("/setup.ts");
			expect(results[0].exports.length).toBe(1);
			expect(results[0].exports[0].getText()).toBe("t");
			expect(results[0].usages.length).toBe(1);

			checkEnumComplexUsage(
				results[0].usages[0],
				{
					1: "This is variable.",
					2: "This is text.",
				},
				{
					Variable: 1,
					Text: 2,
				},
				{}
			);
		});
	});
});
