import { createFiles } from "../../src/app/generators/create";
import { newError } from "../../src/app/errors";

import { mockFilesystem, options, setup, validateProjectWithMocks } from "../utils";
import { TrancorderErrorCode } from "../../src/app";

describe("create languages", () => {
	let setupRes: Awaited<ReturnType<typeof setup>>;

	const defaultFile = {
		"Hello, my name is {name} {surname}.": "Hello, my name is {name} {surname}.",
		"Hello, my name is {user.name} {user.surname}. My email is: {info.email}":
			"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
		"Hello, my {this} name is {name}.": "Hello, my {this} name is {name}.",
		"This is a string with name {name}.": "This is a string with name {name}.",
		"This is a variable with name {name}.": "This is a variable with name {name}.",
	};

	beforeAll(async () => {
		setupRes = await setup("en-US");
	});

	it("create files for default language", async () => {
		const results = await validateProjectWithMocks(setupRes);
		const { store } = mockFilesystem(results.cwd);

		const createResults = await createFiles(results, {
			...options,
			defaultLanguage: "en-US",
			languages: [],
		});

		expect(createResults).toEqual({
			defaultLanguage: "en-US",
			list: ["en-US"],
			paths: ["locales/simple/en-US.json"],
			languages: {
				"en-US": "locales/simple/en-US.json",
			},
			errors: {},
			translations: {
				"locales/simple/en-US.json": defaultFile,
			},
		});

		expect(JSON.parse(store["locales/simple/en-US.json"])).toEqual(defaultFile);
	});

	it("create files for default language and more languages", async () => {
		const results = await validateProjectWithMocks(setupRes);
		const { store } = mockFilesystem(results.cwd);

		const createResults = await createFiles(results, {
			...options,
			defaultLanguage: "en-US",
			languages: ["cs-CZ", "de-DE"],
		});

		expect(createResults).toEqual({
			defaultLanguage: "en-US",
			list: ["en-US", "cs-CZ", "de-DE"],
			paths: [
				"locales/simple/en-US.json",
				"locales/simple/cs-CZ.json",
				"locales/simple/de-DE.json",
			],
			languages: {
				"en-US": "locales/simple/en-US.json",
				"de-DE": "locales/simple/de-DE.json",
				"cs-CZ": "locales/simple/cs-CZ.json",
			},
			errors: {},
			translations: {
				"locales/simple/en-US.json": defaultFile,
				"locales/simple/de-DE.json": defaultFile,
				"locales/simple/cs-CZ.json": defaultFile,
			},
		});

		expect(JSON.parse(store["locales/simple/en-US.json"])).toEqual(defaultFile);
		expect(JSON.parse(store["locales/simple/de-DE.json"])).toEqual(defaultFile);
		expect(JSON.parse(store["locales/simple/cs-CZ.json"])).toEqual(defaultFile);
	});

	it("create files for default language and more languages with error", async () => {
		const results = await validateProjectWithMocks(setupRes);
		const { store } = mockFilesystem(results.cwd, ["locales/simple/cs-CZ.json"]);

		const createResults = await createFiles(results, {
			...options,
			defaultLanguage: "en-US",
			languages: ["cs-CZ", "de-DE"],
		});

		expect(createResults).toEqual({
			defaultLanguage: "en-US",
			list: ["en-US", "cs-CZ", "de-DE"],
			paths: ["locales/simple/en-US.json", "locales/simple/de-DE.json"],
			languages: {
				"en-US": "locales/simple/en-US.json",
				"de-DE": "locales/simple/de-DE.json",
				"cs-CZ": "locales/simple/cs-CZ.json",
			},
			errors: {
				"locales/simple/cs-CZ.json": [
					newError(TrancorderErrorCode.CanNotWriteLocaleFile, [
						"locales/simple/cs-CZ.json",
					]),
				],
			},
			translations: {
				"locales/simple/en-US.json": defaultFile,
				"locales/simple/de-DE.json": defaultFile,
			},
		});

		expect(JSON.parse(store["locales/simple/en-US.json"])).toEqual(defaultFile);
		expect(JSON.parse(store["locales/simple/de-DE.json"])).toEqual(defaultFile);
		expect(store["locales/simple/cs-CZ.json"]).toEqual(undefined);
	});
});
