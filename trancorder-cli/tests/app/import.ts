import { LanguageTranslations } from "trancorder";

import { importXliffFiles } from "../../src/app/exporters/xliff";
import { TrancorderError, TrancorderErrorCode, ValidateProjectResults } from "../../src";
import { ImportedFiles } from "../../src/app/exporters/types";
import { newError } from "../../src/app/errors/index";

const mockResults: ValidateProjectResults = {
	translations: {},
	languages: {},
	setup: null,
	used: [],
	errors: [],
	problems: {},
	format: "json",
	sources: [],
	cwd: "",
};

function buildResults(data: Record<string, LanguageTranslations>) {
	const languages = Object.keys(data);
	return {
		...mockResults,
		translations: languages.reduce((prev, lang) => {
			prev[`locales/${lang}.json`] = data[lang];
			return prev;
		}, {}),
		languages: languages.reduce((prev, lang) => {
			prev[lang] = `locales/${lang}.json`;
			return prev;
		}, {}),
	};
}

function buildFiles(data: Record<string, string>, errors: TrancorderError[] = []): ImportedFiles {
	return {
		files: Object.keys(data).map((path) => ({
			file: path,
			content: data[path],
		})),
		errors,
	};
}

function xmlContent(file: string, content: string[]) {
	return `<?xml version="1.0" encoding="UTF-8"?><xliff version="2.0" xmlns="urn:oasis:names:tc:xliff:document:2.0" srcLang="en-US" trgLang="cs-CZ"><file id="id" original="${file}">${content.join(
		""
	)}</file></xliff>`;
}

describe("imports", () => {
	describe("xliff", () => {
		it("simple translation key", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": "Hello world!",
				},
				"cs-CZ": {
					"Hello world!": "",
				},
			});
			const files = buildFiles({
				"/absolute/path/to/locales/cs-CZ.json": xmlContent("locales/cs-CZ.json", [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source><target>Ahoj světe!</target></segment></unit></group>`,
				]),
			});

			const data = importXliffFiles(results, files);
			expect(data.errors.length).toBe(0);
			expect(data.imports.length).toBe(1);
			expect(data.imports[0]).toEqual({
				errors: [],
				file: "locales/cs-CZ.json",
				source: "/absolute/path/to/locales/cs-CZ.json",
				language: "cs-CZ",
				updated: ["Hello world!"],
				translation: {
					"Hello world!": "Ahoj světe!",
				},
			});
		});

		it("simple translation key in alternative", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": {
						value: "Hello world!",
						maximumCharacters: 100,
						alternatives: {
							"{formal} = true": {
								value: "Good day mr world!",
								maximumCharacters: 100,
							},
						},
					},
				},
				"cs-CZ": {
					"Hello world!": {
						value: "",
						maximumCharacters: 100,
						alternatives: {
							"{formal} = true": {
								value: "",
								maximumCharacters: 100,
							},
						},
					},
				},
			});
			const files = buildFiles({
				"/absolute/path/to/locales/cs-CZ.json": xmlContent("locales/cs-CZ.json", [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source><target>Ahoj světe!</target></segment></unit><unit id="{formal} = true"><segment><source>Good day mr world!</source><target>Dobrý den pane světe!</target></segment></unit></group>`,
				]),
			});

			const data = importXliffFiles(results, files);
			expect(data.errors.length).toBe(0);
			expect(data.imports.length).toBe(1);
			expect(data.imports[0]).toEqual({
				errors: [],
				file: "locales/cs-CZ.json",
				source: "/absolute/path/to/locales/cs-CZ.json",
				language: "cs-CZ",
				updated: ["Hello world!"],
				translation: {
					"Hello world!": {
						value: "Ahoj světe!",
						maximumCharacters: 100,
						alternatives: {
							"{formal} = true": {
								value: "Dobrý den pane světe!",
								maximumCharacters: 100,
							},
						},
					},
				},
			});
		});

		it("translation key with variables", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": "Hello world!",
				},
				"cs-CZ": {
					"Hello world!": "",
				},
			});
			const files = buildFiles({
				"/absolute/path/to/locales/cs-CZ.json": xmlContent("locales/cs-CZ.json", [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source><target>Ahoj <ph id="0" disp="{name}"/> světe!</target></segment></unit></group>`,
				]),
			});

			const data = importXliffFiles(results, files);
			expect(data.errors.length).toBe(0);
			expect(data.imports.length).toBe(1);
			expect(data.imports[0]).toEqual({
				errors: [],
				file: "locales/cs-CZ.json",
				source: "/absolute/path/to/locales/cs-CZ.json",
				language: "cs-CZ",
				updated: ["Hello world!"],
				translation: {
					"Hello world!": "Ahoj {name} světe!",
				},
			});
		});

		it("translation key with more variables", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": "Hello world!",
				},
				"cs-CZ": {
					"Hello world!": "",
				},
			});
			const files = buildFiles({
				"/absolute/path/to/locales/cs-CZ.json": xmlContent("locales/cs-CZ.json", [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source><target>Ahoj <ph id="0" disp="{name}"/> muj <ph id="1" disp="{surname}"/> světe!</target></segment></unit></group>`,
				]),
			});

			const data = importXliffFiles(results, files);
			expect(data.errors.length).toBe(0);
			expect(data.imports.length).toBe(1);
			expect(data.imports[0]).toEqual({
				errors: [],
				file: "locales/cs-CZ.json",
				source: "/absolute/path/to/locales/cs-CZ.json",
				language: "cs-CZ",
				updated: ["Hello world!"],
				translation: {
					"Hello world!": "Ahoj {name} muj {surname} světe!",
				},
			});
		});

		it("translation key with more variables 2", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": "Hello world!",
				},
				"cs-CZ": {
					"Hello world!": "",
				},
			});
			const files = buildFiles({
				"/absolute/path/to/locales/cs-CZ.json": xmlContent("locales/cs-CZ.json", [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source><target>Ahoj <ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/> světe!</target></segment></unit></group>`,
				]),
			});

			const data = importXliffFiles(results, files);
			expect(data.errors.length).toBe(0);
			expect(data.imports.length).toBe(1);
			expect(data.imports[0]).toEqual({
				errors: [],
				file: "locales/cs-CZ.json",
				source: "/absolute/path/to/locales/cs-CZ.json",
				language: "cs-CZ",
				updated: ["Hello world!"],
				translation: {
					"Hello world!": "Ahoj {name} {surname} světe!",
				},
			});
		});

		it("keep additional keys", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": "Hello world!",
				},
				"cs-CZ": {
					"Hello world!": "",
					Friend: "Kamarád",
				},
			});
			const files = buildFiles({
				"/absolute/path/to/locales/cs-CZ.json": xmlContent("locales/cs-CZ.json", [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source><target>Ahoj světe!</target></segment></unit></group>`,
				]),
			});

			const data = importXliffFiles(results, files);
			expect(data.errors.length).toBe(0);
			expect(data.imports.length).toBe(1);
			expect(data.imports[0]).toEqual({
				errors: [],
				file: "locales/cs-CZ.json",
				source: "/absolute/path/to/locales/cs-CZ.json",
				language: "cs-CZ",
				updated: ["Hello world!"],
				translation: {
					"Hello world!": "Ahoj světe!",
					Friend: "Kamarád",
				},
			});
		});

		it("report non existing key", async () => {
			const results = buildResults({
				"en-US": {
					Friend: "Friend",
				},
				"cs-CZ": {
					Friend: "Kamarád",
				},
			});
			const files = buildFiles({
				"/absolute/path/to/locales/cs-CZ.json": xmlContent("locales/cs-CZ.json", [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source><target>Ahoj světe!</target></segment></unit></group>`,
				]),
			});

			const data = importXliffFiles(results, files);
			expect(data.errors.length).toBe(0);
			expect(data.imports.length).toBe(1);
			expect(data.imports[0]).toEqual({
				errors: [
					newError(TrancorderErrorCode.TargetTranslationKeyNotFound, [
						"Hello world!",
						"locales/cs-CZ.json",
					]),
				],
				file: "locales/cs-CZ.json",
				source: "/absolute/path/to/locales/cs-CZ.json",
				language: "cs-CZ",
				updated: [],
				translation: {
					Friend: "Kamarád",
				},
			});
		});

		it("report non existing translation", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": "Hello world!",
				},
				"fr-FR": {
					"Hello world!": "",
				},
			});
			const files = buildFiles({
				"/absolute/path/to/locales/cs-CZ.json": xmlContent("locales/cs-CZ.json", [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source><target>Ahoj světe!</target></segment></unit></group>`,
				]),
			});

			const data = importXliffFiles(results, files);
			expect(data.errors.length).toBe(0);
			expect(data.imports.length).toBe(1);
			expect(data.imports[0]).toEqual({
				errors: [
					newError(TrancorderErrorCode.TargetTranslationNotFound, ["locales/cs-CZ.json"]),
				],
				file: "locales/cs-CZ.json",
				source: "/absolute/path/to/locales/cs-CZ.json",
				language: "cs-CZ",
				updated: [],
				translation: {},
			});
		});

		it("simple translation key in alternative, missing target", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": {
						value: "Hello world!",
						maximumCharacters: 100,
						alternatives: {
							"{formal} = true": {
								value: "Good day mr world!",
								maximumCharacters: 100,
							},
						},
					},
				},
				"cs-CZ": {
					"Hello world!": {
						value: "",
						maximumCharacters: 100,
						alternatives: {
							"{formal} = true": {
								value: "",
								maximumCharacters: 100,
							},
						},
					},
				},
			});
			const files = buildFiles({
				"/absolute/path/to/locales/cs-CZ.json": xmlContent("locales/cs-CZ.json", [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source><target>Ahoj světe!</target></segment></unit></group>`,
				]),
			});

			const data = importXliffFiles(results, files);
			expect(data.errors.length).toBe(0);
			expect(data.imports.length).toBe(1);
			expect(data.imports[0]).toEqual({
				errors: [],
				file: "locales/cs-CZ.json",
				source: "/absolute/path/to/locales/cs-CZ.json",
				language: "cs-CZ",
				updated: ["Hello world!"],
				translation: {
					"Hello world!": {
						value: "Ahoj světe!",
						maximumCharacters: 100,
						alternatives: {
							"{formal} = true": {
								value: "",
								maximumCharacters: 100,
							},
						},
					},
				},
			});
		});
	});
});
