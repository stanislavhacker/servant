import { TrancorderErrorCode } from "../../src";
import {
	TrancorderEnumComplexUsageResult,
	TrancorderEnumSimpleUsageResult,
} from "../../src/app/ast";
import { newError, newInfo } from "../../src/app/errors";
import { setup, validateProjectWithMocks } from "../utils";

describe("project", () => {
	let setupRes: Awaited<ReturnType<typeof setup>>;

	beforeAll(async () => {
		setupRes = await setup("en-US");
	});

	it("valid project default", async () => {
		const proj = await validateProjectWithMocks(setupRes);

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
		});
		expect(proj.used).toEqual([
			"Hello, my name is {name} {surname}.",
			"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
			"Hello, my {this} name is {name}.",
			"This is a variable with name {name}.",
			"This is a string with name {name}.",
		]);
	});

	it("missing key in localized default language", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			translations: (def) => {
				delete def["locales/simple/en-US.json"]["Hello, my name is {name} {surname}."];
				return def;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newError(TrancorderErrorCode.NoKeyInTranslationFile, [
					"Hello, my name is {name} {surname}.",
				]),
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
		});
		expect(proj.used).toEqual([
			"Hello, my name is {name} {surname}.",
			"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
			"Hello, my {this} name is {name}.",
			"This is a variable with name {name}.",
			"This is a string with name {name}.",
		]);
	});

	it("not used key key from localized default language", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			usages: (usages) => {
				usages.splice(0, 1);
				return usages;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
			"locales/simple/en-US.json": [
				newError(TrancorderErrorCode.KeyIsNotUsed, ["Hello, my name is {name} {surname}."]),
			],
		});
		expect(proj.used).toEqual([
			"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
			"Hello, my {this} name is {name}.",
			"This is a variable with name {name}.",
			"This is a string with name {name}.",
		]);
	});

	it("empty message in one of record", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			translations: (def) => {
				def["locales/simple/en-US.json"]["Hello, my name is {name} {surname}."] = "";
				return def;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newError(TrancorderErrorCode.EmptyMessageText, [
					"Hello, my name is {name} {surname}.",
				]),
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
		});
	});

	it("empty message in one of record alternative", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			translations: (def) => {
				def["locales/simple/en-US.json"]["Hello, my name is {name} {surname}."] = {
					value: "Hello, my name is {name} {surname}.",
					alternatives: {
						"{name} = 'Test'": "",
					},
				};
				return def;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newError(TrancorderErrorCode.EmptyAlternativeMessageText, [
					"Hello, my name is {name} {surname}.",
					"{name} = 'Test'",
				]),
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
		});
	});

	it("empty expression in one of record alternative", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			translations: (def) => {
				def["locales/simple/en-US.json"]["Hello, my name is {name} {surname}."] = {
					value: "Hello, my name is {name} {surname}.",
					alternatives: {
						"": "Hello, my name is {name} {surname}.",
					},
				};
				return def;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newError(TrancorderErrorCode.EmptyAlternativeExpression, [
					"Hello, my name is {name} {surname}.",
				]),
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
		});
	});

	it("pointer is not used in message", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			translations: (def) => {
				def["locales/simple/en-US.json"]["Hello, my name is {name} {surname}."] = {
					value: "Hello, my name is {name}.",
				};
				return def;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newError(TrancorderErrorCode.NotUsedValueInMessage, [
					"surname",
					"Hello, my name is {name} {surname}.",
				]),
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
		});
	});

	it("pointer is not in values", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			translations: (def) => {
				def["locales/simple/en-US.json"]["Hello, my name is {name} {surname}."] = {
					value: "Hello, my name is {name} {midname} {surname}.",
				};
				return def;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newError(TrancorderErrorCode.NonExistingPointerInValues, [
					"midname",
					"Hello, my name is {name} {surname}.",
				]),
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
		});
	});

	it("maximum characters length", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			translations: (def) => {
				def["locales/simple/en-US.json"]["Hello, my name is {name} {surname}."] = {
					value: "Hello, my name is {name} {surname}.",
					maximumCharacters: 10,
				};
				return def;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newError(TrancorderErrorCode.MessageIsTooLong, [
					"Hello, my name is {name} {surname}.",
					"35",
					"10",
				]),
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
		});
	});

	it("maximum characters length in alternative", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			translations: (def) => {
				def["locales/simple/en-US.json"]["Hello, my name is {name} {surname}."] = {
					value: "Hello, my name is {name} {surname}.",
					alternatives: {
						"{name} = 'Test'": {
							value: "Hello, my name is {name} {surname}!",
							maximumCharacters: 10,
						},
					},
				};
				return def;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newError(TrancorderErrorCode.MessageAlternativeIsTooLong, [
					"Hello, my name is {name} {surname}.",
					"{name} = 'Test'",
					"35",
					"10",
				]),
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
			],
		});
	});

	it("empty enum object", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			usages: (usages) => {
				usages[2] = {
					...(usages[2] as TrancorderEnumSimpleUsageResult),
					enumObj: {},
				};
				return usages;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
				newError(TrancorderErrorCode.EnumObjectIsEmpty, [
					"Hello, my {this} name is {name}.",
				]),
			],
		});
	});

	it("removed value in object", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			usages: (usages) => {
				usages[3] = {
					...(usages[3] as TrancorderEnumComplexUsageResult),
					enumObj: {
						Variable: 1,
					},
				};
				return usages;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
				newError(TrancorderErrorCode.EnumValueNotExists, ["2"]),
			],
		});
	});

	it("removed value in locales", async () => {
		const proj = await validateProjectWithMocks(setupRes, {
			usages: (usages) => {
				usages[3] = {
					...(usages[3] as TrancorderEnumComplexUsageResult),
					keysObj: {
						1: "This is a variable with name {name}.",
					},
				};
				return usages;
			},
		});

		expect(proj).toBeDefined();
		expect(proj.errors.length).toBe(0);
		expect(proj.problems).toEqual({
			"sources/simple/example.ts": [
				newInfo(TrancorderErrorCode.DynamicValueUsed, [
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
				]),
				newError(TrancorderErrorCode.EnumValueIsNotUsed, ["2", "String"]),
			],
			"locales/simple/en-US.json": [
				newError(TrancorderErrorCode.KeyIsNotUsed, ["This is a string with name {name}."]),
			],
		});
	});
});
