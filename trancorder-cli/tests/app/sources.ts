import * as path from "path";

import { newError } from "../../src/app/errors";
import { TrancorderErrorCode } from "../../src";

import {
	TrancorderEnumComplexUsageResult,
	TrancorderEnumSimpleUsageResult,
	TrancorderSimpleUsageResult,
} from "../../src/app/ast";
import { loadSources, LoadSourcesResult } from "../../src/app/sources/load";
import { validateSources } from "../../src/app/sources/validate";
import { processSources } from "../../src/app/sources";
import { tsCompilerOptions } from "../utils";

const projects = path.resolve(__dirname, "../projects");

describe("sources", () => {
	describe("loadSources", () => {
		it("non existing files", async () => {
			const cwd = path.resolve(projects, "sources");
			const results = await loadSources(cwd, ["empty/example.ts"], { tsCompilerOptions });

			expect(results).toEqual({
				cwd,
				errors: [newError(TrancorderErrorCode.CanNotAddSourceFile, ["empty/example.ts"])],
				sources: ["empty/example.ts"],
				program: jasmine.anything(),
			});
		});

		it("valid existing files", async () => {
			const cwd = path.resolve(projects, "sources");
			const results = await loadSources(cwd, ["simple/**/*.ts"], { tsCompilerOptions });

			expect(results).toEqual({
				cwd,
				errors: [],
				sources: ["simple/example.ts", "simple/setup.ts"],
				program: jasmine.anything(),
			});
		});
	});

	describe("validateSources", () => {
		describe("already invalid", () => {
			let results: LoadSourcesResult;

			beforeEach(async () => {
				const cwd = path.resolve(projects, "sources");
				results = await loadSources(cwd, ["empty/example.ts"], { tsCompilerOptions });
			});

			it("validate with invalid load results", async () => {
				const res = await validateSources(results);

				expect(res).toEqual({
					cwd: results.cwd,
					errors: [
						...results.errors,
						newError(TrancorderErrorCode.NoTrancorderSetupFound, []),
					],
					sources: ["empty/example.ts"],
					setup: null,
				});
			});
		});

		describe("valid", () => {
			it("validate all types of usage with local setup", async () => {
				const cwd = path.resolve(projects, "sources");
				const results = await loadSources(cwd, ["simple/**/*.ts"], { tsCompilerOptions });
				const res = await validateSources(results);

				expect(res).toEqual({
					cwd: results.cwd,
					errors: [],
					sources: ["simple/example.ts", "simple/setup.ts"],
					setup: jasmine.anything(),
				});

				expect(res.setup?.exports.length).toBe(1);
				expect(res.setup?.usages.length).toBe(4);
				expect(res.setup?.path).toBe("simple/setup.ts");
				expect(res.setup?.type).toBe("local");
				expect(res.setup?.file).toEqual(jasmine.anything());

				const usage1 = res.setup?.usages[0] as TrancorderSimpleUsageResult;
				expect(usage1?.type).toBe("simple");
				expect(usage1?.key).toBe("Hello, my name is {name} {surname}.");
				expect(usage1?.values).toEqual({
					name: "Standa",
					surname: "Hacker",
				});
				expect(usage1?.hasOptions).toBe(true);
				expect(usage1?.file).toBe("simple/example.ts");
				expect(usage1?.start).toEqual({ line: 7, column: 9 });
				expect(usage1?.end).toEqual({ line: 7, column: 92 });

				const usage2 = res.setup?.usages[1] as TrancorderSimpleUsageResult;
				expect(usage2?.type).toBe("simple");
				expect(usage2?.key).toBe(
					"Hello, my name is {user.name} {user.surname}. My email is: {info.email}"
				);
				expect(usage2?.values).toEqual({
					user: { name: "Standa", surname: "Hacker" },
					info: "<dynamic>",
				});
				expect(usage2?.hasOptions).toBe(false);
				expect(usage2?.file).toBe("simple/example.ts");
				expect(usage2?.start).toEqual({ line: 14, column: 9 });
				expect(usage2?.end).toEqual({ line: 20, column: 5 });

				const usage3 = res.setup?.usages[2] as TrancorderEnumSimpleUsageResult;
				expect(usage3?.type).toBe("enum-simple");
				expect(usage3?.key).toBe("Hello, my {this} name is {name}.");
				expect(usage3?.enumObj).toEqual({
					Variable: 1,
					String: 2,
				});
				expect(usage3?.values).toEqual({
					enumValue: 1,
					name: "Standa",
				});
				expect(usage3?.hasOptions).toBe(true);
				expect(usage3?.file).toBe("simple/example.ts");
				expect(usage3?.start).toEqual({ line: 33, column: 9 });
				expect(usage3?.end).toEqual({ line: 33, column: 90 });

				const usage4 = res.setup?.usages[3] as TrancorderEnumComplexUsageResult;
				expect(usage4?.type).toBe("enum-complex");
				expect(usage4?.keysObj).toEqual({
					1: "This is a variable with name {name}.",
					2: "This is a string with name {name}.",
				});
				expect(usage3?.enumObj).toEqual({
					Variable: 1,
					String: 2,
				});
				expect(usage4?.values).toEqual({
					enumValue: 1,
					name: "Standa",
				});
				expect(usage4?.hasOptions).toBe(true);
				expect(usage4?.file).toBe("simple/example.ts");
				expect(usage4?.start).toEqual({ line: 46, column: 9 });
				expect(usage4?.end).toEqual({ line: 54, column: 4 });
			});
		});
	});

	it("processSources", async () => {
		const cwd = path.resolve(projects, "sources");
		const sources = await processSources({
			cwd,
			sources: ["simple/**/*.ts"],
			tsCompilerOptions,
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			tsConfigFilePath: undefined as any,
			//not used
			format: "json",
			translations: "",
			defaultLanguage: "en-US",
			debug: false,
		});

		expect(sources).toEqual({
			cwd,
			setup: jasmine.anything(),
			sources: ["simple/example.ts", "simple/setup.ts"],
			errors: [],
		});
	});
});
