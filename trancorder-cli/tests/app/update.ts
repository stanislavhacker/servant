import { updateFiles } from "../../src/app/generators/update";

import { mockFilesystem, options, setup, validateProjectWithMocks } from "../utils";

describe("update languages", () => {
	let setupRes: Awaited<ReturnType<typeof setup>>;

	const defaultFile = {
		"Hello, my name is {name} {surname}.": "Hello, my name is {name} {surname}.",
		"Hello, my name is {user.name} {user.surname}. My email is: {info.email}":
			"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
		"Hello, my {this} name is {name}.": "Hello, my {this} name is {name}.",
		"This is a string with name {name}.": "This is a string with name {name}.",
		"This is a variable with name {name}.": "This is a variable with name {name}.",
	};

	const removedLastUsageFile = {
		"Hello, my name is {name} {surname}.": "Hello, my name is {name} {surname}.",
		"Hello, my name is {user.name} {user.surname}. My email is: {info.email}":
			"Hello, my name is {user.name} {user.surname}. My email is: {info.email}",
		"Hello, my {this} name is {name}.": "Hello, my {this} name is {name}.",
	};

	beforeAll(async () => {
		setupRes = await setup("en-US");
	});

	it("update files for default language, no changes at all", async () => {
		const results = await validateProjectWithMocks(setupRes);
		const { store } = mockFilesystem(results.cwd);

		const createResults = await updateFiles(results, {
			...options,
			defaultLanguage: "en-US",
			languages: [],
		});

		expect(createResults).toEqual({
			defaultLanguage: "en-US",
			list: ["en-US"],
			paths: ["locales/simple/en-US.json"],
			languages: {
				"en-US": "locales/simple/en-US.json",
			},
			errors: {},
			translations: {
				"locales/simple/en-US.json": defaultFile,
			},
			changes: {
				"locales/simple/en-US.json": {
					added: {},
					removed: {},
				},
			},
		});

		expect(JSON.parse(store["locales/simple/en-US.json"])).toEqual(defaultFile);
	});

	it("update files for default language, missing key in file", async () => {
		const results = await validateProjectWithMocks(setupRes, {
			translations: (def) => {
				delete def["locales/simple/en-US.json"]["Hello, my name is {name} {surname}."];
				return def;
			},
		});
		const { store } = mockFilesystem(results.cwd);

		const createResults = await updateFiles(results, {
			...options,
			defaultLanguage: "en-US",
			languages: [],
		});

		expect(createResults).toEqual({
			defaultLanguage: "en-US",
			list: ["en-US"],
			paths: ["locales/simple/en-US.json"],
			languages: {
				"en-US": "locales/simple/en-US.json",
			},
			errors: {},
			translations: {
				"locales/simple/en-US.json": defaultFile,
			},
			changes: {
				"locales/simple/en-US.json": {
					added: {
						"Hello, my name is {name} {surname}.":
							"Hello, my name is {name} {surname}.",
					},
					removed: {},
				},
			},
		});

		expect(JSON.parse(store["locales/simple/en-US.json"])).toEqual(defaultFile);
	});

	it("update files for default language, missing key in file", async () => {
		const results = await validateProjectWithMocks(setupRes, {
			usages: (usages) => {
				usages.pop();
				return usages;
			},
		});
		const { store } = mockFilesystem(results.cwd);

		const createResults = await updateFiles(results, {
			...options,
			defaultLanguage: "en-US",
			languages: [],
		});

		expect(createResults).toEqual({
			defaultLanguage: "en-US",
			list: ["en-US"],
			paths: ["locales/simple/en-US.json"],
			languages: {
				"en-US": "locales/simple/en-US.json",
			},
			errors: {},
			translations: {
				"locales/simple/en-US.json": removedLastUsageFile,
			},
			changes: {
				"locales/simple/en-US.json": {
					added: {},
					removed: {
						"This is a string with name {name}.": "This is a string with name {name}.",
						"This is a variable with name {name}.":
							"This is a variable with name {name}.",
					},
				},
			},
		});

		expect(JSON.parse(store["locales/simple/en-US.json"])).toEqual(removedLastUsageFile);
	});
});
