//SUPPORTED

// language=typescript
export const importDestructingLocal = `import { localTrancorder } from "trancorder";
const { t, setLanguage } = localTrancorder({});
export { t, setLanguage }
`;

// language=typescript
export const importDestructingWithAliasLocal = `import { localTrancorder as tranc } from "trancorder";
const { t } = tranc.localTrancorder({});
export { t };
`;

// language=typescript
export const importNamespaceLocal = `import * as tranc from "trancorder";
const api = tranc.localTrancorder({});
export const t = api.t;
`;

// language=typescript
export const requireDestructingLocal = `const { localTrancorder } = require("trancorder");
const { t, setLanguage } = localTrancorder({});
export { t, setLanguage }
`;

// language=typescript
export const requireNamespaceLocal = `const tranc = require("trancorder");
const api = tranc.localTrancorder({});
export const t = api.t;
`;

// language=typescript
export const importDestructingRemote = `import { remoteTrancorder } from "trancorder";
const { t, setLanguage } = remoteTrancorder({});
export { t, setLanguage }
`;

// language=typescript
export const importDestructingWithAliasRemote = `import { remoteTrancorder as tranc } from "trancorder";
const { t } = tranc.remoteTrancorder({});
export { t };
`;

// language=typescript
export const importNamespaceRemote = `import * as tranc from "trancorder";
const api = tranc.remoteTrancorder({});
export const t = api.t;
`;

// language=javascript
export const requireDestructingRemote = `const { remoteTrancorder } = require("trancorder");
const { t, setLanguage } = remoteTrancorder({});
export { t, setLanguage }
`;

// language=javascript
export const requireNamespaceRemote = `const tranc = require("trancorder");
const api = tranc.remoteTrancorder({});
export const t = api.t;
`;

//UNSUPPORTED

// language=typescript
export const importDestructingLocalAndRemote = `import * as tranc from "trancorder";
const local = tranc.localTrancorder({});
const remote = tranc.remoteTrancorder({});
export const lt = local.t;
export const rt = remote.t;
`;

// language=typescript
export const invalidTs = `import * as from "trancorder";
const local = .localTrancorder({});
const remote = tranc.remoteTrancorder({});
export const lt = local.t;
export const rt = remote.t;
`;

//USAGES

// language=typescript
export const usageWithTextOnly = `import { t } from "./setup";
const message = t("This is simple message");`;

// language=typescript
export const usageWithTextAndValues = `import { t } from "./setup";
const name = "Stanley";
const surname = "Hacker";
const message = t("Hello {name}! How are you?", { name, lastname: surname });`;

// language=typescript
export const usageWithTextAndValuesAndOptions = `import { t } from "./setup";
const message = t("Hello {name}! How are you?", { 
    name: "Stanley", 
    info: { 
        age: 22, 
        text: true,
    },
    data: [1, { test: "test" }]
}, { formats: {} });`;

// language=typescript
export const usageWithEnumAsEnumAndText = `import { t } from "./setup";

enum Type {
    Variable = "variable",
    Text = "text",
}

const message = t(Type, "Hello {this}! How are you?");`;

// language=typescript
export const usageWithEnumAsObjectAndText = `import { t } from "./setup";

const Type = {
    Variable: "variable",
    Text: "text",
}

const message = t(Type, "Hello {this}! How are you?");`;

// language=typescript
export const usageWithEnumAsEnumAndComplex = `import { t } from "./setup";

enum Type {
    Variable = "variable",
    Text = "text",
}

const message = t(Type, {
    [Type.Variable]: "This is variable.",
    [Type.Text]: "This is text.",
});`;

// language=typescript
export const usageWithEnumAsObjectAndComplex = `import { t } from "./setup";

const Type = {
    Variable: "variable",
    Text: "text",
}

const message = t(Type, {
    [Type.Variable]: "This is variable.",
    [Type.Text]: "This is text.",
});`;

// language=typescript
export const usageWithEnumAsObjectAndComplexNumbers = `import { t } from "./setup";

const Type = {
    Variable: 1,
    Text: 2,
}

const message = t(Type, {
    1: "This is variable.",
    [Type.Text]: "This is text.",
});`;
