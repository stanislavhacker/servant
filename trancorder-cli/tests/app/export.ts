import { LanguageTranslations } from "trancorder";

import { exportXliffFiles } from "../../src/app/exporters/xliff";
import { ValidateProjectResults } from "../../src";

const mockResults: ValidateProjectResults = {
	translations: {},
	languages: {},
	setup: null,
	used: [],
	errors: [],
	problems: {},
	format: "json",
	sources: [],
	cwd: "",
};
const options = { defaultLanguage: "en-US" };

function buildResults(data: Record<string, LanguageTranslations>) {
	const languages = Object.keys(data);
	return {
		...mockResults,
		translations: languages.reduce((prev, lang) => {
			prev[`locales/${lang}.json`] = data[lang];
			return prev;
		}, {}),
		languages: languages.reduce((prev, lang) => {
			prev[lang] = `locales/${lang}.json`;
			return prev;
		}, {}),
	};
}

function xmlContent(id: string, content: string[]) {
	return `<?xml version="1.0" encoding="UTF-8"?><xliff version="2.0" xmlns="urn:oasis:names:tc:xliff:document:2.0" srcLang="en-US" trgLang="cs-CZ"><file id="${id}" original="locales/cs-CZ.json">${content.join(
		""
	)}</file></xliff>`;
}

describe("exports", () => {
	describe("xliff", () => {
		it("simple translation key", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": "Hello world!",
				},
				"cs-CZ": {
					"Hello world!": "Hello world!",
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source></segment></unit></group>`,
				])
			);
		});

		it("simple translation key with placeholder", async () => {
			const results = buildResults({
				"en-US": {
					"Hello {name} {surname} how are you?": "Hello {name} {surname} how are you?",
				},
				"cs-CZ": {
					"Hello {name} {surname} how are you?": "Hello {name} {surname} how are you?",
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello {name} {surname} how are you?"><unit id="this"><segment><source>Hello <ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/> how are you?</source></segment></unit></group>`,
				])
			);
		});

		it("simple translation key with more alternatives", async () => {
			const results = buildResults({
				"en-US": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{formal} = true": "Good day sir {name} {surname}.",
							"{informal} = true": "Hi {name} {surname}!",
						},
					},
				},
				"cs-CZ": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{formal} = true": "Good day sir {name} {surname}.",
							"{informal} = true": "Hi {name} {surname}!",
						},
					},
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello {name} {surname} how are you?"><unit id="this"><segment><source>Hello <ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/> how are you?</source></segment></unit><unit id="{formal} = true"><segment><source>Good day sir <ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/>.</source></segment></unit><unit id="{informal} = true"><segment><source>Hi <ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/>!</source></segment></unit></group>`,
				])
			);
		});

		it("simple translation key, base translate = false", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": {
						value: "Hello world!",
						translate: false,
					},
				},
				"cs-CZ": {
					"Hello world!": "Hello world!",
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(xmlContent(data[0].id, []));
		});

		it("simple translation key, base translate = true, lang translate = false", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": {
						value: "Hello world!",
						translate: true,
					},
				},
				"cs-CZ": {
					"Hello world!": {
						value: "Hello world!",
						translate: false,
					},
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(xmlContent(data[0].id, []));
		});

		it("simple translation key, base translate = false, lang translate = true", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": {
						value: "Hello world!",
						translate: false,
					},
				},
				"cs-CZ": {
					"Hello world!": {
						value: "Hello world!",
						translate: true,
					},
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source></segment></unit></group>`,
				])
			);
		});

		it("simple translation key, base translate = false, lang translate = true, empty", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": {
						value: "Hello world!",
						translate: false,
					},
				},
				"cs-CZ": {
					"Hello world!": {
						value: "",
						translate: true,
					},
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source></segment></unit></group>`,
				])
			);
		});

		it("simple translation key, translate if not filled", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": "Hello world!",
				},
				"cs-CZ": {
					"Hello world!": "",
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello world!"><unit id="this"><segment><source>Hello world!</source></segment></unit></group>`,
				])
			);
		});

		it("simple translation key, not translate if unique filled", async () => {
			const results = buildResults({
				"en-US": {
					"Hello world!": "Hello world!",
				},
				"cs-CZ": {
					"Hello world!": "Ahoj světe!",
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(xmlContent(data[0].id, []));
		});

		it("simple translation key with more alternatives, base translate = false", async () => {
			const results = buildResults({
				"en-US": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{name} = true": { value: "{name} {surname}.", translate: false },
						},
					},
				},
				"cs-CZ": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{name} = true": "{name} {surname}.",
						},
					},
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello {name} {surname} how are you?"><unit id="this"><segment><source>Hello <ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/> how are you?</source></segment></unit></group>`,
				])
			);
		});

		it("simple translation key with more alternatives, base translate = true, lang translate = false", async () => {
			const results = buildResults({
				"en-US": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{name} = true": { value: "{name} {surname}." },
						},
					},
				},
				"cs-CZ": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{name} = true": { value: "{name} {surname}.", translate: false },
						},
					},
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello {name} {surname} how are you?"><unit id="this"><segment><source>Hello <ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/> how are you?</source></segment></unit></group>`,
				])
			);
		});

		it("simple translation key with more alternatives, different conditions", async () => {
			const results = buildResults({
				"en-US": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{name} = true": { value: "{name} {surname}." },
						},
					},
				},
				"cs-CZ": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{name} = false": { value: "{name} {surname}." },
						},
					},
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello {name} {surname} how are you?"><unit id="this"><segment><source>Hello <ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/> how are you?</source></segment></unit></group>`,
				])
			);
		});

		it("simple translation key with more alternatives, more conditions", async () => {
			const results = buildResults({
				"en-US": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{name} = true": { value: "{name} {surname}." },
						},
					},
				},
				"cs-CZ": {
					"Hello {name} {surname} how are you?": {
						value: "Hello {name} {surname} how are you?",
						alternatives: {
							"{name} = false": { value: "{name} {surname}." },
							"{name} = true": { value: "{name} {surname}." },
						},
					},
				},
			});

			const data = exportXliffFiles(results, options);
			expect(data.length).toBe(1);
			expect(data[0].file).toEqual("cs-CZ.xliff");
			expect(data[0].content).toBe(
				xmlContent(data[0].id, [
					`<group id="Hello {name} {surname} how are you?"><unit id="this"><segment><source>Hello <ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/> how are you?</source></segment></unit><unit id="{name} = true"><segment><source><ph id="0" disp="{name}"/> <ph id="1" disp="{surname}"/>.</source></segment></unit></group>`,
				])
			);
		});
	});
});
