import { localTrancorder as tranc } from "trancorder";
import * as enUS from "../../locales/empty/en-US.json";
import * as csCZ from "../../locales/empty/cs-CZ.json";

const { t, setLanguage } = tranc({
	languages: {
		"en-US": enUS,
		"cs-CZ": csCZ,
	},
});

export { t, setLanguage };
