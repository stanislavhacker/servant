import { t } from "./setup";

//USE CASE 1: use as t() function

function uc1() {
	return {
		text: t("Hello, my name is {name} {surname}.", { name: "Standa", surname: "Hacker" }, {}),
	};
}

function uc1a() {
	const obj = { email: "" };
	return {
		text: t("Hello, my name is {user.name} {user.surname}. My email is: {info.email}", {
			user: {
				name: "Standa",
				surname: "Hacker",
			},
			info: obj,
		}),
	};
}

//USE CASE 2: use as t() function with enum

function uc2() {
	const Enum = {
		Variable: 1,
		String: 2,
	};

	return {
		text: t(Enum, "Hello, my {this} name is {name}.", { enumValue: 1, name: "Standa" }, {}),
	};
}

//USE CASE 3: use as t() function with enum

function uc3() {
	const Enum = {
		Variable: 1,
		String: 2,
	};

	return {
		text: t(
			Enum,
			{
				1: "This is a variable with name {name}.",
				[Enum.String]: "This is a string with name {name}.",
			},
			{ enumValue: 1, name: "Standa" },
			{}
		),
	};
}

uc1();
uc1a();
uc2();
uc3();
