import { ModuleKind, ScriptTarget } from "ts-morph";
import { LanguageTranslations } from "trancorder";
import { JsxEmit } from "typescript";
import * as path from "path";

import { TrancorderOptions } from "../src/app";
import { processTranslations } from "../src/app/translations";
import { processSources } from "../src/app/sources";
import { validateProject } from "../src/app/project";
import { TrancorderUsageResult } from "../src/app/ast";
import { normalizePath } from "../src/app/io";
import * as io from "../src/app/io";

const projects = path.resolve(__dirname, "./projects");

export const tsCompilerOptions = {
	module: ModuleKind.CommonJS,
	target: ScriptTarget.ES2015,
	sourceMap: false,
	declaration: false,
	jsx: JsxEmit.React,
	skipLibCheck: true,
	strictNullChecks: true,
};

export const options: Required<TrancorderOptions> = {
	cwd: projects,
	sources: ["sources/simple/**/*.ts"],
	translations: "locales/simple",
	format: "json",
	debug: false,
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	tsConfigFilePath: undefined as any,
	tsCompilerOptions,
	defaultLanguage: "",
};

export async function setup(defaultLanguage: string) {
	const opts: Required<TrancorderOptions> = {
		...options,
		defaultLanguage,
	};

	const translationsResults = await processTranslations(opts);
	const sourcesResults = await processSources(opts);

	return {
		options: opts,
		translationsResults,
		sourcesResults,
	};
}

export async function validateProjectWithMocks(
	setupRes: Awaited<ReturnType<typeof setup>>,
	{
		translations,
		defaultLanguage,
		usages,
	}: {
		defaultLanguage?: string;
		translations?: (
			def: Record<string, LanguageTranslations>
		) => Record<string, LanguageTranslations>;
		usages?: (usages: TrancorderUsageResult[]) => TrancorderUsageResult[];
	} = {}
) {
	const { translationsResults, sourcesResults, options } = setupRes;

	const clonedTranslationsResults = JSON.parse(JSON.stringify(translationsResults));

	return await validateProject(
		{
			...clonedTranslationsResults,
			translations: translations
				? translations(clonedTranslationsResults.translations)
				: clonedTranslationsResults.translations,
		},
		{
			...sourcesResults,
			setup: sourcesResults.setup
				? {
						...sourcesResults.setup,
						usages: usages
							? usages(sourcesResults.setup.usages.slice())
							: sourcesResults.setup.usages,
				  }
				: null,
		},
		{
			...options,
			defaultLanguage: defaultLanguage || options.defaultLanguage,
		}
	);
}

export function mockFilesystem(cwd: string, fails: string[] = []) {
	const store = {};

	const writeFile = spyOn(io, "writeFile").and.callFake((filepath, content) => {
		const relative = normalizePath(path.relative(cwd, filepath));
		if (fails.indexOf(relative) >= 0) {
			return Promise.reject(new Error("Some unknown error."));
		}
		store[relative] = content;
		return Promise.resolve();
	});

	return {
		writeFile,
		store,
	};
}
