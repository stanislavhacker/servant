# ![Trancorder][logo] Trancorder CLI

Quick references: **[Client module][2]**

## What is Trancorder CLI?

The Trancorder localisation cli module allows developers to easily validate the translations in their localization files by running a command in the terminal. Module has a modern and colorful command-line interface that makes it easy for developers to use and understand. The interface is designed to be intuitive and user-friendly, with clear and concise instructions. The CLI can detect the type of Trancorder used in the project, and then validate the keys usages in code, missing and additional keys in locales files, as well as used variables and expressions.

The Trancorder cli module can validate the usage of keys in the source code, and compare them to the keys in the localization files to ensure that they match. Additionally, the module can identify any missing or additional keys in the localization files, and validate the variables and expressions used in the translations.

It also can generate new language translations based on the defined languages, by reading the keys from the code base. This allows developers to easily add new translations files without having to manually create it.

Second option is update the current language translation or translations based on the defined languages, by comparing the keys from the code base with the keys already in the localization file. This ensures that the translations are up-to-date and accurate.

It can work with both JavaScript and TypeScript, which makes it easy for developers to use in their projects regardless of the language they are using.

## Installation and start!

#### 1. Globally installed

You need the latest version of nodejs and then run:

> `npm install trancorder-cli -g`

Then you can run in current directory:

> `trancorder validate`

> **Caveats**
>
> Installing globally is not recommended.

#### 2. Locally installed

You need the latest version of nodejs and then run:

> `npm install trancorder-cli`

Then you can run in current directory:

> `"node_modules/.bin/trancorder" validate`

## Command line

Here's a brief description of each of the Trancorder CLI commands:

1. `validate`: This command is used to validate the translations in the localization files against the source code usage. It checks for missing and additional keys in locale files, used variables and expressions, and any key usages in the code. The command can be run with the `trancorder validate` command.

2. `create`: This command is used to generate new language translations or translations based on the defined languages, reading keys from the codebase. The command can be run with the `trancorder create` command.

3. `update`: This command is used to update the current language translations based on the defined languages, comparing keys from the codebase and keys in the localization files. The command can be run with the `trancorder update` command.

4. `export`: This command is used to export all available translations to defined supported format. Now only one supported format is **"xliff"**. The command can be run with the `trancorder export` command.

5. `import`: This command is used to import all translated translations from defined supported format. Now only one supported format is **"xliff"**. The command can be run with the `trancorder import` command.

Each of these commands can be run with various options to configure and customize its behavior. Here's a brief description of the options:

-   `--entry "/path/to/project"`: Specifies the path to the root directory of the project that will be processed by `trancorder`, if omitted, current working directory will be used. _Available for commands **`validate, unpdate, create, import, export`**_
-   `--debug`: Enables debug mode for additional logging and output during the commands process. _Available for commands **`validate, unpdate, create, import, export`**_
-   `--translations "locales"`: Specifies the path to the directory containing the localization files that will be validated and/or updated. _Available for commands **`validate, unpdate, create, import, export`**_
-   `--format "json"`: Specified in which format are saved translations files and also in which format will be translation files generated. For now, only supported format is **json** format. _Available for commands **`validate, unpdate, create, import, export`**_
-   `--exports "xliff"`: Specified in which format are exported translations files and also in which format will be imported translations files. For now, only supported format is standardized **xliff** format. _Available for commands **`validate, unpdate, create, import, export`**_
-   `--config "./tsconfig.json"`: Specifies the path to the TypeScript configuration file for the project. This is required if the project is written in TypeScript and you have this config file on another place than in `entry` folder. _Available for commands **`validate, unpdate, create, import, export`**_
-   `--defaultLanguage "en-US"`: Specifies the language that is used as default and its used as compared language for others supported language. _Available for commands **`validate, unpdate, create, import, export`**_
-   `--sources "src/**/*.{ts,tsx}"`: Specifies the glob pattern for the source files that will be scanned for localization keys. _Available for commands **`validate, unpdate, create, import, export`**_
-   `--languages "cs-CZ,en-DE"`: Specifies the list of languages that `trancorder` should generate or update translations for. _Available for commands **`unpdate, create`**_
-   `--in "/path/to/exported"`: Specifies path to directory from files with given format will be imported and update defined localisation files. _Available for commands **`import`**_
-   `--out "/path/to/exported"`: Specifies path to directory where files with given format will be exported and saved. _Available for commands **`export`**_

Here are some example command line invocations using these options:

#### 1. Validating in entry folder with debug mode as default

This command runs `trancorder` validate runtime on the `~/my-project` directory, enabling debug mode, using the `src/locales` directory for localization json (default) files, and generating/updating translations for TypeScript files in the `src/` directory with the default language `en-US`, and with translations for Czech and French languages.

> `trancorder --entry "~/my-project" --debug --translations "src/locales" --config "./tsconfig.json" --defaultLanguage "en-US" --sources "src/\*_/_.ts"

#### 2. Validating in entry folder specified command

This command runs `trancorder` validate runtime on the current directory, using the `src/locales` directory for localization json files, and updating translations for TypeScript/TSX files in the `src/` directory with the default language `en-US`.

> `trancorder validate json --translations "src/locales" --defaultLanguage "en-US" --sources "src/**/*.{ts,tsx}"`

#### 3. Creating translations files in entry folder

This command runs `trancorder` create runtime on the current directory, using the `src/locales` directory for localization json files destination, and creating translations for TypeScript/TSX files in the `src/` directory with the default language `en-US`, and with creating translations files also for German and Czech languages.

> `trancorder create --translations "src/locales" --defaultLanguage "en-US" --sources "src/**/*.{ts,tsx}" --languages "de-DE,cs-CZ"`

#### 4. Updating translations files in entry folder

This command runs `trancorder` update runtime on the current directory, using the `src/locales` directory for localization json files destination, and updating translations for TypeScript/TSX files in the `src/` directory with the default language `en-US`, and with updating translations files also for German and Czech languages.

> `trancorder create --translations "src/locales" --defaultLanguage "en-US" --sources "src/**/*.{ts,tsx}" --languages "de-DE,cs-CZ"`

#### 5. Exporting translations into xliff file

This command runs `trancorder` export runtime on the current directory, using the `src/locales` directory for localization json files destination for TypeScript/TSX files in the `src/` directory with the default language `en-US`, and with export translations files into `xliff` format with output folder `/path/to/output` where files will be exported.

> `trancorder export --translations "src/locales" --defaultLanguage "en-US" --sources "src/**/*.{ts,tsx}" --out "/path/to/output"`

_or_

> `trancorder export xliff --translations "src/locales" --defaultLanguage "en-US" --sources "src/**/*.{ts,tsx}" --out "/path/to/output"`

#### 6. Importing translations from xliff file

This command runs `trancorder` import runtime on the current directory, using the `src/locales` directory for localization json files destination for TypeScript/TSX files in the `src/` directory with the default language `en-US`, and with import translations files from `xliff` format with input folder `/path/to/output` from which files will be imported.

> `trancorder import --translations "src/locales" --defaultLanguage "en-US" --sources "src/**/*.{ts,tsx}" --in "/path/to/output"`

_or_

> `trancorder import xliff --translations "src/locales" --defaultLanguage "en-US" --sources "src/**/*.{ts,tsx}" --in "/path/to/output"`

## Config file `.trancorder.json`

To use the configuration file `.trancorder.json` with Trancorder, you can follow these steps:

1. Create a file named `.trancorder.json` in the root directory of your project.

2. Open the `.trancorder.json` file in a text editor and define the configuration options based on your requirements. Here is an example of a basic configuration:

```json
{
  "entry": "/path/to/project", //optional
  "translations": "locales",
  "defaultLanguage": "en-US",
  "sources": "src/**/*.{ts,tsx}",
  "languages": "cs-CZ,en-DE", //optional
  "in": "/path/to/exported", //optional
  "out": "/path/to/exported", //optional
  "config": "./tsconfig.json", //optional
  "debug": true //optional
  "format": "json", //optional
  "exports": "xliff" //optional
}
```

The configuration options in the `.trancorder.json` file are equivalent to the command-line options used with Trancorder. This means that you can specify the same options in the configuration file as you would when running Trancorder commands via the command line.

For example, the `translations` and `defaultLanguage` options in the configuration file correspond to the `--translations` and `--defaultLanguage` options used in the command line.

To understand the available options and their usage in more detail, you can refer to the documentation or help section of Trancorder. Typically, you can run `trancorder --help` or `trancorder <command> --help` in the command line to get information about the available options, commands, and their descriptions. You can also read description of parameters that are described previously in this file.

3. Save the `.trancorder.json` file.

4. Open a command-line interface and navigate to the root directory of your project.

5. Run the Trancorder CLI command to validate the translations based on the configuration file. For example:

```
trancorder validate
```

Trancorder will read the configuration from the `.trancorder.json` file and validate the translations files in the specified translations directory based on the source files and language settings.

Make sure to adjust the configuration options in the `.trancorder.json` file according to your project's needs, such as specifying the correct paths, formats and default language.

## Nodejs API

The trancorder cli module is primarily designed to be used in the command line interface, allowing developers to interact with their localization files and code base directly from the terminal.

In addition to its CLI, the trancorder module also provides a Node.js API, which allows developers to integrate its functionalities into their existing workflows or build custom tools on top of it. This API can be used to perform the same localization-related tasks programmatically, using JavaScript or TypeScript code, as opposed to the command line interface. By providing both a CLI and API, the trancorder module aims to be a flexible and versatile tool for localizing JavaScript and TypeScript projects.

The module exports several types, including:

-   `TrancorderCustomizers`: an object type that defines an optional `getSeverity` function that can be used to customize the severity of Trancorder errors.
-   `TrancorderExportType`: a union type that represents the supported export types for Trancorder (`xliff` in this case).
-   `TrancorderApi`: an object type that defines a set of functions that can be used to interact with the Trancorder library. These functions include `validate`, `create`, `update`, `export`, and `import`.
-   `CreateOptions`: an object type that defines optional `languages` property that can be used to specify the languages for which translations should be created.
-   `UpdateOptions`: an object type that defines optional `languages` property that can be used to specify the languages for which translations should be updated.
-   `CreateTrancorderOptions`: an intersection type that combines the `TrancorderOptions` type (described below) with the `TrancorderCustomizers` type.
-   `TrancorderOptions`: an object type that defines the configuration options for Trancorder, including the current working directory (`cwd`), whether to enable debug mode (`debug`), the path to the directory with translation files (`translations`), the paths to the source files (`sources`), the default language (`defaultLanguage`), the format of the translation files (`format`), and optional TypeScript configuration options (`tsConfigFilePath` or `tsCompilerOptions`).
-   `TrancorderSupportedFormats`: a union type that represents the supported formats for Trancorder (`json` in this case).
-   `TrancorderError`: an interface that extends the built-in `Error` interface and defines additional properties that describe a Trancorder error. These properties include a code (`TrancorderErrorCode`), a raw message and parameters (`rawMessage` and `rawParams`), a severity (`TrancorderErrorSeverity`), an optional reference to the original error (`original`), and optional start and end positions (`start` and `end`).
-   `TrancorderErrorSeverity`: an enum that represents the severity levels of a Trancorder error (`error`, `warning`, and `info`).
-   `TrancorderErrorCode`: an enum that represents the error codes that can be returned by Trancorder.
-   `TrancorderErrorMessages`: an object that maps error codes to their corresponding message and parameters.
-   `ValidateProjectResults`: an object type that defines the result of validating a project with Trancorder. This includes information about the project setup, the languages used, the translations, and any problems or errors that were found.
-   `CreateResults`: an object type that defines the result of creating translations for a project with Trancorder. This includes the paths and list of the created translations, the default language, the languages used, the translations themselves, and any errors that were encountered.
-   `UpdateResults`: an object type that extends `CreateResults` and adds a `changes` property that describes the changes that were made to the translations during the update.
-   `ExportResults`: an object type that defines the result of exporting translations with Trancorder. This includes the paths of the exported files.
-   `ImportResults`: an object type that defines the result of importing translations with Trancorder. This includes the paths of the imported files.

Finally, the module exports a single function, `createTrancorder`, which takes a `CreateTrancorderOptions` object as its argument and returns an instance of the `TrancorderApi`, which is an object containing several methods for validating, creating, updating, exporting, and importing translations for a project.

Options object `CreateTrancorderOptions`, which is an interface containing various options for configuring the behavior of the `TrancorderApi` instance. These options include:

-   `cwd`: a string specifying the current working directory for the project
-   `debug`: a boolean indicating whether to output debug information during execution
-   `getSeverity`: a function that takes a `TrancorderError` object and returns the severity of the error
-   `translations`: a string specifying the path to the directory containing the translation files
-   `sources`: an array of strings specifying the paths to the source files to be translated
-   `defaultLanguage`: a string specifying the default language for the project
-   `format`: a string specifying the format of the translation files ("json" in this case)
-   `tsConfigFilePath`: a string specifying the path to the TypeScript configuration file for the project
-   `tsCompilerOptions`: an object specifying additional compiler options to be passed to the TypeScript compiler

The `createTrancorder` function returns an instance of the `TrancorderApi`, which has the following methods:

-   `validate()`: a method that validates the project and returns a `ValidateProjectResults` object containing information about the project's configuration and any errors encountered during validation
-   `create(opts?: CreateOptions)`: a method that creates translation files for the project and returns a `CreateResults` object containing information about the created files and any errors encountered during creation
-   `update(opts?: UpdateOptions)`: a method that updates existing translation files for the project and returns an `UpdateResults` object containing information about the updated files and any errors encountered during update
-   `export(destination: string, type: TrancorderExportType)`: a method that exports the translations in the specified format to the specified destination and returns an `ExportResults` object containing information about the exported files and any errors encountered during export
-   `import(source: string, type: TrancorderExportType)`: a method that imports translations in the specified format from the specified source and returns an `ImportResults` object containing information about the imported files and any errors encountered during import

_"example.ts"_ file

```typescript
import { createTranscoder, TrancorderOptions } from 'trancorder-cli';

const options: TrancorderOptions = {
    cwd: '/path/to/project',
    debug: true,
    translations: 'locales',
    tsConfigFilePath: './tsconfig.json',
    tsCompilerOptions: { ... },
    defaultLanguage: 'en-US',
    format: "json",
    sources: ['src/**/*.{ts,tsx}'],
};

const trancorder = createTranscoder(options);

// Now you can use the trancorder object to perform operations such as validating translations, creating new translations, and updating existing translations.
trancorder.validate();
trancorder.create({ languages: ["cs-CZ", "de-DE"] });
trancorder.update({ languages: ["cs-CZ", "de-DE"] });
trancorder.export("/output/dir", "xliff");
trancorder.import("/output/dir", "xliff");
```

### Donate me

| QR                                                                                                           | Paypal                                                                                                                                                              |
| ------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |

### License

[Licensed under MIT][license]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/trancorder/logo.png
[license]: https://gitlab.com/stanislavhacker/servant/raw/master/trancorder/LICENSE
[1]: https://gitlab.com/stanislavhacker/servant/raw/master/trancorder-cli/README.md
[2]: https://gitlab.com/stanislavhacker/servant/raw/master/trancorder/README.md
