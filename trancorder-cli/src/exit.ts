import { TrancorderErrorHandler, TrancorderStatusHandler, ErrorsStatus } from "./view";

export type ExitHandler = {
	onStatus: TrancorderStatusHandler;
	onError: TrancorderErrorHandler;
	getExitCode(): number;
};

type ExitHandlerState = {
	exitCode: number;
	reason: Error | null;
};

export function createExitHandler(): ExitHandler {
	const state: ExitHandlerState = { exitCode: 0, reason: null };

	return {
		onError: (err: Error) => {
			state.exitCode = 1;
			state.reason = err;
		},
		onStatus: (status: ErrorsStatus) => {
			state.exitCode = calculateExitCode(status);
			state.reason = new Error("");
		},
		getExitCode: () => state.exitCode,
	};
}

function calculateExitCode(status: ErrorsStatus) {
	if (status.errors > 0) {
		return 1;
	}
	return 0;
}
