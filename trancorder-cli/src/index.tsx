import * as React from "react";
import { render } from "ink";

import { resolveMain } from "./resolve";
import { getArguments } from "./cli";
import { createExitHandler } from "./exit";
import { Main } from "./view";

if (resolveMain() === module) {
	const data = getArguments(process);
	const exitHandler = createExitHandler();

	const { waitUntilExit } = render(
		<Main
			cwd={data.entry}
			output={data.out}
			input={data.in}
			tsConfigFilePath={data.tsConfigPath}
			defaultLanguage={data.defaultLanguage}
			format={data.format}
			exports={data.exports}
			sources={data.sources}
			translations={data.translations}
			debug={data.debug}
			command={data.command}
			languages={data.languages}
			help={data.help}
			onStatus={exitHandler.onStatus}
			onError={exitHandler.onError}
		/>
	);

	waitUntilExit().then(() => {
		process.exitCode = exitHandler.getExitCode();
	});
}

export * from "./config";
export * from "./app";
