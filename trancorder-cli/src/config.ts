//NOTE: Same as in "servant-data/src/trancorder.json.ts"
export type TrancorderConfig = {
	entry?: string;
	out?: string;
	in?: string;
	translations?: string;
	sources?: string[];
	defaultLanguage?: string;
	format?: "json";
	exports?: "xliff";
	languages?: string[];
	config?: string;
	debug?: boolean;
};

export const TrancorderConfigDefaults: Required<TrancorderConfig> = {
	entry: "",
	out: "",
	in: "",
	debug: false,
	translations: "translations/",
	sources: ["src/**/*.{js,ts,jsx,tsx}"],
	defaultLanguage: "en-US",
	exports: "xliff",
	format: "json",
	languages: [],
	config: "",
};
