import Process = NodeJS.Process;
import * as path from "path";

import { envfull, EnvfullVars } from "envfull";

import { TrancorderExportType, TrancorderSupportedFormats } from "./app";
import { TrancorderCliCommands } from "./view";
import { TrancorderConfig, TrancorderConfigDefaults } from "./config";

export type TrancorderCliArguments = {
	entry: string;
	translations: string;
	sources: string[];
	defaultLanguage: string;
	languages: string[];
	tsConfigPath: string;
	out: string;
	in: string;
	format: TrancorderSupportedFormats;
	command: TrancorderCliCommands;
	exports: TrancorderExportType;
	debug?: boolean;
	help?: boolean;
};

export function getArguments(process: Process): TrancorderCliArguments {
	const { $, _ } = get(process);

	return {
		entry: $.entry ?? "",
		out: $.out ?? "",
		in: $.in ?? "",
		debug: $.debug,
		help: $.help,
		translations: $.translations as string,
		tsConfigPath: $.config ?? "",
		defaultLanguage: $.defaultLanguage as string,
		sources: parseArrays($.sources),
		languages: parseArrays($.languages),
		format: parseFormat(_),
		exports: parseExports(_),
		command: parseCommand(_),
	};
}

type TrancorderRawArguments = TrancorderConfig & {
	help?: boolean;
};
function get(process: Process): EnvfullVars<TrancorderRawArguments> {
	return envfull<TrancorderRawArguments>(process, {
		defaults: {
			...TrancorderConfigDefaults,
			entry: process.cwd(),
			out: process.cwd(),
			in: process.cwd(),
			help: false,
		},
		arrays: ["sources", "languages"],
		aliases: {},
	})(path.join(process.cwd(), ".trancorder.json"));
}

function parseFormat(data: Array<string | number | boolean>): TrancorderSupportedFormats {
	for (const key in data) {
		if (key.toString().toLowerCase() === "json") {
			return "json";
		}
	}
	return "json";
}

function parseExports(data: Array<string | number | boolean>): TrancorderExportType {
	for (const key in data) {
		if (key.toString().toLowerCase() === "xliff") {
			return "xliff";
		}
	}
	return "xliff";
}

function parseCommand(data: Array<string | number | boolean>): TrancorderCliCommands {
	let command: TrancorderCliCommands = "validate";

	data.forEach((item) => {
		const cmd = item.toString().toLowerCase() as TrancorderCliCommands;
		switch (cmd) {
			case "validate":
			case "create":
			case "update":
			case "import":
			case "export":
				command = cmd;
				break;
			default:
				break;
		}
	});
	return command;
}

function parseArrays(items?: string | string[]): string[] {
	const list = Array.isArray(items) ? items : items ? [items] : [];
	return list
		.map((item) => item.split(",").map((a) => a.trim()))
		.reduce((prev, current) => {
			return [...prev, ...current];
		}, [] as string[]);
}
