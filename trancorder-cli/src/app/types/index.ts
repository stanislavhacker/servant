import { CompilerOptions } from "ts-morph";
import { LanguageTranslations } from "trancorder";

import { TrancorderSetupResult } from "../ast";

export type TrancorderCustomizers = {
	getSeverity?: (error: TrancorderError) => TrancorderErrorSeverity | undefined;
};
export type TrancorderExportType = "xliff";

export type TrancorderApi = {
	validate(): Promise<ValidateProjectResults>;
	create(opts?: CreateOptions): Promise<CreateResults>;
	update(opts?: UpdateOptions): Promise<UpdateResults>;
	//exports, imports
	export(destination: string, type: TrancorderExportType): Promise<ExportResults>;
	import(source: string, type: TrancorderExportType): Promise<ImportResults>;
};

export type CreateOptions = {
	languages?: string[];
};
export type UpdateOptions = {
	languages?: string[];
};
export type CreateTrancorderOptions = TrancorderOptions & TrancorderCustomizers;

export type TrancorderOptions = {
	cwd?: string;
	debug?: boolean;
	translations: string;
	sources: string[];
	defaultLanguage: string;
	format: TrancorderSupportedFormats;
	tsConfigFilePath?: string;
	tsCompilerOptions?: CompilerOptions;
};

export type TrancorderSupportedFormats = "json";

export interface TrancorderError extends Error {
	code: TrancorderErrorCode;
	rawMessage: [string, string];
	rawParams: Array<string>;
	severity: TrancorderErrorSeverity;
	fixable: boolean;
	original?: Error;
	start?: { line: number; column: number };
	end?: { line: number; column: number };
}

export enum TrancorderErrorSeverity {
	Error = "error",
	Warning = "warning",
	Info = "info",
}

export enum TrancorderErrorCode {
	CanNotReadFile,
	CanNotParseJson,
	UnknownFileFormat,
	CanNotReadFiles,
	NoValidLanguagesFound, //fixable
	NoDefaultLanguageFound, //fixable
	DefaultLanguageSchemaError,
	SchemaError,
	ExtraKeyInLanguage, //fixable
	MissingKeyFromDefaultLanguage, //fixable
	CanNotAddSourceFile,
	NoTrancorderSetupFound,
	MoreTrancorderSetupsFound,
	NoExportOfT,
	MoreExportsOfT,
	NoKeyInTranslationFile, //fixable
	EmptyMessageText,
	EmptyAlternativeMessageText,
	EmptyAlternativeExpression,
	DynamicValueUsed,
	NonExistingPointerInValues,
	NotUsedValueInMessage,
	MessageIsTooLong,
	MessageAlternativeIsTooLong,
	EnumObjectIsEmpty,
	EnumValueIsNotUsed,
	EnumValueNotExists,
	KeyIsNotUsed, //fixable
	CanNotWriteLocaleFile,
	CanNotUpdateLocaleFile,
	UnsupportedTypeForExport,
	CanNotWriteExportedFile,
	TargetTranslationNotFound,
	TargetTranslationKeyNotFound,
	XliffStructureIsNotValid,
}

export const TrancorderErrorMessages: Record<TrancorderErrorCode, [string, string]> = {
	[TrancorderErrorCode.CanNotReadFile]: [
		`Can not read file "{0}"`,
		`Some error occurred when Trancorder try to read file "{0}". Check if its accessible or not used by another program.`,
	],
	[TrancorderErrorCode.CanNotParseJson]: [
		`Can not parse json file "{0}"`,
		`Some error occurred when Trancorder try to parse json "{0}". Is it valid json?`,
	],
	[TrancorderErrorCode.UnknownFileFormat]: [
		`Unknown file format "{0}"`,
		`Trancorder is not able to work with file format "{0}". Use one of supported formats "{1}"`,
	],
	[TrancorderErrorCode.CanNotReadFiles]: [
		`Can not read files with pattern "{0}"`,
		`Some error occurred when Trancorder try to read files by pattern "{0}". Check if its valid pattern, files are accessible or not used by another program.`,
	],
	[TrancorderErrorCode.NoValidLanguagesFound]: [
		`No languages load from directory`,
		`There are no languages loaded from translations directory. Do you enter a valid directory path, that contains at least one translation file. You can generate it also for default language.`,
	],
	[TrancorderErrorCode.NoDefaultLanguageFound]: [
		`No default language "{1}" found`,
		`There are loaded languages "{0}", but default language "{1}" is not found inside.`,
	],
	[TrancorderErrorCode.SchemaError]: [
		`Schema error for language file`,
		`Invalid property "{0}". {1} {2}`,
	],
	[TrancorderErrorCode.DefaultLanguageSchemaError]: [
		`Invalid default language file structure`,
		`There is invalid language file for default language "{0}". Can not validate other languages "{1}".`,
	],
	[TrancorderErrorCode.ExtraKeyInLanguage]: [
		`Extra key in language "{0}"`,
		`There is extra key "{2}" that is not defined in default language "{1}".`,
	],
	[TrancorderErrorCode.MissingKeyFromDefaultLanguage]: [
		`Missing key in language "{0}"`,
		`There is missing key "{2}" that is defined in default language "{1}" but not in language "{0}".`,
	],
	[TrancorderErrorCode.NoTrancorderSetupFound]: [
		`No trancorder setup found`,
		`There is not setup file in sources, where "trancorder" module is used to setup local or remote trancorder.`,
	],
	[TrancorderErrorCode.MoreTrancorderSetupsFound]: [
		`More trancorder setups found`,
		`There are more files ("{0}") in sources, that creates local or remote trancorder. That is not supported. Use only one setup or split into more modules / project by defining another sources files pattern.`,
	],
	[TrancorderErrorCode.NoExportOfT]: [
		`No export of "t" function`,
		`Setup found in "{0}" file, but there is no export of "t" function that is used for localisation.`,
	],
	[TrancorderErrorCode.MoreExportsOfT]: [
		`More exports of "t" function`,
		`Setup found in "{0}" file, but there are more exports of "t" function that is used for localisation. Allowed is only one.`,
	],
	[TrancorderErrorCode.NoKeyInTranslationFile]: [
		`Key not exists`,
		`Founded key "{0}" not exists in translation file.`,
	],
	[TrancorderErrorCode.EmptyMessageText]: [
		`Empty message text`,
		`Key "{0}" has empty message text.`,
	],
	[TrancorderErrorCode.EmptyAlternativeMessageText]: [
		`Empty message alternative text`,
		`Key "{0}" has empty message text in one of alternative with expression "{1}".`,
	],
	[TrancorderErrorCode.EmptyAlternativeExpression]: [
		`Empty alternative expression`,
		`Key "{0}" has empty on of defined alternative expression. This expression will never evaluated as true.`,
	],
	[TrancorderErrorCode.DynamicValueUsed]: [
		`Dynamic value used`,
		`For message with key "{0}" was used dynamic values object and Trancorder can not validate it.`,
	],
	[TrancorderErrorCode.NonExistingPointerInValues]: [
		`Non existing pointer {{0}}`,
		`For message with key "{1}" was used pointer {{0}}, but this pointer is not in values.`,
	],
	[TrancorderErrorCode.NotUsedValueInMessage]: [
		`Non used value {{0}} in message`,
		`Pointer for value {{0}} was never used in message with key "{1}" text and can be removed.`,
	],
	[TrancorderErrorCode.MessageIsTooLong]: [
		`Message is too long`,
		`Message with key "{0}" has too long text. Maximum allowed is {2} characters, but message has {1} characters.`,
	],
	[TrancorderErrorCode.MessageAlternativeIsTooLong]: [
		`Message is too long`,
		`Message with key "{0}" has too long text in expression "{1}". Maximum allowed is {3} characters, but message has {2} characters.`,
	],
	[TrancorderErrorCode.EnumObjectIsEmpty]: [
		`Empty enum object used`,
		`There was empty enum object for key "{0}". These messages will never shows up.`,
	],
	[TrancorderErrorCode.EnumValueIsNotUsed]: [
		`Enum value is not used for translation`,
		`There is value "{0}" with key "{1}" in enum, that is not used in translation object.`,
	],
	[TrancorderErrorCode.EnumValueNotExists]: [
		`Value is not exists in enum definition`,
		`There is value "{0}" defined in translation object, but this value is not exists in enum.`,
	],
	[TrancorderErrorCode.CanNotAddSourceFile]: [
		`Can not add source file`,
		`Can not add source file "{0}". File not exists, is not readable or unknown error. Look on original error provided.`,
	],
	[TrancorderErrorCode.KeyIsNotUsed]: [
		`Key is not used`,
		`Translation key "{0}" is not used in code and can be removed from file.`,
	],
	[TrancorderErrorCode.CanNotWriteLocaleFile]: [
		`Can not write locale file`,
		`Locale file "{0}" can not be created. Maybe is already exists or unknown error occurred. If file exists, use "update" instead of "create".`,
	],
	[TrancorderErrorCode.CanNotUpdateLocaleFile]: [
		`Can not update locale file`,
		`Locale file "{0}" can not be updated. Unknown error occurred.`,
	],
	[TrancorderErrorCode.UnsupportedTypeForExport]: [
		`This export type is not supported`,
		`Trancorder export type "{0}" is not supported.`,
	],
	[TrancorderErrorCode.CanNotWriteExportedFile]: [
		`Exported file can not be write`,
		`File "{0}" is not created. Maybe is already exists or unknown error occurred.`,
	],
	[TrancorderErrorCode.TargetTranslationNotFound]: [
		`Can not import translations`,
		`File "{0}" is not found for importing. Check if exists or if imported files are not too old.`,
	],
	[TrancorderErrorCode.TargetTranslationKeyNotFound]: [
		`Key not found in target translations`,
		`There is no key "{0}" in target file "{1}". Can not apply translated string.`,
	],
	[TrancorderErrorCode.XliffStructureIsNotValid]: [
		`Can not parse xliff file`,
		`There is some error when parsing xliff file.`,
	],
};

export type ValidateProjectResults = {
	cwd: string;
	format: TrancorderSupportedFormats;
	sources: string[];
	setup: TrancorderSetupResult | null;
	used: string[];
	translations: Record<string, LanguageTranslations>;
	languages: Record<string, string>;
	problems: Record<string, TrancorderError[]>;
	errors: TrancorderError[];
};

export type CreateResults = {
	paths: string[];
	list: string[];
	defaultLanguage: string;
	languages: Record<string, string>;
	translations: Record<string, LanguageTranslations>;
	errors: Record<string, TrancorderError[]>;
};

export type UpdateResults = CreateResults & {
	changes: Record<
		string,
		{
			added: LanguageTranslations;
			removed: LanguageTranslations;
		}
	>;
};

export type ExportResults = {
	paths: string[];
	errors: TrancorderError[];
};
export type ImportResults = {
	imports: ImportResult[];
	errors: TrancorderError[];
};

export type ImportResult = {
	errors: TrancorderError[];
	translation: LanguageTranslations;
	updated: string[];
	source: string;
	file: string;
	language: string;
};
