import {
	calculateReplacers,
	LanguageTranslation,
	LanguageTranslationFull,
	LanguageTranslations,
	ENUM_VALUE_MARKER,
	THIS_MARKER,
} from "trancorder";

import {
	DYNAMIC,
	TrancorderDefaultUsageResult,
	TrancorderEnumComplexUsageResult,
	TrancorderEnumSimpleUsageResult,
	TrancorderSetupResult,
	TrancorderSimpleUsageResult,
	TrancorderUsageResult,
} from "../ast";
import { ProcessTranslationsResult } from "../translations";
import { ProcessSourcesResult } from "../sources";
import {
	TrancorderError,
	TrancorderErrorCode,
	TrancorderErrorSeverity,
	TrancorderOptions,
	ValidateProjectResults,
} from "../types";
import { newError, newInfo, newWarning } from "../errors";

export function validateProject(
	translations: ProcessTranslationsResult,
	sources: ProcessSourcesResult,
	options: Required<TrancorderOptions>
): Promise<ValidateProjectResults> {
	const translation = getTranslationInfo(options.defaultLanguage, translations);
	const results = validateUsages(sources.cwd, sources.setup, translation);

	return Promise.resolve({
		cwd: sources.cwd,
		sources: sources.sources,
		setup: sources.setup,
		format: translations.format,
		languages: translations.languages,
		translations: translations.translations,
		used: results.used,
		problems: mergeErrors(translations.errors, results.errors, results.problems),
		errors: [...(translations.err ? [translations.err] : []), ...sources.errors],
	});
}

type ValidateUsagesResults = {
	used: string[];
	problems: Record<string, TrancorderError[]>;
	errors: Record<string, TrancorderError[]>;
};

function validateUsages(
	cwd: string,
	setup: TrancorderSetupResult | null,
	translation: TranslationFileInfo
): ValidateUsagesResults {
	const results: ValidateUsagesResults = {
		used: [],
		problems: {},
		errors: {},
	};

	if (setup) {
		setup.usages.forEach((usage) => validateUsage(results, usage, translation));

		const errors = [...validateUsedKeys(translation, results.used)];
		if (errors.length > 0) {
			results.errors[translation.file] = errors;
		}
	}

	return results;
}

function validateUsage(
	results: ValidateUsagesResults,
	usage: TrancorderUsageResult,
	translation: TranslationFileInfo
) {
	switch (usage.type) {
		case "simple":
			validateSimpleUsage(results, usage, translation);
			break;
		case "enum-simple":
			validateEnumSimpleUsage(results, usage, translation);
			break;
		case "enum-complex":
			validateEnumComplexUsage(results, usage, translation);
			break;
		default:
			break;
	}
}

function addPositions(
	errors: TrancorderError[],
	start: TrancorderDefaultUsageResult["start"],
	end: TrancorderDefaultUsageResult["end"]
) {
	errors.forEach((err) => {
		err.start = start;
		err.end = end;
	});
}

function validateDefaults(
	translation: TranslationFileInfo,
	key: string,
	message: LanguageTranslationFull,
	values: object,
	type: TrancorderUsageResult["type"]
) {
	const errors: TrancorderError[] = [];

	errors.push(...validateNonExistingKey(translation, key));
	if (noError(errors)) {
		errors.push(...validateFilledMessages(message, key));
	}
	if (noError(errors)) {
		errors.push(...validateEmptyExpressions(message, key));
	}
	if (noError(errors)) {
		errors.push(
			...validateUsedPointers(message, values, key, type),
			...validateMaximumCharacters(message, key)
		);
	}
	return errors;
}

function validateSimpleUsage(
	results: ValidateUsagesResults,
	usage: TrancorderSimpleUsageResult,
	translation: TranslationFileInfo
) {
	const { key, values, start, end } = usage;
	const message = convertToFull(translation.translation, key);
	const errors = validateDefaults(translation, key, message, values, "simple");

	addPositions(errors, start, end);
	addToProblems(results, usage.file, errors);
	addToUsed(results, key);
}

function validateEnumSimpleUsage(
	results: ValidateUsagesResults,
	usage: TrancorderEnumSimpleUsageResult,
	translation: TranslationFileInfo
) {
	const { key, values, enumObj, start, end } = usage;
	const message = convertToFull(translation.translation, key);
	const errors = validateEnumObject(enumObj, key);
	if (noError(errors)) {
		validateDefaults(translation, key, message, values, "enum-simple");
	}

	addPositions(errors, start, end);
	addToProblems(results, usage.file, errors);
	addToUsed(results, key);
}

function validateEnumComplexUsage(
	results: ValidateUsagesResults,
	usage: TrancorderEnumComplexUsageResult,
	translation: TranslationFileInfo
) {
	const { keysObj, values, enumObj, start, end } = usage;
	const keys = Object.values(keysObj);

	const errors = [
		...validateEnumObject(enumObj, keys[0] || "unknown"),
		...validateUsedEnumValues(enumObj, keysObj),
	];

	keys.forEach((key) => {
		const message = convertToFull(translation.translation, key);

		if (noError(errors)) {
			errors.push(...validateDefaults(translation, key, message, values, "enum-complex"));
		}
		addToUsed(results, key);
	});

	addPositions(errors, start, end);
	addToProblems(results, usage.file, errors);
}

function validateNonExistingKey(translation: TranslationFileInfo, key: string): TrancorderError[] {
	//translation key not found in default translation file
	if (translation.translation[key] === undefined) {
		return [newError(TrancorderErrorCode.NoKeyInTranslationFile, [key], true)];
	}
	return [];
}

function validateMaximumCharacters(
	message: LanguageTranslationFull,
	key: string
): TrancorderError[] {
	const errors: TrancorderError[] = [];
	const maxGlobal = message.maximumCharacters;

	if (maxGlobal && message.value.length > maxGlobal) {
		errors.push(
			newError(TrancorderErrorCode.MessageIsTooLong, [
				key,
				message.value.length.toString(),
				maxGlobal.toString(),
			])
		);
	}

	Object.keys(message.alternatives || {}).forEach((exp) => {
		const alt = convertToFull(message.alternatives, exp);
		const maxAlt = alt.maximumCharacters ? alt.maximumCharacters : maxGlobal;
		if (maxAlt && alt.value.length > maxAlt) {
			errors.push(
				newError(TrancorderErrorCode.MessageAlternativeIsTooLong, [
					key,
					exp,
					alt.value.length.toString(),
					maxAlt.toString(),
				])
			);
		}
	});

	return errors;
}

function validateEnumObject(enumObj: object, key: string): TrancorderError[] {
	const errors: TrancorderError[] = [];
	const values = Object.values(enumObj);

	if (values.length === 0) {
		errors.push(newError(TrancorderErrorCode.EnumObjectIsEmpty, [key]));
	}

	return errors;
}

function validateUsedEnumValues(
	enumObj: object,
	keysObj: Record<string | number, string>
): TrancorderError[] {
	const errors: TrancorderError[] = [];

	const enumValues = Object.values(enumObj).map(String);
	const translationKeys = Object.keys(keysObj);

	const enumValuesBack = Object.keys(enumObj).reduce((prev, key) => {
		prev[enumObj[key]] = key;
		return prev;
	}, {});

	enumValues.forEach((value) => {
		if (translationKeys.indexOf(value) === -1) {
			errors.push(
				newError(TrancorderErrorCode.EnumValueIsNotUsed, [value, enumValuesBack[value]])
			);
		}
	});

	translationKeys.forEach((value) => {
		if (enumValues.indexOf(value) === -1) {
			errors.push(newError(TrancorderErrorCode.EnumValueNotExists, [value]));
		}
	});

	return errors;
}

function validateUsedPointers(
	message: LanguageTranslationFull,
	values: object,
	key: string,
	type: TrancorderUsageResult["type"]
): TrancorderError[] {
	const errors: TrancorderError[] = [];
	const { replacers } = calculateReplacers(values, {});
	const { pointers } = extractPointers(message);

	const usedPointers: string[] = [];
	pointers.forEach((pointer) => {
		//special key for enum value
		if ((type === "enum-simple" || type === "enum-complex") && pointer === THIS_MARKER) {
			pointer = ENUM_VALUE_MARKER;
		}
		//has key
		if (replacers[pointer] !== undefined) {
			usedPointers.push(pointer);
			return;
		}
		//has dynamic
		const dynamic = hasDynamic(replacers, pointer);
		if (dynamic) {
			usedPointers.push(dynamic);
			errors.push(newInfo(TrancorderErrorCode.DynamicValueUsed, [key]));
			return;
		}
		//error
		errors.push(newError(TrancorderErrorCode.NonExistingPointerInValues, [pointer, key]));
	});
	//Always used because is used for matching of message based on enum
	if (type === "enum-complex") {
		usedPointers.push(ENUM_VALUE_MARKER);
	}

	Object.keys(replacers).forEach((pointer) => {
		if (usedPointers.indexOf(pointer) === -1) {
			errors.push(newInfo(TrancorderErrorCode.NotUsedValueInMessage, [pointer, key]));
		}
	});

	return errors;
}

function validateFilledMessages(message: LanguageTranslationFull, key: string): TrancorderError[] {
	const errors: TrancorderError[] = [];

	if (message.value === "") {
		errors.push(newError(TrancorderErrorCode.EmptyMessageText, [key]));
	}

	Object.keys(message.alternatives || {}).forEach((exp) => {
		const alt = convertToFull(message.alternatives, exp);
		if (alt.value === "") {
			errors.push(newError(TrancorderErrorCode.EmptyAlternativeMessageText, [key, exp]));
		}
	});
	return errors;
}

function validateEmptyExpressions(
	message: LanguageTranslationFull,
	key: string
): TrancorderError[] {
	const errors: TrancorderError[] = [];

	Object.keys(message.alternatives || {}).forEach((exp) => {
		if (exp === "") {
			errors.push(newWarning(TrancorderErrorCode.EmptyAlternativeExpression, [key]));
		}
	});
	return errors;
}

function validateUsedKeys(translation: TranslationFileInfo, used: string[]): TrancorderError[] {
	const errors: TrancorderError[] = [];
	const fileKeys = Object.keys(translation.translation);

	fileKeys.forEach((key) => {
		if (used.indexOf(key) === -1) {
			errors.push(newError(TrancorderErrorCode.KeyIsNotUsed, [key], true));
		}
	});

	return errors;
}

type TranslationFileInfo = {
	translation: LanguageTranslations;
	language: string;
	file: string;
};

function getTranslationInfo(
	lang: string,
	translations: ProcessTranslationsResult
): TranslationFileInfo {
	const translationFile = translations.languages[lang];
	return {
		translation: translations.translations[translationFile] || {},
		file: translationFile || "",
		language: lang,
	};
}

const regexp = /\{([^{}()\s]{1,})}/g;

function extractPointers(message: LanguageTranslationFull): {
	messages: Array<string>;
	pointers: Array<string>;
	expressions: Array<string>;
} {
	const locTexts = [
		message.value,
		...Object.keys(message.alternatives || {}).map(
			(key) => convertToFull(message.alternatives, key).value
		),
	];
	const expTexts = Object.keys(message.alternatives || {});

	const messages = collectPointers(locTexts);
	const expressions = collectPointers(expTexts);

	return {
		pointers: [...messages, ...expressions],
		messages,
		expressions,
	};
}

export function collectPointers(texts: string[]) {
	const pointers: string[] = [];
	texts.forEach((msg) => {
		let match = regexp.exec(msg);
		while (match) {
			pointers.push(match[1]);
			match = regexp.exec(msg);
		}
	});
	return pointers;
}

export function convertToFull(
	translation: LanguageTranslations | LanguageTranslationFull["alternatives"],
	key: string
): LanguageTranslationFull {
	const value: LanguageTranslation = (translation && translation[key]) || "";

	if (typeof value === "string") {
		return { value };
	}
	return value;
}

function hasDynamic(replacers: Record<string, object>, pointer: string) {
	const parts = pointer.split(".");
	parts.pop();
	while (parts.length) {
		const c = parts.join(".");
		if (replacers[c] !== undefined && (replacers[c] as unknown as string) === DYNAMIC) {
			return c;
		}
		parts.pop();
	}
	return false;
}

function addToProblems(
	results: ValidateUsagesResults,
	filepath: string,
	errors: TrancorderError[]
) {
	const problems = (results.problems[filepath] = results.problems[filepath] || []);
	problems.push(...errors);
}
function addToUsed(results: ValidateUsagesResults, key: string) {
	if (results.used.indexOf(key) === -1) {
		results.used.push(key);
	}
}

function mergeErrors(...args: Record<string, TrancorderError[]>[]) {
	return args.reduce((prev, current) => {
		Object.keys(current).forEach((key) => {
			prev[key] = prev[key] ? [...prev[key], ...current[key]] : [...current[key]];
			return prev;
		});
		return prev;
	}, {} as Record<string, TrancorderError[]>);
}

function noError(errors: TrancorderError[]): boolean {
	return !errors.some((err) => err.severity === TrancorderErrorSeverity.Error);
}
