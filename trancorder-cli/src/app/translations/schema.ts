import { Validator, Schema, ValidatorResult } from "jsonschema";

export const LanguageTranslationSchema: Schema = {
	type: "object",
	minProperties: 1,
	additionalProperties: {
		oneOf: [
			{ type: "string" },
			{
				type: "object",
				properties: {
					value: { type: "string" },
					description: { type: "string" },
					translate: { type: "boolean" },
					maximumCharacters: { type: "number" },
					alternatives: {
						type: "object",
						additionalProperties: {
							oneOf: [
								{ type: "string" },
								{
									type: "object",
									properties: {
										value: { type: "string" },
										description: { type: "string" },
										translate: { type: "boolean" },
										maximumCharacters: { type: "number" },
									},
									required: ["value"],
									additionalProperties: false,
								},
							],
						},
					},
				},
				required: ["value"],
				additionalProperties: false,
			},
		],
	},
};

export function validateBySchema(json: object): ValidatorResult {
	const validator = new Validator();

	return validator.validate(json, LanguageTranslationSchema);
}
