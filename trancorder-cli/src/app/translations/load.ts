import * as path from "path";
import { TrancorderError, TrancorderErrorCode, TrancorderSupportedFormats } from "../types";
import { loadFiles, LoadFilesResult, readFile } from "../io";
import { newError } from "../errors";

export type LoadTranslationsResult = {
	cwd: string;
	format: TrancorderSupportedFormats;
	translations: Record<string, object>;
	errors: Record<string, TrancorderError[]>;
	err?: TrancorderError;
};

export type LoadTranslationsOptions = {
	debug?: boolean;
};

export function loadTranslations(
	cwd: string,
	translations: string,
	format: TrancorderSupportedFormats,
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	_opts?: LoadTranslationsOptions
): Promise<LoadTranslationsResult> {
	const where = path.join(translations, `*.${format}`).replace(/\\/g, "/");
	return loadFiles(cwd, [where])
		.then((results) => loadFileContents(cwd, format, results))
		.then((loaded) => createResults(cwd, format, loaded));
}

function createResults(
	cwd: string,
	format: TrancorderSupportedFormats,
	{ err, data }: LoadFileContentsResult
): LoadTranslationsResult {
	const results = {
		translations: {} as LoadTranslationsResult["translations"],
		errors: {} as LoadTranslationsResult["errors"],
	};

	const partial = data.reduce((prev, { json, relative, err }) => {
		if (err) {
			prev.errors[relative] = [err];
		} else {
			prev.translations[relative] = json;
		}
		return prev;
	}, results);

	return {
		cwd,
		format,
		...partial,
		err,
	};
}

type LoadFileContentsResult = {
	err?: TrancorderError;
	data: LoadFileByFormatResult[];
};

function loadFileContents(
	cwd: string,
	format: TrancorderSupportedFormats,
	{ files, err }: LoadFilesResult
): Promise<LoadFileContentsResult> {
	const promises = files.map((file) => loadFileByFormat(cwd, file, format));
	return Promise.all(promises).then((data) => {
		return {
			err,
			data,
		};
	});
}

type LoadFileByFormatResult = {
	err?: TrancorderError;
	json: object;
	relative: string;
};

function loadFileByFormat(
	cwd: string,
	relative: string,
	format: TrancorderSupportedFormats
): Promise<LoadFileByFormatResult> {
	return readFile(path.join(cwd, relative), relative).then(({ content, err }) => {
		if (err) {
			return {
				json: {},
				err,
				relative,
			};
		}

		switch (format) {
			case "json":
				return parseJson(relative, content);
			default:
				return {
					json: {},
					err: newError(TrancorderErrorCode.UnknownFileFormat, [
						format,
						(["json"] as TrancorderSupportedFormats[]).join(", "),
					]),
					relative,
				};
		}
	});
}

function parseJson(relative: string, content: string): LoadFileByFormatResult {
	try {
		return {
			json: JSON.parse(content),
			relative,
		};
	} catch (e) {
		return {
			json: {},
			relative,
			err: newError(TrancorderErrorCode.CanNotReadFile, [relative], false, e),
		};
	}
}
