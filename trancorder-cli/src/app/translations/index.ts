import { TrancorderOptions } from "../types";

import { loadTranslations } from "./load";
import { validateTranslations, ValidatedTranslationsResult } from "./validate";

export type ProcessTranslationsResult = ValidatedTranslationsResult;

export function processTranslations({
	cwd,
	format,
	translations,
	defaultLanguage,
	debug,
}: Required<TrancorderOptions>): Promise<ProcessTranslationsResult> {
	return loadTranslations(cwd, translations, format, { debug }).then((results) =>
		validateTranslations(results, defaultLanguage, { debug })
	);
}
