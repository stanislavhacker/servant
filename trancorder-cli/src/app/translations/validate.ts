import { LanguageTranslations } from "trancorder";
import { ValidationError } from "jsonschema";
import * as path from "path";

import { TrancorderError, TrancorderErrorCode, TrancorderSupportedFormats } from "../types";
import { newError, newWarning } from "../errors";

import { validateBySchema } from "./schema";
import { LoadTranslationsResult } from "./load";

export type ValidatedTranslationsResult = {
	cwd: string;
	format: TrancorderSupportedFormats;
	translations: Record<string, LanguageTranslations>;
	languages: Record<string, string>;
	errors: Record<string, TrancorderError[]>;
	err?: TrancorderError;
};

export type ValidateTranslationsOptions = {
	debug?: boolean;
};

export function validateTranslations(
	{ cwd, translations, errors, format, err }: LoadTranslationsResult,
	defaultLanguage: string,
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	opts?: ValidateTranslationsOptions
): Promise<ValidatedTranslationsResult> {
	const languages = extractLanguages(translations);

	const error = validateLanguages(languages, defaultLanguage, err);
	const results = validateLanguagesStructure(errors, languages, translations, defaultLanguage);

	return Promise.resolve({
		cwd,
		format,
		err: error,
		...results,
	});
}

type ExtractedLanguages = {
	languages: Record<string, string>;
	list: string[];
};

function extractLanguages(
	translations: LoadTranslationsResult["translations"]
): ExtractedLanguages {
	const list: string[] = [];
	const languages = Object.keys(translations).reduce((prev, key) => {
		const ext = path.extname(key);
		const language = path.basename(key, ext);
		prev[language] = key;
		list.push(language);
		return prev;
	}, {});

	return {
		languages,
		list,
	};
}

function validateLanguages(
	validLanguages: ExtractedLanguages,
	defaultLanguage: string,
	err?: TrancorderError
): TrancorderError | undefined {
	let error = err;
	if (!error && validLanguages.list.length === 0) {
		error = newError(TrancorderErrorCode.NoValidLanguagesFound, [], true);
	}
	if (!error && !validLanguages.list.includes(defaultLanguage)) {
		error = newError(
			TrancorderErrorCode.NoDefaultLanguageFound,
			[validLanguages.list.join(", "), defaultLanguage],
			true
		);
	}
	return error;
}

function validateLanguagesStructure(
	errors: LoadTranslationsResult["errors"],
	validLanguages: ExtractedLanguages,
	translations: Record<string, object>,
	defaultLanguage: string
) {
	const results = {
		translations: {} as ValidatedTranslationsResult["translations"],
		errors: { ...errors } as ValidatedTranslationsResult["errors"],
		languages: validLanguages.languages,
	};

	//no default language
	if (!validLanguages.list.includes(defaultLanguage)) {
		Object.keys(translations).forEach((relative) => {
			const exists = results.errors[relative] || [];
			results.errors[relative] = [
				...exists,
				newError(
					TrancorderErrorCode.NoDefaultLanguageFound,
					[validLanguages.list.join(", "), defaultLanguage],
					true
				),
			];
		});
		return results;
	}

	const defaultValid = validateDefaultLanguage(
		results,
		validLanguages,
		translations,
		defaultLanguage
	);

	//invalid default file, mark all others as invalid
	if (!defaultValid) {
		Object.keys(translations).forEach((relative) => {
			if (relative !== validLanguages.languages[defaultLanguage]) {
				const exists = results.errors[relative] || [];
				results.errors[relative] = [
					...exists,
					newError(TrancorderErrorCode.DefaultLanguageSchemaError, [
						defaultLanguage,
						validLanguages.list.join(", "),
					]),
				];
			}
		});
		return results;
	}

	const defaultDefinition = results.translations[validLanguages.languages[defaultLanguage]];
	validLanguages.list.forEach((language) => {
		if (language !== defaultLanguage) {
			validateAlternativeLanguage(
				results,
				defaultDefinition,
				translations[validLanguages.languages[language]],
				validLanguages.languages[language],
				language,
				defaultLanguage
			);
		}
	});

	return results;
}

function validateDefaultLanguage(
	results: Pick<ValidatedTranslationsResult, "translations" | "errors">,
	validLanguages: ExtractedLanguages,
	translations: Record<string, object>,
	defaultLanguage: string
) {
	const relative = validLanguages.languages[defaultLanguage];
	const { valid, errors } = validateBySchema(translations[relative]);
	if (valid) {
		results.translations[relative] = translations[relative] as LanguageTranslations;
		return true;
	}

	const exists = results.errors[relative] || [];
	results.errors[relative] = [...exists, ...convertErrorsFromSchema(errors)];
	return false;
}

function validateAlternativeLanguage(
	results: Pick<ValidatedTranslationsResult, "translations" | "errors">,
	defaultDefinition: LanguageTranslations,
	translation: object,
	relative: string,
	language: string,
	defaultLanguage: string
) {
	const { valid, errors } = validateBySchema(translation);
	const exists = results.errors[relative] || [];

	//schema validate
	if (!valid) {
		results.errors[relative] = [...exists, ...convertErrorsFromSchema(errors)];
		return false;
	}

	const collectedErrors: TrancorderError[] = [];
	//compare to default language structure
	Object.keys(translation).map((key) => {
		if (!defaultDefinition[key]) {
			collectedErrors.push(
				newWarning(
					TrancorderErrorCode.ExtraKeyInLanguage,
					[language, defaultLanguage, key],
					true
				)
			);
		}
	});
	Object.keys(defaultDefinition).map((key) => {
		if (!translation[key]) {
			collectedErrors.push(
				newWarning(
					TrancorderErrorCode.MissingKeyFromDefaultLanguage,
					[language, defaultLanguage, key],
					true
				)
			);
		}
	});

	results.translations[relative] = translation as LanguageTranslations;
	if (collectedErrors.length > 0) {
		results.errors[relative] = [...exists, ...collectedErrors];
	}
	return true;
}

function convertErrorsFromSchema(errors: ValidationError[]) {
	return errors.map((err) =>
		newError(TrancorderErrorCode.SchemaError, [err.property, err.name, err.message])
	);
}
