import {
	ValidateProjectResults,
	UpdateResults,
	UpdateOptions,
	TrancorderOptions,
} from "../../types";
import { createTemplate, patchLocaleFile, updateLocaleFile } from "../utils";

export function updateFiles(
	results: ValidateProjectResults,
	opts: Required<TrancorderOptions & UpdateOptions>
): Promise<UpdateResults> {
	const { languages, defaultLanguage } = opts;

	const template = createTemplate(results);
	const all = [defaultLanguage, ...languages];

	const tasks = all.map((language) =>
		patchLocaleFile(template, results.translations[results.languages[language]]).then(
			({ file, added, removed }) =>
				updateLocaleFile(file, language, opts).then((results) => ({
					...results,
					added,
					removed,
				}))
		)
	);

	return Promise.all(tasks).then((results) => {
		const valid = results.filter((res) => res.errors.length === 0);
		const errors = results.filter((res) => res.errors.length > 0);

		return {
			defaultLanguage,
			list: all,
			paths: valid.map((res) => res.relative),
			errors: errors.reduce(
				(prev, { errors, relative }) => ({ ...prev, [relative]: errors }),
				{} as UpdateResults["errors"]
			),
			translations: valid.reduce(
				(prev, { translation, relative }) => ({ ...prev, [relative]: translation }),
				{} as UpdateResults["translations"]
			),
			languages: valid.reduce(
				(prev, { language, relative }) => ({ ...prev, [language]: relative }),
				{} as UpdateResults["languages"]
			),
			changes: valid.reduce(
				(prev, { added, removed, relative }) => ({
					...prev,
					[relative]: { added, removed },
				}),
				{} as UpdateResults["changes"]
			),
		};
	});
}
