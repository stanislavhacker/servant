import {
	ValidateProjectResults,
	TrancorderOptions,
	CreateOptions,
	CreateResults,
	UpdateResults,
} from "../../types";
import { createTemplate, saveLocaleFile } from "../utils";

export function createFiles(
	results: ValidateProjectResults,
	opts: Required<TrancorderOptions & CreateOptions>
): Promise<CreateResults> {
	const { languages, defaultLanguage } = opts;

	const template = createTemplate(results);
	const all = [defaultLanguage, ...languages];

	const tasks = all.map((language) => saveLocaleFile(template, language, opts));

	return Promise.all(tasks).then((results) => {
		const valid = results.filter((res) => res.errors.length === 0);
		const errors = results.filter((res) => res.errors.length > 0);

		return {
			defaultLanguage,
			list: all,
			paths: valid.map((res) => res.relative),
			languages: results.reduce(
				(prev, { language, relative }) => ({ ...prev, [language]: relative }),
				{} as UpdateResults["languages"]
			),
			errors: errors.reduce(
				(prev, { errors, relative }) => ({ ...prev, [relative]: errors }),
				{} as UpdateResults["errors"]
			),
			translations: valid.reduce(
				(prev, { translation, relative }) => ({ ...prev, [relative]: translation }),
				{} as UpdateResults["translations"]
			),
		};
	});
}
