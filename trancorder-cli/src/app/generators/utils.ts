import { LanguageTranslations } from "trancorder";
import * as path from "path";

import {
	TrancorderError,
	TrancorderErrorCode,
	TrancorderOptions,
	TrancorderSupportedFormats,
	ValidateProjectResults,
} from "../types";
import { newError } from "../errors";
import { normalizePath, writeFile } from "../io";

export function createTemplate(results: ValidateProjectResults): LanguageTranslations {
	const keys = results.used.slice().sort();
	const file: LanguageTranslations = {};
	keys.forEach((key) => {
		file[key] = key;
	});
	return file;
}

export type PatchLocaleFileResults = {
	file: LanguageTranslations;
	added: LanguageTranslations;
	removed: LanguageTranslations;
};

export function patchLocaleFile(
	template: LanguageTranslations,
	current: LanguageTranslations = {}
): Promise<PatchLocaleFileResults> {
	const templateKeys = Object.keys(template);
	const currentKeys = Object.keys(current);

	const keys = [...new Set([...templateKeys, ...currentKeys])].sort();

	const created: LanguageTranslations = {};
	const added: LanguageTranslations = {};
	const removed: LanguageTranslations = {};
	keys.forEach((key) => {
		//no change
		if (template[key] && current[key]) {
			created[key] = current[key];
		}
		//added
		if (template[key] && !current[key]) {
			created[key] = template[key];
			added[key] = template[key];
		}
		//removed
		if (!template[key] && current[key]) {
			removed[key] = current[key];
		}
	});

	return Promise.resolve({
		file: created,
		added,
		removed,
	});
}

export type SaveLocaleFileResults = {
	errors: TrancorderError[];
	absolute: string;
	relative: string;
	language: string;
	translation: LanguageTranslations;
};

export function saveLocaleFile(
	data: LanguageTranslations,
	language: string,
	options: Required<TrancorderOptions>
): Promise<SaveLocaleFileResults> {
	return new Promise((resolve) => {
		const { format, cwd, translations } = options;
		const directoryPath = path.join(cwd, translations);

		const { filepath, content, error } = buildDataBasedOnFormat(
			directoryPath,
			language,
			format,
			data
		);
		const relative = path.relative(cwd, filepath);

		const results: SaveLocaleFileResults = {
			absolute: normalizePath(filepath),
			relative: normalizePath(relative),
			translation: data,
			errors: [],
			language,
		};

		if (error) {
			resolve({ ...results, errors: [error] });
			return;
		}

		writeFile(filepath, content, "wx")
			.then(() => resolve(results))
			.catch((err: Error) =>
				resolve({
					...results,
					errors: [
						newError(
							TrancorderErrorCode.CanNotWriteLocaleFile,
							[normalizePath(relative)],
							false,
							err
						),
					],
				})
			);
	});
}

export type UpdateLocaleFileResults = {
	errors: TrancorderError[];
	absolute: string;
	relative: string;
	language: string;
	translation: LanguageTranslations;
};

export function updateLocaleFile(
	data: LanguageTranslations,
	language: string,
	options: Required<TrancorderOptions>
): Promise<UpdateLocaleFileResults> {
	return new Promise((resolve) => {
		const { format, cwd, translations } = options;
		const directoryPath = path.join(cwd, translations);

		const { filepath, content, error } = buildDataBasedOnFormat(
			directoryPath,
			language,
			format,
			data
		);
		const relative = path.relative(cwd, filepath);

		const results: UpdateLocaleFileResults = {
			absolute: normalizePath(filepath),
			relative: normalizePath(relative),
			translation: data,
			errors: [],
			language,
		};

		if (error) {
			resolve({ ...results, errors: [error] });
			return;
		}

		writeFile(filepath, content, "w")
			.then(() => resolve(results))
			.catch((err: Error) =>
				resolve({
					...results,
					errors: [
						newError(
							TrancorderErrorCode.CanNotUpdateLocaleFile,
							[normalizePath(relative)],
							false,
							err
						),
					],
				})
			);
	});
}

function buildDataBasedOnFormat(
	directoryPath: string,
	language: string,
	format: TrancorderSupportedFormats,
	data: LanguageTranslations
) {
	switch (format) {
		case "json": {
			const filepath = path.join(directoryPath, `${language}.${format}`);
			const content = JSON.stringify(data, null, "  ");
			return {
				filepath,
				content,
			};
		}
		default:
			return {
				filepath: "",
				content: "",
				error: newError(TrancorderErrorCode.UnknownFileFormat, [
					format,
					(["json"] as TrancorderSupportedFormats[]).join(", "),
				]),
			};
	}
}
