import {
	ValidateProjectResults,
	CreateTrancorderOptions,
	TrancorderExportType,
	TrancorderCustomizers,
	TrancorderOptions,
	TrancorderError,
	TrancorderApi,
	CreateOptions,
	UpdateOptions,
	CreateResults,
	UpdateResults,
} from "./types";
import { processTranslations } from "./translations";
import { processSources } from "./sources";
import { validateProject } from "./project";

import { createFiles } from "./generators/create";
import { updateFiles } from "./generators/update";
import { exportFiles, importFiles } from "./exporters";

export function createTrancorder({
	cwd = process.cwd(),
	debug = false,
	getSeverity = () => undefined,
	...restOptions
}: CreateTrancorderOptions): TrancorderApi {
	const options = {
		cwd,
		debug,
		...restOptions,
	} as Required<TrancorderOptions>;

	const customizers = {
		getSeverity,
	} as Required<TrancorderCustomizers>;

	const state: TrancorderState = {
		validation: null,
	};

	return {
		validate: () => validate(state, options, customizers),
		create: (opts = {}) => create(state, options, customizers, opts),
		update: (opts = {}) => update(state, options, customizers, opts),
		export: (destination, type) => exportFn(state, options, customizers, destination, type),
		import: (source, type) => importFn(state, options, customizers, source, type),
	};
}

function validate(
	state: TrancorderState,
	options: Required<TrancorderOptions>,
	customizers: Required<TrancorderCustomizers>
) {
	return runValidation(options)
		.then((results) => remapValidationSeverity(results, customizers.getSeverity))
		.then((results) => saveValidation(state, results));
}

function runValidation(options: Required<TrancorderOptions>): Promise<ValidateProjectResults> {
	//load
	const tasks = Promise.all([processTranslations(options), processSources(options)]);
	//validate
	return tasks.then(([translations, sources]) => validateProject(translations, sources, options));
}

function create(
	state: TrancorderState,
	options: Required<TrancorderOptions>,
	customizers: Required<TrancorderCustomizers>,
	opts: CreateOptions
) {
	return loadValidation(state, options, customizers)
		.then((results) => runCreate(results, options, opts))
		.then((results) => remapUpdateSeverity(results, customizers.getSeverity))
		.then((results) => clearValidation(state, results));
}

function runCreate(
	results: ValidateProjectResults,
	options: Required<TrancorderOptions>,
	{ languages = [] }: CreateOptions
) {
	return createFiles(results, { ...options, languages });
}

function update(
	state: TrancorderState,
	options: Required<TrancorderOptions>,
	customizers: Required<TrancorderCustomizers>,
	opts: UpdateOptions
) {
	return loadValidation(state, options, customizers)
		.then((results) => runUpdate(results, options, opts))
		.then((results) => remapUpdateSeverity(results, customizers.getSeverity))
		.then((results) => clearValidation(state, results));
}

function runUpdate(
	results: ValidateProjectResults,
	options: Required<TrancorderOptions>,
	{ languages = [] }: UpdateOptions
) {
	return updateFiles(results, { ...options, languages });
}

function exportFn(
	state: TrancorderState,
	options: Required<TrancorderOptions>,
	customizers: Required<TrancorderCustomizers>,
	destination: string,
	type: TrancorderExportType
) {
	return loadValidation(state, options, customizers).then((results) =>
		runExport(results, options, destination, type)
	);
}

function runExport(
	results: ValidateProjectResults,
	options: Required<TrancorderOptions>,
	destination: string,
	type: TrancorderExportType
) {
	return exportFiles(results, options, destination, type);
}

function importFn(
	state: TrancorderState,
	options: Required<TrancorderOptions>,
	customizers: Required<TrancorderCustomizers>,
	source: string,
	type: TrancorderExportType
) {
	return loadValidation(state, options, customizers).then((results) =>
		runImport(results, options, source, type)
	);
}

function runImport(
	results: ValidateProjectResults,
	options: Required<TrancorderOptions>,
	source: string,
	type: TrancorderExportType
) {
	return importFiles(results, options, source, type);
}

//customizers

function remapValidationSeverity(
	results: ValidateProjectResults,
	customizer: Required<TrancorderCustomizers>["getSeverity"]
) {
	const remap = (err: TrancorderError): TrancorderError => ({
		...err,
		severity: customizer({ ...err }) || err.severity,
	});

	results.errors = results.errors.map(remap);
	Object.keys(results.problems).forEach((file) => {
		results.problems[file] = results.problems[file].map(remap);
	});

	return results;
}

function remapUpdateSeverity<T extends CreateResults | UpdateResults>(
	results: T,
	customizer: Required<TrancorderCustomizers>["getSeverity"]
) {
	const remap = (err: TrancorderError): TrancorderError => ({
		...err,
		severity: customizer({ ...err }) || err.severity,
	});

	Object.keys(results.errors).forEach((file) => {
		results.errors[file] = results.errors[file].map(remap);
	});

	return results;
}

//state

type TrancorderState = {
	validation: ValidateProjectResults | null;
};

function clearValidation<T>(state: TrancorderState, data: T): T {
	state.validation = null;
	return data;
}

function saveValidation(
	state: TrancorderState,
	results: ValidateProjectResults
): ValidateProjectResults {
	state.validation = results;
	return results;
}

function loadValidation(
	state: TrancorderState,
	options: Required<TrancorderOptions>,
	customizers: Required<TrancorderCustomizers>
): Promise<ValidateProjectResults> {
	if (state.validation) {
		return Promise.resolve(state.validation);
	}
	return validate(state, options, customizers);
}
