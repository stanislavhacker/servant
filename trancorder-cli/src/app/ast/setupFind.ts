import {
	Node,
	Project,
	SourceFile,
	Identifier,
	SyntaxKind,
	CallExpression,
	ReferencedSymbol,
	ImportDeclaration,
	VariableDeclaration,
} from "ts-morph";
import * as trancorder from "trancorder";
import * as path from "path";

import { TrancorderUsageResult, trancorderUsageSearch } from "./usageFind";

export const ModuleName = "trancorder";
export const ModuleInitialized = ["localTrancorder", "remoteTrancorder"] as Array<
	keyof typeof trancorder
>;

export type TrancorderSetupResult = {
	file: SourceFile;
	path: string;
	type: "local" | "remote";
	exports: Array<Identifier>;
	usages: Array<TrancorderUsageResult>;
};

export function trancorderSetupSearch(
	cwd: string,
	program: Project,
	files: SourceFile[]
): TrancorderSetupResult[] {
	const data: TrancorderSetupResult[] = [];
	files.forEach((file) => {
		const setup = searchTrancorderSetupAndExports(cwd, program, file);
		setup.forEach((item) => {
			if (item.exports.length > 0) {
				data.push({
					...item,
					file,
					path: relativizePath(cwd, file.getFilePath().toString()),
				});
			}
		});
	});
	return data;
}

function searchTrancorderSetupAndExports(
	cwd: string,
	program: Project,
	file: SourceFile
): TrancorderSetup[] {
	const data = getTrancorderIdentifiers(file);
	return searchTrancorderSetup(cwd, data);
}

//usage of trancorder

type TrancorderIdentifier = {
	type: "local" | "remote";
	identifier: Identifier;
};

function getTrancorderIdentifiers(file: SourceFile): TrancorderIdentifier[] {
	const data: TrancorderIdentifier[] = [];
	file.getImportDeclarations().forEach((importDeclaration) => {
		const module = importDeclaration.getModuleSpecifierValue();
		if (ModuleName === module) {
			data.push(...processNamedImports(importDeclaration));
			data.push(...processNamespaceImport(importDeclaration));
		}
	});
	file.getDescendantsOfKind(SyntaxKind.CallExpression).forEach((call) => {
		const [identifier, module] = processRequires(call);
		if (identifier && ModuleName === module) {
			data.push(...processNamedRequires(identifier));
		}
	});
	return data;
}

// import keyword

function processNamedImports(importDeclaration: ImportDeclaration): TrancorderIdentifier[] {
	const data: TrancorderIdentifier[] = [];
	const namedImports = importDeclaration.getNamedImports();
	namedImports.forEach((namedImport) => {
		const name = namedImport.getName() as keyof typeof trancorder;
		if (ModuleInitialized.includes(name)) {
			const identifier = namedImport.getAliasNode() || namedImport.getNameNode();
			data.push(...findTrancorderSetupDefinitions(identifier, name));
		}
	});
	return data;
}

function processNamespaceImport(importDeclaration: ImportDeclaration): TrancorderIdentifier[] {
	const data: TrancorderIdentifier[] = [];
	const namespaceImport = importDeclaration.getNamespaceImport();

	if (!namespaceImport) {
		return [];
	}

	namespaceImport.findReferences().forEach((reference) => {
		data.push(...processNamespaceSetupExpression(reference, namespaceImport.getText()));
	});
	return data;
}

//require keyword

function processRequires(call: CallExpression): [Identifier | null, string | null] {
	const identifier = call.getFirstChildByKind(SyntaxKind.Identifier);
	if (identifier && identifier.getText() === "require") {
		const literal = call
			.getFirstChildByKind(SyntaxKind.SyntaxList)
			?.getFirstChildByKind(SyntaxKind.StringLiteral);
		return [identifier, (literal && literal.getLiteralText()) ?? null];
	}
	return [null, null];
}

function processNamedRequires(identifier: Identifier): TrancorderIdentifier[] {
	const variableDeclaration = identifier.getFirstAncestorByKind(SyntaxKind.VariableDeclaration);

	if (!variableDeclaration) {
		return [];
	}

	return [
		...processNamedRequiresObjectBindingPattern(variableDeclaration),
		...processNamedRequiresNamespace(variableDeclaration),
	];
}

function processNamedRequiresObjectBindingPattern(
	variableDeclaration: VariableDeclaration
): TrancorderIdentifier[] {
	const data: TrancorderIdentifier[] = [];

	const objectBindingPattern = variableDeclaration.getFirstChildByKind(
		SyntaxKind.ObjectBindingPattern
	);

	if (!objectBindingPattern) {
		return [];
	}

	const variables = objectBindingPattern.getElements();
	variables.forEach((variable) => {
		const name = variable.getText() as keyof typeof trancorder;
		if (ModuleInitialized.includes(name)) {
			const identifier = variable.getNameNode() as Identifier;
			data.push(...findTrancorderSetupDefinitions(identifier, name));
		}
	});
	return data;
}

function processNamedRequiresNamespace(
	variableDeclaration: VariableDeclaration
): TrancorderIdentifier[] {
	const data: TrancorderIdentifier[] = [];

	const variableIdentifier = variableDeclaration.getFirstChildByKind(SyntaxKind.Identifier);

	if (!variableIdentifier) {
		return [];
	}

	variableIdentifier.findReferences().forEach((reference) => {
		data.push(...processNamespaceSetupExpression(reference, variableIdentifier.getText()));
	});
	return data;
}

//universals

function processPropertyAccessExpression(root: string, path: Identifier[]): TrancorderIdentifier[] {
	const data: TrancorderIdentifier[] = [];

	if (path.length === 2) {
		const identifier = path[1];
		const name = identifier.getText() as keyof typeof trancorder;
		if (path[0].getText() === root && ModuleInitialized.includes(name)) {
			data.push({
				type: name === "localTrancorder" ? "local" : "remote",
				identifier,
			});
		}
	}
	return data;
}

function processNamespaceSetupExpression(reference: ReferencedSymbol, root: string) {
	const data: TrancorderIdentifier[] = [];

	const references = reference.getReferences();
	references.forEach((ref) => {
		if (!ref.isDefinition()) {
			const ancestors = ref.getNode().getAncestors();
			ancestors.forEach((ancestor) => {
				switch (ancestor.getKind()) {
					case SyntaxKind.PropertyAccessExpression: {
						const path = ancestor.getChildrenOfKind(SyntaxKind.Identifier);
						data.push(...processPropertyAccessExpression(root, path));
						break;
					}
				}
			});
		}
	});

	return data;
}

function findTrancorderSetupDefinitions(identifier: Identifier, name: string) {
	const data: TrancorderIdentifier[] = [];
	identifier.findReferences().forEach((reference) => {
		const references = reference.getReferences();
		references.forEach((ref) => {
			if (!ref.isDefinition() && ref.getNode().isKind(SyntaxKind.Identifier)) {
				const identifier = ref.getNode() as Identifier;
				data.push({
					identifier,
					type: name === "localTrancorder" ? "local" : "remote",
				});
			}
		});
	});
	return data;
}

//setup find

type TrancorderSetup = {
	type: "local" | "remote";
	exports: Array<Identifier>;
	usages: Array<TrancorderUsageResult>;
};

function searchTrancorderSetup(cwd: string, data: TrancorderIdentifier[]): TrancorderSetup[] {
	return data.map(({ identifier, type }) => {
		const { exports, usages } = processExportedT(cwd, identifier);

		return {
			type,
			exports,
			usages,
		};
	});
}

type ExportedT = {
	exports: Array<Identifier>;
	usages: Array<TrancorderUsageResult>;
};

function processExportedT(cwd: string, identifier: Identifier): ExportedT {
	return processVariableDeclaration(cwd, identifier);
}

function processVariableDeclaration(cwd: string, identifier: Identifier): ExportedT {
	const variableDeclaration = identifier.getFirstAncestorByKind(SyntaxKind.VariableDeclaration);

	if (!variableDeclaration) {
		return {
			exports: [],
			usages: [],
		};
	}

	const processedBinding = processObjectBindingPattern(cwd, variableDeclaration);
	const processedIdentifier = processIdentifier(cwd, variableDeclaration);

	return {
		usages: [...processedBinding.usages, ...processedIdentifier.usages],
		exports: [...processedBinding.exports, ...processedIdentifier.exports],
	};
}

function processObjectBindingPattern(cwd: string, node: Node): ExportedT {
	const objectBindingPattern = node.getFirstChildByKind(SyntaxKind.ObjectBindingPattern);

	if (!objectBindingPattern) {
		return {
			exports: [],
			usages: [],
		};
	}

	const data: ExportedT = { exports: [], usages: [] };
	const variables = objectBindingPattern.getElements();
	variables.forEach((variable) => {
		const name = variable.getText() as keyof trancorder.Messages;
		//found translation variable
		if (name === "t") {
			const identifier = variable.getNameNode() as Identifier;
			identifier.findReferences().forEach((reference) => {
				reference.getReferences().forEach((ref) => {
					const node = ref.getNode() as Identifier;
					if (!ref.isDefinition() && node.isKind(SyntaxKind.Identifier)) {
						data.exports.push(...processExported(node));
						data.usages.push(...trancorderUsageSearch(cwd, node));
					}
				});
			});
		}
	});
	return data;
}

function processIdentifier(cwd: string, node: Node): ExportedT {
	const variableIdentifier = node.getFirstChildByKind(SyntaxKind.Identifier);

	if (!variableIdentifier) {
		return {
			exports: [],
			usages: [],
		};
	}

	const data: ExportedT = { exports: [], usages: [] };
	variableIdentifier.findReferences().forEach((references) => {
		references.getReferences().forEach((ref) => {
			const node = ref.getNode();
			if (!ref.isDefinition()) {
				const processed = processVariableStatement(cwd, node);
				data.exports.push(...processed.exports);
				data.usages.push(...processed.usages);
			}
		});
	});
	return data;
}

function processVariableStatement(cwd: string, node: Node): ExportedT {
	const variableStatement = node.getFirstAncestorByKind(SyntaxKind.VariableStatement);

	if (!variableStatement) {
		return {
			exports: [],
			usages: [],
		};
	}

	const data: ExportedT = { exports: [], usages: [] };
	const syntaxList = variableStatement.getFirstChildByKind(SyntaxKind.SyntaxList);

	if (syntaxList && syntaxList.getFirstChildByKind(SyntaxKind.ExportKeyword)) {
		const variableDeclaration = node.getFirstAncestorByKind(SyntaxKind.VariableDeclaration);
		const variableIdentifier =
			variableDeclaration && variableDeclaration.getFirstChildByKind(SyntaxKind.Identifier);

		if (variableIdentifier) {
			data.exports.push(variableIdentifier);
			variableIdentifier.findReferences().forEach((references) => {
				references.getReferences().forEach((ref) => {
					const node = ref.getNode() as Identifier;
					if (!ref.isDefinition() && node.isKind(SyntaxKind.Identifier)) {
						data.usages.push(...trancorderUsageSearch(cwd, node));
					}
				});
			});
		}
	}
	return data;
}

function processExported(node: Identifier): Array<Identifier> {
	const isExported = node.getFirstAncestorByKind(SyntaxKind.ExportDeclaration);

	if (isExported) {
		return [node];
	}
	return [];
}

function relativizePath(cwd: string, pth: string) {
	if (path.isAbsolute(pth)) {
		return path.relative(cwd, pth).replace(/\\/g, "/");
	}
	return pth.replace(/\\/g, "/");
}
