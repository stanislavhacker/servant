import {
	CallExpression,
	EnumMember,
	Expression,
	Identifier,
	Node,
	NumericLiteral,
	ObjectLiteralExpression,
	PropertyAssignment,
	StringLiteral,
	SyntaxKind,
} from "ts-morph";
import * as path from "path";

import { DYNAMIC } from "./const";

const SKIP = {};

export type TrancorderUsageResult =
	| TrancorderSimpleUsageResult
	| TrancorderEnumSimpleUsageResult
	| TrancorderEnumComplexUsageResult;

export type TrancorderDefaultUsageResult = {
	file: string;
	start: { line: number; column: number };
	end: { line: number; column: number };
};

export type TrancorderSimpleUsageResult = TrancorderDefaultUsageResult & {
	type: "simple";
	key: string;
	values: object;
	hasOptions: boolean;
};

export type TrancorderEnumSimpleUsageResult = TrancorderDefaultUsageResult & {
	type: "enum-simple";
	key: string;
	enumObj: object;
	values: object;
	hasOptions: boolean;
};

export type TrancorderEnumComplexUsageResult = TrancorderDefaultUsageResult & {
	type: "enum-complex";
	keysObj: Record<string | number, string>;
	enumObj: object;
	values: object;
	hasOptions: boolean;
};

export function trancorderUsageSearch(cwd: string, node: Identifier): Array<TrancorderUsageResult> {
	const call = node.getFirstAncestorByKind(SyntaxKind.CallExpression);

	if (!call) {
		return [];
	}

	const list = call.getFirstChildByKind(SyntaxKind.SyntaxList);

	if (!list) {
		return [];
	}

	const params = list
		.getChildren()
		.filter(
			(node) =>
				node.isKind(SyntaxKind.Identifier) ||
				node.isKind(SyntaxKind.StringLiteral) ||
				node.isKind(SyntaxKind.ObjectLiteralExpression)
		);

	return [
		...processTForString(cwd, call, node, params),
		...processTForSimpleEnum(cwd, call, node, params),
		...processTForComposedEnum(cwd, call, node, params),
	];
}

function processTForString(
	cwd: string,
	call: CallExpression,
	node: Identifier,
	params: Array<Node>
): Array<TrancorderSimpleUsageResult> {
	const [text, valuesObject, optionsObject] = params;

	if (text.isKind(SyntaxKind.StringLiteral)) {
		const key = processMessageKey(text);
		const values = processValues(valuesObject) || {};
		const hasOptions = processOptions(optionsObject);
		const file = relativizePath(cwd, node.getSourceFile().getFilePath().toString());

		const start = call.getSourceFile().getLineAndColumnAtPos(call.getStart());
		const end = call.getSourceFile().getLineAndColumnAtPos(call.getEnd());

		return [{ type: "simple", key, values, hasOptions, file, start, end }];
	}
	return [];
}

function processTForSimpleEnum(
	cwd: string,
	call: CallExpression,
	node: Identifier,
	params: Array<Node>
): Array<TrancorderEnumSimpleUsageResult> {
	const [identifier, text, valuesObject, optionsObject] = params;

	if (identifier.isKind(SyntaxKind.Identifier) && text.isKind(SyntaxKind.StringLiteral)) {
		const enumObj = processEnum(identifier);
		const key = processMessageKey(text);
		const values = processValues(valuesObject) || {};
		const hasOptions = processOptions(optionsObject);
		const file = relativizePath(cwd, node.getSourceFile().getFilePath().toString());

		const start = call.getSourceFile().getLineAndColumnAtPos(call.getStart());
		const end = call.getSourceFile().getLineAndColumnAtPos(call.getEnd());

		return [{ type: "enum-simple", key, values, enumObj, hasOptions, file, start, end }];
	}
	return [];
}

function processTForComposedEnum(
	cwd: string,
	call: CallExpression,
	node: Identifier,
	params: Array<Node>
): Array<TrancorderEnumComplexUsageResult> {
	const [identifier, obj, valuesObject, optionsObject] = params;

	if (
		identifier.isKind(SyntaxKind.Identifier) &&
		obj.isKind(SyntaxKind.ObjectLiteralExpression)
	) {
		const enumObj = processEnum(identifier);
		const keysObj = processEnumsKeys(obj);
		const values = processValues(valuesObject) || {};
		const hasOptions = processOptions(optionsObject);
		const file = relativizePath(cwd, node.getSourceFile().getFilePath().toString());

		const start = call.getSourceFile().getLineAndColumnAtPos(call.getStart());
		const end = call.getSourceFile().getLineAndColumnAtPos(call.getEnd());

		return [{ type: "enum-complex", keysObj, values, enumObj, hasOptions, file, start, end }];
	}
	return [];
}

function processMessageKey(literal: StringLiteral) {
	return literal.getLiteralText();
}

//VALUES

export function processValues(node: Node | undefined) {
	//array
	if (node && node.isKind(SyntaxKind.ArrayLiteralExpression)) {
		const obj: unknown[] = [];
		node.getChildrenOfKind(SyntaxKind.SyntaxList).forEach((list) => {
			obj.push(
				...list
					.getChildren()
					.map((item) => processValue(item))
					.filter((item) => item !== SKIP)
			);
		});
		return obj;
	}
	//object
	if (node && node.isKind(SyntaxKind.ObjectLiteralExpression)) {
		const obj = {};
		node.getChildrenOfKind(SyntaxKind.SyntaxList).forEach((list) => {
			list.getChildrenOfKind(SyntaxKind.PropertyAssignment).forEach((property) => {
				const name = property.getNameNode();
				const key = getNodeName(name);
				if (key !== undefined) {
					obj[key] = processValue(property.getInitializer());
				}
			});
			list.getChildrenOfKind(SyntaxKind.ShorthandPropertyAssignment).forEach((property) => {
				const name = property.getNameNode();
				const key = getNodeName(name);
				if (key !== undefined) {
					obj[key] = DYNAMIC;
				}
			});
		});
		return obj;
	}
	return null;
}

function processValue(initializer?: Node) {
	switch (initializer?.getKind()) {
		case SyntaxKind.ObjectLiteralExpression:
			return processValues(initializer);
		case SyntaxKind.ArrayLiteralExpression:
			return processValues(initializer);
		case SyntaxKind.StringLiteral:
			return (initializer as StringLiteral).getLiteralText();
		case SyntaxKind.NumericLiteral:
			return parseFloat((initializer as NumericLiteral).getLiteralText());
		case SyntaxKind.TrueKeyword:
			return true;
		case SyntaxKind.FalseKeyword:
			return false;
		case SyntaxKind.Identifier:
			return DYNAMIC;
		default:
			return SKIP;
	}
}

//ENUMS

export function processEnum(identifier: Identifier): object {
	const enumObj = {};
	identifier.getImplementations().forEach((impl) => {
		const node = impl.getNode();
		processEnumAsDeclaration(node, enumObj);
		processEnumAsVariableDeclaration(node, enumObj);
	});
	return enumObj;
}

export function processEnumsKeys(node: ObjectLiteralExpression): Record<string | number, string> {
	const keys: Record<string | number, string> = {};
	node.getDescendantsOfKind(SyntaxKind.PropertyAssignment).forEach((property) => {
		processEnumProperty(property, keys);
	});
	return keys;
}

function processEnumAsDeclaration(node: Node, enumObj: object) {
	const enumDeclaration = node.isKind(SyntaxKind.EnumDeclaration)
		? node
		: node.getFirstAncestorByKind(SyntaxKind.EnumDeclaration);

	if (!enumDeclaration) {
		return;
	}

	const members = enumDeclaration.getMembers();
	members.forEach((member) => {
		processEnumProperty(member, enumObj);
	});
}

function processEnumAsVariableDeclaration(node: Node, enumObj: object) {
	const variableDeclaration = node.isKind(SyntaxKind.VariableDeclaration)
		? node
		: node.getFirstAncestorByKind(SyntaxKind.VariableDeclaration);

	if (!variableDeclaration) {
		return;
	}

	const initializer = variableDeclaration.getInitializer();
	if (initializer?.isKind(SyntaxKind.ObjectLiteralExpression)) {
		initializer?.getDescendantsOfKind(SyntaxKind.PropertyAssignment).forEach((assignment) => {
			processEnumProperty(assignment, enumObj);
		});
	}
}

function processEnumProperty(node: EnumMember | PropertyAssignment, enumObj: object) {
	const name = node.getNameNode();
	const initializer = node.getInitializer();
	const value = getInitializerValue(initializer);
	const key = getNodeName(name);

	if (key !== undefined) {
		enumObj[key] = value;
	}
}

//OPTIONS

function processOptions(optionsObject: Node | undefined): boolean {
	return Boolean(optionsObject && optionsObject.isKind(SyntaxKind.ObjectLiteralExpression));
}

//utils

function getNodeName(node: Node): string | number | undefined {
	if (node.isKind(SyntaxKind.Identifier)) {
		return node.getText();
	}
	if (node.isKind(SyntaxKind.NumericLiteral)) {
		return node.getLiteralValue();
	}
	if (node.isKind(SyntaxKind.StringLiteral)) {
		return node.getLiteralText();
	}
	if (node.isKind(SyntaxKind.ComputedPropertyName)) {
		const path = node
			.getFirstDescendantByKind(SyntaxKind.PropertyAccessExpression)
			?.getChildren()
			.filter((node) => node.isKind(SyntaxKind.Identifier));
		const what = path?.reverse()[0] as Identifier;
		if (what) {
			const key = what.getText();
			let found: string | number | undefined;
			what.getImplementations().forEach((impl) => {
				const node = impl.getNode();
				if (found === undefined) {
					found =
						getNodeNameAsDeclaration(node, key) ||
						getNodeNameAsVariableDeclaration(node, key);
				}
			});
			return found;
		}
	}
	return undefined;
}

function getNodeNameAsDeclaration(node: Node, key: string): string | number | undefined {
	const enumDeclaration = node.getFirstAncestorByKind(SyntaxKind.EnumDeclaration);

	if (!enumDeclaration) {
		return undefined;
	}

	const found = enumDeclaration.getMembers().find((member) => {
		const name = getNodeName(member.getNameNode());
		return name === key;
	});
	return found?.getValue();
}

function getNodeNameAsVariableDeclaration(node: Node, key: string): string | number | undefined {
	const variableDeclaration = node.getFirstAncestorByKind(SyntaxKind.VariableDeclaration);

	if (!variableDeclaration) {
		return undefined;
	}

	const initializer = variableDeclaration.getInitializer();
	if (initializer?.isKind(SyntaxKind.ObjectLiteralExpression)) {
		const found = initializer
			?.getDescendantsOfKind(SyntaxKind.PropertyAssignment)
			.find((assignment) => {
				const name = getNodeName(assignment.getNameNode());
				return name === key;
			});
		return getInitializerValue(found?.getInitializer());
	}
	return undefined;
}

function getInitializerValue(initializer?: Expression) {
	if (initializer?.isKind(SyntaxKind.StringLiteral)) {
		return initializer.getLiteralText();
	}
	if (initializer?.isKind(SyntaxKind.NumericLiteral)) {
		return initializer.getLiteralValue();
	}
	return undefined;
}

function relativizePath(cwd: string, pth: string) {
	if (path.isAbsolute(pth)) {
		return path.relative(cwd, pth).replace(/\\/g, "/");
	}
	return pth.replace(/\\/g, "/");
}
