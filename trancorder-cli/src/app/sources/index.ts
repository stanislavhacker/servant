import { TrancorderOptions } from "../types";

import { loadSources } from "./load";
import { ValidatedSourcesResult, validateSources } from "./validate";

export type ProcessSourcesResult = ValidatedSourcesResult;

export function processSources({
	cwd,
	sources,
	debug,
	tsConfigFilePath,
	tsCompilerOptions,
}: Required<TrancorderOptions>): Promise<ProcessSourcesResult> {
	return loadSources(cwd, sources, { debug, tsCompilerOptions, tsConfigFilePath }).then(
		(results) => validateSources(results, { debug })
	);
}
