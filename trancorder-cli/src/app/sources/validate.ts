import { TrancorderError, TrancorderErrorCode } from "../types";
import { TrancorderSetupResult, trancorderSetupSearch } from "../ast";

import { LoadSourcesResult } from "./load";
import { newError } from "../errors/index";

export type ValidatedSourcesResult = {
	cwd: string;
	sources: string[];
	setup: TrancorderSetupResult | null;
	errors: TrancorderError[];
};

export type ValidateSourcesOptions = {
	debug?: boolean;
};

export function validateSources(
	{ cwd, sources, program, errors }: LoadSourcesResult,
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	opts?: ValidateSourcesOptions
): Promise<ValidatedSourcesResult> {
	const setups = trancorderSetupSearch(cwd, program, program.getSourceFiles());
	const [setup, setupErrors] = validateSetup(setups);

	return Promise.resolve({
		cwd,
		sources,
		setup,
		errors: [...errors, ...setupErrors],
	});
}

function validateSetup(
	setups: TrancorderSetupResult[]
): [TrancorderSetupResult | null, TrancorderError[]] {
	if (setups.length === 0) {
		return [null, [newError(TrancorderErrorCode.NoTrancorderSetupFound, [])]];
	}
	if (setups.length > 1) {
		const paths = setups.map((s) => s.path);
		return [
			null,
			[newError(TrancorderErrorCode.MoreTrancorderSetupsFound, [paths.join(", ")])],
		];
	}

	const setup = setups[0];
	if (setup.exports.length === 0) {
		return [null, [newError(TrancorderErrorCode.NoExportOfT, [setup.path])]];
	}
	if (setup.exports.length > 1) {
		return [null, [newError(TrancorderErrorCode.MoreExportsOfT, [setup.path])]];
	}

	return [setup, []];
}
