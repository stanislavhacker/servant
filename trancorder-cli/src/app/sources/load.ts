import { CompilerOptions, ModuleKind, Project, ProjectOptions, ScriptTarget } from "ts-morph";
import { JsxEmit } from "typescript";
import * as path from "path";

import { TrancorderError, TrancorderErrorCode } from "../types";
import { existsFile, loadFiles, LoadFilesResult } from "../io";
import { newError } from "../errors";

export type LoadSourcesResult = {
	cwd: string;
	program: Project;
	sources: string[];
	errors: TrancorderError[];
};

export type LoadSourcesOptions = {
	debug?: boolean;
	tsConfigFilePath?: string;
	tsCompilerOptions?: CompilerOptions;
};

export const DefaultCompilerOptions = {
	module: ModuleKind.CommonJS,
	target: ScriptTarget.ES2015,
	sourceMap: false,
	declaration: false,
	jsx: JsxEmit.React,
	skipLibCheck: true,
	strictNullChecks: true,
};

export function loadSources(
	cwd: string,
	sources: string[],
	opts?: LoadSourcesOptions
): Promise<LoadSourcesResult> {
	return loadFiles(cwd, sources).then((loaded) => createTypescriptProgram(cwd, loaded, opts));
}

function createTypescriptProgram(
	cwd: string,
	{ files, errors }: LoadFilesResult,
	opts: LoadSourcesOptions = {}
): LoadSourcesResult {
	const program = new Project({
		skipAddingFilesFromTsConfig: true,
		...loadOptions(cwd, opts),
	});
	const errs = [...errors];

	const sourcesFiltered = files.filter((source) => !source.includes(".d.ts"));
	sourcesFiltered.forEach((source) => {
		try {
			program.addSourceFileAtPath(path.join(cwd, source));
		} catch (e) {
			errs.push(newError(TrancorderErrorCode.CanNotAddSourceFile, [source], false, e));
		}
	});

	return {
		cwd,
		program,
		errors: errs,
		sources: sourcesFiltered,
	};
}

function loadOptions(
	cwd: string,
	opts: LoadSourcesOptions = {}
): Pick<ProjectOptions, "tsConfigFilePath" | "compilerOptions"> {
	const config = opts.tsConfigFilePath ? path.join(cwd, opts.tsConfigFilePath) : null;
	const configExists = config ? existsFile(config) : false;

	if (configExists) {
		return {
			tsConfigFilePath: opts.tsConfigFilePath,
		};
	}

	return {
		compilerOptions: opts.tsCompilerOptions || DefaultCompilerOptions,
	};
}
