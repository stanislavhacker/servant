import { TrancorderError } from "../types";

export type ExporterFile = {
	id: string;
	file: string;
	content: string;
};

export type ExporterFiles = ExporterFile[];

export type ImportedFile = {
	file: string;
	content: string;
};

export type ImportedFiles = {
	files: ImportedFile[];
	errors: TrancorderError[];
};
