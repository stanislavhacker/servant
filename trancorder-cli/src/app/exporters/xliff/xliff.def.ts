export type Xliff = {
	version?: string;
	srcLang?: string;
	trgLang?: string;
	file?: XliffFile;
};

export type XliffFile = {
	id?: string;
	original?: string;
	items: Array<XliffGroup | XliffUnit>;
};

export type XliffGroup = {
	type: "group";
	id?: string;
	notes?: XliffNote[];
	items?: Array<XliffGroup | XliffUnit>;
};

export type XliffUnit = {
	type: "unit";
	id?: string;
	notes?: XliffNote[];
	segment?: {
		target?: XliffTarget;
		source?: XliffTarget;
	};
};

export type XliffNote = {
	category?: string;
	priority?: number;
	note: string;
};

export type XliffPlaceholder = {
	id?: string | number;
	equiv?: string;
	disp?: string;
};

export type XliffTarget = Array<string | XliffPlaceholder>;
