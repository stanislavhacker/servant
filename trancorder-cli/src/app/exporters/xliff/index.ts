/* eslint-disable @typescript-eslint/no-explicit-any */
import { LanguageTranslationFull, LanguageTranslations } from "trancorder";
import * as xml2js from "xml2js";

import {
	ImportResults,
	TrancorderError,
	TrancorderErrorCode,
	TrancorderOptions,
	ValidateProjectResults,
} from "../../types";
import { ExporterFiles, ImportedFile, ImportedFiles } from "../types";
import { convertToFull } from "../../project";
import { newError } from "../../errors";
import {
	getMessagePart,
	getTranslationInfo,
	importTranslationFileInfo,
	RemappedTranslationFileInfo,
	TranslationFileInfo,
} from "../utils";

import {
	Xliff,
	XliffFile,
	XliffGroup,
	XliffNote,
	XliffPlaceholder,
	XliffTarget,
	XliffUnit,
} from "./xliff.def";

const THIS = "this";

export function exportXliffFiles(
	results: ValidateProjectResults,
	options: Required<Pick<TrancorderOptions, "defaultLanguage">>
): ExporterFiles {
	const fromTranslation = getTranslationInfo(options.defaultLanguage, results);

	const languages = Object.keys(results.languages).filter(
		(lang) => lang !== fromTranslation.language
	);
	return languages.map((language) => {
		const toTranslation = getTranslationInfo(language, results);
		const id = Math.random().toString(36).slice(2);
		return {
			id,
			file: `${language}.xliff`,
			content: convertToXml(id, fromTranslation, toTranslation),
		};
	});
}

export function importXliffFiles(
	results: ValidateProjectResults,
	files: ImportedFiles
): ImportResults {
	const imports = files.files
		.map(remapFromXml)
		.map((remapped) =>
			importTranslationFileInfo(remapped, results.translations[remapped.file])
		);

	return {
		imports,
		errors: [],
	};
}

//imports

function remapFromXml({ file, content }: ImportedFile): RemappedTranslationFileInfo {
	return {
		...parseFromXml(content),
		source: file,
	};
}

function parseFromXml(content: string): TranslationFileInfo & { errors: TrancorderError[] } {
	let object: Xliff = {};
	let errors: TrancorderError[] = [];
	xml2js.parseString(
		content,
		{
			preserveChildrenOrder: true,
			charsAsChildren: true,
			explicitChildren: true,
			includeWhiteChars: true,
		},
		(err, data) => {
			[object, errors] = parseRoot(data);
		}
	);

	const toLanguage = object.trgLang ?? "";
	const targetFile = object.file?.original ?? "";

	const translation =
		object.file?.items.reduce((translations, item) => {
			//NOTE: Unit in file directly not supported by trancorder
			//NOTE: Group in group not supported by trancorder
			if (item.type === "group" && item.items && item.id) {
				const translation: LanguageTranslationFull = { value: "" };
				item.items.forEach((item) => {
					if (item.type === "unit") {
						if (item.id === THIS) {
							translation.value = parseValue(item.segment?.target);
						} else if (item.id) {
							const alt = (translation.alternatives = translation.alternatives || {});
							alt[item.id] = { value: parseValue(item.segment?.target) };
						}
					}
				});
				translations[item.id] = translation;
			}
			return translations;
		}, {} as LanguageTranslations) ?? ({} as LanguageTranslations);

	return {
		errors,
		translation,
		file: targetFile,
		language: toLanguage,
	};
}

function parseValue(target: XliffTarget | undefined) {
	if (target) {
		return target.reduce((prev, item) => {
			if (typeof item === "string") {
				return prev + item;
			}
			return prev + (item.disp ?? "");
		}, "") as string;
	}
	return "";
}

function parseRoot(data: any): [Xliff, TrancorderError[]] {
	if (data && data.xliff) {
		const { version = "1.0", srcLang = "", trgLang = "" } = data.xliff.$ || {};
		const [file, errors] = parseFile(data.xliff.$$);
		return [{ version, srcLang, trgLang, file }, errors];
	}
	return [{}, [newError(TrancorderErrorCode.XliffStructureIsNotValid, [])]];
}

function parseFile(data: Array<any>): [XliffFile, TrancorderError[]] {
	const fileNode = data.filter((item) => item["#name"] === "file")[0] || null;
	if (fileNode) {
		const { id = "", original = "" } = fileNode.$ || {};
		const [groups, gerrors] = parseGroups(fileNode.$$ || []);
		const [units, uerrors] = parseUnits(fileNode.$$ || []);
		return [{ id, original, items: [...groups, ...units] }, [...gerrors, ...uerrors]];
	}
	return [{ items: [] }, [newError(TrancorderErrorCode.XliffStructureIsNotValid, [])]];
}

function parseGroups(data: Array<any>): [Array<XliffGroup>, TrancorderError[]] {
	const items: Array<XliffGroup> = [];
	const errs: TrancorderError[] = [];
	data.forEach((item) => {
		switch (item["#name"]) {
			case "group": {
				const [group, errors] = parseGroup(item);
				items.push(group);
				errs.push(...errors);
				break;
			}
			default:
				break;
		}
	});
	return [items, errs];
}

function parseUnits(data: Array<any>): [Array<XliffUnit>, TrancorderError[]] {
	const items: Array<XliffUnit> = [];
	const errs: TrancorderError[] = [];
	data.forEach((item) => {
		switch (item["#name"]) {
			case "unit": {
				const [unit, errors] = parseUnit(item);
				items.push(unit);
				errs.push(...errors);
				break;
			}
			default:
				break;
		}
	});
	return [items, errs];
}

function parseNotes(data: Array<any>): [Array<XliffNote>, TrancorderError[]] {
	const items: Array<XliffNote> = [];
	const errs: TrancorderError[] = [];
	data.forEach((item) => {
		switch (item["#name"]) {
			case "notes": {
				const [note, errors] = parseNotes(item.$$ || []);
				items.push(...note);
				errs.push(...errors);
				break;
			}
			case "note": {
				const [note, errors] = parseNote(item);
				items.push(note);
				errs.push(...errors);
				break;
			}
			default:
				break;
		}
	});
	return [items, errs];
}

function parseGroup(data: any): [XliffGroup, TrancorderError[]] {
	const { id = "" } = data.$ || {};
	const [notes, nerrors] = parseNotes(data.$$ || []);
	const [items, ierrors] = parseUnits(data.$$ || []);
	return [{ type: "group", id, notes, items }, [...nerrors, ...ierrors]];
}

function parseUnit(data: any): [XliffUnit, TrancorderError[]] {
	const { id = "" } = data.$ || {};
	const [notes, nerrors] = parseNotes(data.$$ || []);
	const [source, target, serrors] = parseSegment(data.segment[0]);
	return [{ type: "unit", id, notes, segment: { target, source } }, [...nerrors, ...serrors]];
}

function parseNote(data: any): [XliffNote, TrancorderError[]] {
	const { category = "", priority = 0 } = data.$ || {};
	const note = data._;
	return [{ category, priority, note }, []];
}

function parseSegment(data: any): [XliffTarget, XliffTarget, TrancorderError[]] {
	const [source, serrors] = parseTarget(data.source[0] || null);
	const [target, terrors] = parseTarget(data.target[0] || null);

	return [source, target, [...serrors, ...terrors]];
}

function parseTarget(data: any): [XliffTarget, TrancorderError[]] {
	if (data) {
		const items = data.$$ || [];
		const target: XliffTarget = [];
		const errs: TrancorderError[] = [];

		items.forEach((item) => {
			switch (item["#name"]) {
				case "__text__":
					target.push(item._);
					break;
				case "ph": {
					const [ph, errors] = parsePlaceholder(item);
					errs.push(...errors);
					target.push(ph);
					break;
				}
				default:
					break;
			}
		});
		return [target, errs];
	}
	return [[], []];
}

function parsePlaceholder(data: any): [XliffPlaceholder, TrancorderError[]] {
	const { id = "", disp = "", equiv = "" } = data.$ || {};
	return [{ id, disp, equiv }, []];
}

//exports

function convertToXml(
	id: string,
	fromTranslation: TranslationFileInfo,
	toTranslation: TranslationFileInfo
): string {
	const xliff = buildXliffDefinition(id, fromTranslation, toTranslation);
	const text = convertXliffToXml(xliff);
	return text;
}

function buildXliffDefinition(
	id: string,
	fromTranslation: TranslationFileInfo,
	toTranslation: TranslationFileInfo
): Xliff {
	return {
		version: "2.0",
		trgLang: toTranslation.language,
		srcLang: fromTranslation.language,
		file: {
			id,
			original: toTranslation.file,
			items: Object.keys(fromTranslation.translation).reduce((prev, key) => {
				const from = convertToFull(fromTranslation.translation, key);
				const to = convertToFull(toTranslation.translation, key);

				const translateMain = canTranslate(from, to) && mainNeedTranslate(key, from, to);

				if (translateMain) {
					prev.push({
						id: key,
						notes: [],
						type: "group",
						items: [
							buildXliffTranslation(THIS, from, to),
							...buildXliffAlternatives(from, to),
						],
					});
				}
				return prev;
			}, [] as Required<Xliff>["file"]["items"]),
		},
	};
}

function buildXliffTranslation(
	id: string,
	from: LanguageTranslationFull,
	to: LanguageTranslationFull
): XliffUnit {
	return {
		type: "unit",
		id,
		notes: canAddNotes(from, to) ? [{ note: to.description || from.description || "" }] : [],
		segment: {
			target: [],
			source: buildXliffSource(from.value),
		},
	};
}

function buildXliffSource(source: string): Array<string | XliffPlaceholder> {
	const parts = getMessagePart(source);

	let id = 0;
	return parts.reduce((prev, { value, type }) => {
		if (type === "text") {
			prev.push(value);
		} else if (type === "replacer") {
			prev.push({
				id: id++,
				disp: value,
			});
		}
		return prev;
	}, [] as Array<string | XliffPlaceholder>);
}

function buildXliffAlternatives(
	from: LanguageTranslationFull,
	to: LanguageTranslationFull
): XliffUnit[] {
	const alternativesFrom = from.alternatives || {};
	const alternativesTo = to.alternatives || {};
	const expressions = [
		...new Set([...Object.keys(alternativesFrom), ...Object.keys(alternativesTo)]),
	];
	return expressions.reduce((prev, expr) => {
		const fromAltExists = alternativesFrom[expr];
		const toAltExists = alternativesTo[expr];

		if (fromAltExists && toAltExists) {
			const fromAlt = convertToFull(alternativesFrom, expr);
			const toAlt = convertToFull(alternativesTo, expr);

			const translateAlternative =
				canTranslate(fromAlt, toAlt) && alternativeNeedTranslate(fromAlt, toAlt);

			if (translateAlternative) {
				prev.push(buildXliffTranslation(expr, fromAlt, toAlt));
			}
		}
		return prev;
	}, [] as XliffUnit[]);
}

function convertXliffToXml(xliff: Xliff): string {
	return [
		`<?xml version="1.0" encoding="UTF-8"?>`,
		`<xliff${convertAttributes({
			version: xliff.version,
			xmlns: "urn:oasis:names:tc:xliff:document:2.0",
			srcLang: xliff.srcLang,
			trgLang: xliff.trgLang,
		})}>`,
		...(xliff.file ? convertXliffFileToXml(xliff.file) : []),
		`</xliff>`,
	].join("");
}

function convertXliffFileToXml(file: XliffFile): string {
	return [
		`<file${convertAttributes({ id: file.id, original: file.original })}>`,
		...convertXliffItemsToXml(file.items),
		`</file>`,
	].join("");
}

function convertXliffItemsToXml(items: Array<XliffGroup | XliffUnit>): string {
	return items
		.map((item) => {
			if (item.type === "group") {
				return convertXliffGroupToXml(item);
			}
			if (item.type === "unit") {
				return convertXliffUnitToXml(item);
			}
			return "";
		})
		.join("");
}

function convertXliffGroupToXml(group: XliffGroup): string {
	return [
		`<group${convertAttributes({ id: group.id })}>`,
		...convertXliffNotesToXml(group.notes || []),
		...convertXliffItemsToXml(group.items || []),
		`</group>`,
	].join("");
}

function convertXliffUnitToXml(unit: XliffUnit): string {
	return [
		`<unit${convertAttributes({ id: unit.id })}>`,
		...convertXliffNotesToXml(unit.notes || []),
		`<segment>`,
		...(unit.segment?.source && unit.segment.source.length > 0
			? `<source>${convertXliffTargetToXml(unit.segment.source)}</source>`
			: []),
		...(unit.segment?.target && unit.segment.target.length > 0
			? `<target>${convertXliffTargetToXml(unit.segment.target)}</target>`
			: []),
		`</segment>`,
		`</unit>`,
	].join("");
}

function convertXliffTargetToXml(data: XliffTarget): string {
	return data
		.map((item) => {
			if (typeof item === "string") {
				return convertText(item);
			}
			return `<ph${convertAttributes({ id: item.id, disp: item.disp, equiv: item.equiv })}/>`;
		})
		.join("");
}

function convertXliffNotesToXml(notes: XliffNote[]): string {
	if (notes.length > 0) {
		return [
			`<notes>`,
			...notes.map(
				(note) =>
					`<note${convertAttributes({
						priority: note.priority,
						category: note.category,
					})}>${convertText(note.note)}</note>`
			),
			`</notes>`,
		].join("");
	}
	return "";
}

function convertAttributes(attrs: Record<string, any>) {
	const keys = Object.keys(attrs);
	const items: string[] = [""];

	if (keys.length === 0) {
		return "";
	}

	keys.forEach((key) => {
		if (attrs[key] !== null && attrs[key] !== undefined) {
			items.push(`${key}="${escapeXml(attrs[key])}"`);
		}
	});
	return items.join(" ");
}

function convertText(text: string) {
	return escapeXml(text);
}

function canAddNotes(from: LanguageTranslationFull, to: LanguageTranslationFull) {
	return Boolean(to.description || from.description);
}

function canTranslate(from: LanguageTranslationFull | null, to: LanguageTranslationFull) {
	if (to.translate === true || to.translate === false) {
		return to.translate;
	}
	if (!from) {
		return true;
	}
	return from.translate !== false;
}

function mainNeedTranslate(
	key: string,
	from: LanguageTranslationFull,
	to: LanguageTranslationFull
) {
	if (!to.value) {
		return true;
	}
	if (to.value === key) {
		return true;
	}
	return to.value === from.value;
}

function alternativeNeedTranslate(
	from: LanguageTranslationFull | null,
	to: LanguageTranslationFull
) {
	if (!to.value) {
		return true;
	}
	return !!(from && to.value === from.value);
}

function escapeXml(unsafe: string | number | boolean) {
	return String(unsafe).replace(/[<>&'"]/g, (c) => {
		switch (c) {
			case "<":
				return "&lt;";
			case ">":
				return "&gt;";
			case "&":
				return "&amp;";
			case "'":
				return "&apos;";
			case '"':
				return "&quot;";
			default:
				return "";
		}
	});
}
