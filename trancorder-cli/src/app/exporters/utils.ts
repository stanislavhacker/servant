import { LanguageTranslationFull, LanguageTranslations } from "trancorder";

import {
	ImportResult,
	TrancorderError,
	TrancorderErrorCode,
	ValidateProjectResults,
} from "../types";
import { collectPointers, convertToFull } from "../project";
import { newError } from "../errors";

export type TranslationFileInfo = {
	translation: LanguageTranslations;
	language: string;
	file: string;
};

export function getTranslationInfo(
	lang: string,
	translations: ValidateProjectResults
): TranslationFileInfo {
	const translationFile = translations.languages[lang];
	return {
		translation: translations.translations[translationFile] || {},
		file: translationFile || "",
		language: lang,
	};
}

export type MessagePart = {
	value: string;
	type: "text" | "replacer";
};

export function getMessagePart(source: string): Array<MessagePart> {
	const pointers = collectPointers([source]).map((pointer) => `{${pointer}}`);

	const textsAndPointers = pointers.reduce(
		(prev, pointer) => {
			return prev.reduce((newItems, item) => {
				newItems.push(...item.split(new RegExp(`(${escapeRegExp(pointer)})`, "g")));
				return newItems;
			}, [] as string[]);
		},
		[source]
	);

	return textsAndPointers.map((value) => {
		if (pointers.includes(value)) {
			return {
				value,
				type: "replacer",
			};
		}
		return {
			value,
			type: "text",
		};
	});
}

export type RemappedTranslationFileInfo = TranslationFileInfo & {
	source: string;
	errors: TrancorderError[];
};

export function importTranslationFileInfo(
	remapped: RemappedTranslationFileInfo,
	translation?: LanguageTranslations
): ImportResult {
	if (!translation) {
		return {
			errors: [newError(TrancorderErrorCode.TargetTranslationNotFound, [remapped.file])],
			updated: [],
			translation: {},
			file: remapped.file,
			language: remapped.language,
			source: remapped.source,
		};
	}

	const errors: TrancorderError[] = [];
	const clone = JSON.parse(JSON.stringify(translation)) as LanguageTranslations;
	const remappedKeys = Object.keys(remapped.translation);
	const updated = remappedKeys.reduce((prev, key) => {
		//not found main in target
		if (clone[key] === undefined) {
			errors.push(
				newError(TrancorderErrorCode.TargetTranslationKeyNotFound, [key, remapped.file])
			);
			return prev;
		}
		//found
		const { updated, problems } = importTranslationKey(
			key,
			clone[key],
			convertToFull(remapped.translation, key)
		);
		clone[key] = updated;
		errors.push(...problems);
		return [...prev, key];
	}, [] as string[]);

	return {
		errors,
		updated,
		translation: clone,
		file: remapped.file,
		language: remapped.language,
		source: remapped.source,
	};
}

function importTranslationKey(
	key: string,
	cloneElement: string | LanguageTranslationFull,
	translationElement: LanguageTranslationFull
): { updated: string | LanguageTranslationFull; problems: TrancorderError[] } {
	//simple
	if (typeof cloneElement === "string") {
		return {
			updated: translationElement.value,
			problems: [],
		};
	}
	//objects
	const alternatives = cloneElement.alternatives || {};
	const keys = Object.keys(alternatives);
	const allProblems: TrancorderError[] = [];
	keys.forEach((exp) => {
		const { updated, problems } = importTranslationKey(
			`${key}.${exp}`,
			alternatives[exp],
			convertToFull(translationElement.alternatives, exp)
		);
		alternatives[exp] = updated;
		allProblems.push(...problems);
	});

	return {
		updated: { ...cloneElement, value: translationElement.value },
		problems: allProblems,
	};
}

function escapeRegExp(string) {
	return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}
