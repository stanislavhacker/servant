import * as path from "path";

import {
	ExportResults,
	ImportResults,
	TrancorderError,
	TrancorderErrorCode,
	TrancorderExportType,
	TrancorderOptions,
	ValidateProjectResults,
} from "../types";
import { updateLocaleFile } from "../generators/utils";
import { writeFile, loadFiles, readFile } from "../io";
import { newError } from "../errors";

import { ExporterFiles, ImportedFiles } from "./types";
import { exportXliffFiles, importXliffFiles } from "./xliff";

export function exportFiles(
	results: ValidateProjectResults,
	options: Required<TrancorderOptions>,
	destination: string,
	type: TrancorderExportType
): Promise<ExportResults> {
	switch (type) {
		case "xliff": {
			const files = exportXliffFiles(results, options);
			return writeExporterFiles(destination, files);
		}
		default:
			return Promise.resolve({
				paths: [],
				errors: [newError(TrancorderErrorCode.UnsupportedTypeForExport, [type])],
			});
	}
}

export function importFiles(
	results: ValidateProjectResults,
	options: Required<TrancorderOptions>,
	source: string,
	type: TrancorderExportType
): Promise<ImportResults> {
	switch (type) {
		case "xliff": {
			return readImporterFiles(source, "xliff")
				.then((files) => importXliffFiles(results, files))
				.then((results) => writeImportedFiles(results, options));
		}
		default:
			return Promise.resolve({
				imports: [],
				errors: [newError(TrancorderErrorCode.UnsupportedTypeForExport, [type])],
			});
	}
}

function writeExporterFiles(destination: string, files: ExporterFiles): Promise<ExportResults> {
	const errors: TrancorderError[] = [];
	const paths: string[] = [];
	const tasks = files.map((file) => {
		return writeFile(path.join(destination, file.file), file.content, "wx")
			.then(() => {
				paths.push(file.file);
			})
			.catch((e: Error) => {
				errors.push(
					newError(TrancorderErrorCode.CanNotWriteExportedFile, [file.file], false, e)
				);
			});
	});

	return Promise.all(tasks).then(() => ({ paths, errors }));
}

function readImporterFiles(source: string, type: TrancorderExportType): Promise<ImportedFiles> {
	return loadFiles(source, [`*.${type}`]).then(({ files, err, errors }) => {
		return Promise.all(files.map((file) => readFile(path.join(source, file), file))).then(
			(results) => {
				const files = results
					.filter((r) => !r.err)
					.map(({ content, absolute }) => ({ file: absolute, content }));

				const filesErrors = results
					.filter((r) => r.err)
					.map((r) => r.err) as TrancorderError[];

				return {
					files,
					errors: [...(err ? [err] : []), ...errors, ...filesErrors],
				};
			}
		);
	});
}

function writeImportedFiles(
	results: ImportResults,
	options: Required<TrancorderOptions>
): Promise<ImportResults> {
	const tasks = results.imports.map((file) => {
		return updateLocaleFile(file.translation, file.language, options).then(({ errors }) => {
			return {
				...file,
				errors: [...file.errors, ...errors],
			};
		});
	});

	return Promise.all(tasks).then((imports) => ({
		...results,
		imports,
	}));
}
