import {
	TrancorderError,
	TrancorderErrorCode,
	TrancorderErrorSeverity,
	TrancorderErrorMessages,
} from "../types";

export function newError(
	code: TrancorderErrorCode,
	params: Array<string>,
	fixable?: boolean,
	original?: Error
): TrancorderError {
	const err = new Error() as TrancorderError;
	const [name, desc] = TrancorderErrorMessages[code];

	err.name = buildMessages(name, params);
	err.message = buildMessages(desc, params);
	err.rawMessage = [name, desc];
	err.rawParams = params;
	err.code = code;
	err.original = original;
	err.severity = TrancorderErrorSeverity.Error;
	err.fixable = fixable || false;
	return err;
}

export function newWarning(
	code: TrancorderErrorCode,
	params: Array<string>,
	fixable?: boolean,
	original?: Error
): TrancorderError {
	const err = newError(code, params, fixable, original);
	err.severity = TrancorderErrorSeverity.Warning;
	return err;
}

export function newInfo(
	code: TrancorderErrorCode,
	params: Array<string>,
	fixable?: boolean,
	original?: Error
): TrancorderError {
	const err = newError(code, params, fixable, original);
	err.severity = TrancorderErrorSeverity.Info;
	return err;
}

function buildMessages(message: string, params: Array<string>) {
	return params.reduce(
		(msg, param, index) => msg.replace(new RegExp(`\\{${index}}`, "gi"), param),
		message
	);
}
