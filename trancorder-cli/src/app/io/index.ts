import * as fs from "fs";
import * as path from "path";
import * as glob from "glob";

import { TrancorderError, TrancorderErrorCode } from "../types";
import { newError } from "../errors";

export function writeFile(filepath: string, content: string, flag: string) {
	return new Promise<void>((resolve, reject) => {
		fs.mkdir(path.dirname(filepath), { recursive: true }, (error) => {
			if (error) {
				reject(error);
				return;
			}
			fs.writeFile(filepath, content, { flag }, (error) => {
				if (error) {
					reject(error);
					return;
				}
				resolve();
			});
		});
	});
}

export type ReadFileResult = {
	err?: TrancorderError;
	content: string;
	absolute: string;
	relative: string;
};

export function readFile(filepath: string, relative: string): Promise<ReadFileResult> {
	return new Promise((resolve) => {
		fs.readFile(filepath, (err, data) => {
			resolve({
				content: data ? data.toString() : "",
				absolute: filepath,
				relative,
				...(err
					? { err: newError(TrancorderErrorCode.CanNotReadFile, [relative], false, err) }
					: {}),
			});
		});
	});
}

export function existsFile(file: string) {
	return fs.existsSync(file);
}

export function normalizePath(path: string) {
	return path.replace(/\\/g, "/");
}

export type LoadFilesResult = {
	errors: TrancorderError[];
	err?: TrancorderError;
	files: string[];
};

export function loadFiles(cwd: string, files: string[]): Promise<LoadFilesResult> {
	return new Promise((resolve) => {
		const tasks = files.map((source) => {
			if (glob.hasMagic(source, { dot: true })) {
				return loadPatterns(cwd, source);
			} else {
				return Promise.resolve({ files: [source], errors: [] } as LoadPatternsResult);
			}
		});

		Promise.all(tasks).then((data) => {
			resolve(
				data.reduce(
					(results, { files, errors }) => {
						results.errors.push(...errors);
						results.files.push(...files);
						results.err = results.err || errors[0];
						return results;
					},
					{
						errors: [],
						files: [],
						err: undefined,
					} as LoadFilesResult
				)
			);
		});
	});
}

type LoadPatternsResult = {
	err?: TrancorderError;
	errors: TrancorderError[];
	files: string[];
};

function loadPatterns(cwd: string, pattern: string): Promise<LoadPatternsResult> {
	return new Promise((resolve) => {
		glob(pattern, { cwd, absolute: false }, (err, files) => {
			resolve({
				errors: err ? [newError(TrancorderErrorCode.CanNotReadFiles, [pattern])] : [],
				err: err ? newError(TrancorderErrorCode.CanNotReadFiles, [pattern]) : undefined,
				files,
			});
		});
	});
}
