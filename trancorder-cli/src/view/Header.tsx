import * as React from "react";
import { Box } from "ink";

import { TrancorderSupportedFormats } from "../app";

import { Title, Debug } from "./components";

export type HeaderProps = {
	cwd: string;
	format: TrancorderSupportedFormats;
	defaultLanguage: string;
	translations: string;
	tsConfigFilePath: string;
	sources: string[];
	debug?: boolean;
};
export function Header(props: HeaderProps) {
	const { cwd, defaultLanguage, sources, format, translations } = props;
	return (
		<Box flexDirection="column">
			<Title
				cwd={cwd}
				sources={sources}
				language={defaultLanguage}
				format={format}
				translations={translations}
			/>
			<Debug {...props} />
		</Box>
	);
}
