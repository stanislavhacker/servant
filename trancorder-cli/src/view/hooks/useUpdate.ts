import { useMemo } from "react";

import { UpdateResults, TrancorderApi, UpdateOptions } from "../../app";
import { TrancorderCliCommands } from "../types";

export function useUpdate(
	trancorder: TrancorderApi,
	command: TrancorderCliCommands,
	help: boolean,
	opts?: UpdateOptions
): Promise<false | UpdateResults> {
	return useMemo(() => {
		if (command !== "update" || help) {
			return Promise.resolve(false);
		}
		return trancorder.update(opts);
	}, []);
}
