import { useEffect } from "react";

import { TrancorderErrorHandler } from "../types";

export function useErrorHandler(onError: TrancorderErrorHandler, err: Error) {
	useEffect(() => {
		onError(err);
	}, [err, onError]);
}
