import { useMemo } from "react";

import { TrancorderApi, ValidateProjectResults } from "../../app";
import { TrancorderCliCommands } from "../types";

export function useValidate(
	trancorder: TrancorderApi,
	command: TrancorderCliCommands,
	help: boolean
): Promise<false | ValidateProjectResults> {
	return useMemo(() => {
		if (command !== "validate" || help) {
			return Promise.resolve(false);
		}
		return trancorder.validate();
	}, []);
}
