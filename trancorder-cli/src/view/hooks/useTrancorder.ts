import { useMemo } from "react";

import { createTrancorder, TrancorderApi, TrancorderSupportedFormats } from "../../app";

export function useTrancorder(
	cwd: string,
	format: TrancorderSupportedFormats,
	defaultLanguage: string,
	translations: string,
	sources: string[],
	tsConfigFilePath: string,
	debug = false
): TrancorderApi {
	return useMemo(
		() =>
			createTrancorder({
				cwd,
				debug,
				format,
				defaultLanguage,
				translations,
				tsConfigFilePath,
				sources,
			}),
		[cwd, format, defaultLanguage, translations, sources, debug]
	);
}
