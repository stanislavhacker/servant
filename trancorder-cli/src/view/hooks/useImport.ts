import { useMemo } from "react";

import { TrancorderApi, TrancorderExportType, ImportResults } from "../../app";
import { TrancorderCliCommands } from "../types";

export function useImport(
	trancorder: TrancorderApi,
	command: TrancorderCliCommands,
	help: boolean,
	source: string,
	type: TrancorderExportType
): Promise<false | ImportResults> {
	return useMemo(() => {
		if (command !== "import" || help) {
			return Promise.resolve(false);
		}
		return trancorder.import(source, type);
	}, []);
}
