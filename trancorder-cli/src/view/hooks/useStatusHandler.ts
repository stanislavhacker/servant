import { useEffect } from "react";

import { ErrorsStatus, TrancorderStatusHandler } from "../types";

export function useStatusHandler(onStatus: TrancorderStatusHandler, status: ErrorsStatus) {
	useEffect(() => {
		onStatus(status);
	}, [status, onStatus]);
}
