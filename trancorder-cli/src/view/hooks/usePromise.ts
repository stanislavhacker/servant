import { useState, useEffect, ReactElement } from "react";

export function usePromise<T, E = Error>(
	promise: Promise<T | false>,
	handlers: {
		onLoading: () => ReactElement;
		onComplete: (results: T, time: [number, number]) => ReactElement;
		onError: (err: E, time: [number, number]) => ReactElement;
	}
) {
	const [results, setResults] = useState<T | false | null>(null);
	const [error, setError] = useState<E | null>(null);
	const [start] = useState(process.hrtime());
	const [time, setTime] = useState<[number, number]>([0, 0]);

	useEffect(() => {
		promise
			.then((res) => {
				setResults(res);
				setTime(process.hrtime(start));
			})
			.catch((e) => {
				setError(e);
				setTime(process.hrtime(start));
			});
	}, [promise, start]);

	if (results === false) {
		return null;
	}
	if (results) {
		return handlers.onComplete(results, time);
	}
	if (error) {
		return handlers.onError(error, time);
	}
	return handlers.onLoading();
}
