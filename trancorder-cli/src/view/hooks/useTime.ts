import { useMemo } from "react";

export function useTime(time: [number, number]): string {
	return useMemo(() => {
		return `${(time[0] * 1000000000 + time[1]) / 1000000}ms`;
	}, [time]);
}
