import { useMemo } from "react";

import { TrancorderApi, ExportResults, TrancorderExportType } from "../../app";
import { TrancorderCliCommands } from "../types";

export function useExport(
	trancorder: TrancorderApi,
	command: TrancorderCliCommands,
	help: boolean,
	destination: string,
	type: TrancorderExportType
): Promise<false | ExportResults> {
	return useMemo(() => {
		if (command !== "export" || help) {
			return Promise.resolve(false);
		}
		return trancorder.export(destination, type);
	}, []);
}
