import { useMemo } from "react";

import { ImportResult, TrancorderError, TrancorderErrorSeverity } from "../../app";
import { ErrorsStatus } from "../types";

export function useErrors(
	errors: TrancorderError[],
	debug: boolean
): [TrancorderError[], ErrorsStatus] {
	return useMemo(() => {
		const allErrors = errors;
		const filteredErrors = errors.filter(createFilter(debug));
		return [filteredErrors, createStatus(allErrors, filteredErrors)] as [
			TrancorderError[],
			ErrorsStatus
		];
	}, [errors, debug]);
}

export function useProblems(
	problems: Record<string, TrancorderError[]>,
	debug: boolean
): [Array<TrancorderError & { file: string }>, ErrorsStatus] {
	return useMemo(() => {
		const collected: [Array<TrancorderError & { file: string }>, ErrorsStatus][] = Object.keys(
			problems
		).reduce((prev, file) => {
			const allErrors = problems[file];
			const filteredErrors = allErrors.map(createFile(file)).filter(createFilter(debug));
			return [...prev, [filteredErrors, createStatus(allErrors, filteredErrors)]];
		}, [] as Array<[Array<TrancorderError & { file: string }>, ErrorsStatus]>);

		return collected.reduce(
			([prevErrors, prevStatus], [errors, status]) => [
				[...prevErrors, ...errors],
				mergeErrorStatus(prevStatus, status),
			],
			[[], createStatus([], [])] as [Array<TrancorderError & { file: string }>, ErrorsStatus]
		);
	}, [problems, debug]);
}

export function useImportProblems(imports: ImportResult[], debug: boolean) {
	const problems = useMemo(() => {
		return imports.reduce((prev, current) => {
			prev[current.file] = current.errors;
			return prev;
		}, {} as Record<string, TrancorderError[]>);
	}, []);

	return useProblems(problems, debug);
}

export function mergeErrorStatus(...status: Array<ErrorsStatus>) {
	return status.reduce(
		(prev, status) => ({
			errors: prev.errors + status.errors,
			warnings: prev.warnings + status.warnings,
			infos: prev.infos + status.infos,
			hidden: prev.hidden + status.hidden,
		}),
		createStatus([], [])
	);
}

function createFile(file: string): (err: TrancorderError) => TrancorderError & { file: string } {
	return (err) => ({ ...err, file });
}

function createFilter(debug: boolean): (err: TrancorderError) => boolean {
	return (err) => {
		//filter out info in normal mode
		// noinspection RedundantIfStatementJS
		if (err.severity === TrancorderErrorSeverity.Info && !debug) {
			return false;
		}
		return true;
	};
}

function createStatus(
	allErrors: TrancorderError[],
	filteredErrors: TrancorderError[]
): ErrorsStatus {
	return {
		hidden: allErrors.length - filteredErrors.length,
		infos: filteredErrors.filter((e) => e.severity === TrancorderErrorSeverity.Info).length,
		warnings: filteredErrors.filter((e) => e.severity === TrancorderErrorSeverity.Warning)
			.length,
		errors: filteredErrors.filter((e) => e.severity === TrancorderErrorSeverity.Error).length,
	};
}
