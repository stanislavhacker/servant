import { useMemo } from "react";

import { CreateOptions, CreateResults, TrancorderApi } from "../../app";
import { TrancorderCliCommands } from "../types";

export function useCreate(
	trancorder: TrancorderApi,
	command: TrancorderCliCommands,
	help: boolean,
	opts?: CreateOptions
): Promise<false | CreateResults> {
	return useMemo(() => {
		if (command !== "create" || help) {
			return Promise.resolve(false);
		}
		return trancorder.create(opts);
	}, []);
}
