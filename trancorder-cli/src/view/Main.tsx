import * as React from "react";
import { Box } from "ink";

import { TrancorderExportType, TrancorderSupportedFormats } from "../app";

import { TrancorderCliCommands, TrancorderErrorHandler, TrancorderStatusHandler } from "./types";
import { useTrancorder } from "./hooks/useTrancorder";
import { useValidate } from "./hooks/useValidate";
import { useCreate } from "./hooks/useCreate";
import { useUpdate } from "./hooks/useUpdate";
import { useExport } from "./hooks/useExport";
import { useImport } from "./hooks/useImport";

import { Header } from "./Header";
import {
	ValidateCommand,
	CreateCommand,
	UpdateCommand,
	ExportCommand,
	ImportCommand,
} from "./commands";
import { Help } from "./help";

export type MainProps = {
	cwd: string;
	output: string;
	input: string;
	command: TrancorderCliCommands;
	format: TrancorderSupportedFormats;
	exports: TrancorderExportType;
	defaultLanguage: string;
	translations: string;
	tsConfigFilePath: string;
	sources: string[];
	languages?: string[];
	debug?: boolean;
	onStatus?: TrancorderStatusHandler;
	onError?: TrancorderErrorHandler;
	help?: boolean;
};
export function Main(props: MainProps) {
	const {
		debug = false,
		languages = [],
		onStatus = () => void 0,
		onError = () => void 0,
		help = false,
		...restProps
	} = props;

	const {
		cwd,
		exports,
		output,
		input,
		format,
		defaultLanguage,
		translations,
		sources,
		tsConfigFilePath,
		command,
	} = restProps;

	const trancorder = useTrancorder(
		cwd,
		format,
		defaultLanguage,
		translations,
		sources,
		tsConfigFilePath,
		debug
	);
	const validate = useValidate(trancorder, command, help);
	const create = useCreate(trancorder, command, help, { languages });
	const update = useUpdate(trancorder, command, help, { languages });
	const exp = useExport(trancorder, command, help, output, exports);
	const imp = useImport(trancorder, command, help, input, exports);

	return (
		<Box flexDirection="column">
			<Header {...restProps} debug={debug} />
			{help ? (
				<Help debug={debug} languages={languages} help={help} {...restProps} />
			) : (
				<>
					{command === "validate" && (
						<ValidateCommand
							debug={debug}
							promise={validate}
							onStatus={onStatus}
							onError={onError}
						/>
					)}
					{command === "create" && (
						<CreateCommand
							debug={debug}
							promise={create}
							onStatus={onStatus}
							onError={onError}
						/>
					)}
					{command === "update" && (
						<UpdateCommand
							debug={debug}
							promise={update}
							onStatus={onStatus}
							onError={onError}
						/>
					)}
					{command === "export" && (
						<ExportCommand
							debug={debug}
							promise={exp}
							onStatus={onStatus}
							onError={onError}
						/>
					)}
					{command === "import" && (
						<ImportCommand
							debug={debug}
							promise={imp}
							onStatus={onStatus}
							onError={onError}
						/>
					)}
				</>
			)}
		</Box>
	);
}
