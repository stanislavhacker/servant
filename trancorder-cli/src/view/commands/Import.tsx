import * as React from "react";
import { useMemo } from "react";
import { Box, Text } from "ink";

import { ImportResults } from "../../app";
import { TrancorderErrorHandler, TrancorderStatusHandler } from "../types";
import { useStatusHandler } from "../hooks/useStatusHandler";
import { useErrorHandler } from "../hooks/useErrorHandler";
import { mergeErrorStatus, useErrors, useImportProblems } from "../hooks/useErrors";
import { usePromise } from "../hooks/usePromise";
import { Loader, DefinedError, Error as ErrorComponent } from "../components";

import { Footer } from "./Footer";
import { Header } from "./Header";

export type ImportCommandProps = {
	promise: Promise<false | ImportResults>;
	debug: boolean;
	onStatus: TrancorderStatusHandler;
	onError: TrancorderErrorHandler;
};
export function ImportCommand({ promise, debug, onStatus, onError }: ImportCommandProps) {
	return usePromise(promise, {
		onLoading: () => <Loader text="importing" />,
		onComplete: (res, time) => (
			<ImportResults results={res} time={time} debug={debug} onStatus={onStatus} />
		),
		onError: (err) => <ImportError err={err} debug={debug} onError={onError} />,
	});
}

type ImportResultsProps = {
	results: ImportResults;
	time: [number, number];
	debug: boolean;
	onStatus: TrancorderStatusHandler;
};

function ImportResults({ results, debug, time, onStatus }: ImportResultsProps) {
	const [errors, errorsStatus] = useErrors(results.errors, debug);
	const [problems, problemsStatus] = useImportProblems(results.imports, debug);

	const errorStatus = useMemo(
		() => mergeErrorStatus(errorsStatus, problemsStatus),
		[errorsStatus, problemsStatus]
	);
	useStatusHandler(onStatus, errorStatus);

	let i = 0;
	return (
		<Box flexDirection="column">
			<Header name="import" />
			{errors.map((err) => (
				<DefinedError key={i} error={err} debug={debug} index={i++} />
			))}
			{problems.map((err) => (
				<DefinedError key={i} error={err} debug={debug} index={i++} file={err.file} />
			))}
			{errorStatus.hidden > 0 && <Text italic> +{errorStatus.hidden} hidden items</Text>}
			<Footer time={time} errorStatus={errorStatus} debug={debug} imports={results.imports} />
		</Box>
	);
}

type ImportErrorProps = {
	err: Error;
	debug: boolean;
	onError: TrancorderErrorHandler;
};

function ImportError({ err, debug, onError }: ImportErrorProps) {
	useErrorHandler(onError, err);
	return <ErrorComponent index={0} error={err} debug={debug} />;
}
