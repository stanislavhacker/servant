import * as React from "react";
import { Box, Text } from "ink";

export type HeaderProps = {
	name: string;
};

export function Header({ name }: HeaderProps) {
	return (
		<Box flexDirection="column" marginTop={1}>
			<Box flexDirection="row">
				<Text color="whiteBright">{name} command</Text>
			</Box>
		</Box>
	);
}
