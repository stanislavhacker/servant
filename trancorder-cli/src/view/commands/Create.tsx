import * as React from "react";
import { useMemo } from "react";
import { Box, Text } from "ink";

import { CreateResults } from "../../app";
import { TrancorderErrorHandler, TrancorderStatusHandler } from "../types";
import { useStatusHandler } from "../hooks/useStatusHandler";
import { useErrorHandler } from "../hooks/useErrorHandler";
import { mergeErrorStatus, useProblems } from "../hooks/useErrors";
import { usePromise } from "../hooks/usePromise";
import { Loader, DefinedError, Error as ErrorComponent } from "../components";

import { Footer } from "./Footer";
import { Header } from "./Header";

export type CreateCommandProps = {
	promise: Promise<false | CreateResults>;
	debug: boolean;
	onStatus: TrancorderStatusHandler;
	onError: TrancorderErrorHandler;
};
export function CreateCommand({ promise, debug, onStatus, onError }: CreateCommandProps) {
	return usePromise(promise, {
		onLoading: () => <Loader text="creating" />,
		onComplete: (res, time) => (
			<CreateResults results={res} time={time} debug={debug} onStatus={onStatus} />
		),
		onError: (err) => <CreateError err={err} debug={debug} onError={onError} />,
	});
}

type CreateResultsProps = {
	results: CreateResults;
	time: [number, number];
	debug: boolean;
	onStatus: TrancorderStatusHandler;
};

function CreateResults({ results, debug, time, onStatus }: CreateResultsProps) {
	const [problems, problemsStatus] = useProblems(results.errors, debug);

	const errorStatus = useMemo(() => mergeErrorStatus(problemsStatus), [problemsStatus]);
	useStatusHandler(onStatus, errorStatus);

	let i = 0;
	return (
		<Box flexDirection="column">
			<Header name="create" />
			{problems.map((err) => (
				<DefinedError key={i} error={err} debug={debug} index={i++} file={err.file} />
			))}
			{errorStatus.hidden > 0 && <Text italic> +{errorStatus.hidden} hidden items</Text>}
			<Footer
				time={time}
				errorStatus={errorStatus}
				debug={debug}
				languages={results.list}
				paths={results.paths}
			/>
		</Box>
	);
}

type CreateErrorProps = {
	err: Error;
	debug: boolean;
	onError: TrancorderErrorHandler;
};

function CreateError({ err, debug, onError }: CreateErrorProps) {
	useErrorHandler(onError, err);
	return <ErrorComponent index={0} error={err} debug={debug} />;
}
