import * as React from "react";
import { useMemo } from "react";
import { Box, Text } from "ink";

import { ExportResults } from "../../app";
import { TrancorderErrorHandler, TrancorderStatusHandler } from "../types";
import { useStatusHandler } from "../hooks/useStatusHandler";
import { useErrorHandler } from "../hooks/useErrorHandler";
import { mergeErrorStatus, useErrors } from "../hooks/useErrors";
import { usePromise } from "../hooks/usePromise";
import { Loader, DefinedError, Error as ErrorComponent } from "../components";

import { Footer } from "./Footer";
import { Header } from "./Header";

export type ExportCommandProps = {
	promise: Promise<false | ExportResults>;
	debug: boolean;
	onStatus: TrancorderStatusHandler;
	onError: TrancorderErrorHandler;
};
export function ExportCommand({ promise, debug, onStatus, onError }: ExportCommandProps) {
	return usePromise(promise, {
		onLoading: () => <Loader text="exporting" />,
		onComplete: (res, time) => (
			<ExportResults results={res} time={time} debug={debug} onStatus={onStatus} />
		),
		onError: (err) => <ExportError err={err} debug={debug} onError={onError} />,
	});
}

type ExportResultsProps = {
	results: ExportResults;
	time: [number, number];
	debug: boolean;
	onStatus: TrancorderStatusHandler;
};

function ExportResults({ results, debug, time, onStatus }: ExportResultsProps) {
	const [errors, errorsStatus] = useErrors(results.errors, debug);

	const errorStatus = useMemo(() => mergeErrorStatus(errorsStatus), [errorsStatus]);
	useStatusHandler(onStatus, errorStatus);

	let i = 0;
	return (
		<Box flexDirection="column">
			<Header name="export" />
			{errors.map((err) => (
				<DefinedError key={i} error={err} debug={debug} index={i++} />
			))}
			{errorStatus.hidden > 0 && <Text italic> +{errorStatus.hidden} hidden items</Text>}
			<Footer time={time} errorStatus={errorStatus} debug={debug} paths={results.paths} />
		</Box>
	);
}

type ExportErrorProps = {
	err: Error;
	debug: boolean;
	onError: TrancorderErrorHandler;
};

function ExportError({ err, debug, onError }: ExportErrorProps) {
	useErrorHandler(onError, err);
	return <ErrorComponent index={0} error={err} debug={debug} />;
}
