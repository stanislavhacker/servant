import * as React from "react";
import { Box, Text } from "ink";

import { ImportResult, UpdateResults, ValidateProjectResults } from "../../app";
import { ErrorsStatus } from "../types";
import { useTime } from "../hooks/useTime";
import { Status } from "../components";

export type FooterProps = {
	setup?: ValidateProjectResults["setup"];
	used?: string[];
	languages?: string[];
	paths?: string[];
	changes?: UpdateResults["changes"];
	imports?: ImportResult[];
	errorStatus: ErrorsStatus;
	time: [number, number];
	debug: boolean;
};

export function Footer({
	languages = [],
	used = [],
	paths = [],
	imports = [],
	setup = null,
	changes = {},
	time,
	errorStatus,
}: FooterProps) {
	return (
		<Box flexDirection="column">
			<FooterSetup setup={setup} />
			<FooterLanguages languages={languages} />
			<FooterUsed used={used} />
			<FooterPaths paths={paths} changes={changes} />
			<FooterImports imports={imports} />
			<Status status={errorStatus} />
			<FooterTime time={time} />
		</Box>
	);
}

function FooterUsed(props: { used: string[] }) {
	if (props.used.length === 0) {
		return null;
	}
	return (
		<Box flexDirection="row">
			<Box minWidth={20} justifyContent="flex-end" paddingRight={1}>
				<Text bold>Source keys:</Text>
			</Box>
			<Box>
				<Text color="greenBright">{props.used.length} keys found</Text>
			</Box>
		</Box>
	);
}

function FooterPaths(props: { paths: string[]; changes: UpdateResults["changes"] }) {
	if (props.paths.length === 0) {
		return null;
	}
	return (
		<Box flexDirection="row">
			<Box minWidth={20} justifyContent="flex-end" paddingRight={1}>
				<Text bold>Files:</Text>
			</Box>
			<Box flexDirection="column">
				{props.paths.map((path, i) => {
					const fileChanges = props.changes[path];
					const added = fileChanges ? Object.keys(fileChanges.added).length : 0;
					const removed = fileChanges ? Object.keys(fileChanges.removed).length : 0;

					return (
						<Text key={i}>
							<Text>
								{i + 1}.{" "}
								<Text color="blueBright" underline>
									{path}
								</Text>
							</Text>
							{fileChanges && (
								<>
									<Text> {"=>"} </Text>
									{added > 0 && (
										<Text color="greenBright">
											+{Object.keys(fileChanges.added).length} added keys{" "}
										</Text>
									)}
									{removed > 0 && (
										<Text color="redBright">
											-{Object.keys(fileChanges.removed).length} removed keys{" "}
										</Text>
									)}
									{added === 0 && removed === 0 && (
										<Text color="gray" italic>
											no changes{" "}
										</Text>
									)}
								</>
							)}
						</Text>
					);
				})}
			</Box>
		</Box>
	);
}

function FooterImports(props: { imports: ImportResult[] }) {
	if (props.imports.length === 0) {
		return null;
	}
	return (
		<>
			{props.imports.map(({ file, language, source, updated, errors }, i) => {
				return (
					<Box flexDirection="row" key={i}>
						<Box minWidth={20} justifyContent="flex-end" paddingRight={1}>
							<Text bold>{language}:</Text>
						</Box>
						<Box>
							<Text color="whiteBright">{source}</Text>
							<Text> {"=>"} </Text>
							<Text color="whiteBright">{file}</Text>
							<Text> with </Text>
							<Text color="greenBright">{updated.length} updates</Text>
							{errors.length > 0 && (
								<Text italic>
									{" "}
									and <Text color="magentaBright">{errors.length} problems</Text>
								</Text>
							)}
						</Box>
					</Box>
				);
			})}
		</>
	);
}

function FooterLanguages(props: { languages: string[] }) {
	if (props.languages.length === 0) {
		return null;
	}
	return (
		<Box flexDirection="row">
			<Box minWidth={20} justifyContent="flex-end" paddingRight={1}>
				<Text bold>Languages:</Text>
			</Box>
			<Box>
				<Text color="greenBright">{props.languages.join(", ")}</Text>
			</Box>
		</Box>
	);
}

function FooterSetup(props: { setup?: ValidateProjectResults["setup"] }) {
	if (!props.setup) {
		return null;
	}
	return (
		<Text>
			<Text bold color="green">
				{props.setup.path}
			</Text>{" "}
			<Text bold>{props.setup.type}</Text> with{" "}
			<Text color="magentaBright">{props.setup.usages.length}</Text> usages
		</Text>
	);
}

function FooterTime(props: { time: [number, number] }) {
	const timeText = useTime(props.time);
	return (
		<Text>
			<Text bold>Time:</Text> {timeText}
		</Text>
	);
}
