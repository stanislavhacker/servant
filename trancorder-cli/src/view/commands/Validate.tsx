import * as React from "react";
import { useMemo } from "react";
import { Box, Text } from "ink";

import { ValidateProjectResults } from "../../app";
import { TrancorderErrorHandler, TrancorderStatusHandler } from "../types";
import { useStatusHandler } from "../hooks/useStatusHandler";
import { useErrorHandler } from "../hooks/useErrorHandler";
import { mergeErrorStatus, useErrors, useProblems } from "../hooks/useErrors";
import { usePromise } from "../hooks/usePromise";
import { Loader, DefinedError, Error as ErrorComponent } from "../components";

import { Footer } from "./Footer";
import { Header } from "./Header";

export type ValidateCommandProps = {
	promise: Promise<false | ValidateProjectResults>;
	debug: boolean;
	onStatus: TrancorderStatusHandler;
	onError: TrancorderErrorHandler;
};
export function ValidateCommand({ promise, debug, onStatus, onError }: ValidateCommandProps) {
	return usePromise(promise, {
		onLoading: () => <Loader text="validating" />,
		onComplete: (res, time) => (
			<ValidateResults results={res} time={time} debug={debug} onStatus={onStatus} />
		),
		onError: (err) => <ValidateError err={err} debug={debug} onError={onError} />,
	});
}

type ValidateResultsProps = {
	results: ValidateProjectResults;
	time: [number, number];
	debug: boolean;
	onStatus: TrancorderStatusHandler;
};

function ValidateResults({ results, debug, time, onStatus }: ValidateResultsProps) {
	const [errors, errorsStatus] = useErrors(results.errors, debug);
	const [problems, problemsStatus] = useProblems(results.problems, debug);

	const errorStatus = useMemo(
		() => mergeErrorStatus(errorsStatus, problemsStatus),
		[errorsStatus, problemsStatus]
	);
	useStatusHandler(onStatus, errorStatus);

	let i = 0;
	return (
		<Box flexDirection="column">
			<Header name="validate" />
			{errors.map((err) => (
				<DefinedError key={i} error={err} debug={debug} index={i++} />
			))}
			{problems.map((err) => (
				<DefinedError key={i} error={err} debug={debug} index={i++} file={err.file} />
			))}
			{errorStatus.hidden > 0 && <Text italic> +{errorStatus.hidden} hidden items</Text>}
			<Footer
				time={time}
				errorStatus={errorStatus}
				debug={debug}
				used={results.used}
				setup={results.setup}
				languages={Object.keys(results.languages)}
			/>
		</Box>
	);
}

type ValidateErrorProps = {
	err: Error;
	debug: boolean;
	onError: TrancorderErrorHandler;
};

function ValidateError({ err, debug, onError }: ValidateErrorProps) {
	useErrorHandler(onError, err);
	return <ErrorComponent index={0} error={err} debug={debug} />;
}
