import * as React from "react";
import { useMemo } from "react";
import { Box, Text } from "ink";

import { UpdateResults } from "../../app";
import { TrancorderErrorHandler, TrancorderStatusHandler } from "../types";
import { useStatusHandler } from "../hooks/useStatusHandler";
import { useErrorHandler } from "../hooks/useErrorHandler";
import { mergeErrorStatus, useProblems } from "../hooks/useErrors";
import { usePromise } from "../hooks/usePromise";
import { Loader, DefinedError, Error as ErrorComponent } from "../components";

import { Footer } from "./Footer";
import { Header } from "./Header";

export type UpdateCommandProps = {
	promise: Promise<false | UpdateResults>;
	debug: boolean;
	onStatus: TrancorderStatusHandler;
	onError: TrancorderErrorHandler;
};
export function UpdateCommand({ promise, debug, onStatus, onError }: UpdateCommandProps) {
	return usePromise(promise, {
		onLoading: () => <Loader text="updating" />,
		onComplete: (res, time) => (
			<UpdateResults results={res} time={time} debug={debug} onStatus={onStatus} />
		),
		onError: (err) => <UpdateError err={err} debug={debug} onError={onError} />,
	});
}

type UpdateResultsProps = {
	results: UpdateResults;
	time: [number, number];
	debug: boolean;
	onStatus: TrancorderStatusHandler;
};

function UpdateResults({ results, debug, time, onStatus }: UpdateResultsProps) {
	const [problems, problemsStatus] = useProblems(results.errors, debug);

	const errorStatus = useMemo(() => mergeErrorStatus(problemsStatus), [problemsStatus]);
	useStatusHandler(onStatus, errorStatus);

	let i = 0;
	return (
		<Box flexDirection="column">
			<Header name="update" />
			{problems.map((err) => (
				<DefinedError key={i} error={err} debug={debug} index={i++} file={err.file} />
			))}
			{errorStatus.hidden > 0 && <Text italic> +{errorStatus.hidden} hidden items</Text>}
			<Footer
				time={time}
				errorStatus={errorStatus}
				debug={debug}
				languages={results.list}
				paths={results.paths}
				changes={results.changes}
			/>
		</Box>
	);
}

type UpdateErrorProps = {
	err: Error;
	debug: boolean;
	onError: TrancorderErrorHandler;
};

function UpdateError({ err, debug, onError }: UpdateErrorProps) {
	useErrorHandler(onError, err);
	return <ErrorComponent index={0} error={err} debug={debug} />;
}
