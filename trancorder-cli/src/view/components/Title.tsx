import * as React from "react";
import { useMemo } from "react";
import { Box, Text } from "ink";
import * as path from "path";
import * as fs from "fs";

import { TrancorderSupportedFormats } from "../../app";
import { normalizePath } from "../../app/io";

import { Row } from "./Row";

export type TitleProps = {
	cwd: string;
	sources: string[];
	language: string;
	translations: string;
	format: TrancorderSupportedFormats;
};

export function Title({ cwd, sources, language, translations, format }: TitleProps) {
	const version = useVersion();

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<Text color="whiteBright">
					trancorder <Text color="green">{version}</Text>
				</Text>
				<Text color="whiteBright">
					{" "}
					on{" "}
					<Text italic color="white">
						{cwd}
					</Text>
				</Text>
			</Box>
			<Row type="sources">
				analyzing <Text color="white">{sources.join(", ")}</Text> for default language "
				<Text color="greenBright">{language}</Text>"
			</Row>
			<Row type="locales">
				loading from{" "}
				<Text color="white">
					{normalizePath(path.join(translations, `**/*.${format}`))}
				</Text>
			</Row>
		</Box>
	);
}

function useVersion() {
	return useMemo(() => {
		return getVersion();
	}, []);
}

function getVersion(): string {
	const modulePath = require.main?.path;

	if (modulePath) {
		const items = modulePath.split(path.sep);
		const dir = items.slice(0, items.indexOf("trancorder-cli") + 1);
		const packageJson = path.join(dir.join(path.sep), "package.json");

		try {
			const data = fs.readFileSync(packageJson);
			const json = JSON.parse(data.toString());
			return json.version || "unknown";
		} catch (_e) {
			return "unknown";
		}
	}
	return "unknown";
}
