import * as React from "react";
import { Box, Text } from "ink";

import { ErrorsStatus } from "../types";

export type StatusProps = {
	status: ErrorsStatus;
};
export function Status({ status }: StatusProps) {
	const total = status.errors + status.warnings + status.infos;

	return (
		<Box flexDirection="column">
			{total > 0 && (
				<Box flexDirection="row">
					{status.errors > 0 && (
						<>
							<Box marginRight={1}>
								<Text>
									<Text color="red" bold underline>
										Completed
									</Text>{" "}
									with
								</Text>
							</Box>
							<Box marginRight={1}>
								<Text color="redBright">
									<Text bold>{status.errors}</Text> errors
								</Text>
							</Box>
						</>
					)}
					{status.warnings > 0 && (
						<>
							<Box marginRight={1}>
								<Text>
									<Text color="yellow" bold underline>
										Completed
									</Text>{" "}
									with
								</Text>
							</Box>
							<Box marginRight={1}>
								<Text color="yellowBright">
									<Text bold>{status.warnings}</Text> warnings
								</Text>
							</Box>
						</>
					)}
					{status.infos > 0 && (
						<>
							<Box marginRight={1}>
								<Text>
									<Text color="green" bold underline>
										Completed
									</Text>{" "}
									with
								</Text>
							</Box>
							<Box marginRight={1}>
								<Text color="blueBright">
									<Text bold>{status.infos}</Text> info
								</Text>
							</Box>
						</>
					)}
				</Box>
			)}
			{total === 0 && (
				<Box flexDirection="row">
					<Box marginRight={1}>
						<Text color="green" bold underline>
							Completed without any problem!
						</Text>
					</Box>
				</Box>
			)}
			{status.hidden > 0 && (
				<Box flexDirection="row">
					<Box>
						<Text italic> +{status.hidden} hidden items</Text>
					</Box>
				</Box>
			)}
		</Box>
	);
}
