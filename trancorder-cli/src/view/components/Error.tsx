import * as React from "react";
import { Box, Text } from "ink";

import { TrancorderError, TrancorderErrorSeverity } from "../../app";

export type DefinedErrorProps = {
	index: number;
	error: TrancorderError;
	file?: string;
	debug?: boolean;
};

export function DefinedError({ error, file, index, debug }: DefinedErrorProps) {
	const [main, text] = getColors(error);

	return (
		<ErrorHeader file={file} error={error} index={index}>
			<Text color={main}>
				{error.name}: {error.message}
			</Text>
			{debug && <Text color={text}>{error.stack || error.original?.stack}</Text>}
			<Text color={text}>
				<Text bold>@code</Text> {error.code}
			</Text>
		</ErrorHeader>
	);
}

export type DefaultErrorProps = {
	index: number;
	error: Error;
	debug?: boolean;
};
export function Error({ error, debug, index }: DefaultErrorProps) {
	return (
		<ErrorHeader error={error} index={index}>
			<Text color="red">
				{error.name}: {error.message}
			</Text>
			{debug && <Text color="redBright">{error.stack}</Text>}
		</ErrorHeader>
	);
}

type ErrorHeaderProps = {
	index: number;
	error: TrancorderError | Error;
	file?: string;
};

function ErrorHeader({ index, error, file, children }: React.PropsWithChildren<ErrorHeaderProps>) {
	const indexText = `[${index + 1}] `;
	const header = getHeader(indexText, error, file);

	return (
		<Box flexDirection="column" marginBottom={1} marginTop={index === 0 ? 1 : 0}>
			{header}
			<Box flexDirection="column" marginLeft={indexText.length}>
				{children}
			</Box>
		</Box>
	);
}

function getHeader(errorNo: string, error: TrancorderError | Error, file?: string) {
	const [main] = getColors(error);

	if ("severity" in error) {
		return file ? (
			<Text color={main} bold>
				{errorNo}
				{getType(error)} in <File file={file} start={error.start} end={error.end} />
			</Text>
		) : (
			<Text color={main} bold>
				{errorNo}
				{getType(error)} trancorder settings
			</Text>
		);
	}
	return (
		<Text color={main} bold>
			{errorNo}
			{getType(error)} application error
		</Text>
	);
}

function getColors(error: TrancorderError | Error): [string, string] {
	if ("severity" in error) {
		switch (error.severity) {
			case TrancorderErrorSeverity.Error:
				return ["red", "redBright"];
			case TrancorderErrorSeverity.Warning:
				return ["yellow", "yellowBright"];
			case TrancorderErrorSeverity.Info:
			default:
				return ["blue", "blueBright"];
		}
	}
	return ["red", "redBright"];
}

function getType(error: TrancorderError | Error): string {
	if ("severity" in error) {
		switch (error.severity) {
			case TrancorderErrorSeverity.Error:
				return "ERROR";
			case TrancorderErrorSeverity.Warning:
				return "WARNING";
			case TrancorderErrorSeverity.Info:
			default:
				return "INFO";
		}
	}
	return "ERROR";
}

type FileProps = {
	file: string;
	start: TrancorderError["start"];
	end: TrancorderError["end"];
};

function File({ file, start, end }: FileProps) {
	return (
		<Text>
			<Text>{file} </Text>
			{start && (
				<Text>
					{start.line}:{start.column}
				</Text>
			)}
			{start && end && start.line !== end.line && start.column !== end.column && (
				<Text>
					{" "}
					{"=>"} {end.line}:{end.column}
				</Text>
			)}
		</Text>
	);
}
