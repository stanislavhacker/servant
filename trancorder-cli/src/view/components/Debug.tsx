import * as React from "react";
import { Text } from "ink";

import { TrancorderSupportedFormats } from "../../app";

import { Row } from "./Row";
import { normalizePath } from "../../app/io/index";

export type DebugProps = {
	cwd: string;
	format: TrancorderSupportedFormats;
	defaultLanguage: string;
	translations: string;
	tsConfigFilePath: string;
	sources: string[];
	debug?: boolean;
};
export function Debug({
	cwd,
	defaultLanguage,
	sources,
	format,
	translations,
	tsConfigFilePath,
	debug = false,
}: DebugProps) {
	const rows = [
		<Row type="debug" key="entry">
			<Text bold>--entry</Text> "{normalizePath(cwd)}"
		</Row>,
		<Row type="debug" key="translations">
			<Text bold>--translations</Text> "{normalizePath(translations)}"
		</Row>,
		...sources.map((src, i) => (
			<Row type="debug" key={`sources-${i}`}>
				<Text bold>--sources</Text> "{normalizePath(src)}"
			</Row>
		)),
		<Row type="debug" key="defaultLanguage">
			<Text bold>--defaultLanguage</Text> "{defaultLanguage}"
		</Row>,
		<Row type="debug" key="config">
			<Text bold>--config</Text> "{normalizePath(tsConfigFilePath)}"
		</Row>,
		<Row type="debug" key="debug">
			<Text bold>--debug</Text>
		</Row>,
		<Row type="debug" key="format">
			{format}
		</Row>,
	];

	if (!debug) {
		return <Text italic> +{rows.length} debug lines</Text>;
	}
	return <>{rows}</>;
}
