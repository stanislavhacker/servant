import * as React from "react";
import { Box, Text } from "ink";

export type LoaderProps = {
	text: string;
};
export function Loader({ text }: LoaderProps) {
	return (
		<Box flexDirection="row" marginTop={1}>
			<Text italic color="gray">
				{text} ...
			</Text>
		</Box>
	);
}
