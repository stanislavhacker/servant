import * as React from "react";
import { Box, Text } from "ink";

export type RowProps = {
	type: "sources" | "locales" | "debug";
};
export function Row({ type, children }: React.PropsWithChildren<RowProps>) {
	return (
		<Box flexDirection="row" marginLeft={2}>
			<Text>
				<Text color={getColor(type)}>[{type}]</Text>{" "}
				<Text color="whiteBright">{children}</Text>
			</Text>
		</Box>
	);
}

function getColor(type: RowProps["type"]): string {
	switch (type) {
		case "locales":
			return "magentaBright";
		case "sources":
			return "blueBright";
		case "debug":
			return "gray";
		default:
			return "whiteBright";
	}
}
