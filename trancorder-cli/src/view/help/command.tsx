import * as React from "react";
import { Box, Text } from "ink";

import { MainProps } from "../Main";

import { Options } from "./options";

export const CommandPreview: React.StatelessComponent<
	Required<Omit<MainProps, "onError" | "onStatus">>
> = (props) => {
	const {
		command,
		debug,
		languages,
		defaultLanguage,
		sources,
		format,
		translations,
		cwd,
		exports,
		output,
		input,
		tsConfigFilePath,
	} = props;
	const paddingLeft = 10;

	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<Text italic>
					<Text color="gray">trancorder</Text>
				</Text>
				<Text> </Text>
				<Text color="blueBright">
					{command}
					<Text> </Text>
				</Text>
			</Box>
			{Boolean(cwd) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--entry</Text> "<Text underline>{cwd}</Text>"
						<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(languages && languages.length > 0) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--languages</Text> "
						<Text underline>{languages.join(",")}</Text>"<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(sources && sources.length > 0) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--sources</Text> "
						<Text underline>{sources.join(",")}</Text>"<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(translations) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--translations</Text> "
						<Text underline>{translations}</Text>"<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(debug) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="yellowBright">--debug</Text>
						<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(defaultLanguage) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--defaultLanguage</Text> "
						<Text underline>{defaultLanguage}</Text>"<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(format) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--format</Text> "<Text underline>{format}</Text>"
						<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(exports) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--exports</Text> "<Text underline>{exports}</Text>"
						<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(output) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--out</Text> "<Text underline>{output}</Text>"
						<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(input) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="cyanBright">--in</Text> "<Text underline>{input}</Text>"
						<Text> </Text>
					</Text>
				</Box>
			)}
			{Boolean(tsConfigFilePath) && (
				<Box flexDirection="row" paddingLeft={paddingLeft}>
					<Text>
						<Text color="redBright">--tsConfigPath</Text> <Text underline>{input}</Text>
						"<Text> </Text>
					</Text>
				</Box>
			)}
		</Box>
	);
};

export const CommandCompose: React.StatelessComponent<
	Required<Omit<MainProps, "onError" | "onStatus">>
> = (props) => {
	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<CommandStart command={true} />
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<Text>
						Where <Text color="blueBright">{"<command>"}</Text> is one or more from:
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text underline>validate</Text>
					<Text> </Text>
					<Text underline>create</Text>
					<Text> </Text>
					<Text underline>update</Text>
					<Text> </Text>
					<Text underline>import</Text>
					<Text> </Text>
					<Text underline>export</Text>
				</Box>
			</Box>

			<Options {...props} />
		</Box>
	);
};

export interface CommandStartProps {
	command: boolean;
}

export const CommandStart: React.StatelessComponent<CommandStartProps> = (props) => {
	const { command } = props;

	return (
		<>
			<Text italic color="gray">
				trancorder
			</Text>
			<Text> </Text>
			{command && (
				<Text color="blueBright">
					{"<command>"}
					<Text> </Text>
				</Text>
			)}
		</>
	);
};
