import * as React from "react";
import { Box, Text } from "ink";

import { MainProps } from "../Main";

import { CommandStart } from "./command";

export const Options: React.StatelessComponent<
	Required<Omit<MainProps, "onError" | "onStatus">>
> = (props) => {
	const { command } = props;

	return (
		<Box flexDirection="column">
			<Box flexDirection="row" marginTop={1}>
				<Text bold color="green">
					Settings
				</Text>
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text color="cyanBright">--help</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>Show help for defined {"<command>"}.</Text>
				</Box>
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text>
						<Text color="cyanBright">--entry</Text> {"<path>"}
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						Entry point for project where {"<path>"} is absolute path to project.
					</Text>
				</Box>
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text>
						<Text color="cyanBright">--sources</Text> {"<glob_or_globs>"}
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						Relative path to current entry, that is list of glob patters for loading
						code sources files (ex: {"src/**/*.{ts,tsx}"}).
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text bold>Can be used more times or one time split by ",".</Text>
				</Box>
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text>
						<Text color="cyanBright">--translations</Text> {"<directory>"}
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						Relative path to current entry, where are located translations files with
						defined format.
					</Text>
				</Box>
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text>
						<Text color="cyanBright">--format</Text> {"<format>"}
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>Format of translations files.</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text bold>
						Now supported only "<Text underline>json</Text>".
					</Text>
				</Box>
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text>
						<Text color="cyanBright">--defaultLanguage</Text> {"<language>"}
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>
						Default language that will be used for validating, creating and updating.
					</Text>
				</Box>
			</Box>
			<Box marginTop={1} flexDirection="column">
				<Box flexDirection="row">
					<CommandStart command={true} />
					<Text>
						<Text color="redBright">--tsConfigPath</Text> {"<tsconfg_path>"}
					</Text>
				</Box>
				<Box flexDirection="row" marginLeft={2}>
					<Text>Path to tsconfig file if is not in current entry folder.</Text>
				</Box>
			</Box>
			{(command === "create" || command === "update") && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text>
								<Text color="cyanBright">--languages</Text> {"<languages>"}
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								List of all additional languages, that will be updated or created by
								command.
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text bold>Can be used more times or one time split by ",".</Text>
						</Box>
					</Box>
				</Box>
			)}
			{(command === "import" || command === "export") && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text>
								<Text color="cyanBright">--exports</Text> {"<type>"}
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>Format of exported or imported files.</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text bold>
								Now supported only "<Text underline>xliff</Text>".
							</Text>
						</Box>
					</Box>
				</Box>
			)}
			{command === "import" && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text>
								<Text color="cyanBright">--in</Text> {"<folder>"}
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Absolute path to folder, where are files intended to import with
								defined type (for example .xliff files)
							</Text>
						</Box>
					</Box>
				</Box>
			)}
			{command === "export" && (
				<Box flexDirection="column">
					<Box marginTop={1} flexDirection="column">
						<Box flexDirection="row">
							<CommandStart command={true} />
							<Text>
								<Text color="cyanBright">--out</Text> {"<folder>"}
							</Text>
						</Box>
						<Box flexDirection="row" marginLeft={2}>
							<Text>
								Absolute path to folder, where will be exported files with defined
								type (for example .xliff files)
							</Text>
						</Box>
					</Box>
				</Box>
			)}
		</Box>
	);
};
