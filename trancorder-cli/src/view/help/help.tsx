import * as React from "react";
import { Box, Text } from "ink";

import { MainProps } from "../Main";
import { CommandCompose, CommandPreview } from "./command";

export const Help: React.StatelessComponent<Required<Omit<MainProps, "onError" | "onStatus">>> = (
	props
) => {
	return (
		<Box flexDirection="column" marginTop={1}>
			<Box flexDirection="row">
				<Text color="green" bold>
					Trancorder help
				</Text>
			</Box>
			<Box flexDirection="row" marginTop={1}>
				<Text color="white" underline>
					Provided command for help:
				</Text>
			</Box>
			<Box padding={1}>
				<CommandPreview {...props} />
			</Box>
			<Box flexDirection="row" marginTop={1}>
				<Text color="white" underline>
					Full command composition:
				</Text>
			</Box>
			<Box padding={1}>
				<CommandCompose {...props} />
			</Box>
		</Box>
	);
};
