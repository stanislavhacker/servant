export type TrancorderCliCommands = "validate" | "create" | "update" | "import" | "export";

export type TrancorderStatusHandler = (status: ErrorsStatus) => void;
export type TrancorderErrorHandler = (err: Error) => void;

export type ErrorsStatus = {
	errors: number;
	warnings: number;
	infos: number;
	hidden: number;
};
