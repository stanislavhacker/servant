# ![Servant][logo] Servant data module

Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

Data module for Servant that contains files definitions for used files. In this module there are helper methods for loading
npm's `package.json`, Servant definition file `servant.json`, typescript definition file `.tsconfig`, generator for webpack
definition javascript file `webpack.config.js` and manipulating with `.gitignore` file.

-   **package.json** as `PackageJson` namespace is used for manipulating with `package.json` file
-   **servant.json** as `ServantJson` namespace is used for manipulating with Servant definition file `servant.json`
-   **tsconfig.json** as `TsConfig` namespace for loading `.tsconfig` file for typescript
-   **webpack.config.js** as `WebpackConfig` namespace for loading of webpack definition javascript file `webpack.config.js`
-   **.gitignore** as `GitIgnore` namespace for loading `.gitignore` file and manipulating with content

### License

[Licensed under GPLv3][license]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
[preview]: https://gitlab.com/stanislavhacker/servant/raw/master/assets/servant.example1.gif
[init]: https://gitlab.com/stanislavhacker/servant/raw/master/assets/servant.init.gif
[1]: https://docs.npmjs.com/files/package.json
[2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
[3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
[4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
[5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
[license]: https://gitlab.com/stanislavhacker/servant/blob/master/LICENSE
