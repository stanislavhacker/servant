import { TemplateParameter } from "html-webpack-plugin";
import { Path } from "@servant/servant-files";
import * as HtmlCreator from "html-creator";
import * as path from "path";

import * as ServantJson from "./servant.json";

export type TemplateAdditionalFiles = {
	timestamp: number;
	styles: string[];
};

export function createTemplateAdditionalFiles(
	servantJson: ServantJson.ServantJsonInfo,
	publicPath: string
): TemplateAdditionalFiles {
	const { css } = ServantJson.entries(servantJson);
	const outputDist = path.join(servantJson.cwd, servantJson.content.output.directory);
	const styles = css ? [createPublicPath(publicPath, outputDist, css.to)] : [];

	return {
		timestamp: new Date().getTime(),
		styles,
	};
}

export function createPublicTemplate(
	params: TemplateParameter,
	{ styles, timestamp }: TemplateAdditionalFiles
) {
	const creator = new HtmlCreator([
		{
			type: "head",
			content: [
				{ type: "meta", attributes: { charset: "utf-8" } },
				{
					type: "meta",
					attributes: {
						name: "viewport",
						content: "width=device-width, initial-scale=1",
					},
				},
				{ type: "title", content: params.htmlWebpackPlugin.options.title },
				...styles.map((style) => ({
					type: "link",
					attributes: {
						type: "text/css",
						rel: "stylesheet",
						href: `${style}?${timestamp}`,
					},
				})),
			],
		},
		{
			type: "body",
		},
	]);

	return creator.renderHTML();
}

function createPublicPath(publicPath: string, outputDist: string, file: string): string {
	return "./" + Path.normalize(path.join(publicPath, path.relative(outputDist, file)));
}
