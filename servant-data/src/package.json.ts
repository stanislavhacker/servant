import * as path from "path";
import * as fs from "fs";

import { EslintrcJson } from "./eslintrc.json";
import { PrettierrcJson } from "./prettierrc.json";

export const PACKAGE_JSON = "package.json";

export interface PackageJsonInfo {
	path: string;
	cwd: string;
	content: PackageJsonPom;
	main: boolean;
}
export interface PackageJsonPom {
	name: string;
	version: string;
	dependencies: Array<Dependency>;
	devDependencies: Array<Dependency>;
	peerDependencies: Array<Dependency>;
	bundledDependencies: Array<string>;
	optionalDependencies: Array<Dependency>;
	files?: Array<string>;
	description?: string;
	main?: string;
	types?: string;
	keywords?: Array<string>;
	author?: string;
	license?: string;
	scripts?: Scripts;
	private?: boolean;
	bin?: Bin;
	eslintConfig?: EslintrcJson;
	prettier?: PrettierrcJson;
}
export const PackageJsonDefaults = {
	name: "",
	version: "1.0.0",
	description: "",
	keywords: [],
	author: "",
	license: "ISC",
} as Required<
	Pick<PackageJson, "name" | "version" | "description" | "keywords" | "author" | "license">
>;

export interface PackageJson {
	name: string;
	version: string;
	dependencies?: Dependencies;
	devDependencies?: Dependencies;
	peerDependencies?: Dependencies;
	optionalDependencies?: Dependencies;
	bundledDependencies?: Array<string>;
	bundleDependencies?: Array<string>;
	files?: Array<string>;
	description?: string;
	main?: string;
	types?: string;
	keywords?: Array<string>;
	author?: string;
	license?: string;
	scripts?: Scripts;
	private?: boolean;
	bin?: Bin;
	eslintConfig?: EslintrcJson;
	prettier?: PrettierrcJson;
}
export type Dependencies = {
	[key: string]: string;
};
export type Scripts = {
	[key: string]: string;
};
export type Bin = {
	[key: string]: string;
};

export function load(configPath: string, main: boolean): Promise<PackageJsonInfo> {
	return new Promise((fulfill, reject) => {
		if (!configPath) {
			//error, reject
			reject(new Error(`Empty config path provided!`));
			return;
		}

		const packageJsonPath = path.join(path.dirname(configPath), PACKAGE_JSON);

		fs.stat(packageJsonPath, (err, stat) => {
			//ok
			if (!err && stat.isFile()) {
				readPackageJson(packageJsonPath)
					.then((json: PackageJson) => {
						fulfill(
							packageJsonInfo(
								path.dirname(packageJsonPath),
								packageJsonPath,
								map(json),
								main
							)
						);
					})
					.catch(() => {
						reject(
							new Error(
								`Can not load ${PACKAGE_JSON} on path ${packageJsonPath}. Are you sure that it is valid json file?`
							)
						);
					});
				return;
			}
			//error, reject
			reject(
				new Error(
					`Provided path ${packageJsonPath} is not a ${PACKAGE_JSON} valid path. Are you sure that this path is valid?`
				)
			);
		});
	});
}

export function save(configPath: string, json: unknown): Promise<void> {
	return writePackageJson(configPath, json);
}

function packageJsonInfo(
	cwd: string,
	path: string,
	json: PackageJsonPom,
	main: boolean
): PackageJsonInfo {
	return {
		cwd: cwd,
		path: path,
		content: json,
		main: main,
	};
}

export interface Dependency {
	module: string;
	version: string;
	local: boolean;
	type: DependencyType;
}

export enum DependencyType {
	Production = "production",
	Develop = "develop",
	Optional = "optional",
	Peer = "peer",
}

function createDependency(
	dependencies: Dependencies | undefined,
	type: DependencyType
): Array<Dependency> {
	const info: Array<Dependency> = [];

	if (!dependencies) {
		return info;
	}

	const modules = Object.keys(dependencies);
	for (let i = 0; i < modules.length; i++) {
		const module = modules[i];
		info.push(dependency(module, dependencies[module], type));
	}

	return info;
}

function createBundleDependencies(json: PackageJson): Array<string> {
	const bundled = json.bundledDependencies || json.bundleDependencies || [];

	return bundled.slice();
}

function dependency(module: string, version: string, type: DependencyType): Dependency {
	return {
		module,
		version,
		local: versionIsLocal(version),
		type,
	};
}

export function update(
	path: string,
	updater: (json: PackageJson, done: (err?: Error) => void) => void
): Promise<void> {
	return new Promise((resolve, reject) => {
		readPackageJson(path)
			.then((json) => {
				updater(json as PackageJson, (err) => {
					if (err) {
						reject(err);
						return;
					}
					//write updated
					writePackageJson(path, json).then(resolve).catch(reject);
				});
			})
			.catch(reject);
	});
}

function readPackageJson(path: string): Promise<unknown> {
	return new Promise((resolve, reject) => {
		fs.readFile(path, (err, buffer) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(JSON.parse(buffer.toString()));
		});
	});
}

function writePackageJson(configPath: string, json: unknown): Promise<void> {
	const content = JSON.stringify(json, null, "  ");

	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(configPath), { recursive: true }, (dirError) => {
			fs.writeFile(configPath, content, (err) => {
				if (dirError || err) {
					reject(dirError || err);
					return;
				}
				resolve();
			});
		});
	});
}

export function create(name: string, version: string): PackageJson {
	const packageJson = {} as PackageJson;

	packageJson.name = name;
	packageJson.version = version;

	return packageJson;
}

export function map(json: PackageJson): PackageJsonPom {
	const jsonPom: PackageJsonPom = {} as PackageJsonPom;

	jsonPom.name = json.name;
	jsonPom.version = json.version;
	jsonPom.files = json.files;
	jsonPom.dependencies = createDependency(json.dependencies, DependencyType.Production);
	jsonPom.devDependencies = createDependency(json.devDependencies, DependencyType.Develop);
	jsonPom.optionalDependencies = createDependency(
		json.optionalDependencies,
		DependencyType.Optional
	);
	jsonPom.peerDependencies = createDependency(json.peerDependencies, DependencyType.Peer);
	jsonPom.bundledDependencies = createBundleDependencies(json);
	jsonPom.description = json.description;
	jsonPom.main = json.main;
	jsonPom.types = json.types;
	jsonPom.keywords = json.keywords;
	jsonPom.author = json.author;
	jsonPom.license = json.license;
	jsonPom.scripts = json.scripts;
	jsonPom.bin = json.bin;
	jsonPom.private = json.private;
	jsonPom.eslintConfig = json.eslintConfig;
	jsonPom.prettier = json.prettier;

	return jsonPom;
}

export function dependencies(packageJson: PackageJsonInfo, bundled = true): Array<Dependency> {
	const deps = [
		...new Set([
			...packageJson.content.dependencies,
			//NOTE: Do not load dev dependencies for main (root) package.json
			...(packageJson.main ? [] : packageJson.content.devDependencies),
			...packageJson.content.optionalDependencies,
		]),
	];

	//do not include bundled dependencies
	if (!bundled) {
		const bundled = bundledDependencies(packageJson);
		return deps.filter((dep) => {
			return bundled.indexOf(dep.module) === -1;
		});
	}

	return deps;
}

export function bundledDependencies(packageJson: PackageJsonInfo): Array<string> {
	return packageJson.content.bundledDependencies;
}

//versions

const localDependency = "file:";

export function versionIsLocal(version: string) {
	return version.indexOf(localDependency) === 0;
}

export function versionIsExternal(version: string) {
	return version.indexOf(localDependency) === -1;
}

export function resolveLocalVersion(base: string, version: string) {
	const relativePath = version.replace(localDependency, "");
	return path.resolve(base, relativePath);
}

//files

export function files(packageJson: PackageJsonInfo): Array<string> {
	return packageJson.content.files || [];
}
