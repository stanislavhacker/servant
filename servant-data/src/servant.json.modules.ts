import { LibrariesMapping, LibraryMapping } from "./servant.json";

export function createLibraryMapping(
	root: string,
	umd: string,
	commonjs?: string,
	commonjs2?: string,
	amd?: string
): LibraryMapping {
	return {
		amd: amd || umd,
		commonjs: commonjs || umd,
		commonjs2: commonjs2 || umd,
		root: root,
		umd: umd,
	};
}

export const ModulesMapping: LibrariesMapping = {
	react: createLibraryMapping("React", "react"),
	"react-dom": createLibraryMapping("ReactDOM", "react-dom"),
	"react-trend": createLibraryMapping("Trend", "react-trend"),
};
