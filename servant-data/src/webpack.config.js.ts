import * as internal from "module";
import { Extensions, Path, CSS, TS, RESOURCES } from "@servant/servant-files";

import * as path from "path";
import * as fs from "fs";
import * as webpack from "webpack";

import * as MiniCssExtractPlugin from "mini-css-extract-plugin";
import * as CopyWebpackPlugin from "copy-webpack-plugin";
import * as HtmlWebpackPlugin from "html-webpack-plugin";

import * as PackageJson from "./package.json";
import * as ServantJson from "./servant.json";
import * as Modules from "./modules";
import * as TsConfigJson from "./tsconfig.json";
import { invariant } from "./invariant";
import { createPublicTemplate, createTemplateAdditionalFiles } from "./webpack.config.template";

export type WebpackConfigParams = {
	production?: boolean;
	transpile?: boolean;
	excluded?: Array<string>;
};

export function create(
	cwd: string,
	module: Modules.ModuleInfo | null,
	params: WebpackConfigParams = {}
): webpack.Configuration {
	invariant(
		module,
		"No module declaration found. Invalid data provided or is probably error in Servant."
	);

	const packageJson = module.packageJson;
	const servantJson = module.servantJson;

	const directory = packageJson.cwd;
	const production = params.production || false;
	const transpile = params.transpile || false;
	const excluded = params.excluded || [];

	const entry = servantJson.content.entry as string;
	const mode = production ? "production" : "development";

	return {
		context: directory,
		entry: entry,
		mode: mode,
		plugins: plugins(servantJson, directory),
		...createModuleSettings(module, excluded, production, transpile),
		...createNodeSettings(servantJson),
		...createResolve(module),
		...createResolveLoader(cwd, path.join(__dirname, "../")),
		externals: externals(packageJson, servantJson),
		...createOutputDist(packageJson, servantJson),
	};
}

export function tests(
	cwd: string,
	module: Modules.ModuleInfo | null,
	params: WebpackConfigParams = {}
): webpack.Configuration {
	invariant(
		module,
		"No module declaration found. Invalid data provided or is probably error in Servant."
	);

	const packageJson = module.packageJson;
	const servantJson = module.servantJson;

	const directory = packageJson.cwd;
	const production = params.production || false;
	const transpile = params.transpile || false;
	const excluded = params.excluded || [];

	const entry = servantJson.content.test;
	const mode = production ? "production" : "development";

	return {
		context: directory,
		entry: entry,
		mode: mode,
		plugins: plugins(servantJson, directory),
		...createModuleSettings(module, excluded, production, transpile),
		...createNodeSettings(servantJson),
		...createResolve(module),
		...createResolveLoader(cwd, path.join(__dirname, "../")),
		externals: externals(packageJson, servantJson),
		...createOutputTests(packageJson, servantJson),
	};
}

//RESOLVE

function createResolve(module: Modules.ModuleInfo): Pick<webpack.Configuration, "resolve"> {
	const alias = resolveAlias(module);
	const extensions = resolveExtensions(module);

	return {
		resolve: {
			alias,
			extensions,
		},
	};
}

function resolveAlias(module: Modules.ModuleInfo): Record<string, string> {
	const servantJson = module.servantJson;
	const packageJson = module.packageJson;
	const alias: Record<string, string> = {};

	if (servantJson.content.target === ServantJson.ModuleTarget.web_page) {
		const dependencies = PackageJson.dependencies(packageJson, false);
		dependencies.forEach((dep) => {
			resolveModuleAlias(alias, packageJson.cwd, dep.module);
		});
	}
	packageJson.content.bundledDependencies.forEach((dep) => {
		resolveModuleAlias(alias, packageJson.cwd, dep);
	});

	return alias;
}

function resolveExtensions(module: Modules.ModuleInfo): string[] {
	const servantJson = module.servantJson;
	const resolve = servantJson.content.resolve;
	const extensions: Array<string> = [];

	resolve.forEach((ext) => {
		extensions.push(Extensions.create(ext));
		const file = TS.toJS(ext);
		file && extensions.push(file);
	});

	return [...new Set(extensions)];
}

export function resolveModuleAlias(alias: Record<string, string>, root: string, module: string) {
	try {
		alias[`${module}$`] = path.resolve(root, "node_modules", module);
	} catch {
		return;
	}
}

function createResolveLoader(
	cwd: string,
	module: string
): Pick<webpack.Configuration, "resolveLoader"> {
	return {
		resolveLoader: {
			modules: [
				path.join(cwd, "../../"),
				path.join(cwd, "node_modules"),
				path.join(module, "node_modules"),
				"node_modules",
			],
		},
	};
}

//EXTERNALS

function externals(
	packageJson: PackageJson.PackageJsonInfo,
	servantJson: ServantJson.ServantJsonInfo
): Record<string, string | ServantJson.LibraryMapping> {
	const externals = {};

	//externals
	loadExternalModules(packageJson, servantJson, externals);
	//nodejs internals
	loadInternalModules(servantJson, externals);

	//remapped externals
	return loadExternalRemappedModules(servantJson, externals);
}

function loadExternalRemappedModules(
	servantJson: ServantJson.ServantJsonInfo,
	externals: Record<string, string | ServantJson.LibraryMapping> = {}
) {
	//NOTE: For webpage target, we need to bundle all into one
	if (servantJson.content.target === ServantJson.ModuleTarget.web_page) {
		return externals;
	}

	return {
		...externals,
		...ServantJson.getLibrariesMapping(servantJson),
	};
}

function loadExternalModules(
	packageJson: PackageJson.PackageJsonInfo,
	servantJson: ServantJson.ServantJsonInfo,
	externals: Record<string, string | ServantJson.LibraryMapping> = {}
): Record<string, string | ServantJson.LibraryMapping> {
	const dependencies = PackageJson.dependencies(packageJson, false);

	//NOTE: For webpage target, we need to bundle all into one
	if (servantJson.content.target === ServantJson.ModuleTarget.web_page) {
		return externals;
	}

	//get externals
	dependencies.forEach((dep) => {
		externals[dep.module] = ServantJson.getLibraryMapping(servantJson, dep);
	});

	return externals;
}

function loadInternalModules(
	servantJson: ServantJson.ServantJsonInfo,
	externals: Record<string, string | ServantJson.LibraryMapping> = {}
): Record<string, string | ServantJson.LibraryMapping> {
	switch (servantJson.content.target) {
		case ServantJson.ModuleTarget.node:
		case ServantJson.ModuleTarget.node_cli: {
			const internals = internal.builtinModules;
			internals.forEach((int) => {
				externals[int] = int;
			});
			return externals;
		}
		default:
			return externals;
	}
}

//PLUGINS

function plugins(
	servantJson: ServantJson.ServantJsonInfo,
	directory: string
): Array<webpack.WebpackPluginInstance> {
	const array: Array<webpack.WebpackPluginInstance> = [];
	cssPlugin(servantJson, directory, array);
	bannerPlugin(servantJson, directory, array);
	htmlBundlePlugin(servantJson, directory, array);

	return array;
}

function cssPlugin(
	servantJson: ServantJson.ServantJsonInfo,
	directory: string,
	plugins: Array<webpack.WebpackPluginInstance>
) {
	//CSS: extract from build
	plugins.push(
		new MiniCssExtractPlugin({
			filename: CSS.toCSS(servantJson.content.output.filename),
		})
	);

	//CSS: copy
	const entries = ServantJson.entries(servantJson);
	if (entries.css) {
		plugins.push(
			new CopyWebpackPlugin({
				patterns: [entries.css],
			})
		);
	}
}

function bannerPlugin(
	servantJson: ServantJson.ServantJsonInfo,
	directory: string,
	plugins: Array<webpack.WebpackPluginInstance>
) {
	//target is nodejs cli app, add shebang
	if (servantJson.content.target === ServantJson.ModuleTarget.node_cli) {
		plugins.push(
			new webpack.BannerPlugin({
				banner: "#!/usr/bin/env node",
				raw: true,
			})
		);
	}
}

function htmlBundlePlugin(
	servantJson: ServantJson.ServantJsonInfo,
	directory: string,
	plugins: Array<webpack.WebpackPluginInstance>
) {
	//target is web page
	if (servantJson.content.target === ServantJson.ModuleTarget.web_page) {
		const { favicon, publicPath, title } = (servantJson.content.webpage ||
			ServantJson.ServantJsonWebpageDefaults) as Required<ServantJson.ServantWebpage>;
		const additionalFiles = createTemplateAdditionalFiles(servantJson, publicPath);

		plugins.push(
			new HtmlWebpackPlugin({
				favicon,
				publicPath,
				title,
				//static
				filename: "index.html",
				meta: {},
				hash: true,
				minify: "auto",
				templateContent: (params: HtmlWebpackPlugin.TemplateParameter) =>
					createPublicTemplate(params, additionalFiles),
			})
		);
	}
}

//MODULE

function createModuleSettings(
	module: Modules.ModuleInfo,
	excluded: Array<string>,
	production: boolean,
	transpile: boolean
) {
	return {
		module: {
			rules: [
				createTypescriptRule(module, excluded, production, transpile),
				createCssRule(),
				createAssetsRule(),
			],
		},
	};
}

//RULES

function createTypescriptRule(
	module: Modules.ModuleInfo,
	excluded: Array<string>,
	production: boolean,
	transpile: boolean
): webpack.RuleSetRule {
	const servantJson = module.servantJson;

	return {
		test: TS.REGEX,
		use: [createTsLoader(module, production, transpile), createIfdefLoader(module, production)],
		exclude: [
			/node_modules/,
			(file) => {
				let excl = false;

				excl =
					excl ||
					excluded.some((test) => Path.matched(file, path.join(servantJson.cwd, test)));
				excl =
					excl || servantJson.content.clean.some((ext) => Extensions.matched(file, ext));

				return excl;
			},
		],
	};
}

function createCssRule(): webpack.RuleSetRule {
	return {
		test: CSS.REGEX,
		use: [createCssExtractLoader(), createCssLoader()],
		exclude: [/node_modules/],
	};
}

function createAssetsRule(): webpack.RuleSetRule {
	return {
		test: RESOURCES.REGEX,
		type: "asset",
		exclude: [/node_modules/],
	};
}

//LOADERS

function createCssExtractLoader(): string {
	return MiniCssExtractPlugin.loader;
}

function createCssLoader() {
	return {
		loader: "css-loader",
		options: {
			modules: "global",
		},
	};
}

function createTsLoader(module: Modules.ModuleInfo, production: boolean, transpile: boolean) {
	const options = loadConfigFile(module, production, transpile);

	return {
		loader: "ts-loader",
		...options,
	};
}

function createIfdefLoader(module: Modules.ModuleInfo, production: boolean) {
	const packageJson = module.packageJson;

	// define preprocessor variables
	const opts = {
		DEBUG: !production,
		version: packageJson.content.version,
		"ifdef-verbose": false,
	};

	return {
		loader: "ifdef-loader",
		options: opts,
	};
}

//TS LOADER

type TsLoaderOptions = {
	options: {
		context?: string;
		happyPackMode?: boolean;
		transpileOnly?: boolean;
		configFile?: string;
		compilerOptions?: Partial<TsConfigJson.TSConfig>;
	};
};

function loadConfigFile(
	module: Modules.ModuleInfo,
	production: boolean,
	transpile: boolean
): TsLoaderOptions {
	const packageJson = module.packageJson;
	const servantJson = module.servantJson;

	const configPath = path.join(packageJson.cwd, TsConfigJson.TSCONFIG_JSON);
	const dist = path.join(packageJson.cwd, servantJson.content.output.directory);

	const options = {
		options: {
			context: packageJson.cwd,
			transpileOnly: transpile,
			happyPackMode: false,
			compilerOptions: {
				//Rewrite declaration files
				declaration: false,
				//Rewrite out dir to our dir
				outDir: dist,
			},
		},
	} as TsLoaderOptions;

	//config file exists
	if (fs.existsSync(configPath)) {
		options.options.configFile = configPath;
	}

	//generate defaults if file not specified
	if (!options.options.configFile) {
		const opts = options.options;

		opts.compilerOptions = {
			...opts.compilerOptions,
			...TsConfigJson.create(),
		};
	}

	return options;
}

//OUTPUT

function createOutputDist(
	packageJson: PackageJson.PackageJsonInfo,
	servantJson: ServantJson.ServantJsonInfo
) {
	const library = packageJson.content.name || "";
	const outputDist = path.join(servantJson.cwd, servantJson.content.output.directory);
	const outputFilename = servantJson.content.output.filename;

	return createOutput(library, outputDist, outputFilename);
}

function createOutputTests(
	packageJson: PackageJson.PackageJsonInfo,
	servantJson: ServantJson.ServantJsonInfo
) {
	const library = packageJson.content.name || "";
	const outputDist = path.join(packageJson.cwd, servantJson.content.temp);
	const outputFilename = servantJson.content.output.filename;

	return createOutput(library, outputDist, outputFilename);
}

function createOutput(library: string, outputDist: string, outputFilename: string) {
	return {
		output: {
			umdNamedDefine: true,
			library: library,
			libraryTarget: "umd",
			filename: outputFilename,
			path: outputDist,
			globalObject: "this",
		},
	};
}

//NODE

function createNodeSettings(servantJson: ServantJson.ServantJsonInfo) {
	switch (servantJson.content.target) {
		case ServantJson.ModuleTarget.node:
		case ServantJson.ModuleTarget.node_cli:
			return {
				node: {
					global: false,
					__dirname: false,
					__filename: false,
				},
			};
		default:
			//web target
			return {
				node: {
					__dirname: false,
					__filename: false,
				},
			};
	}
}
