import { ServantJson, PackageJson, EslintrcJson, PrettierrcJson, TrancorderJson } from "./";
import { Path } from "@servant/servant-files";
import * as path from "path";

export interface ModuleDirectories {
	sources: string[];
	tests: string[];
	others: string[];
}

export interface ModuleInfo {
	packageJson: PackageJson.PackageJsonInfo;
	servantJson: ServantJson.ServantJsonInfo;
	eslintrcJson: EslintrcJson.EslintrcJsonInfo;
	prettierrcJson: PrettierrcJson.PrettierrcJsonInfo;
	trancorderJson: TrancorderJson.TrancorderJsonInfo;
	dependencies: Array<PackageJson.PackageJsonInfo>;
	directories: ModuleDirectories;
	internal: boolean;
}

export interface ModuleDefinition {
	name: string;
	versions: Array<string>;
	depType: Array<DependencyType>;
	externals: Array<ModuleDefinition>;
	internals: Array<ModuleDefinition>;
	module: ModuleInfo | null;
	missing: boolean;
}

export enum DependencyType {
	Internal = "internal",
	Production = "production",
	Develop = "develop",
	Optional = "optional",
	Peer = "peer",
}

export function create(
	name: string,
	module: ModuleInfo | null,
	missing: boolean
): ModuleDefinition {
	return {
		name,
		versions: [],
		externals: [],
		internals: [],
		depType: [],
		module,
		missing,
	};
}

export function info(
	packageJson: PackageJson.PackageJsonInfo,
	servantJson: ServantJson.ServantJsonInfo,
	eslintrcJson: EslintrcJson.EslintrcJsonInfo,
	prettierrcJson: PrettierrcJson.PrettierrcJsonInfo,
	trancorderJson: TrancorderJson.TrancorderJsonInfo,
	dependencies: Array<PackageJson.PackageJsonInfo>,
	directories: ModuleDirectories,
	internal: boolean
): ModuleInfo {
	return {
		servantJson,
		packageJson,
		eslintrcJson,
		prettierrcJson,
		trancorderJson,
		dependencies,
		internal,
		directories,
	};
}

export function directories(
	servantJson?: ServantJson.ServantJsonInfo,
	directories: Array<string> = []
): ModuleDirectories {
	const mDirectories: ModuleDirectories = {
		sources: [],
		tests: [],
		others: [],
	};

	if (!servantJson) {
		return mDirectories;
	}

	const normalizer = (prev: string[], item: string) => {
		const dir = Path.directory(servantJson.cwd, item);
		return [...prev, dir, Path.patterns.all(dir)];
	};

	//load dirs
	return directories.reduce((prev, directory) => {
		const normalized = Path.normalize(directory);
		const relative = Path.normalize(path.relative(servantJson.cwd, normalized));
		const sourcesPatterns = (servantJson.content.src || []).reduce(normalizer, []);
		const testsPatterns = (servantJson.content.tests || []).reduce(normalizer, []);

		if (Path.matchedOne(normalized, sourcesPatterns)) {
			prev.sources.push(relative);
			return prev;
		}
		if (Path.matchedOne(normalized, testsPatterns)) {
			prev.tests.push(relative);
			return prev;
		}
		prev.others.push(relative);
		return prev;
	}, mDirectories);
}
