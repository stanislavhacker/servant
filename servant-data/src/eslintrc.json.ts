import * as path from "path";
import * as fs from "fs";

export const ESLINTRC_JSON = ".eslintrc.json";

export interface EslintrcJsonInfo {
	path: string;
	cwd: string;
	content: EslintrcJson | null;
}
export interface EslintrcJson {
	root?: boolean;
	parser?: string;
	plugins?: Array<string>;
	extends?: Array<string>;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	rules?: Record<string, any>;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	[key: string]: any;
}

export function load(configPath: string): Promise<EslintrcJsonInfo> {
	return new Promise((fulfill) => {
		const eslintrcJsonPath = path.join(path.dirname(configPath), ESLINTRC_JSON);

		fs.stat(eslintrcJsonPath, (err, stat) => {
			//ok
			if (!err && stat.isFile()) {
				readEslintrcJson(eslintrcJsonPath)
					.then((json: EslintrcJson) => {
						fulfill(
							eslintrcJsonInfo(path.dirname(eslintrcJsonPath), eslintrcJsonPath, json)
						);
					})
					.catch(() => {
						fulfill(eslintrcJsonInfo(path.dirname(eslintrcJsonPath), eslintrcJsonPath));
					});
				return;
			}
			//error, reject
			fulfill(eslintrcJsonInfo(path.dirname(eslintrcJsonPath), eslintrcJsonPath));
		});
	});
}

export function save(configPath: string, json: EslintrcJson): Promise<void> {
	return writeEslintrcJson(configPath, json);
}

export function createDefault(root: boolean): EslintrcJson {
	return enrichDefaults({ root });
}

export function enrichDefaults(config: EslintrcJson): EslintrcJson {
	return {
		...config,
		parser: "@typescript-eslint/parser",
		plugins: ["@typescript-eslint", ...(config.plugins || [])],
		extends: [
			"eslint:recommended",
			"plugin:@typescript-eslint/eslint-recommended",
			"plugin:@typescript-eslint/recommended",
			...(config.extends || []),
		],
	};
}

function eslintrcJsonInfo(
	cwd: string,
	path: string,
	json: EslintrcJson | null = null
): EslintrcJsonInfo {
	return {
		cwd: cwd,
		path: path,
		content: json,
	};
}

function readEslintrcJson(path: string): Promise<EslintrcJson> {
	return new Promise((resolve, reject) => {
		fs.readFile(path, (err, buffer) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(JSON.parse(buffer.toString()));
		});
	});
}

function writeEslintrcJson(configPath: string, json: EslintrcJson): Promise<void> {
	const content = JSON.stringify(json, null, "  ");

	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(configPath), { recursive: true }, (dirError) => {
			fs.writeFile(configPath, content, (err) => {
				if (dirError || err) {
					reject(dirError || err);
					return;
				}
				resolve();
			});
		});
	});
}
