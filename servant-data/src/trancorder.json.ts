import * as path from "path";
import * as fs from "fs";

export const TRANCORDER_JSON = ".trancorder.json";

export interface TrancorderJsonInfo {
	path: string;
	cwd: string;
	content: TrancorderConfig | null;
}

export type TrancorderConfig = {
	entry?: string;
	out?: string;
	in?: string;
	translations?: string;
	sources?: string[];
	defaultLanguage?: string;
	format?: "json";
	exports?: "xliff";
	languages?: string[];
	config?: string;
	debug?: boolean;
};

export function load(configPath: string): Promise<TrancorderJsonInfo> {
	return new Promise((fulfill) => {
		const trancorderJsonPath = path.join(path.dirname(configPath), TRANCORDER_JSON);

		fs.stat(trancorderJsonPath, (err, stat) => {
			//ok
			if (!err && stat.isFile()) {
				loadTrancorderConfig(trancorderJsonPath)
					.then((json: TrancorderConfig) => {
						fulfill(
							trancorderJsonInfo(
								path.dirname(trancorderJsonPath),
								trancorderJsonPath,
								json
							)
						);
					})
					.catch(() => {
						fulfill(
							trancorderJsonInfo(path.dirname(trancorderJsonPath), trancorderJsonPath)
						);
					});
				return;
			}
			//error, reject
			fulfill(trancorderJsonInfo(path.dirname(trancorderJsonPath), trancorderJsonPath));
		});
	});
}

export function save(configPath: string, json: TrancorderConfig): Promise<void> {
	return writeTrancorderJson(configPath, json);
}

export function createDefault(): TrancorderConfig {
	return enrichDefaults();
}

export function enrichDefaults(config: TrancorderConfig | null = {}): TrancorderConfig {
	return {
		...(config || {}),
	};
}

function trancorderJsonInfo(
	cwd: string,
	path: string,
	json: TrancorderConfig | null = null
): TrancorderJsonInfo {
	return {
		cwd: cwd,
		path: path,
		content: json,
	};
}

function loadTrancorderConfig(path: string): Promise<TrancorderConfig> {
	return new Promise((resolve, reject) => {
		fs.readFile(path, (err, buffer) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(JSON.parse(buffer.toString()));
		});
	});
}

function writeTrancorderJson(configPath: string, json: TrancorderConfig): Promise<void> {
	const content = JSON.stringify(json, null, "  ");

	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(configPath), { recursive: true }, (dirError) => {
			fs.writeFile(configPath, content, (err) => {
				if (dirError || err) {
					reject(dirError || err);
					return;
				}
				resolve();
			});
		});
	});
}
