import * as PackageJson from "./package.json";
import * as ServantJson from "./servant.json";
import * as EslintrcJson from "./eslintrc.json";
import * as PrettierrcJson from "./prettierrc.json";
import * as TsConfig from "./tsconfig.json";
import * as WebpackConfig from "./webpack.config.js";
import * as TrancorderJson from "./trancorder.json";
import * as TestsJson from "./tests.json";
import * as WebpackJson from "./webpack.json";
import * as Modules from "./modules";
import * as GitIgnore from "./gitignore";

export {
	PackageJson,
	ServantJson,
	EslintrcJson,
	PrettierrcJson,
	TrancorderJson,
	WebpackConfig,
	TestsJson,
	TsConfig,
	WebpackJson,
	Modules,
	GitIgnore,
};
