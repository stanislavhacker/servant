import * as path from "path";
import * as fs from "fs";

export const PRETTIERRC_JSON = ".prettierrc.json";

export interface PrettierrcJsonInfo {
	path: string;
	cwd: string;
	content: PrettierrcJson | null;
}
export interface PrettierrcJson {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	[key: string]: any;
}

export function load(configPath: string): Promise<PrettierrcJsonInfo> {
	return new Promise((fulfill) => {
		const prettierrcJsonPath = path.join(path.dirname(configPath), PRETTIERRC_JSON);

		fs.stat(prettierrcJsonPath, (err, stat) => {
			//ok
			if (!err && stat.isFile()) {
				readPrettierrcJson(prettierrcJsonPath)
					.then((json: PrettierrcJson) => {
						fulfill(
							prettierrcJsonInfo(
								path.dirname(prettierrcJsonPath),
								prettierrcJsonPath,
								json
							)
						);
					})
					.catch(() => {
						fulfill(
							prettierrcJsonInfo(path.dirname(prettierrcJsonPath), prettierrcJsonPath)
						);
					});
				return;
			}
			//error, reject
			fulfill(prettierrcJsonInfo(path.dirname(prettierrcJsonPath), prettierrcJsonPath));
		});
	});
}

export function save(configPath: string, json: PrettierrcJson): Promise<void> {
	return writePrettierrcJson(configPath, json);
}

export function createDefault(): PrettierrcJson {
	return enrichDefaults();
}

export function enrichDefaults(config: PrettierrcJson | null = {}): PrettierrcJson {
	return {
		...(config || {}),
	};
}

function prettierrcJsonInfo(
	cwd: string,
	path: string,
	json: PrettierrcJson | null = null
): PrettierrcJsonInfo {
	return {
		cwd: cwd,
		path: path,
		content: json,
	};
}

function readPrettierrcJson(path: string): Promise<PrettierrcJson> {
	return new Promise((resolve, reject) => {
		fs.readFile(path, (err, buffer) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(JSON.parse(buffer.toString()));
		});
	});
}

function writePrettierrcJson(configPath: string, json: PrettierrcJson): Promise<void> {
	const content = JSON.stringify(json, null, "  ");

	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(configPath), { recursive: true }, (dirError) => {
			fs.writeFile(configPath, content, (err) => {
				if (dirError || err) {
					reject(dirError || err);
					return;
				}
				resolve();
			});
		});
	});
}
