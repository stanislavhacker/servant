import * as fs from "fs";
import * as path from "path";
import * as ts from "typescript";

export const TSCONFIG_JSON = "tsconfig.json";

export type TSConfig = {
	//compiler options
	compilerOptions: {
		module: "none" | "commonjs" | "amd" | "system" | "umd" | "es6" | "es2015";
		target: "es3" | "es5" | "es6" | "es2016" | "es2017";
		jsx: "react" | "preserve" | "react-native";
		sourceMap: boolean;
		declaration: boolean;
		skipLibCheck: boolean;
		strictNullChecks: boolean;
	};
	//files
	include?: Array<string>;
	exclude?: Array<string>;
	extends?: string;
};

export function save(configPath: string, json: TSConfig): Promise<void> {
	return writeTsConfigJson(configPath, json);
}

export function load(configPath: string): Promise<TSConfig> {
	return new Promise((fulfill, reject) => {
		const tsconfigPath = path.join(path.dirname(configPath), TSCONFIG_JSON);

		fs.stat(tsconfigPath, (err, stat) => {
			//ok
			if (!err && stat.isFile()) {
				readTsConfigJson(tsconfigPath)
					.then(fulfill)
					.catch(() => {
						reject(
							new Error(
								`Can not load ${TSCONFIG_JSON} on path ${tsconfigPath}. Are you sure that it is valid json file?`
							)
						);
					});
				return;
			}
			//error, reject
			reject(
				new Error(
					`Provided path ${tsconfigPath} is not a ${TSCONFIG_JSON} valid path. Are you sure that this path is valid?`
				)
			);
		});
	});
}

export function create(): TSConfig {
	return {
		compilerOptions: {
			module: "commonjs",
			target: "es5",
			sourceMap: true,
			declaration: true,
			jsx: "react",
			skipLibCheck: true,
			strictNullChecks: true,
		},
	};
}

export function options(configPath: string, config: TSConfig): ts.CompilerOptions {
	const result = ts.convertCompilerOptionsFromJson(
		config.compilerOptions,
		path.dirname(configPath),
		configPath
	);
	return result.options;
}

function readTsConfigJson(filepath: string): Promise<TSConfig> {
	return new Promise((resolve, reject) => {
		fs.readFile(filepath, (err, buffer) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(JSON.parse(buffer.toString()));
		});
	});
}

function writeTsConfigJson(configPath: string, json: TSConfig): Promise<void> {
	const content = JSON.stringify(json, null, "  ");

	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(configPath), { recursive: true }, (errDir) => {
			fs.writeFile(configPath, content, (err) => {
				if (errDir || err) {
					reject(errDir || err);
					return;
				}
				resolve();
			});
		});
	});
}
