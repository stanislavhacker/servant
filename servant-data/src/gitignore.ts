import * as fs from "fs";
import * as path from "path";
import * as os from "os";
import * as ServantJson from "./servant.json";
import { Path, Extensions } from "@servant/servant-files";

export const GITIGNORE_FILE = ".gitignore";

export interface GITIGNOREInfo {
	path: string;
	cwd: string;
	content: GITIGNORE;
}

export type GITIGNORE = Array<Header | Message | Pattern>;

export type Header = {
	name: "header";
	content: string;
};
export type Message = {
	name: "message";
	content: string;
};
export type Pattern = {
	name: "pattern";
	content: string;
};

export function save(configPath: string, file: GITIGNORE): Promise<void> {
	return writeGitignoreFile(configPath, file);
}

export function load(configPath: string): Promise<GITIGNOREInfo> {
	return new Promise((fulfill, reject) => {
		const gitignorePath = path.join(path.dirname(configPath), GITIGNORE_FILE);

		fs.stat(gitignorePath, (err, stat) => {
			//ok
			if (!err && stat.isFile()) {
				readGitignoreFile(gitignorePath)
					.then((file) =>
						fulfill(gitignoreInfo(path.dirname(gitignorePath), gitignorePath, file))
					)
					.catch(() => {
						reject(
							new Error(
								`Can not load ${GITIGNORE_FILE} on path ${gitignorePath}. Are you sure that it is valid json file?`
							)
						);
					});
				return;
			}
			//error, reject
			reject(
				new Error(
					`Provided path ${gitignorePath} is not a ${GITIGNORE_FILE} valid path. Are you sure that this path is valid?`
				)
			);
		});
	});
}

export function init(servantJson: ServantJson.ServantJson, language: "ts" | "js"): GITIGNORE {
	const file: GITIGNORE = [];

	file.push(doMessage("Created by Servant build tool."));
	file.push(doPattern(""));

	file.push(doHeader("NodeJS template"));
	file.push(doPattern(""));

	file.push(doMessage("Logs"));
	file.push(doPattern("logs"));
	file.push(doPattern("npm-debug.log*"));
	file.push(doPattern("yarn-debug.log*"));
	file.push(doPattern("yarn-error.log*"));
	file.push(doPattern(""));

	file.push(doMessage("Compiled binary addons (https://nodejs.org/api/addons.html)"));
	file.push(doPattern("build/Release"));
	file.push(doPattern(""));

	file.push(doMessage("Runtime data"));
	file.push(doPattern("pids"));
	file.push(doPattern("*.pid"));
	file.push(doPattern("*.seed"));
	file.push(doPattern("*.pid.lock"));
	file.push(doPattern(""));

	file.push(doMessage("Dependency directories"));
	file.push(doPattern("node_modules/"));
	file.push(doPattern("jspm_packages/"));
	file.push(doPattern("*/package-lock.json"));
	file.push(doPattern("/package-lock.json"));
	file.push(doPattern(""));

	file.push(doMessage("Production data"));
	file.push(doPattern("*.tgz"));
	file.push(doPattern(""));

	file.push(doHeader("IDE settings"));
	file.push(doPattern(""));

	file.push(doMessage("IntelliJ IDEA, WebStorm"));
	file.push(doPattern(".idea/dictionaries/**"));
	file.push(doPattern(""));

	file.push(doHeader("Project universal paths"));
	file.push(doPattern(""));

	file.push(doMessage("Coverage"));
	file.push(doPattern("coverage"));
	file.push(doPattern(""));

	file.push(doMessage("Bower dependency directory (https://bower.io/)"));
	file.push(doPattern("bower_components"));
	file.push(doPattern(""));

	file.push(doMessage("Caches"));
	file.push(doPattern(".npm"));
	file.push(doPattern(".eslintcache"));
	file.push(doPattern(""));

	file.push(doHeader("Project specific file paths"));
	file.push(doMessage("This section when you can add and remove your files!"));
	file.push(doPattern(""));

	file.push(doMessage("Modules"));
	[
		...(servantJson.src || ServantJson.ServantJsonDefaults.src),
		...(servantJson.tests || ServantJson.ServantJsonDefaults.tests),
	].forEach((src) => {
		const dir = Path.directory("./", src);

		if (language === "ts") {
			file.push(
				doPattern(Path.normalize(path.join("**/", dir, Extensions.patterns.all("d.ts"))))
			);
			file.push(
				doPattern(Path.normalize(path.join("**/", dir, Extensions.patterns.all("js"))))
			);
			file.push(
				doPattern(Path.normalize(path.join("**/", dir, Extensions.patterns.all("js.map"))))
			);
		} else {
			file.push(
				doPattern(Path.normalize(path.join("**/", dir, Extensions.patterns.all("js.map"))))
			);
		}
	});
	file.push(doPattern(""));

	file.push(doMessage("Distribution"));

	const output = servantJson.output || {};
	file.push(
		doPattern(
			Path.normalize(
				path.join(
					"**/",
					Path.patterns.all(
						output.directory || ServantJson.ServantJsonOutputDefaults.directory
					)
				)
			)
		)
	);
	file.push(doPattern(""));

	file.push(doMessage("Temporary"));
	file.push(
		doPattern(
			Path.normalize(servantJson.temp || ServantJson.ServantJsonDefaults.temp || ".temp")
		)
	);
	file.push(doPattern(""));

	return file;
}

function gitignoreInfo(cwd: string, path: string, gitignore: GITIGNORE): GITIGNOREInfo {
	return {
		cwd: cwd,
		path: path,
		content: gitignore,
	};
}

function readGitignoreFile(filepath: string): Promise<GITIGNORE> {
	return new Promise((resolve, reject) => {
		fs.readFile(filepath, (err, buffer) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(parse(buffer.toString()));
		});
	});
}

function writeGitignoreFile(configPath: string, file: GITIGNORE): Promise<void> {
	const content = serialize(file);

	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(configPath), { recursive: true }, (errDir) => {
			fs.writeFile(configPath, content, (err) => {
				if (errDir || err) {
					reject(errDir || err);
					return;
				}
				resolve();
			});
		});
	});
}

export function serialize(file: GITIGNORE): string {
	let content = "";

	file.forEach((item) => {
		switch (item.name) {
			case "header":
				content += `### ${item.content}${os.EOL}`;
				break;
			case "message":
				content += `# ${item.content}${os.EOL}`;
				break;
			default:
				content += `${item.content}${os.EOL}`;
				break;
		}
	});
	return content;
}

export function parse(content: string): GITIGNORE {
	const file: GITIGNORE = [];
	const lines = content.split(/\r\n|\r|\n/);

	lines.forEach((item) => {
		const line = item.trim();

		//header
		if (line.indexOf("###") === 0) {
			file.push(doHeader(line.slice(3).trim()));
			return;
		}
		//message
		if (line.indexOf("#") === 0) {
			file.push(doMessage(line.slice(1).trim()));
			return;
		}
		//pattern
		file.push(doPattern(line));
	});
	return file;
}

export function doHeader(content: string): Header {
	return {
		name: "header",
		content: content,
	};
}
export function doMessage(content: string): Message {
	return {
		name: "message",
		content: content,
	};
}
export function doPattern(content: string): Pattern {
	return {
		name: "pattern",
		content: content,
	};
}
