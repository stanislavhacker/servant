import { TS, JS, CSS, LESS, SASS } from "@servant/servant-files";
import * as path from "path";
import * as fs from "fs";

import * as PackageJson from "./package.json";
import { LibrariesDefaults } from "./servant.json.module";
import { createLibraryMapping, ModulesMapping } from "./servant.json.modules";

export interface ServantJsonInfo {
	path: string;
	cwd: string;
	content: ServantJson;
}
export interface ServantJson {
	package: string;
	modules: Array<string>;
	//output
	output: ServantOutput;
	webpage: ServantWebpage;
	//entry
	entry: string | boolean;
	test: string;
	//directories
	src: Array<string>;
	tests: Array<string>;
	resources: Array<string>;
	watch: Array<string>;
	resolve: Array<string>;
	//extensions
	clean: Array<string>;
	//temp
	temp: string;
	//libraries
	libraries?: LibrariesMapping;
	mappings?: WebMapping;
	shared?: SharedPackages;
	//target
	target: ModuleTarget;
	//commands
	publish: PublishCommand;
	prettify?: ServantPrettify;
	//others
	registry: string | RegistryMapping;
	//issues tracker
	issues: IssuesSettings;
	//server
	server: ServerSettings;
	//testing
	testing?: TestingSettings;
	//generators
	generators?: string;
}

export enum ModuleTarget {
	node_cli = "node-cli",
	node = "node",
	web = "web",
	web_page = "web_page",
}

export interface ServantOutput {
	directory: string;
	filename: string;
	resolve: Array<string>;
}
export interface ServantWebpage {
	title?: string;
	favicon?: string;
	publicPath?: string;
}
export interface LibrariesMapping {
	[key: string]: LibraryMapping | string;
}
export interface RegistryMapping {
	[key: string]: string;
}
export interface LibraryMapping {
	root: string;
	umd: string;
	commonjs?: string;
	commonjs2?: string;
	amd?: string;
}
export interface WebMapping {
	[key: string]: string | false;
}
export interface SharedPackages {
	[key: string]: string;
}
export interface PublishCommand {
	increment:
		| "major"
		| "premajor"
		| "minor"
		| "preminor"
		| "patch"
		| "prepatch"
		| "prerelease"
		| "none";
	access: "public" | "restricted";
	stripDev: boolean;
	commitMessage: string;
}
export interface ServantPrettify {
	sources?: Array<string>;
	extensions?: Array<string>;
}

export interface ServerSettings {
	entries: {
		[key: string]: ServerEntry;
	};
	css: Array<string>;
	js: Array<string>;
}
export interface ServerEntry {
	title?: string;
	template?: string;
	command?: {
		file?: string;
		args?: Array<string>;
	};
	shared?: {
		[key: string]: string;
	};
}

export interface IssuesSettings {
	[key: string]: string;
}

export type Browsers = "Webkit" | "Firefox" | "Chromium";
export interface TestingSettings {
	browsers: Array<Browsers>;
	devices: Array<string>;
}

export const ServantJsonOutputDefaults = {
	directory: "./dist",
	filename: "index.js",
	resolve: [JS.MAIN, CSS.MAIN],
};

export const ServantJsonWebpageDefaults = {
	title: "",
	favicon: undefined,
	publicPath: "./",
};

export const ServantJsonPrettifyDefaults = {
	extensions: [],
	sources: [],
};

export const ServantJsonTrancorderDefaults = {
	translations: "src/translations",
};

export const ServantJsonPublishDefaults = {
	access: "public",
	increment: "patch",
	stripDev: false,
	commitMessage: "Incrementing version by '$increment' number. New version is '$version'.",
};

export const ServantJsonServerDefaults = {
	entries: {},
	css: [],
	js: [],
};

export const ServantJsonDefaults = {
	package: "",
	modules: ["./"],
	output: ServantJsonOutputDefaults,
	webpage: ServantJsonWebpageDefaults,
	entry: "./src/index",
	test: "./tests/index",
	src: ["src/**/*"],
	tests: ["tests/**/*"],
	resolve: [...TS.EXT, ...JS.EXT],
	resources: [],
	watch: [],
	clean: [TS.MAP],
	prettify: ServantJsonPrettifyDefaults,
	temp: ".temp",
	libraries: undefined,
	mappings: undefined,
	shared: undefined,
	target: ModuleTarget.web,
	publish: ServantJsonPublishDefaults,
	registry: "",
	issues: {},
	testing: undefined,
	generators: undefined,
	server: ServantJsonServerDefaults,
} as ServantJson;

export const SERVANT_JSON = "servant.json";

export function load(
	configPath: string,
	packageJson?: PackageJson.PackageJsonInfo,
	servantJson?: ServantJsonInfo
): Promise<ServantJsonInfo> {
	return new Promise((fulfill, reject) => {
		const servantJsonPath = path.join(path.dirname(configPath), SERVANT_JSON);

		fs.stat(servantJsonPath, (err, stat) => {
			//ok
			if (!err && stat.isFile()) {
				readServantJson(servantJsonPath)
					.then((json: ServantJson) => {
						fulfill(
							servantJsonInfo(
								path.dirname(servantJsonPath),
								servantJsonPath,
								fillServantJson(json, packageJson, servantJson)
							)
						);
					})
					.catch(() => {
						reject(
							new Error(
								`Can not load ${SERVANT_JSON} on path ${servantJsonPath}. Are you sure that it is valid json file?`
							)
						);
					});
				return;
			}
			//create default servant.json
			if (packageJson) {
				fulfill(
					servantJsonInfo(
						path.dirname(servantJsonPath),
						servantJsonPath,
						fillServantJson({}, packageJson, servantJson)
					)
				);
				return;
			}
			//error, reject
			reject(
				new Error(
					`Provided path ${servantJsonPath} is not a ${SERVANT_JSON} valid path. Are you sure that this path is valid?`
				)
			);
		});
	});
}

export function save(configPath: string, json: ServantJson): Promise<void> {
	return writeServantJson(configPath, json);
}

const types = "@types";
export function library(servantJson: ServantJsonInfo, name: string): string | null {
	const lib = (servantJson.content.mappings || {})[name];

	//NOTE: Types modules has no content, return empty
	if (name.indexOf(types) === 0) {
		return "";
	}
	//NOTE: Skip including of modules without content (eq: resources, fonts, images modules)
	if (lib === false) {
		return "";
	}
	//load normal
	return lib || LibrariesDefaults[name] || null;
}

export type RegistryInfo = {
	registry: string;
	scopes: {
		[key: string]: string;
	};
};
export function registry(registry: string | RegistryMapping = ""): RegistryInfo {
	if (registry && typeof registry === "string") {
		return createRegistryInfo(registry);
	}
	if (registry && typeof registry === "object") {
		const mapping = { ...(registry as RegistryMapping) };
		const regData = loadRegistryFrom(mapping);
		delete mapping[regData[1]];
		return createRegistryInfo(regData[0], mapping);
	}
	return createRegistryInfo();
}
function createRegistryInfo(registry?: string, scopes?: { [key: string]: string }): RegistryInfo {
	return {
		scopes: scopes || {},
		registry: registry || "",
	};
}

export type EntryFile = {
	from: string;
	to: string;
};
export type Entries = {
	css: EntryFile | null;
	less: EntryFile | null;
	sass: EntryFile | null;
};
export function entries(servantJson: ServantJsonInfo | undefined): Entries {
	const entries: Entries = { css: null, less: null, sass: null };

	if (!servantJson) {
		return entries;
	}

	const directory = servantJson.cwd;
	const filename = servantJson.content.output.filename;
	const entry = servantJson.content.entry;

	//CSS
	if (typeof entry === "string") {
		const from = CSS.toCSS(path.join(directory, entry));
		const to = CSS.toCSS(
			path.join(directory, servantJson.content.output.directory, CSS.toCSS(filename))
		);

		if (fs.existsSync(from)) {
			entries.css = { from: from, to: to };
		}
	}
	//LESS
	if (typeof entry === "string") {
		const from = LESS.toLESS(path.join(directory, entry));
		const to = LESS.toLESS(
			path.join(directory, servantJson.content.output.directory, LESS.toLESS(filename))
		);

		if (fs.existsSync(from)) {
			entries.less = { from: from, to: to };
		}
	}
	//SASS
	if (typeof entry === "string") {
		const from = SASS.toSASS(path.join(directory, entry));
		const to = SASS.toSASS(
			path.join(directory, servantJson.content.output.directory, SASS.toSASS(filename))
		);

		if (fs.existsSync(from)) {
			entries.sass = { from: from, to: to };
		}
	}

	return entries;
}

export function fill(
	json: Partial<ServantJson>,
	packageJson?: PackageJson.PackageJsonInfo,
	servantJson?: ServantJsonInfo
) {
	return fillServantJson(json, packageJson, servantJson);
}

function servantJsonInfo(cwd: string, path: string, json: ServantJson): ServantJsonInfo {
	return {
		cwd: cwd,
		path: path,
		content: json,
	};
}

function fillServantJson(
	json: Partial<ServantJson>,
	packageJson?: PackageJson.PackageJsonInfo,
	servantJson?: ServantJsonInfo
): ServantJson {
	//load from package json
	if (packageJson) {
		json.output = createOutput(packageJson.content.name, json.output, json.output);
		json.server = createServer(json.server, json.server);
		json.package = json.package || packageJson.content.name;
	}

	//load some data from parent servant json
	if (servantJson) {
		json.output = createOutput(
			json.package || packageJson?.content.name,
			servantJson.content.output,
			json.output
		);
		json.webpage = createWebpage(
			json.package || packageJson?.content.name,
			servantJson.content.webpage,
			json.webpage
		);
		json.server = createServer(servantJson.content.server, json.server);
		json.publish = createPublish(servantJson.content.publish, json.publish);
		json.entry = createEntry(json.entry, servantJson.content.entry);
		json.issues = createIssues(json.issues, servantJson.content.issues);
		json.registry = createRegistry(json.registry, servantJson.content.registry);
		json.testing = createTesting(json.testing, servantJson.content.testing);
		json.generators = json.generators || servantJson.content.generators;
		json.test = json.test || servantJson.content.test;
		json.src = json.src || servantJson.content.src;
		json.tests = json.tests || servantJson.content.tests;
		json.clean = json.clean || servantJson.content.clean;
		json.prettify = createPrettify(json.prettify, servantJson.content.prettify);
		json.temp = json.temp || servantJson.content.temp;
		json.libraries = { ...(servantJson.content.libraries || {}), ...(json.libraries || {}) };
		json.shared = { ...(servantJson.content.shared || {}), ...(json.shared || {}) };
		json.mappings = { ...(servantJson.content.mappings || {}), ...(json.mappings || {}) };
		json.resources = (json.resources || []).concat(servantJson.content.resources || []);
		json.watch = (json.watch || []).concat(servantJson.content.watch || []);
		json.target = json.target || servantJson.content.target;
		json.resolve = json.resolve || servantJson.content.resolve;
	}

	//load defaults
	json.output = createOutput(json.package || packageJson?.content.name, json.output, json.output);
	json.webpage = createWebpage(
		json.package || packageJson?.content.name,
		json.webpage,
		json.webpage
	);
	json.publish = createPublish(json.publish, json.publish);
	json.server = createServer(json.server, json.server);
	json.entry = createEntry(json.entry, ServantJsonDefaults.entry);
	json.issues = createIssues(json.issues, ServantJsonDefaults.issues);
	json.registry = createRegistry(json.registry, ServantJsonDefaults.registry);
	json.test = json.test || ServantJsonDefaults.test;
	json.libraries = json.libraries || ServantJsonDefaults.libraries;
	json.shared = json.shared || ServantJsonDefaults.shared;
	json.testing = json.testing || ServantJsonDefaults.testing;
	json.generators = json.generators || ServantJsonDefaults.generators;
	json.mappings = json.mappings || ServantJsonDefaults.mappings;
	json.modules = json.modules || ServantJsonDefaults.modules;
	json.src = json.src || ServantJsonDefaults.src;
	json.tests = json.tests || ServantJsonDefaults.tests;
	json.clean = json.clean || ServantJsonDefaults.clean;
	json.prettify = json.prettify || ServantJsonDefaults.prettify;
	json.temp = json.temp || ServantJsonDefaults.temp;
	json.resources = json.resources || ServantJsonDefaults.resources;
	json.watch = json.watch || ServantJsonDefaults.watch;
	json.target = json.target || ServantJsonDefaults.target;
	json.resolve = json.resolve || ServantJsonDefaults.resolve;

	return json as ServantJson;
}

function createEntry(entry1?: string | boolean, entry2?: string | boolean): string | boolean {
	if (entry1 === false) {
		return entry1;
	}
	return entry1 || entry2 || false;
}

function createIssues(
	issues1?: IssuesSettings | undefined,
	issues2?: IssuesSettings | undefined
): IssuesSettings {
	return { ...(issues1 || {}), ...(issues2 || {}) };
}

function createOutput(
	name: string | undefined | null,
	out1?: ServantOutput,
	out2?: ServantOutput
): ServantOutput {
	let parsedName = "";
	const output1 = (out1 || {}) as ServantOutput;
	const output2 = (out2 || {}) as ServantOutput;

	//try parse name from string
	if (name) {
		parsedName = (name || "").replace(/@/g, "").replace(/\//g, ".").toLowerCase() + ".js";
	}

	const output = {} as ServantOutput;
	output.directory =
		output2.directory || output1.directory || ServantJsonOutputDefaults.directory;
	output.filename =
		output2.filename || output1.filename || parsedName || ServantJsonOutputDefaults.filename;
	output.resolve = output2.resolve || output1.resolve || ServantJsonOutputDefaults.resolve;

	return output;
}

function createWebpage(
	name: string | null | undefined,
	wp1?: ServantWebpage,
	wp2?: ServantWebpage
) {
	let parsedName = "";
	const webpage1 = (wp1 || {}) as ServantWebpage;
	const webpage2 = (wp2 || {}) as ServantWebpage;

	//try parse name from string
	if (name) {
		parsedName = (name || "").replace(/@/g, "").replace(/\//g, ".").toLowerCase();
	}

	const webpage = {} as ServantWebpage;
	webpage.title =
		webpage2.title || webpage1.title || parsedName || ServantJsonWebpageDefaults.title;
	webpage.publicPath =
		webpage2.publicPath || webpage1.publicPath || ServantJsonWebpageDefaults.publicPath;
	webpage.favicon = webpage2.favicon || webpage1.favicon || ServantJsonWebpageDefaults.favicon;

	return webpage;
}

function createPublish(pub1?: PublishCommand, pub2?: PublishCommand): PublishCommand {
	const publish1 = (pub1 || {}) as PublishCommand;
	const publish2 = (pub2 || {}) as PublishCommand;

	const publish = {} as PublishCommand;
	publish.increment =
		publish2.increment || publish1.increment || ServantJsonPublishDefaults.increment;
	publish.access = publish2.access || publish1.access || ServantJsonPublishDefaults.access;
	publish.stripDev =
		publish2.stripDev || publish1.stripDev || ServantJsonPublishDefaults.stripDev;
	publish.commitMessage =
		publish2.commitMessage ||
		publish1.commitMessage ||
		ServantJsonPublishDefaults.commitMessage;

	return publish;
}

function createServer(ser1?: ServerSettings, ser2?: ServerSettings): ServerSettings {
	const server2 = (ser2 || {}) as ServerSettings;

	const server = {} as ServerSettings;
	server.entries = server2.entries || ServantJsonServerDefaults.entries;
	server.css = server2.css || ServantJsonServerDefaults.css;
	server.js = server2.js || ServantJsonServerDefaults.js;

	return server;
}

function createRegistry(
	reg1?: string | RegistryMapping,
	reg2?: string | RegistryMapping
): string | RegistryMapping {
	//first string, second mapping
	if (typeof reg1 === "string" && typeof reg2 === "object") {
		const reg = loadRegistryFrom(reg2);
		reg2[reg[1]] = reg1 || reg[0];
		return reg2;
	}
	//first mapping, second string
	if (typeof reg1 === "object" && typeof reg2 === "string") {
		const reg = loadRegistryFrom(reg1);
		reg1[reg[1]] = reg[0] || reg2;
		return reg1;
	}
	//first mapping, second mapping
	if (typeof reg1 === "object" && typeof reg2 === "object") {
		return {
			...reg1,
			...reg2,
		};
	}
	//both string and defined
	return reg1 || reg2 || "";
}

function createTesting(
	reg1?: TestingSettings,
	reg2?: TestingSettings
): TestingSettings | undefined {
	//merge
	if (reg1 && reg2) {
		return {
			browsers: [...new Set([...reg1.browsers, ...reg2.browsers])],
			devices: [...new Set([...reg1.devices, ...reg2.devices])],
		};
	}
	//choose set one
	return reg1 || reg2;
}

function createPrettify(
	prettify1?: ServantPrettify,
	prettify2?: ServantPrettify
): ServantPrettify | undefined {
	//merge
	if (prettify1 && prettify2) {
		return {
			sources: [...new Set([...(prettify1.sources || []), ...(prettify2.sources || [])])],
			extensions: [
				...new Set([...(prettify1.extensions || []), ...(prettify2.extensions || [])]),
			],
		};
	}
	//choose set one
	return prettify1 || prettify2;
}

function loadRegistryFrom(reg: RegistryMapping): [string, string] {
	return (
		loadRegistry(reg, "") ||
		loadRegistry(reg, "registry") ||
		loadRegistry(reg, "noscope") ||
		loadRegistry(reg, "default") || ["", "default"]
	);
}
function loadRegistry(reg: RegistryMapping, key: string): [string, string] | null {
	if (reg[key]) {
		return [reg[key], key];
	}
	return null;
}

function readServantJson(path: string): Promise<ServantJson> {
	return new Promise((resolve, reject) => {
		fs.readFile(path, (err, buffer) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(JSON.parse(buffer.toString()));
		});
	});
}

function writeServantJson(configPath: string, json: ServantJson): Promise<void> {
	const content = JSON.stringify(json, null, "  ");

	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(configPath), { recursive: true }, (dirErr) => {
			fs.writeFile(configPath, content, (err) => {
				if (dirErr || err) {
					reject(dirErr || err);
					return;
				}
				resolve();
			});
		});
	});
}

export function create(name: string): ServantJson {
	const servantJson = {} as ServantJson;

	servantJson.package = name;

	return servantJson;
}

export function getLibraryMapping(
	servantJson: ServantJsonInfo,
	dep: PackageJson.Dependency
): LibraryMapping | string {
	const module = dep.module;

	if (dep.local) {
		return createLibraryMapping(dep.module, dep.module);
	}
	return getLibrary(servantJson.content.libraries, module);
}

export function getTesting(servantJson: ServantJsonInfo): TestingSettings {
	const testing = servantJson.content.testing;

	if (testing) {
		return {
			devices: testing.devices.slice(),
			browsers: testing.browsers.slice(),
		};
	}

	return (
		servantJson.content.testing || {
			devices: [],
			browsers: ["Webkit"],
		}
	);
}

export function isComplexTesting(testing: Partial<TestingSettings> | null | undefined): boolean {
	if (testing && testing.devices && testing.devices.length > 0) {
		return true;
	}
	if (testing && testing.browsers && testing.browsers.length > 1) {
		return true;
	}
	return Boolean(testing && testing.browsers && testing.browsers[0] !== "Webkit");
}

export function getLibrariesMapping(servantJson: ServantJsonInfo): {
	[key: string]: LibraryMapping | string;
} {
	const externals = {};

	//load from servant json
	Object.keys(servantJson.content.libraries || {}).forEach((key) => {
		externals[key] = getLibrary(servantJson.content.libraries, key);
	});

	return externals;
}

export function getSharedPackages(servantJson: ServantJsonInfo): {
	relative: Record<string, string>;
	absolute: Record<string, string>;
} {
	const shared = servantJson.content.shared || {};
	const packages = Object.keys(shared);

	const absolute = packages.reduce((prev, key) => {
		prev[key] = path.join(servantJson.cwd, shared[key]);
		return prev;
	}, {} as Record<string, string>);

	return {
		relative: shared,
		absolute,
	};
}

export function getPrettify(servantJson: ServantJsonInfo): Required<ServantPrettify> {
	const prettify = servantJson.content.prettify || {};

	return {
		sources: prettify.sources ?? [],
		extensions: prettify.extensions ?? [],
	};
}

export function getServerEntryCommand(
	entry: string,
	servantJson: ServantJsonInfo,
	commandOptional: ServerEntry["command"]
): NonNullable<ServerEntry["command"]> {
	const cwd = servantJson.cwd;
	const output = servantJson.content.output ?? ServantJsonOutputDefaults;
	const command = {
		file: path.join(path.relative(entry, cwd), output.directory, output.filename),
		args: [],
		...commandOptional,
	};

	return {
		...command,
		file: path.join(entry, command.file),
	};
}

function getLibrary(
	libraries: LibrariesMapping | null | undefined,
	module: string
): LibraryMapping | string {
	return (libraries || {})[module] || ModulesMapping[module] || module;
}
