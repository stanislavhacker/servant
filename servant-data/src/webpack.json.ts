import { Modules } from "./";

export type WebpackJson = {
	cwd: string | null;
	entry: string | null;
	production: boolean;
	transpile: boolean;
	module: Modules.ModuleInfo | null;
};

export function create(cwd: string | null, entry: string | null): WebpackJson {
	return {
		cwd: cwd,
		entry: entry,
		module: null,
		production: false,
		transpile: false,
	};
}

export function remap(webpack: Partial<WebpackJson>): WebpackJson {
	const webpackJson = create(webpack.cwd || null, webpack.entry || null);

	webpackJson.module = webpack.module || null;
	webpackJson.production = webpack.production || false;
	webpackJson.transpile = webpack.transpile || false;

	return webpackJson;
}

export type WebpackCompile = {
	errors: Array<CompileError>;
	output: string;
};

export type CompileError = {
	message: string;
	stack: string;
	name: string;
};
