import { Path } from "@servant/servant-files";
import { Modules } from "./";

export type TestsJson = {
	cwd: string | null;
	entry: string | null;
	files: Array<string>;
	extensions: Array<string>;
	externals: Array<string>;
	module: Modules.ModuleInfo | null;
	browsers: Array<string>;
	devices: Array<string>;
	gui: boolean;
};

export function create(cwd: string | null, entry: string | null, files: Array<string>): TestsJson {
	return {
		cwd: cwd ? Path.normalize(cwd) : null,
		entry: entry ? Path.normalize(entry) : null,
		files: files,
		browsers: [],
		devices: [],
		extensions: [],
		externals: [],
		module: null,
		gui: false,
	};
}

export function remap(tests: Partial<TestsJson>): TestsJson {
	const testsJson = create(tests.cwd || null, tests.entry || null, tests.files || []);

	testsJson.module = tests.module || null;
	testsJson.extensions = tests.extensions || [];
	testsJson.externals = tests.externals || [];
	testsJson.browsers = tests.browsers || [];
	testsJson.devices = tests.devices || [];
	testsJson.gui = tests.gui || false;

	return testsJson;
}
