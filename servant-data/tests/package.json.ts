/* eslint-disable @typescript-eslint/ban-ts-comment */
import {
	PACKAGE_JSON,
	versionIsLocal,
	create,
	save,
	load,
	update,
	map,
	DependencyType,
	PackageJsonDefaults,
	PackageJsonInfo,
	dependencies,
	files,
	resolveLocalVersion,
} from "../src/package.json";
import {
	PackageJsonEmpty,
	PackageJsonEslintConfig,
	PackageJsonPrettierConfig,
	PackageJsonServant,
} from "./files/package.jsons";
import { Path } from "@servant/servant-files";
import * as fs from "fs";
import * as path from "path";

describe("package.json", () => {
	it("name", () => {
		expect(PACKAGE_JSON).toBe("package.json");
	});

	it("create", () => {
		const pj = create("Test", "0.9.1");
		expect(pj).toEqual({
			name: "Test",
			version: "0.9.1",
		});
	});

	it("map to pom", () => {
		const pj = map(create("Test", "0.9.1"));
		expect(pj).toEqual({
			name: "Test",
			version: "0.9.1",
			files: undefined,
			dependencies: [],
			devDependencies: [],
			optionalDependencies: [],
			bundledDependencies: [],
			peerDependencies: [],
			description: undefined,
			main: undefined,
			types: undefined,
			keywords: undefined,
			author: undefined,
			license: undefined,
			scripts: undefined,
			bin: undefined,
			private: undefined,
			eslintConfig: undefined,
			prettier: undefined,
		});
	});

	it("defaults", () => {
		expect(PackageJsonDefaults).toEqual({
			name: "",
			version: "1.0.0",
			description: "",
			keywords: [],
			author: "",
			license: "ISC",
		});
	});

	describe("dependencies", () => {
		it("load all supported devs", () => {
			const packageJson = create("Test", "0.9.1");

			packageJson.dependencies = { dep1: "0.1.2" };
			packageJson.devDependencies = { dep2: "0.1.2" };
			packageJson.optionalDependencies = { dep3: "0.1.2" };

			const pj: PackageJsonInfo = {
				content: map(packageJson),
				cwd: "/test",
				path: "test/package.json",
				main: false,
			};

			expect(dependencies(pj)).toEqual([
				{ module: "dep1", version: "0.1.2", local: false, type: DependencyType.Production },
				{ module: "dep2", version: "0.1.2", local: false, type: DependencyType.Develop },
				{ module: "dep3", version: "0.1.2", local: false, type: DependencyType.Optional },
			]);

			expect(dependencies(pj, false)).toEqual([
				{ module: "dep1", version: "0.1.2", local: false, type: DependencyType.Production },
				{ module: "dep2", version: "0.1.2", local: false, type: DependencyType.Develop },
				{ module: "dep3", version: "0.1.2", local: false, type: DependencyType.Optional },
			]);
		});

		it("load all supported devs for main module", () => {
			const packageJson = create("Test", "0.9.1");

			packageJson.dependencies = { dep1: "0.1.2" };
			packageJson.devDependencies = { dep2: "0.1.2" };
			packageJson.optionalDependencies = { dep3: "0.1.2" };

			const pj: PackageJsonInfo = {
				content: map(packageJson),
				cwd: "/test",
				path: "test/package.json",
				main: true,
			};

			expect(dependencies(pj)).toEqual([
				{ module: "dep1", version: "0.1.2", local: false, type: DependencyType.Production },
				{ module: "dep3", version: "0.1.2", local: false, type: DependencyType.Optional },
			]);

			expect(dependencies(pj, false)).toEqual([
				{ module: "dep1", version: "0.1.2", local: false, type: DependencyType.Production },
				{ module: "dep3", version: "0.1.2", local: false, type: DependencyType.Optional },
			]);
		});

		it("load all supported devs without bundled", () => {
			const packageJson = create("Test", "0.9.1");

			packageJson.dependencies = { dep1: "0.1.2" };
			packageJson.devDependencies = { dep2: "0.1.2" };
			packageJson.optionalDependencies = { dep3: "0.1.2" };
			packageJson.bundledDependencies = ["dep1", "dep2"];

			const pj: PackageJsonInfo = {
				content: map(packageJson),
				cwd: "/test",
				path: "test/package.json",
				main: false,
			};

			expect(dependencies(pj)).toEqual([
				{ module: "dep1", version: "0.1.2", local: false, type: DependencyType.Production },
				{ module: "dep2", version: "0.1.2", local: false, type: DependencyType.Develop },
				{ module: "dep3", version: "0.1.2", local: false, type: DependencyType.Optional },
			]);

			expect(dependencies(pj, false)).toEqual([
				{ module: "dep3", version: "0.1.2", local: false, type: DependencyType.Optional },
			]);
		});

		it("load all supported devs without bundled 2", () => {
			const packageJson = create("Test", "0.9.1");

			packageJson.dependencies = { dep1: "0.1.2" };
			packageJson.devDependencies = { dep2: "0.1.2" };
			packageJson.optionalDependencies = { dep3: "0.1.2" };
			packageJson.bundleDependencies = ["dep1", "dep2"];

			const pj: PackageJsonInfo = {
				content: map(packageJson),
				cwd: "/test",
				path: "test/package.json",
				main: false,
			};

			expect(dependencies(pj)).toEqual([
				{ module: "dep1", version: "0.1.2", local: false, type: DependencyType.Production },
				{ module: "dep2", version: "0.1.2", local: false, type: DependencyType.Develop },
				{ module: "dep3", version: "0.1.2", local: false, type: DependencyType.Optional },
			]);

			expect(dependencies(pj, false)).toEqual([
				{ module: "dep3", version: "0.1.2", local: false, type: DependencyType.Optional },
			]);
		});
	});

	describe("files", () => {
		it("load files with empty", () => {
			const packageJson = create("Test", "0.9.1");

			const pj: PackageJsonInfo = {
				content: map(packageJson),
				cwd: "/test",
				path: "test/package.json",
				main: false,
			};

			expect(files(pj)).toEqual([]);
		});

		it("load files with filled", () => {
			const packageJson = create("Test", "0.9.1");

			packageJson.files = ["dist/", "LICENSE", "README.md"];

			const pj: PackageJsonInfo = {
				content: map(packageJson),
				cwd: "/test",
				path: "test/package.json",
				main: false,
			};

			expect(files(pj)).toEqual(["dist/", "LICENSE", "README.md"]);
		});
	});

	describe("version checker", () => {
		it("0.9.1", () => {
			expect(versionIsLocal("0.9.1")).toBe(false);
		});

		it("file:../test", () => {
			expect(versionIsLocal("file:../test")).toBe(true);
		});

		it("a.file:../test", () => {
			expect(versionIsLocal("a.file:../test")).toBe(false);
		});

		it("convert local version to relative", () => {
			expect(resolveLocalVersion("/path/to/project/module/", "file:../test")).toContain(
				path.normalize("/path/to/project/test")
			);
		});
	});

	describe("I/O operations", () => {
		let stats: fs.Stats;
		let data: Buffer;
		let saved: Buffer | string | null = null;
		let statError: Error | null;
		let readFileError: Error | null;
		let writeFileError: Error | null;
		let mkdirError: Error | null;

		beforeEach(() => {
			saved = null;
			stats = new fs.Stats();
			data = Buffer.from("");
			statError = null;
			readFileError = null;
			writeFileError = null;
			mkdirError = null;

			// @ts-ignore
			spyOn(fs, "stat").and.callFake((path, callback) => {
				callback(statError as Error, stats);
			});
			// @ts-ignore
			spyOn(fs, "readFile").and.callFake((path, callback) => {
				callback(readFileError, data);
			});
			// @ts-ignore
			spyOn(fs, "writeFile").and.callFake((path, content, callback) => {
				saved = content;
				callback(writeFileError);
			});
			// @ts-ignore
			spyOn(fs, "mkdir").and.callFake((path, options, callback) => {
				callback(mkdirError);
			});
		});

		describe("load function", () => {
			it("load without error empty", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PackageJsonEmpty);

				const json = await load("/path/package.json", false);
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/package.json");
				expect(json.content).toEqual({
					name: "Test",
					version: "1.0.0",
					dependencies: [],
					devDependencies: [],
					optionalDependencies: [],
					bundledDependencies: [],
					peerDependencies: [],
					files: undefined,
					description: undefined,
					main: undefined,
					types: undefined,
					keywords: undefined,
					author: undefined,
					license: undefined,
					scripts: undefined,
					bin: undefined,
					private: undefined,
					eslintConfig: undefined,
					prettier: undefined,
				});
			});

			it("load without error full", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PackageJsonServant);

				const json = await load("/path/package.json", false);
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/package.json");
				expect(json.content).toEqual({
					name: "@servant/servant",
					version: "0.1.8",
					dependencies: [
						{
							module: "@servant/servant-data",
							version: "file:../servant-data",
							local: true,
							type: DependencyType.Production,
						},
						{
							module: "glob",
							version: "^7.1.3",
							local: false,
							type: DependencyType.Production,
						},
					],
					devDependencies: [
						{
							module: "@types/node",
							version: "^11.12.0",
							local: false,
							type: DependencyType.Develop,
						},
					],
					optionalDependencies: [],
					bundledDependencies: [],
					peerDependencies: [],
					files: ["dist/**/*"],
					description: "Servant builder for node modules.",
					main: "src/index.js",
					types: "src/index.d.ts",
					keywords: ["servant", "build"],
					author: "Stanislav Hacker",
					license: "GPL-3.0-or-later",
					scripts: {},
					bin: undefined,
					private: undefined,
					eslintConfig: undefined,
					prettier: undefined,
				});
			});

			it("load without error full with eslint", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PackageJsonEslintConfig);

				const json = await load("/path/package.json", false);
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/package.json");
				expect(json.content).toEqual({
					name: "@servant/servant",
					version: "0.1.8",
					dependencies: [
						{
							module: "@servant/servant-data",
							version: "file:../servant-data",
							local: true,
							type: DependencyType.Production,
						},
						{
							module: "glob",
							version: "^7.1.3",
							local: false,
							type: DependencyType.Production,
						},
					],
					devDependencies: [
						{
							module: "@types/node",
							version: "^11.12.0",
							local: false,
							type: DependencyType.Develop,
						},
					],
					optionalDependencies: [],
					bundledDependencies: [],
					peerDependencies: [],
					files: ["dist/**/*"],
					description: "Servant builder for node modules.",
					main: "src/index.js",
					types: "src/index.d.ts",
					keywords: ["servant", "build"],
					author: "Stanislav Hacker",
					license: "GPL-3.0-or-later",
					scripts: {},
					bin: undefined,
					private: undefined,
					prettier: undefined,
					eslintConfig: {
						root: true,
					},
				});
			});

			it("load without error full with prettier", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PackageJsonPrettierConfig);

				const json = await load("/path/package.json", false);
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/package.json");
				expect(json.content).toEqual({
					name: "@servant/servant",
					version: "0.1.8",
					dependencies: [
						{
							module: "@servant/servant-data",
							version: "file:../servant-data",
							local: true,
							type: DependencyType.Production,
						},
						{
							module: "glob",
							version: "^7.1.3",
							local: false,
							type: DependencyType.Production,
						},
					],
					devDependencies: [
						{
							module: "@types/node",
							version: "^11.12.0",
							local: false,
							type: DependencyType.Develop,
						},
					],
					optionalDependencies: [],
					bundledDependencies: [],
					peerDependencies: [],
					files: ["dist/**/*"],
					description: "Servant builder for node modules.",
					main: "src/index.js",
					types: "src/index.d.ts",
					keywords: ["servant", "build"],
					author: "Stanislav Hacker",
					license: "GPL-3.0-or-later",
					scripts: {},
					bin: undefined,
					private: undefined,
					eslintConfig: undefined,
					prettier: {
						tabWidth: 4,
					},
				});
			});

			it("load with stat error", async () => {
				statError = new Error("");
				await expectAsync(load("/path/package.json", false)).toBeRejected();
			});

			it("load with read file error", async () => {
				readFileError = new Error("");
				await expectAsync(load("/path/package.json", false)).toBeRejected();
			});
		});

		describe("save function", () => {
			it("save without error", async () => {
				await save("/path/package.json", create("Test", "1.0.0"));
				expect(saved).not.toBeNull();
			});

			it("save with mkdir error", async () => {
				mkdirError = new Error("");
				await expectAsync(
					save("/path/package.json", create("Test", "1.0.0"))
				).toBeRejected();
			});

			it("save with writeFile error", async () => {
				writeFileError = new Error("");
				await expectAsync(
					save("/path/package.json", create("Test", "1.0.0"))
				).toBeRejected();
			});
		});

		describe("update function", () => {
			it("update without error", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PackageJsonEmpty);

				await update("/path/package.json", (json, done) => {
					json.name = "Updated";
					json.version = "0.1.2";
					json.private = true;

					done();
				});

				const savedJson = JSON.parse(saved?.toString() ?? "");
				expect(savedJson.name).toBe("Updated");
				expect(savedJson.version).toBe("0.1.2");
				expect(savedJson.private).toBe(true);
			});

			it("update with read error", async () => {
				readFileError = new Error("");

				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PackageJsonEmpty);
				await expectAsync(
					update("/path/package.json", (json, done) => done())
				).toBeRejected();
			});

			it("update with mkdir error", async () => {
				mkdirError = new Error("");

				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PackageJsonEmpty);
				await expectAsync(
					update("/path/package.json", (json, done) => done())
				).toBeRejected();
			});

			it("update with writeFile error", async () => {
				writeFileError = new Error("");

				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PackageJsonEmpty);
				await expectAsync(
					update("/path/package.json", (json, done) => done())
				).toBeRejected();
			});
		});
	});
});
