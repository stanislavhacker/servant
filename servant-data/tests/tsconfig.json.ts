/* eslint-disable @typescript-eslint/ban-ts-comment */
import { TSCONFIG_JSON, create, save, load, TSConfig } from "../src/tsconfig.json";
import { TsConfigJsonEmpty, TsConfigJsonOne } from "./files/tsconfig.jsons";
import * as fs from "fs";

describe("tsconfig.json", () => {
	it("name", () => {
		expect(TSCONFIG_JSON).toBe("tsconfig.json");
	});

	it("create", () => {
		const tsc = create();
		expect(tsc).toEqual({
			compilerOptions: {
				module: "commonjs",
				target: "es5",
				sourceMap: true,
				declaration: true,
				jsx: "react",
				skipLibCheck: true,
				strictNullChecks: true,
			},
		});
	});

	describe("I/O operations", () => {
		let stats: fs.Stats;
		let data: Buffer;
		let saved: Buffer | string | null = null;
		let writeFileError: Error | null;
		let mkdirError: Error | null;
		let statError: Error | null;
		let readFileError: Error | null;

		beforeEach(() => {
			saved = null;
			stats = new fs.Stats();
			data = Buffer.from("");
			writeFileError = null;
			mkdirError = null;
			statError = null;
			readFileError = null;

			// @ts-ignore
			spyOn(fs, "stat").and.callFake((path, callback) => {
				callback(statError as Error, stats);
			});
			// @ts-ignore
			spyOn(fs, "readFile").and.callFake((path, callback) => {
				callback(readFileError, data);
			});
			// @ts-ignore
			spyOn(fs, "writeFile").and.callFake((path, content, callback) => {
				saved = content;
				callback(writeFileError);
			});
			// @ts-ignore
			spyOn(fs, "mkdir").and.callFake((path, options, callback) => {
				callback(mkdirError);
			});
		});

		describe("load function", () => {
			it("load without error empty", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(TsConfigJsonEmpty);

				const json = await load("/path/tsconfig.json");
				expect(json).toEqual({} as TSConfig);
			});

			it("load without error partial definition", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(TsConfigJsonOne);

				const json = await load("/path/tsconfig.json");
				expect(json).toEqual({
					compilerOptions: {
						module: "commonjs",
						target: "es6",
						sourceMap: true,
						declaration: true,
						jsx: "react",
						skipLibCheck: true,
						strictNullChecks: true,
					},
				} as TSConfig);
			});

			it("load with stat error", async () => {
				statError = new Error("");
				await expectAsync(load("/path/tsconfig.json")).toBeRejected();
			});

			it("load with read file error", async () => {
				readFileError = new Error("");
				await expectAsync(load("/path/tsconfig.json")).toBeRejected();
			});
		});

		describe("save function", () => {
			it("save without error", async () => {
				await save("/path/tsconfig.json", create());
				expect(saved).not.toBeNull();
			});

			it("save with mkdir error", async () => {
				mkdirError = new Error("");
				await expectAsync(save("/path/tsconfig.json", create())).toBeRejected();
			});

			it("save with writeFile error", async () => {
				writeFileError = new Error("");
				await expectAsync(save("/path/tsconfig.json", create())).toBeRejected();
			});
		});
	});
});
