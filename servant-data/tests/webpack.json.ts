import { create, remap } from "../src/webpack.json";

describe("webpack.json", () => {
	it("create", () => {
		const pj = create("/path/to/tests/", "/path/entry");
		expect(pj).toEqual({
			cwd: "/path/to/tests/",
			entry: "/path/entry",
			module: null,
			production: false,
			transpile: false,
		});
	});

	describe("remap", () => {
		it("empty", () => {
			const rm = remap({});

			expect(rm).toEqual({
				cwd: null,
				entry: null,
				module: null,
				production: false,
				transpile: false,
			});
		});

		it("filled", () => {
			const rm = remap({
				entry: "/path/entry",
				cwd: "/path/cwd",
				production: true,
				transpile: true,
			});

			expect(rm).toEqual({
				entry: "/path/entry",
				cwd: "/path/cwd",
				production: true,
				transpile: true,
				module: null,
			});
		});
	});
});
