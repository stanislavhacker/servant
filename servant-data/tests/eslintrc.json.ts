/* eslint-disable @typescript-eslint/ban-ts-comment */
import { ESLINTRC_JSON, createDefault, save, load } from "../src/eslintrc.json";
import { EslintrcJsonEmpty, EslintrcJsonSimple } from "./files/eslintrc.jsons";
import { Path } from "@servant/servant-files";
import * as fs from "fs";

describe(".eslintrc.json", () => {
	it("name", () => {
		expect(ESLINTRC_JSON).toBe(".eslintrc.json");
	});

	it("create root = false;", () => {
		const pj = createDefault(false);
		expect(pj).toEqual({
			root: false,
			parser: "@typescript-eslint/parser",
			plugins: ["@typescript-eslint"],
			extends: [
				"eslint:recommended",
				"plugin:@typescript-eslint/eslint-recommended",
				"plugin:@typescript-eslint/recommended",
			],
		});
	});

	it("create root = true;", () => {
		const pj = createDefault(true);
		expect(pj).toEqual({
			root: true,
			parser: "@typescript-eslint/parser",
			plugins: ["@typescript-eslint"],
			extends: [
				"eslint:recommended",
				"plugin:@typescript-eslint/eslint-recommended",
				"plugin:@typescript-eslint/recommended",
			],
		});
	});

	describe("I/O operations", () => {
		let stats: fs.Stats;
		let data: Buffer;
		let saved: Buffer | string | null = null;
		let statError: Error | null;
		let readFileError: Error | null;
		let writeFileError: Error | null;
		let mkdirError: Error | null;

		beforeEach(() => {
			saved = null;
			stats = new fs.Stats();
			data = Buffer.from("");
			statError = null;
			readFileError = null;
			writeFileError = null;
			mkdirError = null;

			// @ts-ignore
			spyOn(fs, "stat").and.callFake((path, callback) => {
				callback(statError as Error, stats);
			});
			// @ts-ignore
			spyOn(fs, "readFile").and.callFake((path, callback) => {
				callback(readFileError, data);
			});
			// @ts-ignore
			spyOn(fs, "writeFile").and.callFake((path, content, callback) => {
				saved = content;
				callback(writeFileError);
			});
			// @ts-ignore
			spyOn(fs, "mkdir").and.callFake((path, options, callback) => {
				callback(mkdirError);
			});
		});

		describe("load function", () => {
			it("load without error empty", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(EslintrcJsonEmpty);

				const json = await load("/path/.eslintrc.json");
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/.eslintrc.json");
				expect(json.content).toEqual({});
			});

			it("load without error full", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(EslintrcJsonSimple);

				const json = await load("/path/.eslintrc.json");
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/.eslintrc.json");
				expect(json.content).toEqual({
					root: true,
					parser: "@typescript-eslint/parser",
					plugins: ["@typescript-eslint"],
					extends: [
						"eslint:recommended",
						"plugin:@typescript-eslint/eslint-recommended",
						"plugin:@typescript-eslint/recommended",
					],
				});
			});

			it("load with stat error not fail", async () => {
				statError = new Error("");
				await expectAsync(load("/path/.eslintrc.json")).not.toBeRejected();
			});

			it("load with read file error not fail", async () => {
				readFileError = new Error("");
				await expectAsync(load("/path/.eslintrc.json")).not.toBeRejected();
			});
		});

		describe("save function", () => {
			it("save without error", async () => {
				await save("/path/.eslintrc.json", createDefault(true));
				expect(saved).not.toBeNull();
			});

			it("save with mkdir error", async () => {
				mkdirError = new Error("");
				await expectAsync(save("/path/.eslintrc.json", createDefault(true))).toBeRejected();
			});

			it("save with writeFile error", async () => {
				writeFileError = new Error("");
				await expectAsync(save("/path/.eslintrc.json", createDefault(true))).toBeRejected();
			});
		});
	});
});
