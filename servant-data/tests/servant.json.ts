/* eslint-disable @typescript-eslint/ban-ts-comment */
import {
	ModuleTarget,
	ServantJsonInfo,
	ServantJson,
	ServantJsonDefaults,
	SERVANT_JSON,
	create,
	load,
	save,
	entries,
	library,
	getLibraryMapping,
	getSharedPackages,
	registry,
	getTesting,
	getServerEntryCommand,
	isComplexTesting,
	LibraryMapping,
} from "../src/servant.json";
import { DependencyType } from "../src/package.json";
import { createLibraryMapping, ModulesMapping } from "../src/servant.json.modules";
import {
	ServantJsonEmpty,
	ServantJsonServant,
	ServantJsonMultipleRegistry,
	ServantJsonSingleRegistryOne,
	ServantJsonMultipleRegistry2,
	ServantJsonScopesRegistry,
	ServantJsonSingleRegistryTwo,
	ServantJsonNoTesting,
	ServantJsonSimpleTestingOne,
	ServantJsonSimpleTestingTwo,
	ServantJsonNoPrettier,
	ServantJsonSimplerPrettierOne,
	ServantJsonSimplerPrettierTwo,
} from "./files/servant.jsons";
import { Path } from "@servant/servant-files";
import * as fs from "fs";
import * as path from "path";

describe("servant.json", () => {
	it("name", () => {
		expect(SERVANT_JSON).toBe("servant.json");
	});

	it("defaults", () => {
		expect(ServantJsonDefaults).toEqual({
			package: "",
			modules: ["./"],
			output: {
				directory: "./dist",
				filename: "index.js",
				resolve: ["js", "css"],
			},
			webpage: {
				title: "",
				favicon: undefined,
				publicPath: "./",
			},
			entry: "./src/index",
			test: "./tests/index",
			src: ["src/**/*"],
			tests: ["tests/**/*"],
			resolve: ["ts", "tsx", "js", "jsx", "mjs"],
			resources: [],
			watch: [],
			clean: ["js.map"],
			temp: ".temp",
			generators: undefined,
			libraries: undefined,
			mappings: undefined,
			shared: undefined,
			target: ModuleTarget.web,
			publish: {
				access: "public",
				increment: "patch",
				stripDev: false,
				commitMessage:
					"Incrementing version by '$increment' number. New version is '$version'.",
			},
			registry: "",
			testing: undefined,
			issues: {},
			server: {
				entries: {},
				css: [],
				js: [],
			},
			prettify: {
				extensions: [],
				sources: [],
			},
		});
	});

	describe("modules", () => {
		it("create library mapping with defaults", () => {
			const test = createLibraryMapping("root", "Root");
			expect(test).toEqual({
				amd: "Root",
				commonjs: "Root",
				commonjs2: "Root",
				root: "root",
				umd: "Root",
			});
		});

		it("create library mapping full definition", () => {
			const test = createLibraryMapping("root", "Root", "root1", "root2", "amd");
			expect(test).toEqual({
				amd: "amd",
				commonjs: "root1",
				commonjs2: "root2",
				root: "root",
				umd: "Root",
			});
		});

		it("Mappings", () => {
			expect(Object.keys(ModulesMapping).length).toBe(3);
		});
	});

	describe("create", () => {
		it("default servant.json", () => {
			const json = create("Test") as Partial<ServantJson>;

			expect(json).toEqual({
				package: "Test",
			});
		});
	});

	describe("library mapping", () => {
		let servantJsonInfo: ServantJsonInfo;
		let servantJson: ServantJson;

		beforeEach(() => {
			servantJson = create("Test");
			servantJson.libraries = {
				module1: "Module1",
				module2: {
					umd: "Module2Umd",
					root: "Module2Root",
					commonjs2: "Module2Cj2",
					commonjs: "Module2Cj",
					amd: "Module2Amd",
				},
			};
			servantJsonInfo = {
				content: servantJson,
				cwd: "/path",
				path: "/path/servant.json",
			};
		});

		it("external modules, use default or from servant json", () => {
			let lib: LibraryMapping | string;

			lib = getLibraryMapping(servantJsonInfo, {
				module: "react",
				local: false,
				version: "14.5.8",
				type: DependencyType.Production,
			});
			expect(lib).toEqual({
				amd: "react",
				commonjs: "react",
				commonjs2: "react",
				root: "React",
				umd: "react",
			});

			lib = getLibraryMapping(servantJsonInfo, {
				module: "module1",
				local: false,
				version: "2.0.5",
				type: DependencyType.Production,
			});
			expect(lib).toEqual("Module1");

			lib = getLibraryMapping(servantJsonInfo, {
				module: "module2",
				local: false,
				version: "2.0.4",
				type: DependencyType.Production,
			});
			expect(lib).toEqual({
				umd: "Module2Umd",
				root: "Module2Root",
				commonjs2: "Module2Cj2",
				commonjs: "Module2Cj",
				amd: "Module2Amd",
			});

			lib = getLibraryMapping(servantJsonInfo, {
				module: "module0",
				local: false,
				version: "1.10.22",
				type: DependencyType.Production,
			});
			expect(lib).toEqual("module0");
		});

		it("internal modules", () => {
			const lib = getLibraryMapping(servantJsonInfo, {
				module: "servant",
				local: true,
				version: "14.5.8",
				type: DependencyType.Production,
			});
			expect(lib).toEqual({
				amd: "servant",
				commonjs: "servant",
				commonjs2: "servant",
				root: "servant",
				umd: "servant",
			});
		});
	});

	describe("shared", () => {
		let servantJsonInfo: ServantJsonInfo;
		let servantJson: ServantJson;

		beforeEach(() => {
			servantJson = create("Test");
			servantJson.shared = {
				react: "../node_modules/react",
			};
			servantJsonInfo = {
				content: servantJson,
				cwd: "/path",
				path: "/path/servant.json",
			};
		});

		it("external modules, use default or from servant json", () => {
			const { relative, absolute } = getSharedPackages(servantJsonInfo);
			expect(relative).toEqual({
				react: "../node_modules/react",
			});
			expect(absolute).toEqual({
				react: path.normalize("/node_modules/react"),
			});
		});
	});

	describe("library", () => {
		let servantJsonInfo: ServantJsonInfo;
		let servantJson: ServantJson;

		beforeEach(() => {
			servantJson = create("Test");
			servantJson.mappings = {
				"my-module": "./my-module.min.js",
				"my-module-2": "./dist/web/my-module-2.min.js",
			};
			servantJsonInfo = {
				content: servantJson,
				cwd: "/path",
				path: "/path/servant.json",
			};
		});

		it("resolved default libraries", () => {
			expect(library(servantJsonInfo, "react")).toBe("./umd/react.production.min.js");
			expect(library(servantJsonInfo, "react-dom")).toBe("./umd/react-dom.production.min.js");
		});

		it("resolved defined libraries", () => {
			expect(library(servantJsonInfo, "my-module")).toBe("./my-module.min.js");
			expect(library(servantJsonInfo, "my-module-2")).toBe("./dist/web/my-module-2.min.js");
		});

		it("unresolved libraries", () => {
			expect(library(servantJsonInfo, "my-module-3")).toBe(null);
		});

		it("resolved @types libraries", () => {
			expect(library(servantJsonInfo, "@types/jasmine")).toBe("");
		});
	});

	describe("registry", () => {
		it("simple string", () => {
			expect(registry("http://registry.cz")).toEqual({
				registry: "http://registry.cz",
				scopes: {},
			});
		});

		it("simple object", () => {
			expect(
				registry({
					default: "http://registry.cz",
					"@scope1": "http://localhost/scope1",
					"@scope2": "http://localhost/scope2",
				})
			).toEqual({
				registry: "http://registry.cz",
				scopes: {
					"@scope1": "http://localhost/scope1",
					"@scope2": "http://localhost/scope2",
				},
			});
		});

		it("simple object empty registry", () => {
			expect(
				registry({
					"": "http://registry.cz",
					"@scope1": "http://localhost/scope1",
					"@scope2": "http://localhost/scope2",
				})
			).toEqual({
				registry: "http://registry.cz",
				scopes: {
					"@scope1": "http://localhost/scope1",
					"@scope2": "http://localhost/scope2",
				},
			});
		});

		it("simple object registry registry", () => {
			expect(
				registry({
					registry: "http://registry.cz",
					"@scope1": "http://localhost/scope1",
					"@scope2": "http://localhost/scope2",
				})
			).toEqual({
				registry: "http://registry.cz",
				scopes: {
					"@scope1": "http://localhost/scope1",
					"@scope2": "http://localhost/scope2",
				},
			});
		});

		it("simple object noscope registry", () => {
			expect(
				registry({
					noscope: "http://registry.cz",
					"@scope1": "http://localhost/scope1",
					"@scope2": "http://localhost/scope2",
				})
			).toEqual({
				registry: "http://registry.cz",
				scopes: {
					"@scope1": "http://localhost/scope1",
					"@scope2": "http://localhost/scope2",
				},
			});
		});
	});

	describe("testing", () => {
		let servantJsonInfo: ServantJsonInfo;
		let servantJson: ServantJson;

		beforeEach(() => {
			servantJson = create("Test");
			servantJsonInfo = {
				content: servantJson,
				cwd: "/path",
				path: "/path/servant.json",
			};
		});

		it("isComplexTesting", () => {
			expect(isComplexTesting({})).toBeFalse();
			expect(isComplexTesting(null)).toBeFalse();
			expect(isComplexTesting(undefined)).toBeFalse();
			expect(isComplexTesting({ browsers: ["Webkit"] })).toBeFalse();
			expect(isComplexTesting({ browsers: ["Webkit"], devices: [] })).toBeFalse();
			expect(isComplexTesting({ browsers: ["Webkit", "Firefox"] })).toBeTrue();
			expect(isComplexTesting({ browsers: ["Webkit"], devices: ["aaa"] })).toBeTrue();
		});

		it("getTesting empty", () => {
			expect(getTesting(servantJsonInfo)).toEqual({
				devices: [],
				browsers: ["Webkit"],
			});
		});

		it("getTesting filled", () => {
			servantJson.testing = {
				devices: ["AAA"],
				browsers: ["Chromium", "Webkit"],
			};
			expect(getTesting(servantJsonInfo)).toEqual({
				devices: ["AAA"],
				browsers: ["Chromium", "Webkit"],
			});
		});
	});

	describe("I/O operations", () => {
		let stats: fs.Stats;
		let data: Buffer;
		let saved: Buffer | string | null = null;
		let statError: Error | null;
		let readFileError: Error | null;
		let writeFileError: Error | null;
		let mkdirError: Error | null;

		beforeEach(() => {
			saved = null;
			stats = new fs.Stats();
			data = Buffer.from("");
			statError = null;
			readFileError = null;
			writeFileError = null;
			mkdirError = null;

			// @ts-ignore
			spyOn(fs, "stat").and.callFake((path, callback) => {
				callback(statError as Error, stats);
			});
			// @ts-ignore
			spyOn(fs, "readFile").and.callFake((path, callback) => {
				callback(readFileError, data);
			});
			// @ts-ignore
			spyOn(fs, "writeFile").and.callFake((path, content, callback) => {
				saved = content;
				callback(writeFileError);
			});
			// @ts-ignore
			spyOn(fs, "mkdir").and.callFake((path, options, callback) => {
				callback(mkdirError);
			});
		});

		describe("load function", () => {
			it("load without error empty", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(ServantJsonEmpty);

				const json = await load("/path/servant.json");
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/servant.json");
				expect(json.content).toEqual({
					...ServantJsonDefaults,
					output: {
						...ServantJsonDefaults.output,
						filename: "empty.js",
					},
					webpage: {
						...ServantJsonDefaults.webpage,
						title: "empty",
					},
					package: "Empty",
				} as ServantJson);
			});

			it("load without error partial definition", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(ServantJsonServant);

				const json = await load("/path/servant.json");
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/servant.json");
				expect(json.content).toEqual({
					package: "Servant",
					output: {
						directory: "./dist",
						filename: "servant.js",
						resolve: ["ts", "tsx"],
					},
					entry: "./src/index",
					webpage: {
						title: "servant",
						favicon: undefined,
						publicPath: "./",
					},
					libraries: {
						"simple-git/promise": "simple-git/promise",
						"source-map-support/register": "source-map-support/register",
					},
					target: ModuleTarget.node,
					resources: [
						"../logo.png",
						"npm:@servant/servant-data//dist/servantJson.schema.json",
					],
					publish: {
						increment: "patch",
						access: "public",
						stripDev: false,
						commitMessage:
							"Incrementing version by '$increment' number. New version is '$version'.",
					},
					server: {
						entries: {},
						css: [],
						js: [],
					},
					issues: {},
					test: "./tests/index",
					registry: "",
					mappings: undefined,
					testing: undefined,
					shared: undefined,
					generators: undefined,
					modules: ["./"],
					src: ["src/**/*"],
					tests: ["tests/**/*"],
					clean: ["js.map"],
					temp: ".temp",
					watch: [],
					resolve: ["ts", "tsx", "js", "jsx", "mjs"],
					prettify: {
						extensions: [],
						sources: [],
					},
				} as ServantJson);
			});

			it("load with stat error", async () => {
				statError = new Error("");
				await expectAsync(load("/path/servant.json")).toBeRejected();
			});

			it("load with read file error", async () => {
				readFileError = new Error("");
				await expectAsync(load("/path/servant.json")).toBeRejected();
			});

			describe("test multiple registries and merge", () => {
				it("load without error with single registries", async () => {
					spyOn(stats, "isFile").and.returnValue(true);
					data = Buffer.from(ServantJsonMultipleRegistry);

					const json = await load("/path/servant.json");
					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.registry).toEqual({
						registry: "http://reg.cz/test2",
						"@scope1": "http://gitlab.com/scope1",
						"@scope2": "http://github.com/scope2",
					});
				});

				it("load without error with multiple registries", async () => {
					spyOn(stats, "isFile").and.returnValue(true);
					data = Buffer.from(ServantJsonSingleRegistryOne);

					const json = await load("/path/servant.json");
					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.registry).toEqual("http://reg.cz/test0");
				});

				it("merge one registry into more registries", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonSingleRegistryOne);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonMultipleRegistry);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.registry).toEqual({
						registry: "http://reg.cz/test2",
						"@scope1": "http://gitlab.com/scope1",
						"@scope2": "http://github.com/scope2",
					});
				});

				it("merge more registry into one registries", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonMultipleRegistry);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonSingleRegistryOne);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.registry).toEqual({
						registry: "http://reg.cz/test0",
						"@scope1": "http://gitlab.com/scope1",
						"@scope2": "http://github.com/scope2",
					});
				});

				it("merge more registry into more registry one", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonMultipleRegistry);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonMultipleRegistry2);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.registry).toEqual({
						registry: "http://reg.cz/test2",
						"@scope1": "http://gitlab.com/scope1",
						"@scope3": "http://github.com/scope3",
						"@scope2": "http://github.com/scope2",
					});
				});

				it("merge more registry into more registry two", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonMultipleRegistry2);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonMultipleRegistry);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.registry).toEqual({
						registry: "http://reg.cz/test3",
						"@scope1": "http://gitlab.com/scopeA",
						"@scope2": "http://github.com/scope2",
						"@scope3": "http://github.com/scope3",
					});
				});

				it("merge more scoped registry into single registry", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonScopesRegistry);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonSingleRegistryOne);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.registry).toEqual({
						"@scope1": "http://gitlab.com/scopeA",
						"@scope3": "http://github.com/scope3",
						default: "http://reg.cz/test0",
					});
				});

				it("merge single registry into more scoped registry", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonSingleRegistryOne);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonScopesRegistry);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.registry).toEqual({
						"@scope1": "http://gitlab.com/scopeA",
						"@scope3": "http://github.com/scope3",
						default: "http://reg.cz/test0",
					});
				});

				it("merge single object registry into more scoped registry", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonSingleRegistryTwo);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonScopesRegistry);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.registry).toEqual({
						"@scope1": "http://gitlab.com/scopeA",
						"@scope3": "http://github.com/scope3",
						registry: "http://reg.cz/test1",
					});
				});
			});

			describe("test testing and merge", () => {
				it("load without error with no testing", async () => {
					spyOn(stats, "isFile").and.returnValue(true);
					data = Buffer.from(ServantJsonNoTesting);

					const json = await load("/path/servant.json");
					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.testing).toEqual(undefined);
				});

				it("load without error with single testing", async () => {
					spyOn(stats, "isFile").and.returnValue(true);
					data = Buffer.from(ServantJsonSimpleTestingOne);

					const json = await load("/path/servant.json");
					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.testing).toEqual({
						browsers: ["Webkit", "Firefox"],
						devices: ["iPhone", "Nokia Lumia"],
					});
				});

				it("merge no testing into simple one testing", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonNoTesting);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonSimpleTestingOne);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.testing).toEqual({
						browsers: ["Webkit", "Firefox"],
						devices: ["iPhone", "Nokia Lumia"],
					});
				});

				it("merge two testings", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonSimpleTestingOne);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonSimpleTestingTwo);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.testing).toEqual({
						browsers: ["Chromium", "Webkit", "Firefox"],
						devices: ["Pixel 3", "iPhone", "Nokia Lumia"],
					});
				});

				it("load without error with no prettify", async () => {
					spyOn(stats, "isFile").and.returnValue(true);
					data = Buffer.from(ServantJsonNoPrettier);

					const json = await load("/path/servant.json");
					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.prettify).toEqual({
						extensions: [],
						sources: [],
					});
				});

				it("merge no prettify into simple one prettify", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonNoPrettier);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonSimplerPrettierOne);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.prettify).toEqual({
						sources: ["doc/**/*"],
						extensions: ["md"],
					});
				});

				it("merge two prettify", async () => {
					spyOn(stats, "isFile").and.returnValue(true);

					data = Buffer.from(ServantJsonSimplerPrettierOne);
					const parentJson = await load("/servant.json");

					data = Buffer.from(ServantJsonSimplerPrettierTwo);
					const json = await load("/path/servant.json", undefined, parentJson);

					expect(Path.normalize(json.cwd)).toBe("/path");
					expect(Path.normalize(json.path)).toBe("/path/servant.json");
					expect(json.content.prettify).toEqual({
						sources: ["examples/**/*", "doc/**/*"],
						extensions: ["json", "yaml", "md"],
					});
				});
			});
		});

		describe("save function", () => {
			it("save without error", async () => {
				await save("/path/servant.json", create("Test"));
				expect(saved).not.toBeNull();
			});

			it("save with mkdir error", async () => {
				mkdirError = new Error("");
				await expectAsync(save("/path/servant.json", create("Test"))).toBeRejected();
			});

			it("save with writeFile error", async () => {
				writeFileError = new Error("");
				await expectAsync(save("/path/servant.json", create("Test"))).toBeRejected();
			});
		});
	});

	describe("entries", () => {
		let file = "";

		function createServantJson(): ServantJsonInfo {
			const json = create("Test") as ServantJson;
			return {
				content: json,
				cwd: "/path",
				path: "/path/servant.json",
			};
		}

		beforeEach(() => {
			// @ts-ignore
			spyOn(fs, "existsSync").and.callFake((path: string) => {
				return path.indexOf(file) >= 0;
			});
		});

		it("invalid", () => {
			file = "";
			expect(entries(undefined)).toEqual({ sass: null, css: null, less: null });
		});

		it("css entry point", () => {
			file = "project.css";

			const json = createServantJson();
			json.content.entry = "/entry/path/to/project";
			json.content.output = {
				filename: "index.css",
				directory: "out",
				resolve: [],
			};

			expect(entries(json)).toEqual({
				sass: null,
				css: { from: "/path/entry/path/to/project.css", to: "/path/out/index.css" },
				less: null,
			});
		});

		it("scss entry point", () => {
			file = "project.scss";

			const json = createServantJson();
			json.content.entry = "/entry/path/to/project";
			json.content.output = {
				filename: "index.scss",
				directory: "out",
				resolve: [],
			};

			expect(entries(json)).toEqual({
				sass: { from: "/path/entry/path/to/project.scss", to: "/path/out/index.scss" },
				css: null,
				less: null,
			});
		});

		it("less entry point", () => {
			file = "project.less";

			const json = createServantJson();
			json.content.entry = "/entry/path/to/project";
			json.content.output = {
				filename: "index.less",
				directory: "out",
				resolve: [],
			};

			expect(entries(json)).toEqual({
				sass: null,
				css: null,
				less: { from: "/path/entry/path/to/project.less", to: "/path/out/index.less" },
			});
		});
	});

	describe("server entry command", () => {
		function createServantJson(): ServantJsonInfo {
			const json = create("Test") as ServantJson;
			return {
				content: json,
				cwd: "/path/to/data",
				path: "/path/to/data/servant.json",
			};
		}

		it("all empty", () => {
			const servantJson = createServantJson();
			const data = getServerEntryCommand("/path/to/", servantJson, {});
			expect(data).toEqual({
				file: path.normalize("/path/to/data/dist/index.js"),
				args: [],
			});
		});

		it("filled file entry", () => {
			const servantJson = createServantJson();
			const data = getServerEntryCommand("/path/to/", servantJson, {
				file: "./index.js",
			});
			expect(data).toEqual({
				file: path.normalize("/path/to/index.js"),
				args: [],
			});
		});

		it("filled arguments", () => {
			const servantJson = createServantJson();
			const data = getServerEntryCommand("/path/to/", servantJson, {
				args: ["--port", "9000"],
			});
			expect(data).toEqual({
				file: path.normalize("/path/to/data/dist/index.js"),
				args: ["--port", "9000"],
			});
		});
	});
});
