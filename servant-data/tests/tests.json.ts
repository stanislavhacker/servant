import { create, remap } from "../src/tests.json";

describe("tests.json", () => {
	it("create", () => {
		const pj = create("/path/to/tests/", "/path/entry", ["**/*.js"]);
		expect(pj).toEqual({
			cwd: "/path/to/tests/",
			entry: "/path/entry",
			files: ["**/*.js"],
			browsers: [],
			devices: [],
			extensions: [],
			externals: [],
			module: null,
			gui: false,
		});
	});

	describe("remap", () => {
		it("empty", () => {
			const rm = remap({});

			expect(rm).toEqual({
				cwd: null,
				entry: null,
				files: [],
				browsers: [],
				devices: [],
				extensions: [],
				externals: [],
				module: null,
				gui: false,
			});
		});

		it("filled", () => {
			const rm = remap({
				entry: "/path/entry",
				cwd: "/path/cwd",
				gui: true,
				browsers: ["Webkit"],
				devices: ["Nokia"],
				externals: ["a.js"],
				files: ["**/*.js"],
				extensions: ["js"],
			});

			expect(rm).toEqual({
				entry: "/path/entry",
				cwd: "/path/cwd",
				gui: true,
				browsers: ["Webkit"],
				devices: ["Nokia"],
				externals: ["a.js"],
				files: ["**/*.js"],
				extensions: ["js"],
				module: null,
			});
		});
	});
});
