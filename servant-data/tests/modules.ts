/* eslint-disable @typescript-eslint/ban-ts-comment */
import { info, create, directories } from "../src/modules";
import { PackageJson, ServantJson, EslintrcJson, PrettierrcJson, TrancorderJson } from "../src/";

describe("modules", () => {
	const cwd = "/path/to/project";
	const packageJson = {
		cwd,
		path: `${cwd}/${PackageJson.PACKAGE_JSON}`,
		content: PackageJson.map(PackageJson.create("test", "1.0.0")),
		main: true,
	};
	const servantJson = {
		cwd,
		path: `${cwd}/${ServantJson.SERVANT_JSON}`,
		content: ServantJson.create("test"),
	};
	const eslintrcJson = {
		cwd,
		path: `${cwd}/${EslintrcJson.ESLINTRC_JSON}`,
		content: EslintrcJson.createDefault(false),
	};
	const prettierrcJson = {
		cwd,
		path: `${cwd}/${PrettierrcJson.PRETTIERRC_JSON}`,
		content: PrettierrcJson.createDefault(),
	};
	const trancorderJson = {
		cwd,
		path: `${cwd}/${TrancorderJson.TRANCORDER_JSON}`,
		content: TrancorderJson.createDefault(),
	};

	it("generate info", () => {
		const data = info(
			packageJson,
			servantJson,
			eslintrcJson,
			prettierrcJson,
			trancorderJson,
			[],
			directories(),
			false
		);
		expect(data).toEqual({
			packageJson,
			servantJson,
			eslintrcJson,
			prettierrcJson,
			trancorderJson,
			directories: directories(),
			dependencies: [],
			internal: false,
		});
	});

	it("generate module", () => {
		const data = create(
			"Test",
			info(
				packageJson,
				servantJson,
				eslintrcJson,
				prettierrcJson,
				trancorderJson,
				[],
				directories(),
				false
			),
			false
		);
		expect(data).toEqual({
			name: "Test",
			module: jasmine.anything(),
			versions: [],
			depType: [],
			internals: [],
			externals: [],
			missing: false,
		});
	});

	describe("directories", () => {
		it("should return empty directories", () => {
			expect(directories()).toEqual({
				sources: [],
				tests: [],
				others: [],
			});
		});

		it("all dirs as others with empty servant.json", () => {
			const servantJson = {
				cwd: "/path/to/project",
				path: `/path/to/project/${ServantJson.SERVANT_JSON}`,
				content: ServantJson.create("test"),
			};
			const dirs = [
				"/path/to/project/src",
				"/path/to/project/src/component",
				"/path/to/project/tests",
				"/path/to/project/tests/component",
				"/path/to/project/others",
				"/path/to/project/docs",
			];

			expect(directories(servantJson, dirs)).toEqual({
				sources: [],
				tests: [],
				others: ["src", "src/component", "tests", "tests/component", "others", "docs"],
			});
		});

		it("all dirs as others with src and tests servant.json", () => {
			const servantJson = {
				cwd: "/path/to/project",
				path: `/path/to/project/${ServantJson.SERVANT_JSON}`,
				content: ServantJson.create("test"),
			};
			servantJson.content.src = ["src/**/*"];
			servantJson.content.tests = ["tests/**/*"];

			const dirs = [
				"/path/to/project/src",
				"/path/to/project/src/component",
				"/path/to/project/tests",
				"/path/to/project/tests/component",
				"/path/to/project/others",
				"/path/to/project/docs",
			];

			expect(directories(servantJson, dirs)).toEqual({
				sources: ["src", "src/component"],
				tests: ["tests", "tests/component"],
				others: ["others", "docs"],
			});
		});
	});
});
