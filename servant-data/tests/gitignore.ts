/* eslint-disable @typescript-eslint/ban-ts-comment */
import {
	init,
	GITIGNORE_FILE,
	doMessage,
	doHeader,
	doPattern,
	load,
	save,
	serialize,
	parse,
} from "../src/gitignore";
import { SimpleFile } from "./files/gitignore.files";
import { Path } from "@servant/servant-files";
import * as ServantJson from "../src/servant.json";
import * as fs from "fs";

describe(".gitignore", () => {
	it("name", () => {
		expect(GITIGNORE_FILE).toBe(".gitignore");
	});

	it("create for TS from empty", () => {
		const servantJson = ServantJson.create("test");
		const ignore = init(servantJson, "ts");

		expect(ignore.length).toBe(62);

		const data = serialize(ignore);

		expect(data).toContain("**/src/**/*.d.ts");
		expect(data).toContain("**/src/**/*.js");
		expect(data).toContain("**/src/**/*.js.map");
		expect(data).toContain("**/tests/**/*.d.ts");
		expect(data).toContain("**/tests/**/*.js");
		expect(data).toContain("**/tests/**/*.js.map");

		expect(data).toContain("**/dist/**/*");
		expect(data).toContain(".temp");
	});

	it("create for TS", () => {
		const servantJson = ServantJson.create("test");
		servantJson.src = ["src/**/*.ts"];
		servantJson.tests = ["tests/**/*.ts"];
		servantJson.temp = ".temp";
		servantJson.output = { directory: "out/", filename: "index.js", resolve: ["js"] };

		const ignore = init(servantJson, "ts");

		expect(ignore.length).toBe(62);

		const data = serialize(ignore);

		expect(data).toContain("**/src/**/*.d.ts");
		expect(data).toContain("**/src/**/*.js");
		expect(data).toContain("**/src/**/*.js.map");
		expect(data).toContain("**/tests/**/*.d.ts");
		expect(data).toContain("**/tests/**/*.js");
		expect(data).toContain("**/tests/**/*.js.map");

		expect(data).toContain("**/out/**/*");
		expect(data).toContain(".temp");
	});

	it("create for JS", () => {
		const servantJson = ServantJson.create("test");
		servantJson.src = ["src1/**/*.js"];
		servantJson.tests = ["tests1/**/*.js"];
		servantJson.temp = ".temp";
		servantJson.output = { directory: "out1/", filename: "index.js", resolve: ["js"] };

		const ignore = init(servantJson, "js");

		expect(ignore.length).toBe(58);

		const data = serialize(ignore);

		expect(data).toContain("**/src1/**/*.js.map");
		expect(data).toContain("**/tests1/**/*.js.map");

		expect(data).toContain("**/out1/**/*");
		expect(data).toContain(".temp");
	});

	describe("create data", () => {
		it("doMessage", () => {
			const item = doMessage("Message");

			expect(item).toEqual({
				name: "message",
				content: "Message",
			});
		});

		it("doHeader", () => {
			const item = doHeader("Header");

			expect(item).toEqual({
				name: "header",
				content: "Header",
			});
		});

		it("doPattern", () => {
			const item = doPattern("Pattern");

			expect(item).toEqual({
				name: "pattern",
				content: "Pattern",
			});
		});
	});

	describe("parse", () => {
		it("simple", () => {
			const file = parse("### Header\r\n# Message\r\nsrc/**/*\r\n## Test");

			expect(file.length).toBe(4);
			expect(file[0]).toEqual({
				name: "header",
				content: "Header",
			});
			expect(file[1]).toEqual({
				name: "message",
				content: "Message",
			});
			expect(file[2]).toEqual({
				name: "pattern",
				content: "src/**/*",
			});
			expect(file[3]).toEqual({
				name: "message",
				content: "# Test",
			});
		});
	});

	describe("I/O operations", () => {
		let stats: fs.Stats;
		let data: Buffer;
		let saved: Buffer | string | null = null;
		let statError: Error | null;
		let readFileError: Error | null;
		let writeFileError: Error | null;
		let mkdirError: Error | null;

		beforeEach(() => {
			saved = null;
			stats = new fs.Stats();
			data = Buffer.from("");
			statError = null;
			readFileError = null;
			writeFileError = null;
			mkdirError = null;

			// @ts-ignore
			spyOn(fs, "stat").and.callFake((path, callback) => {
				callback(statError as Error, stats);
			});
			// @ts-ignore
			spyOn(fs, "readFile").and.callFake((path, callback) => {
				callback(readFileError, data);
			});
			// @ts-ignore
			spyOn(fs, "writeFile").and.callFake((path, content, callback) => {
				saved = content;
				callback(writeFileError);
			});
			// @ts-ignore
			spyOn(fs, "mkdir").and.callFake((path, options, callback) => {
				callback(mkdirError);
			});
		});

		describe("load function", () => {
			it("load without error empty", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(SimpleFile);

				const gitignore = await load("/path/.gitignore");
				expect(Path.normalize(gitignore.cwd)).toBe("/path");
				expect(Path.normalize(gitignore.path)).toBe("/path/.gitignore");
				expect(gitignore.content).toEqual([
					{ name: "header", content: "HEADING" },
					{ name: "message", content: "Message" },
					{ name: "pattern", content: ".glob" },
				]);
			});

			it("load with stat error", async () => {
				statError = new Error("");
				await expectAsync(load("/path/.gitignore")).toBeRejected();
			});

			it("load with read file error", async () => {
				readFileError = new Error("");
				await expectAsync(load("/path/.gitignore")).toBeRejected();
			});
		});

		describe("save function", () => {
			let servantJson: ServantJson.ServantJson;

			beforeEach(() => {
				servantJson = ServantJson.create("test");
				servantJson.src = ["src/**/*.ts"];
				servantJson.tests = ["tests/**/*.ts"];
				servantJson.temp = ".temp";
				servantJson.output = { directory: "out/", filename: "index.js", resolve: ["js"] };
			});

			it("save without error", async () => {
				await save("/path/.gitignore", init(servantJson, "js"));
				expect(saved).not.toBeNull();
			});

			it("save with mkdir error", async () => {
				mkdirError = new Error("");
				await expectAsync(save("/path/.gitignore", init(servantJson, "js"))).toBeRejected();
			});

			it("save with writeFile error", async () => {
				writeFileError = new Error("");
				await expectAsync(save("/path/.gitignore", init(servantJson, "js"))).toBeRejected();
			});
		});
	});
});
