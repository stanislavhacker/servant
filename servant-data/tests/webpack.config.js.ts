/* eslint-disable @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-explicit-any */
import * as internal from "module";
import { CSS } from "@servant/servant-files";

import * as MiniCssExtractPlugin from "mini-css-extract-plugin";
import { create, tests } from "../src/webpack.config.js";
import { create as moduleDefinition, info, ModuleInfo, directories } from "../src/modules";
import { createPublicTemplate } from "../src/webpack.config.template";
import * as PackageJson from "../src/package.json";
import * as ServantJson from "../src/servant.json";
import * as fs from "fs";
import * as path from "path";
import * as webpack from "webpack";

describe("webpack.config.js", () => {
	let module: ModuleInfo | null;

	const dir = "/path/project";
	const servantCwd = "/path/project/node_modules/@servant/servant";

	function checkCssPlugin(conf: webpack.Configuration) {
		expect((conf.plugins![0] as webpack.WebpackPluginInstance).options).toEqual({
			filename: "index.css",
			chunkFilename: "[id].index.css",
			experimentalUseImportModule: undefined,
			ignoreOrder: false,
			runtime: true,
		});
	}

	function checkCopyCssPlugin(conf: webpack.Configuration) {
		expect((conf.plugins![1] as webpack.WebpackPluginInstance).patterns).toEqual([
			{
				to: CSS.toCSS(path.join(dir, "./dist/index")),
				from: CSS.toCSS(path.join(dir, "./src/index")),
			},
		]);
	}

	function checkBannerPlugin(conf: webpack.Configuration) {
		expect((conf.plugins![2] as webpack.WebpackPluginInstance).options).toEqual({
			banner: "#!/usr/bin/env node",
			raw: true,
		});
	}

	function checkHtmlBundlePlugin(conf: webpack.Configuration, props: Partial<unknown> = {}) {
		expect((conf.plugins![2] as webpack.WebpackPluginInstance).userOptions).toEqual({
			favicon: undefined,
			publicPath: "./",
			title: "",
			filename: "index.html",
			meta: {},
			hash: true,
			minify: "auto",
			templateContent: jasmine.any(Function),
			...props,
		});
	}

	function checkTsModule(conf: webpack.Configuration, production: boolean, transpile: boolean) {
		expect(conf.module!.rules![0]).toEqual({
			test: /\.(ts|tsx)$/,
			exclude: [/node_modules/, (conf.module!.rules![0] as webpack.RuleSetRule).exclude![1]],
			use: [
				{
					loader: "ts-loader",
					options: {
						context: "/path/project",
						transpileOnly: transpile,
						happyPackMode: false,
						compilerOptions: {
							declaration: false,
							outDir: path.join(dir, "./dist"),
						},
						configFile: path.join(dir, "./tsconfig.json"),
					},
				},
				{
					loader: "ifdef-loader",
					options: {
						DEBUG: !production,
						version: "1.0.0",
						"ifdef-verbose": false,
					},
				},
			],
		} as webpack.RuleSetRule);
	}

	function checkCssModule(conf: webpack.Configuration) {
		expect(conf.module!.rules![1]).toEqual({
			test: /\.(css)$/,
			exclude: [/node_modules/],
			use: [
				MiniCssExtractPlugin.loader,
				{
					loader: "css-loader",
					options: {
						modules: "global",
					},
				},
			],
		} as webpack.RuleSetRule);
	}

	function checkAssetModule(conf: webpack.Configuration) {
		expect(conf.module!.rules![2]).toEqual({
			test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2|ico|webp)$/,
			type: "asset",
			exclude: [/node_modules/],
		} as webpack.RuleSetRule);
	}

	function checkNode(conf: webpack.Configuration, target: ServantJson.ModuleTarget) {
		const specials = {};

		switch (target) {
			case ServantJson.ModuleTarget.node:
			case ServantJson.ModuleTarget.node_cli:
				specials["global"] = false;
				break;
			default:
				break;
		}

		expect(conf.node).toEqual({
			...specials,
			__dirname: false,
			__filename: false,
		});
	}

	function checkResolve(conf: webpack.Configuration) {
		expect(conf.resolve).toEqual({
			alias: {},
			extensions: [".ts", ".js", ".tsx", ".jsx", ".mjs"],
		});
		expect(conf.resolveLoader).toEqual({
			modules: [
				path.normalize("/path/project/node_modules/"),
				path.normalize("/path/project/node_modules/@servant/servant/node_modules"),
				path.join(__dirname, "../", "node_modules"),
				"node_modules",
			],
		});
	}

	function checkDistOutput(conf: webpack.Configuration) {
		expect(conf.output).toEqual({
			umdNamedDefine: true,
			library: "Test",
			libraryTarget: "umd",
			filename: "index.js",
			path: path.join(dir, "./dist"),
			globalObject: "this",
		});
	}

	function checkTestsOutput(conf: webpack.Configuration) {
		expect(conf.output).toEqual({
			umdNamedDefine: true,
			library: "Test",
			libraryTarget: "umd",
			filename: "index.js",
			path: path.join(dir, "./.temp"),
			globalObject: "this",
		});
	}

	function checkExternals(
		conf: webpack.Configuration,
		target: ServantJson.ModuleTarget,
		add: Record<string, string | Record<string, string>> = {}
	) {
		const ext = {};

		switch (target) {
			case ServantJson.ModuleTarget.node:
			case ServantJson.ModuleTarget.node_cli: {
				const internals = internal.builtinModules;
				internals.forEach((int) => {
					ext[int] = int;
				});
				break;
			}
			default:
				break;
		}

		expect(conf.externals).toEqual({ ...ext, ...add });
	}

	beforeEach(() => {
		spyOn(fs, "existsSync").and.returnValue(true);
		module = moduleDefinition(
			"Tesřt",
			info(
				{
					path: dir + "/package.json",
					cwd: dir,
					content: PackageJson.map(PackageJson.create("Test", "1.0.0")),
					main: false,
				},
				{
					path: dir + "/servant.json",
					cwd: dir,
					content: { ...ServantJson.ServantJsonDefaults, ...ServantJson.create("Test") },
				},
				{
					path: dir + "/.eslintrc.json",
					cwd: dir,
					content: null,
				},
				{
					path: dir + "/.prettierrc.json",
					cwd: dir,
					content: null,
				},
				{
					path: dir + "/.trancorder.json",
					cwd: dir,
					content: null,
				},
				[],
				directories(),
				true
			),
			false
		).module;
	});

	describe("create", () => {
		it("default", () => {
			const production = false;
			const transpile = false;
			const target = ServantJson.ModuleTarget.web;
			const conf = create(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./src/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(2);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkDistOutput(conf);
		});

		it("default, flag: production", () => {
			const production = true;
			const transpile = false;
			const target = ServantJson.ModuleTarget.web;
			const conf = create(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./src/index");
			expect(conf.mode).toBe("production");

			expect(conf.plugins!.length).toEqual(2);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkDistOutput(conf);
		});

		it("default, flag: transpile", () => {
			const production = false;
			const transpile = true;
			const target = ServantJson.ModuleTarget.web;
			const conf = create(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./src/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(2);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkDistOutput(conf);
		});

		it("default, target: node", () => {
			const production = false;
			const transpile = false;
			const target = ServantJson.ModuleTarget.node;
			module!.servantJson.content.target = target;
			const conf = create(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./src/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(2);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkDistOutput(conf);
		});

		it("default, target: node-cli", () => {
			const production = false;
			const transpile = false;
			const target = ServantJson.ModuleTarget.node_cli;
			module!.servantJson.content.target = target;
			const conf = create(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./src/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(3);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);
			checkBannerPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkDistOutput(conf);
		});

		it("default, target: node, dependencies", () => {
			const production = false;
			const transpile = false;
			const target = ServantJson.ModuleTarget.node;
			module!.servantJson.content.target = target;
			module!.packageJson.content.dependencies = [
				{
					local: false,
					module: "react",
					version: "16.4.2",
					type: PackageJson.DependencyType.Develop,
				},
				{
					local: false,
					module: "aaa",
					version: "0.9.2",
					type: PackageJson.DependencyType.Develop,
				},
				{
					local: true,
					module: "test",
					version: "file:../test",
					type: PackageJson.DependencyType.Develop,
				},
			];
			const conf = create(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./src/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(2);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target, {
				react: {
					amd: "react",
					commonjs: "react",
					commonjs2: "react",
					root: "React",
					umd: "react",
				},
				aaa: "aaa",
				test: {
					amd: "test",
					commonjs: "test",
					commonjs2: "test",
					root: "test",
					umd: "test",
				},
			});
			checkDistOutput(conf);
		});

		it("default, target: node, dependencies, custom mapping", () => {
			const production = false;
			const transpile = false;
			const target = ServantJson.ModuleTarget.node;
			module!.servantJson.content.target = target;
			module!.packageJson.content.dependencies = [
				{
					local: false,
					module: "react",
					version: "16.4.2",
					type: PackageJson.DependencyType.Develop,
				},
				{
					local: false,
					module: "aaa",
					version: "0.9.2",
					type: PackageJson.DependencyType.Develop,
				},
				{
					local: true,
					module: "test",
					version: "file:../test",
					type: PackageJson.DependencyType.Develop,
				},
			];
			module!.servantJson.content.libraries = {
				"simple-git/promise": "simple-git/promise",
				ModuleA: "@mymodules/a",
			};
			const conf = create(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./src/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(2);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target, {
				react: {
					amd: "react",
					commonjs: "react",
					commonjs2: "react",
					root: "React",
					umd: "react",
				},
				aaa: "aaa",
				test: {
					amd: "test",
					commonjs: "test",
					commonjs2: "test",
					root: "test",
					umd: "test",
				},
				"simple-git/promise": "simple-git/promise",
				ModuleA: "@mymodules/a",
			});
			checkDistOutput(conf);
		});

		it("default, target: web_page", () => {
			const production = false;
			const transpile = false;
			const target = ServantJson.ModuleTarget.web_page;
			module!.servantJson.content.target = target;
			const conf = create(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./src/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(3);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);
			checkHtmlBundlePlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkDistOutput(conf);
		});

		it("default, target: web_page and filled data", () => {
			const production = false;
			const transpile = false;
			const target = ServantJson.ModuleTarget.web_page;
			module!.servantJson.content.target = target;
			module!.servantJson.content.webpage = {
				title: "This is title",
				publicPath: "./public/",
				favicon: "./assets/favicon.ico",
			};
			const conf = create(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./src/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(3);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);
			checkHtmlBundlePlugin(conf, {
				title: "This is title",
				publicPath: "./public/",
				favicon: "./assets/favicon.ico",
			});

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkDistOutput(conf);
		});
	});

	describe("tests", () => {
		it("default", () => {
			const production = false;
			const transpile = false;
			const target = ServantJson.ModuleTarget.web;
			const conf = tests(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./tests/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(2);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkTestsOutput(conf);
		});

		it("default, flag: production", () => {
			const production = true;
			const transpile = false;
			const target = ServantJson.ModuleTarget.web;
			const conf = tests(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./tests/index");
			expect(conf.mode).toBe("production");

			expect(conf.plugins!.length).toEqual(2);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkTestsOutput(conf);
		});

		it("default, flag: transpile", () => {
			const production = false;
			const transpile = true;
			const target = ServantJson.ModuleTarget.web;
			const conf = tests(servantCwd, module, {
				excluded: ["tests/**/*.js"],
				production: production,
				transpile: transpile,
			});

			expect(conf.context).toBe(dir);
			expect(conf.entry).toBe("./tests/index");
			expect(conf.mode).toBe("development");

			expect(conf.plugins!.length).toEqual(2);
			checkCssPlugin(conf);
			checkCopyCssPlugin(conf);

			expect(conf.module!.rules!.length).toEqual(3);
			checkTsModule(conf, production, transpile);
			checkCssModule(conf);
			checkAssetModule(conf);

			checkNode(conf, target);
			checkResolve(conf);

			checkExternals(conf, target);
			checkTestsOutput(conf);
		});
	});

	describe("web_page template", () => {
		it("template with title", () => {
			const timestamp = new Date().getTime();
			const data = createPublicTemplate(
				{ htmlWebpackPlugin: { options: { title: "Title" } } } as any,
				{ styles: [], timestamp }
			);

			expect(data).toEqual(`<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Title</title>
  </head>
  <body></body>
</html>
`);
		});

		it("template with title and files", () => {
			const timestamp = new Date().getTime();
			const data = createPublicTemplate(
				{ htmlWebpackPlugin: { options: { title: "Title" } } } as any,
				{ styles: ["./public/styles.css"], timestamp }
			);

			expect(data).toEqual(`<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Title</title>
    <link
      type="text/css"
      rel="stylesheet"
      href="./public/styles.css?${timestamp}"
    />
  </head>
  <body></body>
</html>
`);
		});
	});
});
