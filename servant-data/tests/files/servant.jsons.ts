import { ServantJson, ModuleTarget } from "../../src/servant.json";

export const ServantJsonEmpty = JSON.stringify({
	package: "Empty",
} as ServantJson);

export const ServantJsonServant = JSON.stringify({
	package: "Servant",
	output: {
		directory: "./dist",
		filename: "servant.js",
		resolve: ["ts", "tsx"],
	},
	entry: "./src/index",
	libraries: {
		"simple-git/promise": "simple-git/promise",
		"source-map-support/register": "source-map-support/register",
	},
	target: ModuleTarget.node,
	resources: ["../logo.png", "npm:@servant/servant-data//dist/servantJson.schema.json"],
} as Partial<ServantJson>);

export const ServantJsonSingleRegistryOne = JSON.stringify({
	package: "Servant",
	registry: "http://reg.cz/test0",
} as Partial<ServantJson>);
export const ServantJsonSingleRegistryTwo = JSON.stringify({
	package: "Servant",
	registry: {
		registry: "http://reg.cz/test1",
	},
} as Partial<ServantJson>);
export const ServantJsonMultipleRegistry = JSON.stringify({
	package: "Servant",
	registry: {
		registry: "http://reg.cz/test2",
		"@scope1": "http://gitlab.com/scope1",
		"@scope2": "http://github.com/scope2",
	},
} as Partial<ServantJson>);
export const ServantJsonMultipleRegistry2 = JSON.stringify({
	package: "Servant",
	registry: {
		registry: "http://reg.cz/test3",
		"@scope1": "http://gitlab.com/scopeA",
		"@scope3": "http://github.com/scope3",
	},
} as Partial<ServantJson>);
export const ServantJsonScopesRegistry = JSON.stringify({
	package: "Servant",
	registry: {
		"@scope1": "http://gitlab.com/scopeA",
		"@scope3": "http://github.com/scope3",
	},
} as Partial<ServantJson>);

export const ServantJsonNoTesting = JSON.stringify({
	package: "Servant",
} as Partial<ServantJson>);
export const ServantJsonSimpleTestingOne = JSON.stringify({
	package: "Servant",
	testing: {
		browsers: ["Webkit", "Firefox"],
		devices: ["iPhone", "Nokia Lumia"],
	},
} as Partial<ServantJson>);
export const ServantJsonSimpleTestingTwo = JSON.stringify({
	package: "Servant",
	testing: {
		browsers: ["Chromium"],
		devices: ["Pixel 3"],
	},
} as Partial<ServantJson>);

export const ServantJsonNoPrettier = JSON.stringify({
	package: "Servant",
} as Partial<ServantJson>);
export const ServantJsonSimplerPrettierOne = JSON.stringify({
	package: "Servant",
	prettify: {
		sources: ["doc/**/*"],
		extensions: ["md"],
	},
} as Partial<ServantJson>);
export const ServantJsonSimplerPrettierTwo = JSON.stringify({
	package: "Servant",
	prettify: {
		sources: ["examples/**/*"],
		extensions: ["json", "yaml"],
	},
} as Partial<ServantJson>);
