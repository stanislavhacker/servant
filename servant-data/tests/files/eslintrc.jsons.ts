import { EslintrcJson } from "../../src/eslintrc.json";

export const EslintrcJsonEmpty = JSON.stringify({} as EslintrcJson);

export const EslintrcJsonSimple = JSON.stringify({
	root: true,
	parser: "@typescript-eslint/parser",
	plugins: ["@typescript-eslint"],
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/eslint-recommended",
		"plugin:@typescript-eslint/recommended",
	],
} as EslintrcJson);
