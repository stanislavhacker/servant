export const SimpleFile = `### HEADING
# Message
.glob`;

export const MoreComplexFile = `### HEADING

# Generated
**/src/**/*.d.ts
**/src/**/*.d.ts

### Output

# Dist files
**/dist/**/*`;
