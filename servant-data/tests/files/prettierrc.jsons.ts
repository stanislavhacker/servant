import { PrettierrcJson } from "../../src/prettierrc.json";

export const PrettierrcJsonEmpty = JSON.stringify({} as PrettierrcJson);

export const PrettierrcJsonSimple = JSON.stringify({
	trailingComma: "es5",
	tabWidth: 4,
	semi: false,
	singleQuote: true,
} as PrettierrcJson);
