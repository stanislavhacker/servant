import { TSConfig } from "../../src/tsconfig.json";

export const TsConfigJsonEmpty = JSON.stringify({} as TSConfig);

export const TsConfigJsonOne = JSON.stringify({
	compilerOptions: {
		module: "commonjs",
		target: "es6",
		sourceMap: true,
		declaration: true,
		jsx: "react",
		skipLibCheck: true,
		strictNullChecks: true,
	},
} as TSConfig);
