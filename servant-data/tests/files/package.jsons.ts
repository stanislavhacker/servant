import { PackageJson } from "../../src/package.json";

export const PackageJsonEmpty = JSON.stringify({
	name: "Test",
	version: "1.0.0",
} as PackageJson);

export const PackageJsonServant = JSON.stringify({
	name: "@servant/servant",
	version: "0.1.8",
	description: "Servant builder for node modules.",
	main: "src/index.js",
	types: "src/index.d.ts",
	scripts: {},
	files: ["dist/**/*"],
	keywords: ["servant", "build"],
	author: "Stanislav Hacker",
	repository: {
		type: "git",
		url: "https://gitlab.com/stanislavhacker/servant.git",
	},
	engines: {
		node: ">=10.0.0",
		npm: ">=6.0.0",
	},
	license: "GPL-3.0-or-later",
	dependencies: {
		"@servant/servant-data": "file:../servant-data",
		glob: "^7.1.3",
	},
	devDependencies: {
		"@types/node": "^11.12.0",
	},
	bugs: {
		url: "https://gitlab.com/stanislavhacker/servant/issues",
	},
} as PackageJson);

export const PackageJsonEslintConfig = JSON.stringify({
	...JSON.parse(PackageJsonServant),
	eslintConfig: {
		root: true,
	},
} as PackageJson);

export const PackageJsonPrettierConfig = JSON.stringify({
	...JSON.parse(PackageJsonServant),
	prettier: {
		tabWidth: 4,
	},
} as PackageJson);
