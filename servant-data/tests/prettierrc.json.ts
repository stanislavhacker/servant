/* eslint-disable @typescript-eslint/ban-ts-comment */
import { PRETTIERRC_JSON, createDefault, save, load } from "../src/prettierrc.json";
import { PrettierrcJsonEmpty, PrettierrcJsonSimple } from "./files/prettierrc.jsons";
import { Path } from "@servant/servant-files";
import * as fs from "fs";

describe(".prettierrc.json", () => {
	it("name", () => {
		expect(PRETTIERRC_JSON).toBe(".prettierrc.json");
	});

	it("create", () => {
		const pj = createDefault();
		expect(pj).toEqual({});
	});

	describe("I/O operations", () => {
		let stats: fs.Stats;
		let data: Buffer;
		let saved: Buffer | string | null = null;
		let statError: Error | null;
		let readFileError: Error | null;
		let writeFileError: Error | null;
		let mkdirError: Error | null;

		beforeEach(() => {
			saved = null;
			stats = new fs.Stats();
			data = Buffer.from("");
			statError = null;
			readFileError = null;
			writeFileError = null;
			mkdirError = null;

			// @ts-ignore
			spyOn(fs, "stat").and.callFake((path, callback) => {
				callback(statError as Error, stats);
			});
			// @ts-ignore
			spyOn(fs, "readFile").and.callFake((path, callback) => {
				callback(readFileError, data);
			});
			// @ts-ignore
			spyOn(fs, "writeFile").and.callFake((path, content, callback) => {
				saved = content;
				callback(writeFileError);
			});
			// @ts-ignore
			spyOn(fs, "mkdir").and.callFake((path, options, callback) => {
				callback(mkdirError);
			});
		});

		describe("load function", () => {
			it("load without error empty", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PrettierrcJsonEmpty);

				const json = await load("/path/.prettierrc.json");
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/.prettierrc.json");
				expect(json.content).toEqual({});
			});

			it("load without error full", async () => {
				spyOn(stats, "isFile").and.returnValue(true);
				data = Buffer.from(PrettierrcJsonSimple);

				const json = await load("/path/.prettierrc.json");
				expect(Path.normalize(json.cwd)).toBe("/path");
				expect(Path.normalize(json.path)).toBe("/path/.prettierrc.json");
				expect(json.content).toEqual({
					trailingComma: "es5",
					tabWidth: 4,
					semi: false,
					singleQuote: true,
				});
			});

			it("load with stat error not fail", async () => {
				statError = new Error("");
				await expectAsync(load("/path/.prettierrc.json")).not.toBeRejected();
			});

			it("load with read file error not fail", async () => {
				readFileError = new Error("");
				await expectAsync(load("/path/.prettierrc.json")).not.toBeRejected();
			});
		});

		describe("save function", () => {
			it("save without error", async () => {
				await save("/path/.prettierrc.json", createDefault());
				expect(saved).not.toBeNull();
			});

			it("save with mkdir error", async () => {
				mkdirError = new Error("");
				await expectAsync(save("/path/.prettierrc.json", createDefault())).toBeRejected();
			});

			it("save with writeFile error", async () => {
				writeFileError = new Error("");
				await expectAsync(save("/path/.prettierrc.json", createDefault())).toBeRejected();
			});
		});
	});
});
