import { WebpackJson } from "@servant/servant-data";
import { webpackBuild } from "./webpack";

export { webpackBuild };

process.on("message", (mes: Partial<WebpackJson.WebpackJson>) => {
	const message = WebpackJson.remap(mes);
	webpackBuild(
		message.cwd,
		message.entry,
		message.module,
		message.production,
		message.transpile
	).then(onProgress);
});

function onProgress(results: WebpackJson.WebpackCompile) {
	process.send && process.send(results);
}
