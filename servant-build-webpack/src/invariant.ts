const ServantDataErrors = [
	"No module declaration found. Invalid data provided or is probably error in Servant.",
] as const;

export function invariant(
	// eslint-disable-next-line @typescript-eslint/ban-types
	condition: boolean | string | null | undefined | number | Object,
	message: (typeof ServantDataErrors)[number]
): asserts condition {
	if (!condition) {
		throw new Error(message);
	}
}
