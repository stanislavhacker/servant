import { WebpackConfig, WebpackJson, Modules } from "@servant/servant-data";
import * as webpack from "webpack";
import * as path from "path";
import { invariant } from "./invariant";

export function webpackBuild(
	cwd: string | null,
	entry: string | null,
	module: Modules.ModuleInfo | null,
	production: boolean,
	trnspile: boolean
): Promise<WebpackJson.WebpackCompile> {
	return new Promise((fulfill, reject) => {
		const compile: WebpackJson.WebpackCompile = createCompile();

		//no data provided
		if (!cwd || !entry || !module) {
			fulfill(compile);
			return;
		}

		invariant(
			module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		//normal module build
		const servantJson = module.servantJson;
		const webpackConfig = WebpackConfig.create(cwd, module, {
			production: production,
			transpile: trnspile,
			excluded: servantJson.content.tests,
		});
		//call webpack
		webpack(webpackConfig as webpack.Configuration, (err: Error, stats) => {
			if (err) {
				reject(err);
				return;
			}
			if (!stats) {
				reject(
					new Error(
						`Can get stats from webpack for module ${module}! Something go wrong with Servant :(`
					)
				);
				return;
			}

			const pth = stats.compilation.outputOptions.path || "";
			const filename = stats.compilation.outputOptions.filename as string;
			//compile
			compile.errors = stats.compilation.errors.map((err) => createError(err));
			compile.output = path.join(pth, filename);
			fulfill(compile);
		});
	});
}

function createCompile(): WebpackJson.WebpackCompile {
	return {
		errors: [],
		output: "",
	};
}

function createError(err: Error | string): WebpackJson.CompileError {
	if (typeof err === "string") {
		return {
			name: "Webpack compilation error",
			message: err,
			stack: "",
		};
	}
	return {
		name: err.name,
		message: err.message,
		stack: err.stack || "",
	};
}
