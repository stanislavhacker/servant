import * as UNKNOWN from "../src/types/unknown";

describe("UNKNOWN", () => {
	it("const", () => {
		expect(UNKNOWN.EXT).toEqual(["*"]);
		expect(UNKNOWN.REGEX.toString()).toEqual("/\\.*$/");
		expect(UNKNOWN.MAIN).toEqual("*");

		expect(UNKNOWN.CONTENT_TYPE).toEqual("application/octet-stream");
	});
});
