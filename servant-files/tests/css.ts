import * as CSS from "../src/types/css";

describe("CSS", () => {
	it("const", () => {
		expect(CSS.EXT).toEqual(["css"]);
		expect(CSS.REGEX.toString()).toEqual("/\\.(css)$/");
		expect(CSS.MAIN).toEqual("css");
		expect(CSS.CONTENT_TYPE).toEqual({
			css: "text/css",
		});

		expect(Object.keys(CSS.CONTENT_TYPE)).toEqual(CSS.EXT);
	});

	it("toCSS", () => {
		expect(CSS.toCSS("test/index.js")).toBe("test/index.css");
		expect(CSS.toCSS("test/index")).toBe("test/index.css");
		expect(CSS.toCSS("test/index.css")).toBe("test/index.css");
		expect(CSS.toCSS("test\\index.css")).toBe("test/index.css");
	});

	it("only", () => {
		const filter = CSS.only(["test/index.doc", "test/index", "test/index.css"]);

		expect(filter.length).toBe(1);
	});

	it("isCSS", function () {
		expect(CSS.isCSS("test/test.css")).toBe(true);
		expect(CSS.isCSS("test/test.js")).toBe(false);
		expect(CSS.isCSS("test/test")).toBe(false);
	});
});
