import * as LESS from "../src/types/less";

describe("LESS", () => {
	it("const", () => {
		expect(LESS.EXT).toEqual(["less"]);
		expect(LESS.REGEX.toString()).toEqual("/\\.(less)$/");
		expect(LESS.MAIN).toEqual("less");

		expect(LESS.CONTENT_TYPE).toEqual({
			less: "plain/text",
		});

		expect(Object.keys(LESS.CONTENT_TYPE)).toEqual(LESS.EXT);
	});

	it("toLESS", () => {
		expect(LESS.toLESS("test/index.css")).toBe("test/index.less");
		expect(LESS.toLESS("test/index")).toBe("test/index.less");
		expect(LESS.toLESS("test/index.js")).toBe("test/index.less");
		expect(LESS.toLESS("test\\index.js")).toBe("test/index.less");
	});

	it("only", () => {
		const filter = LESS.only([
			"test/index.doc",
			"test/index",
			"test/index.less",
			"test/test.less",
		]);

		expect(filter.length).toBe(2);
	});

	it("isLESS", function () {
		expect(LESS.isLESS("test/test.less")).toBe(true);
		expect(LESS.isLESS("test/test.jsx")).toBe(false);
		expect(LESS.isLESS("test/test")).toBe(false);
	});

	it("fromCss", () => {
		expect(LESS.fromCss("test/test.css")).toBe("test/test.less");
		expect(LESS.fromCss("test/test.js")).toBe(null);
		expect(LESS.fromCss("test/test.less")).toBe(null);
	});
});
