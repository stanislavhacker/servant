import * as HTML from "../src/types/html";

describe("HTML", () => {
	it("const", () => {
		expect(HTML.EXT).toEqual(["html"]);
		expect(HTML.REGEX.toString()).toEqual("/\\.(html)$/");
		expect(HTML.MAIN).toEqual("html");

		expect(HTML.CONTENT_TYPE).toEqual({
			html: "text/html",
		});

		expect(Object.keys(HTML.CONTENT_TYPE)).toEqual(HTML.EXT);
	});

	it("toHTML", () => {
		expect(HTML.toHTML("test/index.css")).toBe("test/index.html");
		expect(HTML.toHTML("test/index")).toBe("test/index.html");
		expect(HTML.toHTML("test/index.ts")).toBe("test/index.html");
		expect(HTML.toHTML("test\\index.ts")).toBe("test/index.html");
	});
});
