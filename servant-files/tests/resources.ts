import * as RESOURCES from "../src/types/resources";

describe("RESOURCES", () => {
	it("const", () => {
		expect(RESOURCES.EXT).toEqual([
			"png",
			"jpg",
			"gif",
			"svg",
			"eot",
			"ttf",
			"woff",
			"woff2",
			"ico",
			"webp",
		]);
		expect(RESOURCES.REGEX.toString()).toEqual(
			"/\\.(png|jpg|gif|svg|eot|ttf|woff|woff2|ico|webp)$/"
		);

		expect(RESOURCES.CONTENT_TYPE).toEqual({
			png: "image/png",
			jpg: "image/jpeg",
			gif: "image/gif",
			svg: "image/svg+xml",
			eot: "application/vnd.ms-fontobject",
			ttf: "font/ttf",
			woff: "font/woff",
			woff2: "font/woff2",
			ico: "image/x-icon",
			webp: "image/webp",
		});

		expect(Object.keys(RESOURCES.CONTENT_TYPE)).toEqual(RESOURCES.EXT);
	});
});
