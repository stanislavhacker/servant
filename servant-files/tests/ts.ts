import * as TS from "../src/types/ts";

describe("TS", () => {
	it("const", () => {
		expect(TS.EXT).toEqual(["ts", "tsx"]);
		expect(TS.REGEX.toString()).toEqual("/\\.(ts|tsx)$/");
		expect(TS.MAIN).toEqual("ts");
		expect(TS.DECLARATION).toEqual("d.ts");
		expect(TS.MAP).toEqual("js.map");

		expect(TS.CONTENT_TYPE).toEqual({
			ts: "text/x.typescript",
			tsx: "text/x.typescript",
		});

		expect(Object.keys(TS.CONTENT_TYPE)).toEqual(TS.EXT);
	});

	it("toTS", () => {
		expect(TS.toTS("test/index.css")).toBe("test/index.ts");
		expect(TS.toTS("test/index")).toBe("test/index.ts");
		expect(TS.toTS("test/index.ts")).toBe("test/index.ts");
		expect(TS.toTS("test\\index.ts")).toBe("test/index.ts");
	});

	it("toTSX", () => {
		expect(TS.toTSX("test/index.css")).toBe("test/index.tsx");
		expect(TS.toTSX("test/index")).toBe("test/index.tsx");
		expect(TS.toTSX("test/index.ts")).toBe("test/index.tsx");
		expect(TS.toTSX("test\\index.ts")).toBe("test/index.tsx");
	});

	it("toDTS", () => {
		expect(TS.toDTS("test/index.css")).toBe("test/index.d.ts");
		expect(TS.toDTS("test/index")).toBe("test/index.d.ts");
		expect(TS.toDTS("test/index.ts")).toBe("test/index.d.ts");
		expect(TS.toDTS("test\\index.ts")).toBe("test/index.d.ts");
		expect(TS.toDTS("test/index.d.ts")).toBe("test/index.d.ts");
	});

	it("isTS", function () {
		expect(TS.isTS("test/test.ts")).toBe(true);
		expect(TS.isTS("test/test.d.ts")).toBe(true);
		expect(TS.isTS("test/test.jsx")).toBe(false);
		expect(TS.isTS("test/test")).toBe(false);
	});

	it("isTSX", function () {
		expect(TS.isTSX("test/test.tsx")).toBe(true);
		expect(TS.isTSX("test/test.d.ts")).toBe(false);
		expect(TS.isTSX("test/test.ts")).toBe(false);
		expect(TS.isTSX("test/test.jsx")).toBe(false);
		expect(TS.isTSX("test/test")).toBe(false);
	});

	it("isDTS", function () {
		expect(TS.isDTS("test/test.d.ts")).toBe(true);
		expect(TS.isDTS("test/test.ts")).toBe(false);
		expect(TS.isDTS("test/test.tsx")).toBe(false);
		expect(TS.isDTS("test/test.jsx")).toBe(false);
		expect(TS.isDTS("test/test")).toBe(false);
	});

	it("isMAP", function () {
		expect(TS.isMAP("test/test.js.map")).toBe(true);
		expect(TS.isMAP("test/test.map")).toBe(false);
		expect(TS.isMAP("test/test.ts")).toBe(false);
		expect(TS.isMAP("test/test.tsx")).toBe(false);
		expect(TS.isMAP("test/test.js")).toBe(false);
		expect(TS.isMAP("test/test")).toBe(false);
	});

	it("only", () => {
		const filter = TS.only([
			"test/index.ts",
			"test/index.tsx",
			"test/index",
			"test/index.less",
			"test/test.less",
		]);

		expect(filter.length).toBe(2);
	});

	it("fromJs", () => {
		expect(TS.fromJs("test/index.js")).toEqual(["test/index.ts", "test/index.tsx"]);
		expect(TS.fromJs("test/index.jsx")).toEqual(["test/index.ts", "test/index.tsx"]);
		expect(TS.fromJs("test/index.ts")).toBe(null);
		expect(TS.fromJs("test/index.less")).toBe(null);
		expect(TS.fromJs("test/index")).toBe(null);
	});

	it("fromDTS", () => {
		expect(TS.fromDTS("test/index.d.ts")).toEqual(["test/index.ts", "test/index.tsx"]);
		expect(TS.fromDTS("test/index.ts")).toBe(null);
		expect(TS.fromDTS("test/index.less")).toBe(null);
		expect(TS.fromDTS("test/index")).toBe(null);
	});

	it("toJS", () => {
		expect(TS.toJS("ts")).toBe(".js");
		expect(TS.toJS("tsx")).toBe(".jsx");
		expect(TS.toJS(".ts")).toBe(".js");
		expect(TS.toJS(".tsx")).toBe(".jsx");
		expect(TS.toJS(".js")).toBe(null);
		expect(TS.toJS(".jsx")).toBe(null);
	});

	it("isTypescript", function () {
		expect(TS.isTypescript("test/test.ts")).toBe(true);
		expect(TS.isTypescript("test/test.tsx")).toBe(true);
		expect(TS.isTypescript("test/test.map")).toBe(false);
		expect(TS.isTypescript("test/test.js")).toBe(false);
		expect(TS.isTypescript("test/test")).toBe(false);
	});
});
