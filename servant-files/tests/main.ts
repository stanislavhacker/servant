import * as Extensions from "../src/extensions";
import * as Path from "../src/path";

describe("files", () => {
	describe("extensions", () => {
		it("create", () => {
			expect(Extensions.create("aa")).toBe(".aa");
			expect(Extensions.create(".aa")).toBe(".aa");
		});

		it("replace", () => {
			expect(Extensions.replace("test/test", "css")).toBe("test/test.css");
			expect(Extensions.replace("test/test", ".css")).toBe("test/test.css");
			expect(Extensions.replace("test/test.ts", "css")).toBe("test/test.css");
			expect(Extensions.replace("test/test.ts", ".css")).toBe("test/test.css");
		});

		it("replaceAll", () => {
			expect(Extensions.replaceAll("test/test", ["css", "ts"])).toEqual([
				"test/test.css",
				"test/test.ts",
			]);
			expect(Extensions.replaceAll("test/test", ["js", "ts"])).toEqual([
				"test/test.js",
				"test/test.ts",
			]);
			expect(Extensions.replaceAll("test/test.ts", ["css", "less"])).toEqual([
				"test/test.css",
				"test/test.less",
			]);
			expect(Extensions.replaceAll("test/test.ts", ["css", "less"])).toEqual([
				"test/test.css",
				"test/test.less",
			]);
		});

		it("matched", () => {
			expect(Extensions.matched("test/test", "css")).toBe(false);
			expect(Extensions.matched("test/test.css", "css")).toBe(true);
			expect(Extensions.matched("test/test.css", "ts")).toBe(false);
			expect(Extensions.matched("test/test.jsx", "jsx")).toBe(true);
		});

		it("filter", () => {
			const filtered = Extensions.filter(
				["test/test.js", "test/test.jsx", "test/test.css", "test/test", "test/test.ts"],
				["js", "jsx"]
			);

			expect(filtered).toEqual(["test/test.js", "test/test.jsx"]);
		});

		describe("resolve", () => {
			it("array of extensions", () => {
				const res = Extensions.resolve("/path/to/dir", "./src/index", ["ts", "js", "jsx"]);
				expect(res).toEqual([
					"/path/to/dir/src/index.ts",
					"/path/to/dir/src/index.js",
					"/path/to/dir/src/index.jsx",
				]);
			});

			it("array of extensions and extension", () => {
				const res = Extensions.resolve("/path/to/dir", "./src/index.css", [
					"ts",
					"js",
					"jsx",
				]);
				expect(res).toEqual(["/path/to/dir/src/index.css"]);
			});

			it("no array of extensions and no extension", () => {
				const res = Extensions.resolve("/path/to/dir", "./src/index", []);
				expect(res).toEqual([]);
			});

			it("array of extensions without entry", () => {
				const res = Extensions.resolve("", "./src/index", ["ts", "js", "jsx"]);
				expect(res).toEqual(["src/index.ts", "src/index.js", "src/index.jsx"]);
			});
		});

		describe("resolveAll", () => {
			it("array of extensions", () => {
				const res = Extensions.resolveAll("/path/to/dir", ["ts", "js", "jsx"]);
				expect(res).toEqual([
					"/path/to/dir/**/*.ts",
					"/path/to/dir/**/*.js",
					"/path/to/dir/**/*.jsx",
				]);
			});
		});

		it("matchOne", () => {
			expect(Extensions.matchedOne("test/test", ["css", "ts"])).toBe(false);
			expect(Extensions.matchedOne("test/test.css", ["css", "ts"])).toBe(true);
			expect(Extensions.matchedOne("test/test.ts", ["css", "ts"])).toBe(true);
			expect(Extensions.matchedOne("test/test.tsx", ["css", "ts"])).toBe(false);

			expect(Extensions.matchedOne("test/test", [".css", ".ts"])).toBe(false);
			expect(Extensions.matchedOne("test/test.css", [".css", ".ts"])).toBe(true);
			expect(Extensions.matchedOne("test/test.ts", [".css", ".ts"])).toBe(true);
			expect(Extensions.matchedOne("test/test.tsx", [".css", ".ts"])).toBe(false);
		});

		it("get mime type", () => {
			expect(Extensions.getMimeType("html")).toBe("text/html");
			expect(Extensions.getMimeType(".html")).toBe("text/html");
			expect(Extensions.getMimeType("js")).toBe("text/javascript");
			expect(Extensions.getMimeType(".js")).toBe("text/javascript");
			expect(Extensions.getMimeType("png")).toBe("image/png");
			expect(Extensions.getMimeType(".jpg")).toBe("image/jpeg");
			expect(Extensions.getMimeType("css")).toBe("text/css");
			expect(Extensions.getMimeType(".css")).toBe("text/css");
			expect(Extensions.getMimeType("json")).toBe("application/json");
			expect(Extensions.getMimeType(".json")).toBe("application/json");
			expect(Extensions.getMimeType("ts")).toBe("text/x.typescript");
			expect(Extensions.getMimeType(".ts")).toBe("text/x.typescript");
			expect(Extensions.getMimeType("xxx")).toBe("application/octet-stream");
			expect(Extensions.getMimeType(".xxx")).toBe("application/octet-stream");
			expect(Extensions.getMimeType("less")).toBe("plain/text");
			expect(Extensions.getMimeType(".less")).toBe("plain/text");
		});

		it("collectJS", function () {
			expect(
				Extensions.collectJS([
					"test/index.js",
					"test/index.ts",
					"test/app.tsx",
					"test/app.jsx",
					"test/test.js",
				])
			).toEqual(["test/index.js", "test/app.jsx", "test/test.js"]);
		});

		it("collectTS", function () {
			expect(Extensions.collectTS(["test/index.ts", "test/index.tsx", "test/app"])).toEqual([
				"test/index.ts",
				"test/index.tsx",
				"test/app.ts",
				"test/app.tsx",
			]);
		});
	});

	describe("path", () => {
		it("matched", () => {
			expect(Path.matched("/project/path/test.png", "**/*")).toBe(true);
			expect(Path.matched("/project/path/test.png", "**/*.png")).toBe(true);
			expect(Path.matched("/project/path/test.png", "**/*.jpg")).toBe(false);
			expect(Path.matched("/project/path/test.png", "**/path/*")).toBe(true);
			expect(Path.matched("/project/path/test.png", "**/pathx/*")).toBe(false);
		});

		it("matchedOne", () => {
			expect(
				Path.matchedOne("/project/path/test.png", ["**/*", "**/*.png", "**/*.jpg"])
			).toBe(true);
			expect(Path.matchedOne("/project/path/test.png", ["**/*.png", "**/*.jpg"])).toBe(true);
			expect(Path.matchedOne("/project/path/test.png", ["**/*.jpg", "**/*.gif"])).toBe(false);
			expect(Path.matchedOne("/project/path/test.png", ["**/path/*"])).toBe(true);
			expect(Path.matchedOne("/project/path/test.png", ["**/pathx/*"])).toBe(false);
		});

		it("normalize", () => {
			expect(Path.normalize("/project/path/test.png")).toBe("/project/path/test.png");
			expect(Path.normalize("\\project/path\\test.png")).toBe("/project/path/test.png");
		});

		it("join", () => {
			const joined = Path.join("/project/path/", ["./src/test.js", "./test/index.ts"]);
			expect(joined).toEqual(["/project/path/src/test.js", "/project/path/test/index.ts"]);
		});

		it("directory", () => {
			expect(Path.directory("/path/to/dir", "./src/**/*")).toBe("/path/to/dir/src");
			expect(Path.directory("/path/to/dir", "./src")).toBe("/path/to/dir/src");
			expect(Path.directory("/path/to/dir", "./src/*.js")).toBe("/path/to/dir/src");
			expect(Path.directory("/path/to/dir", "./**")).toBe("/path/to/dir");
		});
	});

	describe("patterns", () => {
		it("all", () => {
			expect(Path.patterns.all("/project/path/")).toBe("/project/path/**/*");
		});

		it("everywhere", () => {
			expect(Path.patterns.everywhere("/project/path/")).toBe("**/project/path/");
		});
	});
});
