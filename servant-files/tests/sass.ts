import * as SASS from "../src/types/sass";

describe("SASS", () => {
	it("const", () => {
		expect(SASS.EXT).toEqual(["scss"]);
		expect(SASS.REGEX.toString()).toEqual("/\\.(scss)$/");
		expect(SASS.MAIN).toEqual("scss");

		expect(SASS.CONTENT_TYPE).toEqual({
			scss: "plain/text",
		});

		expect(Object.keys(SASS.CONTENT_TYPE)).toEqual(SASS.EXT);
	});

	it("toSASS", () => {
		expect(SASS.toSASS("test/index.css")).toBe("test/index.scss");
		expect(SASS.toSASS("test/index")).toBe("test/index.scss");
		expect(SASS.toSASS("test/index.js")).toBe("test/index.scss");
		expect(SASS.toSASS("test\\index.js")).toBe("test/index.scss");
	});

	it("only", () => {
		const filter = SASS.only([
			"test/index.doc",
			"test/index",
			"test/index.scss",
			"test/test.scss",
		]);

		expect(filter.length).toBe(2);
	});

	it("isSASS", function () {
		expect(SASS.isSASS("test/test.scss")).toBe(true);
		expect(SASS.isSASS("test/test.less")).toBe(false);
		expect(SASS.isSASS("test/test.jsx")).toBe(false);
		expect(SASS.isSASS("test/test")).toBe(false);
	});

	it("fromCss", () => {
		expect(SASS.fromCss("test/test.css")).toBe("test/test.scss");
		expect(SASS.fromCss("test/test.js")).toBe(null);
		expect(SASS.fromCss("test/test.less")).toBe(null);
		expect(SASS.fromCss("test/test.scss")).toBe(null);
	});
});
