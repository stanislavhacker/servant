import * as JSON from "../src/types/json";

describe("JSON", () => {
	it("const", () => {
		expect(JSON.EXT).toEqual(["json"]);
		expect(JSON.REGEX.toString()).toEqual("/\\.(json)$/");
		expect(JSON.MAIN).toEqual("json");

		expect(JSON.CONTENT_TYPE).toEqual({
			json: "application/json",
		});

		expect(Object.keys(JSON.CONTENT_TYPE)).toEqual(JSON.EXT);
	});

	it("toJSON", () => {
		expect(JSON.toJSON("test/index.css")).toBe("test/index.json");
		expect(JSON.toJSON("test/index")).toBe("test/index.json");
		expect(JSON.toJSON("test/index.ts")).toBe("test/index.json");
		expect(JSON.toJSON("test\\index.ts")).toBe("test/index.json");
	});
});
