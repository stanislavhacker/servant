import * as JS from "../src/types/js";

describe("JS", () => {
	it("const", () => {
		expect(JS.EXT).toEqual(["js", "jsx", "mjs"]);
		expect(JS.REGEX.toString()).toEqual("/\\.(js|jsx|mjs)$/");
		expect(JS.MAIN).toEqual("js");

		expect(JS.CONTENT_TYPE).toEqual({
			js: "text/javascript",
			jsx: "text/javascript",
			mjs: "text/javascript",
		});

		expect(Object.keys(JS.CONTENT_TYPE)).toEqual(JS.EXT);
	});

	it("toJS", () => {
		expect(JS.toJS("test/index.css")).toBe("test/index.js");
		expect(JS.toJS("test/index")).toBe("test/index.js");
		expect(JS.toJS("test/index.js")).toBe("test/index.js");
		expect(JS.toJS("test\\index.js")).toBe("test/index.js");
	});

	it("toJSX", () => {
		expect(JS.toJSX("test/index.css")).toBe("test/index.jsx");
		expect(JS.toJSX("test/index")).toBe("test/index.jsx");
		expect(JS.toJSX("test/index.js")).toBe("test/index.jsx");
		expect(JS.toJSX("test\\index.js")).toBe("test/index.jsx");
	});

	it("toMJS", () => {
		expect(JS.toMJS("test/index.css")).toBe("test/index.mjs");
		expect(JS.toMJS("test/index")).toBe("test/index.mjs");
		expect(JS.toMJS("test/index.js")).toBe("test/index.mjs");
		expect(JS.toMJS("test\\index.js")).toBe("test/index.mjs");
	});

	it("only", () => {
		const filter = JS.only(["test/index.doc", "test/index", "test/index.js", "test/index.jsx"]);

		expect(filter.length).toBe(2);
	});

	it("isJS", function () {
		expect(JS.isJS("test/test.js")).toBe(true);
		expect(JS.isJS("test/test.jsx")).toBe(false);
		expect(JS.isJS("test/test.mjs")).toBe(false);
		expect(JS.isJS("test/test")).toBe(false);
	});

	it("isJSX", function () {
		expect(JS.isJSX("test/test.jsx")).toBe(true);
		expect(JS.isJSX("test/test.js")).toBe(false);
		expect(JS.isJSX("test/test.mjs")).toBe(false);
		expect(JS.isJSX("test/test")).toBe(false);
	});

	it("isMJS", function () {
		expect(JS.isMJS("test/test.mjs")).toBe(true);
		expect(JS.isMJS("test/test.jsx")).toBe(false);
		expect(JS.isMJS("test/test.js")).toBe(false);
		expect(JS.isMJS("test/test")).toBe(false);
	});

	it("isTypescript", function () {
		expect(JS.isJavascript("test/test.js")).toBe(true);
		expect(JS.isJavascript("test/test.jsx")).toBe(true);
		expect(JS.isJavascript("test/test.mjs")).toBe(true);
		expect(JS.isJavascript("test/test.ts")).toBe(false);
		expect(JS.isJavascript("test/test.tsx")).toBe(false);
		expect(JS.isJavascript("test/test.map")).toBe(false);
		expect(JS.isJavascript("test/test")).toBe(false);
	});
});
