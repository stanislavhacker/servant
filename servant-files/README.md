# ![Servant][logo] Servant files module

Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

Files module for Servant that is used for loading files based on patterns, manipulations with extensions and extensions resolving.

### License

[Licensed under GPLv3][license]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
[preview]: https://gitlab.com/stanislavhacker/servant/raw/master/assets/servant.example1.gif
[init]: https://gitlab.com/stanislavhacker/servant/raw/master/assets/servant.init.gif
[1]: https://docs.npmjs.com/files/package.json
[2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
[3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
[4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
[5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
[license]: https://gitlab.com/stanislavhacker/servant/blob/master/LICENSE
