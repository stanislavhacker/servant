import * as path from "path";
import * as minimatch from "minimatch";
import { normalize } from "./path";

import * as CSS from "./types/css";
import * as LESS from "./types/less";
import * as SASS from "./types/sass";
import * as HTML from "./types/html";
import * as JS from "./types/js";
import * as JSON from "./types/json";
import * as RESOURCES from "./types/resources";
import * as TS from "./types/ts";
import * as UNKNOWN from "./types/unknown";

export function replace(file: string, ext: string): string {
	const basename = path.basename(file, path.extname(file)) + create(ext);
	return normalize(path.join(path.dirname(file), basename));
}

export function replaceAll(file: string, exts: Array<string>): Array<string> {
	return exts.map((ext) => {
		return replace(file, ext);
	});
}

export function create(ext: string): string {
	if (ext[0] === ".") {
		return ext;
	}
	return `.${ext}`;
}

export function matched(file: string, ext: string): boolean {
	return minimatch(file, patterns.all(ext));
}

export function filter(files: Array<string>, exts: Array<string>): Array<string> {
	return files.filter((file) => {
		const ext = path.extname(file);
		const basename = path.basename(file);
		const include = exts.some((item) =>
			basename.toLowerCase().endsWith(create(item.toLowerCase()))
		);

		return ext && include;
	});
}

export function resolve(entry: string, relative: string, exts: Array<string>): Array<string> {
	const check: Array<string> = [];

	if (!path.extname(relative)) {
		exts.forEach((ext) => {
			check.push(normalize(path.join(entry, `${relative}${create(ext)}`)));
		});
	} else {
		check.push(normalize(path.join(entry, relative)));
	}

	return check;
}

export function resolveAll(entry: string, exts: Array<string>): Array<string> {
	const check: Array<string> = [];

	exts.forEach((ext) => {
		check.push(normalize(path.join(entry, patterns.all(ext))));
	});

	return check;
}

export function matchedOne(file: string, exts: Array<string>) {
	return !!exts.find((ext) => {
		const extension = create(ext);
		return file.indexOf(extension) === file.length - extension.length;
	});
}

export function getMimeType(extension: string) {
	const ext = extension[0] === "." ? extension.slice(1) : extension;

	return (
		CSS.CONTENT_TYPE[ext] ||
		HTML.CONTENT_TYPE[ext] ||
		JS.CONTENT_TYPE[ext] ||
		JSON.CONTENT_TYPE[ext] ||
		RESOURCES.CONTENT_TYPE[ext] ||
		TS.CONTENT_TYPE[ext] ||
		LESS.CONTENT_TYPE[ext] ||
		SASS.CONTENT_TYPE[ext] ||
		UNKNOWN.CONTENT_TYPE
	);
}

export const patterns = {
	all: (ext: string) => {
		return normalize(`**/*${create(ext)}`);
	},
};

export function collectJS(files: Array<string>): Array<string> {
	const javascripts: Array<string> = [];

	files.forEach((test) => {
		const entry = path.dirname(test);
		const basename = path.basename(test);

		const resolved = resolve(entry, basename, JS.EXT);

		javascripts.push(
			...resolved.map((file) => {
				if (TS.isTS(file)) {
					return JS.toJS(file);
				}
				if (TS.isTSX(file)) {
					return JS.toJSX(file);
				}
				return normalize(file);
			})
		);
	});

	return [...new Set([...javascripts])];
}

export function collectTS(files: Array<string>): Array<string> {
	const typescripts: Array<string> = [];

	files.forEach((test) => {
		const entry = path.dirname(test);
		const basename = path.basename(test);

		const resolved = resolve(entry, basename, TS.EXT);

		typescripts.push(
			...resolved.map((file) => {
				return normalize(file);
			})
		);
	});

	return typescripts;
}
