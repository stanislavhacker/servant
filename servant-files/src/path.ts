import * as minimatch from "minimatch";
import * as path from "path";
import * as glob from "glob";

export function normalize(path: string) {
	return path.replace(/\\/g, "/");
}

export function matched(file: string, pattern: string): boolean {
	return minimatch(file, normalize(pattern));
}

export function matchedOne(file: string, patterns: Array<string>): boolean {
	return patterns.some((pattern) => {
		return minimatch(file, normalize(pattern));
	});
}

export function join(entry: string, files: Array<string>): Array<string> {
	return files.map((file) => {
		return normalize(path.join(entry, file));
	});
}

export function directory(entry: string, dir: string): string {
	const items = normalize(dir).split("/");
	const data: Array<string> = [];

	while (items.length > 0) {
		const itm = items.shift();
		if (glob.hasMagic(itm)) {
			break;
		}
		if (itm) {
			data.push(itm);
		}
	}
	return normalize(path.join(entry, data.join("/")));
}

export const patterns = {
	all: (directory: string) => {
		return normalize(path.join(directory, "**/*"));
	},
	everywhere: (directory: string) => {
		return normalize(path.join("**", directory));
	},
};
