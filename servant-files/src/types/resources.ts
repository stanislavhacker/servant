const extensions = ["png", "jpg", "gif", "svg", "eot", "ttf", "woff", "woff2", "ico", "webp"];

// noinspection UnnecessaryLocalVariableJS
export const EXT = extensions;
export const REGEX = new RegExp("\\.(" + EXT.join("|") + ")$");

export const CONTENT_TYPE = {
	png: "image/png",
	jpg: "image/jpeg",
	gif: "image/gif",
	svg: "image/svg+xml",
	eot: "application/vnd.ms-fontobject",
	ttf: "font/ttf",
	woff: "font/woff",
	woff2: "font/woff2",
	ico: "image/x-icon",
	webp: "image/webp",
};
