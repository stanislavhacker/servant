import { create, filter, replace, matched } from "../extensions";

const extensions = ["css"];

// noinspection UnnecessaryLocalVariableJS
export const EXT = extensions;
export const REGEX = new RegExp("\\.(" + EXT.join("|") + ")$");
export const MAIN = extensions[0];

export const CONTENT_TYPE = {
	css: "text/css",
};

export function toCSS(file: string) {
	return replace(file, create(extensions[0]));
}

export function only(files: Array<string>): Array<string> {
	return filter(files, extensions);
}

export function isCSS(file: string) {
	return matched(file, extensions[0]);
}
