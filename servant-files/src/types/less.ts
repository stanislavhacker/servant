import { create, filter, replace, matched } from "../extensions";

import * as CSS from "./css";

const extensions = ["less"];

// noinspection UnnecessaryLocalVariableJS
export const EXT = extensions;
export const REGEX = new RegExp("\\.(" + EXT.join("|") + ")$");
export const MAIN = extensions[0];

export const CONTENT_TYPE = {
	less: "plain/text",
};

export function fromCss(file: string): string | null {
	if (CSS.isCSS(file)) {
		return toLESS(file);
	}
	return null;
}

export function toLESS(file: string) {
	return replace(file, create(extensions[0]));
}

export function isLESS(file: string) {
	return matched(file, extensions[0]);
}

export function only(files: Array<string>): Array<string> {
	return filter(files, extensions);
}
