import { create, replace } from "../extensions";

const extensions = ["html"];

// noinspection UnnecessaryLocalVariableJS
export const EXT = extensions;
export const REGEX = new RegExp("\\.(" + EXT.join("|") + ")$");
export const MAIN = extensions[0];

export const CONTENT_TYPE = {
	html: "text/html",
};

export function toHTML(file: string) {
	return replace(file, create(extensions[0]));
}
