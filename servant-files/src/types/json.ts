import { create, replace } from "../extensions";

const extensions = ["json"];

// noinspection UnnecessaryLocalVariableJS
export const EXT = extensions;
export const REGEX = new RegExp("\\.(" + EXT.join("|") + ")$");
export const MAIN = extensions[0];

export const CONTENT_TYPE = {
	json: "application/json",
};

export function toJSON(file: string) {
	return replace(file, create(extensions[0]));
}
