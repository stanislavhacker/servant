const extensions = ["*"];

// noinspection UnnecessaryLocalVariableJS
export const EXT = extensions;
export const REGEX = new RegExp("\\." + EXT.join("|") + "$");
export const MAIN = extensions[0];

export const CONTENT_TYPE = "application/octet-stream";
