import { create, replace, filter, matched } from "../extensions";

const extensions = ["js", "jsx", "mjs"];

// noinspection UnnecessaryLocalVariableJS
export const EXT = extensions;
export const REGEX = new RegExp("\\.(" + EXT.join("|") + ")$");
export const MAIN = extensions[0];

export const CONTENT_TYPE = {
	js: "text/javascript",
	jsx: "text/javascript",
	mjs: "text/javascript",
};

export function toJS(file: string) {
	return replace(file, create(extensions[0]));
}

export function toJSX(file: string) {
	return replace(file, create(extensions[1]));
}

export function toMJS(file: string) {
	return replace(file, create(extensions[2]));
}

export function isJS(file: string) {
	return matched(file, extensions[0]);
}

export function isJSX(file: string) {
	return matched(file, extensions[1]);
}

export function isMJS(file: string) {
	return matched(file, extensions[2]);
}

export function only(files: Array<string>): Array<string> {
	return filter(files, extensions);
}

export function isJavascript(file: string) {
	return isJS(file) || isJSX(file) || isMJS(file);
}
