import { create, replace, matched, filter } from "../extensions";
import * as JS from "./js";

const extensions = ["ts", "tsx"];
const declaration = "d.ts";
const map = "js.map";

// noinspection UnnecessaryLocalVariableJS
export const EXT = extensions;
export const REGEX = new RegExp("\\.(" + EXT.join("|") + ")$");
export const MAIN = extensions[0];
export const DECLARATION = declaration;
export const MAP = map;

export const CONTENT_TYPE = {
	ts: "text/x.typescript",
	tsx: "text/x.typescript",
};

export function only(files: Array<string>): Array<string> {
	return filter(files, extensions);
}

export function fromJs(file: string): Array<string> | null {
	if (JS.isJS(file) || JS.isJSX(file)) {
		return [toTS(file), toTSX(file)];
	}
	return null;
}

export function fromDTS(file: string): Array<string> | null {
	if (isDTS(file)) {
		return extensions.map((ext) => file.replace(create(declaration), create(ext)));
	}
	return null;
}

export function toTS(file: string) {
	return replace(file, create(extensions[0]));
}

export function toTSX(file: string) {
	return replace(file, create(extensions[1]));
}

export function toDTS(file: string) {
	if (isDTS(file)) {
		return file;
	}
	return replace(file, create(declaration));
}

export function isTS(file: string) {
	return matched(file, extensions[0]);
}

export function isTSX(file: string) {
	return matched(file, extensions[1]);
}

export function isDTS(file: string) {
	const ext = create(declaration);
	return matched(file, extensions[0]) && file.indexOf(ext) === file.length - ext.length;
}

export function isMAP(file: string) {
	const ext = create(map);
	return file.indexOf(ext) === file.length - ext.length;
}

export function toJS(ext: string): string | null {
	if (create(ext) === create(extensions[0])) {
		return create(JS.EXT[0]);
	}
	if (create(ext) === create(extensions[1])) {
		return create(JS.EXT[1]);
	}
	return null;
}

export function isTypescript(file: string) {
	return isTS(file) || isTSX(file);
}
