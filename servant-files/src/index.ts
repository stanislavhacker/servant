import * as Extensions from "./extensions";
import * as Path from "./path";
import * as CSS from "./types/css";
import * as TS from "./types/ts";
import * as JS from "./types/js";
import * as RESOURCES from "./types/resources";
import * as JSON from "./types/json";
import * as HTML from "./types/html";
import * as LESS from "./types/less";
import * as SASS from "./types/sass";
import * as UNKNOWN from "./types/unknown";

export {
	Path,
	Extensions,
	//types
	RESOURCES,
	CSS,
	TS,
	JS,
	JSON,
	HTML,
	LESS,
	SASS,
	UNKNOWN,
};
