import {
	LocalTrancorderOptions,
	RemoteTrancorderOptions,
	MessageValues,
	Messages,
	Replacer,
	TrancorderApi,
	LanguageTranslations,
	LoggerMessage,
	Logger,
	LogMessageType,
	LanguageTranslation,
	LanguageTranslationFull,
	LanguageTranslationAlternative,
	CommonTrancorderOptions,
} from "./types";
import {
	createT,
	setLanguageLocal,
	setLanguageRemote,
	evaluateExpression,
	buildReplacers,
	ENUM_VALUE_MARKER,
	THIS_MARKER,
} from "./functions";
import { createTrancorderState } from "./state";

export {
	LocalTrancorderOptions,
	RemoteTrancorderOptions,
	CommonTrancorderOptions,
	Messages,
	TrancorderApi,
	Replacer,
	LoggerMessage,
	Logger,
	LogMessageType,
	LanguageTranslations,
	LanguageTranslation,
	LanguageTranslationAlternative,
	LanguageTranslationFull,
	THIS_MARKER,
	ENUM_VALUE_MARKER,
};

export function localTrancorder(options: LocalTrancorderOptions): Messages & TrancorderApi {
	const state = createTrancorderState(options);
	state.languages = options.languages || {};

	return {
		setLanguage: (lang) => setLanguageLocal(state, lang),
		t: (...args) => createT(state, ...args),
	};
}

export function remoteTrancorder(options: RemoteTrancorderOptions): Messages & TrancorderApi {
	const state = createTrancorderState(options);
	const fetch = options.fetch;

	return {
		setLanguage: (lang) => setLanguageRemote(state, fetch, lang),
		t: (...args) => createT(state, ...args),
	};
}

export type CalculatedExpression = {
	result: boolean;
	errors: Error[];
};

export function calculateExpression(
	expression: string,
	replacers: Record<string, object>
): CalculatedExpression {
	const [result, errors] = evaluateExpression(expression, replacers);

	return { result, errors };
}

export type CalculatedReplacers = {
	list: Replacer[];
	replacers: Record<string, object>;
};

export function calculateReplacers(
	values: object,
	options: CommonTrancorderOptions
): CalculatedReplacers {
	const state = createTrancorderState(options);
	const list = buildReplacers(state, [], values as MessageValues, {});
	const replacers = list.reduce((prev, current) => {
		prev[current.pointer] = current.raw;
		return prev;
	}, {});

	return {
		list,
		replacers,
	};
}
