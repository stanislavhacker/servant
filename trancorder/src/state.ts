import { Formatter, LocalTrancorderOptions, TrancorderState } from "./types";
import { createLogger } from "./native/logger";

export function createTrancorderState(options: LocalTrancorderOptions): TrancorderState {
	return {
		language: null,
		languages: {},
		intl: {
			use: false,
			...(options.intl || {}),
		},
		string: options.string || defaultFormatter(),
		boolean: options.boolean || defaultFormatter(),
		number: options.number || defaultFormatter(),
		datetime: options.datetime || defaultFormatter(),
		array: options.array || defaultFormatter(),
		debug: options.debug || false,
		logger: createLogger(options.debug || false, options.logger),
	};
}

function defaultFormatter<T>(): Formatter<T> {
	return (_, value) => {
		if (Array.isArray(value)) {
			return value.join(", ");
		}
		return String(value);
	};
}
