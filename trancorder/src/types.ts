//Languages

export type LanguageTranslations = Record<string, LanguageTranslation>;
export type LanguageTranslation = string | LanguageTranslationFull;
export type LanguageTranslationAlternative = string | Omit<LanguageTranslationFull, "alternatives">;
export type LanguageTranslationFull = {
	//Value of message
	value: string;
	//This is description use for get context about translation message. It will be exported for translation
	//company or service. For example if message is "Space", set some info, that space mean outer space and not a key
	//This description is not visible anywhere in client app
	description?: string;
	//Info for translation company that this message is not intended for translate
	translate?: boolean;
	//Info for translation company what can be maximum length of message.
	maximumCharacters?: number;
	//Alternatives messages based on condition
	alternatives?: Record<string, LanguageTranslationAlternative>;
};

//Trancorder

export type LocalTrancorderOptions = CommonTrancorderOptions & {
	languages?: Record<string, LanguageTranslations>;
};

export type RemoteTrancorderOptions = CommonTrancorderOptions & {
	fetch: (language: string) => Promise<LanguageTranslations>;
};

export type CommonTrancorderOptions = {
	intl?: {
		use: boolean;
		datetime?: DateTimeFormat;
		number?: NumberFormat;
	};
	//basic
	boolean?: BasicBooleanFormat;
	string?: BasicStringFormat;
	datetime?: BasicDateTimeFormat;
	number?: BasicNumberFormat;
	array?: BasicArrayFormat;
	debug?: boolean;
	logger?: Logger;
};

export interface TrancorderApi {
	setLanguage(lang: string): Promise<void>;
}

//Message

export type ValueFormat =
	| BasicBooleanFormat
	| BasicStringFormat
	| BasicDateTimeFormat
	| BasicNumberFormat
	| BasicArrayFormat
	| DateTimeFormat
	| NumberFormat;
export type MessageValuesFormat = {
	[key: string]: ValueFormat;
};
export type MessageOptions = {
	formats?: MessageValuesFormat;
};

export type MessageValues = Record<string, string | number | boolean | object | undefined>;
export type SimpleMessage = string;
export type EnumMessage<T> = {
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-ignore
	[key in T[keyof T]]: SimpleMessage;
};

export interface Messages {
	t<T extends MessageValues>(text: SimpleMessage, values?: T, options?: MessageOptions): string;
	t<E extends object, T extends { enumValue: E[keyof E] } & MessageValues>(
		enumValues: E,
		text: SimpleMessage,
		values: T,
		options?: MessageOptions
	): string;
	t<E extends object, T extends { enumValue: E[keyof E] } & MessageValues>(
		enumValues: E,
		text: EnumMessage<E>,
		values: T,
		options?: MessageOptions
	): string;
}

//State

export type TrancorderState = {
	language: string | null;
	languages: Record<string, LanguageTranslations | undefined>;
	logger: Logger;
	intl: {
		use: boolean;
		datetime?: DateTimeFormat;
		number?: NumberFormat;
	};
	boolean: BasicBooleanFormat;
	string: BasicStringFormat;
	datetime: BasicDateTimeFormat;
	number: BasicNumberFormat;
	array: BasicArrayFormat;
	debug: boolean;
};

//Replacer

export type Replacer<T = unknown> = {
	regexp: RegExp;
	pointer: string;
	value: string;
	raw: T;
};

//Formats

export type DateTimeFormat = Intl.DateTimeFormatOptions;
export type NumberFormat = Intl.NumberFormatOptions;
export type BasicBooleanFormat = Formatter<boolean>;
export type BasicStringFormat = Formatter<string>;
export type BasicDateTimeFormat = Formatter<Date>;
export type BasicNumberFormat = Formatter<number>;
export type BasicArrayFormat = Formatter<Array<unknown>>;
export type Formatter<T> = (locale: string, value: T) => string;

//Logger

export type LoggerMessage = {
	message: string;
};
export type Logger = (type: LogMessageType, msg: LoggerMessage) => boolean;
export enum LogMessageType {
	Info = "info",
	Warning = "warning",
	Error = "error",
}
