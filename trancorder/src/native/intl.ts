import { Formatter, TrancorderState, ValueFormat } from "../types";

const IsIntl = typeof Intl !== "undefined";

export function formatDateTime(state: TrancorderState, value: Date, format?: ValueFormat): string {
	const language = state.language || "";
	return getCustomFormat(state, value, "date", format, () => {
		if (useIntl(state)) {
			return Intl.DateTimeFormat(language, state.intl.datetime).format(value);
		}
		return state.datetime(language, value);
	});
}

export function formatNumber(state: TrancorderState, value: number, format?: ValueFormat): string {
	const language = state.language || "";
	return getCustomFormat(state, value, "number", format, () => {
		if (useIntl(state)) {
			return Intl.NumberFormat(language, state.intl.number).format(value);
		}
		return state.number(language, value);
	});
}

export function formatString(state: TrancorderState, value: string, format?: ValueFormat): string {
	const language = state.language || "";
	return getCustomFormat(state, value, "string", format, () => {
		return state.string(language, value);
	});
}

export function formatBoolean(
	state: TrancorderState,
	value: boolean,
	format?: ValueFormat
): string {
	const language = state.language || "";
	return getCustomFormat(state, value, "boolean", format, () => {
		return state.boolean(language, value);
	});
}

export function formatArray(
	state: TrancorderState,
	value: Array<unknown>,
	format?: ValueFormat
): string {
	const language = state.language || "";
	return getCustomFormat(state, value, "array", format, () => {
		return state.array(language, value);
	});
}

function getCustomFormat<T>(
	state: TrancorderState,
	value: T,
	type: "date" | "number" | "string" | "boolean" | "array",
	format: ValueFormat | undefined,
	fn: () => string
) {
	const language = state.language || "";
	if (format && typeof format === "function") {
		const formatter = format as Formatter<T>;
		return formatter(language, value);
	}
	if (format && useIntl(state) && typeof format === "object" && type === "date") {
		return Intl.DateTimeFormat(language, state.intl.datetime).format(value as Date);
	}
	if (format && useIntl(state) && typeof format === "object" && type === "number") {
		return Intl.NumberFormat(language, state.intl.number).format(value as number);
	}
	return fn();
}

function useIntl(state: TrancorderState): boolean {
	return Boolean(IsIntl && state.intl && state.intl.use);
}
