import { Logger, LogMessageType } from "../types";

const DefaultLogger: Logger = (type, message) => {
	switch (type) {
		case LogMessageType.Info:
			console.info(message.message);
			break;
		case LogMessageType.Warning:
			console.warn(message.message);
			break;
		case LogMessageType.Error:
			console.error(message.message);
			break;
		default:
			console.log(message.message);
			break;
	}
	return true;
};

export function createLogger(debug: boolean, logger?: Logger | null): Logger {
	return (type, message) => {
		//silent in production mode
		if (!debug) {
			return true;
		}
		//log message
		if (logger && logger(type, message)) {
			return true;
		}
		return DefaultLogger(type, message);
	};
}
