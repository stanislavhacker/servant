import {
	Replacer,
	EnumMessage,
	LanguageTranslation,
	LanguageTranslationFull,
	LogMessageType,
	MessageOptions,
	MessageValues,
	SimpleMessage,
	TrancorderState,
} from "../types";
import { formatArray, formatBoolean, formatDateTime, formatNumber, formatString } from "../native";
import { evaluateExpression } from "./expressions";

export const THIS_MARKER = "this";
export const ENUM_VALUE_MARKER = "enumValue";

export function createT(state: TrancorderState, ...args): string {
	if (typeof args[0] === "string") {
		return createForString(state, args[0], args[1], args[2]);
	}
	if (typeof args[0] === "object" && typeof args[1] === "string") {
		return createForSimpleEnum(state, args[0], args[1], args[2], args[3]);
	}
	if (typeof args[0] === "object" && typeof args[1] === "object") {
		return createForComposedEnum(state, args[0], args[1], args[2], args[3]);
	}
	throw new Error(`Unsupported parameters count or types for "t" function.`);
}

function createForString<T extends MessageValues>(
	state: TrancorderState,
	text: SimpleMessage,
	values: T = {} as T,
	options: MessageOptions = {}
): string {
	const replacers = buildReplacers(state, [], values, options);
	return buildMessage(state, replacers, text);
}

function createForSimpleEnum<
	E extends object,
	T extends { [ENUM_VALUE_MARKER]: E[keyof E] } & MessageValues
>(
	state: TrancorderState,
	enumValues: E,
	text: SimpleMessage,
	values: T,
	options: MessageOptions = {}
): string {
	const { enumValue, ...restValues } = values;
	const replacers = buildReplacers(state, [], restValues, options);
	replacers.push({
		regexp: buildRegexp([THIS_MARKER]),
		pointer: THIS_MARKER,
		value: String(enumValue),
		raw: enumValue,
	});
	return buildMessage(state, replacers, text);
}

function createForComposedEnum<
	E extends object,
	T extends { [ENUM_VALUE_MARKER]: E[keyof E] } & MessageValues
>(
	state: TrancorderState,
	enumValues: E,
	text: EnumMessage<E>,
	values: T,
	options: MessageOptions = {}
): string {
	const { enumValue, ...restValues } = values;
	const enumText = text[enumValue] || "";
	const replacers = buildReplacers(state, [], restValues, options);
	return buildMessage(state, replacers, enumText);
}

function buildMessage(state: TrancorderState, replacers: Array<Replacer>, text: string) {
	const found = getLocalizedTextOrDefault(state, text);
	const localized = buildAlternative(state, replacers, found);

	//replace
	return replacers.reduce(
		(prev, { regexp, value }) => prev.replace(regexp, value),
		localized.value
	);
}

export function buildReplacers(
	state: TrancorderState,
	path: (string | number)[],
	values: MessageValues | Array<MessageValues[string]>,
	options: MessageOptions
): Array<Replacer> {
	const data: Array<Replacer> = [];

	if (Array.isArray(values)) {
		values.forEach((value, key) => {
			data.push(...buildReplacer(state, path, key, value, options));
		});
	} else {
		Object.keys(values).forEach((key) => {
			data.push(...buildReplacer(state, path, key, values[key], options));
		});
	}
	return data;
}

function buildReplacer(
	state: TrancorderState,
	path: (string | number)[],
	key: string | number,
	value: MessageValues[string],
	options: MessageOptions
): Array<Replacer> {
	const customFormats = options.formats || {};
	const data: Array<Replacer> = [];
	switch (typeof value) {
		case "object":
			if (value instanceof Date) {
				const pointer = buildPath([...path, key]);
				data.push({
					pointer: pointer,
					regexp: buildRegexp([...path, key]),
					value: formatDateTime(state, value, customFormats[pointer]),
					raw: value,
				});
			} else if (Array.isArray(value)) {
				const pointer = buildPath([...path, key]);
				data.push({
					pointer: pointer,
					regexp: buildRegexp([...path, key]),
					value: formatArray(state, value, customFormats[pointer]),
					raw: value,
				});
				data.push(
					...buildReplacers(
						state,
						[...path, key],
						value as unknown as MessageValues,
						options
					)
				);
			} else {
				data.push(
					...buildReplacers(state, [...path, key], value as MessageValues, options)
				);
			}
			break;
		case "number":
		case "bigint": {
			const pointer = buildPath([...path, key]);
			data.push({
				pointer: pointer,
				regexp: buildRegexp([...path, key]),
				value: formatNumber(state, value, customFormats[pointer]),
				raw: value,
			});
			break;
		}
		case "boolean": {
			const pointer = buildPath([...path, key]);
			data.push({
				pointer: pointer,
				regexp: buildRegexp([...path, key]),
				value: formatBoolean(state, value, customFormats[pointer]),
				raw: value,
			});
			break;
		}
		case "string": {
			const pointer = buildPath([...path, key]);
			data.push({
				pointer: pointer,
				regexp: buildRegexp([...path, key]),
				value: formatString(state, value, customFormats[pointer]),
				raw: value,
			});
			break;
		}
		//unsupported now
		case "symbol":
		case "function":
		case "undefined":
		default: {
			const pointer = buildPath([...path, key]);
			data.push({
				pointer: pointer,
				regexp: buildRegexp([...path, key]),
				value: "",
				raw: "",
			});
			break;
		}
	}
	return data;
}

function buildRegexp(path: (string | number)[]) {
	const replacer = buildPath(path)
		.replace(/\./g, "\\.")
		.replace(/\[/g, "\\[")
		.replace(/]/g, "\\]");
	return new RegExp(`\\{${replacer}\\}`, "g");
}

function buildPath(path: (string | number)[]): string {
	return path.reduce((prev, current, i) => {
		const part =
			i === 0
				? typeof current === "string"
					? `${current}`
					: `[${current}]`
				: typeof current === "string"
				? `.${current}`
				: `[${current}]`;
		return prev + part;
	}, "") as string;
}

function buildAlternative(
	state: TrancorderState,
	replacers: Array<Replacer>,
	localized: LanguageTranslationFull
): LanguageTranslationFull {
	const mappedValues = replacers.reduce(
		(prev, { pointer, raw }) => ({ ...prev, [pointer]: raw }),
		{} as Record<string, unknown>
	);
	const alternatives = localized.alternatives || {};
	const expressions = Object.keys(alternatives);

	let found: LanguageTranslationFull | null = null;
	for (let i = 0; i < expressions.length; i++) {
		const expression = expressions[i];
		const [result, errors] = evaluateExpression(expression, mappedValues);
		//no debug and first true, return
		if (result && !state.debug) {
			return convertToLanguageTranslationFull(alternatives[expression]);
		}
		//no debug and first true, save and continue
		if (result && state.debug) {
			found = found || convertToLanguageTranslationFull(alternatives[expression]);
		}
		//report errors
		errors.forEach((e) => {
			state.logger(LogMessageType.Error, {
				message: `Problem in expression "${expression}": ${e.message}`,
			});
		});
	}
	return found || localized;
}

function getLocalizedTextOrDefault(state: TrancorderState, text: string): LanguageTranslationFull {
	const selectedLanguage = state.languages[state.language ?? ""] || {};
	const value = selectedLanguage[text];

	//found
	if (value !== undefined) {
		return convertToLanguageTranslationFull(value);
	}
	//log default message and return
	state.logger(LogMessageType.Warning, {
		message: `Can not found localised message for "${text}", using this text as default.`,
	});
	return {
		value: text,
	};
}

function convertToLanguageTranslationFull(value: LanguageTranslation): LanguageTranslationFull {
	if (typeof value === "string") {
		return {
			value,
		};
	}
	//found, use it as object
	return value;
}
