import {
	LanguageTranslations,
	LogMessageType,
	RemoteTrancorderOptions,
	TrancorderState,
} from "../types";

export function setLanguageLocal(state: TrancorderState, language: string) {
	return new Promise<void>((resolve) => {
		setLanguage(state, language);
		resolve();
	});
}

export function setLanguageRemote(
	state: TrancorderState,
	fetch: RemoteTrancorderOptions["fetch"],
	language: string
) {
	return new Promise<void>((resolve) => {
		//already loaded
		if (state.languages[language]) {
			setLanguage(state, language);
			resolve();
			return;
		}
		//fetch language
		fetch(language)
			.then((data) => {
				setLanguages(state, language, data);
				setLanguage(state, language);
				resolve();
			})
			.catch(() => {
				setErrorLanguage(state, language);
				resolve();
			});
	});
}

function setLanguage(state: TrancorderState, language: string) {
	state.language = language;
	//no language exists for selected
	if (!state.languages[language]) {
		state.logger(LogMessageType.Error, {
			message: `Can not found messages for language "${language}".`,
		});
	}
}

function setLanguages(state: TrancorderState, language: string, data: LanguageTranslations) {
	state.languages[language] = data;
	//no language exists for selected
	if (Object.keys(data).length === 0 || !data) {
		state.logger(LogMessageType.Warning, {
			message: `Language data for language "${language}" are empty or not defined.`,
		});
	}
}

function setErrorLanguage(state: TrancorderState, language: string) {
	state.language = language;
	//error from server
	state.logger(LogMessageType.Error, {
		message: `Promise data call for "${language}" failed and not return any data.`,
	});
}
