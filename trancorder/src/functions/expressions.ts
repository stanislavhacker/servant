import { init, formula } from "expressionparser";

export function evaluateExpression(
	expression: string,
	replacers: Record<string, unknown>
): [boolean, Error[]] {
	let result = false;
	const errors: Array<Error> = [];
	try {
		const parser = init(formula, (term: string) => {
			const matches = /\{(.*)}/g.exec(term);
			if (matches) {
				const value = replacers[matches[1]];

				if (value !== undefined) {
					// eslint-disable-next-line @typescript-eslint/no-explicit-any
					return replacers[matches[1]] as any;
				}
				throw new Error(`Can not found value for variable "${term}".`);
			}
			throw new Error(`Unknown operator or term "${term}".`);
		});

		const value = parser.expressionToValue(expression);
		if (typeof value !== "boolean") {
			errors.push(new Error(`Result is not a BOOLEAN.`));
		} else {
			result = value;
		}
	} catch (err) {
		errors.push(err);
	}
	return [result, errors];
}
