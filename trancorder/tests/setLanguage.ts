import { setLanguageLocal, setLanguageRemote } from "../src/functions";
import { createTrancorderState } from "../src/state";
import { LanguageTranslations, LoggerMessage, LogMessageType, TrancorderState } from "../src/types";

describe("setLanguage function", () => {
	let state: TrancorderState;
	let logger: Array<[LogMessageType, LoggerMessage]>;

	beforeEach(() => {
		logger = [];
		state = createTrancorderState({
			debug: true,
			logger: (type, message) => {
				logger.push([type, message]);
				return true;
			},
		});
	});

	it("setLanguageLocal for existing language", async () => {
		state.languages["en-US"] = {};

		await setLanguageLocal(state, "en-US");
		expect(state.language).toBe("en-US");
		expect(logger).toEqual([]);
	});

	it("setLanguageLocal for non-existing language", async () => {
		await setLanguageLocal(state, "en-US");
		expect(state.language).toBe("en-US");
		expect(logger).toEqual([
			[LogMessageType.Error, { message: 'Can not found messages for language "en-US".' }],
		]);
	});

	it("setLanguageRemote for existing language", async () => {
		const fetcher = () =>
			new Promise<LanguageTranslations>((resolve) => {
				resolve({ test: "Data" });
			});

		await setLanguageRemote(state, fetcher, "en-US");
		expect(state.language).toBe("en-US");
		expect(state.languages["en-US"]).toEqual({ test: "Data" });
		expect(logger).toEqual([]);
	});

	it("setLanguageRemote for error call", async () => {
		const fetcher = () =>
			new Promise<LanguageTranslations>((resolve, reject) => {
				reject();
			});

		await setLanguageRemote(state, fetcher, "en-US");
		expect(state.language).toBe("en-US");
		expect(state.languages["en-US"]).toBe(undefined);
		expect(logger).toEqual([
			[
				LogMessageType.Error,
				{ message: 'Promise data call for "en-US" failed and not return any data.' },
			],
		]);
	});

	it("setLanguageRemote for already loaded language", async () => {
		const fetcher = () =>
			new Promise<LanguageTranslations>((resolve, reject) => {
				reject();
			});
		state.languages["en-US"] = { data: "Test" };

		await setLanguageRemote(state, fetcher, "en-US");
		expect(state.language).toBe("en-US");
		expect(state.languages["en-US"]).toEqual({ data: "Test" });
		expect(logger).toEqual([]);
	});
});
