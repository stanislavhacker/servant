import { localTrancorder, remoteTrancorder } from "../src/index";

describe("trancorder", () => {
	describe("localTrancorder", () => {
		it("create", async () => {
			const trancorder = localTrancorder({
				languages: {
					["en-US"]: { Test: "This is test value." },
				},
			});

			await trancorder.setLanguage("en-US");
			const text1 = trancorder.t("Test");
			expect(text1).toEqual("This is test value.");

			await trancorder.setLanguage("cs-CZ");
			const text2 = trancorder.t("Test");
			expect(text2).toEqual("Test");
		});
	});

	describe("remoteTrancorder", () => {
		it("create", async () => {
			const trancorder = remoteTrancorder({
				fetch: (lang) =>
					new Promise((resolve) => {
						if (lang === "en-US") {
							resolve({ Test: "This is test value." });
							return;
						}
						resolve({});
					}),
			});

			await trancorder.setLanguage("en-US");
			const text1 = trancorder.t("Test");
			expect(text1).toEqual("This is test value.");

			await trancorder.setLanguage("cs-CZ");
			const text2 = trancorder.t("Test");
			expect(text2).toEqual("Test");
		});
	});
});
