import { evaluateExpression } from "../src/functions";

describe("expression", () => {
	it("test simple expression without variables, 1 + 1 = 2", () => {
		const [result, errors] = evaluateExpression("1 + 1 = 2", {});
		expect(result).toBe(true);
		expect(errors).toEqual([]);
	});

	it("test simple expression with existing variables, 1 + {count} = 2", () => {
		const [result, errors] = evaluateExpression("1 + {count} = 2", {
			count: 1,
		});
		expect(result).toBe(true);
		expect(errors).toEqual([]);
	});

	it("test simple expression with non existing variables, 1 + {count} = 2", () => {
		const [result, errors] = evaluateExpression("1 + {count} = 2", {});
		expect(result).toBe(false);
		expect(errors).toEqual([new Error(`Can not found value for variable "{count}".`)]);
	});

	it("test simple expression with non existing operator, 1 + #test = 2", () => {
		const [result, errors] = evaluateExpression("1 + #test = 2", {});
		expect(result).toBe(false);
		expect(errors).toEqual([new Error(`Unknown operator or term "#test".`)]);
	});

	it("test simple expression with other than boolean result, 1 + 2", () => {
		const [result, errors] = evaluateExpression("1 + 2", {});
		expect(result).toBe(false);
		expect(errors).toEqual([new Error(`Result is not a BOOLEAN.`)]);
	});

	it("test simple expression with invalid expression, 1 + 2 + AND", () => {
		const [result, errors] = evaluateExpression("1 + 2 + AND", {});
		expect(result).toBe(false);
		expect(errors).toEqual([new Error(`Parse Error: unexpected EOF`)]);
	});
});
