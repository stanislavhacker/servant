import { createLogger } from "../src/native/logger";
import { LoggerMessage, LogMessageType } from "../src";

describe("logger function", () => {
	describe("default console logger", () => {
		let logged: Array<string[]>;
		let customLogged: Array<[string, LoggerMessage]>;

		beforeEach(() => {
			logged = [];
			customLogged = [];
			spyOn(console, "log").and.callFake((...args) => {
				logged.push(["log", ...args]);
			});
			spyOn(console, "error").and.callFake((...args) => {
				logged.push(["error", ...args]);
			});
			spyOn(console, "warn").and.callFake((...args) => {
				logged.push(["warn", ...args]);
			});
			spyOn(console, "info").and.callFake((...args) => {
				logged.push(["info", ...args]);
			});
		});

		it("debug is off, no messages", () => {
			const logger = createLogger(false);

			logger(LogMessageType.Info, { message: "This is info message." });
			logger(LogMessageType.Warning, { message: "This is warning message." });
			logger(LogMessageType.Error, { message: "This is error message." });
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			logger("test" as any, { message: "This is log message." });

			expect(logged).toEqual([]);
		});

		it("debug is on, messages", () => {
			const logger = createLogger(true);

			logger(LogMessageType.Info, { message: "This is info message." });
			logger(LogMessageType.Warning, { message: "This is warning message." });
			logger(LogMessageType.Error, { message: "This is error message." });
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			logger("test" as any, { message: "This is log message." });

			expect(logged).toEqual([
				["info", "This is info message."],
				["warn", "This is warning message."],
				["error", "This is error message."],
				["log", "This is log message."],
			]);
		});

		it("debug is on, custom logger, no skip", () => {
			const logger = createLogger(true, (type, msg) => {
				customLogged.push([type, msg]);
				return false;
			});

			logger(LogMessageType.Info, { message: "This is info message." });
			logger(LogMessageType.Warning, { message: "This is warning message." });
			logger(LogMessageType.Error, { message: "This is error message." });
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			logger("test" as any, { message: "This is log message." });

			expect(logged).toEqual([
				["info", "This is info message."],
				["warn", "This is warning message."],
				["error", "This is error message."],
				["log", "This is log message."],
			]);

			expect(customLogged).toEqual([
				["info", { message: "This is info message." }],
				["warning", { message: "This is warning message." }],
				["error", { message: "This is error message." }],
				["test", { message: "This is log message." }],
			]);
		});

		it("debug is on, custom logger, skip", () => {
			const logger = createLogger(true, (type, msg) => {
				customLogged.push([type, msg]);
				return true;
			});

			logger(LogMessageType.Info, { message: "This is info message." });
			logger(LogMessageType.Warning, { message: "This is warning message." });
			logger(LogMessageType.Error, { message: "This is error message." });
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			logger("test" as any, { message: "This is log message." });

			expect(logged).toEqual([]);

			expect(customLogged).toEqual([
				["info", { message: "This is info message." }],
				["warning", { message: "This is warning message." }],
				["error", { message: "This is error message." }],
				["test", { message: "This is log message." }],
			]);
		});
	});
});
