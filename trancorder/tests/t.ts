import { createT } from "../src/functions";
import { createTrancorderState } from "../src/state";
import {
	EnumMessage,
	MessageOptions,
	MessageValues,
	SimpleMessage,
	TrancorderState,
} from "../src/types";

describe("t function", () => {
	let state: TrancorderState;

	function createT_String<T extends MessageValues>(
		text: SimpleMessage,
		values: T = {} as T,
		options: MessageOptions = {}
	) {
		return createT(state, text, values, options);
	}

	function createT_EnumString<
		E extends object,
		T extends { enumValue: E[keyof E] } & MessageValues
	>(enumValues: E, text: SimpleMessage, values: T, options: MessageOptions = {}) {
		return createT(state, enumValues, text, values, options);
	}
	function createT_EnumObject<
		E extends object,
		T extends { enumValue: E[keyof E] } & MessageValues
	>(enumValues: E, text: EnumMessage<E>, values: T, options: MessageOptions = {}) {
		return createT(state, enumValues, text, values, options);
	}

	describe("default options, no intl, default formats", () => {
		beforeEach(() => {
			state = createTrancorderState({});
		});

		describe("simple string case", () => {
			it("simple message | This is simple message.", () => {
				const message = createT_String("This is simple message.");
				expect(message).toBe("This is simple message.");
			});

			it("simple message with param | My name is {name}!", () => {
				const message = createT_String("My name is {name}!", { name: "Standa" });
				expect(message).toBe("My name is Standa!");
			});

			it("simple message with non existing param | My name is {name}!", () => {
				const message = createT_String("My name is {name}!", {});
				expect(message).toBe("My name is {name}!");
			});

			it("simple message with multiple use of param | My name is {name}! {name}!", () => {
				const message = createT_String("My name is {name}! {name}!", { name: "Standa" });
				expect(message).toBe("My name is Standa! Standa!");
			});

			it("simple message with multiple params | Hello {who}! My name is {name}!", () => {
				const message = createT_String("Hello {who}! My name is {name}!", {
					who: "Frank",
					name: "Standa",
				});
				expect(message).toBe("Hello Frank! My name is Standa!");
			});

			it("message for string with default format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: "Test" });
				expect(message).toBe("This is value Test!");
			});

			it("message for number with default format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: 10 });
				expect(message).toBe("This is value 10!");
			});

			it("message for boolean with default format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: true });
				expect(message).toBe("This is value true!");
			});

			it("message for Date with default format | This is value {value}!", () => {
				const date = new Date();
				const message = createT_String("This is value {value}!", { value: date });
				expect(message).toBe(`This is value ${date.toString()}!`);
			});

			it("message for unsupported with default format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: undefined });
				expect(message).toBe("This is value !");
			});

			it("message for array with default format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: [1, 2, 3] });
				expect(message).toBe("This is value 1, 2, 3!");
			});

			it("message for array item with default format | This is value {value}!", () => {
				const message = createT_String("This is value {value[1]}!", { value: [1, 2, 3] });
				expect(message).toBe("This is value 2!");
			});
		});

		describe("enum text case", () => {
			enum TestEnum {
				Variable = "variable",
				Plain = "plain",
				Json = "json",
			}

			it("simple enum message | This is enum value {this}.", () => {
				const message = createT_EnumString(TestEnum, "This is enum value {this}.", {
					enumValue: TestEnum.Variable,
				});
				expect(message).toBe("This is enum value variable.");
			});

			it("simple enum message with another params | This is enum value {this} and name {name}.", () => {
				const message = createT_EnumString(
					TestEnum,
					"This is enum value {this} and name {name}.",
					{ enumValue: TestEnum.Variable, name: "Standa" }
				);
				expect(message).toBe("This is enum value variable and name Standa.");
			});
		});

		describe("enum object case", () => {
			enum TestEnum {
				Variable = "variable",
				Plain = "plain",
				Json = "json",
			}

			it("simple enum message | This is enum value {this}.", () => {
				const message = createT_EnumObject(
					TestEnum,
					{
						[TestEnum.Variable]: "This is variable controlled item.",
						[TestEnum.Json]: "This is item controlled by json.",
						[TestEnum.Plain]: "This is plain content item.",
					},
					{ enumValue: TestEnum.Variable }
				);
				expect(message).toBe("This is variable controlled item.");
			});
		});
	});

	describe("default options, no intl, custom formats", () => {
		beforeEach(() => {
			state = createTrancorderState({
				boolean: (_, val) => (val ? "YES" : "NO"),
				number: (_, val) => String(val * 100),
				string: (_, val) => "[[" + val + "]]",
				array: (_, val) => val.join(" - "),
				datetime: (_, val) => val.toISOString(),
			});
		});

		describe("simple string case", () => {
			it("message for string with defined format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: "Test" });
				expect(message).toBe("This is value [[Test]]!");
			});

			it("message for number with defined format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: 10 });
				expect(message).toBe("This is value 1000!");
			});

			it("message for boolean with defined format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: true });
				expect(message).toBe("This is value YES!");
			});

			it("message for Date with defined format | This is value {value}!", () => {
				const date = new Date();
				const message = createT_String("This is value {value}!", { value: date });
				expect(message).toBe(`This is value ${date.toISOString()}!`);
			});

			it("message for array with defined format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: [1, 2, 3] });
				expect(message).toBe("This is value 1 - 2 - 3!");
			});

			it("message for array item with defined format | This is value {value}!", () => {
				const message = createT_String("This is value {value[1]}!", { value: [1, 2, 3] });
				expect(message).toBe("This is value 200!");
			});
		});
	});

	describe("default options, use intl, custom formats", () => {
		beforeEach(() => {
			state = createTrancorderState({
				boolean: (_, val) => (val ? "YES" : "NO"),
				number: (_, val) => String(val * 100),
				string: (_, val) => "[[" + val + "]]",
				array: (_, val) => val.join(" - "),
				datetime: (_, val) => val.toISOString(),
				intl: {
					use: true,
					number: {
						style: "currency",
						currency: "EUR",
					},
					datetime: {
						minute: "numeric",
						hour: "numeric",
						dayPeriod: "short",
					},
				},
			});
			state.language = "en-US";
		});

		describe("simple string case", () => {
			it("message for string with defined format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: "Test" });
				expect(message).toBe("This is value [[Test]]!");
			});

			it("message for number with defined intl format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: 10 });
				expect(message).toBe("This is value €10.00!");
			});

			it("message for boolean with defined format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: true });
				expect(message).toBe("This is value YES!");
			});

			it("message for Date with defined intl format | This is value {value}!", () => {
				const date = new Date(1990, 0, 1, 12, 13, 22, 0);
				const message = createT_String("This is value {value}!", { value: date });
				expect(message).toBe(`This is value 12:13 in the afternoon!`);
			});

			it("message for array with defined format | This is value {value}!", () => {
				const message = createT_String("This is value {value}!", { value: [1, 2, 3] });
				expect(message).toBe("This is value 1 - 2 - 3!");
			});

			it("message for array item with defined intl format | This is value {value}!", () => {
				const message = createT_String("This is value {value[1]}!", { value: [1, 2, 3] });
				expect(message).toBe("This is value €2.00!");
			});
		});
	});

	describe("using alternatives", () => {
		beforeEach(() => {
			state = createTrancorderState({});
		});

		it("no alternatives at all", () => {
			state.language = "en-US";
			state.languages[state.language] = {
				"This is simple message.": {
					value: "This is simple message.",
					alternatives: {},
				},
			};
			const message = createT_String("This is simple message.");
			expect(message).toBe("This is simple message.");
		});

		it("alternatives based on {count}", () => {
			state.language = "en-US";
			state.languages[state.language] = {
				"There are {count} persons.": {
					value: "There are {count} persons.",
					alternatives: {
						"{count} = 0": "There are no persons.",
						"{count} = 1": "There is {count} person.",
					},
				},
			};

			const message1 = createT_String("There are {count} persons.", { count: 2 });
			expect(message1).toBe("There are 2 persons.");

			const message2 = createT_String("There are {count} persons.", { count: 1 });
			expect(message2).toBe("There is 1 person.");

			const message3 = createT_String("There are {count} persons.", { count: 0 });
			expect(message3).toBe("There are no persons.");
		});
	});
});
