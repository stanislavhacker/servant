import { mockFs } from "./mock-fs";

export function createDirectory(): [() => void, Array<string>] {
	const clean = mockFs({
		// /content/sources
		"/content": {
			sources: {
				"index.ts": `/**
 * This is testing function
 */
export function testing() {
    console.log("This is function");
}`,
				"index.tsx": `/**
 * This is testing function
 */
export function testing() {
    console.log("This is function");
}`,
				"index.less": `//This is main definition
.test {
	.inner {
		display: block;
		//needs to be forced
		font-size: 10px;
	}
}`,
				"index.sass": `//This is main definition
.test {
	.inner {
		display: block;
		//needs to be forced
		font-size: 10px;
	}
}`,
				"index.css": `//This is main definition
.test 	.inner {
	display: block;
	//needs to be forced
	font-size: 10px;
}`,
				"main.ts": `export function testing() {
    console.log("This is function");
}`,
			},
		},
	});

	return [
		clean,
		[
			"/content/sources/index.ts",
			"/content/sources/index.tsx",
			"/content/sources/index.less",
			"/content/sources/index.sass",
			"/content/sources/index.css",
			"/content/sources/main.ts",
		],
	];
}
