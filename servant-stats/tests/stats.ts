import { stats } from "../src/index";
import { createDirectory } from "./projects";

describe("stats", () => {
	let clean, files;

	beforeAll(() => {
		[clean, files] = createDirectory();
	});

	afterAll(() => {
		clean();
	});

	it("only ts file", async () => {
		const data = await stats([files[0]]);

		expect(data.files).toEqual({
			"/content/sources/index.ts": {
				stats: { size: 102, type: 2 },
				lines: { all: 6, meaningful: 3 },
				file: "/content/sources/index.ts",
			},
		});
		expect(data.lines).toEqual({
			".ts": { all: 6, meaningful: 3 },
		});
	});

	it("only tsx file", async () => {
		const data = await stats([files[1]]);

		expect(data.files).toEqual({
			"/content/sources/index.tsx": {
				stats: { size: 102, type: 2 },
				lines: { all: 6, meaningful: 3 },
				file: "/content/sources/index.tsx",
			},
		});
		expect(data.lines).toEqual({
			".tsx": { all: 6, meaningful: 3 },
		});
	});

	it("only less file", async () => {
		const data = await stats([files[2]]);

		expect(data.files).toEqual({
			"/content/sources/index.less": {
				stats: { size: 108, type: 2 },
				lines: { all: 8, meaningful: 6 },
				file: "/content/sources/index.less",
			},
		});
		expect(data.lines).toEqual({
			".less": { all: 8, meaningful: 6 },
		});
	});

	it("only sass file", async () => {
		const data = await stats([files[3]]);

		expect(data.files).toEqual({
			"/content/sources/index.sass": {
				stats: { size: 108, type: 2 },
				lines: { all: 8, meaningful: 6 },
				file: "/content/sources/index.sass",
			},
		});
		expect(data.lines).toEqual({
			".sass": { all: 8, meaningful: 6 },
		});
	});

	it("only css file", async () => {
		const data = await stats([files[4]]);

		expect(data.files).toEqual({
			"/content/sources/index.css": {
				stats: { size: 100, type: 2 },
				lines: { all: 6, meaningful: 4 },
				file: "/content/sources/index.css",
			},
		});
		expect(data.lines).toEqual({
			".css": { all: 6, meaningful: 4 },
		});
	});

	it("multiple files", async () => {
		const data = await stats(files);

		expect(data.lines).toEqual({
			".ts": { all: 9, meaningful: 6 },
			".tsx": { all: 6, meaningful: 3 },
			".less": { all: 8, meaningful: 6 },
			".sass": { all: 8, meaningful: 6 },
			".css": { all: 6, meaningful: 4 },
		});
	});

	it("multiple files but no lines", async () => {
		const data = await stats(files, { countLines: false });

		expect(data.lines).toEqual({
			".ts": { all: 0, meaningful: 0 },
			".tsx": { all: 0, meaningful: 0 },
			".less": { all: 0, meaningful: 0 },
			".sass": { all: 0, meaningful: 0 },
			".css": { all: 0, meaningful: 0 },
		});
	});
});
