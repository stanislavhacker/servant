export enum StatsType {
	Unknown,
	Directory,
	File,
	Link,
}

export type FileStats = {
	size: number;
	type: StatsType;
};

export type FileLines = {
	all: number;
	meaningful: number;
};

export type StatsOptions = {
	countLines?: boolean;
};
export type Stats = {
	files: Record<string, OneStat>;
	lines: Record<string, FileLines>;
};
export type OneStat = {
	file: string;
	stats: FileStats;
	lines: FileLines;
};
