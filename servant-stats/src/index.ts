import { fileStats } from "./stats";
import { extLines, fileLines } from "./lines";

import { FileLines, FileStats, OneStat, Stats, StatsOptions } from "./types";

export { FileLines, FileStats, OneStat, StatsOptions, Stats };

export function stats(
	files: Array<string>,
	{ countLines = true }: StatsOptions = {}
): Promise<Stats> {
	return resolveStats(files, { countLines }).then((files) => {
		const lines = extLines(files);
		return {
			files,
			lines,
		};
	});
}

function resolveStats(
	files: Array<string>,
	opts: Required<StatsOptions>
): Promise<Record<string, OneStat>> {
	return loadStats(files, opts).then((stats) => mapResults(stats));
}

function loadStats(files: Array<string>, opts: Required<StatsOptions>) {
	return Promise.all(files.map((file) => fileStats(file)))
		.then((stats) => mapStats(files, stats))
		.then((stats) =>
			Promise.all(
				files.map(
					(file) =>
						new Promise<OneStat>((resolve) => {
							Promise.all([
								//load special stats
								fileLines(stats, file, opts.countLines),
							]).then(([lines]: [FileLines]) => {
								resolve({
									file,
									lines,
									stats: stats[file],
								});
							});
						})
				)
			)
		);
}

function mapStats(files: Array<string>, stats: Array<FileStats>): Record<string, FileStats> {
	return stats.reduce((prev, stat, index) => {
		return { ...prev, [files[index]]: stat };
	}, {} as Record<string, FileStats>);
}

function mapResults(files: Array<OneStat>): Record<string, OneStat> {
	return files.reduce((prev, { file, stats, lines }) => {
		return { ...prev, [file]: { stats, lines, file } };
	}, {} as Record<string, OneStat>);
}
