import * as strip from "strip-comments";
import * as fs from "fs";
import * as path from "path";
import * as readline from "readline";
import { Readable } from "stream";

import { FileLines, FileStats, OneStat } from "./types";

export function fileLines(
	stats: Record<string, FileStats>,
	file: string,
	on: boolean
): Promise<FileLines> {
	if (!on) {
		return Promise.resolve(createFileLines());
	}
	return new Promise((resolve, reject) => {
		formatAndRemoveComment(file).then(countLines).then(resolve).catch(reject);
	});
}

export function extLines(files: Record<string, OneStat>): Record<string, FileLines> {
	return Object.keys(files).reduce((data, file) => {
		const ext = path.extname(file);
		const lines = (data[ext] = data[ext] || createFileLines(0, 0));

		const all = files[file].lines.all;
		if (all >= 0) {
			lines.all += all;
		}

		const meaningful = files[file].lines.meaningful;
		if (meaningful >= 0) {
			lines.meaningful += meaningful;
		}
		return data;
	}, {} as Record<string, FileLines>);
}

function formatAndRemoveComment(file: string): Promise<{ raw: string; content: string }> {
	return new Promise((resolve) => {
		getFileContent(file)
			.then((raw) => resolve({ raw, content: removeComment(raw) }))
			.catch(() => resolve({ raw: "", content: "" }));
	});
}

function removeComment(data: string): string {
	try {
		return strip(data);
	} catch (_e) {
		return data;
	}
}

function countLines({ raw, content }: { raw: string; content: string }): Promise<FileLines> {
	return new Promise((resolve) =>
		Promise.all([getFileLines(raw), getFileLines(content)]).then(([all, meaningful]) => {
			resolve(createFileLines(all, meaningful));
		})
	);
}

const wsRegex = /^\s+|\s+$/g;
function getFileLines(content: string): Promise<number> {
	return new Promise((resolve) => {
		const input = Readable.from(content);
		const lineReader = readline.createInterface({ input });

		let all = 0;
		lineReader.on("line", (line) => {
			if (line.replace(wsRegex, "").length > 0) {
				all++;
			}
		});
		lineReader.on("close", () => {
			input.destroy();
			resolve(all);
		});
	});
}

function getFileContent(file: string): Promise<string> {
	return new Promise((resolve, reject) => {
		const stats = fs.statSync(file);
		//skip dir
		if (stats.isDirectory()) {
			resolve("");
			return;
		}
		fs.readFile(file, (err, data) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(data.toString());
		});
	});
}

function createFileLines(all = -1, meaningful = -1): FileLines {
	return { all, meaningful };
}
