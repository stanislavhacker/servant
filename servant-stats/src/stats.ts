import * as fs from "fs";
import { FileStats, StatsType } from "./types";

export function fileStats(file: string): Promise<FileStats> {
	return new Promise((resolve) => {
		fs.stat(file, (err, s) => {
			const stats = s as fs.Stats | null;
			const fileStats = createFileStats(StatsType.Unknown, stats?.size);

			if (stats?.isDirectory()) {
				fileStats.type = StatsType.Directory;
			}
			if (stats?.isFile()) {
				fileStats.type = StatsType.File;
			}
			if (stats?.isSymbolicLink()) {
				fileStats.type = StatsType.Link;
			}

			resolve(fileStats);
		});
	});
}

function createFileStats(type = StatsType.Unknown, size = 0): FileStats {
	return {
		size,
		type,
	};
}
