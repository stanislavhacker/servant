# ![Servant][logo] Servant stats module

Quick references: **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**, **[dev-server][5]**

## What is it?

This module is for internal usage in servant. Can receive file stats base on files paths. Is
expected that file paths are loaded before with some glob pattern module or something similar.

### Simple usage

```typescript
import { stats } from "@servant/servant-stats";

async function loadStats() {
	const data = stats(files);

	console.log(data);
}
```

```json5
{
	files: {
		"/content/sources/index.css": {
			file: "/content/sources/index.css",

			//object stats
			// type 0 => unknown
			// type 1 => directory
			// type 2 => file
			// type 3 => link
			stats: { size: 100, type: 2 },

			//options: countLines = true
			//sum count of lines for file
			//  all => all files
			//  meaningful => non empty liens and lines that are not part of comments
			lines: { all: 6, meaningful: 4 },
		},
	},
	lines: {
		//options: countLines = true
		//sum count of lines based on extension
		".css": { all: 6, meaningful: 4 },
	},
}
```

### option: **`countLines`**, default value `true`

If this options is turned on, stats also count lines in files. Default value is `true` so module automatically count of
lines for every provided file. Can be turned off. Returned object has still lines, but are not filled and for file return
`-1` as lines count.

```typescript
const data = stats(files, { countLines: false });
```

### License

[Licensed under GPLv3][license]

[Playwright][playwright] is licensed under [Apache-2.0][license2]

[logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
[playwright]: https://www.npmjs.com/package/playwright
[ms]: https://github.com/microsoft
[1]: https://docs.npmjs.com/files/package.json
[2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
[3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
[4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
[5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-development/doc/servant.devserver.md
[license]: https://gitlab.com/stanislavhacker/servant/blob/master/LICENSE
[license2]: https://github.com/microsoft/playwright/blob/master/LICENSE
[devices]: https://github.com/Microsoft/playwright/blob/master/src/deviceDescriptors.ts
