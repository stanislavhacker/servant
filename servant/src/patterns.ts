import { PackageJson, ServantJson } from "@servant/servant-data";
import { Extensions, JS, LESS, Path, SASS, TS } from "@servant/servant-files";
import * as path from "path";
import * as glob from "glob";

import { extract } from "./libraries/npm";

export function loadPatterns(
	entry: string,
	relatives: Array<string>,
	what?: string
): Promise<Array<string>> {
	return new Promise((fulfill) => {
		const all: Array<Promise<Array<string>>> = [];
		const files: Array<string> = [];

		relatives.forEach((module) => {
			const pattern = Path.normalize(path.join(entry, module, what || ""));

			all.push(
				new Promise((fulfill) => {
					//find all
					glob(pattern, { dot: true }, (err, files) =>
						fulfill(files.map((file) => Path.normalize(file)))
					);
				})
			);
		});

		Promise.all(all).then((data) => {
			data.forEach((fls) => {
				files.push(...fls);
			});

			fulfill([...new Set(files)]);
		});
	});
}

export function loadResources(entry: string, relatives: Array<string>): Promise<Array<string>> {
	return new Promise((fulfill) => {
		const [files, npms] = extract(relatives);

		loadPatterns(entry, files).then((resourceFiles) => {
			fulfill([...resourceFiles, ...npms]);
		});
	});
}

export function loadWatched(files: Array<string>, clean: Array<string> = []): Array<string> {
	return files.filter((file, index, array) => {
		//check clean
		if (Extensions.matchedOne(file, clean)) {
			return false;
		}
		//do not process d.ts file
		if (TS.isDTS(file)) {
			return false;
		}
		//do not process js.map file
		if (TS.isMAP(file)) {
			return false;
		}

		//skip js file if exists ts or tsx file
		const tsFiles = TS.fromJs(file);
		if (tsFiles && tsFiles.some((file) => array.indexOf(file) >= 0)) {
			return false;
		}

		//skip css file if exists less file
		const lessFile = LESS.fromCss(file);
		if (lessFile && array.indexOf(lessFile) >= 0) {
			return false;
		}

		//skip css file if exists scss file
		const sassFile = SASS.fromCss(file);
		if (sassFile && array.indexOf(sassFile) >= 0) {
			return false;
		}
		//FILTERED!
		return true;
	});
}

export function loadCleaned(files: Array<string>): Array<string> {
	return files.filter((file, index, array) => {
		//always clean js.map file
		if (TS.isMAP(file)) {
			return true;
		}
		//clean d.ts file if there is ts or tsx file
		let tsFiles = TS.fromDTS(file);
		if (tsFiles && tsFiles.some((file) => array.indexOf(file) >= 0)) {
			return true;
		}

		//clean js file if there is ts or tsx file
		tsFiles = TS.fromJs(file);
		if (tsFiles && tsFiles.some((file) => array.indexOf(file) >= 0)) {
			return true;
		}

		//clean css file if there is less file
		const lessFile = LESS.fromCss(file);
		if (lessFile && array.indexOf(lessFile) >= 0) {
			return true;
		}

		//clean css file if there is sass file
		const sassFile = SASS.fromCss(file);
		if (sassFile && array.indexOf(sassFile) >= 0) {
			return true;
		}
		//KEEP!
		return false;
	});
}

export function loadValidated(files: Array<string>, extensions: Array<string>): Array<string> {
	return files.filter((file, index, array) => {
		//do not validate map files
		if (TS.isMAP(file)) {
			return false;
		}

		//do not validate d.ts file if there is ts or tsx file
		let tsFiles = TS.fromDTS(file);
		if (tsFiles && tsFiles.some((file) => array.indexOf(file) >= 0)) {
			return false;
		}

		//do not validate js file if there is ts or tsx file
		tsFiles = TS.fromJs(file);
		if (tsFiles && tsFiles.some((file) => array.indexOf(file) >= 0)) {
			return false;
		}

		//no matched provided extensions
		if (!Extensions.matchedOne(file, [...TS.EXT, ...JS.EXT, ...extensions])) {
			return false;
		}

		//VALIDATE!
		return true;
	});
}

export function loadPrettify(files: Array<string>, extensions: Array<string>): Array<string> {
	return files.filter((file) => {
		//skip all paths with node_modules, its probably error glob
		if (file.match(/node_modules/)) {
			return false;
		}
		//no matched provided extensions
		if (extensions.length > 0 && !Extensions.matchedOne(file, extensions)) {
			return false;
		}
		//PRETTIFY!
		return true;
	});
}

export function loadModuleDirectories(
	files: Array<string>,
	servantJson?: ServantJson.ServantJsonInfo
): Array<string> {
	if (!servantJson) {
		return [];
	}

	const sj = servantJson.content;
	const ignored = [
		Path.patterns.everywhere("node_modules"),
		normalizePath(servantJson.cwd, sj.generators),
		normalizePath(servantJson.cwd, sj.temp),
		normalizePath(servantJson.cwd, sj.output?.directory),
	].filter(Boolean) as Array<string>;

	return files.filter((file) => !Path.matchedOne(file, ignored));
}

export function loadPackageJsonFiles(
	packageJson?: PackageJson.PackageJsonInfo
): Promise<{ cwd: string; files: Array<string> }> {
	if (!packageJson) {
		return Promise.resolve({ cwd: "", files: [] });
	}

	const files = PackageJson.files(packageJson);
	const cwd = packageJson.cwd;

	//files defined
	const patterns =
		files.length === 0
			? ["**/*"]
			: files.reduce((prev, file) => {
					if (!path.extname(file)) {
						return [...prev, file, Path.patterns.all(file)];
					}
					return [...prev, file];
			  }, [] as Array<string>);

	return loadPatterns(cwd, patterns).then((files) => {
		return {
			files: files.map((file) => path.relative(cwd, file)),
			cwd,
		};
	});
}

export function loadServantJsonFiles(
	servantJson?: ServantJson.ServantJsonInfo
): Promise<{ cwd: string; sources: Array<string>; tests: Array<string> }> {
	if (!servantJson) {
		return Promise.resolve({ cwd: "", sources: [], tests: [] });
	}

	const cwd = servantJson.cwd;
	return Promise.all([
		loadPatterns(cwd, servantJson.content.src).then((files) =>
			loadWatched(files, servantJson.content.clean)
		),
		loadPatterns(cwd, servantJson.content.tests).then((files) =>
			loadWatched(files, servantJson.content.clean)
		),
	]).then(([sources, tests]) => {
		return {
			cwd,
			sources: sources.map((file) => path.relative(cwd, file)),
			tests: tests.map((file) => path.relative(cwd, file)),
		};
	});
}

function normalizePath(root: string, pth?: string) {
	if (!pth) {
		return null;
	}
	return Path.normalize(path.join(root, pth));
}
