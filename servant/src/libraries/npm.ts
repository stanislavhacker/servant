import { PackageJson, ServantJson } from "@servant/servant-data";
import * as os from "os";
import { ChildProcess, spawn } from "child_process";

import { parse } from "./console";
import { resolveModule } from "../resolver";
import * as path from "path";
import { createTime } from "../utils";

export interface NpmResult {
	error: Error | null;
	data: NpmData | null;
	status: NpmStatus;
	command: string;
	time: string;
}

export enum NpmStatus {
	OK = "ok",
	Error = "error",
}

export interface NpmData {
	audited: number;
	added: number;
	removed: number;
	vulnerabilities: number;
	package: string | null;
	published: string | null;
}

export function install(
	packageJson: PackageJson.PackageJsonInfo,
	registryInfo: ServantJson.RegistryInfo | null = null
): Promise<NpmResult> {
	const params = ["install"];

	params.push(...applyRegistry(registryInfo));
	return npm(packageJson, params);
}

export function update(
	packageJson: PackageJson.PackageJsonInfo,
	registryInfo: ServantJson.RegistryInfo | null = null
): Promise<NpmResult> {
	const params = ["update"];

	params.push(...applyRegistry(registryInfo));
	return npm(packageJson, params);
}

export function pack(packageJson: PackageJson.PackageJsonInfo): Promise<NpmResult> {
	return npm(packageJson, ["pack"]);
}

export function publish(
	packageJson: PackageJson.PackageJsonInfo,
	tag = "latest",
	access: "public" | "restricted" = "public",
	registryInfo: ServantJson.RegistryInfo | null = null
): Promise<NpmResult> {
	const params = ["publish"];

	params.push(...["--tag", tag]);
	params.push(...["--access", access]);
	params.push(...applyRegistry(registryInfo));
	//params.push(...["--dry-run"]);
	return npm(packageJson, params);
}

function applyRegistry(registryInfo: ServantJson.RegistryInfo | null): Array<string> {
	const params: Array<string> = [];

	if (!registryInfo) {
		return params;
	}

	if (registryInfo.registry) {
		params.push(...[`--registry`, `"${registryInfo.registry}"`]);
	}
	Object.keys(registryInfo.scopes).forEach((scope) => {
		params.push(...[`--${scope}:registry`, `"${registryInfo.scopes[scope]}"`]);
	});
	return params;
}

function npm(packageJson: PackageJson.PackageJsonInfo, params: Array<string>): Promise<NpmResult> {
	return new Promise((resolve) => {
		const start = process.hrtime();
		const npmProcess = spawnNpm(packageJson, params);
		const npmResult = createNpmResult(`npm ${params.join(" ")}`);

		let ok = "";
		let error = "";

		npmProcess.stdout?.on("data", (data) => {
			ok += data.toString();
		});
		npmProcess.stderr?.on("data", (data) => {
			error += data.toString();
		});

		npmProcess.on("close", (code) => {
			if (code === 0) {
				npmResult.status = NpmStatus.OK;
				npmResult.data = createNpmData(parse(ok));
			} else {
				npmResult.status = NpmStatus.Error;
				npmResult.error = createError(parse(error));
			}

			npmResult.time = createTime(start);
			resolve(npmResult);
		});
	});
}

function spawnNpm(packageJson: PackageJson.PackageJsonInfo, params: Array<string>): ChildProcess {
	const npm = os.platform() === "win32" ? "npm.cmd" : "npm";
	return spawn(npm, params, {
		cwd: packageJson.cwd,
	});
}

export function createNpmResult(command: string): NpmResult {
	return {
		error: null,
		data: null,
		command: command,
		status: NpmStatus.Error,
		time: "0s 0ms",
	};
}

const ErrorCode = /npm ERR! code ([A-Za-z]{0,})/g;
const ErrorMessage = /npm ERR! [A-Za-z]{0,} (.*)/g;

function createError(lines: Array<string>): Error {
	let errorName = "";
	let errorMsg = "";

	lines.forEach((line) => {
		const errorCode = ErrorCode.exec(line);
		const errorMessage = ErrorMessage.exec(line);

		if (errorCode) {
			errorName = errorCode[1];
		} else if (errorMessage) {
			errorMsg += errorMessage[1] + " ";
		}

		ErrorCode.lastIndex = 0;
		ErrorMessage.lastIndex = 0;
	});

	return new Error(`${errorName}: ${errorMsg}`);
}

const AuditedCount = /audited ([0-9]{0,})/g;
const AddedCount = /added ([0-9]{0,})/g;
const RemovedCount = /removed ([0-9]{0,})/g;
const VulnerabilitiesCount = /found ([0-9]{0,}) vulnerabilities/g;
const PackagePattern = /(.*\.tgz)/g;
const PublishedPattern = /\+ (.*@.*)/g;

function createNpmData(lines: Array<string>): NpmData {
	let auditedCount = "0";
	let addedCount = "0";
	let removedCount = "0";
	let vulnerabilitiesCount = "0";
	let packg: string | null = null;
	let published: string | null = null;

	lines.forEach((line) => {
		let res: RegExpExecArray | null;

		res = AuditedCount.exec(line);
		if (res) {
			auditedCount = (res || [])[1] || "0";
		}

		res = AddedCount.exec(line);
		if (res) {
			addedCount = (res || [])[1] || "0";
		}

		res = RemovedCount.exec(line);
		if (res) {
			removedCount = (res || [])[1] || "0";
		}

		res = VulnerabilitiesCount.exec(line);
		if (res) {
			vulnerabilitiesCount = (res || [])[1] || "0";
		}

		res = PackagePattern.exec(line);
		if (res) {
			packg = (res || [])[1] || null;
		}

		res = PublishedPattern.exec(line);
		if (res) {
			published = (res || [])[1] || null;
		}

		AuditedCount.lastIndex = 0;
		AddedCount.lastIndex = 0;
		RemovedCount.lastIndex = 0;
		VulnerabilitiesCount.lastIndex = 0;
		PackagePattern.lastIndex = 0;
		PublishedPattern.lastIndex = 0;
	});

	return {
		package: packg,
		published: published,
		audited: parseInt(auditedCount, 10),
		added: parseInt(addedCount, 10),
		removed: parseInt(removedCount, 10),
		vulnerabilities: parseInt(vulnerabilitiesCount, 10),
	};
}

const npmPrefix = "npm:";
const npmSufix = "//";

export function extract(files: Array<string>): [Array<string>, Array<string>] {
	const rest: Array<string> = [];
	const npms = files
		.filter((file) => {
			if (isNpm(file)) {
				return true;
			}
			rest.push(file);
			return false;
		})
		.map((file) => {
			return npmToPath(file) || file;
		});

	return [rest, npms];
}

function isNpm(file: string) {
	return file.indexOf(npmPrefix) === 0;
}

function npmToPath(npmPath: string): string | null {
	const parts = npmPath.split(npmSufix);
	const module = (parts[0] || "").replace(npmPrefix, "");
	const filename = path.join("./", parts[1] || "");
	const resolved = resolveModule(__dirname, module, filename, true);

	return resolved.path || null;
}
