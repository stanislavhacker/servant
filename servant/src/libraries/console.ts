// eslint-disable-next-line no-control-regex
const ConsoleClean = /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g;

export function parse(data: string): Array<string> {
	let lines = data.replace(ConsoleClean, "").replace(/[\r]/g, "").split(/[\n]/g);

	// noinspection JSCheckFunctionSignatures
	lines = lines.map((line) => line.trim()).filter((line) => Boolean(line));

	return lines;
}
