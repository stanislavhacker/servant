import { CommandResult } from "./commands";

export function createCommandResult<T>(data: T): CommandResult<T> {
	return {
		time: "0s 0ms",
		data,
	};
}

export function createTime(start: [number, number]): string {
	const end = process.hrtime(start);

	return `${end[0]}s ${end[1] / 1000000}ms`;
}

export function withProgress<R>(result: R, progress: (result: R) => void): R {
	progress(result);
	return result;
}
