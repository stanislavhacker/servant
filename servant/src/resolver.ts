import { Path } from "@servant/servant-files";

import * as path from "path";
import * as fs from "fs";

export type ResolveResult = {
	path: string;
	err: Error | null;
};

export function resolveModule(
	cwd: string,
	module: string,
	file = "",
	absolute = false
): ResolveResult {
	let fullPath = resolveFromModule(cwd, module);
	fullPath = fullPath || resolveFromPath(cwd, module);

	if (fullPath) {
		const filePath = path.join(fullPath, file);

		//check if filepath exists
		if (fs.existsSync(filePath)) {
			//return error with path
			return {
				err: null,
				path: absolute ? filePath : path.relative(cwd, filePath),
			};
		}
	}
	//return error with path
	return {
		err: new Error(`Can not found module for ${module}! Something go wrong with Servant :(`),
		path: "",
	};
}

export function resolveModulePath(module: string, paths?: Array<string>): ResolveResult {
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-ignore
	const requireFn = typeof __webpack_require__ === "function" ? __non_webpack_require__ : require;
	return {
		err: null,
		path: requireFn.resolve(module, { paths }),
	};
}

export function resolveNodeModulePath(
	cwd: string,
	module: string,
	absolute = false
): ResolveResult {
	const fullPath = path.join(cwd, "node_modules", module);

	//check if filepath exists
	if (fs.existsSync(fullPath)) {
		//return error with path
		return {
			err: null,
			path: absolute ? fullPath : path.relative(cwd, fullPath),
		};
	}
	//return error with path
	return {
		err: new Error(`Can not found module for ${module}! Something go wrong with Servant :(`),
		path: "",
	};
}

function resolveFromPath(cwd: string, module: string): string | null {
	const paths: Array<string> = [];
	let where = cwd;

	//Make files to top of path
	do {
		paths.unshift(path.join(where, "node_modules", "$$path").replace("$$path", module));
		where = path.join(where, "../");
	} while (paths[0] !== paths[1]);

	paths.shift();

	for (let i = paths.length - 1; i >= 0; i--) {
		if (fs.existsSync(paths[i])) {
			return paths[i];
		}
	}
	return null;
}

function resolveFromModule(cwd: string, module: string): string | null {
	try {
		const resolved = resolveModulePath(module, [cwd, __dirname]);
		const file = Path.normalize(resolved.path);

		const searchNamespace = `/${module}/`;
		const searchNormal = `/${module.replace("@", "")}/`;

		if (file.indexOf(searchNamespace) >= 0) {
			return file.substring(0, file.indexOf(searchNamespace) + searchNamespace.length);
		}
		if (file.indexOf(searchNormal) >= 0) {
			return file.substring(0, file.indexOf(searchNormal) + searchNormal.length);
		}
		return null;
	} catch (e) {
		return null;
	}
}
