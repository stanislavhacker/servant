import { Path } from "@servant/servant-files";
import { ServantJson } from "@servant/servant-data";
import * as path from "path";
import { URL } from "url";

import { invariant } from "./invariant";
import { DependenciesGraph } from "./module";
import { InitData, ModulesData } from "./types";

export type EntriesOnly = Omit<ServantJson.ServerSettings, "css" | "js">;

export type ResolvedEntries = {
	client: EntriesOnly;
	server: EntriesOnly;
};

export function resolveEntries(init: InitData, modulesData: ModulesData): ResolvedEntries {
	invariant(
		init.servantJson,
		"Servant json is not defined in InitData. This is probably error in Servant."
	);
	invariant(modulesData.graph, "Servant dependencies graph is not defined in command() call.");

	const entry = init.entry;
	const root: EntriesOnly = { entries: init.servantJson.content.server.entries };

	const modules = getServerEntries(modulesData.graph);
	const remappedModules = getRemappedModules(modules, entry);
	const mergedModules = getMergedServer(root, remappedModules);

	return getResolvedEntries(modulesData.graph, mergedModules);
}

type Entries = Record<string, { server: EntriesOnly; cwd: string }>;

function getServerEntries(graph: DependenciesGraph) {
	return graph.all.reduce((prev, name) => {
		const module = graph.modules[name];

		if (module.module) {
			prev[module.name] = {
				server: module.module.servantJson.content.server,
				cwd: module.module.servantJson.cwd,
			};
		}
		return prev;
	}, {} as Entries);
}

function getRemappedModules(modules: Entries, entry: string) {
	return Object.keys(modules).reduce((prev, key) => {
		const module = modules[key];

		prev[key] = {
			entries: Object.keys(module.server.entries).reduce((prev, entryKey) => {
				const entryItem = module.server.entries[entryKey];
				prev[entryKey] = {
					...remapTitle(entryItem),
					...remapTemplate(entryItem, entry, module),
					...remapCommand(entryItem, entry, module),
					...remapShared(entryItem, entry, module),
				};
				return prev;
			}, {} as EntriesOnly["entries"]),
		};
		return prev;
	}, {} as Record<string, EntriesOnly>);
}

function remapTitle(entryItem: ServantJson.ServerEntry) {
	return entryItem.title ? { title: entryItem.title } : {};
}

function remapTemplate(entryItem: ServantJson.ServerEntry, entry: string, module: Entries[string]) {
	return entryItem.template
		? { template: updatePath(entry, module.cwd, entryItem.template) }
		: {};
}

function remapCommand(entryItem: ServantJson.ServerEntry, entry: string, module: Entries[string]) {
	return entryItem.command
		? {
				command: {
					...entryItem.command,
					...remapCommandFile(entryItem.command, entry, module),
				},
		  }
		: {};
}

function remapCommandFile(
	commandItem: NonNullable<ServantJson.ServerEntry["command"]>,
	entry: string,
	module: { server: EntriesOnly; cwd: string }
) {
	return commandItem.file
		? {
				file: updatePath(entry, module.cwd, commandItem.file),
		  }
		: {};
}

function remapShared(entryItem: ServantJson.ServerEntry, entry: string, module: Entries[string]) {
	return entryItem.shared
		? {
				shared: Object.keys(entryItem.shared).reduce((shared, folderPath) => {
					const moduleName = entryItem.shared && entryItem.shared[folderPath];
					//absolute paths and urls are not allowed
					if (path.isAbsolute(folderPath) || isUrl(folderPath)) {
						return;
					}
					if (moduleName) {
						shared[updatePath(entry, module.cwd, folderPath)] = moduleName;
					}
					return shared;
				}, {} as NonNullable<ServantJson.ServerEntry["shared"]>),
		  }
		: {};
}

function getMergedServer(root: EntriesOnly, remappedModules: Record<string, EntriesOnly>) {
	return Object.keys(remappedModules).reduce((prev, key) => {
		const module = remappedModules[key] as EntriesOnly;
		return {
			entries: getMergeEntries(prev.entries, module.entries),
		};
	}, root);
}

function getMergeEntries(
	previous: ServantJson.ServerSettings["entries"],
	current: ServantJson.ServerSettings["entries"]
) {
	return Object.keys(current).reduce((previous, key) => {
		const prevModule = previous[key] || {};

		previous[key] = {
			...prevModule,
			...current[key],
		};
		return previous;
	}, previous);
}

function getResolvedEntries(graph: DependenciesGraph, root: EntriesOnly): ResolvedEntries {
	const server: ResolvedEntries["server"] = {
		entries: filterTargetModules(graph, root.entries, [ServantJson.ModuleTarget.node]),
	};

	const client: ResolvedEntries["client"] = {
		entries: filterTargetModules(graph, root.entries, [
			ServantJson.ModuleTarget.web,
			ServantJson.ModuleTarget.web_page,
		]),
	};

	return {
		server,
		client,
	};
}

function filterTargetModules(
	graph: DependenciesGraph,
	entries: ServantJson.ServerSettings["entries"],
	targets: ServantJson.ModuleTarget[]
) {
	return Object.keys(entries).reduce((prev, module) => {
		const moduleTarget =
			graph.modules[module].module?.servantJson.content.target ??
			ServantJson.ServantJsonDefaults.target;
		if (moduleTarget && targets.includes(moduleTarget)) {
			prev[module] = entries[module];
		}
		return prev;
	}, {} as ServantJson.ServerSettings["entries"]);
}

function updatePath(entry: string, modulePath: string, filePath: string) {
	if (path.isAbsolute(filePath) || isUrl(filePath)) {
		return filePath;
	}

	const relativePath = path.relative(entry, modulePath);
	return Path.normalize(path.join(relativePath, filePath));
}

function isUrl(url: string) {
	try {
		new URL(url);
		return true;
	} catch (err) {
		return false;
	}
}
