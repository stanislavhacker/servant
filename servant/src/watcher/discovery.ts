import {
	createDiscoveryServer,
	DiscoveryInstance,
	DiscoveryServerApi,
} from "@servant/servant-discovery";
import { PackageJson, ServantJson } from "@servant/servant-data";
import * as events from "events";
import { DoneType } from "../module/index";

export { DiscoveryInstance };

export interface DiscoveryEmitter extends events.EventEmitter {
	on(event: "error", listener: (error: Error) => void): this;
	on(event: "ready" | "close" | "ended", listener: (instance: DiscoveryInstance) => void): this;
	on(
		event: "servant-info",
		listener: (instance: DiscoveryInstance, modules: ServantInfoData) => void
	): this;
	on(
		event: "servant-build",
		listener: (instance: DiscoveryInstance, modules: ServantBuildData) => void
	): this;
	on(
		event: "servant-build-status",
		listener: (instance: DiscoveryInstance, modules: ServantBuildStatusData) => void
	): this;
	emit(
		event:
			| "error"
			| "ready"
			| "close"
			| "ended"
			| "servant-info"
			| "servant-build"
			| "servant-build-status",
		...args: unknown[]
	): boolean;
	build(instance: DiscoveryInstance, modules: Array<string>): BuildJob;
	status(instance: DiscoveryInstance, status: ServantBuildStatusData): void;
}

export type BuildJob = {
	jobId: string;
	modules: Array<string>;
	type: DoneType | null;
	done: boolean;
};

export function createDiscovery(
	packageJson: PackageJson.PackageJsonInfo,
	serverJson: ServantJson.ServantJsonInfo
) {
	const emitter = new events.EventEmitter() as DiscoveryEmitter;

	const discovery = createDiscoveryServer({
		onData: (instance, data) => {
			processData(emitter, instance, data as string);
		},
		onDisconnect: (instance) => {
			emitter.emit("ended", instance);
		},
		onConnect: (instance) => {
			//On connect, send graph data to connected instance
			sendData(
				discovery,
				createServantData<ServantInfoData>("modules", {
					servantJson: serverJson.path,
					packageJson: packageJson.path,
				}),
				[instance]
			);
		},
		onStart: (instance) => emitter.emit("ready", instance),
		onClose: (instance) => emitter.emit("close", instance),
		onError: (err) => emitter.emit("error", err),
	});

	emitter.build = (instance, modules) => {
		const jobId = Math.random().toString(36).slice(2);
		//Send data to build
		sendData(
			discovery,
			createServantData<ServantBuildData>("build", {
				jobId,
				modules,
			}),
			[instance]
		);

		return {
			jobId,
			modules,
			done: false,
			type: null,
		};
	};
	emitter.status = (instance, status) => {
		const { jobId, type } = status;

		//Send data to status
		sendData(
			discovery,
			createServantData<ServantBuildStatusData>("status", {
				jobId,
				type,
			}),
			[instance]
		);
	};

	return emitter;
}

//DATA

type ServantData = {
	name: "modules" | "build" | "status";
	payload: string;
};

function createServantData<T>(name: ServantData["name"], payload: T): ServantData {
	return { name, payload: JSON.stringify(payload) };
}

function sendData(
	discovery: DiscoveryServerApi,
	data: unknown,
	instances?: Array<DiscoveryInstance>
) {
	discovery.send(JSON.stringify(data), instances);
}

function parseData(data: string) {
	try {
		const servantData = JSON.parse(data) as ServantData;

		if (servantData && servantData.name && servantData.payload) {
			return servantData;
		}
		return null;
	} catch (_e) {
		return null;
	}
}

function processData(emitter: DiscoveryEmitter, instance: DiscoveryInstance, data: string) {
	const servantData = parseData(data);

	if (!servantData) {
		return;
	}

	switch (servantData && servantData.name) {
		case "modules": {
			const data = JSON.parse(servantData.payload);
			emitter.emit("servant-info", instance, data as ServantInfoData);
			break;
		}
		case "build": {
			const data = JSON.parse(servantData.payload);
			emitter.emit("servant-build", instance, data as ServantBuildData);
			break;
		}
		case "status": {
			const data = JSON.parse(servantData.payload);
			emitter.emit("servant-build-status", instance, data as ServantBuildStatusData);
			break;
		}
		default:
			break;
	}
}

//DATA: info

export type ServantInfoData = {
	packageJson: string;
	servantJson: string;
};

//DATA: build

export type ServantBuildData = {
	jobId: string;
	modules: Array<string>;
};

//DATA: status

export type ServantBuildStatusData = {
	jobId: string;
	type: DoneType;
};
