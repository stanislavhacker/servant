import * as path from "path";
import { ServantJson } from "@servant/servant-data";

import { ChangeBy } from "../vcs";
import { BuildResult } from "../module";
import { command, init, modules } from "../main";
import { InitData, ModulesData } from "../types";
import { CommandResult, DoneType } from "../module";

import { ServantBuildStatusData, BuildJob, ServantInfoData } from "./discovery";
import { changedWatched, updateWatched, ChangeType } from "./watcher";
import {
	createWatcherChange,
	createWatcherInfo,
	ChangeHandler,
	WatcherInfo,
	WatcherChange,
	WatcherDiscovery,
	WatcherSettings,
} from "./types";
import { createWatcherState, WatchRemoteInfo } from "./state";
import { linkSymlinks, unlinkAllSymlinks, unlinkSymlinks } from "./symlinks";
import { invariant } from "../invariant";
import { createTime } from "../utils";

export { WatcherInfo, WatcherChange, WatcherDiscovery, WatcherSettings, ChangeHandler, BuildJob };

export function watch(
	initData: InitData,
	modulesData: ModulesData,
	changed: ChangeHandler,
	settings: WatcherSettings = {}
): Promise<void> {
	return new Promise(() => {
		const packageJson = initData.packageJson;
		const servantJson = initData.servantJson;

		invariant(
			packageJson,
			"Can not start watcher because initData are wrong. Package json or Servant json is empty."
		);
		invariant(
			servantJson,
			"Can not start watcher because initData are wrong. Package json or Servant json is empty."
		);

		const start = process.hrtime();
		const state = createWatcherState(packageJson, servantJson, modulesData);
		const { watcher, discovery } = state;

		//WATCHER
		watcher.on("add", (filepath) =>
			changedWatched(state, ChangeType.NewFile, filepath, changed, settings)
		);
		watcher.on("change", (filepath) =>
			changedWatched(state, ChangeType.ChangeFile, filepath, changed, settings)
		);
		watcher.on("unlink", (filepath) =>
			changedWatched(state, ChangeType.RemoveFile, filepath, changed, settings)
		);

		//DISCOVERY
		discovery.on("ready", (instance) => {
			state.instance = instance;
			changed(createWatcherInfo(state), createWatcherChange(false));
		});
		discovery.on("close", () => {
			state.remotes = {};
			state.instance = null;
			changed(createWatcherInfo(state), createWatcherChange(false));
		});
		//fill data and remove on disconnected
		discovery.on("servant-info", (instance, info) => {
			const remote: WatchRemoteInfo = { instance, info };

			//more watchers on same project running
			if (isMe(servantJson, info)) {
				return;
			}

			init(path.dirname(info.servantJson))
				.then((initData) => {
					remote.initData = initData;
					return modules(initData);
				})
				.then((modules) => {
					remote.modules = modules;
				})
				.catch((err) => {
					remote.err = err;
				})
				.finally(() => {
					state.remotes[instance.id] = remote;
					linkSymlinks(state, remote);
					changed(createWatcherInfo(state), createWatcherChange(false));
				});
		});
		discovery.on("servant-build", (instance, buildData) => {
			const status: ServantBuildStatusData = { jobId: buildData.jobId, type: DoneType.FAIL };
			modules(initData, {
				only: buildData.modules,
				changed: [ChangeBy.GIT, ChangeBy.FILESYSTEM],
			})
				.then((modules) =>
					command(initData, modules, "build", {
						progress: (res) => {
							const result = res as unknown as CommandResult<BuildResult>;
							status.type = result.type;
						},
						transpile: settings.transpile,
					})
				)
				.then(() => discovery.status(instance, status))
				.catch(() => discovery.status(instance, status));
		});
		discovery.on("servant-build-status", (instance, statusData) => {
			const job = state.jobs.find((job) => job.jobId === statusData.jobId);
			if (job) {
				job.type = statusData.type;
				job.done = true;
			}
			changed(createWatcherInfo(state), createWatcherChange(true));
		});
		discovery.on("ended", (instance) => {
			const remote = state.remotes[instance.id] || null;
			unlinkSymlinks(state, remote);
			delete state.remotes[instance.id];
			changed(createWatcherInfo(state), createWatcherChange(false));
		});

		//WATCHER: error and ready
		watcher.on("error", (err) => {
			changed(createWatcherInfo(state, err), createWatcherChange(false));
		});
		watcher.on("ready", () => {
			updateWatched(state, true).then(() => {
				state.time = createTime(start);
				state.ready = true;
				changed(createWatcherInfo(state), createWatcherChange(false));
			});
		});
		//DISCOVERY: error
		discovery.on("error", (err) => {
			changed(createWatcherInfo(state, err), createWatcherChange(false));
		});

		//PROCESS
		function exitHandler() {
			unlinkAllSymlinks(state);
		}
		function signalHandler(signal) {
			unlinkAllSymlinks(state);
			process.kill(process.pid, signal);
		}

		process.on("exit", exitHandler);
		process.on("SIGINT", signalHandler);
		process.on("SIGUSR1", signalHandler);
		process.on("SIGUSR2", signalHandler);
	});
}

function isMe(servantJson: ServantJson.ServantJsonInfo, info: ServantInfoData) {
	const currentServantJson = servantJson.path;
	const remoteServantJson = info.servantJson;

	return path.normalize(currentServantJson) === path.normalize(remoteServantJson);
}
