import { Modules, PackageJson } from "@servant/servant-data";
import { Path } from "@servant/servant-files";
import * as path from "path";

import { buildFiles } from "../module/build";
import { BuildResult, DependenciesGraph, DoneType } from "../module";
import { projectGlobs, WatcherState } from "./state";
import { createWatcherChange, createWatcherInfo, ChangeHandler, WatcherSettings } from "./types";
import { createTime } from "../utils";

//BUILD AND REBUILD

export function buildChanged(
	state: WatcherState,
	changed: Array<[string, -1 | 0 | 1, number]>,
	change: ChangeHandler,
	{ transpile = true }: WatcherSettings
): Promise<void> {
	return new Promise((resolve) => {
		//no changes
		if (changed.length === 0 || !state.modulesData?.graph) {
			resolve();
			return;
		}

		//change modules
		const changeModules = mapModules(state.modulesData.graph, changed);
		const start = process.hrtime();

		//changes
		change(createWatcherInfo(state), createWatcherChange(true));

		//build
		Promise.resolve()
			.then(() => buildExternal(state, changeModules, change))
			.then(() => buildInternal(state, changeModules, transpile))
			.then((results) => doneChanged(state, changeModules, results, start, change))
			.then(resolve);
	});
}

function buildInternal(
	state: WatcherState,
	changeModules: ChangedModules,
	transpile: boolean
): Promise<Array<BuildResult | null>> {
	return new Promise((resolve) => {
		const p: Array<Promise<BuildResult | null>> = [];
		Object.keys(changeModules).forEach((name) => {
			const module = changeModules[name];
			p.push(
				buildChangedModule(
					state.packageJson,
					module.module,
					module.changed.map(([path]) => path),
					transpile
				)
			);
		});

		Promise.all(p)
			.then(resolve)
			.catch(() => resolve([]));
	});
}

function doneChanged(
	state: WatcherState,
	changeModules: ChangedModules,
	results: Array<BuildResult | null>,
	start: [number, number],
	change: ChangeHandler
): Promise<void> {
	return new Promise((resolve) => {
		const watcherChanges = createWatcherChange(false);
		const modules = Object.keys(changeModules);
		const changed: Array<[string, -1 | 0 | 1, number]> = [];

		modules.forEach((module) => {
			changed.push(...changeModules[module].changed);
		});

		watcherChanges.time = createTime(start);
		watcherChanges.progress = false;
		watcherChanges.modules = modules;
		watcherChanges.files = changed;
		watcherChanges.results = results.filter((res) => Boolean(res)) as Array<BuildResult>;

		change(createWatcherInfo(state), watcherChanges);
		resolve();
	});
}

function buildChangedModule(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	changed: Array<string>,
	transpile: boolean
): Promise<BuildResult | null> {
	return new Promise((resolve) => {
		buildFiles(
			packageJson,
			module,
			{
				sources: changed,
				resources: [],
			},
			false,
			transpile
		)
			.then((results) => resolve(results))
			.catch(() => resolve(null));
	});
}

type ChangedModules = {
	[key: string]: ChangedModule;
};
type ChangedModule = {
	module: Modules.ModuleDefinition;
	changed: Array<[string, -1 | 0 | 1, number]>;
};

function mapModules(
	graph: DependenciesGraph,
	changed: Array<[string, -1 | 0 | 1, number]>
): ChangedModules {
	const modules = graph.modules;
	const changedModules: ChangedModules = {};

	Object.keys(modules).forEach((module) => {
		const moduleDefinition = modules[module];
		const moduleInfo = moduleDefinition.module;
		if (moduleInfo) {
			const patterns = projectGlobs(moduleInfo).map((pattern) =>
				path.join(moduleInfo.servantJson.cwd, pattern)
			);
			const changedFiles = changed.filter(([path]) => {
				return Path.matchedOne(path, patterns);
			});
			if (changedFiles.length > 0) {
				changedModules[moduleDefinition.name] = createChangedModule(
					moduleDefinition,
					changedFiles
				);
			}
		}
	});

	return changedModules;
}

function createChangedModule(
	moduleDefinition: Modules.ModuleDefinition,
	changedFiles: Array<[string, -1 | 0 | 1, number]>
) {
	return {
		module: moduleDefinition,
		changed: changedFiles,
	};
}

function buildExternal(
	state: WatcherState,
	changeModules: ChangedModules,
	change: ChangeHandler
): Promise<void> {
	return new Promise((resolve) => {
		const { remotes, instance, discovery } = state;
		const instances = Object.keys(remotes || {});

		//no discovery setup or empty
		if (!instance || instances.length === 0) {
			resolve();
			return;
		}

		const externalModules = getExternalModules(changeModules, []);
		const build = getBuildInstances(state, externalModules);

		//save jobs
		state.jobs = Object.keys(build).map((instanceId) => {
			const instance = remotes[instanceId];

			//instance has error
			if (instance.err) {
				return {
					instance: instance.instance,
					modules: [],
					jobId: "",
					type: DoneType.FAIL,
					done: true,
				};
			}

			const modules = build[instanceId];
			return {
				...discovery.build(instance.instance, modules),
				instance: instance.instance,
			};
		});

		//change
		change(createWatcherInfo(state), createWatcherChange(true));

		//run timers
		waitForDone(state, resolve);
		waitForTimeout(state, resolve);
	});
}

function waitForTimeout(state: WatcherState, done: () => void) {
	state.timeoutTimer = setInterval(() => {
		const allRunning = state.jobs.filter((job) => !job.done);
		allRunning.forEach((job) => {
			job.type = DoneType.FAIL;
			job.done = true;
		});
		clearTimers(state);
		done();

		//1 minute timeout
	}, 60 * 1000);
}

function waitForDone(state: WatcherState, done: () => void) {
	state.doneTimer = setInterval(() => {
		const allDone = state.jobs.every((job) => job.done);
		if (allDone) {
			clearTimers(state);
			done();
		}
	}, 100);
}

function clearTimers(state: WatcherState) {
	state.doneTimer && clearInterval(state.doneTimer);
	state.timeoutTimer && clearInterval(state.timeoutTimer);
	state.doneTimer = null;
	state.timeoutTimer = null;
}

function getBuildInstances(state: WatcherState, externalModules: Array<string>) {
	const { remotes } = state;
	const instances = Object.keys(remotes || {});

	return instances.reduce((prev, instanceId) => {
		const { modules } = remotes[instanceId];
		const found =
			modules && modules.graph
				? modules.graph.all.filter((name) => externalModules.indexOf(name) >= 0)
				: [];
		if (found.length > 0) {
			prev[instanceId] = found;
		}
		return prev;
	}, {} as Record<string, Array<string>>);
}

function getExternalModules(changeModules: ChangedModules, used: Array<string>) {
	return Object.keys(changeModules).reduce((prev, currentValue) => {
		const { module } = changeModules[currentValue];
		return [...prev, ...collectExternalModules(module.externals, used)];
	}, [] as Array<string>);
}

function collectExternalModules(externals: Modules.ModuleDefinition[], used: Array<string>) {
	return externals.reduce((prev, ext) => {
		if (used.indexOf(ext.name) >= 0) {
			return prev;
		}
		used.push(ext.name);
		return [...prev, ext.name, ...collectExternalModules(ext.externals, used)];
	}, []);
}
