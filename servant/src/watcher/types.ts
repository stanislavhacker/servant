import * as path from "path";
import { LinkResult, LinkThrow } from "devlink-core";

import { BuildResult } from "../module";
import { ModulesData } from "../types";

import { BuildJob, DiscoveryInstance } from "./discovery";
import { WatcherState } from "./state";

export interface WatcherInfo {
	ready: boolean;
	error: Error | null;
	time: string;
	date: Date;
	discovery: WatcherDiscovery;
	modulesData: ModulesData | null;
}
export interface WatcherChange {
	progress: boolean;
	time: string;
	modules: Array<string>;
	files: Array<[string, -1 | 0 | 1, number]>;
	results: Array<BuildResult>;
}
export interface WatcherDiscovery {
	instance: DiscoveryInstance | null;
	running: {
		id: string;
		port: number;
		entry: string;
		modules: Array<string>;
		job: BuildJob | null;
		results: { result: LinkResult | null; error: LinkThrow | null }[];
		err?: Error;
	}[];
}
export interface WatcherSettings {
	transpile?: boolean;
}

export type ChangeHandler = (info: WatcherInfo, change: WatcherChange) => void;

export function createWatcherInfo(state: WatcherState, error: Error | null = null): WatcherInfo {
	return {
		ready: state.ready,
		time: state.time,
		date: state.date,
		error: error,
		modulesData: state.modulesData,
		discovery: createWatcherDiscovery(state),
	};
}

export function createWatcherChange(progress: boolean): WatcherChange {
	return {
		progress: progress,
		time: "0s 0ms",
		modules: [],
		files: [],
		results: [],
	};
}

function createWatcherDiscovery(state: WatcherState): WatcherDiscovery {
	const { remotes, instance, jobs, links } = state;

	const running = Object.values(remotes).map(({ instance, modules, initData, err }) => {
		//get jobs
		const job = jobs.find((jb) => jb.instance.id === instance.id) || null;

		//get links
		const entry = path.normalize(initData?.entry ?? "");
		const results = Object.keys(links).reduce((results, link) => {
			const linkData = links[link];
			const useAsResult =
				!linkData.result || path.normalize(linkData.result.to).indexOf(entry) === 0;

			if (useAsResult) {
				results.push({
					result: linkData.result ? linkData.result : null,
					error: linkData.error ? linkData.error : null,
				});
			}

			return results;
		}, [] as { result: LinkResult | null; error: LinkThrow | null }[]);

		return {
			id: instance.id,
			port: instance.port,
			modules: modules ? modules.graph?.all ?? [] : [],
			results,
			entry,
			err,
			job,
		};
	});

	return {
		instance,
		running,
	};
}
