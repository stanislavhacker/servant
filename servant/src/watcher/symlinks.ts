import * as glob from "glob";
import { callback, LinkResult, LinkThrow, UnlinkResult } from "devlink-core";

import { DependenciesGraph, ModuleDefinition } from "../module";

import { WatcherState, WatchRemoteInfo } from "./state";

export function linkSymlinks(state: WatcherState, remote: WatchRemoteInfo | null) {
	if (!remote || !state.modulesData?.graph || !remote.modules || !remote.modules.graph) {
		return;
	}

	const localGraph = state.modulesData.graph;
	const remoteGraph = remote.modules.graph;

	const modules = collectExternalModules(localGraph, remoteGraph);
	const mapped = linkExternalModules(modules);

	//save state
	state.links = {
		...state.links,
		...mapped,
	};
}

export function unlinkSymlinks(state: WatcherState, remote: WatchRemoteInfo | null) {
	if (!remote || !state.modulesData?.graph || !remote.modules || !remote.modules.graph) {
		return;
	}

	const localGraph = state.modulesData.graph;
	const remoteGraph = remote.modules.graph;

	const modules = collectExternalModules(localGraph, remoteGraph);
	const unmapped = unlinkExternalModules(modules);

	//save state
	Object.keys(unmapped).forEach((key) => {
		delete state.links[key];
	});
}

export function unlinkAllSymlinks(state: WatcherState) {
	Object.keys(state.links).forEach((key) => {
		const link = state.links[key];
		if (link.result) {
			callback.unlink(link.result.on, () => void 0);
		}
		delete state.links[key];
	});
}

function collectExternalModules(localGraph: DependenciesGraph, remoteGraph: DependenciesGraph) {
	const localAll = localGraph.all;
	const remoteAll = remoteGraph.all;
	const remoteModules = remoteGraph.modules;

	return localAll.reduce((prev, module) => {
		const local = localGraph.modules[module];
		const remotes = local.externals.reduce((prev, external) => {
			return remoteAll.includes(external.name)
				? [...prev, remoteModules[external.name]]
				: prev;
		}, [] as ModuleDefinition[]);
		return {
			...prev,
			[local.name]: {
				local,
				remotes,
			},
		};
	}, {} as Record<string, { local: ModuleDefinition; remotes: ModuleDefinition[] }>);
}

function linkExternalModules(modules: ReturnType<typeof collectExternalModules>) {
	return Object.values(modules).reduce((prev, { local, remotes }) => {
		//no module definition
		if (!local.module) {
			return prev;
		}
		const cwd = local.module.packageJson.cwd;
		remotes.forEach(({ name, module }) => {
			if (module) {
				const files = glob.sync(`node_modules/${name}`, { cwd, absolute: true });
				const to = module.packageJson.cwd;
				files.forEach((on) => {
					//NOTE: Try remove previously created zombie link, if fails, no worries,
					// its look that link not exists
					callback.unlink(on, () => void 0);
					//create new symbolic link
					callback.link(on, to, (err, res) => {
						prev[on] = { result: res, error: err };
					});
				});
			}
		});
		return prev;
	}, {} as Record<string, { result: LinkResult | null; error: LinkThrow | null }>);
}

function unlinkExternalModules(modules: ReturnType<typeof collectExternalModules>) {
	return Object.values(modules).reduce((prev, { local, remotes }) => {
		//no module definition
		if (!local.module) {
			return prev;
		}
		const cwd = local.module.packageJson.cwd;
		remotes.forEach(({ name }) => {
			const files = glob.sync(`node_modules/${name}`, { cwd, absolute: true });
			files.forEach((on) => {
				callback.unlink(on, (err, res) => {
					prev[on] = { result: res, error: err };
				});
			});
		});
		return prev;
	}, {} as Record<string, { result: UnlinkResult | null; error: LinkThrow | null }>);
}
