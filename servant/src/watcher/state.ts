import { Path } from "@servant/servant-files";
import { PackageJson, ServantJson } from "@servant/servant-data";
import { LinkResult, LinkThrow } from "devlink-core";
import * as chokidar from "chokidar";
import * as path from "path";

import { ModuleInfo } from "../module";
import { InitData, ModulesData } from "../types";

import {
	BuildJob,
	createDiscovery,
	DiscoveryEmitter,
	DiscoveryInstance,
	ServantInfoData,
} from "./discovery";
import { createWatcher } from "./watcher";

export interface WatcherState {
	watcher: chokidar.FSWatcher;
	discovery: DiscoveryEmitter;
	//watcher
	ready: boolean;
	watched: Array<string>;
	crc: { [key: string]: number };
	update: NodeJS.Timer | null;
	files: WatcherStateFiles;
	packageJson: PackageJson.PackageJsonInfo;
	serverJson: ServantJson.ServantJsonInfo;
	modulesData: ModulesData | null;
	time: string;
	date: Date;
	//discovery
	instance: DiscoveryInstance | null;
	remotes: Record<string, WatchRemoteInfo>;
	jobs: (BuildJob & {
		instance: DiscoveryInstance;
	})[];
	links: Record<
		string,
		{
			result: LinkResult | null;
			error: LinkThrow | null;
		}
	>;
	doneTimer: NodeJS.Timer | null;
	timeoutTimer: NodeJS.Timer | null;
}
export interface WatcherStateFiles {
	add: Array<string>;
	change: Array<string>;
	remove: Array<string>;
}
export interface WatchRemoteInfo {
	instance: DiscoveryInstance;
	info: ServantInfoData;
	initData?: InitData;
	modules?: ModulesData;
	err?: Error;
}

export function createWatcherState(
	packageJson: PackageJson.PackageJsonInfo,
	serverJson: ServantJson.ServantJsonInfo,
	modulesData: ModulesData | null
): WatcherState {
	return {
		watcher: createWatcher(packageJson, serverJson),
		discovery: createDiscovery(packageJson, serverJson),
		packageJson: packageJson,
		serverJson: serverJson,
		crc: {},
		modulesData: modulesData,
		ready: false,
		watched: [],
		update: null,
		files: createWatcherStateFiles([], [], []),
		time: "0s 0ms",
		date: new Date(),
		instance: null,
		remotes: {},
		jobs: [],
		links: {},
		doneTimer: null,
		timeoutTimer: null,
	};
}

export function createWatcherStateFiles(
	add: Array<string>,
	change: Array<string>,
	remove: Array<string>
): WatcherStateFiles {
	return {
		add: add,
		change: change,
		remove: remove,
	};
}

//GLOBS

export function projectGlobs(module: ModuleInfo): Array<string> {
	const servantJson = module.servantJson;
	const packageJson = module.packageJson;
	return [
		...servantJson.content.src,
		...servantJson.content.tests,
		...servantJson.content.resources,
		...servantJson.content.watch,
		Path.normalize(path.relative(servantJson.cwd, packageJson.path)),
		Path.normalize(path.relative(servantJson.cwd, servantJson.path)),
	];
}
