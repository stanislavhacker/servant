import { PackageJson } from "@servant/servant-data";

import * as Module from "../module";
import { createCommandResult, createTime, withProgress } from "../utils";

import { CommandProgress, CommandResults } from "./index";

export function update(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	progress: (result: CommandProgress["update"]) => void
): Promise<CommandResults["update"]> {
	return new Promise((fulfill, reject) => {
		const result = createCommandResult<CommandResults["update"]["data"]>([]);
		const start = process.hrtime();
		const results: Module.CommandResult<Module.UpdateResult>[] = [];

		let p: Promise<unknown> = Promise.resolve();

		//create all
		Module.iterateSorted(graph.sorted).forEach((name) => {
			const module = graph.modules[name];

			p = p.then(() => {
				return Module.update(packageJson, module)
					.then((res) => withProgress(res, progress))
					.then((res) => results.push(res));
			});
		});
		//after done
		p.then(() => {
			result.time = createTime(start);
			result.data = [...results];
			fulfill(result);
		}).catch(reject);
	});
}
