import { PackageJson } from "@servant/servant-data";

import * as Module from "../module";
import { createCommandResult, createTime, withProgress } from "../utils";
import { ResolvedEntries } from "../entries";

import { CommandProgress, CommandResults } from "./index";

export function clean(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	progress: (result: CommandProgress["clean"]) => void,
	prune: boolean,
	entries: ResolvedEntries
): Promise<CommandResults["clean"]> {
	return new Promise((fulfill, reject) => {
		const result = createCommandResult<CommandResults["clean"]["data"]>([]);
		const cleans: Array<Promise<Module.CommandResult<Module.CleanResult>>> = [];
		const start = process.hrtime();

		//create all
		Module.iterateSorted(graph.sorted).forEach((name) => {
			const module = graph.modules[name];

			cleans.push(
				Module.clean(packageJson, module, prune, entries).then((res) =>
					withProgress(res, progress)
				)
			);
		});
		//after done
		Promise.all(cleans)
			.then((args) => {
				result.time = createTime(start);
				result.data = [...args];
				fulfill(result);
			})
			.catch(reject);
	});
}
