import { PackageJson } from "@servant/servant-data";

import * as Module from "../module";
import { createCommandResult, createTime, withProgress } from "../utils";

import { CommandProgress, CommandResults } from "./index";

export function build(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	progress: (result: CommandProgress["build"]) => void,
	production: boolean,
	transpile: boolean
): Promise<CommandResults["build"]> {
	return new Promise((fulfill, reject) => {
		const result = createCommandResult<CommandResults["build"]["data"]>([]);
		const start = process.hrtime();

		const schedule = Module.scheduleSorted(graph.sorted, (name: string) => {
			return Module.build(packageJson, graph.modules[name], production, transpile).then(
				(res) => withProgress(res, progress)
			);
		});
		//after done
		schedule
			.then((args) => {
				result.time = createTime(start);
				result.data = [...args];
				fulfill(result);
			})
			.catch(reject);
	});
}
