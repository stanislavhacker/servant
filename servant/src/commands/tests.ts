import { PackageJson } from "@servant/servant-data";

import * as Module from "../module";
import { createCommandResult, createTime } from "../utils";

import { CommandProgress, CommandResults } from "./index";

export function tests(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	progress: (result: CommandProgress["tests"]) => void,
	browsers?: Array<string>,
	devices?: Array<string>,
	gui?: boolean
): Promise<CommandResults["tests"]> {
	return new Promise((fulfill, reject) => {
		const result = createCommandResult<CommandResults["tests"]["data"]>([]);
		const start = process.hrtime();

		const tests: Array<Promise<Module.CommandResult<Module.TestingResults>>> = [];

		//create all
		Module.iterateSorted(graph.sorted).forEach((name) => {
			const module = graph.modules[name];

			tests.push(Module.tests(packageJson, module, progress, browsers, devices, gui));
		});
		//after done
		Promise.all(tests)
			.then((args) => {
				result.time = createTime(start);
				result.data = [...args];
				fulfill(result);
			})
			.catch(reject);
	});
}
