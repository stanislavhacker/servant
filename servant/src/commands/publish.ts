import { PackageJson, ServantJson } from "@servant/servant-data";

import * as Module from "../module";
import { createCommandResult, createTime, withProgress } from "../utils";

import { CommandProgress, CommandResults } from "./index";

export function publish(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	progress: (result: CommandProgress["publish"]) => void,
	production: boolean,
	tag?: string,
	freeze?: boolean,
	commit?: boolean,
	increment?: ServantJson.ServantJson["publish"]["increment"]
): Promise<CommandResults["publish"]> {
	return new Promise((fulfill, reject) => {
		const result = createCommandResult<CommandResults["publish"]["data"]>([]);
		const start = process.hrtime();

		const schedule = Module.scheduleSorted(graph.sorted, (name: string) => {
			return Module.publish(
				packageJson,
				graph.modules[name],
				graph,
				production,
				tag,
				freeze,
				commit,
				increment
			).then((res) => withProgress(res, progress));
		});
		//after done
		schedule
			.then((args) => {
				result.time = createTime(start);
				result.data = [...args];
				fulfill(result);
			})
			.catch(reject);
	});
}
