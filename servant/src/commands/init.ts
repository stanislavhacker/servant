import { PackageJson } from "@servant/servant-data";

import * as Module from "../module";
import { createCommandResult, createTime } from "../utils";

import { CommandResult } from "./index";

export function init(
	packageJson: PackageJson.PackageJsonInfo,
	entry: string,
	params: Module.InitParams
): Promise<CommandResult<Module.InitResults>> {
	return new Promise((fulfill, reject) => {
		const result = createCommandResult<Module.InitResults>({} as Module.InitResults);
		const start = process.hrtime();

		Module.init(packageJson, entry, params)
			.then((initResults) => {
				result.time = createTime(start);
				result.data = initResults;
				fulfill(result);
			})
			.catch(reject);
	});
}
