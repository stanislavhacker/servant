import { PackageJson } from "@servant/servant-data";

import * as Module from "../module";
import { createCommandResult, createTime, withProgress } from "../utils";

import { CommandProgress, CommandResults } from "./index";

export type AnalyzeSummaryResult = {
	modules: {
		[key: string]: Module.AnalyzeResult;
	};
	sorted: Array<string>;
	analyze: {
		missing: Array<string>;
		versions: Record<string, Array<string>>;
	};
};

export function analyze(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	progress: (result: CommandProgress["analyze"]) => void
): Promise<CommandResults["analyze"]> {
	return new Promise((fulfill, reject) => {
		const cache: Record<string, Module.AnalyzeResult> = {};
		const result = createCommandResult<CommandResults["analyze"]["data"]>(
			createAnalyzeSummaryResult([])
		);
		const start = process.hrtime();

		const schedule = Module.scheduleSorted(graph.sorted, (name: string) => {
			return Module.analyze(packageJson, graph, graph.modules[name], cache).then((res) =>
				withProgress(res, progress)
			);
		});
		//after done
		schedule
			.then((results) => {
				result.time = createTime(start);
				result.data = createAnalyzeSummaryResult(results);
				fulfill(result);
			})
			.catch(reject);
	});
}

function createAnalyzeSummaryResult(
	results: Array<Module.CommandResult<Module.AnalyzeResult>>
): AnalyzeSummaryResult {
	const sorted = results.map(({ module }) => module);
	const modules = results.reduce((prev, current) => {
		if (current.data) {
			return { ...prev, [current.module]: current.data };
		}
		return prev;
	}, {} as AnalyzeSummaryResult["modules"]);

	return {
		modules,
		sorted,
		analyze: analyzeResults(modules),
	};
}

function analyzeResults(modules: AnalyzeSummaryResult["modules"]): AnalyzeSummaryResult["analyze"] {
	const versions = {} as Record<string, Array<string>>;
	const missing = [] as Array<string>;

	const all = flattenTree(Object.keys(modules).map((name) => modules[name]));
	all.forEach((module) => {
		//multiple versions
		if (module.versions.ranges.length > 1) {
			versions[module.name] = [
				...new Set([...(versions[module.name] || []), ...module.versions.ranges]),
			];
		}
		//missing modules
		if (module.missing && missing.indexOf(module.name) === -1) {
			missing.push(module.name);
		}
	});

	return {
		missing,
		versions,
	};
}

function flattenTree(results: Module.AnalyzeResult[], arr: Module.AnalyzeResult[] = []) {
	results.forEach((result) => {
		if (arr.indexOf(result) === -1) {
			arr.push(result);
			arr.push(...result.dependencies);
		}
	});
	return arr;
}
