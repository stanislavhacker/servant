import { PackageJson } from "@servant/servant-data";

import * as Module from "../module";
import { createCommandResult, createTime, withProgress } from "../utils";

import { CommandProgress, CommandResults } from "./index";

export function validate(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	progress: (result: CommandProgress["validate"]) => void,
	fix: boolean
): Promise<CommandResults["validate"]> {
	return new Promise((fulfill, reject) => {
		const result = createCommandResult<CommandResults["validate"]["data"]>([]);
		const start = process.hrtime();

		const lints: Array<Promise<Module.CommandResult<Module.ValidateResult>>> = [];

		//create all
		Module.iterateSorted(graph.sorted).forEach((name) => {
			const module = graph.modules[name];

			lints.push(
				Module.validate(packageJson, module, fix).then((res) => withProgress(res, progress))
			);
		});
		//after done
		Promise.all(lints)
			.then((args) => {
				result.time = createTime(start);
				result.data = [...args];
				fulfill(result);
			})
			.catch(reject);
	});
}
