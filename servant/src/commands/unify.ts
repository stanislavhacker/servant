import { GeneratorAnswer, GeneratorResults } from "@servant/servant-generators";
import { PackageJson } from "@servant/servant-data";

import * as Module from "../module";
import {
	DoneType,
	UnifyResults,
	UnifiedVersionsInfo,
	ServantUnifyType,
	ServantUnifyProperty,
	createUnifyResults,
	getUnifyId,
} from "../module";
import { createCommandResult, createTime } from "../utils";

import { CommandProgress, CommandResult, CommandResults } from "./index";

export function unify(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	progress: (result: CommandProgress["unify"]) => void,
	unified: UnifiedVersionsInfo = {},
	latest = true
): Promise<CommandResults["unify"]> {
	return new Promise((fulfill, reject) => {
		const start = process.hrtime();
		const type = latest ? ServantUnifyType.Latest : ServantUnifyType.Provided;

		Module.unify(packageJson, graph, { unified, type })
			.then((result) => {
				//reject because of error
				if (result.errors.length) {
					reject(result.errors[0]);
					return;
				}
				//report all progress to modules
				result.modules.forEach(progress);
				//done
				fulfill(createUnifyResult(result, start));
			})
			.catch(reject);
	});
}

export function convertUnify(
	result: GeneratorResults<Module.UnifyCustomData>,
	start?: [number, number]
): CommandResult<Module.UnifiedVersionsInfo> & {
	modules: Record<string, Module.CommandResult<Module.UnifyResult>>;
} {
	const customData = result.customData || { results: [] };
	const moduleResults = createUnifyResults(customData, result.errors);

	const modules = moduleResults.modules.reduce((prev, data) => {
		prev[data.module] = data;
		return prev;
	}, {} as Record<string, Module.CommandResult<Module.UnifyResult>>);

	return {
		...createUnifyResult(moduleResults, start || process.hrtime()),
		modules,
	};
}

export function answersUnify({
	modules = {},
	latest = false,
}: {
	latest?: boolean;
	modules?: { [key: string]: string };
}): GeneratorAnswer[] {
	const answers: GeneratorAnswer[] = [];
	const packages = Object.keys(modules);

	//add type answer if latest or provided some packages
	if (latest || packages.length > 0) {
		answers.push({
			id: ServantUnifyProperty.Type,
			value: latest ? ServantUnifyType.Latest : ServantUnifyType.Provided,
		});
	}
	//provide type answers for provided modules
	if (packages.length > 0) {
		packages.forEach((packageName) => {
			const id = getUnifyId(packageName);
			answers.push({ id, value: modules[packageName] });
		});
	}

	return answers;
}

function createUnifyResult(results: UnifyResults, start: [number, number]) {
	const result = createCommandResult<CommandResults["unify"]["data"]>({});
	const { modules } = results;

	//done all
	result.time = createTime(start);
	result.data = modules.reduce((prev, current) => {
		if (current.data) {
			current.data.unified.forEach((data) => {
				if (data.type === DoneType.OK) {
					prev[data.package] = data.version;
				}
			});
		}
		return prev;
	}, {} as CommandResults["unify"]["data"]);

	return result;
}
