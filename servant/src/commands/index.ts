import { PackageJson, ServantJson } from "@servant/servant-data";

import { init } from "./init";
import { clean } from "./clean";
import { install } from "./install";
import { update } from "./update";
import { build } from "./build";
import { unify, answersUnify, convertUnify } from "./unify";
import { publish } from "./publish";
import { tests } from "./tests";
import { validate } from "./validate";
import { shared } from "./shared";
import { analyze, AnalyzeSummaryResult } from "./analyze";

import * as Module from "../module";
import { invariant } from "../invariant";
import { InitData, ModulesData } from "../types";
import { resolveEntries } from "../entries";

export type Commands =
	| "clean"
	| "install"
	| "update"
	| "build"
	| "unify"
	| "publish"
	| "tests"
	| "analyze"
	| "validate"
	| "shared";

export interface CommandResult<T> {
	time: string;
	data: T;
}

export type { AnalyzeSummaryResult };

export type CommandResults = {
	["build"]: CommandResult<Module.CommandResult<Module.BuildResult>[]>;
	["clean"]: CommandResult<Module.CommandResult<Module.CleanResult>[]>;
	["install"]: CommandResult<Module.CommandResult<Module.InstallResult>[]>;
	["update"]: CommandResult<Module.CommandResult<Module.UpdateResult>[]>;
	["publish"]: CommandResult<Module.CommandResult<Module.PublishResult>[]>;
	["tests"]: CommandResult<Module.CommandResult<Module.TestingResults>[]>;
	["validate"]: CommandResult<Module.CommandResult<Module.ValidateResult>[]>;
	["unify"]: CommandResult<Module.UnifiedVersionsInfo>;
	["analyze"]: CommandResult<AnalyzeSummaryResult>;
	["shared"]: CommandResult<Module.CommandResult<Module.SharedResult>[]>;
};

export type CommandProgress = {
	["build"]: Module.CommandResult<Module.BuildResult>;
	["clean"]: Module.CommandResult<Module.CleanResult>;
	["install"]: Module.CommandResult<Module.InstallResult>;
	["update"]: Module.CommandResult<Module.UpdateResult>;
	["unify"]: Module.CommandResult<Module.UnifyResult>;
	["publish"]: Module.CommandResult<Module.PublishResult>;
	["tests"]: Module.CommandResult<Module.TestingResults>;
	["validate"]: Module.CommandResult<Module.ValidateResult>;
	["analyze"]: Module.CommandResult<Module.AnalyzeResult>;
	["shared"]: Module.CommandResult<Module.SharedResult>;
};

export interface CommandParams {
	transpile?: boolean;
	production?: boolean;
	freeze?: boolean;
	prune?: boolean;
	noaudit?: boolean;
	tag?: string;
	progress?: <C extends Commands>(result: CommandProgress[C]) => void;
	gui?: boolean;
	commit?: boolean;
	fix?: boolean;
	latest?: boolean;
	link?: boolean;
	unlink?: boolean;
	increment?: string;
	browsers?: Array<string>;
	devices?: Array<string>;
	modules?: {
		[key: string]: string;
	};
}

export function command<C extends Commands>(
	initData: InitData,
	modulesData: ModulesData,
	command: C,
	params: CommandParams
): Promise<CommandResults[C]> {
	return new Promise((fulfill, reject) => {
		const asGeneric = (r) => fulfill(r as CommandResults[C]);

		const production = params.production || false;
		const progress = params.progress || ((): void => void 0);
		const modules = params.modules || {};
		const freeze = params.freeze || false;
		const transpile = params.transpile || false;
		const prune = params.prune || false;
		const noaudit = params.noaudit || false;
		const browsers = params.browsers || [];
		const devices = params.devices || [];
		const tag = params.tag;
		const gui = params.gui;
		const commit = params.commit;
		const fix = params.fix || false;
		const latest = params.latest || false;
		const link = params.link || false;
		const unlink = params.unlink || false;
		const increment = params.increment as ServantJson.ServantJson["publish"]["increment"];

		const packageJson = initData.packageJson;
		const graph = modulesData.graph;

		invariant(packageJson, "Servant package json is not defined in command() call.");
		invariant(graph, "Servant dependencies graph is not defined in command() call.");

		if (command === "clean") {
			const entries = resolveEntries(initData, modulesData);
			clean(packageJson, graph, progress, prune, entries).then(asGeneric).catch(reject);
			return;
		}

		if (command === "install") {
			install(packageJson, graph, progress, noaudit).then(asGeneric).catch(reject);
			return;
		}

		if (command === "update") {
			update(packageJson, graph, progress).then(asGeneric).catch(reject);
			return;
		}

		if (command === "validate") {
			validate(packageJson, graph, progress, fix).then(asGeneric).catch(reject);
			return;
		}

		if (command === "build") {
			build(packageJson, graph, progress, production, transpile)
				.then(asGeneric)
				.catch(reject);
			return;
		}

		if (command === "unify") {
			unify(packageJson, graph, progress, modules, latest).then(asGeneric).catch(reject);
			return;
		}

		if (command === "publish") {
			publish(packageJson, graph, progress, production, tag, freeze, commit, increment)
				.then(asGeneric)
				.catch(reject);
			return;
		}

		if (command === "tests") {
			tests(packageJson, graph, progress, browsers, devices, gui)
				.then(asGeneric)
				.catch(reject);
			return;
		}

		if (command === "analyze") {
			analyze(packageJson, graph, progress).then(asGeneric).catch(reject);
			return;
		}

		if (command === "shared") {
			const mode = link ? "link" : unlink ? "unlink" : undefined;
			shared(packageJson, graph, progress, mode).then(asGeneric).catch(reject);
			return;
		}

		//unknown command
		reject(
			new Error(
				`Command '${command}' not exists. Try to look on help for available commands.`
			)
		);
	});
}

export function initialize(
	packageJson: PackageJson.PackageJsonInfo,
	entry: string,
	params: Module.InitParams
): Promise<CommandResult<Module.InitResults>> {
	return new Promise((fulfill, reject) => {
		init(packageJson, entry, params).then(fulfill).catch(reject);
	});
}

//utils

export { convertUnify, answersUnify };
