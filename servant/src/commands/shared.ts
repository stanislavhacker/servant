import { PackageJson } from "@servant/servant-data";

import * as Module from "../module";
import { createCommandResult, createTime, withProgress } from "../utils";

import { CommandProgress, CommandResults } from "./index";

export function shared(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	progress: (result: CommandProgress["shared"]) => void,
	mode: Module.SharedProps["mode"]
): Promise<CommandResults["shared"]> {
	return new Promise((fulfill, reject) => {
		const result = createCommandResult<CommandResults["shared"]["data"]>([]);
		const start = process.hrtime();
		const results: Module.CommandResult<Module.SharedResult>[] = [];

		//create all
		const schedule = Module.scheduleSorted(graph.sorted, (name) => {
			const module = graph.modules[name];
			return Module.shared(packageJson, module, { mode })
				.then((res) => withProgress(res, progress))
				.then((res) => results.push(res));
		});

		schedule
			.then(() => {
				result.time = createTime(start);
				result.data = [...results];
				fulfill(result);
			})
			.catch(reject);
	});
}
