import { PackageJson, ServantJson } from "@servant/servant-data";

import * as Module from "./module/index";
import * as Watcher from "./watcher/index";
import * as Changes from "./vcs/index";
import * as Commands from "./commands";

export interface ServantApi {
	init: (entry: string) => Promise<InitData>;
	create: (
		packageJson: PackageJson.PackageJsonInfo,
		entry: string,
		initParams: Module.InitParams
	) => Promise<Commands.CommandResult<Module.InitResults>>;
	modules: (initData: InitData, params?: ModulesParams) => Promise<ModulesData>;
	command: <C extends Commands.Commands>(
		initData: InitData,
		modules: ModulesData,
		command: C,
		params: Commands.CommandParams
	) => Promise<Commands.CommandResults[C]>;
	validate: (validate: Module.DependenciesValidations) => Module.DependenciesValidationsResults;
	watch: (initData: InitData, changed: Watcher.ChangeHandler) => Promise<void>;
	//reports
	reports: {
		issues: {
			outputs: string[];
			run: (initData: InitData, issues: Module.Issues) => Promise<string[]>;
		};
		analyze: {
			outputs: string[];
			run: (initData: InitData, analyze: Commands.AnalyzeSummaryResult) => Promise<string[]>;
		};
	};
}

export interface InitData {
	entry: string;
	packageJson?: PackageJson.PackageJsonInfo;
	servantJson?: ServantJson.ServantJsonInfo;
	module?: string;
}

export interface ModulesParams {
	init?: boolean;
	only?: Array<string>;
	changed?: Array<Changes.ChangeBy>;
	dependencies?: boolean;
}
export interface ModulesData {
	graph: Module.DependenciesGraph | null;
	validation: Module.DependenciesValidations | null;
}
