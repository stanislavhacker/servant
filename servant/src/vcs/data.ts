export interface VcsChanges {
	modules: Array<VcsModuleChanges>;
	names: Array<string>;
	available: boolean;
}
export interface VcsModuleChanges {
	module: string;
	changedFiles: Array<string>;
	changed: boolean;
	changedBy: Array<ChangeBy>;
}

export enum ChangeBy {
	GIT = "git",
	FILESYSTEM = "filesystem",
}

export function createVcsChanges(): VcsChanges {
	return {
		modules: [],
		names: [],
		available: false,
	};
}

export function createVcsModuleChanges(module: string): VcsModuleChanges {
	return {
		module: module,
		changedFiles: [],
		changed: false,
		changedBy: [],
	};
}
