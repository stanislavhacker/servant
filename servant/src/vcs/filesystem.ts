import * as path from "path";
import * as fs from "fs";
import { ServantJson, Modules } from "@servant/servant-data";
import { Extensions, JS, CSS, LESS, SASS } from "@servant/servant-files";

import { loadPatterns, loadWatched } from "../patterns";
import { DependenciesGraph, iterateSorted } from "../module";
import { ChangeBy, VcsChanges, VcsModuleChanges } from "./data";
import { invariant } from "../invariant";

export function changes(
	changes: VcsChanges,
	servantJson: ServantJson.ServantJsonInfo,
	modulesInfo: DependenciesGraph
): Promise<void> {
	return new Promise((fulfill, reject) => {
		const modules: Array<Promise<void>> = [];

		iterateSorted(modulesInfo.sorted).forEach((module) => {
			modules.push(
				moduleChanges(
					modulesInfo.modules[module],
					changes.modules[changes.names.indexOf(module)]
				)
			);
		});

		Promise.all(modules)
			.then(() => fulfill())
			.catch(reject);
	});
}

function moduleChanges(
	moduleInfo: Modules.ModuleDefinition,
	moduleChanges: VcsModuleChanges
): Promise<void> {
	return new Promise((fulfill, reject) => {
		loadFiles(moduleInfo)
			.then((data) => loadFilesInfo(moduleInfo, data[0], data[1]))
			.then((filesInfo) => {
				invariant(
					moduleInfo.module,
					"No module declaration found. Invalid data provided or is probably error in Servant."
				);

				//REASON 1: Module is not intended for build
				if (moduleInfo.module.servantJson.content.entry === false) {
					fulfill();
					return;
				}

				//REASON 2: No dist files, no build yet, mark as changed for rebuild
				if (filesInfo.dists.length === 0) {
					moduleChanges.changed = moduleChanges.changed || true;
					moduleChanges.changedBy.push(ChangeBy.FILESYSTEM);
					moduleChanges.changedFiles.push(...filesInfo.sources.map((s) => s.path));
					fulfill();
					return;
				}

				//REASON 3: No sources found, never changed
				if (filesInfo.sources.length === 0) {
					fulfill();
					return;
				}

				const modifiedDist = filesInfo.dists[0].modified;
				const modifiedSource = filesInfo.sources[0].modified;

				//REASON 4: Modify time of dist is smaller than modify time of source, rebuild
				if (modifiedDist <= modifiedSource) {
					moduleChanges.changed = moduleChanges.changed || true;
					moduleChanges.changedBy.push(ChangeBy.FILESYSTEM);
					moduleChanges.changedFiles.push(...filesInfo.sources.map((s) => s.path));
					fulfill();
					return;
				}

				//REASON 5: No change at all
				fulfill();
			})
			.catch(reject);
	});
}

function loadFiles(moduleInfo: Modules.ModuleDefinition): Promise<Array<Array<string>>> {
	return new Promise((fulfill, reject) => {
		invariant(
			moduleInfo.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const moduleServantJson = moduleInfo.module.servantJson;
		const directories: Array<Promise<Array<string>>> = [];

		const sources = getSources(moduleInfo);
		const dist = Extensions.resolveAll(moduleServantJson.content.output.directory, [
			JS.MAIN,
			CSS.MAIN,
		]);

		directories.push(loadPatterns(moduleServantJson.cwd, sources));
		directories.push(loadPatterns(moduleServantJson.cwd, dist));

		Promise.all(directories).then(fulfill).catch(reject);
	});
}

function getSources(moduleInfo: Modules.ModuleDefinition): Array<string> {
	//module is not defined
	if (!moduleInfo.module) {
		return [];
	}

	const moduleServantJson = moduleInfo.module.servantJson;

	//add watched and resources patterns
	const sources = [...moduleServantJson.content.resources, ...moduleServantJson.content.watch];

	//added src and tests based on ext
	const sourcesPatterns = [...moduleServantJson.content.src, ...moduleServantJson.content.tests];
	//add all resolve extensions
	sourcesPatterns.forEach((src) => {
		if (!path.extname(src)) {
			moduleServantJson.content.resolve.forEach((ext) => {
				sources.push(`${src}.${ext}`);
			});
		} else {
			sources.push(src);
		}
	});

	const resolve = [...LESS.EXT, ...CSS.EXT, ...SASS.EXT];
	//add all known extensions
	sourcesPatterns.forEach((src) => {
		Extensions.replaceAll(src, resolve);
	});

	return sources;
}

type FilesInfo = {
	sources: Array<FileInfo>;
	dists: Array<FileInfo>;
};

function loadFilesInfo(
	moduleInfo: Modules.ModuleDefinition,
	sourcesFiles: Array<string>,
	distFiles: Array<string>
): Promise<FilesInfo> {
	return new Promise((fulfill) => {
		invariant(
			moduleInfo.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const filesInfo: FilesInfo = {} as FilesInfo;
		const moduleServantJson = moduleInfo.module.servantJson;

		const sources: Array<Promise<FileInfo | null>> = [];
		const sourcesFiltered = loadWatched(sourcesFiles, moduleServantJson.content.clean);
		sourcesFiltered.forEach((sf) => {
			sources.push(fileInfo(sf));
		});

		const dists: Array<Promise<FileInfo | null>> = [];
		distFiles.forEach((sf) => {
			dists.push(fileInfo(sf));
		});

		Promise.all(sources)
			.then((sourcesInfo) => {
				filesInfo.sources = sourcesInfo
					.filter((si) => Boolean(si))
					.sort(sorter) as Array<FileInfo>;
			})
			.then(() =>
				Promise.all(dists).then((distsInfo) => {
					filesInfo.dists = distsInfo
						.filter((si) => Boolean(si))
						.sort(sorter) as Array<FileInfo>;
					fulfill(filesInfo);
				})
			);
	});
}

type FileInfo = {
	path: string;
	modified: number;
};

function sorter(a: FileInfo, b: FileInfo): number {
	return b.modified - a.modified;
}

function fileInfo(path: string): Promise<FileInfo | null> {
	return new Promise((fulfill) => {
		fs.stat(path, (err, st) => {
			if (st && st.isFile()) {
				const stats = st || {};

				fulfill({
					path: path,
					modified: stats.mtimeMs || 0,
				});
				return;
			}
			fulfill(null);
		});
	});
}
