import { ServantJson } from "@servant/servant-data";

import { DependenciesGraph, iterateSorted } from "../module";
import * as git from "./git";
import * as filesystem from "./filesystem";

import { ChangeBy, createVcsChanges, createVcsModuleChanges, VcsChanges } from "./data";

export { VcsChanges, ChangeBy, git };

export function vcs(
	servantJson: ServantJson.ServantJsonInfo,
	graph: DependenciesGraph,
	changed: Array<ChangeBy>
): Promise<VcsChanges> {
	return new Promise((fulfill, reject) => {
		const changes = createVcsChanges();
		fillChanges(changes, graph);

		const changesData: Array<Promise<unknown>> = [];
		changes.available = changed.length > 0;

		//FILESYSTEM: Get changes from filesystem and changed times
		if (changed.indexOf(ChangeBy.FILESYSTEM) >= 0) {
			changesData.push(filesystem.changes(changes, servantJson, graph));
		}
		//GIT: Git is available, get changed files from git changes
		if (changed.indexOf(ChangeBy.GIT) >= 0) {
			changesData.push(git.changes(changes, servantJson, graph));
		}

		Promise.all(changesData)
			.then(() => fulfill(changes))
			.catch(reject);
	});
}

export function commit(
	packageJson: ServantJson.ServantJsonInfo,
	message: string
): Promise<boolean> {
	return git.commit(packageJson.cwd, message);
}

function fillChanges(changes: VcsChanges, graph: DependenciesGraph) {
	iterateSorted(graph.sorted).forEach((module) => {
		changes.names.push(module);
		changes.modules.push(createVcsModuleChanges(module));
	});
}
