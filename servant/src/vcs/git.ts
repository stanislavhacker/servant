import * as path from "path";
import simpleGit, { StatusResult, StatusResultRenamed } from "simple-git";
import { ServantJson } from "@servant/servant-data";

import { DependenciesGraph, iterateSorted } from "../module";
import { ChangeBy, VcsChanges } from "./data";
import { parse } from "../libraries/console";

export function changes(
	changes: VcsChanges,
	servantJson: ServantJson.ServantJsonInfo,
	modulesInfo: DependenciesGraph
): Promise<void> {
	return new Promise((fulfill, reject) => {
		check(servantJson).then((available) => {
			//git is not available
			if (!available) {
				fulfill();
				return;
			}

			//git is available
			const git = simpleGit(servantJson.cwd);
			git.status()
				.then((statusInfo) => {
					const changed = getChangedFiles(servantJson, statusInfo);

					iterateSorted(modulesInfo.sorted).forEach((module) => {
						const moduleInfo = modulesInfo.modules[module].module;
						if (moduleInfo) {
							const root = moduleInfo.packageJson.cwd + path.sep;

							const moduleChanges = changes.modules[changes.names.indexOf(module)];
							const gitChanges = changed.filter(
								(change) => change.indexOf(root) === 0
							);

							if (gitChanges.length > 0) {
								moduleChanges.changedFiles.push(...gitChanges);
								moduleChanges.changed = moduleChanges.changed || true;
								moduleChanges.changedBy.push(ChangeBy.GIT);
							}
						}
					});

					fulfill();
				})
				.catch(reject);
		});
	});
}

export function versioned(servantJson: ServantJson.ServantJsonInfo): Promise<Array<string>> {
	return new Promise((fulfill) => {
		check(servantJson).then((available) => {
			//git is not available
			if (!available) {
				fulfill([]);
				return;
			}

			//git is available
			const git = simpleGit(servantJson.cwd);
			git.raw([`ls-files`, "-t"])
				.then((result) => {
					const lines = parse(result);
					const files = lines.map((line) => line.substr(2));
					fulfill(files);
				})
				.catch(() => {
					fulfill([]);
				});
		});
	});
}

export function create(cwd: string): Promise<boolean> {
	return new Promise((fulfill) => {
		const git = simpleGit(cwd);

		git.checkIsRepo()
			.then((isRepo: boolean) => {
				//exists, it's ok
				if (isRepo) {
					fulfill(true);
					return;
				}
				//init
				git.init().then(() => fulfill(true));
			})
			.catch(() => fulfill(false));
	});
}

export function exists(cwd: string): Promise<boolean> {
	return new Promise((fulfill) => {
		const git = simpleGit(cwd);

		git.checkIsRepo()
			.then(() => fulfill(true))
			.catch(() => fulfill(false));
	});
}

export function add(cwd: string, files: Array<string>): Promise<boolean> {
	return new Promise((fulfill) => {
		const git = simpleGit(cwd);

		git.checkIsRepo()
			.then(() => {
				git.add(files)
					.then(() => fulfill(true))
					.catch(() => fulfill(false));
			})
			.catch(() => fulfill(false));
	});
}

export function commit(cwd: string, message: string): Promise<boolean> {
	return new Promise((fulfill) => {
		const git = simpleGit(cwd);

		const onError = () => fulfill(false);

		//check repo
		git.checkIsRepo()
			.then(() => {
				//load status
				git.status()
					.then((results) => {
						//add current changed files into commit
						const files = [...results.created, ...results.deleted, ...results.modified];

						git.commit(message, files)
							.then(() => fulfill(true))
							.catch(onError);
					})
					.catch(onError);
			})
			.catch(onError);
	});
}

function check(servantJson: ServantJson.ServantJsonInfo): Promise<boolean> {
	return new Promise((fulfill) => {
		simpleGit(servantJson.cwd)
			.status()
			.then(() => fulfill(true))
			.catch(() => fulfill(false));
	});
}

function getChangedFiles(
	servantJson: ServantJson.ServantJsonInfo,
	statusInfo: StatusResult
): Array<string> {
	const changed: Array<string> = [];

	statusInfo.created.forEach((file) => {
		changed.push(path.join(servantJson.cwd, file));
	});
	statusInfo.modified.forEach((file) => {
		changed.push(path.join(servantJson.cwd, file));
	});
	statusInfo.deleted.forEach((file) => {
		changed.push(path.join(servantJson.cwd, file));
	});

	const renamed = statusInfo.renamed as Array<StatusResultRenamed | string>;
	renamed.forEach((file) => {
		if (typeof file === "string") {
			changed.push(path.join(servantJson.cwd, file));
		} else {
			changed.push(path.join(servantJson.cwd, file.to));
		}
	});

	return [...new Set(changed)];
}
