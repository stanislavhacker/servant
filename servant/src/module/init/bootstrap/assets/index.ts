import * as png from "./png";

export const ASSETS_WEB_FILES = {
	"assets/logo": png.ServantLogoPng,
	"assets/favicon": png.FaviconPng,
};
