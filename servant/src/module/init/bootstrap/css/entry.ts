//language=css
export const WebIndexCss = `
/**
 * @summary Entry plain css file
 * @description
 *   You choose that you want to use plain css in your package (@see https://developer.mozilla.org/en-US/docs/Web/CSS).
 *   Plain css is not recommended because you can not use variables, nesting selectors, mixins and more complex
 *   stuff in you app. 
 * 
 *   This is main entry point for css and you can use this as main css file for your app. But you can also use
 *   importing css for file because Servant support "import * as css from "component.css". In this case that you want to
 *   use css imports, this file can be removed. 
 */

body {
    margin: 0;
    padding: 0;
    background: #282C34;
    color: white;
    font-family: 'Roboto', sans-serif;
}

.container {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: flex;
    flex-direction: row;
    align-content: center;
    justify-content: center;
    align-items: center;
}

.container .content {
    display: flex;
    flex-direction: column;
    align-items: center;
}

.logo {
    width: 30%;
    height: 30%;
    margin: auto;
}

.logo img {
    width: 100%;
    height: 100%;
}

.title {
    font-size: 40px;
    font-weight: bold;
    margin: 10px 0;
}

.description {
    max-width: 400px;
    font-size: 16px;
    margin: 10px 0;
    color: #61DBFA;
    text-align: center;
}
`;

//language=scss
export const WebIndexScss = `
/**
 * @summary Entry sass file
 * @description
 *   You choose that you want to use sass styles in your package (@see https://sass-lang.com/).
 *   Sass is the most mature, stable, and powerful professional grade CSS extension language in the world.
 *   Sass supports lots of features like variables, nesting, partials, modules, mixins, extend and inheritance and 
 *   operators.
 * 
 *   This is main entry point for sass and you can use this as main sass file for your app. But you can also use
 *   importing scss for file because Servant support "import * as css from "component.css". In this case that you want to
 *   use scss imports, this file can be removed. 
 */

@use "sass:math";

$background: #282C34;
$color: white;
$highlight: #61DBFA;

body {
  margin: 0;
  padding: 0;
  color: $color;
  background: $background;
  font-family: 'Roboto', sans-serif;

  .container {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: flex;
    flex-direction: row;
    align-content: center;
    justify-content: center;
    align-items: center;

    .content {
      display: flex;
      flex-direction: column;
      align-items: center;

      .logo {
        width: 30%;
        height: 30%;
        margin: auto;

        img {
          width: 100%;
          height: 100%;
        }
      }

      .title {
        font-size: 40px;
        font-weight: bold;
        margin: 10px 0;
      }

      .description {
        max-width: math.div(800px, 2);
        font-size: 16px;
        margin: 10px 0;
        color: $highlight;
        text-align: center;
      }
    }
  }
}

`;

//language=less
export const WebIndexLess = `
/**
 * @summary Entry less file
 * @description
 *   You choose that you want to use less styles in your package (@see https://lesscss.org/).
 *   It's CSS, with just a little more.
 *   Less supports lots of features like variables, nesting, partials, modules, mixins, extend and inheritance and 
 *   operators.
 * 
 *   This is main entry point for less and you can use this as main less file for your app. But you can also use
 *   importing less for file because Servant support "import * as css from "component.css". In this case that you want to
 *   use less imports, this file can be removed. 
 */

@background: #282C34;
@color: white;
@highlight: #61DBFA;

body {
  margin: 0;
  padding: 0;
  color: if((iscolor(@color)), @color, white);
  background: @background;
  font-family: 'Roboto', sans-serif;

  .container {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    display: flex;
    flex-direction: row;
    align-content: center;
    justify-content: center;
    align-items: center;

    .content {
      display: flex;
      flex-direction: column;
      align-items: center;

      .logo {
        width: 30%;
        height: 30%;
        margin: auto;

        img {
          width: 100%;
          height: 100%;
        }
      }

      .title {
        font-size: 40px;
        font-weight: bold;
        margin: 10px 0;
      }

      .description {
        max-width: 400px;
        font-size: 16px;
        margin: 10px 0;
        color: if((iscolor(@highlight)), @highlight, deepskyblue);
        text-align: center;
      }
    }
  }
}

`;
