import * as entry from "./entry";

export const CSS_WEB_FILES = {
	"src/style": entry.WebIndexCss,
};

export const LESS_WEB_FILES = {
	"src/style": entry.WebIndexLess,
};

export const SCSS_WEB_FILES = {
	"src/style": entry.WebIndexScss,
};
