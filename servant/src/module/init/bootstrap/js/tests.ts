//language=javascript
export const WebIndexJs = `
/**
 * @summary Entry javascript file for tests
 * @description
 *   This is main file that is used as entry file into your tests. If you use Servant for web browser package,
 *   its necessary to include all tests file into this file.
 *   
 */

require("$link(tests/tests)");

`;

//language=javascript
export const WebTestsJs = `
/**
 * @summary Entry javascript file for tests
 * @description
 *   This is a one of file that contains test for your application. Now there is only one test that check if
 *   hello world is written into page.
 *   
 */

const { createScreen } = require("$link(src/index)");

describe("check create hello word", () => {

    it("write hello world into element", () => {
        const div = document.createElement("div");

        createScreen(div);

        const logo = div.querySelectorAll(".logo img");
        expect(logo.length).toBe(1);

        const title = div.querySelectorAll(".title");
        expect(title.length).toBe(1);
        expect(title[0].textContent).toBe("Servant");

        const description = div.querySelectorAll(".description");
        expect(description.length).toBe(1);
        expect(description[0].textContent).toBe("Servant builder is simple build tool for developers, that wants to build and manage modularized large projects in monorepo or separated into more repositories.");

        div.remove();
    });

});

`;

//language=javascript
export const NodeTestsJs = `
/**
 * @summary Entry javascript file for tests
 * @description
 *   This is a one of file that contains test for your application. Now there is only one test that check if
 *   hello world is written into page.
 *   
 */

const { createServer  } = require("$link(src/index)");

describe("check create server with hello word", () => {

    it("create servet on port", (done) => {
        const listener = createServer("127.0.0.1", 3000, (hostname, port) => {
            expect(listener).toBeDefined();
            expect(hostname).toBe("127.0.0.1");
            expect(port).toBe(3000);
            listener.close(done);
        });
    });

});

`;
