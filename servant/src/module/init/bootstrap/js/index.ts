import * as tests from "./tests";
import * as entry from "./entry";

export const JS_WEB_FILES = {
	"tests/index": tests.WebIndexJs,
	"tests/tests": tests.WebTestsJs,
	"src/index": entry.WebIndexJs,
};

export const JS_NODE_FILES = {
	"tests/tests": tests.NodeTestsJs,
	"src/index": entry.NodeIndexJs,
};
