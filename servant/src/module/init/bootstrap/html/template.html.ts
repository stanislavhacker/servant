//language=xhtml
export const WebTemplateHtml = `
    <template>
        <style type="text/css">
            body {
                margin: 0;
                padding: 0;
            }
        </style>
        <script type="text/javascript">
            let item = window["$link(name)"];
            item.createScreen(document.body);
        </script>
    </template>
`;
