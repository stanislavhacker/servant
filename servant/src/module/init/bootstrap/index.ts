import { ServantJson } from "@servant/servant-data";
import { Extensions, JS, LESS, SASS, TS, CSS, Path, HTML } from "@servant/servant-files";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorNextHandler,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorQuestions,
	GeneratorLoaded,
	GeneratorFile,
	FnCreateDir,
	FnCreateFile,
	FnUpdateFile,
} from "@servant/servant-generators";
import * as path from "path";

import { InitParamsDefault } from "../init";
import { getValueOrDefault } from "../utils";
import { ServantInitProperty, InitParams } from "../types";

import { HTML_WEB_FILES } from "./html";
import { ASSETS_WEB_FILES } from "./assets";
import { JS_WEB_FILES, JS_NODE_FILES } from "./js";
import { TS_WEB_FILES, TS_NODE_FILES } from "./ts";
import { CSS_WEB_FILES, LESS_WEB_FILES, SCSS_WEB_FILES } from "./css";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-bootstrap-basic-generator",
	description: "This is generator that is used to generate basic application structure.",
	path: "internal://servant/servant-bootstrap-basic-generator",
	entry: "bundled",
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [];
};

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	{ createFile, createDir, getFile, getFiles, updateFile }: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	const hasEntry = getValueOrDefault(getAnswer, ServantInitProperty.HasEntry, true.toString());
	const language = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Language,
		InitParamsDefault.language
	);
	const target = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Target,
		InitParamsDefault.target
	);
	const styles = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Styles,
		InitParamsDefault.styles
	);

	const servantJson = getFile(ServantJson.SERVANT_JSON);
	const servantJsonContent = servantJson?.content as unknown as ServantJson.ServantJson;

	//has entry point
	if (hasEntry === true.toString()) {
		const name = getValueOrDefault(getAnswer, ServantInitProperty.Name, InitParamsDefault.name);
		const data = {
			...createSourcesSources(servantJsonContent, language, target, {
				createDir,
				createFile,
			}),
			...createTestsSources(servantJsonContent, language, target, { createDir, createFile }),
			...createStyleFiles(servantJsonContent, target, styles, { createFile }),
			...createHtmlFiles(servantJsonContent, target, name, { createFile }),
			...createAssetsFiles(servantJsonContent, target, { createFile }),
		};
		const files = getFiles();
		updateLinks(files, data, { updateFile });
		updateServantJson(servantJson, data, target, name, { updateFile });
	}

	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: {},
	},
};

//HELPERS

function createTestsSources(
	servantJson: ServantJson.ServantJson,
	language: Required<InitParams>["language"],
	target: Required<InitParams>["target"],
	{ createDir, createFile }: { createDir: FnCreateDir; createFile: FnCreateFile }
) {
	const data = {};

	//create directory structure for tests
	servantJson.tests.forEach((test) => {
		createDir(Path.directory("", test));
	});
	if (servantJson.test) {
		const entry = getTestsEntryFile(servantJson.test, target, language);
		createFile(entry.filename, entry.content);
		data[entry.key] = entry.filename;

		const testsDir = path.dirname(servantJson.test);
		const tests = getTestsTestFile(path.join(testsDir, "tests"), target, language);
		createFile(tests.filename, tests.content);
		data[tests.key] = tests.filename;
	} else {
		const testsDir = Path.directory("", servantJson.tests[0]);
		const { filename, content, key } = getTestsTestFile(
			path.join(testsDir, "tests"),
			target,
			language
		);
		createFile(filename, content);
		data[key] = filename;
	}

	return data;
}

function createSourcesSources(
	servantJson: ServantJson.ServantJson,
	language: Required<InitParams>["language"],
	target: Required<InitParams>["target"],
	{ createDir, createFile }: { createDir: FnCreateDir; createFile: FnCreateFile }
) {
	//create directory structure for sources
	servantJson.src.forEach((src) => {
		createDir(Path.directory("", src));
	});
	const { filename, content, key } = getSrcEntryFile(
		servantJson.entry as string,
		target,
		language
	);
	createFile(filename, content);

	return {
		[key]: filename,
	};
}

function createStyleFiles(
	servantJson: ServantJson.ServantJson,
	target: Required<InitParams>["target"],
	styles: Required<InitParams>["styles"],
	{ createFile }: { createFile: FnCreateFile }
) {
	//create style root file
	if (target === ServantJson.ModuleTarget.web || target === ServantJson.ModuleTarget.web_page) {
		const { filename, content, key } = getStyleFile(
			servantJson.entry as string,
			target,
			styles
		);
		createFile(filename, content);
		return {
			[key]: filename,
		};
	}
	return {};
}

function createHtmlFiles(
	servantJson: ServantJson.ServantJson,
	target: Required<InitParams>["target"],
	name: string,
	{ createFile }: { createFile: FnCreateFile }
) {
	//create style root file
	if (target === ServantJson.ModuleTarget.web || target === ServantJson.ModuleTarget.web_page) {
		const { filename, content, key } = getHtmlFile("template", name);
		createFile(filename, content);
		return {
			[key]: filename,
		};
	}
	return {};
}

function createAssetsFiles(
	servantJson: ServantJson.ServantJson,
	target: Required<InitParams>["target"],
	{ createFile }: { createFile: FnCreateFile }
) {
	const assets = {};
	//create logo
	if (target === ServantJson.ModuleTarget.web || target === ServantJson.ModuleTarget.web_page) {
		const { filename, content, key } = getAssetLogoFile("assets/logo", "png");
		createFile(filename, content);
		assets[key] = filename;
	}
	//create favicon
	if (target === ServantJson.ModuleTarget.web_page) {
		const { filename, content, key } = getAssetFaviconFile("assets/favicon", "png");
		createFile(filename, content);
		assets[key] = filename;
	}
	return assets;
}

type FileInfo = {
	filename: string;
	content: string | Buffer;
	key: string;
};

type SourceFileName = keyof (typeof JS_WEB_FILES & typeof TS_WEB_FILES);
type StylesFileName = keyof (typeof CSS_WEB_FILES & typeof LESS_WEB_FILES & typeof SCSS_WEB_FILES);
type HtmlFileName = keyof typeof HTML_WEB_FILES;
type AssetsFileName = keyof typeof ASSETS_WEB_FILES;

//typescript / javascript

function getLanguageContent(
	target: Required<InitParams>["target"],
	language: Required<InitParams>["language"],
	file: SourceFileName
): string {
	switch (language) {
		case "js": {
			const sources =
				target === ServantJson.ModuleTarget.web ||
				target === ServantJson.ModuleTarget.web_page
					? JS_WEB_FILES
					: JS_NODE_FILES;
			return sources[file];
		}
		default: {
			const sources =
				target === ServantJson.ModuleTarget.web ||
				target === ServantJson.ModuleTarget.web_page
					? TS_WEB_FILES
					: TS_NODE_FILES;
			return sources[file];
		}
	}
}

function getLanguageFilename(language: Required<InitParams>["language"], file: string) {
	return language === "js"
		? Extensions.resolve("", file, [JS.MAIN])[0]
		: Extensions.resolve("", file, [TS.MAIN])[0];
}

function getTestsEntryFile(
	file: string,
	target: Required<InitParams>["target"],
	language: Required<InitParams>["language"]
): FileInfo {
	const key: SourceFileName = "tests/index";
	const content = getLanguageContent(target, language, key);
	const filename = getLanguageFilename(language, file);

	return {
		filename,
		content,
		key,
	};
}

function getTestsTestFile(
	file: string,
	target: Required<InitParams>["target"],
	language: Required<InitParams>["language"]
): FileInfo {
	const key: SourceFileName = "tests/tests";
	const content = getLanguageContent(target, language, key);
	const filename = getLanguageFilename(language, file);

	return {
		filename,
		content,
		key,
	};
}

function getSrcEntryFile(
	file: string,
	target: Required<InitParams>["target"],
	language: Required<InitParams>["language"]
): FileInfo {
	const key: SourceFileName = "src/index";
	const content = getLanguageContent(target, language, key);
	const filename = getLanguageFilename(language, file);

	return {
		filename,
		content,
		key,
	};
}

//styles

function getStylesContent(
	target: Required<InitParams>["target"],
	styles: Required<InitParams>["styles"],
	file: StylesFileName
): string {
	switch (styles) {
		case "sass": {
			return SCSS_WEB_FILES[file];
		}
		case "less": {
			return LESS_WEB_FILES[file];
		}
		default: {
			return CSS_WEB_FILES[file];
		}
	}
}

function getStylesFilename(styles: Required<InitParams>["styles"], file: string) {
	if (styles === "sass") {
		return Extensions.resolve("", file, [SASS.MAIN])[0];
	}
	if (styles === "less") {
		return Extensions.resolve("", file, [LESS.MAIN])[0];
	}
	//default: CSS
	return Extensions.resolve("", file, [CSS.MAIN])[0];
}

function getStyleFile(
	file: string,
	target: Required<InitParams>["target"],
	styles: Required<InitParams>["styles"]
): FileInfo {
	const key: StylesFileName = "src/style";
	const content = getStylesContent(target, styles, key);
	const filename = getStylesFilename(styles, file);

	return {
		filename,
		content,
		key,
	};
}

//html

function getHtmlContent(name: string, file: HtmlFileName): string {
	let content = HTML_WEB_FILES[file];

	content = content.replace(`$link(name)`, name);
	return content;
}

function getHtmlFilename(file: string) {
	return Extensions.resolve("", file, [HTML.MAIN])[0];
}

function getHtmlFile(file: string, name: string): FileInfo {
	const key: HtmlFileName = "template";
	const content = getHtmlContent(name, key);
	const filename = getHtmlFilename(file);

	return {
		filename,
		content,
		key,
	};
}

//assets

function getAssetContent(file: AssetsFileName): Buffer {
	return Buffer.from(ASSETS_WEB_FILES[file], "base64");
}

function getAssetFilename(file: string, ext: string) {
	return Extensions.resolve("", file, [ext])[0];
}

function getAssetLogoFile(file: string, ext: string): FileInfo {
	const key: AssetsFileName = "assets/logo";
	const content = getAssetContent(key);
	const filename = getAssetFilename(file, ext);

	return {
		filename,
		content,
		key,
	};
}

function getAssetFaviconFile(file: string, ext: string): FileInfo {
	const key: AssetsFileName = "assets/favicon";
	const content = getAssetContent(key);
	const filename = getAssetFilename(file, ext);

	return {
		filename,
		content,
		key,
	};
}

//servant json

function updateServantJson(
	servantJson: GeneratorFile | null,
	data: Record<string, string>,
	target: Required<InitParams>["target"],
	name: string,
	{ updateFile }: { updateFile: FnUpdateFile }
) {
	//no servant json
	if (!servantJson) {
		return;
	}

	const content = servantJson.content as unknown as ServantJson.ServantJson;

	//defaults
	content.package = name;
	//web and page
	if (target === ServantJson.ModuleTarget.web || target === ServantJson.ModuleTarget.web_page) {
		content.server = {
			...content.server,
			entries: {
				...content.server?.entries,
				[name]: {
					title: `${name} - Preview Page`,
					template: `./${data["template"]}`,
				},
			},
			css: [
				...(content.server?.css ?? []),
				"https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap",
			],
		};
		content.resources = [`./${data["assets/logo"]}`];
	}
	//page
	if (target === ServantJson.ModuleTarget.web_page) {
		content.webpage.title = name;
		content.webpage.favicon = `./${data["assets/favicon"]}`;
	}

	updateFile(servantJson.relativePath, content as unknown as Record<string, unknown>);
}

//links

function generateRelativePath(savedPath: string, file: GeneratorFile) {
	const savedFilename = path.basename(savedPath);
	const savedResolved = path.resolve("/", path.dirname(savedPath));
	const currentResolved = path.resolve("/", path.dirname(file.relativePath));
	const relative = path.relative(currentResolved, savedResolved);
	const fullRelativePath = path.join(relative, savedFilename);
	const ext = path.extname(fullRelativePath);
	const relativePath = Path.normalize(fullRelativePath.replace(ext, ""));

	if (relativePath.startsWith("../") || relativePath.startsWith("./")) {
		return relativePath;
	}
	return `./${relativePath}`;
}

function updateLinks(
	files: GeneratorFile[],
	data: Record<string, string>,
	{ updateFile }: { updateFile: FnUpdateFile }
) {
	files.map((file) => {
		//content is not string
		if (typeof file.content !== "string") {
			return;
		}

		let content = file.content;

		Object.keys(data).forEach((key) => {
			const savedPath = data[key];
			const relativePath = generateRelativePath(savedPath, file);

			content = content.replace(`$link(${key})`, relativePath);
		});

		updateFile(file.relativePath, content);
	});
}
