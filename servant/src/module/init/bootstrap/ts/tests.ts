//language=typescript
export const WebIndexTs = `
/**
 * @summary Entry typescript file for tests in browser
 * @description
 *   This is main file that is used as entry file into your tests. If you use Servant for web browser package,
 *   its necessary to include all tests file into this file.
 *   
 */

import "$link(tests/tests)";

`;

//language=typescript
export const WebTestsTs = `
/**
 * @summary Entry typescript file for tests
 * @description
 *   This is a one of file that contains test for your application. Now there is only one test that check if
 *   hello world is written into page.
 *   
 */

import { createScreen  } from "$link(src/index)";

describe("check create screen", () => {

    it("write servant data and check structure", () => {
       const div = document.createElement("div");

        createScreen(div);
        
        const logo = div.querySelectorAll(".logo img");
        expect(logo.length).toBe(1);
        
        const title = div.querySelectorAll(".title");
        expect(title.length).toBe(1);
        expect(title[0].textContent).toBe("Servant");
        
        const description = div.querySelectorAll(".description");
        expect(description.length).toBe(1);
        expect(description[0].textContent).toBe("Servant builder is simple build tool for developers, that wants to build and manage modularized large projects in monorepo or separated into more repositories.");

        div.remove();
    });

});

`;

//language=typescript
export const NodeTestsTs = `
/**
 * @summary Entry typescript file for tests
 * @description
 *   This is a one of file that contains test for your application. Now there is only one test that check if
 *   hello world is written into page.
 *   
 */

import { createServer  } from "$link(src/index)";

describe("check create server with hello word", () => {

    it("create servet on port", (done) => {
        const listener = createServer("127.0.0.1", 3000, (hostname, port) => {
            expect(listener).toBeDefined();
            expect(hostname).toBe("127.0.0.1");
            expect(port).toBe(3000);
            listener.close(done);
        });
    });

});

`;
