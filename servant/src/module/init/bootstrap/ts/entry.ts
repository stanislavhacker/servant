//language=typescript
export const WebIndexTs = `
/**
 * @summary Entry typescript file
 * @description
 *   This is main file that is used as entry file into your package. Code in this project
 *   is written in typescript language (@see https://www.typescriptlang.org/). There can
 *   be exported functions, classes or types. This is content used for browsers mainly.
 *   
 */

export function createScreen(parent: HTMLElement) {
    const screen = createDiv(["container"],
        [
            createDiv(["content"],
                [
                    createDiv(["logo"], [createImage("logo.png", "Servant logo")]),
                    createDiv(["title"], [createText("Servant")]),
                    createDiv(["description"], [createText("Servant builder is simple build tool for developers, that wants to build and manage modularized large projects in monorepo or separated into more repositories.")]),
                ])
        ]
    );
    parent.appendChild(screen);
}

function createText(text: string) {
    return document.createTextNode(text);
}

function createDiv(className: string[], children: Node[]) {
    const div = document.createElement('div');
    div.className = className.join(" ");
    children.forEach((child) => {
        div.appendChild(child);
    });
    return div;
}

function createImage(url: string, alt: string) {
    const img = document.createElement('img');
    img.src = url;
    img.alt = alt;
    return img;
}

`;

//language=typescript
export const NodeIndexTs = `
/**
 * @summary Entry typescript file
 * @description
 *   This is main file that is used as entry file into your package. Code in this project
 *   is written in typescript language (@see https://www.typescriptlang.org/). There can
 *   be exported functions, classes or types. This is content used for node js or node cli
 *   mainly.
 *   
 */

import * as http from "http";

const hostname = '127.0.0.1';
const port = 3000;

//language=html
const html = \`
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Servant nodejs project</title>
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap">
        <style>
            body {
              margin: 0;
              padding: 0;
              color: white;
              background: #282C34;
              font-family: "Roboto", sans-serif;
            }
            body .container {
              position: absolute;
              top: 0;
              left: 0;
              right: 0;
              bottom: 0;
              display: flex;
              flex-direction: row;
              align-content: center;
              justify-content: center;
              align-items: center;
            }
            body .container .content {
              display: flex;
              flex-direction: column;
              align-items: center;
            }
            body .container .content .logo {
              width: 30%;
              height: 30%;
              margin: auto;
            }
            body .container .content .logo img {
              width: 100%;
              height: 100%;
            }
            body .container .content .title {
              font-size: 40px;
              font-weight: bold;
              margin: 10px 0;
            }
            body .container .content .description {
              max-width: 400px;
              font-size: 16px;
              margin: 10px 0;
              color: #61DBFA;
              text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="logo"><img src="https://gitlab.com/stanislavhacker/servant/raw/master/assets/logo.png" alt="Servant logo"></div>
                <div class="title">Servant</div>
                <div class="description">Servant builder is simple build tool for developers, that wants to build and manage modularized large projects in monorepo or separated into more repositories.
            </div>
        </div>
    </div>
    </body>
</html>
\`;

export function createServer(hostname: string, port: number, onDone: (hostname: string, port: number) => void): http.Server {
    const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');
        res.end(html);
    });

    return server.listen(port, hostname, () => {
        onDone(hostname, port);
    });
}

if (require.main === module) {
    createServer(hostname, port, () => {
        console.log("This is a Servant generated nodejs project!");
        console.log(\`Server running at http://\${hostname}:\${port}\`);
    });
}

`;
