import * as tests from "./tests";
import * as entry from "./entry";

export const TS_WEB_FILES = {
	"tests/index": tests.WebIndexTs,
	"tests/tests": tests.WebTestsTs,
	"src/index": entry.WebIndexTs,
};

export const TS_NODE_FILES = {
	"tests/tests": tests.NodeTestsTs,
	"src/index": entry.NodeIndexTs,
};
