import {
	api,
	GeneratorAnswer,
	GeneratorEmptyResults,
	GeneratorResults,
} from "@servant/servant-generators";

import { PrepareInitResults } from "./types";
import { loaded } from "./generators/main";

export function run(
	answers: GeneratorAnswer[],
	props: PrepareInitResults
): ReturnType<typeof api.multiRunner> {
	return new Promise((resolve) => {
		api.multiRunner({ ...GeneratorEmptyResults, answers }, loaded, props).then(resolve);
	});
}

export function save<D>(into: string, results: GeneratorResults<D>): ReturnType<typeof api.saver> {
	return new Promise((resolve) => {
		api.saver(into, results).then(resolve);
	});
}
