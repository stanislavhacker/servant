import { GeneratorAnswer, GeneratorSaverResults } from "@servant/servant-generators";
import {
	EslintrcJson,
	GitIgnore,
	PackageJson,
	PrettierrcJson,
	ServantJson,
	TsConfig,
} from "@servant/servant-data";
import { Path } from "@servant/servant-files";
import * as path from "path";

import { git } from "../../vcs";
import { resolveModule } from "../../resolver";

import {
	ServantInitProperty,
	ServantInitType,
	PrepareInitResults,
	InitParams,
	InitResults,
	InitCustomData,
} from "./types";
import { run, save } from "./generator";

export const InitParamsDefault: Required<InitParams> = {
	//type
	type: ServantInitType.Main,
	language: "ts",
	styles: "css",
	//package.json
	name: PackageJson.PackageJsonDefaults.name,
	version: PackageJson.PackageJsonDefaults.version,
	description: PackageJson.PackageJsonDefaults.description,
	keywords: PackageJson.PackageJsonDefaults.keywords,
	author: PackageJson.PackageJsonDefaults.author,
	license: PackageJson.PackageJsonDefaults.license,
	//servant.json
	moduleDirectory: "",
	modules: ServantJson.ServantJsonDefaults.modules,
	outputDirectory: ServantJson.ServantJsonOutputDefaults.directory,
	outputFilename: ServantJson.ServantJsonOutputDefaults.filename,
	entry: ServantJson.ServantJsonDefaults.entry,
	src: ServantJson.ServantJsonDefaults.src,
	tests: ServantJson.ServantJsonDefaults.tests,
	test: ServantJson.ServantJsonDefaults.test,
	clean: ServantJson.ServantJsonDefaults.clean,
	target: ServantJson.ServantJsonDefaults.target,
	registry: ServantJson.ServantJsonDefaults.registry as string,
	eslintType: "none",
	prettierType: "none",
	prettifySources: ServantJson.ServantJsonPrettifyDefaults.sources,
	prettifyExtensions: ServantJson.ServantJsonPrettifyDefaults.extensions,
	trancorderType: "none",
	trancorderTranslations: ServantJson.ServantJsonTrancorderDefaults.translations,
};

export function init(
	packageJson: PackageJson.PackageJsonInfo,
	entry: string,
	params: InitParams
): Promise<InitResults> {
	return new Promise((fulfill, reject) => {
		prepareInit(packageJson, entry).then((results) => {
			const answers = generateAnswers(params);
			run(answers, results).then(({ results, fn }) => {
				const { getCustomData } = fn;
				const { type, into } = getCustomData<InitCustomData>();

				//error state
				if (results.errors.length > 0) {
					//NOTE: Report only first error from all generators
					reject(results.errors[0]);
					return;
				}

				save(into, results)
					.then((saveResults) => initializeGit(into, type, saveResults))
					.then(({ results, fn, vcs }) => {
						//error state
						if (results.errors.length > 0) {
							//NOTE: Report only first error from all generators
							reject(results.errors[0]);
							return;
						}

						const packageJson = fn.getFile(PackageJson.PACKAGE_JSON)
							?.content as unknown as PackageJson.PackageJson;
						const packageJsonPath =
							fn.getFile(PackageJson.PACKAGE_JSON)?.absolutePath ?? "";
						const servantJsonPath =
							fn.getFile(ServantJson.SERVANT_JSON)?.absolutePath ?? "";
						const tsconfigPath =
							fn.getFile(TsConfig.TSCONFIG_JSON)?.absolutePath ?? null;
						const eslintrcPath =
							fn.getFile(EslintrcJson.ESLINTRC_JSON)?.absolutePath ?? null;
						const prettierrcPath =
							fn.getFile(PrettierrcJson.PRETTIERRC_JSON)?.absolutePath ?? null;
						const gitignorePath =
							fn.getFile(GitIgnore.GITIGNORE_FILE)?.absolutePath ?? null;

						const paths = results.paths.filter(
							(p) =>
								[
									packageJsonPath,
									servantJsonPath,
									tsconfigPath,
									eslintrcPath,
									prettierrcPath,
									gitignorePath,
								].includes(p) === false
						);

						fulfill(
							createResults(
								packageJson,
								packageJsonPath,
								servantJsonPath,
								tsconfigPath,
								eslintrcPath,
								prettierrcPath,
								gitignorePath,
								paths,
								vcs
							)
						);
					});
			});
		});
	});
}

export function prepareInit(
	packageJson: PackageJson.PackageJsonInfo,
	entry: string
): Promise<PrepareInitResults> {
	const name = path.basename(entry);
	const dependenciesToResolve = ["@typescript-eslint/eslint-plugin", "@typescript-eslint/parser"];

	return new Promise((fulfill) => {
		loadDependenciesInfo(packageJson.cwd, dependenciesToResolve).then((dependencies) => {
			fulfill({ entry, packageJson, name, dependencies });
		});
	});
}

//RESULTS

function createInitResults(name: string, packageJson: string, servantJson: string): InitResults {
	return {
		packageJsonProps: {
			prettier: false,
			eslintConfig: false,
		},
		packageJson: packageJson,
		servantJson: servantJson,
		tsconfigJson: null,
		gitignoreFile: null,
		eslintrcJson: null,
		prettierrcJson: null,
		paths: [],
		name: name,
		vcs: false,
	};
}

function createResults(
	packageJson: PackageJson.PackageJson,
	packageJsonPath: string,
	servantJsonPath: string,
	tsConfigPath: string | null,
	esLintRcPath: string | null,
	prettierRcPath: string | null,
	gitignoreFilePath: string | null,
	paths: Array<string>,
	vcs: boolean
): InitResults {
	const initResults = createInitResults(
		packageJson.name,
		Path.normalize(packageJsonPath),
		Path.normalize(servantJsonPath)
	);
	//for TS
	if (tsConfigPath) {
		initResults.tsconfigJson = Path.normalize(tsConfigPath);
	}
	//eslint rc file created
	if (esLintRcPath) {
		initResults.eslintrcJson = Path.normalize(esLintRcPath);
	}
	if (packageJson.eslintConfig) {
		initResults.packageJsonProps.eslintConfig = true;
	}
	//prettier rc file created
	if (prettierRcPath) {
		initResults.prettierrcJson = Path.normalize(prettierRcPath);
	}
	if (packageJson.prettier) {
		initResults.packageJsonProps.prettier = true;
	}
	//ignore
	if (gitignoreFilePath) {
		initResults.gitignoreFile = Path.normalize(gitignoreFilePath);
	}
	//paths
	initResults.paths = paths.map((file) => Path.normalize(file));
	//vcs
	initResults.vcs = vcs;

	return initResults;
}

//GIT

type GeneratorSaverResultsWithVcs = GeneratorSaverResults & {
	vcs: boolean;
};

function initializeGit(
	where: string,
	type: ServantInitType,
	{ results, fn }: GeneratorSaverResults
): Promise<GeneratorSaverResultsWithVcs> {
	return new Promise((resolve) => {
		//init git, .gitignore
		createGit(where, type)
			.then(() => filesGit(where, results.paths))
			.then((vcs) => resolve({ results, fn, vcs }));
	});
}

function createGit(where: string, type: ServantInitType): Promise<boolean> {
	return new Promise((resolve) => {
		//create git if necessary for main module
		if (type === ServantInitType.Main) {
			git.create(path.dirname(where)).then(resolve);
			return;
		}
		//check exists
		git.exists(path.dirname(where)).then(resolve);
	});
}

function filesGit(where: string, files: Array<string>): Promise<boolean> {
	return new Promise((resolve) => {
		git.add(where, files).then(resolve);
	});
}

//DEPENDENCIES

function loadDependenciesInfo(
	cwd: string,
	dependencies: Array<string>
): Promise<PrepareInitResults["dependencies"]> {
	return new Promise((resolve) => {
		const depsVersions = {};
		const promises: Promise<void>[] = [];

		dependencies.forEach((dep) => {
			const { path } = resolveModule(cwd, dep, `./${PackageJson.PACKAGE_JSON}`, true);
			promises.push(
				PackageJson.load(path, true)
					.then((info) => {
						depsVersions[dep] = `^${info.content.version}`;
					})
					.catch(() => {
						depsVersions[dep] = "*";
					})
			);
		});

		Promise.all(promises).finally(() => resolve(depsVersions));
	});
}

//ANSWERS

function generateAnswers(params: InitParams): GeneratorAnswer[] {
	const answers: GeneratorAnswer[] = [];

	addAnswer(answers, ServantInitProperty.Type, InitParamsDefault.type, params.type);
	addAnswer(answers, ServantInitProperty.Styles, InitParamsDefault.styles, params.styles);
	addAnswer(
		answers,
		ServantInitProperty.HasEntry,
		true.toString(),
		(params.entry !== false).toString()
	);
	addAnswer(
		answers,
		ServantInitProperty.Entry,
		InitParamsDefault.entry.toString(),
		params.entry?.toString()
	);
	addAnswer(answers, ServantInitProperty.Language, InitParamsDefault.language, params.language);
	addAnswer(answers, ServantInitProperty.Name, InitParamsDefault.name, params.name);
	addAnswer(answers, ServantInitProperty.Version, InitParamsDefault.version, params.version);
	addAnswer(
		answers,
		ServantInitProperty.Description,
		InitParamsDefault.description,
		params.description
	);
	addAnswer(
		answers,
		ServantInitProperty.Keywords,
		InitParamsDefault.keywords.join(","),
		params.keywords?.join(",")
	);
	addAnswer(answers, ServantInitProperty.Author, InitParamsDefault.author, params.author);
	addAnswer(answers, ServantInitProperty.License, InitParamsDefault.license, params.license);
	addAnswer(
		answers,
		ServantInitProperty.ModuleDirectory,
		InitParamsDefault.moduleDirectory,
		params.moduleDirectory
	);
	addAnswer(
		answers,
		ServantInitProperty.Modules,
		InitParamsDefault.modules.join(","),
		params.modules?.join(",")
	);
	addAnswer(
		answers,
		ServantInitProperty.OutputDirectory,
		InitParamsDefault.outputDirectory,
		params.outputDirectory
	);
	addAnswer(
		answers,
		ServantInitProperty.OutputFilename,
		InitParamsDefault.outputFilename,
		params.outputFilename
	);
	addAnswer(
		answers,
		ServantInitProperty.Src,
		InitParamsDefault.src.join(","),
		params.src?.join(",")
	);
	addAnswer(
		answers,
		ServantInitProperty.Tests,
		InitParamsDefault.tests.join(","),
		params.tests?.join(",")
	);
	addAnswer(answers, ServantInitProperty.Test, InitParamsDefault.test, params.test);
	addAnswer(
		answers,
		ServantInitProperty.Clean,
		InitParamsDefault.clean.join(","),
		params.clean?.join(",")
	);
	addAnswer(answers, ServantInitProperty.Target, InitParamsDefault.target, params.target);
	addAnswer(answers, ServantInitProperty.Registry, InitParamsDefault.registry, params.registry);
	addAnswer(
		answers,
		ServantInitProperty.EslintExpose,
		false.toString(),
		Boolean(params.eslintType !== "none" && params.eslintType).toString()
	);
	addAnswer(
		answers,
		ServantInitProperty.EslintExposeType,
		InitParamsDefault.eslintType,
		params.eslintType
	);
	addAnswer(
		answers,
		ServantInitProperty.PrettierExpose,
		false.toString(),
		Boolean(params.prettierType !== "none" && params.prettierType).toString()
	);
	addAnswer(
		answers,
		ServantInitProperty.PrettierExposeType,
		InitParamsDefault.prettierType,
		params.prettierType
	);
	addAnswer(
		answers,
		ServantInitProperty.PrettierPrettifySources,
		InitParamsDefault.prettifySources.join(","),
		params.prettifySources?.join(",")
	);
	addAnswer(
		answers,
		ServantInitProperty.PrettierPrettifyExtensions,
		InitParamsDefault.prettifyExtensions.join(","),
		params.prettifyExtensions?.join(",")
	);
	addAnswer(
		answers,
		ServantInitProperty.TrancorderExpose,
		false.toString(),
		Boolean(params.trancorderType !== "none" && params.trancorderType).toString()
	);
	addAnswer(
		answers,
		ServantInitProperty.TrancorderTranslations,
		InitParamsDefault.trancorderTranslations,
		params.trancorderTranslations
	);

	return answers;
}

function addAnswer(
	answers: GeneratorAnswer[],
	id: ServantInitProperty,
	def: string,
	value?: string
) {
	answers.push({
		id,
		value: value ?? def,
	});
}
