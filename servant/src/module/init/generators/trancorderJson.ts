import { TrancorderJson } from "@servant/servant-data";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorNextHandler,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorQuestions,
	GeneratorLoaded,
} from "@servant/servant-generators";

import { InitParamsDefault } from "../init";
import { categories, getValueOrDefault } from "../utils";
import { ServantInitProperty, ServantQuestionCategory } from "../types";
import { Path } from "@servant/servant-files";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-trancorder-json-generator",
	description:
		"This is generator that is used to generate trancorder.json of servant init command.",
	path: "internal://servant/servant-trancorder-json-generator",
	entry: "bundled",
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [
		//trancorder.json questions
		{
			type: "select",
			category: ServantQuestionCategory.TrancorderConfig,
			question: 'Do you want to use "trancorder" in your project?',
			id: ServantInitProperty.TrancorderExpose,
			tip: 'If choose "No", config for trancorder will not created in your project! You can create config file later to allow "trancorder" validator.',
			values: [
				{ label: "Yes", value: () => true.toString() },
				{ label: "No", value: () => false.toString() },
			],
		},
		{
			type: "string",
			category: ServantQuestionCategory.TrancorderConfig,
			question: "What is directory where localisation files will be stored?",
			id: ServantInitProperty.TrancorderTranslations,
			defaultValue: {
				label: "",
				value: (fn) => {
					const sources = fn.getAnswer(ServantInitProperty.Src);
					if (sources) {
						const dir = sources.value.split(",")[0];
						const rootDir = Path.directory("./", dir);
						return Path.join(rootDir, ["translations"])[0];
					}
					return "src/translations";
				},
			},
			tip: 'For example "src/translations". Its recommended to have locales in directory that is subdirectory of one of "src" folder!',
			condition: {
				and: {
					[ServantInitProperty.TrancorderExpose]: true.toString(),
				},
			},
		},
	];
};

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	fn: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	const trancorder = getValueOrDefault(
		getAnswer,
		ServantInitProperty.TrancorderExpose,
		false.toString()
	) as string;
	const trancorderTranslations = getValueOrDefault(
		getAnswer,
		ServantInitProperty.TrancorderTranslations,
		InitParamsDefault.trancorderTranslations
	);

	if (trancorder === true.toString()) {
		const src = getAnswer(ServantInitProperty.Src);
		const sources = src ? src.value.split(",") : undefined;
		const file = TrancorderJson.createDefault();
		file.translations = trancorderTranslations;
		if (sources) {
			file.sources = sources;
		}
		fn.createFile(TrancorderJson.TRANCORDER_JSON, file);
	}

	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: categories,
	},
};
