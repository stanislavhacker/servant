import { EslintrcJson } from "@servant/servant-data";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorNextHandler,
	GeneratorQuestions,
} from "@servant/servant-generators";

import { InitParamsDefault } from "../init";
import { categories, getValueOrDefault } from "../utils";
import { ServantInitProperty, ServantQuestionCategory, InitParams } from "../types";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-eslintrc-json-generator",
	description:
		"This is generator that is used to generate eslintrc.json of servant init command.",
	path: "internal://servant/servant-eslintrc-json-generator",
	entry: "bundled",
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [
		//eslintrc.json questions
		{
			type: "select",
			category: ServantQuestionCategory.EslintConfig,
			question: 'Do you want to use "eslint" in your project?',
			id: ServantInitProperty.EslintExpose,
			tip: 'If choose "No", config for eslint will not created in your project! You can create config file later to allow "eslint" validator.',
			values: [
				{ label: "Yes", value: () => true.toString() },
				{ label: "No", value: () => false.toString() },
			],
		},
		{
			type: "select",
			category: ServantQuestionCategory.EslintConfig,
			question: 'Where settings for "eslint" will be created?',
			id: ServantInitProperty.EslintExposeType,
			defaultValue: { label: "", value: () => InitParamsDefault.eslintType as string },
			values: [
				{
					label: '".eslintrc.json" - Single file for eslint configuration.',
					value: () => ".eslintrc.json" as InitParams["eslintType"] as string,
				},
				{
					label: '"eslintConfig" - Property in package.json',
					value: () => "eslintConfig" as InitParams["eslintType"] as string,
				},
			],
			condition: {
				and: {
					[ServantInitProperty.EslintExpose]: true.toString(),
				},
			},
		},
	];
};

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	fn: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	const eslint = getValueOrDefault(
		getAnswer,
		ServantInitProperty.EslintExpose,
		false.toString()
	) as string;
	const eslintExpose = getValueOrDefault(
		getAnswer,
		ServantInitProperty.EslintExposeType,
		InitParamsDefault.eslintType
	);

	if (eslint === true.toString() && eslintExpose === ".eslintrc.json") {
		fn.createFile(EslintrcJson.ESLINTRC_JSON, EslintrcJson.createDefault(true));
	}

	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: categories,
	},
};
