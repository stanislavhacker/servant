import { ServantJson } from "@servant/servant-data";
import { JS, TS, Path } from "@servant/servant-files";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorNextHandler,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorQuestions,
	GeneratorLoaded,
} from "@servant/servant-generators";
import * as path from "path";

import { InitParamsDefault } from "../init";
import { sanitizeDirname, getValueOrDefault, getArray, categories } from "../utils";
import {
	ServantStyles,
	ServantInitProperty,
	ServantInitType,
	ServantQuestionCategory,
} from "../types";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-servant-json-generator",
	description: "This is generator that is used to generate servant.json of servant init command.",
	path: "internal://servant/servant-servant-json-generator",
	entry: "bundled",
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	const module = "module.*";

	return [
		//servant.json questions
		{
			type: "select",
			category: ServantQuestionCategory.ServantJson,
			question: "Has module entry point?",
			id: ServantInitProperty.HasEntry,
			values: [
				{ label: "Yes", value: () => true.toString() },
				{ label: "No", value: () => false.toString() },
			],
		},
		{
			type: "select",
			category: ServantQuestionCategory.ServantJson,
			question: "What will be module used for?",
			id: ServantInitProperty.Target,
			values: [
				{ label: "Mainly for WEB browsers.", value: () => ServantJson.ModuleTarget.web },
				{
					label: "WEBPAGE root for deploy on server.",
					value: () => ServantJson.ModuleTarget.web_page,
				},
				{ label: "Mainly as a NODE module.", value: () => ServantJson.ModuleTarget.node },
				{
					label: "NODE module intended for running as command line program.",
					value: () => ServantJson.ModuleTarget.node_cli,
				},
			],
		},
		{
			type: "string",
			category: ServantQuestionCategory.ServantJson,
			question: "What pattern for submodules will be used?",
			id: ServantInitProperty.Modules,
			defaultValue: { label: "", value: () => `${module}/` },
			tip: "For example module.*/, multiple values separate by comma (,).",
			condition: {
				and: {
					[ServantInitProperty.Type]: ServantInitType.Main,
				},
			},
		},
		{
			type: "string",
			category: ServantQuestionCategory.ServantJson,
			question: "What will be name for submodule directory?",
			id: ServantInitProperty.ModuleDirectory,
			defaultValue: {
				label: "",
				value: ({ getAnswer }) => {
					const answer = getAnswer(ServantInitProperty.Name);
					return module.replace("*", sanitizeDirname(answer ? answer.value : ""));
				},
			},
			tip: "Must match pattern defined in modules in your main servant.json!",
			condition: {
				and: {
					[ServantInitProperty.Type]: ServantInitType.Submodule,
				},
			},
		},
		//entry
		{
			type: "string",
			category: ServantQuestionCategory.ServantJson,
			question: "What file is an entry point for build?",
			id: ServantInitProperty.Entry,
			defaultValue: { label: "", value: () => InitParamsDefault.entry.toString() },
			condition: {
				and: {
					[ServantInitProperty.HasEntry]: true.toString(),
				},
			},
		},
		{
			type: "string",
			category: ServantQuestionCategory.ServantJson,
			question: "In which directory will be output saved?",
			id: ServantInitProperty.OutputDirectory,
			defaultValue: { label: "", value: () => InitParamsDefault.outputDirectory },
			condition: {
				and: {
					[ServantInitProperty.HasEntry]: true.toString(),
				},
			},
		},
		{
			type: "string",
			category: ServantQuestionCategory.ServantJson,
			question: "What will be name of file for build module?",
			id: ServantInitProperty.OutputFilename,
			defaultValue: { label: "", value: () => InitParamsDefault.outputFilename },
			condition: {
				and: {
					[ServantInitProperty.HasEntry]: true.toString(),
				},
			},
		},
		//entry and web
		{
			type: "select",
			category: ServantQuestionCategory.ServantJson,
			question: "What do you want to use for writing css styles?",
			id: ServantInitProperty.Styles,
			values: [
				{ label: "CSS - plain css", value: () => ServantStyles.Css },
				{
					label: "LESS - It's CSS, with just a little more.",
					value: () => ServantStyles.Less,
				},
				{ label: "SASS - CSS with superpowers.", value: () => ServantStyles.Sass },
			],
			condition: {
				or: {
					target_is_web: {
						and: {
							[ServantInitProperty.HasEntry]: true.toString(),
							[ServantInitProperty.Target]: ServantJson.ModuleTarget.web,
						},
					},
					target_is_web_page: {
						and: {
							[ServantInitProperty.HasEntry]: true.toString(),
							[ServantInitProperty.Target]: ServantJson.ModuleTarget.web_page,
						},
					},
				},
			},
		},
		//defaults
		{
			type: "string",
			category: ServantQuestionCategory.ServantJson,
			question: "What pattern for source files will be used?",
			id: ServantInitProperty.Src,
			defaultValue: { label: "", value: () => InitParamsDefault.src.join(",") },
			tip: "For example src/**/* or src/**/*.js, multiple values separate by comma (,).",
		},
		{
			type: "string",
			category: ServantQuestionCategory.ServantJson,
			question: "What pattern for tests files will be used?",
			id: ServantInitProperty.Tests,
			defaultValue: { label: "", value: () => InitParamsDefault.tests.join(",") },
			tip: "For example tests/**/* or tests/**/*.specs.js, multiple values separate by comma (,).",
		},
		{
			type: "string",
			category: ServantQuestionCategory.ServantJson,
			question: "What extensions will be also clean with clean command?",
			id: ServantInitProperty.Clean,
			defaultValue: { label: "", value: () => "" },
			tip: "For example js or d.ts, multiple values separate by comma (,).",
		},
		{
			type: "string",
			category: ServantQuestionCategory.ServantJson,
			question: "What registry you will be used?",
			id: ServantInitProperty.Registry,
			defaultValue: { label: "", value: () => InitParamsDefault.registry },
			tip: "Default registry is empty. Servant will be publish into default NPM registry.",
		},
	] as GeneratorQuestions;
};

const SELF = "./";

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	{ createFile }: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	const type = getValueOrDefault(getAnswer, ServantInitProperty.Type, InitParamsDefault.type);
	const modules = getArray(
		getValueOrDefault(
			getAnswer,
			ServantInitProperty.Modules,
			InitParamsDefault.modules?.join(",")
		)
	);
	const name = getValueOrDefault(getAnswer, ServantInitProperty.Name, InitParamsDefault.name);
	const target = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Target,
		InitParamsDefault.target
	);
	const hasEntry = getValueOrDefault(getAnswer, ServantInitProperty.HasEntry, true.toString());
	const entry = getValueOrDefault(getAnswer, ServantInitProperty.Entry, InitParamsDefault.entry);
	const language = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Language,
		InitParamsDefault.language
	);
	const filename = getValueOrDefault(
		getAnswer,
		ServantInitProperty.OutputFilename,
		InitParamsDefault.outputFilename
	);
	const directory = getValueOrDefault(
		getAnswer,
		ServantInitProperty.OutputDirectory,
		InitParamsDefault.outputDirectory
	);
	const registry = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Registry,
		InitParamsDefault.registry
	);
	const clean = getArray(
		getValueOrDefault(getAnswer, ServantInitProperty.Clean, InitParamsDefault.clean?.join(","))
	);
	const src = getArray(
		getValueOrDefault(getAnswer, ServantInitProperty.Src, InitParamsDefault.src?.join(","))
	);
	const tests = getArray(
		getValueOrDefault(getAnswer, ServantInitProperty.Tests, InitParamsDefault.tests?.join(","))
	);
	const test = getValueOrDefault(getAnswer, ServantInitProperty.Test, InitParamsDefault.test);
	const prettierType = getValueOrDefault(
		getAnswer,
		ServantInitProperty.PrettierExpose,
		InitParamsDefault.prettierType
	);
	const prettifyExtensions = getArray(
		getValueOrDefault(
			getAnswer,
			ServantInitProperty.PrettierPrettifyExtensions,
			InitParamsDefault.prettifyExtensions?.join(",")
		)
	);
	const prettifySources = getArray(
		getValueOrDefault(
			getAnswer,
			ServantInitProperty.PrettierPrettifySources,
			InitParamsDefault.prettifySources?.join(",")
		)
	);

	const servantJson = ServantJson.create(name);

	//ENTRY
	if (hasEntry === false.toString()) {
		servantJson.entry = false;
	} else {
		servantJson.entry = entry;
	}

	//TARGET
	servantJson.target = target;
	if (
		servantJson.target === ServantJson.ModuleTarget.web ||
		servantJson.target === ServantJson.ModuleTarget.web_page
	) {
		servantJson.test = updateTestEntry(test, tests);
	}

	//WEBPAGE
	if (target === ServantJson.ModuleTarget.web_page) {
		servantJson.webpage = {
			title: name,
		};
	}

	//SOURCES
	if (src && src.length > 0) {
		servantJson.src = src;
	}

	//TESTS
	if (tests && tests.length > 0) {
		servantJson.tests = tests;
	}

	//CLEAN
	servantJson.clean = [...new Set((clean || []).concat([TS.MAP]))];

	//RESOLVE
	if (language === "js") {
		servantJson.resolve = JS.EXT;
	}
	if (language === "ts") {
		servantJson.resolve = TS.EXT;
	}

	//OUTPUT
	if (filename) {
		servantJson.output = servantJson.output || ({} as ServantJson.ServantOutput);
		servantJson.output.filename = filename;
	}
	if (directory) {
		servantJson.output = servantJson.output || ({} as ServantJson.ServantOutput);
		servantJson.output.directory = directory;
	}

	//PUBLISH
	if (registry) {
		servantJson.registry = registry;
	}

	//VALIDATE
	if (prettierType === "prettier" || prettierType === ".prettierrc.json") {
		if (prettifyExtensions && prettifyExtensions.length) {
			servantJson.prettify = servantJson.prettify || {};
			servantJson.prettify.extensions = prettifyExtensions;
		}
		if (prettifySources && prettifySources.length) {
			servantJson.prettify = servantJson.prettify || {};
			servantJson.prettify.sources = prettifySources;
		}
	}

	//MAIN
	if (type === ServantInitType.Main && modules.length > 0) {
		servantJson.modules = [...(hasEntry && !modules.includes(SELF) ? [SELF] : []), ...modules];
	}

	//create file
	createFile(ServantJson.SERVANT_JSON, servantJson as unknown as Record<string, unknown>);

	next("success");
};

function updateTestEntry(test: string, tests: Array<string>): string {
	if (tests.length > 0) {
		const [dir, entry] = [path.normalize(path.dirname(test)), path.basename(test)];
		if (!tests.some((test) => test.indexOf(dir) === 0)) {
			const newTestEntryDir = Path.directory("", tests[0]);
			return Path.normalize(`./${path.join(newTestEntryDir, entry)}`);
		}
	}
	return test;
}

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: categories,
	},
};
