import { GitIgnore, ServantJson } from "@servant/servant-data";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorNextHandler,
	GeneratorQuestions,
} from "@servant/servant-generators";

import { InitParamsDefault } from "../init";
import { categories, getValueOrDefault } from "../utils";
import { ServantInitProperty } from "../types";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-gitignore-generator",
	description: "This is generator that is used to generate .gitignore of servant init command.",
	path: "internal://servant/servant-gitignore-generator",
	entry: "bundled",
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [];
};

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	fn: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	const language = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Language,
		InitParamsDefault.language
	);
	const servantJsonFile = fn.getFile(ServantJson.SERVANT_JSON);
	const servantJson = servantJsonFile
		? (servantJsonFile.content as unknown as ServantJson.ServantJson)
		: null;

	if (servantJson) {
		fn.createFile(
			GitIgnore.GITIGNORE_FILE,
			GitIgnore.serialize(GitIgnore.init(servantJson, language))
		);
		next("success");
	} else {
		next("error");
	}
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: categories,
	},
};
