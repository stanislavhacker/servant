import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorNextHandler,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorQuestions,
	GeneratorLoaded,
} from "@servant/servant-generators";

import {
	ServantInitProperty,
	ServantInitType,
	ServantQuestionCategory,
	ServantLanguage,
} from "../types";
import { categories } from "../utils";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-module-generator",
	description: "This is generator that is used to generate module of servant init command.",
	path: "internal://servant/servant-module-generator",
	entry: "bundled",
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [
		//main questions
		{
			type: "select",
			category: ServantQuestionCategory.General,
			question: "What do you want to init?",
			id: ServantInitProperty.Type,
			values: [
				{ label: "Init main package.", value: () => ServantInitType.Main },
				{ label: "Add submodule.", value: () => ServantInitType.Submodule },
			],
		},
		{
			type: "select",
			category: ServantQuestionCategory.General,
			question: "What will be module programming language?",
			id: ServantInitProperty.Language,
			values: [
				{ label: "TypeScript", value: () => ServantLanguage.TypeScript },
				{ label: "JavaScript", value: () => ServantLanguage.JavaScript },
			],
		},
	];
};

export const generator = <D>(
	data: GeneratorDataHandlers,
	fn: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: categories,
	},
};
