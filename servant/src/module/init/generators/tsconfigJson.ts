import { TsConfig } from "@servant/servant-data";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorNextHandler,
	GeneratorQuestions,
} from "@servant/servant-generators";
import { categories, getValueOrDefault } from "../utils";
import { ServantInitProperty } from "../types";
import { InitParamsDefault } from "../init";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-tsconfig-generator",
	description:
		"This is generator that is used to generate tsconfig.json of servant init command.",
	path: "internal://servant/servant-tsconfig-generator",
	entry: "bundled",
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [];
};

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	fn: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	const language = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Language,
		InitParamsDefault.language
	);

	if (language === "ts") {
		fn.createFile(TsConfig.TSCONFIG_JSON, TsConfig.create());
	}
	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: categories,
	},
};
