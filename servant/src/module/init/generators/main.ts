import * as path from "path";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorNextHandler,
	GeneratorQuestions,
} from "@servant/servant-generators";

import { manifest as moduleManifest, loaded as moduleLoaded } from "./module";
import { manifest as packageJsonManifest, loaded as packageJsonLoaded } from "./packageJson";
import { manifest as servantJsonManifest, loaded as servantJsonLoaded } from "./servantJson";
import { manifest as eslintrcManifest, loaded as eslintrcLoaded } from "./eslintrcJson";
import { manifest as prettierrcManifest, loaded as prettierrcLoaded } from "./prettierrcJson";
import { manifest as trancorderManifest, loaded as trancorderLoaded } from "./trancorderJson";
import { manifest as gitignoreManifest, loaded as gitignoreLoaded } from "./gitignore";
import { manifest as tsconfigManifest, loaded as tsconfigLoaded } from "./tsconfigJson";

import { manifest as bootstrapManifest, loaded as bootstrapLoaded } from "../bootstrap";
import { InitParams, PrepareInitResults, ServantInitProperty, ServantInitType } from "../types";
import { categories, getValueOrDefault } from "../utils";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-init-generator",
	description:
		"This is generator that is used to generate whole structure of module with init command.",
	path: "internal://servant/servant-init-generator",
	entry: "bundled",
	use: [
		moduleManifest.name,
		eslintrcManifest.name,
		prettierrcManifest.name,
		servantJsonManifest.name,
		packageJsonManifest.name,
		trancorderManifest.name,
		gitignoreManifest.name,
		tsconfigManifest.name,
		bootstrapManifest.name,
	],
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [];
};

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	{ updateCustomData, createMessage }: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>,
	{ entry }: PrepareInitResults
) => {
	const type = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Type,
		ServantInitType.Main
	) as ServantInitType;
	const name = getValueOrDefault(getAnswer, ServantInitProperty.Name, "") as string;
	const dir = getValueOrDefault(getAnswer, ServantInitProperty.ModuleDirectory, "") as string;
	const into = type === ServantInitType.Submodule ? path.join(entry, dir) : entry;

	updateCustomData({ into, type });

	createMessage("eslint-in-package_json", {
		type: "info",
		title: `Config for "eslint"`,
		text: `was created into package.json file directly.`,
		condition: {
			and: {
				[ServantInitProperty.EslintExposeType]:
					"eslintConfig" as Required<InitParams>["eslintType"],
			},
		},
	});
	createMessage("prettier-in-package_json", {
		type: "info",
		title: `Config for "prettier"`,
		text: `was created into package.json file directly.`,
		condition: {
			and: {
				[ServantInitProperty.PrettierExposeType]:
					"prettier" as Required<InitParams>["prettierType"],
			},
		},
	});

	createMessage("created-ok", {
		type: "success",
		text: `Package "${name}" was successfully created.`,
	});

	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [
		moduleLoaded,
		eslintrcLoaded,
		prettierrcLoaded,
		servantJsonLoaded,
		packageJsonLoaded,
		trancorderLoaded,
		gitignoreLoaded,
		tsconfigLoaded,
		bootstrapLoaded,
	],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: categories,
	},
};
