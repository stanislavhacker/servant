import { EslintrcJson, PackageJson, PrettierrcJson } from "@servant/servant-data";
import { Extensions, JS, Path, TS } from "@servant/servant-files";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorNextHandler,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorQuestions,
	GeneratorLoaded,
} from "@servant/servant-generators";
import * as path from "path";

import { InitParamsDefault } from "../init";
import { categories, getArray, getValueOrDefault } from "../utils";
import {
	ServantInitProperty,
	ServantInitType,
	ServantQuestionCategory,
	PrepareInitResults,
} from "../types";
import { Commands } from "../../../commands/index";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-package-json-generator",
	description: "This is generator that is used to generate package.json of servant init command.",
	path: "internal://servant/servant-package-json-generator",
	entry: "bundled",
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [
		//package.json questions
		{
			type: "string",
			category: ServantQuestionCategory.PackageJson,
			question: "What will be your package name?",
			id: ServantInitProperty.Name,
			defaultValue: {
				label: "",
				value: ({ getProp }) => getProp("name") || InitParamsDefault.name,
			},
		},
		{
			type: "string",
			category: ServantQuestionCategory.PackageJson,
			question: "What will be your package description?",
			id: ServantInitProperty.Description,
			defaultValue: { label: "", value: () => InitParamsDefault.description },
		},
		{
			type: "string",
			category: ServantQuestionCategory.PackageJson,
			question: "What do you have starting version?",
			id: ServantInitProperty.Version,
			defaultValue: { label: "", value: () => InitParamsDefault.version },
		},
		{
			type: "string",
			category: ServantQuestionCategory.PackageJson,
			question: "What keywords do you want to have?",
			id: ServantInitProperty.Keywords,
			defaultValue: { label: "", value: () => InitParamsDefault.keywords.join(", ") },
		},
		{
			type: "string",
			category: ServantQuestionCategory.PackageJson,
			question: "Who is the author of package?",
			id: ServantInitProperty.Author,
			defaultValue: { label: "", value: () => InitParamsDefault.author },
		},
		{
			type: "string",
			category: ServantQuestionCategory.PackageJson,
			question: "What licencing do you have?",
			id: ServantInitProperty.License,
			defaultValue: { label: "", value: () => InitParamsDefault.license },
		},
	];
};

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	{ createFile }: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>,
	props: PrepareInitResults
) => {
	const { packageJson, dependencies } = props;

	const outputDirectory = getValueOrDefault(
		getAnswer,
		ServantInitProperty.OutputDirectory,
		InitParamsDefault.outputDirectory
	);
	const hasEntry = getValueOrDefault(getAnswer, ServantInitProperty.HasEntry, true.toString());
	const entry = getValueOrDefault(getAnswer, ServantInitProperty.Entry, InitParamsDefault.entry);
	const type = getValueOrDefault(getAnswer, ServantInitProperty.Type, InitParamsDefault.type);

	const eslint = getValueOrDefault(getAnswer, ServantInitProperty.EslintExpose, false.toString());
	const prettier = getValueOrDefault(
		getAnswer,
		ServantInitProperty.PrettierExpose,
		false.toString()
	);
	const eslintExpose = getValueOrDefault(
		getAnswer,
		ServantInitProperty.EslintExposeType,
		InitParamsDefault.eslintType
	);
	const prettierExpose = getValueOrDefault(
		getAnswer,
		ServantInitProperty.PrettierExposeType,
		InitParamsDefault.prettierType
	);

	const name = getValueOrDefault(getAnswer, ServantInitProperty.Name, InitParamsDefault.name);
	const version = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Version,
		InitParamsDefault.version
	);
	const license = getValueOrDefault(
		getAnswer,
		ServantInitProperty.License,
		InitParamsDefault.license
	);
	const author = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Author,
		InitParamsDefault.author
	);
	const description = getValueOrDefault(
		getAnswer,
		ServantInitProperty.Description,
		InitParamsDefault.description
	);
	const keywords = getArray(
		getValueOrDefault(
			getAnswer,
			ServantInitProperty.Keywords,
			InitParamsDefault.keywords?.join(",")
		)
	);

	const createdPackageJson = PackageJson.create(name, version);

	//MAIN FIELDS
	createdPackageJson.license = license;
	createdPackageJson.author = author;
	createdPackageJson.keywords = keywords;
	createdPackageJson.description = description;

	//DEFAULT FILES
	const files = Path.normalize(Path.patterns.all(outputDirectory));
	createdPackageJson.files = [files, "README.md", "LICENSE"];

	//ENTRY NOT DISABLED
	if (hasEntry === true.toString() && typeof entry === "string") {
		const extension = path.extname(entry);
		const filename = entry.replace(extension, "");
		createdPackageJson.main = JS.toJS(filename);
		createdPackageJson.types = Extensions.replace(createdPackageJson.main, TS.DECLARATION);
	}
	//ENTRY DISABLED
	if (hasEntry === false.toString()) {
		createdPackageJson.main = "";
	}

	//ESLINT
	if (eslint === true.toString() && eslintExpose === "eslintConfig") {
		createdPackageJson.eslintConfig = EslintrcJson.createDefault(type === ServantInitType.Main);
	}
	if (eslint === true.toString()) {
		const dep = createdPackageJson.devDependencies || (createdPackageJson.devDependencies = {});
		dep["@typescript-eslint/eslint-plugin"] =
			dependencies["@typescript-eslint/eslint-plugin"] || "*";
		dep["@typescript-eslint/parser"] = dependencies["@typescript-eslint/parser"] || "*";
	}

	//PRETTIER
	if (prettier === true.toString() && prettierExpose === "prettier") {
		createdPackageJson.prettier = PrettierrcJson.createDefault();
	}

	//SCRIPTS
	fillScripts(packageJson, createdPackageJson, type);

	createFile(PackageJson.PACKAGE_JSON, createdPackageJson as unknown as Record<string, unknown>);

	next("success");
};

function fillScripts(
	servantPackageJson: PackageJson.PackageJsonInfo,
	createdPackageJson,
	type: ServantInitType
) {
	const name = Object.keys(servantPackageJson.content.bin || {})[0];

	//only for main type
	if (type === ServantInitType.Main) {
		//add self as dependency
		const dep = createdPackageJson.devDependencies || (createdPackageJson.devDependencies = {});
		dep[servantPackageJson.content.name] = `^${servantPackageJson.content.version}`;
		//Scripts
		const scripts = (createdPackageJson.scripts = {});
		//servant only
		addScript(scripts, name, name, [], []);
		addScript(scripts, name, "clean", ["clean"], []);
		//builds
		addScript(scripts, name, "build", ["clean", "build"], []);
		addScript(scripts, name, "rebuild", ["clean", "build"], [["--changed", null]]);
		//tests
		addScript(scripts, name, "test", ["tests"], []);
		addScript(scripts, name, "retest", ["tests"], [["--changed", null]]);
		//validate
		addScript(scripts, name, "validate", ["validate"], []);
		addScript(scripts, name, "fix", ["validate"], [["--fix", null]]);
		//preview
		addScript(scripts, name, "preview", [], [["--server", null]]);
		//publish
		addScript(scripts, name, "make-package", ["clean", "build", "publish"], []);
		addScript(
			scripts,
			name,
			"make-publish",
			["clean", "build", "publish"],
			[
				["--production", null],
				["--tag", "latest"],
			]
		);
		//install, update
		addScript(scripts, name, "make-install", ["install"], []);
		addScript(scripts, name, "make-update", ["update"], []);
	}
	//only for submodule type
	if (type === ServantInitType.Submodule) {
		//Scripts
		const scripts = (createdPackageJson.scripts = {});
		//servant only
		addScript(scripts, name, "clean", ["clean"], []);
		//builds
		addScript(scripts, name, "build", ["clean", "build"], []);
		addScript(scripts, name, "rebuild", ["clean", "build"], [["--changed", null]]);
		//tests
		addScript(scripts, name, "test", ["tests"], []);
		addScript(scripts, name, "retest", ["tests"], [["--changed", null]]);
		//validate
		addScript(scripts, name, "validate", ["validate"], []);
		addScript(scripts, name, "fix", ["validate"], [["--fix", null]]);
	}
}

function addScript(
	scripts: PackageJson.Scripts,
	servant: string,
	name: string,
	commands: Array<Commands>,
	flags: Array<[string, string | null]>
) {
	const script = [
		`${servant}`,
		`${commands.join(" ")}`,
		`${flags.map((flag) => (flag[1] ? `${flag[0]} "${flag[1]}"` : flag[0])).join(" ")}`,
	].filter((item) => Boolean(item));

	scripts[name] = script.join(" ");
}

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: categories,
	},
};
