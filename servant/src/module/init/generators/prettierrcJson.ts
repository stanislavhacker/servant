import { PrettierrcJson } from "@servant/servant-data";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorNextHandler,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorQuestions,
	GeneratorLoaded,
} from "@servant/servant-generators";

import { InitParamsDefault } from "../init";
import { categories, getValueOrDefault } from "../utils";
import { ServantInitProperty, ServantQuestionCategory, InitParams } from "../types";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-prettierrc-json-generator",
	description:
		"This is generator that is used to generate prettierrc.json of servant init command.",
	path: "internal://servant/servant-prettierrc-json-generator",
	entry: "bundled",
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [
		//prettierrc.json questions
		{
			type: "select",
			category: ServantQuestionCategory.PrettierConfig,
			question: 'Do you want to use "prettier" in your project?',
			id: ServantInitProperty.PrettierExpose,
			tip: 'If choose "No", config for prettier will not created in your project! You can create config file later to allow "prettier" validator.',
			values: [
				{ label: "Yes", value: () => true.toString() },
				{ label: "No", value: () => false.toString() },
			],
		},
		{
			type: "select",
			category: ServantQuestionCategory.PrettierConfig,
			question: 'Where settings for "prettier" will be created?',
			id: ServantInitProperty.PrettierExposeType,
			defaultValue: { label: "", value: () => InitParamsDefault.prettierType as string },
			values: [
				{
					label: '".prettierrc.json" - Single file for prettier configuration.',
					value: () => ".prettierrc.json" as InitParams["prettierType"] as string,
				},
				{
					label: '"prettier" - Property in package.json',
					value: () => "prettier" as InitParams["prettierType"] as string,
				},
			],
			condition: {
				and: {
					[ServantInitProperty.PrettierExpose]: true.toString(),
				},
			},
		},
		{
			type: "string",
			category: ServantQuestionCategory.PrettierConfig,
			question: "What pattern for validation additional sources files will be used?",
			id: ServantInitProperty.PrettierPrettifySources,
			defaultValue: { label: "", value: () => InitParamsDefault.prettifySources.join(",") },
			tip: "For example doc/**/* or fixtures/**/*.json, multiple values separate by comma (,). Used only files supported by Prettier!",
			condition: {
				and: {
					[ServantInitProperty.PrettierExpose]: true.toString(),
				},
			},
		},
		{
			type: "string",
			category: ServantQuestionCategory.PrettierConfig,
			question: "What extensions will be also checked by prettier with validate command?",
			id: ServantInitProperty.PrettierPrettifyExtensions,
			defaultValue: {
				label: "",
				value: () => InitParamsDefault.prettifyExtensions.join(","),
			},
			tip: "For example md or json, multiple values separate by comma (,).",
			condition: {
				and: {
					[ServantInitProperty.PrettierExpose]: true.toString(),
				},
			},
		},
	];
};

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	fn: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	const prettier = getValueOrDefault(
		getAnswer,
		ServantInitProperty.PrettierExpose,
		false.toString()
	) as string;
	const prettierExpose = getValueOrDefault(
		getAnswer,
		ServantInitProperty.PrettierExposeType,
		InitParamsDefault.prettierType
	);

	if (prettier === true.toString() && prettierExpose === ".prettierrc.json") {
		fn.createFile(PrettierrcJson.PRETTIERRC_JSON, PrettierrcJson.createDefault());
	}

	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: categories,
	},
};
