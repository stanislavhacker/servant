import { FnGetAnswer } from "@servant/servant-generators";

import { ServantInitProperty, ServantQuestionCategory } from "./types";

export function sanitizeDirname(name: string) {
	let dirname = name;
	dirname = dirname.replace(/[^a-z0-9/_\-.]/gi, " ");
	dirname = dirname.replace(/\//gi, ".");
	dirname = dirname.trim();
	dirname = dirname.replace(/ /gi, "_").toLowerCase();

	return dirname;
}

export function getValueOrDefault<T>(
	getAnswer: FnGetAnswer,
	property: ServantInitProperty,
	def: T
): T {
	const nameValue = getAnswer(property);
	return (nameValue ? nameValue.value : def) as T;
}

export function getArray(data: string): Array<string> {
	return data
		.split(",")
		.map((item) => item.trim())
		.filter((item) => item.length > 0);
}

export const categories = {
	[ServantQuestionCategory.General]: "grey",
	[ServantQuestionCategory.PackageJson]: "green",
	[ServantQuestionCategory.ServantJson]: "blue",
	[ServantQuestionCategory.EslintConfig]: "cyan",
	[ServantQuestionCategory.PrettierConfig]: "yellow",
};
