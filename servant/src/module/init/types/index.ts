import { PackageJson, ServantJson } from "@servant/servant-data";

export enum ServantQuestionCategory {
	None = "none",
	General = "general",
	PackageJson = "package.json",
	ServantJson = "servant.json",
	EslintConfig = "eslint",
	PrettierConfig = "prettier",
	TrancorderConfig = "trancorder",
}

export enum ServantInitProperty {
	//module
	Type = "type",
	Styles = "styles",
	HasEntry = "hasEntry",
	Language = "language",
	//package.json
	Name = "name",
	Version = "version",
	Description = "description",
	Keywords = "keywords",
	Author = "author",
	License = "license",
	//servant.json
	ModuleDirectory = "moduleDirectory",
	Modules = "modules",
	OutputDirectory = "outputDirectory",
	OutputFilename = "outputFilename",
	Entry = "entry",
	Src = "src",
	Tests = "tests",
	Test = "test",
	Clean = "clean",
	Target = "target",
	Registry = "registry",
	//eslintrc.json
	EslintExpose = "eslintExpose",
	EslintExposeType = "eslintExposeType",
	//prettierrc.json
	PrettierExpose = "prettierExpose",
	PrettierExposeType = "prettierExposeType",
	PrettierPrettifySources = "prettierPrettifySources",
	PrettierPrettifyExtensions = "prettierPrettifyExtensions",
	//trancorder.json
	TrancorderExpose = "trancorderExpose",
	TrancorderTranslations = "trancorderTranslations",
}

export enum ServantInitType {
	Submodule = "submodule",
	Main = "main",
}
export enum ServantLanguage {
	JavaScript = "js",
	TypeScript = "ts",
}
export enum ServantStyles {
	Css = "css",
	Sass = "sass",
	Less = "less",
}

export interface InitResults {
	packageJsonProps: {
		prettier: boolean;
		eslintConfig: boolean;
	};
	packageJson: string;
	servantJson: string;
	tsconfigJson: string | null;
	eslintrcJson: string | null;
	prettierrcJson: string | null;
	gitignoreFile: string | null;
	paths: Array<string>;
	name: string;
	vcs: boolean;
}

export type PrepareInitResults = {
	entry: string;
	packageJson: PackageJson.PackageJsonInfo;
	name: string;
	dependencies: { [key: string]: string };
};

export type InitCustomData = {
	into: string;
	type: ServantInitType;
};

export interface InitParams {
	//type
	type: ServantInitType;
	language?: "js" | "ts";
	//package.json
	name?: string;
	version?: string;
	description?: string;
	keywords?: Array<string>;
	author?: string;
	license?: string;
	//servant.json
	styles?: "less" | "sass" | "css";
	moduleDirectory?: string;
	modules?: Array<string>;
	outputDirectory?: string;
	outputFilename?: string;
	entry?: string | boolean;
	src?: Array<string>;
	tests?: Array<string>;
	test?: string;
	clean?: Array<string>;
	target?: ServantJson.ModuleTarget;
	registry?: string;
	//.eslintrc.json
	eslintType?: "none" | ".eslintrc.json" | "eslintConfig";
	//.prettierrc.json
	prettierType?: "none" | ".prettierrc.json" | "prettier";
	prettifySources?: Array<string>;
	prettifyExtensions?: Array<string>;
	//.trancorder.json
	trancorderType?: "none" | ".trancorder.json";
	trancorderTranslations?: string;
}
