import { Modules, PackageJson, ServantJson } from "@servant/servant-data";
import { Path } from "@servant/servant-files";

import { vcs, VcsChanges, ChangeBy } from "../vcs";

export interface DependenciesGraph {
	modules: DependenciesModules;
	sorted: SortedDependencies;
	all: Array<string>;
	changes: VcsChanges | null;
}
export type SortedDependencies = Array<Array<string>>;
export interface DependenciesModules {
	[key: string]: Modules.ModuleDefinition;
}

export function dependencies(
	servantJson: ServantJson.ServantJsonInfo | null,
	modulesInfo: Array<Modules.ModuleInfo>,
	only: Array<string>,
	changed: Array<ChangeBy>,
	dependencies: boolean
): Promise<DependenciesGraph> {
	return new Promise(function (fulfill, reject) {
		const depGraph = dependenciesGraph(modulesInfo);

		//determine changed modules
		if (changed.length > 0 && servantJson) {
			vcs(servantJson, depGraph, changed)
				.then((changesResult) => {
					//get only change and dependants graph
					changedModules(depGraph, changesResult);
					fulfill(loadOnly(depGraph, only, dependencies));
				})
				.catch(reject);
		} else {
			//normal
			fulfill(loadOnly(depGraph, only, dependencies));
		}
	});
}

//GRAPH

function graph(): DependenciesGraph {
	return {
		modules: {},
		sorted: [],
		all: [],
		changes: null,
	};
}

function dependenciesGraph(modulesInfo: Array<Modules.ModuleInfo>): DependenciesGraph {
	const depGraph = graph();
	const modules = modulesMap(modulesInfo);

	//collect external modules
	collectModules(modules, modulesInfo, depGraph);
	//collect dependencies from config file
	collectDependencies(modulesInfo, depGraph);

	//calculate dependencies list
	const data = calculateDependenciesList(depGraph.modules);
	depGraph.all = data.all;
	depGraph.sorted = data.sorted;

	return depGraph;
}

function modulesMap(allModulesInfo: Array<Modules.ModuleInfo>): {
	[key: string]: Modules.ModuleInfo;
} {
	return allModulesInfo.reduce((map, moduleInfo) => {
		const name = moduleInfo.packageJson.content.name;
		map[name] = map[name] || moduleInfo;
		return map;
	}, {});
}

function createModuleDefinition(
	graph: DependenciesGraph,
	name: string,
	info: Modules.ModuleInfo | null,
	deps: Array<PackageJson.PackageJsonInfo> = []
): Modules.ModuleDefinition {
	//exists in modules
	if (graph.modules[name]) {
		return graph.modules[name];
	}

	//local modules has defined servant json and always exists
	if (info && info.internal) {
		return Modules.create(name, info, false);
	}
	//external modules need exists in dependencies
	const exists = deps.some((dep) => dep.content.name === name);
	return Modules.create(name, info, !exists);
}

function collectModules(
	modules: { [key: string]: Modules.ModuleInfo },
	moduleInfos: Array<Modules.ModuleInfo>,
	graph: DependenciesGraph
) {
	moduleInfos.forEach((moduleInfo) => {
		const packageJson = moduleInfo.packageJson;
		const name = packageJson.content.name;
		const version = packageJson.content.version;
		const deps = PackageJson.dependencies(packageJson);

		//dependency info
		const info = createModuleDefinition(graph, name, moduleInfo);
		//for internal modules
		if (moduleInfo.internal) {
			//dependency type
			info.depType.push(Modules.DependencyType.Internal);
			info.depType = [...new Set(info.depType)];
			//version
			info.versions.push(version);
			info.versions = [...new Set(info.versions)];
			info.versions.sort();
		}
		//force not missing, is root
		info.missing = false;
		//save
		graph.modules[name] = info;

		//collect dependencies
		deps.forEach((dependency) => {
			//dependency info
			const info = createModuleDefinition(
				graph,
				dependency.module,
				modules[dependency.module] || null,
				moduleInfo.dependencies
			);
			//for internal parent modules only
			if (moduleInfo.internal) {
				//dependency type
				info.depType.push(dependency.type);
				info.depType = [...new Set(info.depType)];
				//version
				info.versions.push(dependency.version);
				info.versions = [...new Set(info.versions)];
				info.versions.sort();
			}
			//save
			graph.modules[dependency.module] = info;
		});
	});
}

function collectDependencies(moduleInfos: Array<Modules.ModuleInfo>, graph: DependenciesGraph) {
	moduleInfos.forEach((moduleInfo) => {
		const currentDependencyInfo = graph.modules[moduleInfo.packageJson.content.name];
		const packageJson = moduleInfo.packageJson;
		const deps = PackageJson.dependencies(packageJson);

		deps.forEach((dependency) => {
			//load info
			const dependencyInfo = graph.modules[dependency.module];
			//add into external or internal
			if (dependencyInfo.depType.includes(Modules.DependencyType.Internal)) {
				if (!currentDependencyInfo.internals.includes(dependencyInfo)) {
					currentDependencyInfo.internals.push(dependencyInfo);
				}
			} else {
				if (!currentDependencyInfo.externals.includes(dependencyInfo)) {
					currentDependencyInfo.externals.push(dependencyInfo);
				}
			}
		});
	});
}

type DependenciesList = {
	sorted: SortedDependencies;
	all: Array<string>;
};

type ModuleNeeds = {
	module: Modules.ModuleDefinition;
	needs: Array<Modules.ModuleDefinition>;
};

function calculateDependenciesList(modulesMap: DependenciesModules): DependenciesList {
	const sorted: SortedDependencies = [];

	const all: Array<string> = [];
	let group: Array<string> = [];

	let modules: Array<ModuleNeeds> = [];
	Object.keys(modulesMap).forEach((moduleName) => {
		if (modulesMap[moduleName].module?.internal) {
			const module = modulesMap[moduleName];
			modules.push({ module, needs: determineNeeds(modulesMap, module) });
			all.push(moduleName);
		}
	});

	let mapAll: Record<string, boolean> = {};
	const iterates = modules.length + 2;
	for (let i = 0; i < iterates; i++) {
		const rest: Array<ModuleNeeds> = [];
		const mapCurrent: Record<string, boolean> = {};

		modules.forEach((module) => {
			//internal
			if (module.needs.length === 0 || isBuild(mapAll, module)) {
				group.push(module.module.name);
				mapCurrent[module.module.name] = true;
			} else {
				rest.push(module);
			}
		});
		//add filled group
		if (group.length > 0) {
			sorted.push(group);
		}
		//reset
		modules = rest;
		group = [];
		mapAll = { ...mapAll, ...mapCurrent };

		//leave cycle, all done
		if (modules.length === 0) {
			break;
		}
	}

	findGraphCycleInModules(mapAll, modules);

	return {
		all: all,
		sorted: sorted,
	};
}

function isBuild(mapAll: Record<string, boolean>, module: ModuleNeeds) {
	return module.needs.every((int) => {
		return mapAll[int.name];
	});
}

function determineNeeds(
	modulesMap: DependenciesModules,
	module: Modules.ModuleDefinition
): Array<Modules.ModuleDefinition> {
	//no module
	if (!module.module) {
		return module.internals;
	}

	const root = module.module.packageJson.cwd;
	const modules = Object.values(modulesMap);

	const externals = module.externals.map((ext) => ext.name);
	const dependencies = PackageJson.dependencies(module.module.packageJson);
	const locals = dependencies.filter((dep) => dep.local && externals.includes(dep.module));

	const resolvedRelatives = locals.reduce((prev, local) => {
		const found = findLocalDependencyModule(modules, root, local);
		//throw error if not found, that's invalid setup
		if (!found) {
			throw new Error(
				`There is defined reference "${local.module}" to another module, but this module can not be found. This module needs to be internal and under Servant manage.`
			);
		}
		return [...prev, found];
	}, [] as Array<Modules.ModuleDefinition>);

	return [...module.internals, ...resolvedRelatives];
}

function findLocalDependencyModule(
	modules: Array<Modules.ModuleDefinition>,
	root: string,
	local: PackageJson.Dependency
): Modules.ModuleDefinition | undefined {
	const resolved = Path.normalize(PackageJson.resolveLocalVersion(root, local.version));
	//found module from relative external dependency is loaded
	return modules.find((mod) => {
		if (mod.module && mod.module.internal) {
			const internal = Path.join(mod.module.packageJson.cwd, [
				`node_modules/${local.module}`,
			])[0];
			return internal === resolved;
		}
		return false;
	});
}

function findGraphCycleInModules(mapAll: Record<string, boolean>, restModules: ModuleNeeds[]) {
	//no cycle
	if (restModules.length === 0) {
		return;
	}

	const modules = restModules.reduce((prev, { module, needs }) => {
		prev[module.name] = needs.filter((need) => !mapAll[need.name]).map((need) => need.name);
		return prev;
	}, {} as Record<string, string[]>);

	const cycles = findCycle(modules, Object.keys(modules));
	const sorted = cycles.sort((a, b) => a.length - b.length);

	//not cycle found, strange graph error
	if (sorted.length === 0) {
		throw new Error(
			`There is some cycle detected but servant can not determine cycle that cause it. Possible error in Servant.`
		);
	}

	const cycle = sorted[0];
	const invalid = cycle[cycle.length - 1];

	throw new Error(
		`There is possible circular references in modules "${cycle.join(
			" => "
		)}". Look into module "${invalid}", where is a problematic dependency.`
	);
}

function findCycle(
	modules: Record<string, string[]>,
	entries: string[],
	stack: string[] = []
): string[][] {
	return entries.reduce((cycles, entry) => {
		if (stack.includes(entry)) {
			//cycle
			cycles.push([...stack, entry]);
			return cycles;
		}
		return [...cycles, ...findCycle(modules, modules[entry], [...stack, entry])];
	}, [] as string[][]);
}

//DEPENDANTS

function changedModules(graph: DependenciesGraph, changes: VcsChanges) {
	const all = retrieveChangeAffectedModules(graph, changes);
	graph.sorted = retrieveSortedByChanges(graph, all);
	graph.changes = changes;
}

function retrieveChangeAffectedModules(
	graph: DependenciesGraph,
	changes: VcsChanges
): Array<string> {
	const dependants: Array<string> = [];
	const changedModules = changes.modules
		.filter((module) => module.changed)
		.map((module) => module.module);

	graph.all.forEach((module) => {
		const moduleInfo = graph.modules[module];
		const internals = moduleInfo.internals;

		internals.forEach((internal) => {
			if (changedModules.indexOf(internal.name) >= 0) {
				dependants.push(module);
			}
		});
	});

	return [...new Set([...changedModules, ...dependants])];
}

function retrieveSortedByChanges(graph: DependenciesGraph, all: Array<string>): SortedDependencies {
	const sorted: SortedDependencies = [];

	graph.sorted.forEach((modules) => {
		const batch = modules.filter((module) => {
			return all.indexOf(module) >= 0;
		});
		if (batch.length > 0) {
			sorted.push(batch);
		}
	});
	return sorted;
}

//ITERATORS

export function iterateSorted(deps: SortedDependencies): Array<string> {
	const sorted: Array<string> = [];
	deps.forEach((modules) => {
		modules.forEach((module) => {
			sorted.push(module);
		});
	});
	return sorted;
}

export function scheduleSorted<T>(
	deps: SortedDependencies,
	callbackFn: (name: string) => Promise<T>
) {
	return new Promise<T[]>((resolve, reject) => {
		const results: T[] = [];
		let p: Promise<unknown> = Promise.resolve();
		//create all
		deps.forEach((modules) => {
			p = p
				.then(() =>
					Promise.all(
						modules.map((name) => {
							return callbackFn(name).then((value) => {
								results.push(value);
							});
						})
					)
				)
				.catch(reject);
		});
		p.then(() => resolve(results));
	});
}

//VALIDATIONS

export interface DependenciesValidations {
	internal: {
		[key: string]: Array<string>;
	};
	external: {
		[key: string]: Array<string>;
	};
	missing: {
		[key: string]: Array<string>;
	};
}

export function validateDependencies(graph: DependenciesGraph): DependenciesValidations {
	const validation = createDependenciesValidations();

	graph.all.forEach((name) => {
		const module = graph.modules[name];

		validation.internal[name] = [
			...new Set<string>(module.versions.concat(validation.internal[name] || [])),
		];

		module.internals.forEach((internal) => {
			validation.internal[internal.name] = [
				...new Set<string>(
					internal.versions.concat(validation.internal[internal.name] || [])
				),
			];
		});

		module.externals.forEach((external) => {
			validation.external[external.name] = [
				...new Set<string>(
					external.versions.concat(validation.external[external.name] || [])
				),
			];
		});

		const missing = module.module ? module.externals.filter(({ missing }) => missing) : [];
		if (missing.length > 0) {
			validation.missing[name] = missing.map(({ name }) => name);
		}
	});

	return validation;
}

export interface DependenciesValidationsResults {
	externalInvalidVersions: Array<string>;
	internalInvalidVersions: Array<string>;
	missing: {
		[key: string]: Array<string>;
	};
}

export function validateVersions(
	validations: DependenciesValidations
): DependenciesValidationsResults {
	const internal = Object.keys(validations.internal);
	const internalInvalid = internal.filter((name) => !validateVersion(validations.internal[name]));

	const external = Object.keys(validations.external);
	const externalInvalid = external.filter((name) => !validateVersion(validations.external[name]));

	return {
		externalInvalidVersions: externalInvalid,
		internalInvalidVersions: internalInvalid,
		missing: validations.missing,
	};
}

function validateVersion(versions: Array<string>): boolean {
	let filtered: Array<string> = versions.slice(0);

	filtered = filtered.filter((version) => {
		return !PackageJson.versionIsLocal(version);
	});

	return filtered.length <= 1;
}

function createDependenciesValidations(): DependenciesValidations {
	return {
		external: {},
		internal: {},
		missing: {},
	};
}

function loadOnly(
	depGraph: DependenciesGraph,
	only: Array<string>,
	dependencies: boolean
): DependenciesGraph {
	//update by only
	if (only.length > 0) {
		only = updateOnlyForDependencies(depGraph, only, dependencies);
		updateSortedForOnly(depGraph, only);
		filterSorted(depGraph);
	}
	return depGraph;
}

function updateOnlyForDependencies(
	depGraph: DependenciesGraph,
	only: Array<string>,
	dependencies: boolean
): Array<string> {
	if (!dependencies) {
		return only;
	}

	const dependants: Array<string> = [];
	Object.keys(depGraph.modules).forEach((module) => {
		const moduleDef = depGraph.modules[module];
		const internals = moduleDef.internals;

		internals.forEach((internal) => {
			if (only.indexOf(internal.name) >= 0) {
				dependants.push(module);
			}
		});
	});

	return [...new Set([...only, ...dependants])];
}

function updateSortedForOnly(depGraph: DependenciesGraph, only: Array<string>) {
	depGraph.sorted = depGraph.sorted.map((modules) => {
		return modules.filter((name) => {
			return only.indexOf(name) >= 0;
		});
	});
}

function filterSorted(depGraph: DependenciesGraph) {
	depGraph.sorted = depGraph.sorted.filter((modules) => {
		return modules.length > 0;
	});
}
