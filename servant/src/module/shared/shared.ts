import { Modules, PackageJson, ServantJson } from "@servant/servant-data";
import { Path } from "@servant/servant-files";
import * as devlink from "devlink-core";
import { promise } from "devlink-core";
import * as path from "path";
import * as fs from "fs";

import { CommandResult, createResult, DoneType } from "../index";
import { resolveNodeModulePath } from "../../resolver";
import { invariant } from "../../invariant";

export type SharedResult = {
	module: string;
	shared: Record<string, string>;
	missing: string[];
	errors: Error[];
	exists: Record<string, boolean>;
	links?: devlink.LinkResult[];
	unlinks?: devlink.UnlinkResult[];
	mode?: SharedProps["mode"];
};

export type SharedProps = {
	mode?: "link" | "unlink";
};

export function shared(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	{ mode }: SharedProps
): Promise<CommandResult<SharedResult>> {
	return new Promise((resolve) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const moduleServantJson = module.module.servantJson;
		const { relative, absolute } = ServantJson.getSharedPackages(moduleServantJson);
		const local = collectLocalPackages(module);

		const data = createSharedResult(module.name, { ...local, ...relative }, mode);

		const tasks: Promise<unknown>[] = [];
		//mode: link
		if (mode === "link") {
			tasks.push(
				makeLinksAndCollectErrors(module, absolute).then((result) => {
					data.missing = [...data.missing, ...result.missing];
					data.errors = [...data.errors, ...result.errors];
					data.links = result.results;
				})
			);

			//mode: unlink
		} else if (mode === "unlink") {
			tasks.push(
				makeUnlinksAndCollectErrors(module, absolute).then((result) => {
					data.missing = [...data.missing, ...result.missing];
					data.errors = [...data.errors, ...result.errors];
					data.unlinks = result.results;
				})
			);
		}

		//mode: exists
		tasks.push(
			makeExists(module, local, absolute).then((result) => {
				data.missing = [...new Set([...data.missing, ...result.missing])];
				data.exists = result.exists;
			})
		);

		Promise.all(tasks).then(() => {
			const error = determineLinkError(data);
			const type = error ? DoneType.FAIL : DoneType.OK;
			resolve(createResult("shared", module.name, type, error, [], data));
		});
	});
}

//utils

type LinkResult = {
	errors: Error[];
	missing: string[];
	results: devlink.LinkResult[];
};

function makeLinksAndCollectErrors(
	module: Modules.ModuleDefinition,
	shared: Record<string, string>
): Promise<LinkResult> {
	const errors: Error[] = [];
	const missing: string[] = [];
	const results: devlink.LinkResult[] = [];

	const links = collectSharedModules(module, shared).map(({ name, path, err }) => {
		if (err) {
			return Promise.resolve().then(() => missing.push(name));
		}
		return cleanDeathLinks(path).then(() =>
			promise
				.link(path, shared[name])
				.then((result) => results.push(result))
				.catch((err: devlink.LinkThrow) => errors.push(convertToError(err)))
		);
	});
	return Promise.all(links).then(() => ({ results, errors, missing }));
}

type UnlinkResult = {
	errors: Error[];
	missing: string[];
	results: devlink.UnlinkResult[];
};

function makeUnlinksAndCollectErrors(
	module: Modules.ModuleDefinition,
	shared: Record<string, string>
): Promise<UnlinkResult> {
	const errors: Error[] = [];
	const missing: string[] = [];
	const results: devlink.UnlinkResult[] = [];

	const unlinks = collectSharedModules(module, shared).map(({ name, path, err }) => {
		if (err) {
			return Promise.resolve().then(() => missing.push(name));
		}
		return promise
			.unlink(path)
			.then((result) => results.push(result))
			.catch((err: devlink.LinkThrow) => errors.push(convertToError(err)));
	});
	return Promise.all(unlinks).then(() => ({ results, errors, missing }));
}

type ExistsResult = {
	missing: string[];
	exists: Record<string, boolean>;
};

function makeExists(
	module: Modules.ModuleDefinition,
	local: Record<string, string>,
	shared: Record<string, string>
): Promise<ExistsResult> {
	const missing: string[] = [];
	const exists: Record<string, boolean> = {};

	const sharedCheck = collectSharedModules(module, shared).map(({ name, path, err }) => {
		if (err) {
			return Promise.resolve().then(() => missing.push(name));
		}
		return promise.linked(path).then((result) => {
			exists[name] = Boolean(result && result.exists);
		});
	});

	const localCheck = Object.keys(local).map((name) => {
		const version = module.module
			? PackageJson.resolveLocalVersion(module.module.packageJson.cwd, local[name])
			: null;

		if (!version) {
			return Promise.resolve();
		}

		return new Promise<void>((resolve) => {
			fs.stat(version, (err) => {
				if (!err) {
					exists[name] = true;
				}
				resolve();
			});
		});
	});

	return Promise.all([...sharedCheck, ...localCheck]).then(() => ({ exists, missing }));
}

function determineLinkError(data: SharedResult): Error | null {
	//missing packages
	if (data.missing.length > 0) {
		return new Error(
			`Missing packages: "${data.missing.join(
				", "
			)}". Do you not forgot to run "servant install"?`
		);
	}
	if (data.errors.length > 0) {
		return data.errors[0];
	}
	return null;
}

function collectLocalPackages(module: Modules.ModuleDefinition): Record<string, string> {
	invariant(
		module.module,
		"No module declaration found. Invalid data provided or is probably error in Servant."
	);

	const modulePackageJson = module.module.packageJson;
	const dependencies = PackageJson.dependencies(modulePackageJson);
	const internals = module.internals.map(({ name }) => name);

	return dependencies.reduce((acc, { module, local, version }) => {
		if (local && !internals.includes(module)) {
			acc[module] = Path.normalize(
				path.relative(
					modulePackageJson.cwd,
					PackageJson.resolveLocalVersion(modulePackageJson.cwd, version)
				)
			);
		}
		return acc;
	}, {} as Record<string, string>);
}

function createSharedResult(
	module: string,
	shared: Record<string, string>,
	mode?: "link" | "unlink"
): SharedResult {
	return {
		module,
		mode,
		shared,
		errors: [],
		missing: [],
		exists: {},
	};
}

//// utils shared modules

type CollectedSharedModules = { name: string } & ReturnType<typeof resolveNodeModulePath>;

function collectSharedModules(
	module: Modules.ModuleDefinition,
	shared: Record<string, string>
): CollectedSharedModules[] {
	invariant(
		module.module,
		"No module declaration found. Invalid data provided or is probably error in Servant."
	);

	const modulePackageJson = module.module.packageJson;

	return module.externals
		.map(({ name }) => {
			if (shared[name]) {
				const data = resolveNodeModulePath(modulePackageJson.cwd, name, true);
				return { name, ...data };
			}
			return null;
		})
		.filter(Boolean) as CollectedSharedModules[];
}

//clean

function cleanDeathLinks(path: string): Promise<void> {
	return promise.linked(path).then((result) => {
		//try delete death links
		if (result && !result.exists) {
			return new Promise<void>((resolve) => {
				fs.rm(result.backup, { recursive: true, force: true }, () => resolve());
			});
		}
		return Promise.resolve();
	});
}

//utils

function convertToError(err: devlink.LinkThrow) {
	if (err.original) {
		return err.original;
	}

	const message = [err.name, err.message || err.description].filter(Boolean).join(" ");
	return new Error(message);
}
