import { PackageJson, ServantJson, Modules } from "@servant/servant-data";
import { LESS, CSS, SASS, JS, TS } from "@servant/servant-files";

import { CommandResult, createResult, DoneType } from "../index";
import { loadPatterns, loadResources } from "../../patterns";
import { css, filterCss, mapCss, cssEntry, CssResult, CssState } from "../../build/css";
import { less, filterLess, lessEntry, LessResult, LessState } from "../../build/less";
import { filterSass, sass, sassEntry, SassResult, SassState } from "../../build/sass";
import {
	typescript,
	transpile,
	filterTypescript,
	TypescriptResult,
	TypescriptState,
} from "../../build/typescript";
import { resources, ResourcesResults, ResourcesState } from "../../build/resources";
import { invariant } from "../../invariant";

export interface BuildResult {
	module: string;
	less: LessResult | null;
	sass: SassResult | null;
	css: CssResult | null;
	typescript: TypescriptResult | null;
	resources: ResourcesResults | null;
	type: DoneType;
}

export function build(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	production: boolean,
	transpile: boolean
): Promise<CommandResult<BuildResult>> {
	return new Promise((resolve, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const moduleServantJson = module.module.servantJson;
		const entry = moduleServantJson.cwd;

		const sourcesGlobs = ([] as Array<string>).concat(moduleServantJson.content.src);

		const resourcesGlobs = ([] as Array<string>).concat(moduleServantJson.content.resources);

		Promise.all([loadPatterns(entry, sourcesGlobs), loadResources(entry, resourcesGlobs)])
			.then(([sourcesFiles, resourcesFiles]) => {
				buildFiles(
					packageJson,
					module,
					{
						sources: sourcesFiles,
						resources: resourcesFiles,
					},
					production,
					transpile
				).then((buildResult) => {
					if (buildResult.type === DoneType.OK) {
						resolve(
							createResult(
								"build",
								module.name,
								buildResult.type,
								null,
								[],
								buildResult
							)
						);
					} else {
						resolve(
							createResult(
								"build",
								module.name,
								buildResult.type,
								new Error(
									`Build command failed in building of module "${module.name}"`
								),
								[],
								buildResult
							)
						);
					}
				});
			})
			.catch(reject);
	});
}

type BuildFiles = {
	sources: Array<string>;
	resources: Array<string>;
};
type BuildTypes = "less" | "sass" | "css" | "declarations" | "typescript" | "resources";

export function buildFiles(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	files: BuildFiles,
	production: boolean,
	transpile: boolean
): Promise<BuildResult> {
	return new Promise((resolve, reject) => {
		const what = determineWhat(files);
		const lessFiles = filterLess(files.sources);
		const sassFiles = filterSass(files.sources);
		const cssFiles = [
			...new Set([...filterCss(files.sources), ...mapCss(lessFiles), ...mapCss(sassFiles)]),
		];
		const typescriptFiles = filterTypescript(files.sources);
		const buildResult = createBuildResult(module.name);

		const entryLess = lessEntry(packageJson, module);
		const entrySass = sassEntry(packageJson, module);
		const entryCss = cssEntry(packageJson, module);

		let p: Promise<void> = Promise.resolve();
		//LESS
		if (what.indexOf("less") >= 0) {
			//If we use "entry" less file, mark it as also for compile
			addEntry(entryLess, lessFiles);
			p = buildLess(p, packageJson, module, lessFiles, buildResult).catch(reject);
		}
		//SASS
		if (what.indexOf("sass") >= 0) {
			//If we use "entry" sass file, mark it as also for compile
			addEntry(entrySass, sassFiles);
			p = buildSass(p, packageJson, module, sassFiles, buildResult).catch(reject);
		}
		//CSS
		if (what.indexOf("css") >= 0) {
			//If we use "entry" css file, mark it as also for compile
			addEntry(entryCss, cssFiles);
			p = buildCss(p, packageJson, module, cssFiles, buildResult).catch(reject);
		}
		//DECLARATIONS
		if (what.indexOf("declarations") >= 0) {
			p = buildDeclarations(p, packageJson, module, typescriptFiles, buildResult).catch(
				reject
			);
		}
		//TYPESCRIPT
		if (what.indexOf("typescript") >= 0 || what.indexOf("css") >= 0) {
			p = buildTypescript(
				p,
				packageJson,
				module,
				typescriptFiles,
				cssFiles,
				production,
				transpile,
				buildResult
			).catch(reject);
		}
		//RESOURCES
		if (what.indexOf("resources") >= 0) {
			p = buildResources(p, packageJson, module, files.resources, buildResult).catch(reject);
		}

		//ALL DONE
		p.then(() => {
			buildResult.type = buildType(buildResult);
			resolve(buildResult);
		});
	});
}

function buildLess(
	p: Promise<void>,
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	lessFiles: Array<string>,
	result: BuildResult
): Promise<void> {
	return p.then(() =>
		less(packageJson, module, lessFiles).then((lessResult) => {
			result.less = lessResult;
		})
	);
}

function buildSass(
	p: Promise<void>,
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	sassFiles: Array<string>,
	result: BuildResult
): Promise<void> {
	return p.then(() =>
		sass(packageJson, module, sassFiles).then((sassResult) => {
			result.sass = sassResult;
		})
	);
}

function buildCss(
	p: Promise<void>,
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	cssFiles: Array<string>,
	result: BuildResult
): Promise<void> {
	return p
		.then(() => css(packageJson, module, cssFiles))
		.then((cssResult) => {
			result.css = cssResult;
		});
}

function buildTypescript(
	p: Promise<void>,
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	typescriptFiles: Array<string>,
	cssFiles: Array<string>,
	production: boolean,
	transpile: boolean,
	result: BuildResult
): Promise<void> {
	return p
		.then(() =>
			typescript(packageJson, module, typescriptFiles, cssFiles, production, transpile)
		)
		.then((typescriptResult) => {
			result.typescript = typescriptResult;
		});
}

function buildDeclarations(
	p: Promise<void>,
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	typescriptFiles: Array<string>,
	result: BuildResult
): Promise<void> {
	return p
		.then(() => transpile(packageJson, module, typescriptFiles))
		.then((typescriptResult) => {
			result.typescript = typescriptResult;
		});
}

function buildResources(
	p: Promise<void>,
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	resourcesFiles: Array<string>,
	result: BuildResult
): Promise<void> {
	return p
		.then(() => resources(packageJson, module, resourcesFiles))
		.then((resourcesResult) => {
			result.resources = resourcesResult;
		});
}

function buildType(buildResult: BuildResult): DoneType {
	//ERRORS
	if (buildResult.less && buildResult.less.status === LessState.Error) {
		return DoneType.FAIL;
	}
	if (buildResult.sass && buildResult.sass.status === SassState.Error) {
		return DoneType.FAIL;
	}
	if (buildResult.css && buildResult.css.status === CssState.Error) {
		return DoneType.FAIL;
	}
	if (buildResult.typescript && buildResult.typescript.status === TypescriptState.Error) {
		return DoneType.FAIL;
	}

	//WARNINGS
	if (buildResult.resources && buildResult.resources.status === ResourcesState.Warning) {
		return DoneType.WARNING;
	}

	//OK
	return DoneType.OK;
}

function createBuildResult(module: string): BuildResult {
	return {
		module: module,
		less: null,
		css: null,
		sass: null,
		typescript: null,
		resources: null,
		type: DoneType.FAIL,
	};
}

function addEntry(entry: ServantJson.EntryFile | null, files: Array<string>) {
	if (entry && files.indexOf(entry.from) === -1) {
		files.push(entry.from);
	}
}

function determineWhat(files: BuildFiles): Array<BuildTypes> {
	const what: Partial<Record<BuildTypes, true>> = {};

	const hasLess = files.sources.some(LESS.isLESS);
	if (hasLess) {
		what["less"] = true;
		what["css"] = true;
	}

	const hasSass = files.sources.some(SASS.isSASS);
	if (hasSass) {
		what["sass"] = true;
		what["css"] = true;
	}

	const hasCss = files.sources.some(CSS.isCSS);
	if (hasCss) {
		what["css"] = true;
	}

	const hasScript = files.sources.some((f) => TS.isTypescript(f) || JS.isJavascript(f));
	if (hasScript) {
		what["typescript"] = true;
		what["declarations"] = true;
	}

	if (files.resources.length > 0) {
		what["resources"] = true;
	}

	return Object.keys(what) as Array<BuildTypes>;
}
