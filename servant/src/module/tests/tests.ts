import { Modules, PackageJson, ServantJson, TestsJson } from "@servant/servant-data";
import * as NodeTests from "@servant/servant-jasmine-node";
import * as BrowserTests from "@servant/servant-jasmine-browser";
import { Path, Extensions } from "@servant/servant-files";
import { fork } from "child_process";
import * as path from "path";

import { CommandResult, createResult, DoneType } from "../index";

import * as BuildTS from "../../build/typescript";
import * as BuildJS from "../../build/javascript";
import { loadLibrary } from "../../main";
import { extractIssues, Issues } from "../reports";
import { resolveModulePath } from "../../resolver";
import { loadPatterns } from "../../patterns";
import { invariant } from "../../invariant";

const jasmineNode = resolveModulePath("@servant/servant-jasmine-node").path;
const jasmineBrowser = resolveModulePath("@servant/servant-jasmine-browser").path;

export interface TestingResults {
	summary: {
		//time
		time: [number, number];
		//numbers
		count: number;
		completed: number;
		ok: number;
		failed: number;
		excluded: number;
		pending: number;
		//boolean
		passed: boolean;
		done: boolean;
	};
	error: Error | null;
	failed: Array<Item>;
	excluded: Array<Item>;
	pending: Array<Item>;
	issues: Issues;
	//playwright
	playwright: boolean;
	browsers: Array<string>;
	devices: {
		used: Array<string>;
		invalid: Array<string>;
	};
}
export enum ItemType {
	Suit,
	Spec,
}
export type ItemStatus = "failed" | "passed" | "excluded" | "pending";
export interface Item {
	name: string;
	fullName: string;
	type: ItemType;
	status: ItemStatus;
	passed: Array<Expectation>;
	failed: Array<Expectation>;
	pending: string | null;
	duration: null | number;
	done: boolean;
}
export interface Expectation {
	actual: string;
	expected: string;
	matcherName: string;
	message: string;
	passed: boolean;
	stack: string;
}

export function tests(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	progress: (result: CommandResult<TestingResults>) => void,
	browsers?: Array<string>,
	devices?: Array<string>,
	gui?: boolean
): Promise<CommandResult<TestingResults>> {
	return new Promise((resolve, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const moduleServantJson = module.module.servantJson;
		const files = BuildTS.collect(module);
		const sourcesGlobs = [...files.src, ...files.tests];

		loadPatterns(moduleServantJson.cwd, sourcesGlobs).then((typescripts) => {
			//translate for tests
			BuildTS.translate(packageJson, module, typescripts)
				.then(() => {
					switch (moduleServantJson.content.target) {
						case ServantJson.ModuleTarget.node:
						case ServantJson.ModuleTarget.node_cli:
							nodeTests(packageJson, module, progress).then(resolve).catch(reject);
							break;
						case ServantJson.ModuleTarget.web:
						case ServantJson.ModuleTarget.web_page:
							browserTests(packageJson, module, progress, browsers, devices, gui)
								.then(resolve)
								.catch(reject);
							break;
						default:
							reject(
								new Error(
									`Unknown target ${moduleServantJson.content.target} defined in servant.json.`
								)
							);
							break;
					}
				})
				.catch(reject);
		});
	});
}

//NODE TESTS

function nodeTests(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	progress: (result: CommandResult<TestingResults>) => void
): Promise<CommandResult<TestingResults>> {
	return new Promise((resolve) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const jasmineProcess = fork(jasmineNode, []);

		const cwd = packageJson.cwd;
		const moduleServantJson = module.module.servantJson;

		const entry = moduleServantJson.cwd;
		const files = BuildJS.collect(module);
		let testsProgress: NodeTests.TestsProgress | null = null;

		function done() {
			const res = nodeResult(
				module,
				testsProgress,
				!testsProgress
					? new Error(
							`There is no test results for module ${module.name}. Maybe there is problem with tests.`
					  )
					: null
			);
			//send
			resolve(res);
		}

		const message = TestsJson.create(cwd, entry, files.tests);
		message.module = module.module;
		message.extensions = resolveExtensions(module, moduleServantJson.content.resolve);
		message.externals = [];

		//send message
		jasmineProcess.send(message);
		jasmineProcess.on("message", (tp: NodeTests.TestsProgress) => {
			testsProgress = tp;

			const results = nodeResult(module, testsProgress, null);
			progress(results);
			//check if done
			if (results.data && results.data.summary.done) {
				jasmineProcess.kill();
			}
		});
		jasmineProcess.on("error", (err) => {
			const res = nodeResult(module, testsProgress, err);
			progress(res);
			resolve(res);
		});
		jasmineProcess.on("exit", done);
	});
}

function nodeResult(
	module: Modules.ModuleDefinition,
	progress: NodeTests.TestsProgress | null,
	err: Error | null
): CommandResult<TestingResults> {
	const result: TestingResults = {
		summary: {
			//time
			time: [0, 0],
			//counts
			count: 0,
			ok: 0,
			failed: 0,
			excluded: 0,
			pending: 0,
			completed: 0,
			//boolean
			passed: true,
			done: true,
		},
		error: null,
		excluded: [],
		failed: [],
		pending: [],
		issues: {},
		playwright: false,
		browsers: [],
		devices: {
			invalid: [],
			used: [],
		},
	};

	if (progress) {
		//time
		result.summary.time = progress.summary.time;
		//counts
		result.summary.count = progress.summary.testsCount;
		result.summary.ok =
			progress.summary.testsCount -
			progress.summary.failedCount -
			progress.summary.pendingCount -
			progress.summary.excludedCount;
		result.summary.failed = progress.summary.failedCount;
		result.summary.excluded = progress.summary.excludedCount;
		result.summary.pending = progress.summary.pendingCount;
		result.summary.completed = progress.summary.completedCount;
		//boolean
		result.summary.passed = progress.summary.passed;
		result.summary.done = progress.summary.done;
		//data
		result.error = progress.error ? new Error(progress.error) : null;
		result.excluded = nodeToResults(progress.excluded);
		result.failed = nodeToResults(progress.failed);
		result.pending = nodeToResults(progress.pending);
		result.issues = nodeIssues(module.module?.servantJson, {}, progress.results);
	}
	//err
	result.error = result.error || err;

	//error
	if (result.error) {
		return createResult("tests", module.name, DoneType.FAIL, null, [], result);
	}

	switch (progress && progress.type) {
		case "ok":
			return createResult("tests", module.name, DoneType.OK, null, [], result);
		case "warn":
			return createResult("tests", module.name, DoneType.WARNING, null, [], result);
		default:
			return createResult("tests", module.name, DoneType.FAIL, null, [], result);
	}
}

function nodeToResults(array: Array<NodeTests.TestsSuit | NodeTests.TestsSpec>): Array<Item> {
	return array
		.map((item) => {
			if (item.type === NodeTests.TestsItemType.Spec) {
				const itm = item as NodeTests.TestsSpec;

				return {
					done: itm.done,
					duration: null,
					failed: itm.failed,
					fullName: itm.fullName,
					name: itm.name,
					passed: itm.passed,
					pending: itm.pending,
					status: itm.status,
					type: ItemType.Spec,
				};
			}
			if (item.type === NodeTests.TestsItemType.Suit) {
				const itm = item as NodeTests.TestsSuit;

				return {
					done: itm.done,
					duration: itm.duration,
					failed: itm.failed,
					fullName: itm.fullName,
					name: itm.name,
					passed: [],
					pending: null,
					status: itm.status,
					type: ItemType.Suit,
				};
			}
			return null;
		})
		.filter((item) => Boolean(item)) as Array<Item>;
}

//BROWSERS TESTS

function browserTests(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	progress: (result: CommandResult<TestingResults>) => void,
	browsers?: Array<string>,
	devices?: Array<string>,
	gui?: boolean
): Promise<CommandResult<TestingResults>> {
	return new Promise((resolve) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const cwd = packageJson.cwd;
		const moduleServantJson = module.module.servantJson;

		const entry = moduleServantJson.cwd;

		const message = TestsJson.create(cwd, entry, [
			...moduleServantJson.content.src,
			...moduleServantJson.content.tests,
		]);
		message.module = module.module;
		message.extensions = resolveExtensions(module, moduleServantJson.content.resolve);
		message.browsers = browsers || [];
		message.devices = devices || [];
		message.gui = gui || false;

		const externals = resolveExternals(entry, module);
		message.externals = externals.files;

		//error
		if (externals.err.length !== 0) {
			const res = browserResult(
				module,
				null,
				new Error(
					`Can not load libraries mappings for modules '${externals.err.join(", ")}'.`
				)
			);
			progress(res);
			resolve(res);
			return;
		}

		const jasmineProcess = fork(jasmineBrowser, []);
		let testsProgresses: BrowserTests.TestsProgresses | null = null;

		function done() {
			const res = browserResult(
				module,
				testsProgresses,
				!testsProgresses
					? new Error(
							`There is no test results for module ${module.name}. Maybe there is problem with tests.`
					  )
					: null
			);
			//send
			resolve(res);
		}

		//send message
		jasmineProcess.send(message);
		jasmineProcess.on("message", (tp: BrowserTests.TestsProgresses) => {
			testsProgresses = tp;

			const results = browserResult(module, testsProgresses, null);
			progress(results);
			//check if done
			if (results.data && results.data.summary.done) {
				jasmineProcess.kill();
			}
		});
		jasmineProcess.on("error", (err) => {
			const res = browserResult(module, null, err);
			progress(res);
			resolve(res);
		});
		jasmineProcess.on("exit", done);
	});
}

function browserResult(
	module: Modules.ModuleDefinition,
	progresses: BrowserTests.TestsProgresses | null,
	err: Error | null
): CommandResult<TestingResults> {
	const result: TestingResults = {
		summary: {
			//time
			time: [0, 0],
			//counts
			count: 0,
			ok: 0,
			failed: 0,
			excluded: 0,
			pending: 0,
			completed: 0,
			//boolean
			passed: true,
			done: true,
		},
		error: null,
		excluded: [],
		failed: [],
		pending: [],
		issues: {},
		playwright: false,
		browsers: [],
		devices: {
			invalid: [],
			used: [],
		},
	};
	const types: Array<"ok" | "warn" | "fail"> = [];

	if (progresses) {
		Object.keys(progresses.progresses).forEach((name) => {
			const progress: BrowserTests.TestsProgress = progresses.progresses[name];
			//time
			result.summary.time = determineTime(result.summary.time, progress.summary.time);
			//counts
			result.summary.count += progress.summary.testsCount;
			result.summary.ok +=
				progress.summary.testsCount -
				progress.summary.failedCount -
				progress.summary.pendingCount -
				progress.summary.excludedCount;
			result.summary.failed += progress.summary.failedCount;
			result.summary.excluded += progress.summary.excludedCount;
			result.summary.pending += progress.summary.pendingCount;
			result.summary.completed += progress.summary.completedCount;
			//boolean
			result.summary.passed = result.summary.passed && progress.summary.passed;
			result.summary.done = result.summary.done && progress.summary.done;

			result.excluded = [
				...result.excluded,
				...browserToResults(progress, progress.excluded),
			];
			result.failed = [...result.failed, ...browserToResults(progress, progress.failed)];
			result.pending = [...result.failed, ...browserToResults(progress, progress.pending)];
			result.issues = browsersIssues(
				module.module?.servantJson,
				result.issues,
				progress.results
			);

			//add types
			types.push(progress.type);
		});

		result.error = progresses.error ? new Error(progresses.error) : null;
		result.playwright = progresses.playwright;
		result.browsers = progresses.browsers;
		result.devices.used = progresses.devices.used;
		result.devices.invalid = progresses.devices.invalid;
	}
	//error
	result.error = result.error || err;

	//error
	if (result.error) {
		result.summary.done = true;
		return createResult("tests", module.name, DoneType.FAIL, null, [], result);
	}

	switch (determineType(types)) {
		case "ok":
			return createResult("tests", module.name, DoneType.OK, null, [], result);
		case "warn":
			return createResult("tests", module.name, DoneType.WARNING, null, [], result);
		default:
			return createResult("tests", module.name, DoneType.FAIL, null, [], result);
	}
}

function browserToResults(
	progress: BrowserTests.TestsProgress,
	array: Array<BrowserTests.TestsSuit | BrowserTests.TestsSpec>
): Array<Item> {
	const browserName = progress.browser.name;

	return array
		.map((item) => {
			if (item.type === BrowserTests.TestsItemType.Spec) {
				const itm = item as BrowserTests.TestsSpec;

				return {
					done: itm.done,
					duration: null,
					failed: itm.failed,
					fullName: `${browserName}: ${itm.fullName}`,
					name: itm.name,
					passed: itm.passed,
					pending: itm.pending,
					status: itm.status,
					type: ItemType.Spec,
				};
			}
			if (item.type === BrowserTests.TestsItemType.Suit) {
				const itm = item as BrowserTests.TestsSuit;

				return {
					done: itm.done,
					duration: itm.duration,
					failed: itm.failed,
					fullName: `${browserName}: ${itm.fullName}`,
					name: itm.name,
					passed: [],
					pending: null,
					status: itm.status,
					type: ItemType.Suit,
				};
			}
			return null;
		})
		.filter((item) => Boolean(item)) as Array<Item>;
}

function determineType(types: Array<"ok" | "warn" | "fail">): "ok" | "warn" | "fail" {
	if (types.indexOf("fail") >= 0) {
		return "fail";
	}
	if (types.indexOf("warn") >= 0) {
		return "warn";
	}
	return "ok";
}

function determineTime(time1: [number, number], time2: [number, number]): [number, number] {
	const [ms1, ns1] = time1;
	const [ms2, ns2] = time2;

	//ms are same
	if (ms1 === ms2) {
		return ns1 > ns2 ? time1 : time2;
	}
	return ms1 > ms2 ? time1 : time2;
}

//EXTERNALS

type ResolveExternalResults = {
	files: Array<string>;
	err: Array<string>;
};

function resolveExternals(
	entry: string,
	module: Modules.ModuleDefinition,
	used: Array<string> = []
): ResolveExternalResults {
	const errors: Array<string> = [];
	const internals: Array<string> = [];
	const externals: Array<string> = [];

	//used and check
	if (used.includes(module.name)) {
		return {
			err: errors,
			files: [],
		};
	}
	used.push(module.name);

	//resolve externals
	if (module.module) {
		const mod = module.module;
		const servantJson = mod.servantJson;
		const packageJson = mod.packageJson;

		module.externals.forEach((external) => {
			//skip external libraries that are bundled
			if (packageJson.content.bundledDependencies.indexOf(external.name) >= 0) {
				return;
			}

			const inner = resolveExternals(entry, external, used);
			const lib = loadLibrary(servantJson, external.name);

			//skip empty libraries that are no content (types, resources, ...)
			if (lib && lib.empty) {
				return;
			}

			//NOTE: Library found nad
			if (lib && !lib.err) {
				externals.push(Path.normalize(lib.path));
				//error
			} else {
				externals.push(...inner.files);
				errors.push(...inner.err);
				errors.push(external.name);
			}
		});
	}

	module.internals.forEach((internal) => {
		const inner = resolveExternals(entry, internal, used);
		internals.push(...inner.files);
		errors.push(...inner.err);

		const servantJson = internal.module?.servantJson;
		if (servantJson) {
			const output = servantJson.content.output;

			const file = path.join(servantJson.cwd, output.directory);
			output.resolve.forEach((ext) => {
				internals.push(...Path.join(file, [Extensions.patterns.all(ext)]));
			});
		}
	});

	return {
		err: errors,
		files: [...new Set([...internals, ...externals])],
	};
}

//EXTENSIONS

function resolveExtensions(
	module: Modules.ModuleDefinition,
	resolve: Array<string>
): Array<string> {
	if (!module.module) {
		return [];
	}

	const servantJson = module.module.servantJson;
	const clean = servantJson.content.clean.map((ext) => Extensions.create(ext.toLowerCase()));

	return resolve.filter((ext) => {
		return clean.indexOf(Extensions.create(ext.toLowerCase())) === -1;
	});
}

//ISSUES

function nodeIssues(
	servantJson: ServantJson.ServantJsonInfo | undefined,
	issues: Issues,
	results: { [key: string]: NodeTests.TestsSpec | NodeTests.TestsSuit }
): Issues {
	Object.keys(results).forEach((name) => {
		const item = results[name];

		if (item.type === NodeTests.TestsItemType.Spec) {
			const spec = item as NodeTests.TestsSpec;
			extractIssues(
				servantJson,
				spec.name,
				spec.status === NodeTests.TestsSpecStatus.Passed,
				spec.status === NodeTests.TestsSpecStatus.Excluded,
				spec.status === NodeTests.TestsSpecStatus.Pending,
				issues
			);
		}
		if (item.type === NodeTests.TestsItemType.Suit) {
			const suit = item as NodeTests.TestsSuit;
			nodeIssues(servantJson, issues, suit.suits);
		}
	});

	return issues;
}

function browsersIssues(
	servantJson: ServantJson.ServantJsonInfo | undefined,
	issues: Issues,
	results: { [key: string]: BrowserTests.TestsSpec | BrowserTests.TestsSuit }
): Issues {
	Object.keys(results).forEach((name) => {
		const item = results[name];

		if (item.type === BrowserTests.TestsItemType.Spec) {
			const spec = item as NodeTests.TestsSpec;
			extractIssues(
				servantJson,
				spec.name,
				spec.status === NodeTests.TestsSpecStatus.Passed,
				spec.status === NodeTests.TestsSpecStatus.Excluded,
				spec.status === NodeTests.TestsSpecStatus.Pending,
				issues
			);
		}
		if (item.type === BrowserTests.TestsItemType.Suit) {
			const suit = item as NodeTests.TestsSuit;
			browsersIssues(servantJson, issues, suit.suits);
		}
	});

	return issues;
}
