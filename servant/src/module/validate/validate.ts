import { Modules, PackageJson, ServantJson } from "@servant/servant-data";

import { loadPatterns, loadPrettify, loadValidated } from "../../patterns";

import {
	ValidationResult,
	ValidationDoneType,
	ValidationUniversalResults,
	ValidationUniversalMessage,
	ValidationUniversalResult,
	ValidationUniversalSeverity,
	ValidationEngine,
} from "./types";

import { createEslintValidationResult, EslintValidationResult, validateEslint } from "./eslint";
import {
	createTrancorderValidationResult,
	TrancorderValidationResult,
	validateTrancorder,
} from "./trancorder";
import {
	createPrettierValidationResult,
	PrettiesValidationResult,
	validatePrettier,
} from "./prettier";

import { CommandResult, createResult, DoneType } from "../index";
import { invariant } from "../../invariant";

export {
	ValidationUniversalResults,
	ValidationUniversalMessage,
	ValidationUniversalResult,
	ValidationDoneType,
	ValidationUniversalSeverity,
	ValidationEngine,
	EslintValidationResult,
	ValidationResult,
};

export interface ValidateResult {
	module: string;
	type: DoneType;
	eslint: EslintValidationResult;
	prettier: PrettiesValidationResult;
	trancorder: TrancorderValidationResult;
}

export function validate(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	fix: boolean
): Promise<CommandResult<ValidateResult>> {
	return new Promise((resolve, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const validateResult = createValidateResult(module.name);

		const moduleServantJson = module.module.servantJson;
		const entry = moduleServantJson.cwd;

		loadAllPatterns(entry, moduleServantJson)
			.then(({ validatedFiles, additionalFiles }) => {
				Promise.resolve()
					.then(() =>
						Promise.all([
							validateEslint(entry, module, validatedFiles, { fix }),
							validatePrettier(
								entry,
								module,
								[...validatedFiles, ...additionalFiles],
								{ fix }
							),
							validateTrancorder(entry, module, validatedFiles, { fix }),
						])
					)
					.then(([eslintResults, prettierResults, trancorderResults]) => {
						validateResult.eslint = eslintResults;
						validateResult.prettier = prettierResults;
						validateResult.trancorder = trancorderResults;
					})
					.then(() => {
						validateResult.type = getDoneType(validateResult);
						resolve(
							createResult(
								"validate",
								module.name,
								validateResult.type,
								null,
								[],
								validateResult
							)
						);
					})
					.catch(reject);
			})
			.catch(reject);
	});
}

function loadAllPatterns(
	entry: string,
	moduleServantJson: ServantJson.ServantJsonInfo
): Promise<{ validatedFiles: Array<string>; additionalFiles: Array<string> }> {
	const prettify = ServantJson.getPrettify(moduleServantJson);

	const sourcesGlobs = [...moduleServantJson.content.src, ...moduleServantJson.content.tests];
	const sourcesExtensions = [...moduleServantJson.content.resolve];

	const prettifyGlobs = [...prettify.sources];
	const prettifyExtensions = [...prettify.extensions];

	return new Promise((resolve, reject) => {
		Promise.all([loadPatterns(entry, sourcesGlobs), loadPatterns(entry, prettifyGlobs)])
			.then(([sourcesFiles, prettifyFiles]) => {
				const validatedFiles = loadValidated(sourcesFiles, sourcesExtensions);
				const additionalFiles = loadPrettify(prettifyFiles, prettifyExtensions);

				resolve({ validatedFiles, additionalFiles });
			})
			.catch(reject);
	});
}

//Others

function getDoneType(results: ValidateResult) {
	const status: Array<ValidationDoneType> = [];

	if (results.eslint.available) {
		status.push(results.eslint.type);
	}
	if (results.prettier.available) {
		status.push(results.prettier.type);
	}
	if (results.trancorder.available) {
		status.push(results.trancorder.type);
	}

	//determine done type
	if (status.includes(ValidationDoneType.FAIL) || status.includes(ValidationDoneType.ERROR)) {
		return DoneType.FAIL;
	}
	if (status.includes(ValidationDoneType.WARNING)) {
		return DoneType.WARNING;
	}
	return DoneType.OK;
}

function createValidateResult(module: string): ValidateResult {
	return {
		module: module,
		type: DoneType.FAIL,
		eslint: createEslintValidationResult(false),
		prettier: createPrettierValidationResult(false),
		trancorder: createTrancorderValidationResult(false),
	};
}
