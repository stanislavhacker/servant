import { PrettierrcJson, Modules } from "@servant/servant-data";
import { format, check, getSupportInfo, SupportInfo } from "prettier";
import * as fs from "fs";
import * as path from "path";

import {
	ValidationResult,
	ValidateOptions,
	ValidationDoneType,
	ValidationEngine,
	ValidationUniversalResults,
	ValidationUniversalSeverity,
} from "./types";

type PrettierState = {
	invalidFiles: Array<string>;
	fixedFiles: Array<string>;
	unknownFiles: Array<string>;
};

export type PrettiesValidationResult = ValidationResult & {
	missingPlugins: Array<string>;
};

export function validatePrettier(
	cwd: string,
	module: Modules.ModuleDefinition,
	sourcesFiles: Array<string>,
	{ fix }: ValidateOptions
): Promise<PrettiesValidationResult> {
	return new Promise((resolve, reject) => {
		const result = createPrettierValidationResult(fix);
		const validFiles = sourcesFiles.filter(() => true);

		if (!isPrettier(module)) {
			result.type = ValidationDoneType.OK;
			resolve(result);
			return;
		}

		result.available = true;

		initPrettier(cwd, module).then(
			({ prettierConfig, prettierSupport, missingPlugins, error }) => {
				//no error, load successfully
				if (prettierConfig && prettierSupport) {
					result.type = ValidationDoneType.FAIL;
					Promise.resolve()
						.then(() =>
							fix
								? fixFiles(prettierConfig, prettierSupport, validFiles)
								: checkFiles(prettierConfig, prettierSupport, validFiles)
						)
						.then((state) => {
							const { universalResults, type } = convertToUniversalResults(state);
							result.results = universalResults;
							result.type = type;
							resolve(result);
						})
						.catch(reject);
					return;
				}
				//missing plugins error
				if (missingPlugins && missingPlugins.length > 0) {
					result.missingPlugins = missingPlugins;
					resolve(result);
					return;
				}
				//fail at all
				reject(error);
			}
		);
	});
}

type PrettierInitResults = {
	prettierConfig: PrettierrcJson.PrettierrcJson | null;
	prettierSupport: SupportInfo | null;
	missingPlugins?: Array<string>;
	error?: Error;
};

function initPrettier(cwd: string, module: Modules.ModuleDefinition): Promise<PrettierInitResults> {
	return new Promise((resolve) => {
		const prettierSupport = getSupportInfo();
		const loadedConfig = getPrettierConfig(module);

		if (!loadedConfig) {
			resolve({
				prettierConfig: null,
				prettierSupport: null,
				error: new Error(`Can not load prettier config file.`),
				missingPlugins: [],
			});
			return;
		}

		const prettierConfig = PrettierrcJson.enrichDefaults(loadedConfig);

		if (loadedConfig && prettierSupport) {
			resolve({ prettierConfig, prettierSupport, missingPlugins: [] });
			return;
		}

		resolve({ prettierConfig: null, prettierSupport: null, missingPlugins: [] });
	});
}

function isPrettier(module: Modules.ModuleDefinition) {
	return Boolean(getPrettierConfig(module));
}

export function getPrettierConfig(
	module: Modules.ModuleDefinition
): PrettierrcJson.PrettierrcJson | null {
	if (module.module && module.module.prettierrcJson.content) {
		return module.module.prettierrcJson.content;
	}
	if (module.module && module.module.packageJson.content.prettier) {
		return module.module.packageJson.content.prettier;
	}
	//no def
	return null;
}

function checkFiles(
	config: PrettierrcJson.PrettierrcJson,
	support: SupportInfo,
	files: Array<string>
): Promise<PrettierState> {
	return new Promise((resolve, reject) => {
		const state = createPrettierState();
		Promise.all(
			files.map((file) =>
				checkFile(config, support, file)
					.then(({ valid, unknown }) => {
						state.invalidFiles = [...state.invalidFiles, ...(valid ? [] : [file])];
						state.unknownFiles = [...state.unknownFiles, ...(unknown ? [file] : [])];
					})
					.catch(reject)
			)
		)
			.then(() => resolve(state))
			.catch(reject);
	});
}

function fixFiles(
	config: PrettierrcJson.PrettierrcJson,
	support: SupportInfo,
	files: Array<string>
): Promise<PrettierState> {
	return new Promise((resolve, reject) => {
		const state = createPrettierState();
		Promise.all(
			files.map((file) =>
				fixFile(config, support, file)
					.then(({ fixed, unknown }) => {
						state.fixedFiles = [...state.fixedFiles, ...(fixed ? [file] : [])];
						state.unknownFiles = [...state.unknownFiles, ...(unknown ? [file] : [])];
					})
					.catch(reject)
			)
		)
			.then(() => resolve(state))
			.catch(reject);
	});
}

type FileCheckResults = {
	valid: boolean;
	unknown: boolean;
};

function checkFile(
	config: PrettierrcJson.PrettierrcJson,
	support: SupportInfo,
	file: string
): Promise<FileCheckResults> {
	return new Promise((resolve, reject) => {
		fs.readFile(file, (err, content) => {
			if (err) {
				reject(err);
				return;
			}
			const unknown = isUnknown(support, file);
			const valid = unknown || check(content.toString(), { ...config, filepath: file });
			//check
			resolve({
				valid,
				unknown,
			});
		});
	});
}

type FileFixResults = {
	fixed: boolean;
	unknown: boolean;
};

function fixFile(
	config: PrettierrcJson.PrettierrcJson,
	support: SupportInfo,
	file: string
): Promise<FileFixResults> {
	return new Promise((resolve, reject) => {
		checkFile(config, support, file)
			.then(({ valid, unknown }) => {
				//filer is valid, continue
				if (valid || unknown) {
					resolve({ fixed: false, unknown });
					return;
				}
				//read content
				fs.readFile(file, (err, content) => {
					if (err) {
						reject(err);
						return;
					}
					//format
					const output = format(content.toString(), { ...config, filepath: file });
					fs.writeFile(file, output, (err) => {
						if (err) {
							reject(err);
							return;
						}
						//done
						resolve({ fixed: true, unknown: false });
					});
				});
			})
			.catch(reject);
	});
}

export function createPrettierValidationResult(fix: boolean): PrettiesValidationResult {
	return {
		available: false,
		type: ValidationDoneType.ERROR,
		missingPlugins: [],
		results: {
			files: {},
			errors: [],
		},
		fix,
	};
}

function isUnknown(support: SupportInfo, file: string) {
	const ext = path.extname(file);
	const languages = support.languages.filter((lang) => (lang.extensions || []).includes(ext));

	return languages.length === 0;
}

function createPrettierState(): PrettierState {
	return {
		invalidFiles: [],
		fixedFiles: [],
		unknownFiles: [],
	};
}

//convert related

function convertToUniversalResults({ invalidFiles, unknownFiles, fixedFiles }: PrettierState) {
	const universalResults: ValidationUniversalResults = {
		files: {},
		errors: [],
	};

	invalidFiles.forEach((invalid) => {
		universalResults.files[invalid] = {
			file: invalid,
			fixed: false,
			messages: [
				createError(
					"File is not formatted according to rules defined by prettier",
					"@prettier/core/invalid-format",
					"InvalidFormat",
					true
				),
			],
		};
	});

	unknownFiles.forEach((unknown) => {
		universalResults.files[unknown] = {
			file: unknown,
			fixed: false,
			messages: [
				createError(
					`File is not known to prettier. Invalid extension is "${path.extname(
						unknown
					)}"`,
					"@prettier/core/unknown-file",
					"UnknownFile",
					false
				),
			],
		};
	});

	fixedFiles.forEach((fixed) => {
		universalResults.files[fixed] = {
			file: fixed,
			fixed: true,
			messages: [],
		};
	});

	return {
		universalResults,
		type: convertDoneType(invalidFiles, unknownFiles),
	};
}

function convertDoneType(invalid: Array<string>, unknown: Array<string>): ValidationDoneType {
	if (invalid.length > 0) {
		return ValidationDoneType.ERROR;
	}
	if (unknown.length > 0) {
		return ValidationDoneType.ERROR;
	}
	return ValidationDoneType.OK;
}

function createError(
	message: string,
	ruleId: string,
	nodeType: string,
	fixable: boolean,
	fatal = false
) {
	return {
		engine: ValidationEngine.Prettier,
		message,
		ruleId,
		messageId: ruleId,
		nodeType,
		//status
		fatal,
		fixable,
		severity: ValidationUniversalSeverity.ERROR,
	};
}
