import { EslintrcJson, Modules } from "@servant/servant-data";
import { Extensions, TS, JS } from "@servant/servant-files";
import { ESLint } from "eslint";

import {
	ValidationResult,
	ValidateOptions,
	ValidationDoneType,
	ValidationEngine,
	ValidationUniversalResults,
	ValidationUniversalSeverity,
} from "./types";
import { resolveModule } from "../../resolver";

export const EslintValidExtensions = [...TS.EXT, ...JS.EXT];

export type EslintValidationResult = ValidationResult & {
	missingPlugins: Array<string>;
};

export function validateEslint(
	cwd: string,
	module: Modules.ModuleDefinition,
	sourcesFiles: Array<string>,
	{ fix }: ValidateOptions
): Promise<EslintValidationResult> {
	return new Promise((resolve, reject) => {
		const result = createEslintValidationResult(fix);
		const validFiles = sourcesFiles.filter((file) =>
			Extensions.matchedOne(file, EslintValidExtensions)
		);

		if (!isEslint(module)) {
			result.type = ValidationDoneType.OK;
			resolve(result);
			return;
		}

		result.available = true;

		initEslint(cwd, module, fix).then(({ eslint, missingPlugins, error }) => {
			//no error, load successfully
			if (eslint) {
				result.type = ValidationDoneType.FAIL;
				lintFiles(eslint, validFiles)
					.then((results) => fixFiles(eslint, results, fix))
					.then((results) => {
						const { universalResults, type } = convertToUniversalResults(results);
						result.results = universalResults;
						result.type = type;
						resolve(result);
					})
					.catch(reject);
				return;
			}
			//missing plugins error
			if (missingPlugins && missingPlugins.length > 0) {
				result.missingPlugins = missingPlugins;
				resolve(result);
				return;
			}
			//fail at all
			reject(error);
		});
	});
}

type EslintInitResults = {
	eslint: ESLint | null;
	missingPlugins?: Array<string>;
	error?: Error;
};

function initEslint(
	cwd: string,
	module: Modules.ModuleDefinition,
	fix: boolean
): Promise<EslintInitResults> {
	return new Promise((resolve) => {
		const loadedConfig = getEslintConfig(module);

		if (!loadedConfig) {
			resolve({
				eslint: null,
				error: new Error(`Can not load eslint config file.`),
				missingPlugins: [],
			});
			return;
		}

		const baseConfig = EslintrcJson.enrichDefaults(loadedConfig);

		const parserModule = resolveModule(cwd, baseConfig.parser || "", "", true);
		if (parserModule.err) {
			resolve({
				eslint: null,
				error: parserModule.err,
				missingPlugins: baseConfig.parser ? [baseConfig.parser] : [],
			});
			return;
		}

		try {
			const eslint = new ESLint({
				cwd,
				fix,
				baseConfig,
				//forced
				ignore: true,
				globInputPaths: true,
			});
			resolve({ eslint });
		} catch (error) {
			resolve({ eslint: null, error, missingPlugins: parseMissingPlugins(error) });
		}
	});
}

function isEslint(module: Modules.ModuleDefinition) {
	return Boolean(getEslintConfig(module));
}

function getEslintConfig(module: Modules.ModuleDefinition) {
	if (module.module && module.module.eslintrcJson.content) {
		return module.module.eslintrcJson.content;
	}
	if (module.module && module.module.packageJson.content.eslintConfig) {
		return module.module.packageJson.content.eslintConfig;
	}
	//no def
	return null;
}

function lintFiles(eslint: ESLint, files: Array<string>): Promise<ESLint.LintResult[]> {
	return eslint.lintFiles(files);
}

function fixFiles(
	eslint: ESLint,
	results: ESLint.LintResult[],
	fix: boolean
): Promise<ESLint.LintResult[]> {
	return new Promise((resolve, reject) => {
		if (!fix) {
			resolve(results);
			return;
		}
		ESLint.outputFixes(results)
			.then(() => resolve(results))
			.catch(reject);
	});
}

export function createEslintValidationResult(fix: boolean): EslintValidationResult {
	return {
		available: false,
		type: ValidationDoneType.ERROR,
		missingPlugins: [],
		results: {
			files: {},
			errors: [],
		},
		fix,
	};
}

//eslint related

function parseMissingPlugins(error) {
	if (error.code === "MODULE_NOT_FOUND") {
		return [error.messageData.pluginName];
	}
	return [];
}

//convert related

function convertToUniversalResults(results: ESLint.LintResult[]) {
	const universalResults: ValidationUniversalResults = {
		files: {},
		errors: [],
	};
	const severities: Array<ValidationUniversalSeverity> = [];

	results.forEach((result) => {
		universalResults.files[result.filePath] = {
			file: result.filePath,
			fixed: !!result.output,
			messages: result.messages.map((msg) => {
				const severity = convertSeverity(msg.severity);
				severities.push(severity);

				return {
					engine: ValidationEngine.Eslint,
					column: msg.column,
					line: msg.line,
					endColumn: msg.endColumn ?? msg.column,
					endLine: msg.endLine ?? msg.line,
					message: msg.message,
					messageId: msg.messageId || "",
					ruleId: msg.ruleId || "",
					nodeType: msg.nodeType || "unknown",
					//status
					fatal: msg.fatal || false,
					fixable: Boolean(msg.fix),
					severity,
				};
			}),
		};
	});

	return {
		universalResults,
		type: convertDoneType(severities),
	};
}

function convertSeverity(severity: 0 | 1 | 2): ValidationUniversalSeverity {
	switch (severity) {
		case 2:
			return ValidationUniversalSeverity.ERROR;
		case 1:
			return ValidationUniversalSeverity.WARNING;
		default:
			return ValidationUniversalSeverity.NONE;
	}
}

function convertDoneType(severities: Array<ValidationUniversalSeverity>): ValidationDoneType {
	if (severities.includes(ValidationUniversalSeverity.ERROR)) {
		return ValidationDoneType.ERROR;
	}
	if (severities.includes(ValidationUniversalSeverity.WARNING)) {
		return ValidationDoneType.WARNING;
	}
	return ValidationDoneType.OK;
}
