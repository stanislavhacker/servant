import { TrancorderJson, Modules } from "@servant/servant-data";
import { Extensions, TS, JS, Path } from "@servant/servant-files";
import * as path from "path";
import {
	TrancorderConfigDefaults,
	createTrancorder,
	ValidateProjectResults,
	UpdateResults,
	TrancorderApi,
	TrancorderErrorSeverity,
	TrancorderError,
	TrancorderErrorCode,
} from "trancorder-cli";

import {
	ValidationResult,
	ValidateOptions,
	ValidationDoneType,
	ValidationEngine,
	ValidationUniversalResults,
	ValidationUniversalSeverity,
} from "./types";

export const TrancorderValidExtensions = [...TS.EXT, ...JS.EXT];

export type TrancorderValidationResult = ValidationResult;

export function validateTrancorder(
	cwd: string,
	module: Modules.ModuleDefinition,
	sourcesFiles: Array<string>,
	{ fix }: ValidateOptions
): Promise<TrancorderValidationResult> {
	return new Promise((resolve, reject) => {
		const result = createTrancorderValidationResult(fix);
		const validFiles = sourcesFiles.filter((file) =>
			Extensions.matchedOne(file, TrancorderValidExtensions)
		);

		if (!isTrancorder(module)) {
			result.type = ValidationDoneType.OK;
			resolve(result);
			return;
		}

		result.available = true;

		initTrancorder(cwd, module, validFiles).then(({ trancorder, config, error }) => {
			//no error, load successfully
			if (trancorder && config) {
				result.type = ValidationDoneType.FAIL;
				validateFiles(trancorder, config, fix)
					.then(([results, updates]) => {
						const { universalResults, type } = convertToUniversalResults(
							cwd,
							results,
							updates
						);
						result.results = universalResults;
						result.type = type;
						resolve(result);
					})
					.catch(reject);
				return;
			}
			//fail at all
			reject(error);
		});
	});
}

type TrancorderInitResults = {
	trancorder: TrancorderApi | null;
	config: TrancorderJson.TrancorderConfig | null;
	error?: Error;
};

function initTrancorder(
	cwd: string,
	module: Modules.ModuleDefinition,
	sources: string[]
): Promise<TrancorderInitResults> {
	return new Promise((resolve) => {
		const loadedConfig = getTrancorderConfig(module);

		if (!loadedConfig) {
			resolve({
				trancorder: null,
				config: null,
				error: new Error(`Can not load trancorder config file.`),
			});
			return;
		}

		const baseConfig = TrancorderJson.enrichDefaults(loadedConfig);
		const config = {
			...TrancorderConfigDefaults,
			...baseConfig,
			cwd,
			sources: sources.map((pth) => path.relative(cwd, pth)),
		};

		try {
			const trancorder = createTrancorder(config);
			resolve({ trancorder, config });
		} catch (error) {
			resolve({ trancorder: null, config, error });
		}
	});
}

function isTrancorder(module: Modules.ModuleDefinition) {
	return Boolean(getTrancorderConfig(module));
}

function getTrancorderConfig(module: Modules.ModuleDefinition) {
	if (module.module && module.module.trancorderJson.content) {
		return module.module.trancorderJson.content;
	}
	//no def
	return null;
}

async function validateFiles(
	trancorder: TrancorderApi,
	config: TrancorderJson.TrancorderConfig,
	fix: boolean
): Promise<[ValidateProjectResults, UpdateResults | null]> {
	if (fix) {
		return await fixFiles(trancorder, config);
	}
	return [await trancorder.validate(), null];
}

async function fixFiles(
	trancorder: TrancorderApi,
	config: TrancorderJson.TrancorderConfig
): Promise<[ValidateProjectResults, UpdateResults | null]> {
	const updates = await trancorder.update({
		languages: config.languages || TrancorderConfigDefaults.languages,
	});
	const results = await trancorder.validate();
	return [results, updates];
}

export function createTrancorderValidationResult(fix: boolean): TrancorderValidationResult {
	return {
		available: false,
		type: ValidationDoneType.ERROR,
		results: {
			errors: [],
			files: {},
		},
		fix,
	};
}

//convert related

function convertToUniversalResults(
	cwd: string,
	results: ValidateProjectResults,
	updates: UpdateResults | null
) {
	const universalResults: ValidationUniversalResults = {
		errors: [],
		files: {},
	};
	const severities: Array<ValidationUniversalSeverity> = [];

	universalResults.errors = results.errors.map((err) => {
		return convertError(severities, err);
	});

	Object.keys(results.problems).forEach((file) => {
		convertErrors(
			universalResults,
			severities,
			Path.join(cwd, [file])[0],
			results.problems[file]
		);
	});

	if (updates) {
		Object.keys(updates.errors).forEach((file) => {
			convertErrors(
				universalResults,
				severities,
				Path.join(cwd, [file])[0],
				updates.errors[file]
			);
		});
		Object.keys(updates.changes).forEach((file) => {
			const validations = getValidations(universalResults, Path.join(cwd, [file])[0]);
			validations.fixed = true;
		});
	}

	return {
		universalResults,
		type: convertDoneType(severities),
	};
}

function getValidations(universalResults: ValidationUniversalResults, file: string) {
	return (universalResults.files[file] = universalResults.files[file] || {
		file: file,
		fixed: false,
		messages: [],
	});
}

function convertErrors(
	universalResults: ValidationUniversalResults,
	severities: Array<ValidationUniversalSeverity>,
	file: string,
	problems: TrancorderError[]
) {
	const validations = getValidations(universalResults, file);

	validations.messages.push(
		...problems.map((problem) => {
			return convertError(severities, problem);
		})
	);
}

function convertError(severities: Array<ValidationUniversalSeverity>, problem: TrancorderError) {
	const severity = convertSeverity(problem.severity);
	severities.push(severity);
	const code = TrancorderErrorCode[problem.code];

	return {
		engine: ValidationEngine.Trancorder,
		column: problem.start?.column,
		line: problem.start?.line,
		endColumn: problem.end?.column,
		endLine: problem.end?.line,
		message: problem.message,
		messageId: code,
		ruleId: problem.name,
		nodeType: code,
		//status
		fixable: problem.fixable,
		fatal: false,
		severity,
	};
}

function convertSeverity(severity: TrancorderErrorSeverity): ValidationUniversalSeverity {
	switch (severity) {
		case TrancorderErrorSeverity.Error:
			return ValidationUniversalSeverity.ERROR;
		case TrancorderErrorSeverity.Warning:
			return ValidationUniversalSeverity.WARNING;
		case TrancorderErrorSeverity.Info:
			return ValidationUniversalSeverity.INFO;
		default:
			return ValidationUniversalSeverity.NONE;
	}
}

function convertDoneType(severities: Array<ValidationUniversalSeverity>): ValidationDoneType {
	if (severities.includes(ValidationUniversalSeverity.ERROR)) {
		return ValidationDoneType.ERROR;
	}
	if (severities.includes(ValidationUniversalSeverity.WARNING)) {
		return ValidationDoneType.WARNING;
	}
	return ValidationDoneType.OK;
}
