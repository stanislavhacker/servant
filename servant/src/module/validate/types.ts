export type ValidateOptions = {
	fix: boolean;
};

export enum ValidationDoneType {
	OK = "OK",
	WARNING = "WARNING",
	FAIL = "FAIL",
	ERROR = "ERROR",
}

export enum ValidationUniversalSeverity {
	INFO = 3,
	WARNING = 2,
	ERROR = 1,
	NONE = 0,
}

export enum ValidationEngine {
	Eslint = "eslint",
	Prettier = "prettier",
	Trancorder = "trancorder",
}

export type ValidationUniversalResults = {
	files: Record<string, ValidationUniversalResult>;
	errors: ValidationUniversalMessage[];
};

export type ValidationUniversalResult = {
	file: string;
	fixed: boolean;
	messages: ValidationUniversalMessage[];
};

export type ValidationUniversalMessage = {
	//engine
	engine: ValidationEngine;
	//position in file
	column?: number;
	line?: number;
	endColumn?: number;
	endLine?: number;
	//message
	message: string;
	messageId: string;
	//rule or grammar info
	ruleId: string;
	nodeType: string | "unknown";
	//status
	fatal: boolean;
	severity: ValidationUniversalSeverity;
	fixable: boolean;
};

export type ValidationResult = {
	fix: boolean;
	available: boolean;
	type: ValidationDoneType;
	results: ValidationUniversalResults;
};
