import { PackageJson, ServantJson, Modules } from "@servant/servant-data";
import * as npm from "../../libraries/npm";
import { CommandResult, createResult, DoneType } from "../index";
import { invariant } from "../../invariant";

export type InstallResult = npm.NpmData;

export function install(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	noaudit: boolean
): Promise<CommandResult<InstallResult>> {
	return new Promise((resolve, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const modulePackageJson = module.module.packageJson;
		const moduleServantJson = module.module.servantJson;

		npm.install(modulePackageJson, ServantJson.registry(moduleServantJson.content.registry))
			.then((npmResult) => {
				const message: Array<string> = [];

				switch (npmResult.status) {
					case npm.NpmStatus.OK: {
						let status = DoneType.OK;

						//audit is turned on and found some vulnerabilities
						if (npmResult.data && !noaudit && npmResult.data.vulnerabilities > 0) {
							status = DoneType.WARNING;
							message.push(
								`Install command found ${npmResult.data.vulnerabilities} vulnerabilities in ${module.name}.`
							);
						}

						resolve(
							createResult<npm.NpmData>(
								"install",
								module.name,
								status,
								null,
								message,
								npmResult.data ?? undefined
							)
						);
						break;
					}
					default:
						resolve(
							createResult(
								"install",
								module.name,
								DoneType.FAIL,
								npmResult.error,
								message
							)
						);
						break;
				}
			})
			.catch(reject);
	});
}
