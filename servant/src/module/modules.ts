import {
	PackageJson,
	ServantJson,
	EslintrcJson,
	PrettierrcJson,
	TrancorderJson,
	Modules,
} from "@servant/servant-data";
import { Path } from "@servant/servant-files";
import * as path from "path";
import * as fs from "fs";

import { loadModuleDirectories, loadPatterns } from "../patterns";
import { resolveModule } from "../resolver";

const SELF = "./";

export function loadsModules(
	servantJsonInfo: ServantJson.ServantJsonInfo,
	init?: boolean
): Promise<Array<Modules.ModuleInfo>> {
	return new Promise((fulfill, reject) => {
		const mainPackage = Path.normalize(
			path.join(servantJsonInfo.cwd, PackageJson.PACKAGE_JSON)
		);
		loads(
			servantJsonInfo.cwd,
			loadModulesInternal(servantJsonInfo, init),
			loadModulesExternals(servantJsonInfo, init),
			servantJsonInfo,
			mainPackage
		)
			.then(fulfill)
			.catch(reject);
	});
}

function loads(
	where: string,
	internal: Array<string>,
	external: Array<string>,
	servantJsonInfo?: ServantJson.ServantJsonInfo,
	mainPackage?: string
): Promise<Array<Modules.ModuleInfo>> {
	return new Promise((fulfill, reject) => {
		Promise.all([
			loadPatterns(where, internal, PackageJson.PACKAGE_JSON),
			loadPatterns(where, external, PackageJson.PACKAGE_JSON),
		]).then(([internal, external]) => {
			const items: Array<Promise<Modules.ModuleInfo>> = [];
			//add items
			internal.forEach((file) => {
				//NOTE: If package json is also main package json (root of project), skip loading devDependencies
				items.push(load(file, servantJsonInfo, file === mainPackage, true));
			});
			//add items
			external.forEach((file) => {
				items.push(load(file, servantJsonInfo));
			});
			//prepare all
			Promise.all(items).then(fulfill).catch(reject);
		});
	});
}

export function load(
	file: string,
	servantJsonInfo?: ServantJson.ServantJsonInfo,
	main?: boolean,
	internal?: boolean
): Promise<Modules.ModuleInfo> {
	return new Promise((fulfill, reject) => {
		PackageJson.load(file, main || false)
			.then((packageJson) => {
				const promises: Array<Promise<unknown> | string> = [];
				const deps = PackageJson.dependencies(packageJson);

				//servant.json
				promises.push(ServantJson.load(file, packageJson, servantJsonInfo));
				//eslintrc.json
				promises.push(EslintrcJson.load(file));
				//prettierrc.json
				promises.push(PrettierrcJson.load(file));
				//trancorder.json
				promises.push(TrancorderJson.load(file));
				//deps
				deps.forEach((dep) => {
					if (!PackageJson.versionIsLocal(dep.version)) {
						const resolved = resolveModule(
							packageJson.cwd,
							dep.module,
							PackageJson.PACKAGE_JSON,
							true
						);

						if (!resolved.err) {
							promises.push(PackageJson.load(resolved.path, false));
						}
					}
				});
				//do all
				Promise.all(promises)
					.catch(reject)
					.then((data) => {
						const array = data as Array<unknown>;
						const servantJson = array.shift() as ServantJson.ServantJsonInfo;
						const eslintrcJson = array.shift() as EslintrcJson.EslintrcJsonInfo;
						const prettierJson = array.shift() as PrettierrcJson.PrettierrcJsonInfo;
						const trancorderJson = array.shift() as TrancorderJson.TrancorderJsonInfo;
						const dependencies = array as Array<PackageJson.PackageJsonInfo>;

						loadDirectories(servantJson, internal || false)
							.catch(reject)
							.then((directories: Modules.ModuleDirectories) => {
								fulfill(
									Modules.info(
										packageJson,
										servantJson,
										eslintrcJson,
										prettierJson,
										trancorderJson,
										dependencies,
										directories,
										internal || false
									)
								);
							});
					});
			})
			.catch(reject);
	});
}

function loadModulesInternal(servantJsonInfo: ServantJson.ServantJsonInfo, init?: boolean) {
	const modules = servantJsonInfo.content.modules;

	if (init && !modules.includes(SELF)) {
		return [...modules, SELF];
	}
	return [...modules];
}

function loadModulesExternals(servantJsonInfo: ServantJson.ServantJsonInfo, init?: boolean) {
	const internals = loadModulesInternal(servantJsonInfo, init);

	return internals.reduce(
		(prev, internal) => [
			...prev,
			path.join(internal, "node_modules/@*/*"),
			path.join(internal, "node_modules/*"),
		],
		[]
	);
}

function loadDirectories(
	servantJsonInfo: ServantJson.ServantJsonInfo | undefined,
	internal?: boolean
): Promise<Modules.ModuleDirectories> {
	return new Promise((fulfill, reject) => {
		//no servant
		if (!servantJsonInfo || !internal) {
			fulfill(Modules.directories());
			return;
		}

		readDirectories(servantJsonInfo, servantJsonInfo.cwd)
			.catch(reject)
			.then((directories: string[]) =>
				fulfill(Modules.directories(servantJsonInfo, directories))
			);
	});
}

function readDirectories(
	servantJsonInfo: ServantJson.ServantJsonInfo | undefined,
	dir: string
): Promise<string[]> {
	return new Promise((fulfill, reject) => {
		fs.readdir(dir, (err, files) => {
			//error
			if (err) {
				reject(err);
				return;
			}

			const directories = filterDirectoriesOnly(files, dir);
			const filtered = loadModuleDirectories(directories, servantJsonInfo);

			Promise.all(filtered.map((directory) => readDirectories(servantJsonInfo, directory)))
				.catch(reject)
				.then((data: string[][]) => {
					fulfill([
						...filtered,
						...data.reduce((prev, current) => [...prev, ...current], []),
					]);
				});
		});
	});
}

function filterDirectoriesOnly(files: string[], dir: string) {
	return files
		.map((file) => {
			const stats = fs.statSync(path.join(dir, file));
			if (stats.isDirectory()) {
				return path.join(dir, file);
			}
			return undefined;
		})
		.filter(Boolean) as string[];
}
