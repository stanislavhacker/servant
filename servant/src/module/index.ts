import { Modules } from "@servant/servant-data";

import { Commands } from "../commands";

import {
	DependenciesGraph,
	dependencies,
	DependenciesValidations,
	validateDependencies,
	validateVersions,
	DependenciesValidationsResults,
	scheduleSorted,
	iterateSorted,
	SortedDependencies,
} from "./dependencies";
import { load, loadsModules } from "./modules";

import { clean, CleanResult } from "./clean";
import { install, InstallResult } from "./install";
import { update, UpdateResult } from "./update";
import {
	validate,
	ValidateResult,
	ValidationUniversalResult,
	ValidationResult,
	ValidationUniversalMessage,
	EslintValidationResult,
	ValidationUniversalResults,
	ValidationDoneType,
	ValidationUniversalSeverity,
	ValidationEngine,
} from "./validate";
import { tests, TestingResults, ItemStatus, Expectation, Item, ItemType } from "./tests";
import { build, BuildResult } from "./build";
import {
	unify,
	getUnifyId,
	createUnifyResults,
	UnifyResult,
	UnifyResults,
	UnifiedVersionsInfo,
	UnifyPackageResult,
	UnifyParams,
	ServantUnifyType,
	ServantUnifyProperty,
	PrepareUnifyResults,
	UnifyCustomData,
	UnifyParamsDefault,
	unifyGenerator,
} from "./unify";
import { publish, PublishResult } from "./publish";
import {
	init,
	InitResults,
	InitParams,
	InitCustomData,
	ServantInitType,
	InitParamsDefault,
	initGenerator,
	PrepareInitResults,
	prepareInit,
} from "./init";
import { analyze, AnalyzeResult, LinesResult } from "./analyze";
import { shared, SharedResult, SharedProps } from "./shared";

import ModuleDefinition = Modules.ModuleDefinition;
import ModuleInfo = Modules.ModuleInfo;
import DependencyType = Modules.DependencyType;
import ModuleDirectories = Modules.ModuleDirectories;

export interface CommandResult<T> {
	message: Array<string>;
	type: DoneType;
	command: Commands;
	module: string;
	error: Error | null;
	data: T | null;
}

export enum DoneType {
	OK = "OK",
	WARNING = "WARNING",
	FAIL = "FAIL",
}

export function createResult<T = null>(
	command: Commands,
	module: string,
	type: DoneType,
	error: Error | null,
	message: Array<string>,
	data?: T
): CommandResult<T> {
	return {
		command: command,
		module: module,
		type: type,
		message: message || [],
		error: error,
		data: data || null,
	};
}

export * from "./reports";

export {
	DependenciesGraph,
	ModuleDefinition,
	ModuleInfo,
	ModuleDirectories,
	DependenciesValidations,
	DependenciesValidationsResults,
	iterateSorted,
	scheduleSorted,
	SortedDependencies,
	validateVersions,
	validateDependencies,
	dependencies,
	load,
	loadsModules,
	clean,
	install,
	update,
	build,
	unify,
	publish,
	init,
	prepareInit,
	tests,
	validate,
	analyze,
	shared,
	getUnifyId,
	createUnifyResults,
	AnalyzeResult,
	LinesResult,
	UnifyPackageResult,
	UnifiedVersionsInfo,
	SharedResult,
	SharedProps,
	UnifyParams,
	UnifyResults,
	PrepareUnifyResults,
	UnifyCustomData,
	ServantUnifyType,
	ServantUnifyProperty,
	UnifyParamsDefault,
	unifyGenerator,
	TestingResults,
	ItemStatus,
	Expectation,
	Item,
	ItemType,
	InitParams,
	InitCustomData,
	InitParamsDefault,
	initGenerator,
	ServantInitType,
	InitResults,
	PrepareInitResults,
	BuildResult,
	UnifyResult,
	PublishResult,
	CleanResult,
	InstallResult,
	UpdateResult,
	ValidateResult,
	ValidationResult,
	ValidationDoneType,
	ValidationUniversalResult,
	ValidationUniversalMessage,
	ValidationUniversalResults,
	ValidationUniversalSeverity,
	ValidationEngine,
	EslintValidationResult,
	DependencyType,
};
