import { PackageJson, ServantJson, Modules } from "@servant/servant-data";
import { Path, TS } from "@servant/servant-files";
import * as semver from "semver";
import * as path from "path";
import * as fs from "fs";

import * as npm from "../../libraries/npm";
import * as vcs from "../../vcs/index";
import { invariant } from "../../invariant";
import { CommandResult, createResult, DoneType } from "../index";
import * as Module from "../index";

const anyVersion = "*";

export interface PublishResult {
	module: string;
	versions: [string, string | null];
	anyVersions: Array<string>;
	changed: Array<string>;
	package: string | null;
	published: string | null;
	type: DoneType;
	private: boolean;
	commit: boolean;
	message: string;
}

export function publish(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	graph: Module.DependenciesGraph,
	production: boolean,
	tag?: string,
	freeze?: boolean,
	commit?: boolean,
	increment?: ServantJson.ServantJson["publish"]["increment"]
): Promise<CommandResult<PublishResult>> {
	return new Promise((resolve) => {
		const publishResult = createPublishResult(module.name);
		const reverts = {} as Partial<PackageJson.PackageJson>;

		let p = Promise.resolve();

		function error(err) {
			publishResult.type = DoneType.FAIL;
			resolve(
				createResult("publish", module.name, publishResult.type, err, [], publishResult)
			);
		}

		//update package json
		p = p.then(() =>
			updatePackageJson(
				module,
				graph,
				publishResult,
				freeze || false,
				increment,
				reverts
			).catch(error)
		);
		//publish or create package
		p = p.then(() => publishModule(module, publishResult, production, tag).catch(error));
		//revert some changes if publish is ok
		p = p.then(() => revertPackageJson(module, graph, publishResult, reverts).catch(error));

		//automatic commit to git
		if (commit) {
			p = p.then(() => commitChanges(module, graph, publishResult).catch(error));
		}

		p.then(() => {
			publishResult.type = resultType(publishResult);
			resolve(
				createResult("publish", module.name, publishResult.type, null, [], publishResult)
			);
		});
		p.catch(error);
	});
}

//UPDATE PACKAGE JSON

function updatePackageJson(
	module: Modules.ModuleDefinition,
	graph: Module.DependenciesGraph,
	results: PublishResult,
	freeze: boolean,
	increment: ServantJson.ServantJson["publish"]["increment"] | undefined,
	reverts: Partial<PackageJson.PackageJson>
): Promise<void> {
	invariant(
		module.module,
		"No module declaration found. Invalid data provided or is probably error in Servant."
	);

	const packageJson = module.module.packageJson;
	return PackageJson.update(packageJson.path, (json, done) => {
		let p = Promise.resolve() as Promise<unknown>;

		p = p.then(() => updateDependencies(graph, results, module, json, reverts));
		p = p.then(() => updateMain(graph, results, module, json, reverts));
		p = p.then(() => updateVersion(graph, results, module, json, increment));

		if (freeze) {
			p = p.then(() => freezeVersions(graph, results, module, json));
		}
		//done
		p.then(done).catch(done);
	});
}

function updateDependencies(
	graph: Module.DependenciesGraph,
	results: PublishResult,
	module: Modules.ModuleDefinition,
	json: PackageJson.PackageJson,
	reverts: Partial<PackageJson.PackageJson>
): Promise<void> {
	return new Promise((fulfill) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const servantJson = module.module.servantJson;

		const dependencies = json.dependencies || {};
		const devDependencies = json.devDependencies || {};
		const optionalDependencies = json.optionalDependencies || {};
		const bundledDependencies = json.bundledDependencies || json.bundleDependencies || [];

		//save
		reverts.dependencies = json.dependencies ? { ...json.dependencies } : undefined;
		reverts.devDependencies = json.devDependencies ? { ...json.devDependencies } : undefined;
		reverts.optionalDependencies = json.optionalDependencies
			? { ...json.optionalDependencies }
			: undefined;

		Object.keys(dependencies).forEach((dep) => {
			if (bundledDependencies.indexOf(dep) >= 0) {
				delete dependencies[dep];
			} else {
				dependencies[dep] = updateLocalDependencyVersion(
					dep,
					dependencies[dep],
					graph,
					results
				);
			}
		});
		Object.keys(devDependencies).forEach((dep) => {
			if (bundledDependencies.indexOf(dep) >= 0) {
				delete devDependencies[dep];
			} else {
				devDependencies[dep] = updateLocalDependencyVersion(
					dep,
					devDependencies[dep],
					graph,
					results
				);
			}
		});
		Object.keys(optionalDependencies).forEach((dep) => {
			if (bundledDependencies.indexOf(dep) >= 0) {
				delete optionalDependencies[dep];
			} else {
				optionalDependencies[dep] = updateLocalDependencyVersion(
					dep,
					optionalDependencies[dep],
					graph,
					results
				);
			}
		});

		//delete dev dependencies
		if (servantJson.content.publish.stripDev) {
			delete json.devDependencies;
		}

		//delete bundled dependencies
		reverts.bundledDependencies = json.bundledDependencies;
		delete json.bundledDependencies;
		reverts.bundleDependencies = json.bundleDependencies;
		delete json.bundleDependencies;

		fulfill();
	});
}

function updateMain(
	graph: Module.DependenciesGraph,
	results: PublishResult,
	module: Modules.ModuleDefinition,
	json: PackageJson.PackageJson,
	reverts: Partial<PackageJson.PackageJson>
): Promise<void> {
	return new Promise((fulfill) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const servantJson = module.module.servantJson;
		const output = servantJson.content.output;

		const main = json.main;
		const types = json.types;
		const entry =
			servantJson.content.entry !== false && typeof servantJson.content.entry === "string";

		//rewrite main for publish entry point
		if (entry) {
			json.main = getMain(output);
		}
		//check if types exists
		if (json.types && entry && typeof servantJson.content.entry === "string") {
			json.types = getTypes(servantJson.cwd, servantJson.content.entry, output);
		}

		//save
		reverts.main = main;
		reverts.types = types;

		fulfill();
	});
}

function getMain(output: ServantJson.ServantOutput): string {
	return Path.normalize(path.join(output.directory, output.filename));
}

function getTypes(
	cwd: string,
	entry: string,
	output: ServantJson.ServantOutput
): string | undefined {
	const paths: Array<string> = [];
	const filename = TS.toDTS(entry);

	let dir = path.dirname(entry);

	//Make files to top of path
	do {
		paths.unshift(path.join(cwd, output.directory, dir, filename));
		dir = path.join(dir, "../");
	} while (paths[0] !== paths[1] && paths[0] !== path.join(cwd, output.directory, filename));

	for (let i = paths.length - 1; i >= 0; i--) {
		if (fs.existsSync(paths[i])) {
			return Path.normalize(path.relative(cwd, paths[i]));
		}
	}
	return undefined;
}

function updateVersion(
	graph: Module.DependenciesGraph,
	results: PublishResult,
	module: Modules.ModuleDefinition,
	json: PackageJson.PackageJson,
	increment?: ServantJson.ServantJson["publish"]["increment"]
): Promise<void> {
	return new Promise((fulfill, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const servantJson = module.module.servantJson;
		const [version, inc] = updateModuleVersion(servantJson, json.version, results, increment);

		//increment is ok
		if (version) {
			json.version = version;
			module.versions = [json.version];

			fulfill();
			return;
		}
		//error
		reject(
			new Error(
				`Can not update version in "package.json" for module "${json.name}". Value "${inc}" is not valid for increment type.`
			)
		);
	});
}

function freezeVersions(
	graph: Module.DependenciesGraph,
	results: PublishResult,
	module: Modules.ModuleDefinition,
	json: PackageJson.PackageJson
): Promise<void> {
	return new Promise((fulfill) => {
		const dependencies = json.dependencies || {};
		const devDependencies = json.devDependencies || {};

		const modules = {};
		(module.module?.dependencies ?? []).forEach((dep) => {
			modules[dep.content.name] = dep.content.version;
		});

		Object.keys(dependencies).forEach((dep) => {
			dependencies[dep] = modules[dep] || dependencies[dep];
		});
		Object.keys(devDependencies).forEach((dep) => {
			devDependencies[dep] = modules[dep] || devDependencies[dep];
		});

		fulfill();
	});
}

function updateLocalDependencyVersion(
	depName: string,
	depVersion: string,
	graph: Module.DependenciesGraph,
	results: PublishResult
) {
	const module = graph.modules[depName];

	let isLocal = false;
	//NOTE: Local is module with loaded definition ...
	isLocal = isLocal || Boolean(module && module.module);
	//NOTE: ... or module with file: dependency
	isLocal = isLocal || PackageJson.versionIsLocal(depVersion);

	if (isLocal) {
		const version = getLocalDependencyVersion(graph, depName);

		if (version === anyVersion) {
			results.anyVersions.push(depName);
		}

		results.changed = [...new Set([...results.changed, depName])];

		return version;
	}
	return depVersion;
}

function getLocalDependencyVersion(graph: Module.DependenciesGraph, module: string): string {
	if (graph.modules[module]) {
		const info = graph.modules[module];
		return info.versions.filter(PackageJson.versionIsExternal)[0];
	}

	return anyVersion;
}

function updateModuleVersion(
	servantJson: ServantJson.ServantJsonInfo,
	version: string,
	results: PublishResult,
	increment?: ServantJson.ServantJson["publish"]["increment"]
): [string | null, string] {
	let nextVersion: string | null = version;
	const inc = increment || servantJson.content.publish.increment;

	if (!validateIncrement(inc)) {
		return [null, inc];
	}

	//increment version if is not disabled
	if (inc !== "none") {
		nextVersion = semver.inc(version, inc);
	}

	results.versions = [version, nextVersion];
	return [nextVersion || version, inc];
}

function validateIncrement(increment: ServantJson.ServantJson["publish"]["increment"]) {
	const values = [
		"major",
		"premajor",
		"minor",
		"preminor",
		"patch",
		"prepatch",
		"prerelease",
		"none",
	] as Array<ServantJson.ServantJson["publish"]["increment"]>;

	return values.indexOf(increment) >= 0;
}

//PUBLISH

function publishModule(
	module: Modules.ModuleDefinition,
	results: PublishResult,
	production: boolean,
	tag?: string
): Promise<void> {
	return new Promise((fulfill, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const packageJson = module.module.packageJson;
		const servantJson = module.module.servantJson;

		//NOTE: Production mode and set as private, do not try to publish it
		if (production && packageJson.content.private) {
			results.private = packageJson.content.private;
			fulfill();
			return ``;
		}

		//NOTE: Production mode
		if (production) {
			npm.publish(
				packageJson,
				tag,
				servantJson.content.publish.access,
				ServantJson.registry(servantJson.content.registry)
			)
				.then((npmResult) => {
					if (npmResult.error) {
						reject(npmResult.error);
						return;
					}
					if (npmResult.data) {
						results.published = npmResult.data.published;
					}
					fulfill();
				})
				.catch(reject);
			return;
		}

		//NOTE: Create pack if is not production call
		npm.pack(packageJson)
			.then((npmResult) => {
				if (npmResult.error) {
					reject(npmResult.error);
					return;
				}
				if (npmResult.data) {
					results.package = npmResult.data.package;
				}
				fulfill();
			})
			.catch(reject);
	});
}

//REVERT

function revertPackageJson(
	module: Modules.ModuleDefinition,
	graph: Module.DependenciesGraph,
	results: PublishResult,
	reverts: Partial<PackageJson.PackageJson>
): Promise<void> {
	invariant(
		module.module,
		"No module declaration found. Invalid data provided or is probably error in Servant."
	);

	const packageJson = module.module.packageJson;
	return PackageJson.update(packageJson.path, (json, done) => {
		//revert
		json.dependencies = reverts.dependencies;
		json.devDependencies = reverts.devDependencies;
		json.optionalDependencies = reverts.optionalDependencies;
		json.bundledDependencies = reverts.bundledDependencies;
		json.bundleDependencies = reverts.bundleDependencies;
		json.main = reverts.main;
		json.types = reverts.types;
		//done
		done();
	});
}

//COMMIT

function commitChanges(
	module: Modules.ModuleDefinition,
	graph: Module.DependenciesGraph,
	results: PublishResult
): Promise<void> {
	invariant(
		module.module,
		"No module declaration found. Invalid data provided or is probably error in Servant."
	);

	const servantJson = module.module.servantJson;
	const publish = servantJson.content.publish;

	return new Promise((fulfill, reject) => {
		let message = publish.commitMessage;

		message = message.replace(/\$increment/gi, publish.increment);
		message = message.replace(/\$version/gi, results.versions[1] || results.versions[0]);

		vcs.commit(servantJson, message).then((success) => {
			//ok
			if (success) {
				results.commit = true;
				results.message = message;
				fulfill();
				return;
			}
			//error
			reject(
				new Error(
					`Can not perform commit with message '${message}'. Do you have git repository initialized?`
				)
			);
		});
	});
}

//Others

function createPublishResult(module: string): PublishResult {
	return {
		package: null,
		published: null,
		module: module,
		anyVersions: [],
		changed: [],
		versions: ["", null],
		private: false,
		type: DoneType.FAIL,
		commit: false,
		message: "",
	};
}

function resultType(result: PublishResult): DoneType {
	let warning = false;

	warning = warning || result.anyVersions.length > 0;
	warning = warning || result.versions[1] === null;

	return warning ? DoneType.WARNING : DoneType.OK;
}
