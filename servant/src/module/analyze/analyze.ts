import { Modules, PackageJson } from "@servant/servant-data";
import { TS, JS, SASS, LESS, CSS, Path } from "@servant/servant-files";
import { FileLines, Stats, stats } from "@servant/servant-stats";
import * as semver from "semver";

import { invariant } from "../../invariant";
import { loadPackageJsonFiles, loadServantJsonFiles } from "../../patterns";
import { CommandResult, createResult, DoneType } from "../index";
import * as Module from "../index";

export interface AnalyzeResult {
	name: string;
	missing: boolean;
	type: Modules.DependencyType[];
	versions: {
		ranges: Array<string>;
		intersects: boolean;
	};
	sizes: {
		bundle: number;
		sources: number;
		tests: number;
		installation: number;
	};
	lines: {
		sources: LinesResult;
		tests: LinesResult;
	};
	//recursive
	dependencies: Array<AnalyzeResult>;
}

export interface LinesResult {
	typescript: number;
	javascript: number;
	less: number;
	sass: number;
	css: number;
}

export function analyze(
	packageJson: PackageJson.PackageJsonInfo,
	graph: Module.DependenciesGraph,
	module: Modules.ModuleDefinition,
	cache: Record<string, AnalyzeResult> = {}
): Promise<CommandResult<AnalyzeResult>> {
	return new Promise((resolve, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		buildAnalyzeResult(graph, module, cache)
			.then((result) => {
				resolve(createResult("analyze", module.name, DoneType.OK, null, [], result));
			})
			.catch(reject);
	});
}

function buildAnalyzeResult(
	graph: Module.DependenciesGraph,
	module: Modules.ModuleDefinition,
	cache: Record<string, AnalyzeResult>
): Promise<AnalyzeResult> {
	return new Promise((resolve, reject) => {
		const usedTree = cache[module.name];

		if (usedTree) {
			resolve(usedTree);
			return;
		}

		const versions = module.versions.filter(PackageJson.versionIsExternal);
		const tree = createAnalyzeResult(module.name, versions, module.depType);

		//add as used
		cache[module.name] = tree;

		processDependencies(tree, module, graph, cache)
			.then(() => processStats(module))
			.then((stats) => processProps(tree, module, stats))
			.then(() => resolve(tree))
			.catch(reject);
	});
}

function processDependencies(
	tree: AnalyzeResult,
	module: Modules.ModuleDefinition,
	graph: Module.DependenciesGraph,
	used: Record<string, AnalyzeResult>
) {
	const dependencies = Promise.all([
		...module.internals.map((m) => buildAnalyzeResult(graph, m, used)),
		...module.externals.map((m) => buildAnalyzeResult(graph, m, used)),
	]);

	return dependencies.then((deps) => {
		tree.dependencies = deps;
	});
}

//props

function processProps(tree: AnalyzeResult, module: Modules.ModuleDefinition, stats: ModuleStats) {
	return Promise.resolve().then(() => {
		tree.missing = module.missing;
		tree.versions = processVersions(tree.versions);
		tree.sizes = processSizes(tree, module, stats);
		tree.lines = processLines(tree, module, stats);
	});
}

function processVersions(versions: AnalyzeResult["versions"]): AnalyzeResult["versions"] {
	const ranges = versions.ranges.filter((ver) => semver.validRange(ver));
	if (ranges.length > 0) {
		return {
			...versions,
			intersects: ranges.every((a) => ranges.every((b) => semver.intersects(a, b))),
		};
	}
	return versions;
}

type ModuleStats = {
	bundle: Stats;
	sources: Stats;
	tests: Stats;
};
function processStats(module: Modules.ModuleDefinition): Promise<ModuleStats> {
	return new Promise((resolve) => {
		const filesStats = loadPackageJsonFiles(module.module?.packageJson).then(({ cwd, files }) =>
			stats(Path.join(cwd, files), { countLines: false })
		);
		const sourcesStats = loadServantJsonFiles(module.module?.servantJson).then(
			({ cwd, sources, tests }) =>
				Promise.all([stats(Path.join(cwd, sources)), stats(Path.join(cwd, tests))])
		);

		Promise.all([filesStats, sourcesStats])
			.then(([bundleStats, sourcesStats]) => [bundleStats, ...sourcesStats])
			.then(([bundleStats, sourcesStats, testsStats]) =>
				resolve({
					bundle: bundleStats,
					sources: sourcesStats,
					tests: testsStats,
				})
			);
	});
}

function processSizes(
	tree: AnalyzeResult,
	module: Modules.ModuleDefinition,
	stats: ModuleStats
): AnalyzeResult["sizes"] {
	const bundle = calculateSize(stats.bundle);
	const sources = calculateSize(stats.sources);
	const tests = calculateSize(stats.tests);
	const installation = calculateInstallationSize(tree.dependencies, module, bundle);

	return { bundle, sources, tests, installation };
}

function processLines(
	tree: AnalyzeResult,
	module: Modules.ModuleDefinition,
	stats: ModuleStats
): AnalyzeResult["lines"] {
	//no internal module
	if (!module.module?.internal) {
		return {
			sources: createLinesResult(),
			tests: createLinesResult(),
		};
	}

	const sources = calculateLines(stats.sources);
	const tests = calculateLines(stats.tests);

	return { sources, tests };
}

//create

function createAnalyzeResult(
	name: string,
	versions: string[],
	type: Modules.DependencyType[]
): AnalyzeResult {
	return {
		name,
		type,
		missing: false,
		versions: {
			ranges: versions,
			intersects: true,
		},
		sizes: {
			bundle: 0,
			tests: 0,
			sources: 0,
			installation: 0,
		},
		lines: {
			sources: createLinesResult(),
			tests: createLinesResult(),
		},
		dependencies: [],
	};
}

function createLinesResult(): LinesResult {
	return {
		css: 0,
		sass: 0,
		javascript: 0,
		less: 0,
		typescript: 0,
	};
}

//size

function calculateSize(files: Stats): number {
	return Object.values(files.files).reduce((prev, item) => prev + item.stats.size, 0);
}

function calculateInstallationSize(
	dependencies: AnalyzeResult[],
	module: Modules.ModuleDefinition,
	bundle: number
) {
	const bundled = module.module
		? PackageJson.bundledDependencies(module.module?.packageJson)
		: [];
	const installed = dependencies.filter((dep) => bundled.indexOf(dep.name) === -1);

	return installed.reduce((prev, dep) => prev + dep.sizes.bundle, 0) + bundle;
}

//lines

function calculateLines(data: Stats) {
	return Object.keys(data.lines).reduce((prev, current) => {
		const lines: FileLines = data.lines[current];
		const file = "test" + current;

		if (TS.isTypescript(file)) {
			prev["typescript"] += lines.meaningful;
		}
		if (JS.isJavascript(file)) {
			prev["javascript"] += lines.meaningful;
		}
		if (LESS.isLESS(file)) {
			prev["less"] += lines.meaningful;
		}
		if (SASS.isSASS(file)) {
			prev["sass"] += lines.meaningful;
		}
		if (CSS.isCSS(file)) {
			prev["css"] += lines.meaningful;
		}

		return prev;
	}, createLinesResult());
}
