import { ServantJson } from "@servant/servant-data";

export interface Issues {
	[key: string]: Issue;
}
export interface Issue {
	id: string;
	url: string;
	passed: number;
	failed: number;
	excluded: number;
	pending: number;
}

export function extractIssues(
	servantJson: ServantJson.ServantJsonInfo | undefined,
	text: string,
	passed: boolean,
	excluded: boolean,
	pending: boolean,
	bugs: Issues = {}
) {
	//no servant json defined
	if (!servantJson) {
		return;
	}

	const issues = servantJson.content.issues;

	Object.keys(issues).forEach((pattern) => {
		const results = text.match(reg(pattern)) || [];

		results.forEach((id) => {
			const args = reg(pattern).exec(id);
			const bug = (bugs[id] =
				bugs[id] || createIssue(id, createUrl(issues[pattern], args || [])));

			if (excluded) {
				bug.excluded++;
			} else if (pending) {
				bug.pending++;
			} else if (passed) {
				bug.passed++;
			} else {
				bug.failed++;
			}
		});
	});
}

export function mergeIssues(bugs1: Issues, bugs2: Issues = {}): Issues {
	const bugs: Issues = {};
	Object.keys(bugs1).forEach((id) => {
		bugs[id] = mergeIssue(bugs[id], bugs1[id]);
	});
	Object.keys(bugs2).forEach((id) => {
		bugs[id] = mergeIssue(bugs[id], bugs2[id]);
	});
	return bugs;
}

function mergeIssue(bug1: Issue | null | undefined, bug2: Issue): Issue {
	if (bug1) {
		const bug = { ...bug1 };
		bug.passed += bug2.passed;
		bug.excluded += bug2.excluded;
		bug.failed += bug2.failed;
		bug.pending += bug2.pending;
		return bug;
	}
	return { ...bug2 };
}

export function createIssue(id: string, url: string): Issue {
	return {
		id: id,
		url: url,
		excluded: 0,
		failed: 0,
		passed: 0,
		pending: 0,
	};
}

export function createUrl(url: string, args: Array<string>): string {
	let fullUrl = url;

	args.forEach((arg, index) => {
		fullUrl = fullUrl.replace(`$${index}`, arg);
	});

	return fullUrl;
}

function reg(pattern: string): RegExp {
	return new RegExp(pattern, "gi");
}
