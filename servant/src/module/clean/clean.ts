import { PackageJson, ServantJson, Modules } from "@servant/servant-data";
import { Path, Extensions } from "@servant/servant-files";
import * as path from "path";
import * as fs from "fs";
import * as minimatch from "minimatch";

import { CommandResult, createResult, DoneType } from "../index";
import * as vcs from "../../vcs/index";
import { loadPatterns, loadCleaned } from "../../patterns";
import { ResolvedEntries } from "../../entries";
import { invariant } from "../../invariant";

const prunePatterns = [Path.patterns.all("node_modules"), "package-lock.json"];

export interface CleanResult {
	module: string;
	removed: Array<string>;
}

type FilesData = {
	dirs: Array<string>;
	files: Array<string>;
};

export function clean(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	prune: boolean,
	entries: ResolvedEntries
): Promise<CommandResult<CleanResult>> {
	return new Promise((resolve, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const moduleDef = module.module;
		const cleanResult = createCleanResult(module.name);

		const loads: Array<Promise<Array<string>>> = [];
		loads.push(loadSkippedPatterns(module));
		loads.push(loadCleanedFiles(module, entries, prune));

		Promise.all(loads)
			.catch(reject)
			.then((data: [Array<string>, Array<string>]) => {
				const [skipped, clean] = data;
				const files = filterClean(skipped, clean);
				const filesData = loadData(files);

				deleteData(moduleDef.servantJson, filesData)
					.catch(reject)
					.then((removed: Array<string>) => {
						cleanResult.removed = removed;
						resolve(
							createResult<CleanResult>(
								"clean",
								module.name,
								DoneType.OK,
								null,
								[],
								cleanResult
							)
						);
					});
			});
	});
}

function getVersionedFiles(servantJson: ServantJson.ServantJsonInfo): Promise<Array<string>> {
	return new Promise((resolve, reject) => {
		vcs.git
			.versioned(servantJson)
			.then((versioned) => {
				resolve(versioned.map((file) => Path.normalize(path.join(servantJson.cwd, file))));
			})
			.catch(reject);
	});
}

function getSymlinkFiles(module: Modules.ModuleDefinition): Promise<Array<string>> {
	return new Promise((resolve) => {
		const patterns: Array<string> = [];
		module.internals.forEach((int) => {
			const directory = Path.patterns.everywhere(path.join("node_modules", int.name));
			patterns.push(Path.patterns.all(directory));
			patterns.push(directory);
		});
		resolve(patterns);
	});
}

function loadSkippedPatterns(module: Modules.ModuleDefinition): Promise<Array<string>> {
	return new Promise((resolve, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const moduleServantJson = module.module.servantJson;
		const skipped: Array<Promise<Array<string>>> = [];

		skipped.push(getVersionedFiles(moduleServantJson));
		skipped.push(getSymlinkFiles(module));

		Promise.all(skipped)
			.then((patterns: Array<Array<string>>) => {
				const skippedPatterns: Array<string> = [];
				patterns.forEach((ptrns) => {
					skippedPatterns.push(...ptrns);
				});
				resolve(skippedPatterns);
			})
			.catch(reject);
	});
}

function loadCleanedFiles(
	module: Modules.ModuleDefinition,
	entries: ResolvedEntries,
	prune: boolean
): Promise<Array<string>> {
	return new Promise((resolve, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const promises: Array<Promise<Array<string>>> = [];

		const modulePackageJson = module.module.packageJson;
		const moduleServantJson = module.module.servantJson;

		//prune: delete node_modules and package-lock.json
		if (prune) {
			promises.push(loadPatterns(moduleServantJson.cwd, prunePatterns));
		}
		//check files
		if (
			modulePackageJson &&
			modulePackageJson.content.files &&
			modulePackageJson.content.files.length
		) {
			//all files
			promises.push(loadPatterns(moduleServantJson.cwd, modulePackageJson.content.files));
		}
		//output directory
		if (moduleServantJson.content.entry !== false) {
			const outputDir = Path.patterns.all(moduleServantJson.content.output.directory);
			promises.push(loadPatterns(moduleServantJson.cwd, [outputDir]));
		}
		//check tmp
		const tempDir = Path.patterns.all(moduleServantJson.content.temp);
		promises.push(
			loadPatterns(moduleServantJson.cwd, [tempDir, moduleServantJson.content.temp])
		);
		//check extensions
		if (modulePackageJson && moduleServantJson) {
			promises.push(loadExtensions(modulePackageJson, moduleServantJson));
		}

		//load shared
		promises.push(loadSharedPatterns(moduleServantJson, entries, module.name));
		//load generated
		promises.push(loadGenerated(modulePackageJson, moduleServantJson));

		//load all
		Promise.all(promises)
			.catch(reject)
			.then((loaded: Array<Array<string>>) => {
				const files: Array<string> = [];
				loaded.forEach((fls) => {
					files.push(...fls);
				});
				resolve(files);
			});
	});
}

function loadData(paths: Array<string>): FilesData {
	const sorted = paths.slice(0).sort((a, b) => b.length - a.length);
	const files: Array<string> = [];
	const dirs: Array<string> = [];

	sorted.forEach((s) => {
		try {
			const stat = fs.statSync(s);
			if (stat.isDirectory()) {
				dirs.push(s);
			} else {
				files.push(s);
			}
		} catch (e) {
			files.push(s);
		}
	});

	return {
		files: files,
		dirs: dirs,
	};
}

function loadExtensions(
	packageJson: PackageJson.PackageJsonInfo,
	servantJson: ServantJson.ServantJsonInfo
): Promise<Array<string>> {
	const all: Array<string> = [];

	servantJson.content.clean.forEach((ext) => {
		[...servantJson.content.src, ...servantJson.content.tests].forEach((src) => {
			all.push(Extensions.replace(src, ext));
		});
	});

	return loadPatterns(servantJson.cwd, all);
}

function loadSharedPatterns(
	servantJson: ServantJson.ServantJsonInfo,
	entries: ResolvedEntries,
	name: string
): Promise<Array<string>> {
	const { client, server } = entries;
	const folder = path.basename(servantJson.cwd);

	const folders = [
		...Object.keys(client.entries[name]?.shared ?? {}),
		...Object.keys(server.entries[name]?.shared ?? {}),
	].map((item) => {
		return Path.normalize(item).replace(`${folder}/`, "");
	});

	return loadPatterns(servantJson.cwd, folders);
}

function loadGenerated(
	packageJson: PackageJson.PackageJsonInfo,
	servantJson: ServantJson.ServantJsonInfo
): Promise<Array<string>> {
	return new Promise((resolve) => {
		const what = [...servantJson.content.src, ...servantJson.content.tests];

		loadPatterns(servantJson.cwd, what).then((files) => resolve(loadCleaned(files)));
	});
}

function filterClean(skipped: Array<string>, clean: Array<string>): Array<string> {
	return clean.filter((file) => {
		if (skipped.indexOf(file) >= 0) {
			return false;
		}
		return skipped.every((skip) => {
			return !minimatch(file, skip, { dot: true });
		});
	});
}

//Delete

function deleteData(
	servantJson: ServantJson.ServantJsonInfo,
	data: FilesData
): Promise<Array<string>> {
	return new Promise((resolve) => {
		const items = [data.files, ...data.dirs.map((dir) => [dir])];
		const removed: Array<string | null> = [];
		let p: Promise<void> = Promise.resolve();

		items.forEach((items) => {
			p = p.then(() => {
				const promises: Array<Promise<string | null>> = [];

				items.forEach((file) => {
					promises.push(deleteFile(servantJson, file));
				});

				return Promise.all(promises).then((files) => {
					removed.push(...files);
				});
			});
		});

		//done
		p.then(() => resolve(removed.filter((file) => Boolean(file)) as Array<string>));
	});
}

function deleteFile(
	servantJson: ServantJson.ServantJsonInfo,
	file: string
): Promise<string | null> {
	return new Promise((resolve) => {
		fs.stat(file, (err, stat) => {
			if (err) {
				resolve(null);
				return;
			}

			if (stat.isDirectory()) {
				fs.rmdir(file, () => {
					resolve(file);
				});
			} else {
				fs.unlink(file, () => {
					resolve(file);
				});
			}
		});
	});
}

//Others

function createCleanResult(module: string): CleanResult {
	return {
		module: module,
		removed: [],
	};
}
