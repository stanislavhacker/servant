import { PackageJson, ServantJson, Modules } from "@servant/servant-data";

import * as npm from "../../libraries/npm";
import { CommandResult, createResult, DoneType } from "../index";
import { invariant } from "../../invariant";

export type UpdateResult = npm.NpmData;

export function update(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition
): Promise<CommandResult<UpdateResult>> {
	return new Promise((resolve, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const modulePackageJson = module.module.packageJson;
		const moduleServantJson = module.module.servantJson;

		npm.update(modulePackageJson, ServantJson.registry(moduleServantJson.content.registry))
			.then((npmResult) => {
				const message: Array<string> = [];

				switch (npmResult.status) {
					case npm.NpmStatus.OK: {
						let status = DoneType.OK;

						if (npmResult.data && npmResult.data.vulnerabilities > 0) {
							status = DoneType.WARNING;
							message.push(
								`Update command found ${npmResult.data.vulnerabilities} vulnerabilities in ${module.name}.`
							);
						}

						resolve(
							createResult<npm.NpmData>(
								"update",
								module.name,
								status,
								null,
								message,
								npmResult.data ?? undefined
							)
						);
						break;
					}
					default:
						resolve(
							createResult(
								"update",
								module.name,
								DoneType.FAIL,
								npmResult.error,
								message
							)
						);
						break;
				}
			})
			.catch(reject);
	});
}
