import { PackageJson } from "@servant/servant-data";
import { GeneratorAnswer } from "@servant/servant-generators";

import { createResult, DependenciesGraph } from "../";

import {
	ServantUnifyType,
	UnifyCustomData,
	UnifyParams,
	UnifiedVersionsInfo,
	ServantUnifyProperty,
	UnifyResults,
} from "./types";
import { run, save } from "./generator";
import { getUnifyId } from "./utils";

export const UnifyParamsDefault: Required<UnifyParams> = {
	unified: {},
	type: ServantUnifyType.Latest,
};

export function unify(
	packageJson: PackageJson.PackageJsonInfo,
	graph: DependenciesGraph,
	{ unified = UnifyParamsDefault.unified, type = UnifyParamsDefault.type }: UnifyParams
): Promise<UnifyResults> {
	return new Promise((resolve) => {
		const answers = generateAnswers(type, unified);
		run(answers, { graph }).then(({ results, fn }) => {
			const customData = fn.getCustomData<UnifyCustomData>();
			save(customData.entry, results).then(() => {
				resolve(createUnifyResults(customData, results.errors));
			});
		});
	});
}

export function createUnifyResults(data: Partial<UnifyCustomData>, errors: Error[]): UnifyResults {
	const modules = (data.results || []).map((result) => {
		return createResult("unify", result.module, result.type, result.err, [], result);
	});

	return {
		errors,
		modules,
	};
}

function generateAnswers(type: ServantUnifyType, unified: UnifiedVersionsInfo): GeneratorAnswer[] {
	const answers: GeneratorAnswer[] = [];

	addAnswer(answers, ServantUnifyProperty.Type, UnifyParamsDefault.type, type);

	Object.keys(unified).forEach((module) => {
		const version = unified[module];
		addAnswer(answers, getUnifyId(module), version, version);
	});

	return answers;
}

function addAnswer(
	answers: GeneratorAnswer[],
	id: ServantUnifyProperty | string,
	def: string,
	value?: string
) {
	answers.push({
		id,
		value: value ?? def,
	});
}
