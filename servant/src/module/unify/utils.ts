import { FnGetAnswer } from "@servant/servant-generators";

import { ServantUnifyProperty } from "./types";

export function getValueOrDefault<T>(
	getAnswer: FnGetAnswer,
	property: ServantUnifyProperty,
	def: T
): T {
	const nameValue = getAnswer(property);
	return (nameValue ? nameValue.value : def) as T;
}

export function getUnifyId(module: string): string {
	return ServantUnifyProperty.Unified + "-" + module;
}
