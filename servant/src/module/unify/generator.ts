import {
	api,
	GeneratorAnswer,
	GeneratorEmptyResults,
	GeneratorResults,
} from "@servant/servant-generators";

import { loaded } from "./generators/main";
import { PrepareUnifyResults } from "./types";

export function run(
	answers: GeneratorAnswer[],
	props: PrepareUnifyResults
): ReturnType<typeof api.multiRunner> {
	return new Promise((resolve) => {
		api.multiRunner({ ...GeneratorEmptyResults, answers }, loaded, props).then(resolve);
	});
}

export function save<D>(into: string, results: GeneratorResults<D>): ReturnType<typeof api.saver> {
	return new Promise((resolve) => {
		api.saver(into, results).then(resolve);
	});
}
