import { CommandResult, DependenciesGraph, DoneType } from "../../";

export enum ServantUnifyProperty {
	Type = "type",
	Unified = "unified",
}

export enum ServantUnifyType {
	Provided = "provided",
	Latest = "latest",
}

export interface UnifyResult {
	module: string;
	unified: UnifyPackageResult[];
	type: DoneType;
	err: Error | null;
}

export interface UnifyResults {
	modules: CommandResult<UnifyResult>[];
	errors: Error[];
}

export interface UnifyPackageResult {
	package: string;
	version: string;
	type: DoneType;
}

export interface PrepareUnifyResults {
	graph: DependenciesGraph;
}

export interface UnifyCustomData {
	entry: string;
	modules: {
		[key: string]: Array<string>;
	};
	packageJsons: Array<{
		relativePath: string;
		module: string;
	}>;
	results: UnifyResult[];
}

export interface UnifyParams {
	unified: UnifiedVersionsInfo;
	type: ServantUnifyType;
}

export type UnifiedVersionsInfo = {
	[key: string]: string;
};
