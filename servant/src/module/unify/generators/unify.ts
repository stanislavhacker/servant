import { PackageJson } from "@servant/servant-data";
import {
	FnGetAnswer,
	GeneratorDataHandlers,
	GeneratorFile,
	GeneratorFnHandlers,
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorNextHandler,
	GeneratorQuestions,
} from "@servant/servant-generators";

import { UnifyCustomData, UnifyPackageResult, UnifyResult } from "../types";
import { getUnifyId } from "../utils";
import { DoneType } from "../../index";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-unify-select-generator",
	description:
		"This is generator that is used to select single version for module with unify command.",
	path: "internal://servant/servant-unify-select-generator",
	entry: "bundled",
	use: [],
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [];
};

export const generator = <D>(
	{ getAnswer }: GeneratorDataHandlers,
	{ getFile, updateFile, removeFile, getCustomData, updateCustomData }: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	const { modules, packageJsons } = getCustomData<UnifyCustomData>();

	const results = packageJsons.map(({ relativePath, module }) => {
		const packageJson = getFile(relativePath)?.content as unknown as PackageJson.PackageJson;
		const result = updatePackageJson(module, packageJson, modules, getAnswer);

		const unified = result.unified.filter((u) => u.type === DoneType.OK);
		const file =
			unified.length === 0
				? removeFile(relativePath)
				: updateFile(relativePath, packageJson as unknown as Record<string, unknown>);

		result.err = getError(result, file);
		return result;
	});

	updateCustomData<Partial<UnifyCustomData>>({
		results,
	});

	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: {},
	},
};

//utils

function updatePackageJson(
	module: string,
	packageJson: PackageJson.PackageJson,
	unified: UnifyCustomData["modules"],
	getAnswer: FnGetAnswer
): UnifyResult {
	const packageResults = [
		...updateVersions(
			Object.keys(unified),
			[
				packageJson.dependencies,
				packageJson.devDependencies,
				packageJson.optionalDependencies,
				packageJson.peerDependencies,
			],
			getAnswer
		),
	];

	return createUnifyResult(module, packageResults, getDoneType(packageResults), null);
}

function updateVersions(
	externals: string[],
	where: Array<PackageJson.Dependencies | undefined | null> = [],
	getAnswer: FnGetAnswer
): Array<UnifyPackageResult> {
	return where
		.reduce(
			(prev, dependencies) => [
				...prev,
				...(dependencies
					? Object.keys(dependencies).map((module) => {
							const version = getAnswer(getUnifyId(module));

							//replace dependencies, version is selected
							if (version && dependencies[module] !== version.value) {
								dependencies[module] = version.value;
								return createUnifyPackageResult(module, version.value, DoneType.OK);
							}
							//not selected, error
							if (!version && externals.indexOf(module) >= 0) {
								return createUnifyPackageResult(module, "", DoneType.FAIL);
							}
							//noting to do
							return null;
					  })
					: []),
			],
			[]
		)
		.filter(Boolean) as Array<UnifyPackageResult>;
}

function getError(result: UnifyResult, file: GeneratorFile): Error | null {
	if (result.type === DoneType.FAIL) {
		const packages = result.unified
			.filter((res) => res.type === DoneType.FAIL)
			.map((res) => res.package);

		return new Error(
			`Module "${result.module}" has not been unified. Packages "${packages.join(
				", "
			)}" can not not been unified due to missing unified version.`
		);
	}
	if (file.err) {
		return new Error(
			`Module "${result.module}" has not been unified. File ${file.relativePath} has error: "${file.err}"`
		);
	}
	return null;
}

function getDoneType(unified: UnifyPackageResult[]) {
	if (unified.every((u) => u.type === DoneType.OK)) {
		return DoneType.OK;
	}
	return DoneType.FAIL;
}

function createUnifyPackageResult(
	pckage: string,
	version: string,
	type: DoneType
): UnifyPackageResult {
	return {
		type,
		version,
		package: pckage,
	};
}

function createUnifyResult(
	module: string,
	unified: UnifyPackageResult[],
	type: DoneType,
	err: Error | null
): UnifyResult {
	return {
		type,
		module,
		unified,
		err,
	};
}
