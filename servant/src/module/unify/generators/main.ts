import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorNextHandler,
	GeneratorQuestions,
} from "@servant/servant-generators";

import { DoneType } from "../../index";
import { UnifyCustomData } from "../types";

import { loaded as moduleLoaded, manifest as moduleManifest } from "./module";
import { loaded as unifyLoaded, manifest as unifyManifest } from "./unify";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-unify-generator",
	description: "This is generator that is used to unify versions of modules with unify command.",
	path: "internal://servant/servant-unify-generator",
	entry: "bundled",
	use: [moduleManifest.name, unifyManifest.name],
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [];
};

export const generator = <D>(
	data: GeneratorDataHandlers,
	{ getCustomData, createMessage }: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>
) => {
	const { results } = getCustomData<UnifyCustomData>();

	const changes = results.reduce((acc, result) => acc - result.unified.length, 0);
	const modules = results.reduce((data, result) => {
		result.unified.forEach((unified) => {
			const module = (data[unified.package] = data[unified.package] || {
				version: unified.version,
				ok: [],
				fail: [],
			});

			if (unified.type === DoneType.OK) {
				module.ok.push(result.module);
			}
			if (unified.type === DoneType.OK) {
				module.fail.push(result.module);
			}
		});
		return data;
	}, {} as Record<string, { version: string; ok: string[]; fail: string[] }>);

	//unified versions
	Object.keys(modules).forEach((module) => {
		const { version, ok, fail } = modules[module];

		if (ok.length > 0) {
			createMessage(`${module}-unified`, {
				type: "success",
				title: `Package "${module}"`,
				text: `was unified to version "${version}" in modules: ${ok.join(", ")}`,
			});
		}
		if (fail.length > 0) {
			createMessage(`${module}-unified`, {
				type: "error",
				title: `Package "${module}"`,
				text: `was not unified in modules: ${fail.join(", ")}`,
			});
		}
	});

	//no changes
	if (changes === 0) {
		createMessage(`no_changes`, {
			type: "warning",
			title: `No changes`,
			text: `was made into package jsons. That mean that all packages are already unified.`,
		});
	}

	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [moduleLoaded, unifyLoaded],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: {},
	},
};
