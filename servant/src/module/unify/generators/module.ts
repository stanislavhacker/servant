import { PackageJson } from "@servant/servant-data";
import {
	GeneratorDataHandlers,
	GeneratorFnHandlers,
	GeneratorLoaded,
	GeneratorManifest,
	GeneratorMessage,
	GeneratorNextHandler,
	GeneratorQuestions,
} from "@servant/servant-generators";
import * as semver from "semver";
import * as path from "path";
import * as fs from "fs";

import {
	iterateSorted,
	validateDependencies,
	validateVersions,
	DependenciesGraph,
	ModuleDefinition,
} from "../../../module";
import {
	ServantUnifyProperty,
	ServantUnifyType,
	UnifyCustomData,
	PrepareUnifyResults,
} from "../types";
import { getUnifyId, getValueOrDefault } from "../utils";

import { manifest as unifyManifest } from "./unify";

export const manifest: GeneratorManifest = {
	engine: "servant-generator",
	name: "servant-unify-type-generator",
	description: "This is generator that is used to select unify type with unify command.",
	path: "internal://servant/servant-unify-type-generator",
	entry: "bundled",
	use: [],
};

export const messages: () => GeneratorMessage[] = () => {
	return [];
};
export const questions: () => GeneratorQuestions = () => {
	return [
		//default
		{
			type: "select",
			question: "What you want to do now?",
			id: ServantUnifyProperty.Type,
			values: [
				{ label: "Update to latest versions", value: () => ServantUnifyType.Latest },
				{ label: "Select version manually", value: () => ServantUnifyType.Provided },
			],
		},
	];
};

export const generator = <D>(
	{ getAnswer, addQuestion, addAnswer }: GeneratorDataHandlers,
	{ createFile, updateCustomData }: GeneratorFnHandlers,
	next: GeneratorNextHandler<D>,
	props: PrepareUnifyResults
) => {
	const entry = findEntry(props.graph);
	const type = getValueOrDefault(getAnswer, ServantUnifyProperty.Type, ServantUnifyType.Latest);
	const versionsInfo = loadVersions(props.graph);

	const modules: UnifyCustomData["modules"] = {};
	Object.keys(versionsInfo.versions).map((module) => {
		const versions = versionsInfo.versions[module];

		addQuestion(
			{
				type: "select",
				question: `What version want you use for package "${module}"`,
				tip: `Latest version used in project is "${versions[0]}"`,
				id: getUnifyId(module),
				values: versions.map((ver) => ({ label: ver, value: () => ver })),
				defaultValue: { label: versions[0], value: () => versions[0] },
			},
			unifyManifest.name
		);

		if (type === ServantUnifyType.Latest) {
			addAnswer({ id: getUnifyId(module), value: versions[0] });
		}

		modules[module] = versions;
	});

	const packageJsons: UnifyCustomData["packageJsons"] = [];
	versionsInfo.packageJsons.forEach(({ packageJson, module }) => {
		const relativePath = getRelativePath(entry, packageJson.path);
		const data = JSON.parse(fs.readFileSync(packageJson.path, "utf8"));
		createFile(relativePath, data as unknown as Record<string, unknown>);
		packageJsons.push({ relativePath, module });
	});

	updateCustomData({ entry, modules, packageJsons });

	next("success");
};

export const loaded: GeneratorLoaded<unknown, unknown> = {
	use: [],
	manifest,
	generator,
	config: {
		messages: messages(),
		questions: questions(),
		answers: [],
		categories: {},
	},
};

//helper functions

type VersionsInfo = {
	versions: {
		[key: string]: Array<string>;
	};
	packageJsons: {
		module: string;
		packageJson: PackageJson.PackageJsonInfo;
	}[];
};

function loadVersions(graph: DependenciesGraph): VersionsInfo {
	const validations = validateDependencies(graph);
	const versions = validateVersions(validations);

	const externals = versions.externalInvalidVersions;
	const versionsInfo: VersionsInfo = {
		packageJsons: [],
		versions: {},
	};

	externals.forEach((module) => {
		versionsInfo.versions[module] = validations.external[module].sort(semver.compare).reverse();
	});

	versionsInfo.packageJsons = iterateSorted(graph.sorted)
		.map((module) => ({ module, packageJson: graph.modules[module].module?.packageJson }))
		.filter(({ packageJson }) => Boolean(packageJson)) as VersionsInfo["packageJsons"];

	return versionsInfo;
}

function getRelativePath(entry: string, filepath: string): string {
	return path.relative(entry, filepath);
}

function findEntry(graph: DependenciesGraph): string {
	const paths = Object.values(graph.modules)
		.map((module: ModuleDefinition) => {
			if (module.module && module.module.internal) {
				return path.normalize(module.module.packageJson.cwd).split(path.sep);
			}
			return null;
		})
		.filter(Boolean) as Array<string[]>;
	const maxDept = Math.min(...paths.map((path) => path.length));

	const common: string[] = [];
	for (let i = 0; i < maxDept; i++) {
		const current = paths[0][i];
		if (paths.every((path) => path[i] === current)) {
			common.push(current);
		}
	}
	return common.join(path.sep);
}
