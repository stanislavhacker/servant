const ServantDataErrors = [
	"No module declaration found. Invalid data provided or is probably error in Servant.",
	"Servant package json is not defined in command() call.",
	"Servant dependencies graph is not defined in command() call.",
	"Can not start watcher because initData are wrong. Package json or Servant json is empty.",
	"Servant json is not defined in InitData. This is probably error in Servant.",
	"Servant json is not specified and because of that report can not be created.",
] as const;

export function invariant(
	// eslint-disable-next-line @typescript-eslint/ban-types
	condition: boolean | string | null | undefined | number | Object,
	message: (typeof ServantDataErrors)[number]
): asserts condition {
	if (!condition) {
		throw new Error(message);
	}
}
