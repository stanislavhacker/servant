import { PackageJson, ServantJson } from "@servant/servant-data";
import { Path } from "@servant/servant-files";
import * as path from "path";

import * as Module from "./module";
import * as Commands from "./commands";
import * as Watcher from "./watcher";
import { resolveModule } from "./resolver";
import { InitData, ModulesData, ModulesParams } from "./types";
import { invariant } from "./invariant";

export function init(entry: string): Promise<InitData> {
	return new Promise((fulfill, reject) => {
		loadDirectoryData(entry).then(fulfill).catch(reject);
	});
}

export function modules(initData: InitData, params: ModulesParams = {}): Promise<ModulesData> {
	return new Promise((fulfill, reject) => {
		invariant(
			initData.servantJson,
			"Servant json is not defined in InitData. This is probably error in Servant."
		);

		const servantJson = initData.servantJson;

		Module.loadsModules(servantJson, params.init)
			.then((modulesData) =>
				Module.dependencies(
					servantJson,
					modulesData,
					params.only || [],
					params.changed || [],
					params.dependencies || false
				)
			)
			.then((graph) => {
				//OK
				fulfill({
					graph,
					validation: Module.validateDependencies(graph),
				});
			})
			.catch(reject);
	});
}

export function command<C extends Commands.Commands>(
	initData: InitData,
	modules: ModulesData,
	command: C,
	params: Commands.CommandParams
): Promise<Commands.CommandResults[C]> {
	return Commands.command(initData, modules, command, params);
}

export function create(
	packageJson: PackageJson.PackageJsonInfo,
	entry: string,
	initParams: Module.InitParams
): Promise<Commands.CommandResult<Module.InitResults>> {
	return Commands.initialize(packageJson, entry, initParams);
}

export function watch(
	initData: InitData,
	changed: Watcher.ChangeHandler,
	{ transpile = true }: Watcher.WatcherSettings = {}
): Promise<void> {
	return new Promise((_, reject) => {
		modules(initData)
			.then((modules: ModulesData) =>
				Watcher.watch(initData, modules, changed, { transpile })
			)
			.catch(reject);
	});
}

export function loadLibrary(
	servantJson: ServantJson.ServantJsonInfo,
	name: string
): { path: string; err: Error | null; empty: boolean } | null {
	const lib = ServantJson.library(servantJson, name);

	if (lib === "") {
		return {
			...resolveModule(servantJson.cwd, name, lib, true),
			empty: true,
		};
	}
	if (lib !== null) {
		return {
			...resolveModule(servantJson.cwd, name, lib, true),
			empty: false,
		};
	}
	return null;
}

function loadDirectoryData(entry: string): Promise<{
	entry: string;
	packageJson: PackageJson.PackageJsonInfo;
	servantJson: ServantJson.ServantJsonInfo;
	module?: string;
}> {
	return new Promise((fulfill, reject) => {
		const items = [
			loadParentServantServantJson(entry),
			loadServantPackageJson(),
			loadServantServantJson(entry),
		] as const;

		Promise.all(items)
			.then(([parent, packageJson, servantJson]) => {
				//parent
				if (parent) {
					fulfill({ ...parent, packageJson });
					return;
				}
				//no parent
				fulfill({ packageJson, servantJson, entry });
			})
			.catch(reject);
	});
}

function loadServantPackageJson(): Promise<PackageJson.PackageJsonInfo> {
	const directory = loadServantDirectory();
	const packageJson = path.join(directory, PackageJson.PACKAGE_JSON);
	return PackageJson.load(packageJson, true);
}

function loadServantServantJson(entry: string): Promise<ServantJson.ServantJsonInfo> {
	const packageJson = path.join(entry, PackageJson.PACKAGE_JSON);
	const servantJson = path.join(entry, ServantJson.SERVANT_JSON);

	return PackageJson.load(packageJson, true).then((packageJsonInfo) =>
		ServantJson.load(servantJson, packageJsonInfo)
	);
}

function loadParentServantServantJson(entry: string): ReturnType<typeof loadSubmoduleInfo> {
	return new Promise((fulfill) => {
		const parentEntry = path.join(entry, "../");
		loadServantServantJson(parentEntry)
			.then((parentServantJsonInfo) =>
				loadSubmoduleInfo(parentServantJsonInfo, entry).then(fulfill)
			)
			.catch(() => fulfill(undefined));
	});
}

function loadSubmoduleInfo(
	servantInfo: ServantJson.ServantJsonInfo,
	entry: string
): Promise<
	| {
			servantJson: ServantJson.ServantJsonInfo;
			entry: string;
			module: string;
	  }
	| undefined
> {
	return new Promise((fulfill) => {
		const modules = servantInfo.content.modules || [];
		const module = Path.join(entry, [PackageJson.PACKAGE_JSON])[0];
		const what = modules.map((module) => Path.patterns.all(Path.patterns.everywhere(module)));
		const isSubmodule = Path.matchedOne(module, what);

		//is submodule
		if (isSubmodule) {
			PackageJson.load(module, false)
				.then((packageJson) => {
					fulfill({
						entry: servantInfo.cwd,
						servantJson: servantInfo,
						module: packageJson.content.name,
					});
				})
				.catch(() => fulfill(undefined));
			return;
		}
		//no submodule
		fulfill(undefined);
	});
}

const name = "servant";

function loadServantDirectory() {
	let pth = __dirname;

	while (pth && path.basename(pth) !== name) {
		pth = path.dirname(pth);
	}

	return pth;
}
