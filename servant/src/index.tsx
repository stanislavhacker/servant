import {
	PackageJson,
	ServantJson,
	TsConfig,
	WebpackConfig,
	EslintrcJson,
	PrettierrcJson,
	GitIgnore,
} from "@servant/servant-data";
import * as Files from "@servant/servant-files";

import * as Module from "./module";
import * as Commands from "./commands";
import * as Changes from "./vcs";
import * as Reports from "./reports";
import * as Watcher from "./watcher";

import { InitData, ModulesData, ModulesParams, ServantApi } from "./types";
import { command, create, init, modules, watch, loadLibrary } from "./main";
import { initGenerator, unifyGenerator } from "./module";
import { resolveEntries, EntriesOnly, ResolvedEntries } from "./entries";

export {
	//namespaces
	Files,
	Module,
	Commands,
	Changes,
	Reports,
	Watcher,
	//files handlers
	PackageJson,
	ServantJson,
	TsConfig,
	WebpackConfig,
	EslintrcJson,
	PrettierrcJson,
	GitIgnore,
	//types
	ServantApi,
	InitData,
	ModulesParams,
	ModulesData,
	//api
	command,
	create,
	init,
	modules,
	watch,
	loadLibrary,
	//entries
	resolveEntries,
	EntriesOnly,
	ResolvedEntries,
};

export const generators = {
	initGenerator,
	unifyGenerator,
};

export function Servant(): ServantApi {
	return {
		init: (entry) => init(entry),
		create: (packageJson, entry, initParams) => create(packageJson, entry, initParams),
		modules: (initData, params) => modules(initData, params),
		command: (initData, modules, cmd, params) => command(initData, modules, cmd, params),
		validate: (validate) => Module.validateVersions(validate),
		watch: (initData, changed) => watch(initData, changed),
		reports: {
			issues: {
				outputs: Reports.issues.outputs,
				run: (initData, issues) => Reports.issues.run(initData.servantJson, issues),
			},
			analyze: {
				outputs: Reports.analyze.outputs,
				run: (initData, analyze) => Reports.analyze.run(initData.servantJson, analyze),
			},
		},
	};
}
