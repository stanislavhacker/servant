import { PackageJson, Modules, ServantJson } from "@servant/servant-data";
import { SASS, CSS } from "@servant/servant-files";

import * as sassNode from "sass";
import * as path from "path";
import * as fs from "fs";
import { invariant } from "../invariant";
import { createTime } from "../utils";

export interface SassFileResult {
	source: string;
	destination: string;
	status: SassState;
	error: Error | null;
	time: string;
}
export interface SassResult {
	files: Array<SassFileResult>;
	errors: Array<SassFileResult>;
	status: SassState;
	time: string;
}

export enum SassState {
	OK = "ok",
	Error = "error",
}

export function filterSass(files: Array<string>): Array<string> {
	return SASS.only(files);
}

export function sassEntry(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition
): ServantJson.EntryFile | null {
	const entries = ServantJson.entries(module.module?.servantJson);

	return entries.sass;
}

export function sass(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	sass: Array<string>
): Promise<SassResult> {
	return new Promise((fulfill, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const start = process.hrtime();
		const result = createSassResult();
		const directory = module.module.packageJson.cwd;

		if (sass.length === 0) {
			result.time = createTime(start);
			fulfill(result);
			return;
		}

		const items: Array<Promise<SassFileResult>> = [];
		sass.forEach((file) => {
			items.push(buildSass(packageJson, directory, file));
		});

		//after done
		Promise.all(items)
			.then((results) => {
				result.files = results.filter((res) => res.status === SassState.OK);
				result.errors = results.filter((res) => res.status === SassState.Error);
				result.status = result.errors.length === 0 ? SassState.OK : SassState.Error;
				result.time = createTime(start);
				fulfill(result);
			})
			.catch(reject);
	});
}

function buildSass(
	packageJson: PackageJson.PackageJsonInfo,
	directory: string,
	file: string
): Promise<SassFileResult> {
	return new Promise((fulfill) => {
		const start = process.hrtime();

		const sass = file;
		const dirname = path.dirname(sass);
		const css = CSS.toCSS(file);

		const lessResult = createSassFileResult(
			path.relative(directory, sass),
			path.relative(directory, css)
		);

		//clone options
		const options = {} as sassNode.StringOptions<"sync">;

		options.loadPaths = [dirname];
		options.url = new URL(sass);
		//options.sourceMap = true;

		renderSass(sass, options).then((result) => {
			lessResult.status = result.error ? SassState.Error : SassState.OK;
			lessResult.error = result.error;
			lessResult.time = createTime(start);
			fulfill(lessResult);
		});
	});
}

export interface SassRenderResult {
	sass: string;
	error: Error | null;
	css: string;
	imports: Array<string>;
}

function renderSass(
	path: string,
	options: sassNode.StringOptions<"sync">
): Promise<SassRenderResult> {
	return new Promise((resolve) => {
		const results = createSassRenderResult(path);

		const error = (err) => {
			results.error = err;
			resolve(results);
		};

		//read sass file
		readSass(path)
			.then((data) => {
				//render sass
				const result = sassNode.compileString(removeBOM(data), options);

				results.css = result.css;
				results.imports = result.loadedUrls.map((u) => u.toString());

				//write created css
				writeCss(CSS.toCSS(path), result.css)
					.then(() => resolve(results))
					.catch(error);
			})
			.catch(error);
	});
}

function createSassRenderResult(sass: string): SassRenderResult {
	return {
		sass: sass,
		error: null,
		css: "",
		imports: [],
	};
}

function createSassResult(): SassResult {
	return {
		errors: [],
		files: [],
		status: SassState.OK,
		time: "0s 0ms",
	};
}

function createSassFileResult(source: string, destination: string): SassFileResult {
	return {
		error: null,
		source: source,
		destination: destination,
		status: SassState.Error,
		time: "0s 0ms",
	};
}

function readSass(path: string): Promise<string> {
	return new Promise((resolve, reject) => {
		fs.readFile(path, (err, buffer) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(buffer.toString());
		});
	});
}

function writeCss(pth: string, data: string): Promise<void> {
	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(pth), { recursive: true }, (errDir) => {
			fs.writeFile(pth, data, (err) => {
				if (errDir || err) {
					reject(errDir || err);
					return;
				}
				resolve();
			});
		});
	});
}

function removeBOM(data: string | Buffer): string {
	//remove bom
	return data.toString().replace(/^\uFEFF/, "");
}
