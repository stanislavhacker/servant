import { PackageJson, Modules, ServantJson } from "@servant/servant-data";
import { LESS, CSS } from "@servant/servant-files";

import * as lessNode from "less";
import * as path from "path";
import * as fs from "fs";
import { invariant } from "../invariant";
import { createTime } from "../utils";

export interface LessFileResult {
	source: string;
	destination: string;
	status: LessState;
	error: Error | null;
	time: string;
}
export interface LessResult {
	files: Array<LessFileResult>;
	errors: Array<LessFileResult>;
	status: LessState;
	time: string;
}

export enum LessState {
	OK = "ok",
	Error = "error",
}

export function filterLess(files: Array<string>): Array<string> {
	return LESS.only(files);
}

export function lessEntry(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition
): ServantJson.EntryFile | null {
	const entries = ServantJson.entries(module.module?.servantJson);

	return entries.less;
}

export function less(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	less: Array<string>
): Promise<LessResult> {
	return new Promise((fulfill, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const start = process.hrtime();
		const result = createLessResult();
		const directory = module.module.packageJson.cwd;

		if (less.length === 0) {
			result.time = createTime(start);
			fulfill(result);
			return;
		}

		const items: Array<Promise<LessFileResult>> = [];
		less.forEach((file) => {
			items.push(buildLess(packageJson, directory, file));
		});

		//after done
		Promise.all(items)
			.then((results) => {
				result.files = results.filter((res) => res.status === LessState.OK);
				result.errors = results.filter((res) => res.status === LessState.Error);
				result.status = result.errors.length === 0 ? LessState.OK : LessState.Error;
				result.time = createTime(start);
				fulfill(result);
			})
			.catch(reject);
	});
}

function buildLess(
	packageJson: PackageJson.PackageJsonInfo,
	directory: string,
	file: string
): Promise<LessFileResult> {
	return new Promise((fulfill) => {
		const start = process.hrtime();

		const less = file;
		const dirname = path.dirname(less);
		const css = CSS.toCSS(file);

		const lessResult = createLessFileResult(
			path.relative(directory, less),
			path.relative(directory, css)
		);

		//clone options
		const options = { ...lessNode.options } as Less.Options;

		options.paths = [dirname, ...(options.paths || [])];
		options.filename = less;
		//options.sourceMap = {};

		renderLess(less, options).then((result) => {
			lessResult.status = result.error ? LessState.Error : LessState.OK;
			lessResult.error = result.error;
			lessResult.time = createTime(start);
			fulfill(lessResult);
		});
	});
}

export interface LessRenderResult {
	less: string;
	error: Error | null;
	css: string;
	imports: Array<string>;
}

function renderLess(path: string, options: Less.Options): Promise<LessRenderResult> {
	return new Promise((resolve) => {
		const results = createLessRenderResult(path);

		const error = (err) => {
			results.error = err;
			resolve(results);
		};

		//read less file
		readLess(path)
			.then((data) => {
				//render less
				lessNode
					.render(removeBOM(data), options)
					.then((result) => {
						results.css = result.css;
						results.imports = result.imports;

						//write created css
						writeCss(CSS.toCSS(path), result.css)
							.then(() => resolve(results))
							.catch(error);
					})
					.catch(error);
			})
			.catch(error);
	});
}

function createLessRenderResult(less: string): LessRenderResult {
	return {
		less: less,
		error: null,
		css: "",
		imports: [],
	};
}

function createLessResult(): LessResult {
	return {
		errors: [],
		files: [],
		status: LessState.OK,
		time: "0s 0ms",
	};
}

function createLessFileResult(source: string, destination: string): LessFileResult {
	return {
		error: null,
		source: source,
		destination: destination,
		status: LessState.Error,
		time: "0s 0ms",
	};
}

function readLess(path: string): Promise<string> {
	return new Promise((resolve, reject) => {
		fs.readFile(path, (err, buffer) => {
			if (err) {
				reject(err);
				return;
			}
			resolve(buffer.toString());
		});
	});
}

function writeCss(pth: string, data: string): Promise<void> {
	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(pth), { recursive: true }, (errDir) => {
			fs.writeFile(pth, data, (err) => {
				if (errDir || err) {
					reject(errDir || err);
					return;
				}
				resolve();
			});
		});
	});
}

function removeBOM(data: string | Buffer): string {
	//remove bom
	return data.toString().replace(/^\uFEFF/, "");
}
