import { PackageJson, WebpackJson, Modules, TsConfig } from "@servant/servant-data";
import { TS, Extensions } from "@servant/servant-files";

import * as ts from "typescript";
import * as path from "path";
import * as fs from "fs";
import { fork } from "child_process";

import { resolveModulePath } from "../resolver";
import { invariant } from "../invariant";
import { createTime } from "../utils";

const webpackBuild = resolveModulePath("@servant/servant-build-webpack").path;

export interface TypescriptResult {
	status: TypescriptState;
	time: string;
	errors: Array<Error>;
	files: Array<string>;
	css: Array<string>;
	declarations: Array<string>;
	transpilePatterns: Array<string>;
	output: string;
}
export interface DeclarationResult {
	ok: boolean;
	from: string;
	to: string;
}

export enum TypescriptState {
	OK = "ok",
	Error = "error",
}

export interface CollectData {
	src: Array<string>;
	tests: Array<string>;
}

export function filterTypescript(files: Array<string>): Array<string> {
	return TS.only(files);
}

export function typescript(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	typescripts: Array<string>,
	stylesheets: Array<string>,
	prod: boolean,
	trans: boolean
): Promise<TypescriptResult> {
	return new Promise((fulfill, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const start = process.hrtime();
		const moduleServantJson = module.module.servantJson;
		const files = typescripts.length + stylesheets.length;

		//mark as module without entry
		if (moduleServantJson.content.entry === false || files === 0) {
			const typescriptResults = empty();
			typescriptResults.time = createTime(start);
			fulfill(typescriptResults);
			return;
		}

		const promises: Array<Promise<TypescriptResult>> = [];
		//do not make transpile and dts if there build only
		//translate and get declaration files
		!trans && promises.push(transpile(packageJson, module, typescripts));
		//build and make bundle
		promises.push(build(packageJson, module, typescripts, stylesheets, prod, trans));

		//run all
		Promise.all(promises)
			.then((results: Array<TypescriptResult>) => {
				const summaryResults = merge(results);
				summaryResults.time = createTime(start);
				//done
				fulfill(summaryResults);
			})
			.catch(reject);
	});
}

export function transpile(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	typescripts: Array<string>
): Promise<TypescriptResult> {
	return new Promise((resolve, reject) => {
		const results: Array<TypescriptResult> = [];
		let p: Promise<TypescriptResult | void> = Promise.resolve();

		p = p.then(() => {
			return translate(packageJson, module, typescripts).catch(reject);
		});
		p = p.then((result: TypescriptResult) => {
			result && results.push(result);
			return dts(packageJson, module, typescripts).catch(reject);
		});
		p.then((result: TypescriptResult) => {
			result && results.push(result);
			resolve(merge(results));
		}).catch(reject);
	});
}

function build(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	typescripts: Array<string>,
	css: Array<string>,
	production: boolean,
	trnspile: boolean
): Promise<TypescriptResult> {
	return new Promise((fulfill, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const typescriptProcess = fork(webpackBuild, []);
		const typescriptResults = createTypescriptResult(typescripts, [], [], css);

		const cwd = packageJson.cwd;
		const moduleServantJson = module.module.servantJson;

		const entry = moduleServantJson.cwd;
		const message = WebpackJson.create(cwd, entry);
		message.module = module.module;
		message.production = production;
		message.transpile = trnspile;

		function done() {
			fulfill(typescriptResults);
		}

		//send message
		typescriptProcess.send(message);
		typescriptProcess.on("message", (results: WebpackJson.WebpackCompile) => {
			//check if done
			typescriptProcess.kill();
			//errors
			typescriptResults.output = results.output;
			typescriptResults.errors = results.errors.map((err) => createError(err));
			typescriptResults.status =
				typescriptResults.errors.length === 0 ? TypescriptState.OK : TypescriptState.Error;
		});
		typescriptProcess.on("error", (err) => {
			reject(err);
		});
		typescriptProcess.on("exit", done);
	});
}

export function translate(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	typescripts: Array<string>
): Promise<TypescriptResult> {
	return new Promise((resolve) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const start = process.hrtime();

		const moduleServantJson = module.module.servantJson;
		const configPath = path.join(moduleServantJson.cwd, TsConfig.TSCONFIG_JSON);
		const sources = typescripts.filter((file) => !TS.isDTS(file));
		const typescriptResults = createTypescriptResult([], [], sources, []);

		loadTsConfig(configPath).then((config: TsConfig.TSConfig) => {
			const program = ts.createProgram(sources, TsConfig.options(configPath, config));
			const emitResult = program.emit();

			const allDiagnostics = ts.getPreEmitDiagnostics(program).concat(emitResult.diagnostics);
			const errors = loadDiagnostics(moduleServantJson.cwd, allDiagnostics);

			typescriptResults.errors = [];
			typescriptResults.status =
				errors.length > 0 ? TypescriptState.Error : TypescriptState.OK;
			typescriptResults.time = createTime(start);
			resolve(typescriptResults);
		});
	});
}

function loadDiagnostics(cwd: string, allDiagnostics: ReadonlyArray<ts.Diagnostic>): Array<Error> {
	const err: Array<Error> = [];

	allDiagnostics.forEach((diagnostic) => {
		let msg = ts.flattenDiagnosticMessageText(diagnostic.messageText, "\n");

		if (diagnostic.file && diagnostic.start !== undefined) {
			const { line, character } = diagnostic.file.getLineAndCharacterOfPosition(
				diagnostic.start
			);
			msg = `${diagnostic.file.fileName} (${line + 1},${character + 1}): ${msg}`;
		}
		err.push(new Error(msg));
	});

	return err;
}

function loadTsConfig(configPath: string): Promise<TsConfig.TSConfig> {
	return new Promise((resolve) => {
		TsConfig.load(configPath)
			.then((config) => resolve(config))
			.catch(() => resolve(TsConfig.create()));
	});
}

export function collect(module: Modules.ModuleDefinition): CollectData {
	const moduleServantJson = module.module?.servantJson;

	return {
		src: moduleServantJson ? collectFiles(moduleServantJson.content.src) : [],
		tests: moduleServantJson ? collectFiles(moduleServantJson.content.tests) : [],
	};
}

function collectFiles(files: Array<string>): Array<string> {
	const typescripts = filterTypescript(Extensions.collectTS(files));

	return [...new Set(typescripts)];
}

//DTS

function dts(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	typescripts: Array<string>
): Promise<TypescriptResult> {
	return new Promise((resolve) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const moduleServantJson = module.module.servantJson;
		const output = moduleServantJson.content.output;
		const dist = path.join(moduleServantJson.cwd, output.directory);

		const files = [
			...new Set(
				typescripts.map((src) => {
					return TS.toDTS(src);
				})
			),
		];

		const promises: Array<Promise<unknown>> = [];
		const typescriptResults = createTypescriptResult([], files, [], []);

		//iterate files
		files.forEach((file) => {
			const from = file;
			const to = path.join(dist, path.relative(moduleServantJson.cwd, from));

			promises.push(copyDeclaration(from, to));
		});

		Promise.all(promises).then((results: Array<DeclarationResult>) => {
			const errors = results
				.filter((res) => !res.ok)
				.map((res) => {
					return new Error(`Can not copy d.ts file from "${res.from}" to "${res.to}".`);
				});

			typescriptResults.errors = errors;
			typescriptResults.status =
				errors.length === 0 ? TypescriptState.OK : TypescriptState.Error;
			resolve(typescriptResults);
		});
	});
}

function copyDeclaration(from: string, to: string): Promise<DeclarationResult> {
	return new Promise((fulfill) => {
		const directory = path.dirname(to);

		fs.mkdir(directory, { recursive: true }, (dirErr) => {
			fs.copyFile(from, to, (err) => {
				fulfill(createDeclarationResults(!(dirErr || err), from, to));
			});
		});
	});
}

function createDeclarationResults(ok: boolean, from: string, to: string): DeclarationResult {
	return {
		ok: ok,
		to: to,
		from: from,
	};
}

//RESULTS

function merge(results: Array<TypescriptResult>): TypescriptResult {
	const result = createTypescriptResult([], [], [], []);

	results.forEach((res) => {
		result.errors.push(...res.errors);
		result.files = [...new Set<string>([...result.files, ...res.files])];
		result.css = [...new Set<string>([...result.css, ...res.css])];
		result.declarations = [...new Set<string>([...result.declarations, ...res.declarations])];
		result.transpilePatterns = [
			...new Set<string>([...result.transpilePatterns, ...res.transpilePatterns]),
		];
		result.output = result.output || res.output;
	});
	result.status = result.errors.length === 0 ? TypescriptState.OK : TypescriptState.Error;
	return result;
}

function empty(): TypescriptResult {
	const typescriptResults = createTypescriptResult([], [], [], []);
	typescriptResults.errors = [];
	typescriptResults.status = TypescriptState.OK;
	return typescriptResults;
}

//RESULTS

function createTypescriptResult(
	files: Array<string>,
	declarations: Array<string>,
	transpile: Array<string>,
	css: Array<string>
): TypescriptResult {
	return {
		files: files,
		declarations: declarations,
		css: css,
		transpilePatterns: transpile,
		errors: [],
		status: TypescriptState.Error,
		time: "0s 0ms",
		output: "",
	};
}

function createError(err: WebpackJson.CompileError): Error {
	const error = new Error(err.message);

	error.name = err.name;
	error.stack = err.stack;
	return error;
}
