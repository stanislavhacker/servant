import { PackageJson, Modules, ServantJson } from "@servant/servant-data";
import { Path, Extensions, CSS, TS } from "@servant/servant-files";

import TypedCssModule from "typed-css-modules";
import { invariant } from "../invariant";
import { createTime } from "../utils";

export interface CssResult {
	where: string;
	status: CssState;
	time: string;
	errors: Array<Error>;
	files: Array<string>;
}

export enum CssState {
	OK = "ok",
	Error = "error",
}

export function filterCss(files: Array<string>): Array<string> {
	return CSS.only(files);
}

export function mapCss(files: Array<string>): Array<string> {
	return files.map((file) => {
		return CSS.toCSS(file);
	});
}

export function cssEntry(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition
): ServantJson.EntryFile | null {
	const entries = ServantJson.entries(module.module?.servantJson);

	return entries.css;
}

export function css(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	css: Array<string>
): Promise<CssResult> {
	return new Promise((fulfill, reject) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const start = process.hrtime();
		const servantJson = module.module.servantJson;

		const pattern = Path.join(servantJson.cwd, [Extensions.patterns.all(CSS.MAIN)])[0];
		const cssResult = createCssResult(pattern, css);

		if (css.length === 0) {
			cssResult.status = CssState.OK;
			cssResult.time = createTime(start);
			fulfill(cssResult);
			return;
		}

		const creator = new TypedCssModule({
			rootDir: packageJson.cwd,
			searchDir: "./",
		});

		const creators: Array<Promise<CssFileResult>> = [];
		css.forEach((file) => {
			creators.push(createDtsFile(creator, file));
		});

		Promise.all(creators)
			.catch(reject)
			.then((files: Array<CssFileResult>) => {
				const errors: Array<Error> = files
					.filter((item) => Boolean(item.error))
					.map((item) => {
						return item.error;
					}) as Array<Error>;

				cssResult.status = errors.length > 0 ? CssState.Error : CssState.OK;
				cssResult.errors = errors;
				cssResult.time = createTime(start);
				fulfill(cssResult);
			});
	});
}

function createCssResult(where: string, files: Array<string>): CssResult {
	return {
		errors: [],
		where: where,
		files: files,
		status: CssState.Error,
		time: "0s 0ms",
	};
}

type CssFileResult = {
	from: string;
	to: null | string;
	error: Error | null;
};

function createDtsFile(creator: TypedCssModule, file: string): Promise<CssFileResult> {
	return new Promise((resolve) => {
		const cssFileResult = createCssFileResult(file);

		creator
			.create(file, undefined, false)
			.then((content) => content.writeFile(repairExports))
			.then(() => {
				cssFileResult.to = Extensions.replace(file, TS.DECLARATION);
				resolve(cssFileResult);
			})
			.catch((err) => {
				cssFileResult.error = err;
				resolve(cssFileResult);
			});
	});
}

function repairExports(data: string) {
	//NOTE: Repair exports to have been as default export
	return data.replace("export = styles", "export default styles");
}

function createCssFileResult(file: string): CssFileResult {
	return {
		from: file,
		to: null,
		error: null,
	};
}
