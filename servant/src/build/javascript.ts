import { Modules } from "@servant/servant-data";
import { JS, Extensions } from "@servant/servant-files";

export function filterJavascript(files: Array<string>): Array<string> {
	return JS.only(files);
}

export interface CollectData {
	src: Array<string>;
	tests: Array<string>;
}

export function collect(module: Modules.ModuleDefinition): CollectData {
	const moduleServantJson = module.module?.servantJson;

	return {
		src: moduleServantJson ? collectFiles(moduleServantJson.content.src) : [],
		tests: moduleServantJson ? collectFiles(moduleServantJson.content.tests) : [],
	};
}

function collectFiles(files: Array<string>): Array<string> {
	const javascripts = filterJavascript(Extensions.collectJS(files));

	return [...new Set(javascripts)];
}
