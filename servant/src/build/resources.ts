import { PackageJson, Modules } from "@servant/servant-data";
import * as path from "path";
import * as fs from "fs";
import { invariant } from "../invariant";
import { createTime } from "../utils";

export interface ResourcesResults {
	status: ResourcesState;
	time: string;
	files: Array<ResourcesFileResults>;
	warnings: Array<ResourcesFileResults>;
}

export interface ResourcesFileResults {
	to: string;
	from: string;
	status: ResourcesState;
}

export enum ResourcesState {
	OK = "ok",
	Warning = "warning",
}

export function resources(
	packageJson: PackageJson.PackageJsonInfo,
	module: Modules.ModuleDefinition,
	resources: Array<string>
): Promise<ResourcesResults> {
	return new Promise((fulfill) => {
		invariant(
			module.module,
			"No module declaration found. Invalid data provided or is probably error in Servant."
		);

		const start = process.hrtime();

		const modulePackageJson = module.module.packageJson;
		const moduleServantJson = module.module.servantJson;
		const directory = modulePackageJson.cwd;
		const resourcesResults = createResourcesResults();

		if (resources.length === 0) {
			resourcesResults.warnings = [];
			resourcesResults.files = [];
			resourcesResults.status = ResourcesState.OK;
			resourcesResults.time = createTime(start);
			fulfill(resourcesResults);
			return;
		}

		const dist = path.join(directory, moduleServantJson.content.output.directory);
		const items: Array<Promise<ResourcesFileResults>> = [];

		resources.forEach((res) => {
			items.push(copyResource(directory, res, dist));
		});

		Promise.all(items).then((results) => {
			resourcesResults.warnings = results.filter((file) => file.status !== ResourcesState.OK);
			resourcesResults.files = results.filter((file) => file.status === ResourcesState.OK);
			resourcesResults.status =
				resourcesResults.warnings.length === 0 ? ResourcesState.OK : ResourcesState.Warning;
			resourcesResults.time = createTime(start);
			fulfill(resourcesResults);
		});
	});
}

function copyResource(
	directory: string,
	resourcePath: string,
	destinationDirectory: string
): Promise<ResourcesFileResults> {
	return new Promise((fulfill) => {
		const basename = path.basename(resourcePath);
		const destination = path.join(destinationDirectory, basename);

		fs.copyFile(resourcePath, destination, (err) => {
			const state = err ? ResourcesState.Warning : ResourcesState.OK;
			const from = path.relative(directory, resourcePath);
			const to = path.relative(directory, destination);

			fulfill(createResourcesFileResults(state, from, to));
		});
	});
}

function createResourcesFileResults(
	status: ResourcesState,
	from: string,
	to: string
): ResourcesFileResults {
	return {
		status: status,
		to: to,
		from: from,
	};
}

function createResourcesResults(): ResourcesResults {
	return {
		status: ResourcesState.OK,
		time: "0s 0ms",
		files: [],
		warnings: [],
	};
}
