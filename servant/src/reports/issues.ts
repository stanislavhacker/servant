import { ServantJson } from "@servant/servant-data";
import * as fs from "fs";
import * as path from "path";
import { Issues } from "../module";
import { invariant } from "../invariant";

//issues

const issuesTxt = "issues.txt";

export const outputs = [issuesTxt];
export function run(
	servantJson: ServantJson.ServantJsonInfo | undefined,
	issues: Issues
): Promise<string[]> {
	return new Promise((resolve, reject) => {
		invariant(
			servantJson,
			"Servant json is not specified and because of that report can not be created."
		);

		const reportPath = path.join(servantJson.cwd, servantJson.content.temp, issuesTxt);

		const lines = [
			//first line
			`ID\tP\tF\tE\tP\tURL`,
			//lines
			...Object.keys(issues).map((id) => {
				const issue = issues[id];
				return `${issue.id}\t${issue.passed}\t${issue.failed}\t${issue.excluded}\t${issue.pending}\t${issue.url}`;
			}),
		];

		writeFile(reportPath, lines.join("\r\n"))
			.then((path) => resolve([path]))
			.catch(reject);
	});
}

function writeFile(filepath: string, data: string): Promise<string> {
	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(filepath), { recursive: true }, (dirErr) => {
			fs.writeFile(filepath, data, (err) => {
				dirErr || err ? reject(dirErr || err) : resolve(filepath);
			});
		});
	});
}
