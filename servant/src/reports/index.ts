import * as issues from "./issues";
import * as analyze from "./analyze";

export type Reporter = "issues" | "analyze";

export { issues, analyze };

export function parseReport(report: string | Array<string>): Array<Reporter> {
	const reports = extractReports(report);

	return reports.filter((item) => ["issues", "analyze"].includes(item)) as Array<Reporter>;
}

function extractReports(report): Array<string> {
	//is string
	if (typeof report === "string") {
		return extractFromString(report);
	}
	//is array
	if (Array.isArray(report)) {
		return extractFromArray(report);
	}
	//empty
	return [];
}

function extractFromString(report: string): Array<string> {
	return report.split(",").map((item) => item.toLowerCase().trim());
}

function extractFromArray(report: string[]): Array<string> {
	return report.reduce((prev, item) => [...prev, ...extractFromString(item)], []);
}
