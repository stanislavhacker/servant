import { ServantJson, Modules } from "@servant/servant-data";
import * as fs from "fs";
import * as path from "path";
import { Builder } from "xml2js";

import { invariant } from "../invariant";
import { Commands } from "../index";

//issues

const analyzeJson = "analyze.json";
const analyzeXml = "analyze.xml";

type AnalyzeFile = {
	dependencies: {
		all: {
			name: string;
			versions: string[];
		}[];
	};
	missing: string[];
};

export const outputs = [analyzeJson, analyzeXml];
export function run(
	servantJson: ServantJson.ServantJsonInfo | undefined,
	analyze: Commands.AnalyzeSummaryResult
): Promise<string[]> {
	return new Promise((resolve, reject) => {
		invariant(
			servantJson,
			"Servant json is not specified and because of that report can not be created."
		);

		const jsonPath = path.join(servantJson.cwd, servantJson.content.temp, analyzeJson);
		const xmlPath = path.join(servantJson.cwd, servantJson.content.temp, analyzeXml);

		const data: AnalyzeFile = {
			dependencies: {
				all: [],
			},
			missing: [],
		};

		//deps
		analyze.sorted.forEach((module) => {
			const def = analyze.modules[module];
			def.dependencies.forEach((dep) => {
				//do not process internal dependency
				if (dep.type.includes(Modules.DependencyType.Internal)) {
					return;
				}
				const exists = data.dependencies.all.find((item) => item.name === dep.name);
				if (exists) {
					exists.versions = [...new Set([...exists.versions, ...dep.versions.ranges])];
				} else {
					data.dependencies.all.push({
						name: dep.name,
						versions: dep.versions.ranges,
					});
				}
			});
		});
		//missing
		data.missing = analyze.analyze.missing;

		const json = JSON.stringify(data, null, 2);
		const xml = new Builder().buildObject(data);
		Promise.all([writeFile(jsonPath, json), writeFile(xmlPath, xml)])
			.then((paths) => resolve(paths))
			.catch(reject);
	});
}

function writeFile(filepath: string, data: string): Promise<string> {
	return new Promise((resolve, reject) => {
		fs.mkdir(path.dirname(filepath), { recursive: true }, (dirErr) => {
			fs.writeFile(filepath, data, (err) => {
				dirErr || err ? reject(dirErr || err) : resolve(filepath);
			});
		});
	});
}
