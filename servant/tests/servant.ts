/* eslint-disable @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-explicit-any */
import "jasmine";
import {
	EslintrcJson,
	Modules,
	PackageJson,
	PrettierrcJson,
	ServantJson,
	TsConfig,
} from "@servant/servant-data";
import { Path } from "@servant/servant-files";

import { InitData, Servant } from "../src";
import {
	createProjectEmpty,
	createProjectPrepared,
	createProjectToDo,
	createProjectToDoForUnify,
} from "./projects";
import { loadPatterns } from "../src/patterns";
import {
	CommandResult,
	DoneType,
	InitResults,
	iterateSorted,
	ServantInitType,
} from "../src/module";
import * as fs from "fs";

describe("servant.api", () => {
	describe(".init()", () => {
		describe("on empty directory", () => {
			let clean;

			beforeAll(() => {
				clean = createProjectEmpty();
			});

			it("fail on empty directory", async () => {
				const servant = Servant();
				await expectAsync(servant.init("/projects/project")).toBeRejected();
			});

			afterAll(() => {
				clean();
			});
		});

		describe("with project directory", () => {
			let clean;

			beforeAll(() => {
				clean = createProjectToDo();
			});

			it("not fail with project dir", async () => {
				const servant = Servant();
				const initData = await servant.init("/projects/project");

				expect(initData.servantJson).toBeDefined();
				expect(initData.packageJson).toBeDefined();

				expect(Path.normalize(initData.servantJson!.cwd)).toBe("/projects/project");
				expect(Path.normalize(initData.servantJson!.path)).toBe(
					"/projects/project/servant.json"
				);
				expect(initData.servantJson!.content).toBeDefined();

				expect(Path.normalize(initData.packageJson!.cwd)).toContain("servant/servant");
				expect(Path.normalize(initData.packageJson!.path)).toContain(
					"servant/servant/package.json"
				);
				expect(initData.packageJson!.content).toBeDefined();
			});

			afterAll(() => {
				clean();
			});
		});
	});

	describe(".create()", () => {
		describe("on empty directory", () => {
			let clean;
			let servantCli: PackageJson.PackageJsonInfo;

			function servantCliPackageJson(): PackageJson.PackageJsonInfo {
				const packageJson = PackageJson.create("@servant/servant-cli", "1.0.0");
				packageJson.bin = {
					servant: "./dist/servant-cli.js",
				};

				return {
					content: PackageJson.map(packageJson),
					path: "/projects/project/node_modules/@servant/servant-cli/pacakge.json",
					cwd: "/projects/project/node_modules/@servant/servant-cli",
					main: false,
				};
			}

			describe("init structure into file system", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Main,
					});
					initResults = createData.data as InitResults;
				});

				it("check results", () => {
					expect(initResults.name).toBe("");
					expect(Path.normalize(initResults.servantJson)).toBe(
						"/projects/project/servant.json"
					);
					expect(Path.normalize(initResults.packageJson)).toBe(
						"/projects/project/package.json"
					);
					expect(Path.normalize(initResults.tsconfigJson!)).toBe(
						"/projects/project/tsconfig.json"
					);
					expect(initResults.eslintrcJson).toBe(null);
					expect(initResults.paths.map((pth) => Path.normalize(pth))).toEqual([
						"/projects/project/src",
						"/projects/project/tests",
						"/projects/project/src/index.ts",
						"/projects/project/tests/index.ts",
						"/projects/project/tests/tests.ts",
						"/projects/project/src/index.css",
						"/projects/project/template.html",
						"/projects/project/assets/logo.png",
					]);
				});

				it("check servant.json", () => {
					const servantJson = JSON.parse(
						fs.readFileSync(initResults.servantJson).toString()
					) as ServantJson.ServantJson;
					expect(servantJson.package).toBe("");
					expect(servantJson.target).toBe("web");
					expect(servantJson.entry).toBe("./src/index");
					expect(servantJson.test).toBe("./tests/index");
					expect(servantJson.src).toEqual(["src/**/*"]);
					expect(servantJson.tests).toEqual(["tests/**/*"]);
					expect(servantJson.modules).toEqual(["./"]);
					expect(servantJson.output.directory).toEqual("./dist");
					expect(servantJson.output.filename).toEqual("index.js");
				});

				it("check package.json", () => {
					const packageJson = JSON.parse(
						fs.readFileSync(initResults.packageJson).toString()
					) as PackageJson.PackageJson;
					expect(packageJson.name).toBe("");
					expect(packageJson.author).toBe("");
					expect(packageJson.description).toBe("");
					expect(packageJson.files).toEqual(["dist/**/*", "README.md", "LICENSE"]);
					expect(packageJson.keywords).toEqual([]);
					expect(packageJson.license).toBe("ISC");
					expect(packageJson.main).toBe("src/index.js");
					expect(packageJson.types).toBe("src/index.d.ts");
					expect(packageJson.version).toBe("1.0.0");
					expect(packageJson.devDependencies).toEqual({
						"@servant/servant-cli": "^1.0.0",
					});
					expect(packageJson.scripts!["servant"]).toBe("servant");
					expect(Object.keys(packageJson.scripts!).length).toBe(13);
				});

				it("check tsconfig.json", () => {
					const tsconfigJson = JSON.parse(
						fs.readFileSync(initResults.tsconfigJson!).toString()
					) as TsConfig.TSConfig;
					expect(tsconfigJson.compilerOptions).toEqual({
						module: "commonjs",
						target: "es5",
						sourceMap: true,
						declaration: true,
						jsx: "react",
						skipLibCheck: true,
						strictNullChecks: true,
					});
				});

				it("check paths", () => {
					expect(initResults.paths).toEqual([
						"/projects/project/src",
						"/projects/project/tests",
						"/projects/project/src/index.ts",
						"/projects/project/tests/index.ts",
						"/projects/project/tests/tests.ts",
						"/projects/project/src/index.css",
						"/projects/project/template.html",
						"/projects/project/assets/logo.png",
					]);
				});

				it("check name", () => {
					expect(initResults.name).toEqual("");
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for ts", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Main,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.node,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
					});
					initResults = createData.data as InitResults;
				});

				it("check servant.json", () => {
					const servantJson = JSON.parse(
						fs.readFileSync(initResults.servantJson).toString()
					) as ServantJson.ServantJson;
					expect(servantJson.package).toBe("Project");
					expect(servantJson.target).toBe("node");
					expect(servantJson.entry).toBe("./sources/index");
					expect(servantJson.test).toBeUndefined();
					expect(servantJson.src).toEqual(["sources/**/*"]);
					expect(servantJson.tests).toEqual(["specs/**/*"]);
					expect(servantJson.modules).toEqual(["./", "module.*/"]);
					expect(servantJson.output.directory).toEqual("./output");
					expect(servantJson.output.filename).toEqual("out.js");
				});

				it("check package.json", () => {
					const packageJson = JSON.parse(
						fs.readFileSync(initResults.packageJson).toString()
					) as PackageJson.PackageJson;
					expect(packageJson.name).toBe("Project");
					expect(packageJson.author).toBe("Standa");
					expect(packageJson.description).toBe("Description of project");
					expect(packageJson.files).toEqual(["output/**/*", "README.md", "LICENSE"]);
					expect(packageJson.keywords).toEqual(["a", "b"]);
					expect(packageJson.license).toBe("GPL");
					expect(packageJson.main).toBe("sources/index.js");
					expect(packageJson.types).toBe("sources/index.d.ts");
					expect(packageJson.version).toBe("1.2.3");
					expect(packageJson.devDependencies).toEqual({
						"@servant/servant-cli": "^1.0.0",
					});
					expect(packageJson.scripts!["servant"]).toBe("servant");

					expect(packageJson.scripts!["install"]).toBeUndefined();
					expect(packageJson.scripts!["update"]).toBeUndefined();
					expect(packageJson.scripts!["package"]).toBeUndefined();
					expect(packageJson.scripts!["publish"]).toBeUndefined();

					expect(packageJson.scripts!["make-install"]).toBe("servant install");
					expect(packageJson.scripts!["make-update"]).toBe("servant update");
					expect(packageJson.scripts!["make-package"]).toBe(
						"servant clean build publish"
					);
					expect(packageJson.scripts!["make-publish"]).toBe(
						'servant clean build publish --production --tag "latest"'
					);
					expect(Object.keys(packageJson.scripts!).length).toBe(13);
				});

				it("check tsconfig.json", () => {
					const tsconfigJson = JSON.parse(
						fs.readFileSync(initResults.tsconfigJson!).toString()
					) as TsConfig.TSConfig;
					expect(tsconfigJson.compilerOptions).toEqual({
						module: "commonjs",
						target: "es5",
						sourceMap: true,
						declaration: true,
						jsx: "react",
						skipLibCheck: true,
						strictNullChecks: true,
					});
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for ts and with init submodule", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Submodule,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.node,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
					});
					initResults = createData.data as InitResults;
				});

				it("check servant.json", () => {
					const servantJson = JSON.parse(
						fs.readFileSync(initResults.servantJson).toString()
					) as ServantJson.ServantJson;
					expect(servantJson.package).toBe("Project");
					expect(servantJson.target).toBe("node");
					expect(servantJson.entry).toBe("./sources/index");
					expect(servantJson.test).toBeUndefined();
					expect(servantJson.src).toEqual(["sources/**/*"]);
					expect(servantJson.tests).toEqual(["specs/**/*"]);
					expect(servantJson.modules as any).toBe(undefined);
					expect(servantJson.output.directory).toEqual("./output");
					expect(servantJson.output.filename).toEqual("out.js");
				});

				it("check package.json", () => {
					const packageJson = JSON.parse(
						fs.readFileSync(initResults.packageJson).toString()
					) as PackageJson.PackageJson;
					expect(packageJson.name).toBe("Project");
					expect(packageJson.author).toBe("Standa");
					expect(packageJson.description).toBe("Description of project");
					expect(packageJson.files).toEqual(["output/**/*", "README.md", "LICENSE"]);
					expect(packageJson.keywords).toEqual(["a", "b"]);
					expect(packageJson.license).toBe("GPL");
					expect(packageJson.main).toBe("sources/index.js");
					expect(packageJson.types).toBe("sources/index.d.ts");
					expect(packageJson.version).toBe("1.2.3");
					expect(packageJson.devDependencies).toEqual(undefined);

					expect(packageJson.scripts!["clean"]).toBeDefined();
					expect(packageJson.scripts!["build"]).toBeDefined();
					expect(packageJson.scripts!["rebuild"]).toBeDefined();
					expect(packageJson.scripts!["test"]).toBeDefined();
					expect(packageJson.scripts!["retest"]).toBeDefined();
					expect(packageJson.scripts!["validate"]).toBeDefined();
				});

				it("check tsconfig.json", () => {
					const tsconfigJson = JSON.parse(
						fs.readFileSync(initResults.tsconfigJson!).toString()
					) as TsConfig.TSConfig;
					expect(tsconfigJson.compilerOptions).toEqual({
						module: "commonjs",
						target: "es5",
						sourceMap: true,
						declaration: true,
						jsx: "react",
						skipLibCheck: true,
						strictNullChecks: true,
					});
				});

				it("check paths", () => {
					const paths = initResults.paths;

					expect(paths.length).toBe(4);
					expect(paths[0]).toContain("projects/project/module.test/sources");
					expect(paths[1]).toContain("projects/project/module.test/specs");
					expect(paths[2]).toContain("projects/project/module.test/sources/index.ts");
					expect(paths[3]).toContain("projects/project/module.test/specs/tests.ts");
				});

				it("check name", () => {
					expect(initResults.name).toEqual("Project");
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for js", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Main,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.node,
						language: "js",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
					});
					initResults = createData.data as InitResults;
				});

				it("check servant.json", () => {
					const servantJson = JSON.parse(
						fs.readFileSync(initResults.servantJson).toString()
					) as ServantJson.ServantJson;
					expect(servantJson.package).toBe("Project");
					expect(servantJson.target).toBe("node");
					expect(servantJson.entry).toBe("./sources/index");
					expect(servantJson.test).toBeUndefined();
					expect(servantJson.src).toEqual(["sources/**/*"]);
					expect(servantJson.tests).toEqual(["specs/**/*"]);
				});

				it("check package.json", () => {
					const packageJson = JSON.parse(
						fs.readFileSync(initResults.packageJson).toString()
					) as PackageJson.PackageJson;
					expect(packageJson.name).toBe("Project");
					expect(packageJson.author).toBe("Standa");
					expect(packageJson.description).toBe("Description of project");
					expect(packageJson.files).toEqual(["output/**/*", "README.md", "LICENSE"]);
					expect(packageJson.keywords).toEqual(["a", "b"]);
					expect(packageJson.license).toBe("GPL");
					expect(packageJson.main).toBe("sources/index.js");
					expect(packageJson.types).toBe("sources/index.d.ts");
					expect(packageJson.version).toBe("1.2.3");
					expect(packageJson.devDependencies).toEqual({
						"@servant/servant-cli": "^1.0.0",
					});
					expect(packageJson.scripts!["servant"]).toBe("servant");

					expect(packageJson.scripts!["install"]).toBeUndefined();
					expect(packageJson.scripts!["update"]).toBeUndefined();
					expect(packageJson.scripts!["package"]).toBeUndefined();
					expect(packageJson.scripts!["publish"]).toBeUndefined();

					expect(packageJson.scripts!["make-install"]).toBe("servant install");
					expect(packageJson.scripts!["make-update"]).toBe("servant update");
					expect(packageJson.scripts!["make-package"]).toBe(
						"servant clean build publish"
					);
					expect(packageJson.scripts!["make-publish"]).toBe(
						'servant clean build publish --production --tag "latest"'
					);
					expect(Object.keys(packageJson.scripts!).length).toBe(13);
				});

				it("check tsconfig.json", () => {
					expect(initResults.tsconfigJson).toBe(null);
				});

				it("check paths", () => {
					expect(initResults.paths).toEqual([
						"/projects/project/sources",
						"/projects/project/specs",
						"/projects/project/sources/index.js",
						"/projects/project/specs/tests.js",
					]);
				});

				it("check name", () => {
					expect(initResults.name).toEqual("Project");
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for js and with init submodule", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Submodule,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.node,
						language: "js",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
					});
					initResults = createData.data as InitResults;
				});

				it("check servant.json", () => {
					const servantJson = JSON.parse(
						fs.readFileSync(initResults.servantJson).toString()
					) as ServantJson.ServantJson;
					expect(servantJson.package).toBe("Project");
					expect(servantJson.target).toBe("node");
					expect(servantJson.entry).toBe("./sources/index");
					expect(servantJson.test).toBeUndefined();
					expect(servantJson.src).toEqual(["sources/**/*"]);
					expect(servantJson.tests).toEqual(["specs/**/*"]);
				});

				it("check package.json", () => {
					const packageJson = JSON.parse(
						fs.readFileSync(initResults.packageJson).toString()
					) as PackageJson.PackageJson;
					expect(packageJson.name).toBe("Project");
					expect(packageJson.author).toBe("Standa");
					expect(packageJson.description).toBe("Description of project");
					expect(packageJson.files).toEqual(["output/**/*", "README.md", "LICENSE"]);
					expect(packageJson.keywords).toEqual(["a", "b"]);
					expect(packageJson.license).toBe("GPL");
					expect(packageJson.main).toBe("sources/index.js");
					expect(packageJson.types).toBe("sources/index.d.ts");
					expect(packageJson.version).toBe("1.2.3");
					expect(packageJson.devDependencies).toEqual(undefined);

					expect(packageJson.scripts!["clean"]).toBeDefined();
					expect(packageJson.scripts!["build"]).toBeDefined();
					expect(packageJson.scripts!["rebuild"]).toBeDefined();
					expect(packageJson.scripts!["test"]).toBeDefined();
					expect(packageJson.scripts!["retest"]).toBeDefined();
					expect(packageJson.scripts!["validate"]).toBeDefined();
				});

				it("check tsconfig.json", () => {
					expect(initResults.tsconfigJson).toBe(null);
				});

				it("check paths", () => {
					const paths = initResults.paths;

					expect(paths.length).toBe(4);
					expect(paths[0]).toContain("projects/project/module.test/sources");
					expect(paths[1]).toContain("projects/project/module.test/specs");
					expect(paths[2]).toContain("projects/project/module.test/sources/index.js");
					expect(paths[3]).toContain("projects/project/module.test/specs/tests.js");
				});

				it("check name", () => {
					expect(initResults.name).toEqual("Project");
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for ts and eslint", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Main,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.node,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
						eslintType: ".eslintrc.json",
					});
					initResults = createData.data as InitResults;
				});

				it("check .eslintrc.json", () => {
					expect(initResults.packageJsonProps.eslintConfig).toBe(false);
					const eslintrcJson = JSON.parse(
						fs.readFileSync(initResults.eslintrcJson!).toString()
					) as EslintrcJson.EslintrcJson;
					expect(eslintrcJson.root).toBe(true);
				});

				it("check package.json", () => {
					const packageJson = JSON.parse(
						fs.readFileSync(initResults.packageJson).toString()
					) as PackageJson.PackageJson;
					expect(packageJson.name).toBe("Project");
					expect(packageJson.eslintConfig).toEqual(undefined);
					expect(packageJson.devDependencies).toEqual({
						"@servant/servant-cli": "^1.0.0",
						"@typescript-eslint/eslint-plugin": jasmine.any(String),
						"@typescript-eslint/parser": jasmine.any(String),
					});
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for ts and eslint package.json property", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Main,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.node,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
						eslintType: "eslintConfig",
					});
					initResults = createData.data as InitResults;
				});

				it("check .eslintrc.json not exists", () => {
					expect(initResults.packageJsonProps.eslintConfig).toBe(true);
					expect(initResults.eslintrcJson).toBe(null);
				});

				it("check package.json", () => {
					const packageJson = JSON.parse(
						fs.readFileSync(initResults.packageJson).toString()
					) as PackageJson.PackageJson;
					expect(packageJson.name).toBe("Project");
					expect(packageJson.eslintConfig).toEqual({
						root: true,
						parser: "@typescript-eslint/parser",
						plugins: ["@typescript-eslint"],
						extends: [
							"eslint:recommended",
							"plugin:@typescript-eslint/eslint-recommended",
							"plugin:@typescript-eslint/recommended",
						],
					});
					expect(packageJson.devDependencies).toEqual({
						"@servant/servant-cli": "^1.0.0",
						"@typescript-eslint/eslint-plugin": jasmine.any(String),
						"@typescript-eslint/parser": jasmine.any(String),
					});
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for ts and prettier", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Main,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.node,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
						prettierType: ".prettierrc.json",
					});
					initResults = createData.data as InitResults;
				});

				it("check .prettierrc.json", () => {
					expect(initResults.packageJsonProps.prettier).toBe(false);
					const prettierrcJson = JSON.parse(
						fs.readFileSync(initResults.prettierrcJson!).toString()
					) as PrettierrcJson.PrettierrcJson;
					expect(prettierrcJson).toEqual({});
				});

				it("check package.json", () => {
					const packageJson = JSON.parse(
						fs.readFileSync(initResults.packageJson).toString()
					) as PackageJson.PackageJson;
					expect(packageJson.name).toBe("Project");
					expect(packageJson.prettier).toEqual(undefined);
					expect(packageJson.devDependencies).toEqual({
						"@servant/servant-cli": "^1.0.0",
					});
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for ts and prettier package.json property", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Main,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.node,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
						prettierType: "prettier",
					});
					initResults = createData.data as InitResults;
				});

				it("check .prettierrc.json not exists", () => {
					expect(initResults.packageJsonProps.prettier).toBe(true);
					expect(initResults.prettierrcJson).toBe(null);
				});

				it("check package.json", () => {
					const packageJson = JSON.parse(
						fs.readFileSync(initResults.packageJson).toString()
					) as PackageJson.PackageJson;
					expect(packageJson.name).toBe("Project");
					expect(packageJson.prettier).toEqual({});
					expect(packageJson.devDependencies).toEqual({
						"@servant/servant-cli": "^1.0.0",
					});
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for ts and css", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Submodule,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.web,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
						prettierType: "prettier",
						styles: "css",
					});
					initResults = createData.data as InitResults;
				});

				it("check results", () => {
					expect(initResults.name).toBe("Project");
					expect(Path.normalize(initResults.servantJson)).toBe(
						"/projects/project/module.test/servant.json"
					);
					expect(Path.normalize(initResults.packageJson)).toBe(
						"/projects/project/module.test/package.json"
					);
					expect(Path.normalize(initResults.tsconfigJson!)).toBe(
						"/projects/project/module.test/tsconfig.json"
					);
					expect(initResults.eslintrcJson).toBe(null);
					expect(initResults.paths.map((pth) => Path.normalize(pth))).toEqual([
						"/projects/project/module.test/sources",
						"/projects/project/module.test/specs",
						"/projects/project/module.test/sources/index.ts",
						"/projects/project/module.test/specs/index.ts",
						"/projects/project/module.test/specs/tests.ts",
						"/projects/project/module.test/sources/index.css",
						"/projects/project/module.test/template.html",
						"/projects/project/module.test/assets/logo.png",
					]);
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for ts and less", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Submodule,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.web,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
						prettierType: "prettier",
						styles: "less",
					});
					initResults = createData.data as InitResults;
				});

				it("check results", () => {
					expect(initResults.name).toBe("Project");
					expect(Path.normalize(initResults.servantJson)).toBe(
						"/projects/project/module.test/servant.json"
					);
					expect(Path.normalize(initResults.packageJson)).toBe(
						"/projects/project/module.test/package.json"
					);
					expect(Path.normalize(initResults.tsconfigJson!)).toBe(
						"/projects/project/module.test/tsconfig.json"
					);
					expect(initResults.eslintrcJson).toBe(null);
					expect(initResults.paths.map((pth) => Path.normalize(pth))).toEqual([
						"/projects/project/module.test/sources",
						"/projects/project/module.test/specs",
						"/projects/project/module.test/sources/index.ts",
						"/projects/project/module.test/specs/index.ts",
						"/projects/project/module.test/specs/tests.ts",
						"/projects/project/module.test/sources/index.less",
						"/projects/project/module.test/template.html",
						"/projects/project/module.test/assets/logo.png",
					]);
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for ts and scss", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Submodule,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.web,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
						prettierType: "prettier",
						styles: "sass",
					});
					initResults = createData.data as InitResults;
				});

				it("check results", () => {
					expect(initResults.name).toBe("Project");
					expect(Path.normalize(initResults.servantJson)).toBe(
						"/projects/project/module.test/servant.json"
					);
					expect(Path.normalize(initResults.packageJson)).toBe(
						"/projects/project/module.test/package.json"
					);
					expect(Path.normalize(initResults.tsconfigJson!)).toBe(
						"/projects/project/module.test/tsconfig.json"
					);
					expect(initResults.eslintrcJson).toBe(null);
					expect(initResults.paths.map((pth) => Path.normalize(pth))).toEqual([
						"/projects/project/module.test/sources",
						"/projects/project/module.test/specs",
						"/projects/project/module.test/sources/index.ts",
						"/projects/project/module.test/specs/index.ts",
						"/projects/project/module.test/specs/tests.ts",
						"/projects/project/module.test/sources/index.scss",
						"/projects/project/module.test/template.html",
						"/projects/project/module.test/assets/logo.png",
					]);
				});

				afterAll(() => {
					clean();
				});
			});

			describe("init structure into file system with filled params for web page target", () => {
				let initResults: InitResults;

				beforeAll(() => {
					clean = createProjectPrepared();
					servantCli = servantCliPackageJson();
				});

				beforeAll(async () => {
					const createData = await Servant().create(servantCli, "/projects/project", {
						type: ServantInitType.Submodule,
						version: "1.2.3",
						target: ServantJson.ModuleTarget.web_page,
						language: "ts",
						tests: ["specs/**/*"],
						outputDirectory: "./output",
						moduleDirectory: "module.test",
						src: ["sources/**/*"],
						outputFilename: "out.js",
						modules: ["module.*/"],
						entry: "./sources/index",
						clean: ["tmp"],
						registry: "http://avignon:10004/",
						name: "Project",
						description: "Description of project",
						keywords: ["a", "b"],
						author: "Standa",
						license: "GPL",
						styles: "less",
					});
					initResults = createData.data as InitResults;
				});

				it("check results", () => {
					expect(initResults.name).toBe("Project");
					expect(Path.normalize(initResults.servantJson)).toBe(
						"/projects/project/module.test/servant.json"
					);
					expect(Path.normalize(initResults.packageJson)).toBe(
						"/projects/project/module.test/package.json"
					);
					expect(Path.normalize(initResults.tsconfigJson!)).toBe(
						"/projects/project/module.test/tsconfig.json"
					);
					expect(initResults.eslintrcJson).toBe(null);
					expect(initResults.prettierrcJson).toBe(null);
					expect(initResults.paths.map((pth) => Path.normalize(pth))).toEqual([
						"/projects/project/module.test/sources",
						"/projects/project/module.test/specs",
						"/projects/project/module.test/sources/index.ts",
						"/projects/project/module.test/specs/index.ts",
						"/projects/project/module.test/specs/tests.ts",
						"/projects/project/module.test/sources/index.less",
						"/projects/project/module.test/template.html",
						"/projects/project/module.test/assets/logo.png",
						"/projects/project/module.test/assets/favicon.png",
					]);
				});

				it("check servant.json", () => {
					const servantJson = JSON.parse(
						fs.readFileSync(initResults.servantJson).toString()
					) as ServantJson.ServantJson;
					expect(servantJson.package).toBe("Project");
					expect(servantJson.target).toBe("web_page");
					expect(servantJson.entry).toBe("./sources/index");
					expect(servantJson.test).toBe("./specs/index");
					expect(servantJson.src).toEqual(["sources/**/*"]);
					expect(servantJson.tests).toEqual(["specs/**/*"]);
					expect(servantJson.modules as any).toBe(undefined);
					expect(servantJson.output.directory).toEqual("./output");
					expect(servantJson.output.filename).toEqual("out.js");
					expect(servantJson.webpage).toEqual({
						title: "Project",
						favicon: "./assets/favicon.png",
					});
				});

				afterAll(() => {
					clean();
				});
			});
		});
	});

	describe(".modules()", () => {
		describe("check on todo project", () => {
			let clean;
			let initData: InitData;
			const api = Servant();

			beforeAll(() => {
				clean = createProjectToDo();
			});

			beforeAll(async () => {
				initData = await api.init("/projects/project");
			});

			it("load modules data from project", async () => {
				const modules = await api.modules(initData, {});

				expect(modules.validation!.external).toEqual({});
				expect(modules.validation!.internal).toEqual({
					"todo-core": ["0.1.0"],
					"todo-view": ["0.1.0"],
					todo: ["0.1.0"],
				});
				expect(modules.graph!.sorted).toEqual([["todo-core", "todo-view", "todo"]]);
				expect(iterateSorted(modules.graph!.sorted)).toEqual([
					"todo-core",
					"todo-view",
					"todo",
				]);
				expect(modules.graph!.changes).toEqual(null);
				expect(modules.graph!.all).toEqual(["todo-core", "todo-view", "todo"]);

				const todoCore = modules.graph!.modules["todo-core"] as Modules.ModuleDefinition;
				expect(todoCore).toEqual({
					name: "todo-core",
					versions: ["0.1.0"],
					externals: [],
					internals: [],
					module: {
						dependencies: [],
						servantJson: todoCore.module!.servantJson,
						packageJson: todoCore.module!.packageJson,
						eslintrcJson: todoCore.module!.eslintrcJson,
						prettierrcJson: todoCore.module!.prettierrcJson,
						trancorderJson: todoCore.module!.trancorderJson,
						directories: {
							sources: ["src", "src/views"],
							tests: ["tests", "tests/main"],
							others: [],
						},
						internal: true,
					},
					missing: false,
					depType: [Modules.DependencyType.Internal],
				});

				const todoView = modules.graph!.modules["todo-view"];
				expect(todoView).toEqual({
					name: "todo-view",
					versions: ["0.1.0"],
					externals: [],
					internals: [],
					module: {
						dependencies: [],
						servantJson: todoView.module!.servantJson,
						packageJson: todoView.module!.packageJson,
						eslintrcJson: todoView.module!.eslintrcJson,
						prettierrcJson: todoView.module!.prettierrcJson,
						trancorderJson: todoView.module!.trancorderJson,
						directories: {
							sources: ["src", "src/views"],
							tests: ["tests", "tests/main"],
							others: [],
						},
						internal: true,
					},
					missing: false,
					depType: [Modules.DependencyType.Internal],
				});

				const todo = modules.graph!.modules["todo"];
				expect(todo).toEqual({
					name: "todo",
					versions: ["0.1.0"],
					externals: [],
					internals: [],
					module: {
						dependencies: [],
						servantJson: todo.module!.servantJson,
						packageJson: todo.module!.packageJson,
						eslintrcJson: todo.module!.eslintrcJson,
						prettierrcJson: todo.module!.prettierrcJson,
						trancorderJson: todo.module!.trancorderJson,
						directories: {
							sources: [],
							tests: [],
							others: [
								"todo_core",
								"todo_view",
								"todo_core/src",
								"todo_core/tests",
								"todo_core/src/views",
								"todo_core/tests/main",
								"todo_view/src",
								"todo_view/tests",
								"todo_view/src/views",
								"todo_view/tests/main",
							],
						},
						internal: true,
					},
					missing: false,
					depType: [Modules.DependencyType.Internal],
				});
			});

			it("load modules data from project with only prop", async () => {
				const modules = await api.modules(initData, {
					only: ["todo-core"],
				});

				expect(modules.validation!.external).toEqual({});
				expect(modules.validation!.internal).toEqual({
					"todo-core": ["0.1.0"],
					"todo-view": ["0.1.0"],
					todo: ["0.1.0"],
				});
				expect(modules.graph!.sorted).toEqual([["todo-core"]]);
				expect(iterateSorted(modules.graph!.sorted)).toEqual(["todo-core"]);
				expect(modules.graph!.changes).toEqual(null);
				expect(modules.graph!.all).toEqual(["todo-core", "todo-view", "todo"]);
			});

			afterAll(() => {
				clean();
			});
		});
	});

	describe(".validate()", () => {
		describe("check on todo project", () => {
			let clean;
			let initData: InitData;
			const api = Servant();

			beforeAll(() => {
				clean = createProjectToDo();
			});

			beforeAll(async () => {
				initData = await api.init("/projects/project");
			});

			it("load modules data from project", async () => {
				const modules = await api.modules(initData, {});
				const validations = await api.validate(modules.validation!);

				expect(validations.internalInvalidVersions).toEqual([]);
				expect(validations.externalInvalidVersions).toEqual([]);
			});

			afterAll(() => {
				clean();
			});
		});
	});

	describe(".command()", () => {
		describe("clean", () => {
			let clean;
			let initData: InitData;
			const api = Servant();

			beforeEach(() => {
				clean = createProjectToDo();
			});

			beforeEach(async () => {
				initData = await api.init("/projects/project");
			});

			it("clean all .d.ts files", async () => {
				let files = await loadPatterns("/projects/project", ["**/*.d.ts"]);
				expect(files.length).toBe(2);

				const modules = await api.modules(initData, {});
				const command = await api.command(initData, modules, "clean", {});

				expect(command.time).toBeDefined();
				expect(command.data.length).toBe(3);
				expect(command.data[0].type).toBe(DoneType.OK);
				expect(command.data[1].type).toBe(DoneType.OK);
				expect(command.data[2].type).toBe(DoneType.OK);

				files = await loadPatterns("/projects/project", ["**/*.d.ts"]);
				expect(files.length).toBe(0);
			});

			afterEach(() => {
				clean();
			});
		});

		describe("validate", () => {
			let clean;
			let initData: InitData;
			const api = Servant();

			beforeEach(() => {
				clean = createProjectToDo();
			});

			beforeEach(async () => {
				initData = await api.init("/projects/project");
			});

			it("validate all projects", async () => {
				const result = {} as { [ley: string]: CommandResult<any> };
				const modules = await api.modules(initData, {});
				const command = await api.command(initData, modules, "validate", {
					progress: (r) => {
						result[r.module] = r;
					},
				});

				expect(command.time).toBeDefined();
				expect(command.data.length).toBe(3);
				expect(command.data[0].type).toBe(DoneType.OK);
				expect(command.data[1].type).toBe(DoneType.OK);
				expect(command.data[2].type).toBe(DoneType.OK);

				expect(Object.keys(result).length).toBe(3);
				expect(result["todo"].command).toBe("validate");
				expect(result["todo"].type).toBe(DoneType.OK);
			});

			afterEach(() => {
				clean();
			});
		});

		describe("analyze", () => {
			let clean;
			let initData: InitData;
			const api = Servant();

			beforeEach(() => {
				clean = createProjectToDo();
			});

			beforeEach(async () => {
				initData = await api.init("/projects/project");
			});

			it("analyze all projects", async () => {
				const result = {} as { [ley: string]: CommandResult<any> };
				const modules = await api.modules(initData, {});
				const command = await api.command(initData, modules, "analyze", {
					progress: (r) => {
						result[r.module] = r;
					},
				});

				expect(command.time).toBeDefined();
				expect(command.data.sorted).toEqual(["todo-core", "todo-view", "todo"]);
				expect(command.data.analyze).toEqual({
					missing: [],
					versions: {},
				});

				expect(Object.keys(result)).toEqual(["todo-core", "todo-view", "todo"]);
				expect(result["todo"]).toEqual({
					command: "analyze",
					module: "todo",
					type: DoneType.OK,
					message: [],
					error: null,
					data: {
						name: "todo",
						type: ["internal"],
						missing: false,
						versions: { ranges: ["0.1.0"], intersects: true },
						sizes: {
							bundle: 1209,
							sources: 0,
							tests: 0,
							installation: 1209,
						},
						lines: {
							sources: { css: 0, sass: 0, javascript: 0, less: 0, typescript: 0 },
							tests: { css: 0, sass: 0, javascript: 0, less: 0, typescript: 0 },
						},
						dependencies: [],
					},
				});
			});

			afterEach(() => {
				clean();
			});
		});

		describe("unify", () => {
			let clean;
			let initData: InitData;
			const api = Servant();

			function collectDeps(...args: Record<string, string>[]) {
				const versions = {};

				args.forEach((arg) => {
					Object.keys(arg).forEach((module) => {
						const version = arg[module];
						versions[module] = [...new Set([...(versions[module] || []), version])];
					});
				});

				return versions;
			}

			function checkModuleVersions(check: Record<string, string[]>) {
				const todo_core_pj = JSON.parse(
					fs.readFileSync("/projects/project/todo_core/package.json", "utf8")
				);
				const todo_view_pj = JSON.parse(
					fs.readFileSync("/projects/project/todo_view/package.json", "utf8")
				);
				const todo_pj = JSON.parse(
					fs.readFileSync("/projects/project/package.json", "utf8")
				);

				expect(
					collectDeps(
						todo_core_pj.dependencies,
						todo_view_pj.dependencies,
						todo_pj.dependencies
					)
				).toEqual(check);
			}

			beforeEach(() => {
				clean = createProjectToDoForUnify();
			});

			beforeEach(async () => {
				initData = await api.init("/projects/project");
			});

			it("unify versions to latest", async () => {
				checkModuleVersions({
					react: ["16.0.0", "15.0.0", "14.0.0"],
					"react-dom": ["15.0.0", "15.0.1"],
					"react-router": ["3.0.0"],
				});

				const result = {} as { [ley: string]: CommandResult<any> };
				const modules = await api.modules(initData, {});
				const command = await api.command(initData, modules, "unify", {
					modules: {},
					latest: true,
					progress: (r) => {
						result[r.module] = r;
					},
				});

				expect(command.time).toBeDefined();
				expect(command.data).toEqual({
					react: "16.0.0",
					"react-dom": "15.0.1",
				});

				expect(Object.keys(result)).toEqual(["todo-core", "todo-view", "todo"]);
				expect(result["todo-view"]).toEqual({
					command: "unify",
					module: "todo-view",
					type: DoneType.OK,
					message: [],
					error: null,
					data: {
						type: DoneType.OK,
						module: "todo-view",
						unified: [{ type: DoneType.OK, version: "16.0.0", package: "react" }],
						err: null,
					},
				});

				checkModuleVersions({
					react: ["16.0.0"],
					"react-dom": ["15.0.1"],
					"react-router": ["3.0.0"],
				});
			});

			it("unify versions to provided empty", async () => {
				checkModuleVersions({
					react: ["16.0.0", "15.0.0", "14.0.0"],
					"react-dom": ["15.0.0", "15.0.1"],
					"react-router": ["3.0.0"],
				});

				const result = {} as { [ley: string]: CommandResult<any> };
				const modules = await api.modules(initData, {});
				const command = await api.command(initData, modules, "unify", {
					modules: {},
					latest: false,
					progress: (r) => {
						result[r.module] = r;
					},
				});

				expect(command.time).toBeDefined();
				expect(command.data).toEqual({});

				expect(Object.keys(result)).toEqual(["todo-core", "todo-view", "todo"]);
				expect(result["todo-view"]).toEqual({
					command: "unify",
					module: "todo-view",
					type: DoneType.FAIL,
					message: [],
					error: jasmine.any(Error),
					data: {
						type: DoneType.FAIL,
						module: "todo-view",
						unified: [
							{ type: DoneType.FAIL, version: "", package: "react" },
							{ type: DoneType.FAIL, version: "", package: "react-dom" },
						],
						err: jasmine.any(Error),
					},
				});
				expect(result["todo-view"].error!.toString()).toEqual(
					'Error: Module "todo-view" has not been unified. Packages "react, react-dom" can not not been unified due to missing unified version.'
				);
				expect(result["todo-view"].data.err!.toString()).toEqual(
					'Error: Module "todo-view" has not been unified. Packages "react, react-dom" can not not been unified due to missing unified version.'
				);

				checkModuleVersions({
					react: ["16.0.0", "15.0.0", "14.0.0"],
					"react-dom": ["15.0.0", "15.0.1"],
					"react-router": ["3.0.0"],
				});
			});

			it("unify versions to provided versions", async () => {
				checkModuleVersions({
					react: ["16.0.0", "15.0.0", "14.0.0"],
					"react-dom": ["15.0.0", "15.0.1"],
					"react-router": ["3.0.0"],
				});

				const result = {} as { [ley: string]: CommandResult<any> };
				const modules = await api.modules(initData, {});
				const command = await api.command(initData, modules, "unify", {
					modules: {
						react: "1.0.0",
						"react-dom": "2.0.0",
						"react-router": "9.0.0",
					},
					latest: false,
					progress: (r) => {
						result[r.module] = r;
					},
				});

				expect(command.time).toBeDefined();
				expect(command.data).toEqual({
					react: "1.0.0",
					"react-dom": "2.0.0",
					"react-router": "9.0.0",
				});

				expect(Object.keys(result)).toEqual(["todo-core", "todo-view", "todo"]);
				expect(result["todo-view"]).toEqual({
					command: "unify",
					module: "todo-view",
					type: DoneType.OK,
					message: [],
					error: null,
					data: {
						type: DoneType.OK,
						module: "todo-view",
						unified: [
							{ type: DoneType.OK, version: "1.0.0", package: "react" },
							{ type: DoneType.OK, version: "2.0.0", package: "react-dom" },
							{ type: DoneType.OK, version: "9.0.0", package: "react-router" },
						],
						err: null,
					},
				});

				checkModuleVersions({
					react: ["1.0.0"],
					"react-dom": ["2.0.0"],
					"react-router": ["9.0.0"],
				});
			});

			afterEach(() => {
				clean();
			});
		});
	});

	describe("reports", () => {
		it("check", () => {
			const servant = Servant();

			expect(servant.reports.issues.outputs).toEqual(["issues.txt"]);
			expect(servant.reports.issues.run).toBeDefined();

			expect(servant.reports.analyze.outputs).toEqual(["analyze.json", "analyze.xml"]);
			expect(servant.reports.analyze.run).toBeDefined();
		});
	});
});
