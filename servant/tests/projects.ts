import { mockFs } from "./mock-fs";
import { PackageJson, ServantJson, TsConfig } from "../src";

export function createProjectEmpty(): () => void {
	return mockFs({
		// /projects/project
		"/projects": {
			project: {},
		},
	});
}

export function createProjectPrepared(): () => void {
	return mockFs({
		// /projects/project
		"/projects": {
			project: {},
		},
	});
}

export type ProjectToDoFiles = {
	"todo_core/package.json": PackageJson.PackageJson;
	"todo_core/servant.json": ServantJson.ServantJson;
	"todo_view/package.json": PackageJson.PackageJson;
	"todo_view/servant.json": ServantJson.ServantJson;
	"package.json": PackageJson.PackageJson;
	"servant.json": ServantJson.ServantJson;
};
export function createProjectToDoFiles(): ProjectToDoFiles {
	const mainServantJson = ServantJson.create("todo");
	mainServantJson.modules = ["./todo_*/", "./"];

	return {
		"todo_core/package.json": PackageJson.create("todo-core", "0.1.0"),
		"todo_core/servant.json": ServantJson.create("todo-core"),
		"todo_view/package.json": PackageJson.create("todo-view", "0.1.0"),
		"todo_view/servant.json": ServantJson.create("todo-view"),
		"package.json": PackageJson.create("todo", "0.1.0"),
		"servant.json": mainServantJson,
	};
}
export function createProjectToDo(files: ProjectToDoFiles = createProjectToDoFiles()): () => void {
	return mockFs({
		// /projects/project
		"/projects": {
			project: {
				todo_core: {
					src: {
						views: {
							"main.css": `.root {
	padding: 0;
	margin: 0;
	font.size: 12px;
}`,
							"main.less": `.root {
	padding: 0;
	margin: 0;
	font.size: 12px;
}`,
						},
						"index.js": `//This is main file
export function test() {
	console.log("This is test function");
}`,
						"index.ts": `//This is main file
export function test() {
	console.log("This is test function");
}`,
						"index.d.ts": "",
					},
					tests: {
						main: {
							"test.spec.ts": "",
							"test.spec.js": "",
						},
					},
					"package.json": JSON.stringify(files["todo_core/package.json"], null, "\t"),
					"servant.json": JSON.stringify(files["todo_core/servant.json"], null, "\t"),
					"tsconfig.json": JSON.stringify(TsConfig.create(), null, "\t"),
				},
				todo_view: {
					src: {
						views: {
							"main.css": "",
							"main.less": "",
						},
						"index.js": "",
						"index.ts": "",
						"index.d.ts": "",
					},
					tests: {
						main: {
							"test.spec.ts": "",
							"test.spec.js": "",
						},
					},
					"package.json": JSON.stringify(files["todo_view/package.json"], null, "\t"),
					"servant.json": JSON.stringify(files["todo_view/servant.json"], null, "\t"),
					"tsconfig.json": JSON.stringify(TsConfig.create(), null, "\t"),
				},
				"README.md": "This is readme file.",
				"logo.png": Buffer.from(
					"iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg==",
					"base64"
				),
				"description.txt": "This is description.",
				"package.json": JSON.stringify(files["package.json"], null, "\t"),
				"servant.json": JSON.stringify(files["servant.json"], null, "\t"),
				"tsconfig.json": JSON.stringify(TsConfig.create(), null, "\t"),
				"index.js": "console.log('Hello word!');",
			},
		},
	});
}

export function createProjectToDoForUnify(
	files: ProjectToDoFiles = createProjectToDoFiles()
): () => void {
	let packageJson: PackageJson.PackageJson;

	packageJson = files["todo_core/package.json"];
	packageJson.dependencies = {
		react: "16.0.0",
		"react-dom": "15.0.0",
		"react-router": "3.0.0",
	};

	packageJson = files["todo_view/package.json"];
	packageJson.dependencies = {
		react: "15.0.0",
		"react-dom": "15.0.1",
		"react-router": "3.0.0",
	};

	packageJson = files["package.json"];
	packageJson.dependencies = {
		react: "14.0.0",
		"react-dom": "15.0.1",
		"react-router": "3.0.0",
	};

	return createProjectToDo(files);
}
