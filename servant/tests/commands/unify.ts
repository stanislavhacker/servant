import { answersUnify } from "../../src/commands";

describe("unify", () => {
	describe("answers", () => {
		it("empty", () => {
			expect(answersUnify({})).toEqual([]);
		});

		it("latest", () => {
			expect(answersUnify({ latest: true })).toEqual([{ id: "type", value: "latest" }]);
		});

		it("define modules", () => {
			expect(answersUnify({ modules: { react: "15" } })).toEqual([
				{ id: "type", value: "provided" },
				{ id: "unified-react", value: "15" },
			]);
		});

		it("define modules and latest", () => {
			expect(answersUnify({ modules: { react: "15" }, latest: true })).toEqual([
				{ id: "type", value: "latest" },
				{ id: "unified-react", value: "15" },
			]);
		});
	});
});
