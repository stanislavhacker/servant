/* eslint-disable @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-explicit-any */
import { install, update, publish, NpmStatus } from "../../src/libraries/npm";
import { PackageJson } from "@servant/servant-data";
import * as chp from "child_process";

describe("servant libraries", () => {
	type ChildProcessRemote = {
		close: (code: number) => void;
		data: (text: string) => void;
		error: (text: string) => void;
	};

	function createChildProcess(name: string): [chp.ChildProcess, ChildProcessRemote] {
		const mainMock = {};
		const stdoutMock = {};
		const stderrMock = {};
		const mock = jasmine.createSpyObj(name, ["on"]);

		mock.on.and.callFake((eventName, callback) => {
			mainMock[eventName] = callback;
		});

		mock.stdout = jasmine.createSpyObj("stdout", ["on"]);
		mock.stdout.on.and.callFake((eventName, callback) => {
			stdoutMock[eventName] = callback;
		});

		mock.stderr = jasmine.createSpyObj("stderr", ["on"]);
		mock.stderr.on.and.callFake((eventName, callback) => {
			stderrMock[eventName] = callback;
		});

		return [
			mock,
			{
				close: (code: number) => mainMock["close"](code),
				data: (text: string) => stdoutMock["data"](text),
				error: (text: string) => stderrMock["data"](text),
			},
		];
	}

	describe("npm", () => {
		let packageJson: PackageJson.PackageJsonInfo;

		beforeEach(() => {
			packageJson = {
				cwd: "/path/to/dir",
				path: "/path/to/dir/package.json",
				content: PackageJson.map(PackageJson.create("test", "1.0.0")),
				main: false,
			};
		});

		it("install without registry and error", (done) => {
			const child = createChildProcess("npm");
			spyOn(chp, "spawn").and.returnValue(child[0]);

			const p = install(packageJson);

			child[1].data("audited: 1112\nremoved: 100\nadded: 99\nfound 3 vulnerabilities");
			child[1].close(0);

			p.then((info) => {
				expect(info.command).toBe("npm install");
				expect(info.data).toEqual({
					package: null,
					published: null,
					audited: 0,
					added: 0,
					removed: 0,
					vulnerabilities: 3,
				});
				expect(info.error).toBe(null);
				expect(info.status).toBe(NpmStatus.OK);
				done();
			});
		});

		it("install without registry and with error", (done) => {
			const child = createChildProcess("npm");
			spyOn(chp, "spawn").and.returnValue(child[0]);

			const p = install(packageJson);

			child[1].data("");
			child[1].error("npm ERR! code ETARGET\nnpm ERR! ETARGET This is error message.");
			child[1].close(9);

			p.then((info) => {
				expect(info.command).toBe("npm install");
				expect(info.data).toEqual(null);
				expect(info.error!.message).toBe("ETARGET: This is error message. ");
				expect(info.status).toBe(NpmStatus.Error);
				done();
			});
		});

		it("install with simple registry and without error", (done) => {
			const child = createChildProcess("npm");
			spyOn(chp, "spawn").and.returnValue(child[0]);

			const p = install(packageJson, {
				registry: "http://my-registry.com/main",
				scopes: {},
			});

			child[1].data("");
			child[1].close(0);

			p.then((info) => {
				expect(info.command).toBe('npm install --registry "http://my-registry.com/main"');
				done();
			});
		});

		it("install with simple registry and scopes and without error", (done) => {
			const child = createChildProcess("npm");
			spyOn(chp, "spawn").and.returnValue(child[0]);

			const p = install(packageJson, {
				registry: "http://my-registry.com/main",
				scopes: {
					"@scope1": "http://my-registry.com/scope1",
					"@scope2": "http://my-registry.com/scope2",
				},
			});

			child[1].data("");
			child[1].close(0);

			p.then((info) => {
				expect(info.command).toBe(
					'npm install --registry "http://my-registry.com/main" --@scope1:registry "http://my-registry.com/scope1" --@scope2:registry "http://my-registry.com/scope2"'
				);
				done();
			});
		});

		it("update with simple registry and scopes and without error", (done) => {
			const child = createChildProcess("npm");
			spyOn(chp, "spawn").and.returnValue(child[0]);

			const p = update(packageJson, {
				registry: "http://my-registry.com/main",
				scopes: {
					"@scope1": "http://my-registry.com/scope1",
					"@scope2": "http://my-registry.com/scope2",
				},
			});

			child[1].data("");
			child[1].close(0);

			p.then((info) => {
				expect(info.command).toBe(
					'npm update --registry "http://my-registry.com/main" --@scope1:registry "http://my-registry.com/scope1" --@scope2:registry "http://my-registry.com/scope2"'
				);
				done();
			});
		});

		it("publish with simple registry and scopes and without error", (done) => {
			const child = createChildProcess("npm");
			spyOn(chp, "spawn").and.returnValue(child[0]);

			const p = publish(packageJson, "latest", "public", {
				registry: "http://my-registry.com/main",
				scopes: {
					"@scope1": "http://my-registry.com/scope1",
					"@scope2": "http://my-registry.com/scope2",
				},
			});

			child[1].data("");
			child[1].close(0);

			p.then((info) => {
				expect(info.command).toBe(
					'npm publish --tag latest --access public --registry "http://my-registry.com/main" --@scope1:registry "http://my-registry.com/scope1" --@scope2:registry "http://my-registry.com/scope2"'
				);
				done();
			});
		});
	});
});
