import {
	EslintrcJson,
	PackageJson,
	PrettierrcJson,
	ServantJson,
	TrancorderJson,
} from "@servant/servant-data";

import { resolveEntries, InitData, ModulesData, Module } from "../src";

describe("resolveEntries", () => {
	const path = "/path/to/project";
	const name = "root-project";

	function createInit(
		path: string,
		name: string,
		root: { server: ServantJson.ServerSettings; target: ServantJson.ModuleTarget }
	) {
		const servantJson = ServantJson.create(name);
		servantJson.server = root.server;
		servantJson.target = root.target;

		return {
			entry: path,
			module: undefined,
			servantJson: {
				content: servantJson,
				path: `${path}/${ServantJson.SERVANT_JSON}`,
				cwd: path,
			},
			packageJson: {
				main: true,
				content: PackageJson.map(PackageJson.create(name, "1.0.0")),
				path: `${path}/${PackageJson.PACKAGE_JSON}`,
				cwd: path,
			},
		} as InitData;
	}

	function createModuleDefinition(
		path: string,
		name: string,
		server: ServantJson.ServerSettings,
		target: ServantJson.ModuleTarget
	): Module.ModuleDefinition {
		const servantJson = ServantJson.create(name);
		servantJson.server = server;
		servantJson.target = target;

		return {
			module: {
				servantJson: {
					content: servantJson,
					path: `${path}/${ServantJson.SERVANT_JSON}`,
					cwd: path,
				},
				packageJson: {
					main: true,
					content: PackageJson.map(PackageJson.create(name, "1.0.0")),
					path: `${path}/${PackageJson.PACKAGE_JSON}`,
					cwd: path,
				},
				prettierrcJson: {
					content: PrettierrcJson.createDefault(),
					path: `${path}/${PrettierrcJson.PRETTIERRC_JSON}`,
					cwd: path,
				},
				eslintrcJson: {
					content: EslintrcJson.createDefault(false),
					path: `${path}/${EslintrcJson.ESLINTRC_JSON}`,
					cwd: path,
				},
				trancorderJson: {
					content: TrancorderJson.createDefault(),
					path: `${path}/${TrancorderJson.TRANCORDER_JSON}`,
					cwd: path,
				},
				directories: {
					tests: [],
					others: [],
					sources: [],
				},
				dependencies: [],
				internal: true,
			},
			name: name,
			externals: [],
			internals: [],
			depType: [],
			versions: [],
			missing: false,
		};
	}

	function createData(
		root: { server: ServantJson.ServerSettings; target: ServantJson.ModuleTarget },
		modules: Record<
			string,
			{ server: ServantJson.ServerSettings; target: ServantJson.ModuleTarget }
		>
	) {
		const init = createInit(path, name, root);
		const data: ModulesData = {
			validation: null,
			graph: {
				changes: null,
				sorted: [Object.keys(modules)],
				all: Object.keys(modules),
				modules: Object.keys(modules).reduce(
					(prev, module) => {
						const { server, target } = modules[module];
						prev[module] = createModuleDefinition(
							`${path}/${module}`,
							module,
							server,
							target
						);
						return prev;
					},
					{
						[name]: createModuleDefinition(path, name, root.server, root.target),
					}
				),
			},
		};

		return {
			init,
			modules: data,
		};
	}

	function createServer(
		target: ServantJson.ModuleTarget,
		entries: ServantJson.ServerSettings["entries"] = {},
		css: ServantJson.ServerSettings["css"] = [],
		js: ServantJson.ServerSettings["js"] = []
	): { server: ServantJson.ServerSettings; target: ServantJson.ModuleTarget } {
		return {
			server: {
				entries,
				css,
				js,
			},
			target,
		};
	}

	it("client empty object", () => {
		const { init, modules } = createData(createServer(ServantJson.ModuleTarget.web), {});
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: {},
		});
	});

	it("client filled root object", () => {
		const { init, modules } = createData(
			createServer(
				ServantJson.ModuleTarget.web,
				{ [name]: { title: "Root Project", template: "./template.html" } },
				["./local.css", "https://remote.css"],
				["./local.js", "https://remote.js"]
			),
			{}
		);
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: { [name]: { title: "Root Project", template: "./template.html" } },
		});
	});

	it("client filled modules object", () => {
		const { init, modules } = createData(createServer(ServantJson.ModuleTarget.web), {
			[name]: createServer(
				ServantJson.ModuleTarget.web,
				{ [name]: { title: "Root Project", template: "./template.html" } },
				["./local.css", "https://remote.css"],
				["./local.js", "https://remote.js"]
			),
			["sub-project-1"]: createServer(
				ServantJson.ModuleTarget.web,
				{ ["sub-project-1"]: { title: "Sub Project 1", template: "./index.html" } },
				["./sp1.css", "https://sp1.css"],
				["./sp1.js", "https://sp1.js"]
			),
		});
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: {
				"root-project": { title: "Root Project", template: "root-project/template.html" },
				"sub-project-1": { title: "Sub Project 1", template: "sub-project-1/index.html" },
			},
		});
	});

	it("client filled root and modules object", () => {
		const { init, modules } = createData(
			createServer(
				ServantJson.ModuleTarget.web,
				{ [name]: { title: "Root Project", template: "./template.html" } },
				["./local.css", "https://remote.css"],
				["./local.js", "https://remote.js"]
			),
			{
				["sub-project-1"]: createServer(
					ServantJson.ModuleTarget.web,
					{ ["sub-project-1"]: { title: "Sub  Project 1", template: "./index1.html" } },
					["./sp1.css", "https://sp1.css"],
					["./sp1.js", "https://sp2.js"]
				),
				["sub-project-2"]: createServer(
					ServantJson.ModuleTarget.web,
					{ ["sub-project-2"]: { title: "Sub Project 2", template: "./index2.html" } },
					["./sp2.css", "https://sp2.css"],
					["./sp2.js", "https://sp2.js"]
				),
			}
		);
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: {
				"root-project": { title: "Root Project", template: "./template.html" },
				"sub-project-1": { title: "Sub  Project 1", template: "sub-project-1/index1.html" },
				"sub-project-2": { title: "Sub Project 2", template: "sub-project-2/index2.html" },
			},
		});
	});

	it("client merging same defined modules but with override all", () => {
		const { init, modules } = createData(
			createServer(
				ServantJson.ModuleTarget.web,
				{ [name]: { title: "Root Project", template: "./template.html" } },
				["./local.css", "https://remote.css"],
				["./local.js", "https://remote.js"]
			),
			{
				[name]: createServer(
					ServantJson.ModuleTarget.web,
					{ [name]: { title: "Root Project 1", template: "./template1.html" } },
					["./local.css", "https://remote.css"],
					["./local.js", "https://remote.js"]
				),
			}
		);
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: {
				"root-project": {
					title: "Root Project 1",
					template: "root-project/template1.html",
				},
			},
		});
	});

	it("client merging same defined modules but with no override some props", () => {
		const { init, modules } = createData(
			createServer(
				ServantJson.ModuleTarget.web,
				{ [name]: { title: "Root Project", template: "./template.html" } },
				[],
				[]
			),
			{
				[name]: createServer(
					ServantJson.ModuleTarget.web,
					{ [name]: {} },
					["./local.css", "https://remote.css"],
					["./local.js", "https://remote.js"]
				),
			}
		);
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: {
				"root-project": { title: "Root Project", template: "./template.html" },
			},
		});
	});

	it("client merging same defined modules but with override some props", () => {
		const { init, modules } = createData(
			createServer(
				ServantJson.ModuleTarget.web,
				{ [name]: { title: "Root Project", template: "./template.html" } },
				[],
				[]
			),
			{
				[name]: createServer(
					ServantJson.ModuleTarget.web,
					{ [name]: { template: "./current.html" } },
					["./local.css", "https://remote.css"],
					["./local.js", "https://remote.js"]
				),
			}
		);
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: {
				"root-project": { title: "Root Project", template: "root-project/current.html" },
			},
		});
	});

	it("client filled some relative paths", () => {
		const { init, modules } = createData(createServer(ServantJson.ModuleTarget.web), {
			[name]: createServer(
				ServantJson.ModuleTarget.web,
				{ [name]: { template: "../module-static/current.html", title: "Title" } },
				["../module-static/local.css", "https://remote.css"],
				["../module-static/local.js", "https://remote.js"]
			),
		});
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: {
				"root-project": { title: "Title", template: "module-static/current.html" },
			},
		});
	});

	it("server empty object", () => {
		const { init, modules } = createData(createServer(ServantJson.ModuleTarget.node), {});
		const { server } = resolveEntries(init, modules);
		expect(server).toEqual({
			entries: {},
		});
	});

	it("server filled root object", () => {
		const { init, modules } = createData(
			createServer(
				ServantJson.ModuleTarget.node,
				{ [name]: { command: { file: "./dist/out.js" } } },
				["./local.css", "https://remote.css"],
				["./local.js", "https://remote.js"]
			),
			{}
		);
		const { server } = resolveEntries(init, modules);
		expect(server).toEqual({
			entries: { [name]: { command: { file: "./dist/out.js" } } },
		});
	});

	it("server filled modules object", () => {
		const { init, modules } = createData(createServer(ServantJson.ModuleTarget.web), {
			[name]: createServer(
				ServantJson.ModuleTarget.node,
				{
					[name]: {
						command: { file: "./dist/out.js", args: ["start", "--port", "9000"] },
					},
				},
				["./local.css", "https://remote.css"],
				["./local.js", "https://remote.js"]
			),
			["sub-project-1"]: createServer(
				ServantJson.ModuleTarget.node,
				{
					["sub-project-1"]: {
						command: { file: "./dist/out.js", args: ["start", "--port", "9000"] },
					},
				},
				["./sp1.css", "https://sp1.css"],
				["./sp1.js", "https://sp1.js"]
			),
		});
		const { server } = resolveEntries(init, modules);
		expect(server).toEqual({
			entries: {
				"root-project": {
					command: {
						file: "root-project/dist/out.js",
						args: ["start", "--port", "9000"],
					},
				},
				"sub-project-1": {
					command: {
						file: "sub-project-1/dist/out.js",
						args: ["start", "--port", "9000"],
					},
				},
			},
		});
	});

	it("server filled root and modules object", () => {
		const { init, modules } = createData(
			createServer(
				ServantJson.ModuleTarget.node,
				{
					[name]: {
						command: { file: "./dist/root.js", args: ["start", "--port", "9000"] },
					},
				},
				["./local.css", "https://remote.css"],
				["./local.js", "https://remote.js"]
			),
			{
				["sub-project-1"]: createServer(
					ServantJson.ModuleTarget.node,
					{
						["sub-project-1"]: {
							command: { file: "./dist/out1.js", args: ["start", "--port", "9001"] },
						},
					},
					["./sp1.css", "https://sp1.css"],
					["./sp1.js", "https://sp2.js"]
				),
				["sub-project-2"]: createServer(
					ServantJson.ModuleTarget.node,
					{
						["sub-project-2"]: {
							command: { file: "./dist/out2.js", args: ["start", "--port", "9002"] },
						},
					},
					["./sp2.css", "https://sp2.css"],
					["./sp2.js", "https://sp2.js"]
				),
			}
		);
		const { server } = resolveEntries(init, modules);
		expect(server).toEqual({
			entries: {
				"root-project": {
					command: { file: "./dist/root.js", args: ["start", "--port", "9000"] },
				},
				"sub-project-1": {
					command: {
						file: "sub-project-1/dist/out1.js",
						args: ["start", "--port", "9001"],
					},
				},
				"sub-project-2": {
					command: {
						file: "sub-project-2/dist/out2.js",
						args: ["start", "--port", "9002"],
					},
				},
			},
		});
	});

	it("server merging same defined modules but with no override some props", () => {
		const { init, modules } = createData(
			createServer(
				ServantJson.ModuleTarget.node,
				{
					[name]: {
						command: { file: "./dist/root.js", args: ["start", "--port", "9000"] },
					},
				},
				[],
				[]
			),
			{
				[name]: createServer(
					ServantJson.ModuleTarget.node,
					{ [name]: {} },
					["./local.css", "https://remote.css"],
					["./local.js", "https://remote.js"]
				),
			}
		);
		const { server } = resolveEntries(init, modules);
		expect(server).toEqual({
			entries: {
				"root-project": {
					command: { file: "./dist/root.js", args: ["start", "--port", "9000"] },
				},
			},
		});
	});

	it("server merging same defined modules but with override some props", () => {
		const { init, modules } = createData(
			createServer(
				ServantJson.ModuleTarget.node,
				{
					[name]: {
						command: { file: "./dist/root.js", args: ["start", "--port", "9000"] },
					},
				},
				[],
				[]
			),
			{
				[name]: createServer(
					ServantJson.ModuleTarget.node,
					{ [name]: { command: { args: ["start", "--port", "9001"] } } },
					["./local.css", "https://remote.css"],
					["./local.js", "https://remote.js"]
				),
			}
		);
		const { server } = resolveEntries(init, modules);
		expect(server).toEqual({
			entries: {
				"root-project": { command: { args: ["start", "--port", "9001"] } },
			},
		});
	});

	it("server filled some relative paths", () => {
		const { init, modules } = createData(createServer(ServantJson.ModuleTarget.node), {
			[name]: createServer(
				ServantJson.ModuleTarget.node,
				{
					[name]: {
						command: { file: "./dist/root.js", args: ["start", "--port", "9000"] },
					},
				},
				["../module-static/local.css", "https://remote.css"],
				["../module-static/local.js", "https://remote.js"]
			),
		});
		const { server } = resolveEntries(init, modules);
		expect(server).toEqual({
			entries: {
				"root-project": {
					command: {
						file: "root-project/dist/root.js",
						args: ["start", "--port", "9000"],
					},
				},
			},
		});
	});

	it("client as page filled some relative paths", () => {
		const { init, modules } = createData(createServer(ServantJson.ModuleTarget.web_page), {
			[name]: createServer(
				ServantJson.ModuleTarget.web_page,
				{ [name]: { template: "../module-static/current.html", title: "Title" } },
				["../module-static/local.css", "https://remote.css"],
				["../module-static/local.js", "https://remote.js"]
			),
		});
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: {
				"root-project": { title: "Title", template: "module-static/current.html" },
			},
		});
	});

	it("shared folders resolve", () => {
		const { init, modules } = createData(createServer(ServantJson.ModuleTarget.web), {
			[name]: createServer(
				ServantJson.ModuleTarget.web,
				{
					[name]: {
						title: "Root Project",
						template: "./template.html",
						shared: { "./static/public": "sub-project-1" },
					},
				},
				["./local.css", "https://remote.css"],
				["./local.js", "https://remote.js"]
			),
			["sub-project-1"]: createServer(
				ServantJson.ModuleTarget.web,
				{ ["sub-project-1"]: { title: "Sub Project 1", template: "./index.html" } },
				["./sp1.css", "https://sp1.css"],
				["./sp1.js", "https://sp1.js"]
			),
		});
		const { client } = resolveEntries(init, modules);
		expect(client).toEqual({
			entries: {
				"root-project": {
					title: "Root Project",
					template: "root-project/template.html",
					shared: { "root-project/static/public": "sub-project-1" },
				},
				"sub-project-1": {
					title: "Sub Project 1",
					template: "sub-project-1/index.html",
				},
			},
		});
	});
});
