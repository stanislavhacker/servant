/* eslint-disable @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-explicit-any */
import { PackageJson } from "@servant/servant-data";

import * as npm from "../../src/libraries/npm";
import * as fs from "fs";
import { createProjectToDo, createProjectToDoFiles } from "../projects";
import { init, InitData, modules, ModulesData } from "../../src";
import { publish } from "../../src/module";
import { createNpmResult } from "../../src/libraries/npm";

describe("servant modules", () => {
	describe("publish", () => {
		async function prepare1(): Promise<[ModulesData, InitData, () => void]> {
			const clean = createProjectToDo();
			const initData = await init("/projects/project");
			const md = await modules(initData, {});

			return [md, initData, clean];
		}

		it("simple publish call in dev mode with empty project", async () => {
			const [md, init, clean] = await prepare1();

			spyOn(npm, "publish").and.callFake(() =>
				Promise.resolve(createNpmResult("npm publish"))
			);
			spyOn(npm, "pack").and.callFake(() => {
				const json = JSON.parse(
					fs.readFileSync("/projects/project/package.json").toString()
				) as PackageJson.PackageJson;

				expect(json.name).toBe("todo");
				expect(json.version).toBe("0.1.1");
				expect(json.dependencies).toEqual(undefined);
				expect(json.devDependencies).toEqual(undefined);
				expect(json.main).toBe("dist/todo.js");
				expect(json.types).toBe(undefined);

				return Promise.resolve(createNpmResult("npm pack"));
			});

			const graph = md.graph!;
			const result = await publish(init.packageJson!, graph.modules["todo"], graph, false);
			expect(result.command).toBe("publish");

			expect(npm.publish).not.toHaveBeenCalled();
			expect(npm.pack).toHaveBeenCalled();

			await clean();
		});

		async function prepare2(): Promise<[ModulesData, InitData, () => void]> {
			const files = createProjectToDoFiles();

			files["package.json"].dependencies = { "todo-view": "file:./todo_view" };
			files["todo_view/package.json"].dependencies = { "todo-core": "file:../todo_core" };
			files["todo_view/package.json"].devDependencies = {
				eslint: "3.3.3",
				"generate-doc": "0.8.9",
			};
			files["todo_view/servant.json"].entry = "src/index";
			files["todo_view/servant.json"].publish = {
				access: "public",
				increment: "patch",
				stripDev: false,
				commitMessage: "",
			};
			files["todo_core/servant.json"].entry = "src/index";
			files["todo_core/servant.json"].publish = {
				access: "public",
				increment: "patch",
				stripDev: false,
				commitMessage: "",
			};

			const clean = createProjectToDo(files);
			const initData = await init("/projects/project");
			const md = await modules(initData, {});

			return [md, initData, clean];
		}

		it("simple publish call in dev mode with filled project 1", async () => {
			const [md, init, clean] = await prepare2();

			spyOn(npm, "publish").and.callFake(() =>
				Promise.resolve(createNpmResult("npm publish"))
			);
			spyOn(npm, "pack").and.callFake(() => {
				const json = JSON.parse(
					fs.readFileSync("/projects/project/todo_view/package.json").toString()
				) as PackageJson.PackageJson;

				expect(json.name).toBe("todo-view");
				expect(json.version).toBe("0.1.1");
				expect(json.dependencies).toEqual({
					"todo-core": "0.1.0",
				});
				expect(json.devDependencies).toEqual({
					eslint: "3.3.3",
					"generate-doc": "0.8.9",
				});
				expect(json.main).toBe("dist/todo-view.js");
				expect(json.types).toBe(undefined);

				return Promise.resolve(createNpmResult("npm pack"));
			});

			const graph = md.graph!;
			const result = await publish(
				init.packageJson!,
				graph.modules["todo-view"],
				graph,
				false
			);
			expect(result.command).toBe("publish");

			expect(npm.publish).not.toHaveBeenCalled();
			expect(npm.pack).toHaveBeenCalled();

			await clean();
		});

		it("simple publish call in prod mode with filled project 1", async () => {
			const [md, init, clean] = await prepare2();

			spyOn(npm, "publish").and.callFake(() => {
				const json = JSON.parse(
					fs.readFileSync("/projects/project/todo_view/package.json").toString()
				) as PackageJson.PackageJson;

				expect(json.name).toBe("todo-view");
				expect(json.version).toBe("0.1.1");
				expect(json.dependencies).toEqual({
					"todo-core": "0.1.0",
				});
				expect(json.devDependencies).toEqual({
					eslint: "3.3.3",
					"generate-doc": "0.8.9",
				});
				expect(json.main).toBe("dist/todo-view.js");
				expect(json.types).toBe(undefined);

				return Promise.resolve(createNpmResult("npm publish"));
			});
			spyOn(npm, "pack").and.callFake(() => Promise.resolve(createNpmResult("npm pack")));

			const graph = md.graph!;
			const result = await publish(
				init.packageJson!,
				graph.modules["todo-view"],
				graph,
				true
			);
			expect(result.command).toBe("publish");

			expect(npm.publish).toHaveBeenCalled();
			expect(npm.pack).not.toHaveBeenCalled();

			await clean();
		});

		async function prepare3(): Promise<[ModulesData, InitData, () => void]> {
			const files = createProjectToDoFiles();

			files["package.json"].dependencies = { "todo-view": "file:./todo_view" };
			files["todo_view/package.json"].dependencies = { "todo-core": "file:../todo_core" };
			files["todo_view/package.json"].devDependencies = {
				eslint: "3.3.3",
				"generate-doc": "0.8.9",
			};
			files["todo_view/servant.json"].entry = "src/index";
			files["todo_view/servant.json"].publish = {
				access: "public",
				increment: "patch",
				stripDev: true,
				commitMessage: "",
			};
			files["todo_core/servant.json"].entry = "src/index";
			files["todo_core/servant.json"].publish = {
				access: "public",
				increment: "patch",
				stripDev: false,
				commitMessage: "",
			};

			const clean = createProjectToDo(files);
			const initData = await init("/projects/project");
			const md = await modules(initData, {});

			return [md, initData, clean];
		}

		it("simple publish call in prod mode and strip dev with filled project 1", async () => {
			const [md, init, clean] = await prepare3();

			spyOn(npm, "publish").and.callFake(() => {
				const json = JSON.parse(
					fs.readFileSync("/projects/project/todo_view/package.json").toString()
				) as PackageJson.PackageJson;

				expect(json.name).toBe("todo-view");
				expect(json.version).toBe("0.1.1");
				expect(json.dependencies).toEqual({
					"todo-core": "0.1.0",
				});
				expect(json.devDependencies).toEqual(undefined);
				expect(json.main).toBe("dist/todo-view.js");
				expect(json.types).toBe(undefined);

				return Promise.resolve(createNpmResult("npm publish"));
			});
			spyOn(npm, "pack").and.callFake(() => Promise.resolve(createNpmResult("npm pack")));

			const graph = md.graph!;
			const result = await publish(
				init.packageJson!,
				graph.modules["todo-view"],
				graph,
				true
			);
			expect(result.command).toBe("publish");

			expect(npm.publish).toHaveBeenCalled();
			expect(npm.pack).not.toHaveBeenCalled();

			await clean();
		});

		async function prepare4(): Promise<[ModulesData, InitData, () => void]> {
			const files = createProjectToDoFiles();

			files["package.json"].dependencies = { "todo-view": "file:./todo_view" };
			files["servant.json"].publish = { stripDev: true } as any;
			files["todo_view/package.json"].dependencies = { "todo-core": "file:../todo_core" };
			files["todo_view/package.json"].devDependencies = {
				eslint: "3.3.3",
				"generate-doc": "0.8.9",
			};
			files["todo_view/servant.json"].entry = "src/index";
			files["todo_view/servant.json"].publish = {
				access: "public",
				increment: "patch",
			} as any;
			files["todo_core/servant.json"].entry = "src/index";
			files["todo_core/servant.json"].publish = {
				access: "public",
				increment: "patch",
			} as any;

			const clean = createProjectToDo(files);
			const initData = await init("/projects/project");
			const md = await modules(initData, {});

			return [md, initData, clean];
		}

		it("simple publish call in prod mode and strip dev in parent with filled project 1", async () => {
			const [md, init, clean] = await prepare4();

			spyOn(npm, "publish").and.callFake(() => {
				const json = JSON.parse(
					fs.readFileSync("/projects/project/todo_view/package.json").toString()
				) as PackageJson.PackageJson;

				expect(json.name).toBe("todo-view");
				expect(json.version).toBe("0.1.1");
				expect(json.dependencies).toEqual({
					"todo-core": "0.1.0",
				});
				expect(json.devDependencies).toEqual(undefined);
				expect(json.main).toBe("dist/todo-view.js");
				expect(json.types).toBe(undefined);

				return Promise.resolve(createNpmResult("npm publish"));
			});
			spyOn(npm, "pack").and.callFake(() => Promise.resolve(createNpmResult("npm pack")));

			const graph = md.graph!;
			const result = await publish(
				init.packageJson!,
				graph.modules["todo-view"],
				graph,
				true
			);
			expect(result.command).toBe("publish");

			expect(npm.publish).toHaveBeenCalled();
			expect(npm.pack).not.toHaveBeenCalled();

			await clean();
		});

		async function prepare5(): Promise<[ModulesData, InitData, () => void]> {
			const files = createProjectToDoFiles();

			files["package.json"].dependencies = { "todo-view": "file:./todo_view" };
			files["todo_view/package.json"].dependencies = { "todo-core": "file:../todo_core" };
			files["todo_view/package.json"].devDependencies = {
				eslint: "3.3.3",
				"generate-doc": "0.8.9",
			};
			files["todo_view/package.json"].bundledDependencies = ["eslint"];
			files["todo_view/servant.json"].entry = "src/index";
			files["todo_view/servant.json"].publish = {
				access: "public",
				increment: "patch",
			} as any;
			files["todo_core/servant.json"].entry = "src/index";
			files["todo_core/servant.json"].publish = {
				access: "public",
				increment: "patch",
			} as any;

			const clean = createProjectToDo(files);
			const initData = await init("/projects/project");
			const md = await modules(initData, {});

			return [md, initData, clean];
		}

		it("simple publish call in prod mode and remove bundled dependencies", async () => {
			const [md, init, clean] = await prepare5();

			spyOn(npm, "publish").and.callFake(() => {
				const json = JSON.parse(
					fs.readFileSync("/projects/project/todo_view/package.json").toString()
				) as PackageJson.PackageJson;

				expect(json.name).toBe("todo-view");
				expect(json.version).toBe("0.1.1");
				expect(json.dependencies).toEqual({
					"todo-core": "0.1.0",
				});
				expect(json.devDependencies).toEqual({
					"generate-doc": "0.8.9",
				});
				expect(json.bundledDependencies).toEqual(undefined);
				expect(json.main).toBe("dist/todo-view.js");
				expect(json.types).toBe(undefined);

				return Promise.resolve(createNpmResult("npm publish"));
			});
			spyOn(npm, "pack").and.callFake(() => Promise.resolve(createNpmResult("npm pack")));

			const graph = md.graph!;
			const result = await publish(
				init.packageJson!,
				graph.modules["todo-view"],
				graph,
				true
			);
			expect(result.command).toBe("publish");

			expect(npm.publish).toHaveBeenCalled();
			expect(npm.pack).not.toHaveBeenCalled();

			await clean();
		});

		it("simple publish call in prod mode and set increment as major", async () => {
			const [md, init, clean] = await prepare5();

			spyOn(npm, "publish").and.callFake(() => {
				const json = JSON.parse(
					fs.readFileSync("/projects/project/todo_view/package.json").toString()
				) as PackageJson.PackageJson;

				expect(json.name).toBe("todo-view");
				expect(json.version).toBe("1.0.0");

				return Promise.resolve(createNpmResult("npm publish"));
			});
			spyOn(npm, "pack").and.callFake(() => Promise.resolve(createNpmResult("npm pack")));

			const graph = md.graph!;
			const result = await publish(
				init.packageJson!,
				graph.modules["todo-view"],
				graph,
				true,
				undefined,
				false,
				false,
				"major"
			);
			expect(result.command).toBe("publish");

			expect(npm.publish).toHaveBeenCalled();
			expect(npm.pack).not.toHaveBeenCalled();

			await clean();
		});

		it("simple publish call in prod mode and set increment as nonsense", async () => {
			const [md, init, clean] = await prepare5();

			spyOn(npm, "publish").and.callFake(() =>
				Promise.resolve(createNpmResult("npm publish"))
			);
			spyOn(npm, "pack").and.callFake(() => Promise.resolve(createNpmResult("npm pack")));

			const graph = md.graph!;
			const result = await publish(
				init.packageJson!,
				graph.modules["todo-view"],
				graph,
				true,
				undefined,
				false,
				false,
				"nonsense" as any
			);
			expect(result.command).toBe("publish");
			expect(result.error!.message).toEqual(
				'Can not update version in "package.json" for module "todo-view". Value "nonsense" is not valid for increment type.'
			);

			expect(npm.publish).not.toHaveBeenCalled();
			expect(npm.pack).not.toHaveBeenCalled();

			await clean();
		});
	});
});
