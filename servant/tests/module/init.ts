import { sanitizeDirname } from "../../src/module/init/utils";

describe("utils", () => {
	describe("sanitize dirname", () => {
		it("Test", () => {
			expect(sanitizeDirname("Test")).toBe("test");
		});

		it("This is cool module", () => {
			expect(sanitizeDirname("This is cool module")).toBe("this_is_cool_module");
		});

		it("Joho!&$module", () => {
			expect(sanitizeDirname("Joho!&$module")).toBe("joho___module");
		});

		it("@servant/data", () => {
			expect(sanitizeDirname("@servant/data")).toBe("servant.data");
		});

		it("This.module", () => {
			expect(sanitizeDirname("This.module")).toBe("this.module");
		});

		it("!&$module!&$", () => {
			expect(sanitizeDirname("!&$module!&$")).toBe("module");
		});
	});
});
