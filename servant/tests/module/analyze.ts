/* eslint-disable @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-explicit-any */

import { createProjectToDo } from "../projects";
import { init, InitData, modules, ModulesData } from "../../src";
import { analyze, DependencyType } from "../../src/module";

describe("servant modules", () => {
	describe("analyze", () => {
		async function prepare1(): Promise<[ModulesData, InitData, () => void]> {
			const clean = createProjectToDo();
			const initData = await init("/projects/project");
			const md = await modules(initData, {});

			return [md, initData, clean];
		}

		it("simple analyze call with todo project on todo module", async () => {
			const [md, init, clean] = await prepare1();
			const graph = md.graph!;

			const data = await analyze(init.packageJson!, graph, graph.modules["todo"]);
			expect(data.command).toBe("analyze");
			expect(data.module).toBe("todo");

			expect(data.data!.name).toBe("todo");
			expect(data.data!.type).toEqual([DependencyType.Internal]);
			expect(data.data!.missing).toEqual(false);
			expect(data.data!.versions.ranges).toEqual(["0.1.0"]);
			expect(data.data!.versions.intersects).toEqual(true);
			expect(data.data!.sizes.bundle).toEqual(1209);
			expect(data.data!.sizes.sources).toEqual(0);
			expect(data.data!.sizes.tests).toEqual(0);
			expect(data.data!.sizes.installation).toEqual(1209);

			await clean();
		});

		it("simple analyze call with todo project on todo_core module", async () => {
			const [md, init, clean] = await prepare1();
			const graph = md.graph!;

			const data = await analyze(init.packageJson!, graph, graph.modules["todo-core"]);
			expect(data.command).toBe("analyze");
			expect(data.module).toBe("todo-core");

			expect(data.data!.name).toBe("todo-core");
			expect(data.data!.type).toEqual([DependencyType.Internal]);
			expect(data.data!.missing).toEqual(false);
			expect(data.data!.versions.ranges).toEqual(["0.1.0"]);
			expect(data.data!.versions.intersects).toEqual(true);
			expect(data.data!.sizes.bundle).toEqual(530);
			expect(data.data!.sizes.sources).toEqual(137);
			expect(data.data!.sizes.tests).toEqual(0);
			expect(data.data!.sizes.installation).toEqual(530);

			expect(data.data!.lines.sources).toEqual({
				css: 0,
				sass: 0,
				javascript: 0,
				less: 5,
				typescript: 3,
			});
			expect(data.data!.lines.tests).toEqual({
				css: 0,
				sass: 0,
				javascript: 0,
				less: 0,
				typescript: 0,
			});

			await clean();
		});
	});
});
