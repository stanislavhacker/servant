/* eslint-disable @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-explicit-any */
import "jasmine";
import { Path } from "@servant/servant-files";
import { resolveModule } from "../../src/resolver";

import * as path from "path";

describe("servant.resolver", () => {
	describe("resolveModule", () => {
		it("resolve existing module 'glob' package.json as absolute", () => {
			const data = resolveModule(__dirname, "glob", "package.json", true);
			expect(Path.normalize(data.path)).toContain("node_modules/glob/package.json");
			expect(path.isAbsolute(data.path)).toBe(true);
			expect(data.err).toBe(null);
		});

		it("resolve existing module 'glob' package.json as relative", () => {
			const data = resolveModule(__dirname, "glob", "package.json");
			expect(Path.normalize(data.path)).toBe("../../node_modules/glob/package.json");
			expect(path.isAbsolute(data.path)).toBe(false);
			expect(data.err).toBe(null);
		});

		it("resolve existing types module '@types/node' package.json as absolute", () => {
			const data = resolveModule(__dirname, "@types/node", "package.json", true);
			expect(Path.normalize(data.path)).toContain("node_modules/@types/node/package.json");
			expect(path.isAbsolute(data.path)).toBe(true);
			expect(data.err).toBe(null);
		});

		it("resolve existing types module '@types/node' package.json as relative", () => {
			const data = resolveModule(__dirname, "@types/node", "package.json", false);
			expect(Path.normalize(data.path)).toContain(
				"../../node_modules/@types/node/package.json"
			);
			expect(path.isAbsolute(data.path)).toBe(false);
			expect(data.err).toBe(null);
		});

		it("do not resolve non-existing module 'AAA'", () => {
			const data = resolveModule(__dirname, "AAA", "package.json");
			expect(Path.normalize(data.path)).toBe("");
			expect(data.err).not.toBe(null);
			expect(data.err!.message).toBe(
				"Can not found module for AAA! Something go wrong with Servant :("
			);
		});
	});
});
