/* eslint-disable @typescript-eslint/no-non-null-assertion,@typescript-eslint/no-explicit-any */
import "jasmine";
import { Servant, InitData } from "../../src";
import { createIssue } from "../../src/module";

import { issues, analyze } from "../../src/reports";
import { createProjectToDo } from "../projects";

import * as fs from "fs";

describe("servant.reports", () => {
	describe("issues", () => {
		let clean;
		let initData: InitData;

		beforeAll(async () => {
			clean = createProjectToDo();
			initData = await Servant().init("/projects/project");
		});

		it("name", () => {
			expect(issues.outputs).toEqual(["issues.txt"]);
		});

		it("write data into file", async () => {
			const path = await issues.run(initData.servantJson!, {
				"#3": createIssue("#3", "http://issues.gitlab.com/issue/3"),
				"#2": createIssue("#2", "http://issues.gitlab.com/issue/2"),
			});

			const content = fs.readFileSync(path[0]).toString();
			expect(content).toBe(
				"ID\tP\tF\tE\tP\tURL\r\n#3\t0\t0\t0\t0\thttp://issues.gitlab.com/issue/3\r\n#2\t0\t0\t0\t0\thttp://issues.gitlab.com/issue/2"
			);
		});

		afterAll(() => {
			clean();
		});
	});

	describe("analyze", () => {
		let clean;
		let initData: InitData;
		let modulesData;

		beforeAll(async () => {
			clean = createProjectToDo();
			initData = await Servant().init("/projects/project");
			modulesData = await Servant().modules(initData);
		});

		it("name", () => {
			expect(analyze.outputs).toEqual(["analyze.json", "analyze.xml"]);
		});

		it("write data into file", async () => {
			const analyzeData = await Servant().command(initData, modulesData, "analyze", {});
			const path = await analyze.run(initData.servantJson!, analyzeData.data);

			const content1 = fs.readFileSync(path[0]).toString();
			expect(content1).toBe(
				`{
  "dependencies": {
    "all": []
  },
  "missing": []
}`
			);
			const content2 = fs.readFileSync(path[1]).toString();
			expect(content2).toBe(
				`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<root>
  <dependencies/>
</root>`
			);
		});

		afterAll(() => {
			clean();
		});
	});
});
