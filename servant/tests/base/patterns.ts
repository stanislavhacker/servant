import "jasmine";
import { Path } from "@servant/servant-files";
import { ServantJson } from "@servant/servant-data";

import {
	loadCleaned,
	loadPatterns,
	loadResources,
	loadWatched,
	loadValidated,
	loadPrettify,
	loadModuleDirectories,
} from "../../src/patterns";
import { createProjectToDo } from "../projects";

describe("servant.patterns", () => {
	describe("globs operations", () => {
		it("normalize path", () => {
			expect(Path.normalize("c:\\test\\file.*")).toBe("c:/test/file.*");
		});

		it("do not change glob", () => {
			expect(Path.normalize("c:/test/file.*")).toBe("c:/test/file.*");
			expect(Path.normalize("")).toBe("");
		});
	});

	describe("pattern loading", () => {
		let clean;

		beforeAll(() => {
			clean = createProjectToDo();
		});

		it("load all js from /projects/project", async () => {
			const js = await loadPatterns("/projects/", ["project"], "*.js");

			expect(js.length).toBe(1);
			expect(js[0]).toContain("projects/project/index.js");
		});

		it("load all ts from /projects/project/todo_core/src and /projects/project/todo_core/tests", async () => {
			const ts = await loadPatterns(
				"/projects/project/todo_core/",
				["src", "tests"],
				"**/*.ts"
			);

			expect(ts.length).toBe(3);
			expect(ts[0]).toContain("projects/project/todo_core/src/index.d.ts");
			expect(ts[1]).toContain("projects/project/todo_core/src/index.ts");
			expect(ts[2]).toContain("projects/project/todo_core/tests/main/test.spec.ts");
		});

		it("load all txt from /users", async () => {
			const txts = await loadPatterns("/users/", ["**/*.txt"]);

			expect(txts.length).toBe(0);
		});

		afterAll(() => {
			clean();
		});
	});

	describe("resources loading", () => {
		let clean;

		beforeAll(() => {
			clean = createProjectToDo();
		});

		it("load all txt from /project", async () => {
			const resources = await loadResources("/projects/", ["**/*.txt"]);

			expect(resources.length).toBe(1);
			expect(resources[0]).toContain("projects/project/description.txt");
		});

		it("load all txt from /project + npm path load", async () => {
			const data = await loadResources("/projects/", [
				"**/*.txt",
				"npm:@servant/servant-data//README.md",
			]);
			expect(data.length).toEqual(2);
			expect(data[0]).toContain("projects/project/description.txt");
			expect(data[1]).not.toBe("npm:@servant/servant-data//README.md");
		});

		it("load all txt from /project + npm wrong path load", async () => {
			const data = await loadResources("/projects/", ["**/*.txt", "npm:AAA//README.md"]);
			expect(data.length).toEqual(2);
			expect(data[0]).toContain("projects/project/description.txt");
			expect(data[1]).toBe("npm:AAA//README.md");
		});

		afterAll(() => {
			clean();
		});
	});

	describe("watched loading", () => {
		describe("without extensions", () => {
			it("with simple files, remove d.ts and js.map files", () => {
				const files = loadWatched([
					"src/data/index.d.ts",
					"src/data/index.js",
					"src/data/index.js.map",
				]);

				expect(files).toEqual(["src/data/index.js"]);
			});

			it("with simple files, remove js and css files if necessary", () => {
				const files = loadWatched([
					"src/data/index.d.ts",
					"src/data/index.js",
					"src/data/index.js.map",
					"src/data/index.ts",
					"src/data/a.js",
					"src/data/index.less",
					"src/data/index.css",
					"src/data/a.css",
				]);

				expect(files).toEqual([
					"src/data/index.ts",
					"src/data/a.js",
					"src/data/index.less",
					"src/data/a.css",
				]);
			});

			it("with simple files, remove js if there is tsx", () => {
				const files = loadWatched(["src/data/index.js", "src/data/index.tsx"]);

				expect(files).toEqual(["src/data/index.tsx"]);
			});

			it("with simple files, remove jsx if there is ts", () => {
				const files = loadWatched(["src/data/index.jsx", "src/data/index.ts"]);

				expect(files).toEqual(["src/data/index.ts"]);
			});
		});

		describe("with extensions", () => {
			it("with simple files, remove d.ts and js.map files and extensions", () => {
				const files = loadWatched(
					[
						"src/data/index.d.ts",
						"src/data/index.js",
						"src/data/index.js.map",
						"src/data/index.temp",
						"src/data/index.y",
					],
					["temp", "y"]
				);

				expect(files).toEqual(["src/data/index.js"]);
			});
		});
	});

	describe("clean loading", () => {
		it("with simple files, on js.map will be clean", () => {
			const files = loadCleaned([
				"src/data/index.d.ts",
				"src/data/index.js",
				"src/data/index.js.map",
			]);

			expect(files).toEqual(["src/data/index.js.map"]);
		});

		it("with simple files, do not clean alone css and js files", () => {
			const files = loadCleaned([
				"src/data/index.d.ts",
				"src/data/index.js",
				"src/data/index.js.map",
				"src/data/index.ts",
				"src/data/a.js",
				"src/data/index.less",
				"src/data/index.css",
				"src/data/a.css",
			]);

			expect(files).toEqual([
				"src/data/index.d.ts",
				"src/data/index.js",
				"src/data/index.js.map",
				"src/data/index.css",
			]);
		});

		it("with simple files, remove js if there is tsx", () => {
			const files = loadCleaned(["src/data/index.js", "src/data/index.tsx"]);

			expect(files).toEqual(["src/data/index.js"]);
		});

		it("with simple files, remove jsx if there is ts", () => {
			const files = loadCleaned(["src/data/index.jsx", "src/data/index.ts"]);

			expect(files).toEqual(["src/data/index.jsx"]);
		});
	});

	describe("validate loading", () => {
		const defaultExtensions = [];

		it("only .tsx is validated", () => {
			const files = loadValidated(
				[
					"src/data/index.tsx",
					"src/data/index.d.ts",
					"src/data/index.js",
					"src/data/index.js.map",
				],
				defaultExtensions
			);

			expect(files).toEqual(["src/data/index.tsx"]);
		});

		it("only .ts is validated", () => {
			const files = loadValidated(
				[
					"src/data/index.ts",
					"src/data/index.d.ts",
					"src/data/index.js",
					"src/data/index.js.map",
				],
				defaultExtensions
			);

			expect(files).toEqual(["src/data/index.ts"]);
		});

		it("only .d.ts and js file is validated", () => {
			const files = loadValidated(
				["src/data/index.d.ts", "src/data/index.js", "src/data/index.js.map"],
				defaultExtensions
			);

			expect(files).toEqual(["src/data/index.d.ts", "src/data/index.js"]);
		});

		it("only .js file is validated", () => {
			const files = loadValidated(
				["src/data/index.js", "src/data/index.js.map"],
				defaultExtensions
			);

			expect(files).toEqual(["src/data/index.js"]);
		});

		it("only relevant files are validated", () => {
			const files = loadValidated(
				[
					"src/data/index.ts",
					"src/data/index.d.ts",
					"src/data/index.js",
					"src/data/index.js.map",
					"src/data/test.js",
					"src/data/mock.ts",
				],
				defaultExtensions
			);

			expect(files).toEqual(["src/data/index.ts", "src/data/test.js", "src/data/mock.ts"]);
		});

		it("only known extensions", () => {
			const files = loadValidated(
				["src/data/index.ts", "src/data/index.mjs", "src/data/index.md"],
				defaultExtensions
			);

			expect(files).toEqual(["src/data/index.ts", "src/data/index.mjs"]);
		});

		it("only known and provided extensions", () => {
			const files = loadValidated(
				["src/data/index.ts", "src/data/index.mjs", "src/data/index.md"],
				["mjs", "md"]
			);

			expect(files).toEqual(["src/data/index.ts", "src/data/index.mjs", "src/data/index.md"]);
		});
	});

	describe("prettify loading", () => {
		it("no files at all", () => {
			const files = loadPrettify(
				[
					"src/data/index.tsx",
					"src/data/index.d.ts",
					"src/data/index.js",
					"src/data/index.js.map",
				],
				["md"]
			);

			expect(files).toEqual([]);
		});

		it("defined extensions only", () => {
			const files = loadPrettify(
				[
					"src/data/index.tsx",
					"src/data/index.d.ts",
					"src/data/index.js",
					"src/data/index.js.map",
					"src/data/index.md",
				],
				["md"]
			);

			expect(files).toEqual(["src/data/index.md"]);
		});

		it("all files", () => {
			const files = loadPrettify(
				[
					"src/data/index.tsx",
					"src/data/index.d.ts",
					"src/data/index.js",
					"src/data/index.js.map",
					"src/data/index.md",
				],
				[]
			);

			expect(files).toEqual([
				"src/data/index.tsx",
				"src/data/index.d.ts",
				"src/data/index.js",
				"src/data/index.js.map",
				"src/data/index.md",
			]);
		});
	});

	describe("module directories loading", () => {
		let servantJson;

		beforeEach(() => {
			servantJson = {
				cwd: "/path/to/project",
				path: `/path/to/project/${ServantJson.SERVANT_JSON}`,
				content: ServantJson.create("test"),
			};
		});

		it("no files at all", () => {
			const files = loadModuleDirectories([
				"/path/to/project/node_modules",
				"/path/to/project/dist",
				"/path/to/project/.temp",
				"/path/to/project/generators",
			]);

			expect(files).toEqual([]);
		});

		it("empty servant json", () => {
			const files = loadModuleDirectories(
				[
					"/path/to/project/node_modules",
					"/path/to/project/dist",
					"/path/to/project/.temp",
					"/path/to/project/generators",
				],
				servantJson
			);

			expect(files).toEqual([
				"/path/to/project/dist",
				"/path/to/project/.temp",
				"/path/to/project/generators",
			]);
		});

		it("filter out node_modules", () => {
			const files = loadModuleDirectories(
				[
					"/path/to/project/node_modules",
					"/path/to/project/dist",
					"/path/to/project/.temp",
					"/path/to/project/generators",
				],
				servantJson
			);

			expect(files).toEqual([
				"/path/to/project/dist",
				"/path/to/project/.temp",
				"/path/to/project/generators",
			]);
		});

		it("filter out node_modules and output", () => {
			servantJson.content.output = { directory: "./dist" };
			const files = loadModuleDirectories(
				[
					"/path/to/project/node_modules",
					"/path/to/project/dist",
					"/path/to/project/.temp",
					"/path/to/project/generators",
				],
				servantJson
			);

			expect(files).toEqual(["/path/to/project/.temp", "/path/to/project/generators"]);
		});

		it("filter out node_modules, output, temp and generators", () => {
			servantJson.content.output = { directory: "./dist" };
			servantJson.content.temp = "./.temp";
			servantJson.content.generators = "./generators";
			const files = loadModuleDirectories(
				[
					"/path/to/project/node_modules",
					"/path/to/project/dist",
					"/path/to/project/.temp",
					"/path/to/project/generators",
				],
				servantJson
			);

			expect(files).toEqual([]);
		});
	});
});
