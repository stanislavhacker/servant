Config 'servant.json'
======
Quick references: **[Command line][4]**, **[Node API][3]**

Main configuration file for Servant. It's used for modules and package definition. In this 
 file there is described some properties that Servant needs to known. Lots of properties are 
 not required, because Servant can derive it from `package.json`, but some needs to be defined 
 to proper Servant work.

### There is example of the smallest config file

**Module `servant.json`**
```
{
  "output": {
    "directory": "./dist",
    "filename": "servant.js"
  },
  "entry": "./src/index",
  "target": "node"
}
``` 

Of course, this config file can be even smaller for subproject, because **config files are 
 merged from parent to child module**. So you can define one definition for all modules and 
 then override only changed.

**Main `servant.json`**
```
{
  "package": "servant",
  "target": "node",
  "modules": [
    "servant*/",
  ],
  "entry": false,
  "output": {
    "directory": "./dist"
  },
  "src": [
    "src/**/*"
  ],
  "tests": [
    "tests/**/*"
  ],
}
```

**Submodule `servant.json`**
```
{
  "output": {
    "filename": "servant.js"
  },
  "entry": "./src/index"
}
``` 

For all modules all paths and structure of project will be same. Same `src` and `tests` paths, 
 same target and some output folder. Only bundled name and entry point is defined for every module. 
 And that's all.

But, there is more to configure in `servant.json`. Structure and all properties of `servant.json`
 are described below.
 
### Structure of servant.json

This is an example structure of servant json. It's used for loading data about project structure and
 automatically generation of webpack config and jasmine config.  

```json
      {
        "package": "package-name",
        "modules": ["module-*/"],
        "output": {
          "directory": "./dist",
          "filename": "package-name-dist.js",
          "resolve": ["js"]
        },
        "webpage": {
          "title": "Webpage Title",
          "favicon": "./assets/favicon.ico",
          "publicPath": "./public"
        },
        "entry": "./index",
        "test": "tests/index",
        "src": ["src/**/*"],
        "tests": ["test/**/*", "tests/**/*"],
        "resolve": ["ts", "js", "tsx", "jsx"],
        "resources": ["fonts/*", "images/*", "npm:react//umd/react.production.min.js"],
        "clean": ["js.map"],
        "prettify": {
            "sources": ["doc/**/*", "fixtures/**/*"],
            "extensions": ["md", "json"]
        },
        "generators": "./generators",
        "temp": ".temp",
        "libraries": {
          "simple-git/promise": "simple-git/promise"
        },
        "mappings": {
          "react": "./umd/react.production.min.js",
          "@resources/fonts": false
        },
        "shared": {
         "react": "../node_modules/react",
         "ink": "../node_modules/ink"
        },
        "target": "node-cli",
        "publish": {
          "increment": "patch",
          "access": "public",
          "stripDev": true,
          "commitMessage": "Incrementing version by '$increment' number. New version is '$version'."
        },
        "registry": "https://registry.npmjs.org",
        "issues": {
          "#([0-9]{1,})": "http://issue-tracker.rg/id/$0"
        },
        "testing": {
            "browsers": ["Webkit", "Firefox", "Chromium"],
            "devices": ["iPad Mini", "iPhone 6"]
        },
        "server": {
          "entries": {
             "web-package-name": {
               "title": "Debug Server for package-name",
               "template": "./web-package-name/template.html"
             },
             "node-package-name": {
               "command": {
                  "file": "./node-package-name/dist/package-name-dist.js",
                  "args": ["--port", "3000"]
               },
               "shared": {
                  "./static/public": "web-package-name"
               }
             }
          },
          "css": ["http://fonts.googleapis.com/css?family=Open+Sans", "./globals/styles.css"],
          "js": ["https://code.highcharts.com/highcharts.js"]
        }
      }
```

 > **INFO**
 >
 > There is available JSON schema mapping for servant.json. If you install package is available in 
 > `/dist/servantJson.schema.json` or in gitlab.
 > JSON schema: [servant/src/files/servantJson.schema.json][schema]
      
#### `"package": string`

Name of whole package. Is used only for printing this name into Servant console. If field is not provided, it's
 load from [`package.json`][1] "name" field.
 
 **Default:** [`package.json`][1] `name` field or ""
 
 **Examples:** 
  - `"package": "package-name"`
  - `"package": "MySuperProject"`
  
#### `"modules": Array<string>`

Array of glob patterns for loading and determining modules. It's recommended to use prefixes for you inner modules
 directories, so you can distinguish between modules directories and helpers and tools directories. FOr example 
 you use for main modules `module.name` and for views modules `view.name`.
 
  **Default:** `["./"]`
  
  **Examples:** 
   - `"modules": ["module-*/"]`
   - `"modules": ["module-*/", "views-*/", "utils-*/"]`
   
#### `"src": Array<string>`

Array of glob patterns for determining src files of module. If it's specified in main servant.json it's
 used as a default for every submodule. You can override it in submodule servant.json. Doing this you can
 have different structures for each module. But this approach is not recommended.  
 
  **Default:** `["src/**/*"]` 
  
  **Examples:** 
   - `"src": ["src/**/*"]`
   - `"src": ["src/**/*", "utils/**/*"]`
   
#### `"tests": Array<string>`

Array of glob patterns for determining tests files of module. If it's specified in main servant.json it's
 used as a default for every submodule. You can override it in submodule servant.json. Doing this you can
 have different structures for each module. But this approach is not recommended.  
 
  **Default:** `["tests/**/*"]` 
  
  **Examples:** 
   - `"tests": ["tests/**/*"]`
   - `"tests": ["tests/**/*", "test/**/*"]`
   
#### `"clean": Array<string>`

Array of all extension that are removed by `clean` command. By default `clean` command clearing you dist files
 defined in [`package.json`][1] "files" field and also all files under temporary directory. But it can additionally 
 clean some other files with specified extensions. This is used for cleaning autogenerated files for example. You don't
 need specify autogenerated files for ts, tsx, less or scss files. Servant automatically clean all js or jsx files for
 typescripts files and css files for less or scss files. 
 
 > **Caveats**
 >
 > Clean property id also used by tests command for not including files that are not part of project! So it's
 > necessary to have clean property properly set up!
 
  **Default:** `[]` 
  
  **Examples:** 
   - `"clean": ["js.stats", "log", "js.map"]`
   - `"clean": ["tmp"]`


#### `"prettify.sources": Array<string>`

Array of glob patterns for determining directories and files for prettier validation by `validate` command. By default 
 `validate` command prettify src and tests files defined in `servant.json` "src" and "tests" field. This glob can be
 extended by adding come folders here. For example if it's necessary to validate documentation or exampel files.
 
  **Default:** `[]` 
  
  **Examples:** 
   - `"prettify": { "sources": ["docs/**/*", "fixtures/**/*"] }`

#### `"prettify.extensions": Array<string>`

Array of all extension that are prettified and validated by `validate` command. By default `validate` command prettify 
 src and tests files defined in `servant.json` "src" and "tests" fields. But it can additionally validate some other 
 files with specified extensions. This is used for validating another files than sources. For example, it can validate
 documentation markdown files, examples json files and others. Can prettify all files supported by `prettier`.
 
  **Default:** `[]` 
  
  **Examples:** 
   - `"prettify": { "extensions": ["md", "json"] }`
   
#### `"temp": string`

Temporary dirname that is removed by `clean` command. By default `clean` command clearing you dist files
 defined in [`package.json`][1] "files" field and also all files under temporary directory. Some commands can create
 files under this temp directory. You can also use this dir to store some temporary files here. But don't forget that
 will be deleted by `clean` command!
 
  **Default:** `".temp"` 
  
  **Examples:** 
   - `"temp": ".temp"`
   - `"temp": "tmp"`
   
#### `"generators": string`

Dirname that is used for storing local generators (generators that are not installed via **npm**). This
 directory is used by `--generate` flag.
 
  **Default:** `"./generators"` 
  
  **Examples:** 
   - `"generators": "./generators"`
   - `"generators": "../generator"`
   
#### `"output.directory": string`

Directory for final build output. There will be saved all resources and bundled files. This is used by `build` command.

 **Default:** `"./dist"`
 
 **Examples:** 
   - `"output": { "directory": "./dist" }`
   - `"output": { "directory": "./bundle" }`

#### `"output.filename": string`

Name of bundled file. This is used by `build` command.

 **Default:** `"index.js"`
 
 **Examples:** 
   - `"output": { "filename": "./index.js" }`
   
#### `"output.resolve": Array<string>`

 Determine all extensions than will be resolved by Servant if he tries to load data from dist folder.
  For example if you copying resources that are necessary for loading into tests page, used this property
  to force this files. By default Servant will try to resolve this files and load into test page or into
  development mode page. By default you don't need change this, so ommit this property in 
  your servant.json. 

 **Default:** `["js", "css"]`
 
 **Examples:** 
   - `"output": { "resolve": ["js", "css"] }`

#### `"webpage.title": string`

Name of page that will be injected into title meta tag in html. This is a production name and will be available
 in final web page bundle, that is used to server by backend server.

**Default:** `""`

**Examples:**
- `"webpage": { "title": "Application" }`

#### `"webpage.favicon": string`

Relative path to asset for favicon. This icon will be served and used by browsers. Favicon is included
 in web page that is generated by Servant. This is a production ico and will be available
 in final web page bundle, that is used to server by backend server.

**Default:** `""`

**Examples:**
- `"webpage": { "favicon": "./assets/favicon.ico" }`

#### `"webpage.publicPath": string`

Path where web page will be served on server side. This is a typically root where page is served by server
 if not in root of domain. Normally can be used default `"./"`, that is work for most of the cases.

**Default:** `"./"`

**Examples:**
- `"webpage": { "publicPath": "./public" }`

#### `"entry": string | boolean`

Main entry point into your module. It can be `*.js` or `*.ts` file or file without extension.
 If it's specified in main servant.json it's used as a default for every submodule. You can override 
 it in submodule servant.json. Doing this you can have different structures for each module. This 
 is used by `build` command.
 
**Value `false`**

You can provided `false` value for entry. This mean, that module is not intended for build and
 will not produce any output do dist folder.
 
**Resolving `.css` files**

Servant can resolve `.css` **two ways**. 

 1. First way is to **import `.css` file** in your `.ts` file. It can be done like `import * as css from "./my.css";`. Because Servant
 have css to d.ts converter it can generate d.ts from css and your IDE can hint class names for you. Servant will 
 be used css file as normal module and can build bundled css file into output folder.
 1. Second way is add file beside your entry point file, with same name, but with extension `.css`, `.scss` or `.less`. Servant will load this file
  and copy result file into output folder. If this file is `.less` or `.scss`, Servant build it before copying.
  
  _For example_: If you have `"./src/index"` as  entry, create `"./src/index.less"` file, and it's done!

  _For example_: If you have `"./src/index"` as  entry, create `"./src/index.scss"` file, and it's done!

 **Default:** `"./src/index"`
 
 **Examples:** 
   - `"output": "./src/index"`
   - `"output": "./src/entry.js"`
   - `"output": false`
   
#### `"test": string`

Main entry point for tests into your module. It's used only for module that are build for web 
 browsers or for universal use It can be `*.js` or `*.ts` file or file without extension.
 If it's specified in main servant.json it's used as a default for every submodule. You can override 
 it in submodule servant.json. Doing this you can have different structures for each module. This 
 is used by `build` command.
 
 **Default:** `"./tests/index"`
 
 **Examples:** 
   - `"output": "./tests/index"`
   - `"output": "./tests/index.ts"` 
   
#### `"resolve": Array<string>`

Determine all extensions than must be resolved by Servant. It's similar to webpack resolve property.
 By default you don't need change this, so ommit this property in your servant.json. 

 **Default:** `["ts", "js", "tsx", "jsx"]`
 
 **Examples:** 
   - `"output": ["ts", "js", "tsx", "jsx"]`

#### `"resources": Array<string>`

Array of glob patterns for resources files of module. If it's specified in main servant.json it's
 used as a default for every submodule. You can override it in submodule servant.json. Doing this you can
 have different structures for each module. Every module has different sets of resources so it must be 
 defined in every module separately.
 
Resources can be also defined as path into another installed npm module. For example you want to
 copy documentation for submodule into you dist package. Npm can install modules into different folders
 so you don't known where the documentation really is. In this case you can use path 
 **`npm:`**_`my-module`_**`//`**_`docs/doc.md`_ . Where `my-module` is npm module name and rest is
 path inside module.
 
  **Default:** `[]` 
  
  **Examples:** 
   - `"resources": ["fonts/*.ttf"]`
   - `"resources": ["fonts/*", "images/*.jpg"]`
   - `"resources": ["npm:react//umd/react.production.min.js"]`

#### `"watch": Array<string>`

Array of glob patterns for watched files in module. This property is used by `--changed` flag. All files in
 this array are watched and if there are some changes in in, Servant can rebuild a module. You can watch
 node_modules directory, some hot folder for resources and so on. 
 
  **Default:** `[]` 
  
  **Examples:** 
   - `"watch": ["node_modules/**/*"]`
   - `"watch": ["graphics/**/*"]`

#### `"libraries": Map<string, string>`

Directory of all libraries that will not be build into output file. By default every dependency and every node module
 is not bundled into output. But if there are some special cases (for example: you use library "simple-git" but you
 need import "simple-git/promise") Servant is not able to determine library to not include. So if you wan't this library
 as build into output, you must specify in there.
 
  **Default:** `{}` 
  
  **Examples:** 
   - `"libraries": {"simplge-git/promies": "simplge-git/promies"}`
   
#### `"mappings": { [key: string]: string | false }`

There are defined all links on external libraries. For example if you use react as external library (that
 is not part of your build) you can defined link where library sources are located. Servant known most uses
 libraries. Now only **react** and **react-dom** are automatically resolved. 
 
**Path for file is relative inside module! Path for module is automatically resolved by Servant.** If you are
 a developer and you want to add new module into automatically detections, add path inside file 
 [servant.json.module.ts][modules] and create merge request.
 
**`false` value** can be used as value in mapping. Servant do not try to include this modules and skip it.
 This is good for resource only modules (fonts, images and so on).
 
  **Default:** `{}` 
  
  **Examples:** 
   - `"mappings": {"react": "./dist/react.production.js"}`
   - `"mappings": {"react-dom": "./dist/react-dom.production.js"}`
   - `"mappings": {"@fortawsome/fontawsome": false}`

#### `"shared": { [key: string]: string }`

There are defined all shared libraries. That is for case that you need use
some libraries that's need to be included and loaded in module or app
only once time. For example "react" needs to be loaded only once. For web 
developing is ok, because build ensure that react is loaded only once. But
for example nodejs app can load react multiple times. So if you want to
share react between modules, you can define it here. There is key that is
module name and value is path to library. Path is relative to module.
 
  **Default:** `{}` 
  
  **Examples:** 
   - `"mappings": {"react": "../node_modules/react"}`

#### `"target": "web" | "web_page" | "node" | "node-cli"`

This is setting of output target. You must specify for what is module used. 

##### `web` target
If you set up  "web" as target, Servant will try to include modules like "path", "fs" and all others internal node js modules. 
 You need to install appropriate browserify or browser polyfill to use it in browser. For this reasons is recommended to use
 only modules and packages, that are compatible with browser. This target also bundle all files into one output file and can build
 also all dependencies!
 
##### `web_page` target
If you set up  "web_page" as target, Servant will behave similar as "web" target, but also will generate html file and all
 necessary assets nad files into a output folder, that can be used to serve by backend. This target use property `webpage`
 in `servant.json`.
 
##### `node` target
If you set up  "node" as target, Servant will not include modules like "path", "fs" and all others internal node js modules. 
 This cause that **if you use this module in project, module will not be bundled into output file** because we expected that
 this module is inside nodejs.
 
##### `node-cli` target
This setup is similar like node, but is expected, that this module will be used as command line application,
 
  **Default:** `"web"` 
  
  **Examples:** 
   - `"target": "web"`
   - `"target": "node"`

#### `"publish.increment": "major" | "premajor" | "minor" | "preminor" | "patch" | "prepatch" | "prerelease" | "none"`

This is increment type used by `publish` command. Servant will increase semver version fo your module by specify type. If you
 don't want to increment version automatically, set "none" as increment.
 
  **Default:** `"patch"` 
  
  **Examples:** 
   - `"publish": {"increment": "none"}`
   - `"publish": {"increment": "minor"}`

#### `"publish.access": "public" | "restricted"`

Define if you publish public or restricted package. Is is used by `publish` command and if you want to use "restricted"
 on default registry, you need **paid account**!
 
  **Default:** `"public"` 
  
  **Examples:** 
   - `"publish": {"access": "public"}`
   - `"publish": {"access": "restricted"}`

#### `"publish.stripDev": boolean`

Define if you want to delete `devDependencies` from published `package.json`. Servant will delete this property and make package without development dependencies.
 
  **Default:** `false` 
  
  **Examples:** 
   - `"publish": {"stripDev": true}`

#### `"publish.commitMessage": string`

Commit message that will be attached to commit if you use `--commit` flag in `publish` command. For every module
 Servant make single commit with this message. You can use **variables** that will be replaced in final message
 
##### Variables:
 - `$version` - Will be replaced for current module version
 - `$increment` - Will be replaced for current increment style defined in `increment` property in servant.json
 
  **Default:** `Incrementing version by '$increment' number. New version is '$version'.` 
  
  **Examples:** 
   - `"publish": {"commitMessage": "New version $version."}`
   
#### `"registry": string | Object`

You can setup your registry if you do not use default "registry.npmjs.org". Registry 
 is used by `publish`, `install` and `update`  commands.

  **Default:** `""` 
  
  **Examples:** 
   - `"registry": "/local/registry/npm"`
   - `"registry": "http://www.my.registry.com"`

  **Example with more scopes:**
   - `"registry": { "default": "http://www.my.registry.com", "@scope": "http://scope.registry.com", "@ext": "http://ext.registry.com" }`
   
#### `"issues": Object`

Settings for bug name resolving in test name. It's used by `tests` command and is used for collecting
 all bugs that are reported in test or suit name. On end of tests task you can view the count of tested
 issues and all passed tests for it. 
 
 **Key** of object is pattern for resolving issue in tests name.
 
 **Value** if object is url for issues that are testing. This url is show in list of all tested issues
  on end. You can use $0 for whole matching string and the $1, $2, ... for groups.
 
  **Default:** `{}` 
  
  **Examples:** 
   - `"issues": { "#([0-9]{1,})": "http://issue-tracker.rg/id/$0" }`
   
#### `"testing.browsers": Array<string>`

This settings is used for define in which browsers tests will be run. You can setup here **Webkit**, **Chromium** and
 **Firefox**. All browsers can be run in headless or gui mode (`--gui` switch in command line). **If you omitted whole 
 testing property, Servant will fallback into only "Chrome" browser testing.**

 > **Caveats**
 >
 > This is special feature that use [playwright][playwright] module from [Microsoft][ms] to run tests against more
 > browsers and you need install optional module `@servant/servant-playwright` to get this working. So after install
 > Servant run `npm install @servant/servant-playwright --save-dev` to install this optional module. **Be careful 
 > because this module is big (about 300 MB to download)!**

#### `"testing.devices": Array<string>`

This settings is used for define in which device tests will be run. You can setup here more than 70 devices. Full list
 of devices can be found [on playwright devices page][devices].

 > **Caveats**
 >
 > This is special feature that use [playwright][playwright] module from [Microsoft][ms] to run tests against more
 > devices and you need install optional module `@servant/servant-playwright` to get this working. So after install
 > Servant run `npm install @servant/servant-playwright --save-dev` to install this optional module. **Be careful 
 > because this module is big (about 300 MB to download)!**
  
#### `"server": Object`

Settings for development server of Servant. There are all settings that you need if you use `--server` flag
 for running development server.
 
  **Default:** `{}` 
  
  **Examples:** 
   - `"server": {...}`

#### `"server.css": Array.<string>`

List of all externals css libraries. You can specify url or relative path.
 
  **Default:** `[]` 
  
  **Examples:** 
   - `"server.css": ["http://fonts.googleapis.com/css?family=Open+Sans", "./globals/styles.css"]`
   
#### `"server.js": Array.<string>`

List of all externals js libraries. You can specify url or relative path.
 
  **Default:** `[]` 
  
  **Examples:** 
   - `"server.js": ["http://libs.js/jquery.js", "./globals/my.js"]`
   
#### `"server.entries": { [key: string]: Entry }`

List of all entries. You define module name (defined in [`package.json`][1]) as key. Then you can define other
 properties, but these properties are not necessary for running development server. **If you not defined any entry,
 development server will no start :)**. Default port for running is 9000. For every entry that you define here,
 Servant will increment port number and create new dev server with this entry. If ports are used, Servant automatically
 increments ports and find first not used.

You can defined here web target modules and also node js target modules. If servant detect that entry is 
 a nodejs module starts process with giver file and arguments provided in entry.
 
  **Default:** `{}` 
  
  **Examples:** 
   - `"server.entry": { "package.name": {} }`
   - `"server.entry": { "package.name": {"title": "Package Name", "template": "./debug.html"} }`
   - `"server.entry": { "package.name": {"command": { "file": "./dist/server.js", "args": ["start --port 3069"] } }`
   - `"server.entry": { "package.name": {"shared": { "./public": "pacakge.web" } }`
   
##### `Entry`

Entry is used for define some entry information about module in development server.

 - `Entry.title` - Define title of development page with module.
 - `Entry.template` - Define template file with data for development server.
 - `Entry.command.file` - Define what file will be run for node process.
 - `Entry.command.args` - Define all arguments for file that will be run.
 - `Entry.shared` - Define shared folder for current module.

##### `Entry` shared settings

Servant can create symbolic links for defined modules in development mode. This is helpfully when developing server and
 client packages, and we need to server build web client into some folder in server module. In this case is possible
 to use shared folders in development server entry setting. It can look like this

```json
{
  ...
  "server": {
    "entries": {
      "web-package-name": {
        "title": "Debug Server for package-name",
        "template": "./web-package-name/template.html"
      },
      "node-package-name": {
        "command": {
          "file": "./node-package-name/dist/package-name-dist.js",
          "args": ["--port", "3000"]
        },
        "shared": {
          "./node-package-name/static/public": "web-package-name"
        }
      }
    }
  }
}
```

This is a root `servant.json` and there are 2 entries modules. One is for web "web-package-name" and second if for
 server "node-package-name". Server module has defined shared folder. This shared folder mean that into directory
 `./node-package-name/static/public` will be linked content from output folder of module "web-package-name". There
 can not be path name, only module name, Servant will automatically resolve it, because every module can have different
 output folder. After running of dev server, these shared folder was **firstly removed** and than recreated with 
 symlinked content. How server can server data from its `static/public` folder and all changes will be propagated!

> **Caveats**
>
> Properties **title** nad **template** are used for web based modules, property **command** is used for node
> and node-cli modules. Property **shared** apply on all targets.
 
##### Entry template definition

Template file is used for injecting data into development server. For now, you can inject only css styles,
 javascript files and meta tags. In example, you must defined `template` as root. Then you can use `style`, `script` 
and `meta` here. You can define more styles, scripts and meta tags in template.

 - `meta` - You can define meta tags and servant include this meta tags into dev server `index.html`
 - `body` - You can define body html content and servant include this body into dev server `index.html`
 - `style` - You can define css here. This css will be used in development.
 - `script` - You can define js content here. This script will be injected on the end of body tag.

```html
   <template>
       <meta name="author" content="John Doe" />
       <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
       <style type="text/css">
           body {
               margin: 0;
               padding: 0;
           }
       </style>
       <script type="text/javascript">
           let item = window["package-name"];
           item.render(document.body);
       </script>
       <script type="text/javascript">
           ...
       </script>
       <body><![CDATA[
           <div id="main-app"></div>
           <span id="anchor">Anchor</span>
           <div id="modal-app"></div>
       ]]>
       </body>
       <body>
           <div id="main-app">Content</div>
       </body>
   </template>
```

And thats all. This is a whole configuration of `servant.json` file. Now it will be pretty easy 
 to configure Servant as you want and customize main parts of monorepo. Config options will be 
 increased as new features will be derived and also this article will be updated.

 [playwright]: https://www.npmjs.com/package/playwright
 [ms]: https://github.com/microsoft
 [devices]: https://github.com/Microsoft/playwright/blob/master/src/deviceDescriptors.ts
 
 [1]: https://docs.npmjs.com/files/package.json
 [3]: servant.nodejs.md
 [4]: ../../servant-cli/doc/servant.clia.md
 [schema]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/src/files/servantJson.schema.json
 [modules]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-data/src/servant.json.module.ts