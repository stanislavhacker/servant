Servant NodeJs api
======
Quick references: **[Command line][4]**, **[servant.json][2]**

Servant can also be used from node js process. For this there is available simple ServantApi that can provide
 all necessary information and can be used inside your integration or another runtime.
 
### Installation and start!
 
 You need latest version of nodejs and then run 
 
  > `npm install @servant/servant --save-dev`
  
And then import with require or import statement.

 > `var servant = require("@servant/servant")`
 
or
 
 > `import * as servant from "@servant/servant"`
 > `import {Servant} from "@servant/servant"`
 
### User servant api

You can use Servant nodejs api two ways. Create object as a new "instance" or through single functions.

```typescript
//case one ...
import {Servant} from "@servant/servant";
const servant = Servant();
servant.init("/path/to/entry").then(...);

//...or
import * as s from "@servant/servant";
s.init("/path/to/entry").then(...);
```
# .init method
#### `servant.init(entry: string): Promise<`[`s.InitData`][InitData]`>`
#### `s.init(entry: string): Promise<`[`s.InitData`][InitData]`>`

Create init data on given `entry` point. This entry is path to directory with 
 servant project.
 
##### This methods return [InitData][InitData] object with following properties:
 
  - **`packageJson`**: Object with loaded package.json data 
  - **`servantJson`**: Object with loaded servant.json data
 

# .modules method
#### `servant.modules(init: `[`s.InitData`][InitData]`, params: `[`s.ModulesParams`][ModulesParams]`): Promise<`[`s.ModulesData`][ModulesData]`>`
#### `s.modules(init: `[`s.InitData`][InitData]`, params: `[`s.ModulesParams`][ModulesParams]`): Promise<`[`s.ModulesData`][ModulesData]`>`

Load modules data based on provided [init data][InitData] and [modules parameters][ModulesParams]. Parameters can change behaviour
 of loaded modules data. 
 
##### There are modules parameters that can be provided, all parameters are optional:
 
 - `only?: Array<string>`: Filter only specified modules from results. Can be good for build only some modules from
  all. This filter keep sorting of modules and also determine order for parallel build. 
 - `changed?: Array<`[`Changes.ChangeBy`][ChangeBy]`>`: If you specified this param, only modules that are change will
  be added to result. You can now add "GIT" or "FILESYSTEM" into changed array.
  
##### This methods return [ModulesData][ModulesData] object with following properties:

 - `graph`: Object with information about graph structure of a project and all necessary data for manipulating
 with Servant project.
 - `validation`: Contains data with invalid and missing dependencies. 
 
```typescript
import {Servant, Changes} from "@servant/servant";
const servant = Servant();
servant.init("/path/to/entry").then((initData) => {
	servant.modules(initData, {
		only: ["todo-view", "todo-core"],
		changed: [Changes.ChangeBy.GIT]
	}).then(...);
});
```


# .command method
#### `servant.command(init: `[`s.InitData`][InitData]`, modules: `[`s.ModulesData`][ModulesData]`, command: `[`s.Commands.Commands`][Commands]`, params: `[`s.Commands.CommandParams`][CommandsParam]`): Promise<`[`s.Commands.CommandResult`][CommandResult]`>`
#### `s.command(init: `[`s.InitData`][InitData]`, modules: `[`s.ModulesData`][ModulesData]`, command: `[`s.Commands.Commands`][Commands]`, params: `[`s.Commands.CommandParams`][CommandsParam]`): Promise<`[`s.Commands.CommandResult`][CommandResult]`>`

Run specified [command][Commands] against provided [modules data][ModulesData] with additional [commands parameters][CommandsParam].

##### These parameters are listed in bellow, all parameters are optional:
 
 - **transpile**: Used by `build` command, only transpile code, do not make typechecking
 - **production**: Used by `build` command, build with production mode (no source maps, minification)
 - **freeze**: Used by `publish` command, freeze all version in package.json (make exact versions) 
 - **tag**: Used by `publich` command, same as npm publish --tag
 - **prune**: Used by `clean` command, remove also all node_modules and package-lock.json files
 - **gui**: Used by `tests` command, run web browsers tests in gui mode (open browser with dev tools)
 - **fix**: Used by `validate` command, used for autofix code that's breaks validation rules
 - **dependencies**: Used by `tests` command, add also all dependant modules
 - **modules**: Used by `unify` command, map of all libraries and desired version for unify across project
 - **latest**: Used by `unify` command, used for increment not unified packages versions to latest on that is used in project
 - **progress**: Callback that is called after some progress is done. **Used by all commands**
 - **browsers**: Used by `tests` command, list of browsers in which tests will run
 - **devices**: Used by `tests` command, list of devices in which tests will run
 - **commit**: Used by `publich` command, can creating commit message with new version and changed package.json files
 - **noaudit**: Used by `install` command, skip auditing dependencies

```typescript
import {Servant} from "@servant/servant";
const servant = Servant();
servant.init("/path/to/entry").then((initData) => {
	servant.modules(initData, {}).then((modulesData) => {
		servant.command(initData, modulesData, "build", {
			transpile: false,
			production: true
		}).then(...)
	});
});
```


# .create method
#### `servant.create(packageJson: `[`s.PackageJson.PackageJsonInfo`][PackageJson]`, entry: string, initParams: `[`s.Module.InitParams`][InitParams]`): Promise<`[`s.Commands.CommandResult`][CommandResult]`>`
#### `s.create(packageJson: `[`s.PackageJson.PackageJsonInfo`][PackageJson]`, entry: string, initParams: `[`s.Module.InitParams`][InitParams]`): Promise<`[`s.Commands.CommandResult`][CommandResult]`>`

Init and create servant repo app with predefined files and directory structure. It's create new project in folder
 defined in `entry`. Create new project based on [`init params`][InitParams].
 
##### These parameters are listed in bellow, all parameters except `type` are optional:
 
 - **type**: Set type of init, can be `main` or `submodule`
 - **language**: `string` language of module. can by `ts` or `js`, _default_: `ts`
 
##### These properties are used for [`servant.json`][2]:
 
 - **moduleDirectory**: Only if you set up type as `submodule`, create directory for submodule
 with this name, _default_: `""`
 - **modules**: Only if you set up type as `main`, init servant.json with this in modules property, _default_: `./`
 - **outputDirectory**: Directory for final build output, _default_: `./dist`
 - **outputFilename**: Name of bundled file, _default_: `index.js`
 - **entry**: Main entry point into your module, _default_: `./src/index`
 - **src**: Array of glob patterns for determining src files of module, _default_: `["src/**/*"]`
 - **tests**: Array of glob patterns for determining tests files of module, _default_: `["tests/**/*"]`
 - **test**: Main entry point for tests into your module, _default_: `"./tests/index"`
 - **clean**: Array of all extension that are removed by `clean` command, _default_: `[]`
 - **target**: This is setting of output target, _default_: `"web"`
 
 More info is available on separate [`servant.json`][2] documentation page.

##### These properties are same like in [`package.json`][1]:
 
  > name, version, description, keywords, author, license, registry

 [1]: https://docs.npmjs.com/files/package.json
 [2]: servant.json.md
 [4]: ../../servant-cli/doc/servant.clia.md
 [InitData]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/src/index.tsx#L41
 [ModulesParams]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/src/index.tsx#L46
 [ModulesData]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/src/index.tsx#L50
 [ChangeBy]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/src/vcs/data.ts#L13
 [Commands]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/src/commands/index.ts#L14
 [CommandsParam]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/src/commands/index.ts#L21
 [CommandResult]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/src/commands/index.ts#L16
 [InitParams]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/src/module/init.ts#L8
 [PackageJson]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-data/src/package.json.ts#L6
 [ServantJson]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-data/src/servant.json.ts#L8